Sms = {
    new_sms_code_interval:'',

    init: function()
    {
        $('body').on('click', '.phone-confirm', function(){
           Sms.getCode(0);
        });

        $('body').on('click', '#btnSmsConfirm', function(){

            $.ajax({ type:"POST",
                cache:false,
                /* url: '/sms-v1',*/
                url: '/ro/sms-v1/check',
                dataType:"json",
                data: {
                    'sms_id':$('#sms_id').val(),
                    'sms_code':$('#sms_code').val(),
                },
                /* callback:$(this).data('callback'),*/
                success: function(data)
                {
                    $('#phone_confirm_container').show();
                    $('#phone_confirm_container').html(data.html);

                    if(data.confirm == true)
                    {
                        $('.phone-confirm').hide();
                    }
                    else{
                        Sms.initSmsCode();
                    }

                }
            });

        });

        $('body').on('click', '#btnSmsNewCode', function(){

            Sms.getCode(1);
        });
    },

    initSmsCode: function()
    {
        clearInterval(Sms.new_sms_code_interval);

        if($('#new_sms_code').length == 1)
        {
            var second = $('#new_sms_code .time').data('second');
            $('#new_sms_code span.time').html(second);

            Sms.new_sms_code_interval = setInterval(function(){
                second--;
                if(second<=0)
                {
                    clearInterval(Sms.new_sms_code_interval);
                    $("#new_sms_code").hide();
                    $("#btnSmsNewCode").show();
                }

                $('#new_sms_code span.time').html(second);
            }, 1000);
        }

    },

    getCode: function(newcode){


        $.ajax({ type:"POST",
            cache:false,
            /* url: '/sms-v1',*/
            url: '/ro/sms-v1/code',
            dataType:"json",
            data: {
                'newcode':newcode
            },
            /* callback:$(this).data('callback'),*/
            success: function(data)
            {
                $('#phone_confirm_container').show();
                $('#phone_confirm_container').html(data.html);


                if(data.confirm == true)
                {
                    $('.phone-confirm').hide();
                }
                else{
                    Sms.initSmsCode();
                }

            }
        });
    }
};

$(function(){
    Sms.init();
});
