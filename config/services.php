<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

//    'mailgun' => [
//        'domain' => env('MAILGUN_DOMAIN'),
//        'secret' => env('MAILGUN_SECRET'),
//    ],

    'facebook' => [
        'client_id' => '827140761213275',
        'client_secret' => '05181a6c91af15458f5646a7f4f47328',
        'redirect' => env('APP_URL') . '/en/login/facebook/callback',

    ],

    'google' => [
        'client_id' => '58570494201-knllbgivbol2vibgumaq74esvuj6lt8t.apps.googleusercontent.com',
        'client_secret' => 'ICUOMLo8ZSUqHiWGTB8zjkRU',
        'redirect' => env('APP_URL') . '/en/login/google/callback',
    ],

//    'twitter' => [
//        'client_id' => 'your client id',
//        'client_secret' => 'your client secret',
//        'redirect' => 'http://localhost:8000/callback/twitter',
//    ],
//
//    'vk' => [
//        'client_id' => 'your client id',
//        'client_secret' => 'your client secret',
//        'redirect' => 'http://localhost:8000/callback/twitter',
//    ],
];
