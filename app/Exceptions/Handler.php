<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $exception
     * @return mixed|void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param Exception $exception
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $exception)
    {
        // dd($exception);
        if (is_null($request->segment(1))) {
            return redirect()->to(LANG);
        }

        if ($request->segment(2) == 'admin')
            $path = 'admin';
        else
            $path = 'front';

        $errorMsg = [];

        if ($this->isHttpException($exception)) {

            // dd($exception);
            switch ($exception->getStatusCode()) {
                case 400:
                    $errorMsg = [
                        'msg' => 'Bad Request!',
                        'status' => 400
                    ];

                    break;

                case 401:
                    $errorMsg = [
                        'msg' => 'Unauthorized!',
                        'status' => 401
                    ];

                    break;

                case 403:
                    $errorMsg = [
                        'msg' => 'Forbidden!',
                        'status' => 403
                    ];

                    break;

                case 404:

                    $errorMsg = [
                        'msg' => 'Page not found!',
                        'status' => 404
                    ];

                    break;

                case 405:
                    $errorMsg = [
                        'msg' => 'Method Not Allowed!',
                        'status' => 405
                    ];

                    break;

                case 500:
                    $errorMsg = [
                        'msg' => 'Internal Server Error!',
                        'status' => 500
                    ];

                    break;

                case 502:
                    $errorMsg = [
                        'msg' => 'Bad Gateway!',
                        'status' => 502
                    ];

                    break;

                case 503:
                    $errorMsg = [
                        'msg' => 'Service Unavailable!',
                        'status' => 503
                    ];

                    break;

                default:
                    return $this->renderHttpException($exception);
                    break;
            }
        }

        if ($exception instanceof TokenMismatchException)
            $errorMsg = [
                'msg' => 'Token expired! Please try again!',
                'status' => 419
            ];

        if (!config('app.debug')) {
            if ($exception instanceof ModelNotFoundException)
                $errorMsg = [
                    'msg' => 'Page not found!',
                    'status' => 404
                ];

            if ($exception instanceof \BadMethodCallException)
                $errorMsg = [
                    'msg' => 'Page not found!',
                    'status' => 404
                ];

            if ($exception instanceof QueryException)
                $errorMsg = [
                    'msg' => 'Internal Server Error!',
                    'status' => 500
                ];
        }

        if (!empty($errorMsg)) {
            if ($request->ajax())
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => $errorMsg['msg'],
                        'type' => 'error'
                    ],
                ], $errorMsg['status']);

            return response()->view("{$path}.errors.{$errorMsg['status']}", [], $errorMsg['status']);
        } else {

//            dd($exception);
            $errorMsg = [
                'msg' => 'Page not found!',
                'status' => 404
            ];

            if ($request->ajax())
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => $errorMsg['msg'],
                        'type' => 'error'
                    ],
                ], $errorMsg['status']);

            return response()->view("{$path}.errors.{$errorMsg['status']}", [], $errorMsg['status']);

        }

        return parent::render($request, $exception);
    }
}
