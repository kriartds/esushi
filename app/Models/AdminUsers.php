<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;

class AdminUsers extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use FilesRelation;


    protected $table = 'admin_users';

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'password', 'remember_token', 'email'
    ];

    protected $fillable = [
        'admin_user_group_id',
        'name',
        'login',
        'email',
        'password',
        'root',
        'active',
        'for_logout',
        'has_exported_products',
        'has_imported_products'
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'users';
        self::$noFile = asset("admin-assets/img/avatar.svg");
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strip_tags($value);
    }

    public function group()
    {
        return $this->hasOne(AdminUserGroup::class, 'id', 'admin_user_group_id');
    }

    public function getIsOnlineAttribute()
    {
        return Cache::get('user-is-online-' . $this->id);
    }

}
