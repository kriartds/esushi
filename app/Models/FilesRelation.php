<?php

namespace App\Models;


trait FilesRelation
{

    public static $component;
    public static $globalLangId;
    public static $noFile;
    public static $isVariation;

    public function files()
    {

        return $this->hasMany('App\Models\FilesItems', (self::$isVariation ? 'variation_id' : 'item_id'), 'id')
            ->where('active', 1)
            ->where(function ($q) {
                if (self::$component)
                    $q->whereHas('component', function ($q) {
                        $q->where('slug', self::$component);
                    });
                else
                    $q->whereNull('component_id');

                if (!self::$isVariation)
                    $q->whereNull('variation_id');
            })
            ->whereRaw(" (CASE WHEN lang_id is null then " . (is_null(self::$globalLangId) ? LANG_ID : self::$globalLangId) . " else lang_id END) = " . (is_null(self::$globalLangId) ? LANG_ID : self::$globalLangId))
            ->orderBy('position');
    }

    public function file()
    {
        return $this->hasOne('App\Models\FilesItems', (self::$isVariation ? 'variation_id' : 'item_id'), 'id')
            ->where('active', 1)
            ->where(function ($q) {
                if (self::$component)
                    $q->whereHas('component', function ($q) {
                        $q->where('slug', self::$component);
                    });
                else
                    $q->whereNull('component_id');

                if (!self::$isVariation)
                    $q->whereNull('variation_id');
            })
            ->whereRaw(" (CASE WHEN lang_id is null then " . (is_null(self::$globalLangId) ? LANG_ID : self::$globalLangId) . " else lang_id END) = " . (is_null(self::$globalLangId) ? LANG_ID : self::$globalLangId))
            ->orderBy('position');
    }

    public function getFirstFileAttribute()
    {

        self::$noFile = is_null(self::$noFile) ? asset("admin-assets/img/no-image.png") : self::$noFile;

        $noFile = self::$noFile;

        $response = (object)[
            'small' => $noFile,
            'medium' => $noFile,
            'large' => $noFile,
            'original' => $noFile,
        ];

        $currFileItem = $this->file;

        if (!is_null($currFileItem)) {
            $currFile = $currFileItem->file;

            if (file_exists(public_path($currFile->file))) {
                $fileLocations = getAllFileLocationsByUrl($currFile->file, 'magic');

                $response = (object)array_map(function ($val) {
                    return asset($val);
                }, $fileLocations);
            }
        }

        return $response;
    }

    public function getAllFilesExceptFirstAttribute()
    {
        $response = [];
        self::$noFile = is_null(self::$noFile) ? asset("admin-assets/img/no-image.png") : self::$noFile;
        $noFile = self::$noFile;

        $firstFileId = @$this->file->id;
        $allFiles = $this->files->where('active', 1)->where('id', '!=', $firstFileId);

        if (!$allFiles->isEmpty())
            foreach ($allFiles as $file) {
                $currFileItem = $file->file;

                if (!is_null($file) && file_exists(public_path($currFileItem->file))) {
                    $fileLocations = getAllFileLocationsByUrl($currFileItem->file, 'magic');

                    $response[] = (object)array_map(function ($val) {
                        return asset($val);
                    }, $fileLocations);
                }
            }
        else
            $response[] = (object)[
                'small' => $noFile,
                'medium' => $noFile,
                'large' => $noFile,
                'original' => $noFile,
            ];

        return $response;
    }

    public function getAllFilesAttribute()
    {
        $response = [];
        self::$noFile = is_null(self::$noFile) ? asset("admin-assets/img/no-image.png") : self::$noFile;
        $noFile = self::$noFile;

        $allFiles = $this->files->where('active', 1);

        if (!$allFiles->isEmpty())
            foreach ($allFiles as $file) {
                $currFileItem = $file->file;

                if (!is_null($file) && file_exists(public_path($currFileItem->file))) {
                    $fileLocations = getAllFileLocationsByUrl($currFileItem->file, 'magic');

                    $response[] = (object)array_map(function ($val) {
                        return asset($val);
                    }, $fileLocations);
                }
            }
        else
            $response[] = (object)[
                'small' => $noFile,
                'medium' => $noFile,
                'large' => $noFile,
                'original' => $noFile,
            ];

        return $response;
    }

}