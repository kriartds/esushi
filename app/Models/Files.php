<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class Files extends Model
{
    protected $table = 'files';

    protected $fillable = [
        'file',
        'original_name',
        'active',
        'position',
        'type',
        'mime_type',
        'size',
        'resolution',
    ];

    protected $appends = [
        'originalFileName',
        'fileLocation',
        'fileLocationBySize',
//        'fileSize',
//        'fileResolution'
    ];

    protected $casts = [
        'resolution' => 'object'
    ];

    public function filesItems()
    {
        return $this->hasMany(FilesItems::class, 'file_id', 'id');
    }

    public function getOriginalFileNameAttribute()
    {

        $response = basename($this->file);

        if (!empty($response) && !is_null($response)) {

            $extension = strstr(strstr($response, '-_-'), '.');

            $response = substr($response, 0, strpos($response, '-_-')) . $extension;
        } else
            $response = 'No name';

        return subStrText($response, 80);
    }

    public function getFileLocationAttribute()
    {

        if (@strpos(mime_content_type($this->file), 'image') !== false || pathinfo($this->file, PATHINFO_EXTENSION) == 'svg') {
            $response = $this->file;
        } else {

            $extension = substr(strstr(strstr(basename($this->file), '-_-'), '.'), 1);
            $response = 'admin-assets/img/file-icons/' . strtolower($extension) . '.svg';

        }

        return $response;
    }

    public function getFileLocationBySizeAttribute()
    {

        if (@strpos(mime_content_type($this->file), 'image') !== false || pathinfo($this->file, PATHINFO_EXTENSION) == 'svg') {
            $fileLocations = getAllFileLocationsByUrl($this->file, 'magic');

            if (empty($fileLocations))
                return response()->json([
                    'status' => false
                ]);

            $fileLocations = array_map(function ($val) {
                return asset($val);
            }, $fileLocations);

            $response = array_key_exists('small', $fileLocations) ? $fileLocations['small'] : $fileLocations['original'];
        } else {
            $extension = substr(strstr(strstr(basename($this->file), '-_-'), '.'), 1);
            $response = 'admin-assets/img/file-icons/' . strtolower($extension) . '.svg';

        }

        return $response;
    }

//    public function getFileSizeAttribute()
//    {
//        return @File::size($this->file);
//    }
//
//    public function getFileResolutionAttribute()
//    {
//        $response = [];
//
//        if (file_exists($this->file) && strpos(mime_content_type($this->file), 'image') !== false && pathinfo($this->file, PATHINFO_EXTENSION) != 'svg' && pathinfo($this->file, PATHINFO_EXTENSION) != 'ico') {
//            $image = Image::make($this->file);
//
//            $response = [
//                'width' => @$image->width(),
//                'height' => @$image->height()
//            ];
//        }
//
//        return $response;
//    }
}
