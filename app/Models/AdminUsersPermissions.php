<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminUsersPermissions extends Model
{
    protected $table = 'admin_users_permissions';

    protected $fillable = [
        'admin_user_group_id',
        'component_id',
        'for_create',
        'for_edit',
        'for_delete',
        'for_view',
        'for_active'
    ];

    public function adminUserGroup()
    {
        return $this->hasOne(AdminUserGroup::class, 'id', 'admin_user_group_id');
    }

    public function componentId()
    {
        return $this->hasOne(ComponentsId::class, 'id', 'component_id');
    }
}
