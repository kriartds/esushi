<?php

namespace App\Models;

use App\Http\Controllers\Admin\PagesController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SettingsTrans extends Model
{

    use FilesRelation;

    protected $table = 'settings_trans';

    protected $fillable = [
        'setting_trans_id',
        'lang_id',
        'value',
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'translatable-settings';

    }

}
