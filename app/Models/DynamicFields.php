<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DynamicFields extends Model
{

    protected $table = 'dynamic_fields';

    protected $fillable = [
        'dynamic_field_id',
        'lang_id',
//        'name',
        'placeholder',
        'value',
    ];

}
