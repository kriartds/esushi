<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nwidart\Modules\Facades\Module;

class ComponentsId extends Model
{
    use SoftDeletes;
    use FilesRelation;

    protected $table = 'components_id';

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'controller',
        'module',
        'for_root',
        'for_count',
        'for_sidebar',
        'for_menu',
        'for_statistics',
        'file',
        'parent',
        'children'
    ];

    protected $fillable = [
        'p_id',
        'slug',
        'position',
        'module',
        'controller',
        'level',
        'active',
        'for_root',
        'for_count',
        'for_sidebar',
        'for_menu',
        'for_statistics',
        'for_sitemap',
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'components';
        self::$noFile = asset("admin-assets/img/small-icons/side-no-icon.svg");
        self::$globalLangId = request()->segment(6, null);

    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function scopeComponentsWithPermissions($q)
    {

        $user = auth()->guard('admin')->user();
        $isGroupRoot = $user->group->isRoot;

        return $q->where(function ($q) use ($isGroupRoot, $user) {
            if (!$isGroupRoot)
                $q->where('for_root', 0);

            if (!$user->root)
                $q->whereHas('permissions', function ($q) use ($user) {
                    $q->where('admin_user_group_id', $user->admin_user_group_id);
                });
        })
            ->with([
                'children' => function ($q) use ($isGroupRoot, $user) {
                    $q->where('active', 1);
                    $q->where(function ($q) use ($isGroupRoot) {
                        if (!$isGroupRoot)
                            $q->where('for_root', 0);
                    });
                    $q->whereHas('permissions', function ($q) use ($user) {
                        $q->where('admin_user_group_id', $user->admin_user_group_id);
                    });
                }
            ])
            ->orderBy('position');
    }

    public function permissions()
    {
        return $this->hasMany('App\Models\AdminUsersPermissions', 'component_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\ComponentsId', 'p_id', 'id')->orderBy('position');
    }

    public function parents(){
        return $this->hasMany('App\Models\ComponentsId', 'id', 'p_id');
    }

    public function recursiveParents()
    {
        return $this->parents()->with(['recursiveParents', 'recursiveParents.globalName', 'globalName'])->orderBy('position');
    }

    public function recursiveChildren()
    {
        return $this->children()->with('recursiveChildren')->orderBy('position');
    }

    public function parent()
    {
        return $this->hasOne('App\Models\ComponentsId', 'id', 'p_id');
    }

    public function globalName()
    {
        return $this->hasOne('App\Models\Components', 'component_id', 'id')->whereIn('components.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(components.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('App\Models\Components', 'component_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('App\Models\Components', 'component_id', 'id');
    }

    public function galleryFiles()
    {
        return $this->hasMany('App\Models\FilesItems', 'component_id', 'id')
            ->whereRaw(" (CASE WHEN lang_id is null then " . (is_null(self::$globalLangId) ? LANG_ID : self::$globalLangId) . " else lang_id END) = " . (is_null(self::$globalLangId) ? LANG_ID : self::$globalLangId));
    }

    public function getComponentsCountAttribute()
    {

        $response = '';

        if ($this->module && Module::has($this->module)) {
            $existFile = file_exists(module_path($this->module) . "/Http/Controllers/{$this->controller}.php");
            $namespace = "Modules\\{$this->module}\\Http\\Controllers\\{$this->controller}";
        } else {
            $existFile = file_exists(app_path("Http/Controllers/Admin/{$this->controller}.php"));
            $namespace = "App\\Http\\Controllers\\Admin\\{$this->controller}";
        }

        if ($this->for_count && $existFile) {
            $method = 'countItems';

            try {
                $response = app()->call("{$namespace}@{$method}");
            } catch (\Exception $e) {
            }

            if ($response < 1)
                $response = '';
        }

        return $response;
    }

    public function getComponentsCountForWidgetsAttribute()
    {

        $response = '';

        if ($this->module && Module::has($this->module)) {
            $existFile = file_exists(module_path($this->module) . "/Http/Controllers/{$this->controller}.php");
            $namespace = "Modules\\{$this->module}\\Http\\Controllers\\{$this->controller}";
        } else {
            $existFile = file_exists(app_path("Http/Controllers/Admin/{$this->controller}.php"));
            $namespace = "App\\Http\\Controllers\\Admin\\{$this->controller}";
        }

        if ($this->for_statistics && $existFile) {
            $method = 'widgetsCountItems';

            try {
                $response = app()->call("{$namespace}@{$method}");
            } catch (\Exception $e) {
            }

            if ($response < 1)
                $response = '';
        }

        return $response;
    }

    public function getHasChildrenAttribute()
    {
        return $this->children->isNotEmpty();
    }

    public function getHasChildrenWithTrashedAttribute()
    {
        return $this->children()->withTrashed()->get()->isNotEmpty();
    }

    public function getHasActiveChildrenAttribute()
    {
        return $this->children->where('slug', request()->segment(3))->isNotEmpty();
    }

}
