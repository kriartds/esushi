<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Components extends Model
{
    protected $table = 'components';

    protected $fillable = [
        'component_id',
        'lang_id',
        'name',
        'name_category'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

    public function componentId()
    {
        return $this->hasOne('App\Models\ComponentsId', 'id', 'component_id');
    }
}
