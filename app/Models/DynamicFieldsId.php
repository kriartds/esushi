<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DynamicFieldsId extends Model
{

    use FilesRelation;

    protected $table = 'dynamic_fields_id';

    protected $fillable = [
        'component_id',
        'item_id',
        'p_id',
        'field_type_id',
        'slug',
        'repeater_group',
        'class_name',
        'attributes',
        'is_required',
        'label_name'
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$globalLangId = request()->segment(6, null);
    }

    public function globalName()
    {
        return $this->hasOne('App\Models\DynamicFields', 'dynamic_field_id', 'id')->where(function ($q) {
            $q->where('dynamic_fields.lang_id', LANG_ID);
            $q->orWhereNull('dynamic_fields.lang_id');
        })->orderByRaw("FIELD(dynamic_fields.lang_id, '" . LANG_ID . "', 'null' ) ASC ");
    }

//    public function itemByLang()
//    {
//        return $this->hasOne('App\Models\DynamicFields', 'dynamic_field_id', 'id')->where('lang_id', self::$globalLangId);
//    }

    public function itemByLang()
    {
        return $this->hasOne('App\Models\DynamicFields', 'dynamic_field_id', 'id')->where(function ($q) {
            $q->where('dynamic_fields.lang_id', self::$globalLangId);
            $q->orWhereNull('dynamic_fields.lang_id');
        })->orderByRaw("FIELD(dynamic_fields.lang_id, '" . self::$globalLangId . "', 'null' ) ASC ");
    }

    public function items()
    {
        return $this->hasMany('App\Models\DynamicFields', 'dynamic_field_id', 'id');
    }

    public function fieldType()
    {
        return $this->hasOne('App\Models\DynamicFieldsTypes', 'id', 'field_type_id');
    }

    public function fieldValues()
    {
        return $this->hasMany('App\Models\DynamicFieldsValuesId', 'dynamic_field_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\DynamicFieldsId', 'p_id', 'id');
    }

    public function component()
    {
        return $this->hasOne('App\Models\ComponentsId', 'id', 'component_id');
    }

}
