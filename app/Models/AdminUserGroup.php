<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class AdminUserGroup extends Model
{
    use SoftDeletes;

    protected $table = 'admin_user_group';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'active',
        'name',
        'slug',
        'root'
    ];

    public function scopeIsRoot($q)
    {
        return $q->where('root', 1);
    }

    public function permissions()
    {
        return $this->hasMany(AdminUsersPermissions::class, 'admin_user_group_id', 'id');
    }

    public function users()
    {
        return $this->hasMany(AdminUsers::class, 'admin_user_group_id', 'id');
    }

    public function getIsRootAttribute()
    {
        return (int)$this->root === 1;
    }
}
