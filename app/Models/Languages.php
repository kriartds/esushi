<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Languages extends Model
{
    use SoftDeletes;
    use FilesRelation;

    protected $table = 'languages';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'slug',
        'position',
        'is_default',
        'active'
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'languages';
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

}
