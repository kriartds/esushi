<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DynamicFieldsTypes extends Model
{

    protected $table = 'dynamic_fields_types';

    protected $fillable = [
        'slug',
        'with_many_values'
    ];

    protected $with = [
        'fields'
    ];

    public function fields()
    {
        return $this->hasMany('App\Models\DynamicFieldsId', 'field_type_id', 'id');
    }

}
