<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DynamicFieldsValues extends Model
{

    protected $table = 'dynamic_fields_values';

    protected $fillable = [
        'dynamic_fields_value_id',
        'lang_id',
        'name',
    ];

}
