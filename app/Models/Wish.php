<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Products\Models\ProductsItemsId;


class Wish extends Model
{

    protected $table = 'wish';

    protected $fillable = [
        'user_id',
        'auth_user_id'
    ];

    public function wishProducts()
    {
        return $this->hasMany(WishProducts::class, 'wish_id', 'id');
    }

    public function products()
    {
        return $this->hasManyThrough(ProductsItemsId::class, WishProducts::class, 'wish_id', 'id', 'id', 'products_item_id');
    }
}
