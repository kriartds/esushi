<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Products\Models\ProductsItemsId;


class WishProducts extends Model
{

    protected $table = 'wish_products';

    protected $fillable = [
        'wish_id',
        'products_item_id'
    ];

    public function wish()
    {
        return $this->hasOne(Wish::class, 'id', 'wish_id');
    }

    public function product()
    {
        return $this->hasOne(ProductsItemsId::class, 'id', 'products_item_id');
    }
}
