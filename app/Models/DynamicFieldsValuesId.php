<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DynamicFieldsValuesId extends Model
{

    protected $table = 'dynamic_fields_values_id';

    protected $fillable = [
        'dynamic_field_id',
        'input_key',
        'is_selected'
    ];

    public static $globalLangId;

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$globalLangId = request()->segment(6, null);
    }

    public function globalName()
    {
        return $this->hasOne(DynamicFieldsValues::class, 'dynamic_fields_value_id', 'id')->whereIn('dynamic_fields_values.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(dynamic_fields_values.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('App\Models\DynamicFieldsValues', 'dynamic_fields_value_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('App\Models\DynamicFieldsValues', 'dynamic_fields_value_id', 'id');
    }

}
