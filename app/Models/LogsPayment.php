<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogsPayment extends Model
{
    protected $table = 'logs_payments';

    protected $fillable = [
        'user_type',
        'user_id',
        'user_ip',
        'cart_id',
        'action',
        'text',
        'details',
    ];

    protected $casts = [
        'details' => 'object'
    ];

    public static function log($action, $cartid, $text, array $details=null){
        //dd(auth()->guard());
        self::create([
            'user_type' => auth()->guard('web')->check() || auth()->guard()->guest() ? 'user' : 'admin',
            'user_id' => auth()->check() ? auth()->user()->id : null,
            'user_ip' => request()->ip(),
            'cart_id' => $cartid,
            'text' => $text,
            'details' => $details,
            'action' => $action
        ]);
    }
}
