<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FilesItems extends Model
{
    protected $table = 'files_items';

    protected $fillable = [
        'file_id',
        'component_id',
        'item_id',
        'variation_id',
        'lang_id',
        'active',
        'position',
    ];

    public function file()
    {
        return $this->hasOne('App\Models\Files', 'id', 'file_id');
    }

    public function component()
    {
        return $this->hasOne('App\Models\ComponentsId', 'id', 'component_id');
    }
}
