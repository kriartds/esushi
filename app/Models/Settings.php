<?php

namespace App\Models;

use App\Http\Controllers\Admin\PagesController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Settings extends Model
{

    use FilesRelation;

    protected $table = 'settings';

    protected $fillable = [
        'slug',
        'value'
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'settings';

    }

}
