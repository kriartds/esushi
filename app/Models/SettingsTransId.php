<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingsTransId extends Model
{

    use FilesRelation;

    protected $table = 'settings_trans_id';

    protected $fillable = [
        'slug',
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'translatable-settings';
        self::$globalLangId = !is_null(request()->get('langId')) ? (int)request()->get('langId') : LANG_ID;

    }

    public function globalName()
    {
        return $this->hasOne(SettingsTrans::class, 'setting_trans_id', 'id')->whereIn('settings_trans.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(settings_trans.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('App\Models\SettingsTrans', 'setting_trans_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('App\Models\SettingsTrans', 'setting_trans_id', 'id');
    }

}
