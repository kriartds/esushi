<?php

namespace App\IikoApi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
//use Modules\Products\Models\ProductsAdditionsId;
use Illuminate\Support\Facades\Session;
use Modules\Orders\Models\Cart;
use Modules\Products\Models\ProductsItemsId;
use App\IikoApi\Models\LogIiko;
use Modules\Restaurants\Models\RestaurantsId;
use Modules\SiteUsers\Models\Users;

class IikoClient extends Client
{
    private $client;
    private $iiko_config = [];
    private $response = [];
    private $access_token = '';
    private $create_order_attempts = 0;

    public function __construct(RestaurantsId $restaurant = null)
    {
        if(!is_null($restaurant)) {
            $this->iiko_config = [
                'organization_id' => $restaurant->iiko_id,
                'apiLogin' => 'a04140f0',
                'terminal_group_id' => $restaurant->iiko_terminal_id,
            ];
        }else{
            $this->iiko_config['apiLogin'] = 'a04140f0';
        }

        $options = [
            'base_uri' => 'https://api-eu.iiko.services/api/1/',//test
            'debug' => false,
            'verify' => false,
            'headers' => [
                //'Authorization' => 'Bearer '.$this->getAccessToken(),
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ]
            //'handler' => $handler,
            //'curl' => array( CURLOPT_SSL_VERIFYPEER => false, CURLOPT_SSL_VERIFYHOST => false ),
            //'cert' => [storage_path('certificates/0149583.pem'), 'Za86DuC$'],
        ];

        parent::__construct($options);
    }

    private function getResponse()
    {
        return $this->response;
    }

    private function toArray($str)
    {
        return json_decode($str, true);
    }

    static public function getGUID(){
        if (function_exists("com_create_guid")){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charId = md5(uniqid(rand(), true));
            $hyphen = chr(45);// "-"
            $uuid =substr($charId, 0, 8).$hyphen
                .substr($charId, 8, 4).$hyphen
                .substr($charId,12, 4).$hyphen
                .substr($charId,16, 4).$hyphen
                .substr($charId,20,12);
            return $uuid;
        }
    }

    /**
     *get token from iiko
    */
    public function getAccessToken()
    {
        if(session()->has('iiko_access_token'))
        {
            $this->access_token = session()->get('iiko_access_token');
            return $this->access_token;
        }
        else
        {
            try
            {
                $params = [
                    "body" => json_encode([
                        'apiLogin' => $this->iiko_config['apiLogin']
                    ]),
                ];

                $temp_resp = $this->request('POST', 'access_token', $params);

                if($temp_resp->getStatusCode() === 200){
                    $this->access_token = $this->toArray($temp_resp->getBody()->getContents())['token'];
                    session()->put('iiko_access_token', $this->access_token);
                    session()->save();
                }

                LogIiko::log('getAccessToken|success', 0, $this->access_token);

            }catch (ClientException $e){
                $this->access_token = '';
                LogIiko::log('getAccessToken|error', 0, $e->getMessage());
                //dd($e->getMessage());
            }

            return $this->access_token;
        }
    }

    public function getRegions($organizationId=null)
    {
        try
        {
            if(isset($this->iiko_config['organization_id']) && !empty($this->iiko_config['organization_id'])){
                $body['organizationIds'] = [$this->iiko_config['organization_id']];
            }elseif (!is_null($organizationId)){
                if(is_array($organizationId)) $body['organizationIds'] = $organizationId;
                else $body['organizationIds'] = [$organizationId];
            }

            $params = [
                'body' => json_encode($body),
                'headers' => [
                    'Authorization' => 'Bearer '.$this->getAccessToken(),
                ]
            ];

            LogIiko::log('getRegions|Request', 0, "", $params);

            $temp_resp = $this->request('POST', 'regions', $params);

            $body = $temp_resp->getBody()->getContents();

            LogIiko::log('getRegions|Response', 0, $body, $this->toArray($body));

            return $this->toArray($body);

        }catch (ClientException $e){
            LogIiko::log('getRegions|error', 0, $e->getMessage());
        }

        return false;
    }

    public function getCities($organizationId=null)
    {
        try
        {
            if(isset($this->iiko_config['organization_id']) && !empty($this->iiko_config['organization_id'])){
                $body['organizationIds'] = [$this->iiko_config['organization_id']];
            }elseif (!is_null($organizationId)){
                if(is_array($organizationId)) $body['organizationIds'] = $organizationId;
                else $body['organizationIds'] = [$organizationId];
            }

            $params = [
                'body' => json_encode($body),
                'headers' => [
                    'Authorization' => 'Bearer '.$this->getAccessToken(),
                ]
            ];

            LogIiko::log('getCities|Request', 0, "", $params);

            $temp_resp = $this->request('POST', 'cities', $params);

            $body = $temp_resp->getBody()->getContents();

            LogIiko::log('getCities|Response', 0, $body, $this->toArray($body));

            return $this->toArray($body);

        }catch (ClientException $e)
        {
            if($e->getCode() === 401)
            {
                session()->forget('iiko_access_token');
                return $this->getCities($organizationId);
            }

            LogIiko::log('getCities|error', 0, $e->getMessage());
        }

        return false;
    }

    public function getStreetsByCity($cityId, $organizationId=null)
    {
        try
        {
            if(isset($this->iiko_config['organization_id']) && !empty($this->iiko_config['organization_id'])){
                $body['organizationId'] = $this->iiko_config['organization_id'];
            }elseif (!is_null($organizationId)){
                $body['organizationId'] = $organizationId;
            }

            $body['cityId'] = $cityId;

            $params = [
                'body' => json_encode($body),
                'headers' => [
                    'Authorization' => 'Bearer '.$this->getAccessToken(),
                ]
            ];

            LogIiko::log('getStreetsByCity|Request', 0, "", $params);

            $temp_resp = $this->request('POST', 'streets/by_city', $params);

            $body = $temp_resp->getBody()->getContents();

            LogIiko::log('getStreetsByCity|Response', 0, $body, $this->toArray($body));

            return $this->toArray($body);

        }catch (ClientException $e){

            if($e->getCode() === 401)
            {
                session()->forget('iiko_access_token');
                return $this->getStreetsByCity($cityId, $organizationId);
            }

            LogIiko::log('getStreetsByCity|error', 0, $e->getMessage());
        }

        return false;
    }

    public function getOrganizations($organizationId=null)
    {
        try
        {
            $body['returnAdditionalInfo'] = true;
            $body['includeDisabled'] = false;

            if(isset($this->iiko_config['organization_id']) && !empty($this->iiko_config['organization_id'])){
                $body['organizationIds'] = [$this->iiko_config['organization_id']];
            }elseif (!is_null($organizationId)){
                $body['organizationIds'] = [$organizationId];
            }

            $temp_resp = $this->request('POST', 'organizations', [
                'body' => json_encode($body),
                'headers' => [
                    'Authorization' => 'Bearer '.$this->getAccessToken(),
                ]
            ]);

            if($temp_resp->getStatusCode() === 200)
            {
                $body = $temp_resp->getBody()->getContents();

                LogIiko::log('getOrganizations|success', 0, $body);

                return $this->toArray($body);
            }

        }catch (ClientException $e){

            if($e->getCode() === 401)
            {
                session()->forget('iiko_access_token');
                return $this->getOrganizations($organizationId);
            }

            LogIiko::log('getOrganizations|error', 0, $e->getMessage());
        }

        return false;
    }

    public function getTerminals($organizationIds=null)
    {
        try
        {
            if(isset($this->iiko_config['organization_id']) && !empty($this->iiko_config['organization_id'])){
                $body['organizationIds'] = [$this->iiko_config['organization_id']];
            }elseif (!is_null($organizationIds)){
                if(is_array($organizationIds)) $body['organizationIds'] = $organizationIds;
                else $body['organizationIds'] = [$organizationIds];
            }

            $temp_resp = $this->request('POST', 'terminal_groups', [
                'body' => json_encode($body),
                'headers' => [
                    'Authorization' => 'Bearer '.$this->getAccessToken(),
                ]
            ]);

            if($temp_resp->getStatusCode() === 200)
            {
                $body = $temp_resp->getBody()->getContents();

                LogIiko::log('getTerminals|success', 0, $body);

                return $this->toArray($body);
            }

        }catch (ClientException $e){

            if($e->getCode() === 401)
            {
                session()->forget('iiko_access_token');
                return $this->getTerminals($organizationIds);
            }

            LogIiko::log('getTerminals|error', 0, $e->getMessage());
        }

        return false;
    }

    public function getOrderTypes($organizationIds=null)
    {
        try
        {
            if(isset($this->iiko_config['organization_id']) && !empty($this->iiko_config['organization_id'])){
                $body['organizationIds'] = [$this->iiko_config['organization_id']];
            }elseif (!is_null($organizationIds)){
                if(is_array($organizationIds)) $body['organizationIds'] = $organizationIds;
                else $body['organizationIds'] = [$organizationIds];
            }

            $temp_resp = $this->request('POST', 'deliveries/order_types', [
                'body' => json_encode($body),
                'headers' => [
                    'Authorization' => 'Bearer '.$this->getAccessToken(),
                ]
            ]);

            $body = $temp_resp->getBody()->getContents();

            LogIiko::log('getOrderTypes|success', 0, $body);

            return $this->toArray($body);

        }catch (ClientException $e){

            if($e->getCode() === 401)
            {
                session()->forget('iiko_access_token');
                return $this->getOrderTypes($organizationIds);
            }

            LogIiko::log('getOrderTypes|error', 0, $e->getMessage());
        }

        return false;
    }

    public function getDiscounts($organizationId=null)
    {
        try
        {
            if(isset($this->iiko_config['organization_id']) && !empty($this->iiko_config['organization_id'])){
                $body['organizationIds'] = [$this->iiko_config['organization_id']];
            }elseif (!is_null($organizationId)){
                $body['organizationIds'] = [$organizationId];
            }

            $temp_resp = $this->request('POST', 'discounts', [
                'body' => json_encode($body),
                'headers' => [
                    'Authorization' => 'Bearer '.$this->getAccessToken(),
                ]
            ]);

            $body = $temp_resp->getBody()->getContents();

            LogIiko::log('getDiscounts|success', 0, $body);

            return $this->toArray($body);

        }catch (ClientException $e){

            if($e->getCode() === 401)
            {
                session()->forget('iiko_access_token');
                return $this->getDiscounts($organizationId);
            }

            LogIiko::log('getDiscounts|error', 0, $e->getMessage());
        }

        return false;
    }

    public function getPaymentTypes($organizationIds=null)
    {
        try
        {
            if(isset($this->iiko_config['organization_id']) && !empty($this->iiko_config['organization_id'])){
                $body['organizationIds'] = [$this->iiko_config['organization_id']];
            }elseif (!is_null($organizationIds)){
                if(is_array($organizationIds)) $body['organizationIds'] = $organizationIds;
                else $body['organizationIds'] = [$organizationIds];
            }

            $params = [
                'body' => json_encode($body),
                'headers' => [
                    'Authorization' => 'Bearer '.$this->getAccessToken(),
                ]
            ];

            $temp_resp = $this->request('POST', 'payment_types', $params);

            $body = $temp_resp->getBody()->getContents();

            LogIiko::log('getPaymentTypes|success', 0, '');

            return $this->toArray($body);

        }catch (ClientException $e){

            if($e->getCode() === 401)
            {
                session()->forget('iiko_access_token');
                return $this->getPaymentTypes($organizationIds);
            }

            LogIiko::log('getPaymentTypes|error', 0, $e->getMessage());
        }
    }

    public function getNomenclature($organizationId=null)
    {
        try
        {
            if(isset($this->iiko_config['organization_id']) && !empty($this->iiko_config['organization_id'])){
                $body['organizationId'] = $this->iiko_config['organization_id'];
            }elseif (!is_null($organizationId)){
                $body['organizationId'] = $organizationId;
            }

            $params = [
                'body' => json_encode($body),
                'headers' => [
                    'Authorization' => 'Bearer '.$this->getAccessToken(),
                ]
            ];

            $temp_resp = $this->request('POST', 'nomenclature', $params);

            $body = $temp_resp->getBody()->getContents();

            LogIiko::log('getMenu|success', 0, '');

            return $this->toArray($body);

        }catch (ClientException $e){

            if($e->getCode() === 401)
            {
                session()->forget('iiko_access_token');
                return $this->getNomenclature($organizationId);
            }

            //dd($e->getMessage());

            LogIiko::log('getMenu|error', 0, $e->getMessage());
        }
    }

    private function prepareCreateOrder(Cart $cart)
    {
        $items = [];

        foreach ($cart->cartProducts as $cartProduct) {
            $items[] = [
                'type' => 'Product',
                'productId' => $cartProduct->product->item_type_id == 1 ? $cartProduct->product->iiko_id : $cartProduct->variation->iiko_id,
                'name' => @$cartProduct->product->globalName->name,
                'amount' => $cartProduct->quantity,
            ];
        }

        $persons_number = 0;
        if(isset($cart->details->number_of_simple_sticks) && !empty($cart->details->number_of_simple_sticks)){
            $persons_number += $cart->details->number_of_simple_sticks;
            $items[] = [
                'type' => 'Product',
                'productId' => 'e4e014af-be6e-41dd-9af4-8aac9218cb22',
                'name' => 'Simple chopsticks',
                'amount' => $cart->details->number_of_simple_sticks,
            ];
        }

        if(isset($cart->details->number_of_beginner_sticks) && !empty($cart->details->number_of_beginner_sticks)){
            $persons_number += $cart->details->number_of_beginner_sticks;
            $items[] = [
                'type' => 'Product',
                'productId' => '5bed6bb3-75c6-4afa-b6fe-6e304ff27f59',
                'name' => 'Beginner chopsticks',
                'amount' => $cart->details->number_of_beginner_sticks,
            ];
        }

        //add shipping or pickup
        if(!is_null($cart->shipping_details))
        {
            $order['orderTypeId'] = '76067ea3-356f-eb93-9d14-1fa00d082c4e';
            //$order['orderServiceType'] = 'DeliveryByCourier';

            $order['deliveryPoint']['address'] = [
                'street' => [
                    'id' => $cart->shipping_details->iiko_street_id,
                    //'city' => $cart->shipping_details->city,
                ],
                'house' => $cart->shipping_details->house,
                'flat' => $cart->shipping_details->apartment,
                'floor' => $cart->shipping_details->floor,
                'doorphone' => $cart->shipping_details->intercom,
            ];

            if(isset($cart->shipping_details->ground_house) && !empty(isset($cart->shipping_details->ground_house))){
                $order['deliveryPoint']['comment'] = __('front.ground_house');
            }

            if(isset($cart->shipping_details->iiko_service_id) && !empty($cart->shipping_details->iiko_service_id) && $cart->shipping_amount > 0){
                $items[] = [
                    'type' => 'Product',
                    'productId' => $cart->shipping_details->iiko_service_id,
                    'name' => $cart->shipping_details->city,
                    'amount' => 1,
                ];
            }

        }else{
            $order['orderTypeId'] = '5b1508f9-fe5b-d6af-cb8d-043af587d5c2';
            //$order['orderServiceType'] = 'DeliveryPickUp';
        }

        $order['items'] = $items;

        if($cart->payment_method == 'cash'){
            $order['payments'][] = [
                'paymentTypeKind' => 'Cash',
                'sum' => $cart->amount,
                'paymentTypeId' => '09322f46-578a-d210-add7-eec222a08871',
                'isProcessedExternally' => 'FALSE',
            ];
        }elseif($cart->payment_method === 'card_curier'){
            $order['payments'][] = [
                'paymentTypeKind' => 'Card',
                'sum' => $cart->amount,
                'paymentTypeId' => 'bbb50f73-6d8b-4912-9fbf-ed4e3fd5e91a',
                'isProcessedExternally' => 'FALSE',
            ];
        }else{
            $order['payments'][] = [
                'paymentTypeKind' => 'Card',
                'sum' => $cart->amount,
                'paymentTypeId' => '43240813-d755-43b1-92cf-4403ceab624f',
                'isProcessedExternally' => 'TRUE',
            ];
        }

        $order['phone'] = "+373".str_replace('+373', '', $cart->phone);

        if(!is_null($cart->details->comment))
            $order['comment'] = $cart->details->comment;

        $customer = [
            'name' => $cart->details->name,
            'email' => $cart->email,
        ];

        if(!is_null($cart->details->comment)) $customer['comment'] = $cart->details->comment;

        if(!is_null($cart->auth_user_id)){
            $user = Users::find($cart->auth_user_id);
            if(!is_null($user) && !is_null($user->iiko_id))
            {
                $customer['id'] = $user->iiko_id;
            }
        }

        $order['customer'] = $customer;

        $order['guests'] = [
            'count' => $persons_number,
            'splitBetweenPersons' => 'TRUE',
        ];

        $order['sourceKey'] = "Site";

        return $order;
    }

    public function createOrder(Cart $cart, $organizationId = null, $terminalGroupId = null)
    {
        try
        {
            if(isset($this->iiko_config['organization_id']) && !empty($this->iiko_config['organization_id'])){
                $body['organizationId'] = $this->iiko_config['organization_id'];
                $body['terminalGroupId'] = $this->iiko_config['terminal_group_id'];
            }elseif (!is_null($organizationId)){
                $body['organizationId'] = $organizationId;
                $body['terminalGroupId'] = $terminalGroupId;
            }

            //$body['createOrderSettings'] = ['mode' => 'Async'];

            $body['order'] = $this->prepareCreateOrder($cart);

            $params = [
                'body' => json_encode($body),
                'headers' => [
                    'Authorization' => 'Bearer '.$this->getAccessToken(),
                ]
            ];
            //dd(json_encode($body));
            LogIiko::log('createOrder|Request', $cart->id, '', $params);

            //possible method will be different for Dselivery and pickup
            $temp_resp = $this->request('POST', 'deliveries/create', $params);

            $body = $temp_resp->getBody()->getContents();

            LogIiko::log('createOrder|success', $cart->id, 'Successfuly created', $this->toArray($body));

            return $this->toArray($body);

        }catch (ClientException $e){

            $resp_arr = $this->toArray($e->getResponse()->getBody()->getContents());

            LogIiko::log('createOrder|error', $cart->id, $e->getMessage(), $resp_arr);

            if($e->getCode() === 401 && $this->create_order_attempts < 5)
            {
                $this->create_order_attempts++;
                session()->forget('iiko_access_token');
                return $this->createOrder($cart, $organizationId, $terminalGroupId);
            }

            //dump($resp_arr);
            //dd($e->getMessage());
        }

        return null;
    }

    public function closeOrder(Cart $cart, $organizationId = null)
    {
        try
        {
            if(isset($this->iiko_config['organization_id']) && !empty($this->iiko_config['organization_id'])){
                $body['organizationId'] = $this->iiko_config['organization_id'];
            }elseif (!is_null($organizationId)){
                $body['organizationId'] = $organizationId;
            }

            $body['orderId'] = $cart->iiko_id;

            $params = [
                'body' => json_encode($body),
                'headers' => [
                    'Authorization' => 'Bearer '.$this->getAccessToken(),
                ]
            ];

            $temp_resp = $this->request('POST', 'deliveries/close', $params);

            $body = $temp_resp->getBody()->getContents();

            LogIiko::log('closeOrder:success', $cart->id, json_encode($body));

            return $this->toArray($body);

        }catch (ClientException $e){

            if($e->getCode() === 401)
            {
                session()->forget('iiko_access_token');
                return $this->createOrder($organizationId);
            }

            LogIiko::log('closeOrder|error', $cart->id, $e->getMessage(), $this->toArray($e->getResponse()->getBody()->getContents()));
        }
    }

    public function checkOrder($organizationId = null, Cart $cart = null)
    {
        try
        {
            if(isset($this->iiko_config['organization_id']) && !empty($this->iiko_config['organization_id'])){
                $body['organizationId'] = $this->iiko_config['organization_id'];
            }elseif (!is_null($organizationId)){
                $body['organizationId'] = $organizationId;
            }

            $body['orderIds'] = [$cart->iiko_id];

            $params = [
                'body' => json_encode($body),
                'headers' => [
                    'Authorization' => 'Bearer '.$this->getAccessToken(),
                ]
            ];

            $temp_resp = $this->request('POST', 'deliveries/by_id', $params);

            $body = $temp_resp->getBody()->getContents();

            LogIiko::log('checkOrder:success', $cart->id, '', $this->toArray($body));

            return $this->toArray($body);

        }catch (ClientException $e){

            if($e->getCode() === 401)
            {
                session()->forget('iiko_access_token');
                return $this->checkOrder($organizationId, $cart);
            }

            LogIiko::log('checkOrder|error', $cart->id, $e->getMessage(), $this->toArray($e->getResponse()->getBody()->getContents()));
        }
    }
}