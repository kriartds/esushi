<?php

namespace App\IikoApi\Models;

use Illuminate\Database\Eloquent\Model;

class LogIiko extends Model
{
    //
    protected $table = 'logs_iiko';

    protected $fillable = [
        'user_type',
        'user_id',
        'user_ip',
        'cart_id',
        'action',
        'text',
        'details',
    ];

    protected $casts = [
        'details' => 'object'
    ];

    public static function log($action, $cartid, $text, array $details=null)
    {
        //dd(auth()->guard());
        self::create([
            'user_type' => auth()->guard('web')->check() || auth()->guard()->guest() ? 'user' : 'admin',
            'auth_user_id' => auth()->check() ? auth()->user()->id : null,
            'cart_user_id' => request()->cookie('cart', null),
            'user_ip' => request()->ip(),
            'cart_id' => $cartid,
            'text' => $text,
            'details' => $details,
            'action' => $action
        ]);
    }
}
