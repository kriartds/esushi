<?php

namespace App\IikoApi\Models;

use Illuminate\Database\Eloquent\Model;

class IikoCities extends Model
{
    protected $fillable = [];
    protected $guarded = [];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
}
