<?php

namespace App\IikoApi\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Restaurants\Models\RestaurantsId;
use Modules\ShippingZones\Models\ShippingZones;

class IikoStreets extends Model
{
    protected $fillable = [];
    protected $guarded = [];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function getShippingZone()
    {
        return $this->hasOne(ShippingZones::class, 'id', 'shipping_zone_id');
    }

    public function restaurant(){
        return $this->hasOne(RestaurantsId::class, 'id', 'organization_id');
    }
}
