<?php

namespace App\IikoApi\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Iiko\Models\IikoGroups;
use Modules\Iiko\Models\IikoProductCategories;

class IikoProducts extends Model
{
    protected $table = 'iiko_products';

    protected $guarded = [];

    protected $casts = [
        'modifiers' => 'array',
        'group_modifiers' => 'array',
        'size_prices' => 'array',
    ];

    public function productCategory(){
        return $this->hasOne(IikoProductCategories::class, 'id', 'product_category_id');
    }

    public function group(){
        return $this->hasOne(IikoGroups::class, 'id', 'group_id');
    }
}
