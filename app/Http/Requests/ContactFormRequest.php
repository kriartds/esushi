<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;


class ContactFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'tema' => 'required',
            'message' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name:required' => 'Name is required',
            'phone:required' => 'Phone is required',
            'uploadedCv:*' => 'Wrong format file',
            'email:required' => 'Complete with corect email',
            'tema:required' => 'Tema is required',
            'message:required' => 'Message is required'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $data = [
            'status'    => false,
            'validator' => true,
            'msg'       => [
                'message' => __('front.erorr_validator'),
                'e'    => $validator->errors(),
                'type' => 'error',
            ],
        ];

        throw new HttpResponseException(response()->json($data, 200));
    }
}
