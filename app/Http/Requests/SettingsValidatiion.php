<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class SettingsValidatiion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cms_items_per_page' => 'nullable|numeric|min:1',
            'site_items_per_page' => 'nullable|numeric|min:1',
            'home_page' => 'required',
            'config_email_pass' => 'required_with:config_email|required_with:config_email_host|required_with:config_email_port',
            'config_email_host' => 'required_with:config_email|required_with:config_email_pass|required_with:config_email_port',
            'config_email_port' => 'required_with:config_email|required_with:config_email_host|required_with:config_email_pass',
            'config_email' => 'required_with:config_email_pass|required_with:config_email_host|required_with:config_email_port'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $data = [
            'status'    => false,
            'validator' => true,
            'msg'       => [
                'e'    => $validator->errors(),
                'type' => 'error',
            ],
        ];

        throw new HttpResponseException(response()->json($data, 200));
    }
}
