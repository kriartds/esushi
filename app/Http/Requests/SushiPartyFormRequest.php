<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;


class SushiPartyFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_user' => 'required',
            'email_user' => 'required',
            'time' => 'required',
            'number_employment' => 'required',
            'phone_user' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name_user:required' => 'Name is required',
            'time:required' => 'Phone is required',
            'number_employment:required' => 'Number of people is required',
            'phone_user:required' => 'Phone is required',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $data = [
            'status'    => false,
            'validator' => true,
            'msg'       => [
                'e'    => $validator->errors(),
                'type' => 'error',
            ],
        ];

        throw new HttpResponseException(response()->json($data, 200));
    }
}
