<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;


class ReservationFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'time' => 'required',
            'number_of_people' => 'required',
            'phone' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name:required' => 'Name is required',
            'time:required' => 'Phone is required',
            'number_of_people:required' => 'Number of people is required',
            'phone:required' => 'Phone is required',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $data = [
            'status'    => false,
            'validator' => true,
            'msg'       => [
                'e'    => $validator->errors(),
                'type' => 'error',
            ],
        ];

        throw new HttpResponseException(response()->json($data, 200));
    }
}
