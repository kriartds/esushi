<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;


class CareersCvRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'acceptTerms' => 'required',
            'uploadedCv' => 'mimes:pdf,doc,docx,odt,txt|nullable',
        ];
    }
    public function messages()
    {
        return [
            'name:required' => 'Name is required',
            'phone:required' => 'Phone is required',
            'uploadedCv:*' => 'Wrong format file'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $data = [
            'status'    => false,
            'validator' => true,
            'msg'       => [
                'message' => __('front.erorr_validator'),
                'e'    => $validator->errors(),
                'type' => 'error',
            ],
        ];

        throw new HttpResponseException(response()->json($data, 200));
    }
}
