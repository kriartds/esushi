<?php

namespace App\Http\Controllers;


use App\Sms;
use Illuminate\Http\Request;


class SmsController extends Controller
{

    public function index(){

        $t = preg_replace("/[^0-9]/", "", 'asdas +34234 )( ]');

        echo $t;
        exit;

        var_dump($user = auth()->user()->id);
        // return view("{$this->path}.itemPage", $response);
        $response = [];
        return view("sms", $response);
    }

    public function code(\Illuminate\Http\Request $request){

        $newcode = $request->request->get('newcode');

        $user = auth()->user();

        $smsLast = [];
        if(empty($newcode)){
            $smsLast = Sms::getLast([
                ['user_id', '=', $user->id],
                ['object_type_id', '=', Sms::OBJECT_TYPE_USER],
                ['object_id', '=', $user->id]
            ]);
        }
        $smsReturn = [];
        $smsCountInDay = 0;
        if(empty($smsLast))
        {
            $smsCountInDay = Sms::getCountInDay([
                ['user_id', '=', $user->id],
                ['object_type_id', '=', Sms::OBJECT_TYPE_USER],
                ['object_id', '=', $user->id]
            ]);

            if($smsCountInDay < env('SMS_COUNT_IN_DAY'))
            {
                $smsReturn = \App\Sms::newCode(
                    [
                        'object_type_id'=>\App\Sms::OBJECT_TYPE_USER,
                        'object_id'=> $user->id,
                        'user_id'=> $user->id,
                        'phone'=> $user->phone,
                    ],
                    [
                        'action'=>'userobjectSign',
                        'param'=>[
                            'user_id'=> auth()->user()->id,
                        ],
                    ]
                );
            }
        }

        /*
        $smsCountInDay = Sms::getCountInDay([
            'user_id'=>$smsModel->user_id,
            'object_type_id'=>Sms::OBJECT_TYPE_USER_OBJECT,
            'object_id'=>$smsModel->object_id
        ]);
        */

        // var_dump($smsReturn);

        $html = view("sms", [
            'model'=>$smsReturn,
            'smsLast'=>$smsLast,
            'smsCountInDay'=>$smsCountInDay,
            'newcode'=>$newcode,
        ]);


        return response()->json([
            'html' =>$html->render(),
            'confirm' =>false,
        ]);

    }
    public function check(\Illuminate\Http\Request $request){

        // $model = new UserObject();
        // $id = \Yii::$app->request->get('id');
        // $sms_id = \Yii::$app->request->get('sms_id');

        $modelSms = Sms::check($request);
        /// var_dump($modelSms);

        if ($modelSms) {
            return response()->json([
                'html' =>'<p>Спасибо! подтверждения прошла успешно</p>',
                'confirm' =>true,
            ]);
        }
        $user = auth()->user();

        $smsCountInDay = Sms::getCountInDay([
            ['user_id', '=', $user->id],
            ['object_type_id', '=', Sms::OBJECT_TYPE_USER],
            ['object_id', '=', $user->id]
        ]);

        $smsReturn = [];
        if($smsCountInDay < env('SMS_COUNT_IN_DAY'))
        {
            $smsReturn = \App\Sms::newCode(
                [
                    'object_type_id'=>\App\Sms::OBJECT_TYPE_USER,
                    'object_id'=> $user->id,
                    'user_id'=> $user->id,
                    'phone'=> $user->phone,
                ],
                [
                    'action'=>'userobjectSign',
                    'param'=>[
                        'user_id'=> auth()->user()->id,
                    ],
                ]
            );
        }

        $smsLast = [];

        $html = view("sms", [
            'model'=>$smsReturn,
            'smsLast'=>$smsLast,
            'smsCountInDay'=>$smsCountInDay,
            'newcode'=>1,
            'codeerror'=>1,
        ]);

        return response()->json([
            'html' =>$html->render(),
            'confirm' =>false,
        ]);



    }


}
