<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\ComponentsId;
use App\Models\Files;
use App\Models\FilesItems;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class FileController extends Controller
{

    public function uploadFile(Request $request)
    {

        $file = $request->file();

        if (empty($file) || !array_key_exists('file', $file))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => __("front.file_required"),
                    'type' => 'warning'
                ]
            ]);

        $file = $file['file'];

        if ($file->getClientSize() > 10485760) // 10 MB
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("front.image_max_size", ['size' => 10])],
                    'type' => 'warning'
                ]
            ]);

        $component = ComponentsId::where('active', 1)->find($request->get('componentId'));

        if (is_null($component)) {
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => __("front.something_went_wrong"),
                    'type' => 'warning'
                ]
            ]);
        }

        $extension = $file->getClientOriginalExtension();
        $originalFileName = $file->getClientOriginalName();
        $fileType = $file->getClientMimeType();

        $fileName = getCleanFileName($originalFileName) . '-_-' . time() . '.' . $extension;

        switch (strtolower($extension)) {
            case 'jpg':
            case 'jpeg':
            case 'png':
            {

                $destinationPath = 'files/' . date('Y-m-d') . '/images';

                break;
            }
            default :
            {
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => __("front.invalid_file_format"),
                        'type' => 'error'
                    ]
                ]);
                break;
            }
        }

        $file->move($destinationPath, $fileName);
        $originalPath = "{$destinationPath}/";

        $largePath = "{$destinationPath}/large/";
        $mediumPath = "{$destinationPath}/medium/";
        $smallPath = "{$destinationPath}/small/";

        File::makeDirectory($largePath, 0775, true, true);
        File::makeDirectory($mediumPath, 0775, true, true);
        File::makeDirectory($smallPath, 0775, true, true);

        Image::make($originalPath . $fileName)->resize(1200, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($largePath . $fileName);

        Image::make($originalPath . $fileName)->resize(500, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($mediumPath . $fileName);

        Image::make($originalPath . $fileName)->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($smallPath . $fileName);


        $item = Files::create([
            'file' => $originalPath . $fileName,
            'original_name' => $originalFileName,
            'active' => 1,
            'type' => substr($fileType, 0, strpos($fileType, '/')),
            'mime_type' => $fileType
        ]);

        $items[] = $item;
        $itemIds[] = $item->id;

        $itemFile = null;


        $fileItems = new FilesItems();
        $nextPosition = Helpers::getNextItemPosition($fileItems);

        $itemFile = $fileItems->create([
            'file_id' => $item->id,
            'component_id' => $component->id,
            'item_id' => null,
            'lang_id' => null,
            'active' => 1,
            'position' => $nextPosition

        ]);

        $fileLocations = getAllFileLocationsByUrl($item->fileLocation, 'magic');

        if (empty($fileLocations))
            return response()->json([
                'status' => false
            ]);

        $fileLocations = array_map(function ($val) {
            return asset($val);
        }, $fileLocations);

        $uploadedFileUrl = array_key_exists('small', $fileLocations) ? $fileLocations['small'] : $fileLocations['original'];

        return response()->json([
            'status' => true,
            'fileId' => $itemFile->id,
            'fileUrl' => $uploadedFileUrl,
        ]);

    }

    public function destroyFile(Request $request)
    {

        $fileItem = FilesItems::where('component_id', $request->get('componentId'))
            ->whereNull('item_id')
            ->find($request->get('fileId'));

        if (is_null($fileItem))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("front.file_not_exist")],
                    'type' => 'warning'
                ]
            ]);

        if (!is_null($fileItem->file)) {
            File::delete(getAllFileLocationsByUrl(@$fileItem->file->file));
            $fileItem->file->delete();
        }

        $fileItem->delete();

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => [__("front.file_was_deleted")],
                'type' => 'info'
            ]
        ]);
    }
}