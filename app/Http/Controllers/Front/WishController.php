<?php

namespace App\Http\Controllers\Front;

use App\Http\Helpers\Helpers;
use Modules\Products\Models\ProductsItemsId;
use App\Models\Wish;
use App\Models\WishProducts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class WishController extends DefaultController
{

    private $path;
    private $cartLifeTime;

    public function __construct()
    {
        $this->path = 'front.products.wish';
        $this->cartLifeTime = 20000; // 2 weeks
    }

    public function index(Request $request)
    {

        $userId = $request->cookie('wish');
        $wish = null;
        if (!is_null($userId) || auth()->check())
            $wish = Wish::where(function ($q) use ($userId) {
                if (!is_null($userId))
                    $q->where('user_id', $userId);

                if (auth()->check())
                    $q->where('auth_user_id', auth()->user()->id);

            })->with('wishProducts')->first();

        $items = collect();

        if (!is_null($wish)) {
            $perPage = Helpers::getSettingsField('site_items_per_page', parent::globalSettings());
            $items = $wish->products()
                ->where('active', 1)
                ->with(['categories', 'file.file', 'globalName', 'type', 'variationsDetails'])
                ->paginate($perPage);
        }

//        Meta
        $titlePage = __("front.wish-list");
//        Meta

        $response = [
            'path' => $this->path,
            'currComponent' => $this->currComponent(),
            'items' => $items,
            'titlePage' => $titlePage
        ];

        return view("{$this->path}.wishPage", $response);
    }

    public function store(Request $request)
    {

        $item = ProductsItemsId::where('active', 1)
            ->where('id', $request->get('itemId'))
            ->first();

        if (is_null($item))
            return response()->json([
                'status' => false
            ]);

        $userId = $request->cookie('wish');

        if (is_null($userId)) {
            $userId = bcrypt(time());

            if (auth()->check()) {
                $existWish = Wish::where('auth_user_id', auth()->user()->id)->first();

                if (!is_null($existWish))
                    $userId = $existWish->user_id;
            }

            Cookie::queue('wish', $userId, 20000);
        }

        $wish = Wish::where(function ($q) use ($userId) {
            if (!is_null($userId))
                $q->where('user_id', $userId);

            if (auth()->check())
                $q->where('auth_user_id', auth()->user()->id);
            else
                $q->whereNull('auth_user_id');

        })->with('wishProducts')->first();

        if (is_null($wish))
            $wish = Wish::create([
                'user_id' => $userId,
                'auth_user_id' => auth()->check() ? auth()->user()->id : null
            ]);
        elseif (auth()->check())
            $wish->update([
                'auth_user_id' => auth()->user()->id
            ]);


        $duplicate = WishProducts::where('wish_id', $wish->id)
            ->where('products_item_id', $item->id)
            ->first();

        if (is_null($duplicate))
            WishProducts::create([
                'wish_id' => $wish->id,
                'products_item_id' => $item->id
            ]);
        else {
            $duplicate->delete();

            $wishProductsCount = WishProducts::where('wish_id', $wish->id)->count();

            if ($wishProductsCount == 0) {
                Cookie::queue(Cookie::forget('wish'));
                $wish->delete();
            }
        }

        return response()->json([
            'count' => (!is_null($wish)) ? count($wish->products) : null,
            'status' => true
        ]);
    }

    public function destroy(Request $request)
    {
        $item = ProductsItemsId::where('active', 1)
            ->where('id', $request->get('itemId'))
            ->first();

        if (is_null($item))
            return response()->json([
                'status' => false
            ]);

        $userId = $request->cookie('wish');

        $wish = null;
        if (!is_null($userId) || auth()->check())
            $wish = Wish::where(function ($q) use ($userId) {
                if (!is_null($userId))
                    $q->where('user_id', $userId);

                if (auth()->check())
                    $q->where('auth_user_id', auth()->user()->id);

            })->with('wishProducts')->first();

        $items = collect();

        if (!is_null($wish)) {
            $perPage = Helpers::getSettingsField('site_items_per_page', parent::globalSettings());

            if ($wish->wishProducts->count() > 1) {
                $wish->wishProducts()->where('products_item_id', $item->id)->delete();

                $items = $wish->products()
                    ->where('active', 1)
                    ->with(['categories', 'file.file', 'globalName', 'type', 'variationsDetails'])
                    ->paginate($perPage);

                $items->setPath(url(LANG, 'wish-list'));
            } else {
                $wish->wishProducts()->where('products_item_id', $item->id)->delete();
                $wish->delete();
            }
        }

        try {
            return response()->json([
                'status' => true,
                'count' => (!is_null($wish)) ? count($wish->products) : null,
                'view' => view('front.components.catalog.itemsList', ['products' => $items, 'isWishListPage' => true])->render()
            ]);
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false
            ]);
        }
    }
}
