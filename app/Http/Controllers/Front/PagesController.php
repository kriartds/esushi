<?php

namespace App\Http\Controllers\Front;


use App\Http\Requests\CareersCvRequest;
use App\Http\Requests\ContactFormRequest;
use App\Http\Requests\ReservationFormRequest;
use App\Http\Requests\SushiPartyFormRequest;
use Illuminate\Support\Facades\Mail;
use Modules\Pages\Models\PagesId;
use Modules\Pages\Models\SushiPartyBlockId;
use Modules\Reservation\Models\Reservation;
use Modules\Restaurants\Models\RestaurantsId;
use Modules\Products\Models\ProductsPromotionsId;
use Modules\Reviews\Models\Reviews;
use Modules\SushiPartyOrder\Models\SushiPartyOrder;
use Modules\Vacancies\Models\VacanciesId;
use Illuminate\Http\Request;

class PagesController extends DefaultController
{
    private $path;

    public $data = [];

    public function __construct()
    {
        $this->path = 'front.pages';
    }

    public function index($itemSlug)
    {
        $item = PagesId::where('active', 1)
            ->where('slug', $itemSlug)
            ->firstOrFail();

        if ($item->template == 'sushi-party') {
            $sushiParty = SushiPartyBlockId::with('globalName')->where('active', 1)->get();
            $this->data['sushiParty'] = $sushiParty;
        }

        if ($item->template == 'Careers') {
            $ourVacancies = VacanciesId::with('globalName')->where('active', 1)->get();
            $this->data['ourVacancies'] = $ourVacancies;
        }

        if ($item->template == 'restaurants') {
            $ourRestaurants = RestaurantsId::with('globalName')->where('active', 1)->get();
            $this->data['ourRestaurants'] = $ourRestaurants;
        }

        if ($item->template == 'promotions') {
            $ourPromotions = ProductsPromotionsId::availablePromotions()
                ->whereHas('products', function ($q) {
                    $q->where('active', 1);
                })
                ->with(['globalName', 'file.file'])
                ->get();

            $this->data['ourPromotions'] = $ourPromotions;
        }

        $view = "{$this->path}." . (is_null($item->template) ? "default" : "templates." . findFileInPath("{$this->path}.templates", $item->template));
        $this->data['item'] = $item;

        $response = array_merge($this->data, getItemMeta($item, 'globalName', $this->globalSettings()));

        return view($view, $response);
    }

    public function getParty($lang, $slug)
    {
        $sushiParty = SushiPartyBlockId::with('globalName', 'SushiPartyBlockProducts', 'SushiPartyBlockProducts.globalName')
            ->where('active', 1)
            ->where('slug', $slug)
            ->firstOrFail();

        $page = PagesId::where('template', 'sushi-party')->first();

        $this->data['item'] = $page;
        $this->data['sushiParty'] = $sushiParty;
        $response = array_merge($this->data, getItemMeta($sushiParty, 'globalName', $this->globalSettings()));
        return view($this->path . '.templates.party', $response);
    }

    public function getRestaurant($lang, $slug)
    {
        $restaurant = RestaurantsId::with('globalName')
            ->where('active', 1)
            ->where('slug', $slug)
            ->firstOrFail();

        //$page = PagesId::where('template', 'sushi-party')->first();
        $this->data['item'] = $restaurant;
        $this->data['restaurant'] = $restaurant;
        $response = array_merge($this->data, getItemMeta($restaurant, 'globalName', $this->globalSettings()));

        return view($this->path . '.templates.restaurant', $response);
    }

    public function getPromotion($lang, $slug){

        $item = ProductsPromotionsId::availablePromotions()
            ->whereHas('products', function ($q) {
                $q->where('active', 1);
            })
            ->where('slug', $slug)
            ->firstOrFail();

        $promotionProducts = $item->products()
            ->with(['file.file', 'globalName', 'variationsDetails'])->get();

        $response = [
            'path' => $this->path,
            'currComponent' => parent::currComponent(),
            'item' => $item,
            'promotionProducts' => $promotionProducts
        ];

        $response = array_merge($response, getItemMeta($item, 'globalName', $this->globalSettings()));

        return view($this->path . '.templates.promotion', $response);
    }

    public function sendCvForm(CareersCvRequest $request)
    {

        Mail::send('front.emails.TemplateCvForm', ['userInfo' => $request->all()], function ($message) use ($request) {
            $message->from(config('mail.from.address'));
            $message->to(getSettingsField('email', parent::globalSettings()));
            $message->subject(__('front.upload_cv_subject'));
            if ($request->file('uploadedCv')) {
                $message->attach($request->file('uploadedCv')->getRealPath(), [
                    'as' => $request->file('uploadedCv')->getClientOriginalName(),
                    'mime' => $request->file('uploadedCv')->getMimeType()
                ]);
            }
        });

        return response()->json([
            'status' => true,
            'msg' => [
                'message' => __('front.success_validator'),
                'type' => 'success'
            ]
        ]);
    }

    public function sendContactForm(ContactFormRequest $request)
    {
        $reviews = new Reviews();
        $reviews->name = $request->input('name');
        $reviews->phone = $request->input('phone');
        $reviews->email = $request->input('email');
        $reviews->tema = $request->input('tema');
        $reviews->message = $request->input('message');
        $reviews->save();
        try {
            Mail::send('front.emails.TemplateContactForm', ['userInfo' => $request->toArray()], function ($message) use ($request) {
                $message->from(config('mail.from.address'));
                $message->to(getSettingsField('email', parent::globalSettings()));
                $message->subject(__('front.contact_form_subject'));
            });
        } catch (\Exception $e) {

        }
        return response()->json([
            'status' => true,
            'msg' => [
                'message' => __('front.success_validator'),
                'type' => 'success'
            ]
        ]);
    }

    public function sendReservationForm(ReservationFormRequest $request)
    {
        $reservation = new Reservation();
        $reservation->name = $request->input('name');
        $reservation->phone = $request->input('phone');
        $reservation->time = carbon($request->input('time'));
        $reservation->number_of_people = $request->input('number_of_people');
        $reservation->restaurant = $request->input('restaurant');
        $reservation->status = 'unconfirmed';
        $reservation->save();

        try {
            Mail::send('front.emails.TemplateReservationForm', ['userInfo' => $reservation], function ($message) use ($request) {
                $message->from(config('mail.from.address'));
                $message->to(getSettingsField('order_email', parent::globalSettings()));
                $message->subject(__('front.reservation_form_subject'));
            });
        } catch (\Exception $e) {

        }
        return response()->json([
            'status' => true,
            'msg' => [
                'message' => __('front.success_validator'),
                'type' => 'success'
            ]
        ]);
    }

    public function sendSushiPartyForm(SushiPartyFormRequest $request)
    {
        $party = new SushiPartyOrder();
        $party->name = $request->input('name_user');
        $party->email = $request->input('email_user');
        $party->phone = $request->input('phone_user');
        $party->time = carbon($request->input('time'));
        $party->number_of_people = $request->input('number_employment');
        $party->regions = $request->input('regions');
        $party->party = $request->input('party');
        if ($request->input('productIds') ){
            $party->products = $request->input('productIds');
        }
        $party->status = 'unconfirmed';
        $party->save();

        try {
            Mail::send('front.emails.TemplateSushiPartyForm', ['userInfo' => $party], function ($message) use ($request) {
                $message->from(config('mail.from.address'));
                $message->to(getSettingsField('email', parent::globalSettings()));
                $message->subject(__('front.sushi_party_form_subject'));
            });
        } catch (\Exception $e) {

        }
        return response()->json([
            'status' => true,
            'msg' => [
                'message' => __('front.success_validator'),
                'type' => 'success'
            ]
        ]);
    }
}
