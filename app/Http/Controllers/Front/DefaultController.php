<?php

namespace App\Http\Controllers\Front;


use App\Http\Helpers\Helpers;

class DefaultController extends RoleManager
{

    public function currComponent()
    {
        return Helpers::getComponentIdBySlug(request()->segment(2, null), true);
    }

    public function globalSettings()
    {
        return view()->getShared()['allSettings'];
    }

    public function currCurrency()
    {
        return getCurrentCurrency(@view()->getShared()['cookieCurrencies']);
    }

    public function currLocation()
    {
        $currLocation = @view()->getShared()['currLocation'];

        if (!$currLocation)
            $currLocation = @getLocation();

        return $currLocation;
    }

}
