<?php

namespace App\Http\Controllers\Front;

use App\Http\Helpers\Helpers;
use Illuminate\Http\Request;
use Modules\Blog\Models\BlogCategoriesId;
use Modules\Blog\Models\BlogItemsId;
use Modules\Products\Models\ProductsPromotionsId;

class ProductsPromotionsController extends DefaultController
{
    private $path;

    public function __construct()
    {
        $this->path = 'front.promotions';
    }

    public function index()
    {
        $perPage = Helpers::getSettingsField('site_items_per_page', parent::globalSettings());

        $items = ProductsPromotionsId::availablePromotions()
            ->whereHas('products', function ($q) {
                $q->where('active', 1);
            })
            ->with(['globalName', 'file.file'])
            ->paginate($perPage);

//        Meta
        $titlePage = __("front.promotions");
//        Meta

        $response = [
            'path' => $this->path,
            'currComponent' => $this->currComponent(),
            'items' => $items,
            'titlePage' => $titlePage,
        ];

        return view("{$this->path}.promotionsList", $response);
    }

    public function itemPage(Request $request, $itemSlug)
    {
        $perPage = Helpers::getSettingsField('site_items_per_page', parent::globalSettings());

        $item = ProductsPromotionsId::availablePromotions()
            ->whereHas('products', function ($q) {
                $q->where('active', 1);
            })
            ->where('slug', $itemSlug)
            ->firstOrFail();

        $promotionProducts = $item->products()
            ->with(['file.file', 'globalName', 'variationsDetails', 'reviews'])
            ->paginate($perPage);

        $response = [
            'path' => $this->path,
            'currComponent' => parent::currComponent(),
            'item' => $item,
            'promotionProducts' => $promotionProducts
        ];

        $response = array_merge($response, getItemMeta($item, 'globalName', $this->globalSettings()));

        return view("{$this->path}.promotionPage", $response);
    }
}
