<?php

namespace App\Http\Controllers\Front;

use App\Http\Helpers\Helpers;

use Modules\Products\Models\ProductsCategoriesId;
use Modules\Products\Repositories\ProductsCategoriesIdRepository;
use Illuminate\Http\Request;


class ProductsCategoriesController extends DefaultController
{
    private $path;
    private $productsCategoriesIdRepo;

    public function __construct()
    {
        $this->path = 'front.products';
        $this->productsCategoriesIdRepo = new ProductsCategoriesIdRepository();
        $this->productsInstancesWiths = ['file.file', 'products', 'globalName', 'products.globalName'];
    }

    public function index()
    {
        $titlePage = __('front.products-categories');

        $categories = ProductsCategoriesId::where('active', 1)
            ->with([
                'file.file',
                'products' => function($q){
                    $q->where('active', 1);
                },
                'globalName',
                'products.globalName'
            ])
            ->get();

        $currComponent = $this->currComponent();

        $response['path'] = $this->path;
        $response['titlePage'] = $titlePage;
        $response['sectionCategories'] = $categories;
        $response['currComponent'] = $currComponent;

        return view("{$this->path}.categoriesList", $response);
    }

    public function filter(Request $request, $category = null)
    {
        $perPage = Helpers::getSettingsField('site_items_per_page', $this->globalSettings());
        $currComponent = $this->currComponent();
        $currCurrency = $this->currCurrency();

        $currCategory = $category;

        if ($request->ajax())
            $currCategory = $this->productsCategoriesIdRepo->find($request->categoryId, ['active' => 1], ['products', 'products.productAttributes', 'products.type', 'products.productAttributes', 'products.productAttributes.globalName', 'products.productAttributes.type', 'products.productAttributes.children', 'products.productAttributes.globalName']);

        $products = filterProducts($request, $currComponent, $perPage, $currCurrency, $currCategory, ['page', 'categoryId']);

        if (is_array($products) && $products['status'])
            $products['items']->setPath(url(LANG, [$currComponent->slug, (!is_null($currCategory) ? $currCategory->slug : '')]) . $products['pushUrl']);

        $response = [
            'status' => true,
            'pushUrl' => $products['pushUrl'],
            'products' => $products['items'],
            'filter' => $products['filter'],
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("front.components.catalog.itemsList", $response)->render();
            } catch (\Throwable $e) {
                abort(503);
            }

            return $response;
        }

        $response = array_merge($response, [
            'currComponent' => $currComponent,
            'attributes' => $products['attributes'],
            'filteredProductsPricesRange' => $products['filteredProductsPricesRange']
        ]);

        return $response;
    }

    public function itemPage(Request $request, $itemSlug)
    {
        $item = ProductsCategoriesId::where('active', 1)
            ->where('slug', $itemSlug)
            ->with('parent')
            ->first();

        $categories = ProductsCategoriesId::where('active', 1)
            ->where('slug', $itemSlug)
            ->whereHas('children')
            ->with('children', 'globalName')
            ->first();

        if (!is_null($categories)) {
            abort(404);
        } else {
            $response = $this->filter($request, $item);
            $response['item'] = $item;
            $response = array_merge($response, getItemMeta($item, 'globalName', $this->globalSettings()));
        }
        $response['path'] = $this->path;

        return view("{$this->path}.categoriesList", $response);
    }
}
