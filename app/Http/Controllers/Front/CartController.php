<?php

namespace App\Http\Controllers\Front;

use App\Http\Helpers\Helpers;
use App\IikoApi\IikoClient;
use App\IikoApi\Models\IikoStreets;
use App\Models\LogsPayment;
use App\Models\Wish;
//use Modules\CountriesRegion\Models\Countries;
use Modules\Orders\Http\Helpers\Helpers as OrdersHelpers;
use Modules\Products\Models\ProductsToppingsId;
//use Modules\ShippingZones\Http\Helpers\Helpers as ShippingHelpers;
use Modules\Coupons\Http\Helpers\Helpers as CouponsHelpers;
use Modules\Orders\Models\CartProducts;
//use Modules\CountriesRegion\Models\Regions;
use Modules\Products\Models\ProductsItemsId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Modules\Orders\Models\Cart;
use Modules\Products\Models\ProductsItemsVariationsDetailsId;
use Modules\Restaurants\Models\RestaurantsId;
use App\MaibApi\MaibClient;
use Modules\ShippingZones\Models\ShippingZones;

class CartController extends DefaultController
{

    private $path;
    private $cartLifeTime;
    private $_apiContext;

    public function __construct()
    {
        $this->path = 'front.cart';
//        $this->cartLifeTime = 20000; // 2 weeks
//        $paypal_conf = config()->get('paypal');
//        $this->_apiContext = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
//        $this->_apiContext->setConfig($paypal_conf['settings']); /* Don't remove this line */
    }

    public function index($lang_code, $common_id=null)
    {
        if(!empty($common_id))
        {
            //check if common cart id is assigned for this user
            session(['common_cart_id' => $common_id]);
        }

        $authUser = auth()->user();
        $currCurrency = $this->currCurrency();

        //$cart = Cart::where('user_id', $this->userId())->where('is_draft', 0)->first();
        $cart = Cart::personalOrCommon()->where('is_draft', 0)->first();

        $cartDiscount = @$cart->discount;
        $allCartProducts = OrdersHelpers::getCartProducts($cart, -1, true, true);

        $cartCount = @$allCartProducts->count ?: 0;
        $cartSubtotal = @$allCartProducts->total ?: 0;
        $cartProducts = @$allCartProducts->products ?: collect();
        $cartOutOfStockProducts = @$allCartProducts->outOfStockProducts ?: collect();
        $cartDiscountAmount = 0;

        $validateCouponErrorMsg = '';
        $validateCoupon = null;

        if ($cartDiscount) {
            $validateCoupon = CouponsHelpers::validateCoupon(@$cartDiscount->code, $currCurrency, $cartSubtotal, false);

            if (is_array($validateCoupon))
                $validateCouponErrorMsg = $validateCoupon;
            else {
                if (!$validateCoupon->allow_free_shipping) {
                    if ($validateCoupon->type == 'fixed')
                        $cartDiscountAmount = $validateCoupon->amount;
                    else
                        $cartDiscountAmount = $cartSubtotal * $validateCoupon->amount / 100;
                }
            }

        }

        $shippingZones = ShippingZones::where('active', 1)->orderBy('position')->get();
        $shippingAmount = 0;
        $defaultShipping = null;
        foreach ($shippingZones as $zone){
            if($zone->is_default) {
                $defaultShipping = $zone;
                if ($cartSubtotal < $zone->free_shipping_amount) {
                    $shippingAmount = $zone->shipping_amount;
                    break;
                }
            }
        }

        $shippingAdresses = collect();
        if(!is_null($defaultShipping))
        {
            $shippingAdresses = IikoStreets::where('shipping_zone_id', $defaultShipping->id)->get();
        }

        $cartSubtotalWithCoupon = $cartSubtotal - $cartDiscountAmount;
        $cartSubtotalWithCoupon = $cartSubtotalWithCoupon >= 0 ? $cartSubtotalWithCoupon : 0;
        $cartTotal = $cartSubtotalWithCoupon + $shippingAmount;
        $titlePage = __('front.cart');

        $restaurants = RestaurantsId::where('active', 1)->whereNotNull('iiko_id')->with('globalName')->get();

        $allowToOrder = helpers()->restaurantsIsOpen();

        $response = [
            'path' => $this->path,
            'currComponent' => $this->currComponent(),
            'products' => $cartProducts,
            'outOfStockProducts' => $cartOutOfStockProducts,
            'cartCount' => $cartCount,
            'cartTotal' => $cartTotal,
            'cartSubtotal' => $cartSubtotal,
            'cartSubtotalWithCoupon' => $cartSubtotalWithCoupon,
            'cartDiscount' => $cartDiscount,
            'shippingZones' => $shippingZones,
            'validateCouponErrorMsg' => $validateCouponErrorMsg,
            'authUser' => $authUser,
            'titlePage' => $titlePage,
            'currentCart' => $cart,
            'isAvailableCommonCart' => isAvailableCommonCart($cart),
            'restaurants' => $restaurants,
            'shippingAmount' => $shippingAmount,
            'defaultShipping' => $defaultShipping,
            'shippingAdresses' => $shippingAdresses,
            'allowToOrder' => $allowToOrder
        ];

        return view("{$this->path}.cart", $response);
    }

    public function store(Request $request)
    {
        $authUser = auth()->check() ? auth()->user() : null;
        $buyInOneClick = $request->get('action', null) == 'buyInOneClick';

        if ($buyInOneClick) {
            $rules = [
                'client_phone' => 'required|regex:/^((\+)(\d+\s?)?(\(\d+\)\s?)?)?(\d+\s?\-?)+(\d)?$/|max:20'
            ];
            $messages = [
                'client_phone.required' => __("front.phone_required"),
                'client_phone.regex' => __("front.phone_format_invalid"),
                'client_phone.max' => __("front.phone_max_chars", ['number' => 20]),
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'validator' => true,
                    'msg' => [
                        'e' => $validator->messages(),
                        'type' => 'error'
                    ]
                ]);
            }
        }

        $item = ProductsItemsId::where('active', 1)
            ->find($request->get('item_id'));

        if (is_null($item))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => __("front.product_not_exist"),
                    'type' => 'warning'
                ]
            ]);

        $countItems = (int)$request->get('count-items') == 0 ? 1 : (int)$request->get('count-items');

        $toppings_amount = 0;
        if(isset($request->toppings) && !empty($request->toppings)){
            $selected_toppings = ProductsToppingsId::whereIn('id', $request->toppings)->get();
            $toppings_amount = $selected_toppings->sum('price');
        }

        $clearStoreCartData = [
            'user_id' => null,
            'auth_user_id' => @$authUser->id,
            'user_ip' => $request->ip(),
            'payment_method' => null,
            'online_payment_name' => null,
            'shipping_method' => null,
            'amount' => null,
            'shipping_amount' => null,
            'discount_amount' => null,
            'count_items' => null,
            'email' => @$authUser->email,
            'phone' => $buyInOneClick ? $request->get('client_phone', null) : @$authUser->phone,
            'country_id' => @$authUser->country_id,
            'region_id' => @$authUser->region_id,
            'details' => null,
            'shipping_details' => null,
            'pickup_details' => null,
            'discount_details' => null,
            'seen' => 0,
            'status' => $buyInOneClick ? 'checkout' : null,
            'bank_id_transaction' => null,
            'order_id' => null,
            'order_date' => $buyInOneClick ? now() : null,
            'is_fast_buy' => $buyInOneClick ? 1 : 0,
            'order_lang' => LANG,
            'is_draft' => $buyInOneClick ? 1 : 0,
        ];

        $cartAttributes = $cartAttributesIds = $attributesIds = [];
        $variationsDetails = null;

        $attributesIds = array_filter(array_map(function ($key, $value) {
            if (strpos($key, 'attributes') !== false)
                return $value;
        }, array_keys($request->all()), $request->all()));

        if ($item->isVariationProduct)
        {
            $variationId = $request->get('variation_id', null);
            if(!is_null($variationId))
                $variationsDetails = ProductsItemsVariationsDetailsId::where('products_item_id', $item->id)->find($variationId);
            else
                $variationsDetails = ProductsItemsVariationsDetailsId::where('products_item_id', $item->id)->where('is_default', 1)->first();

            if (is_null($variationsDetails))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => __("front.variation_not_exist"),
                        'type' => 'warning'
                    ]
                ]);

            $finallyProductQtyStock = $variationsDetails->finStock;

            if (!$finallyProductQtyStock || (is_numeric($finallyProductQtyStock) && $countItems > $finallyProductQtyStock))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => is_numeric($finallyProductQtyStock) && $countItems > $finallyProductQtyStock ? __("front.select_qty_lower_than", ['qty' => $finallyProductQtyStock]) : __("front.product_out_of_stock"),
                        'type' => 'warning'
                    ]
                ]);

            $price = $variationsDetails->sale_price ? $variationsDetails->sale_price : $variationsDetails->price;

            $variationAttributesOptions = OrdersHelpers::formatAttributesForStoreCartProduct($item, $variationsDetails, $attributesIds);

            $cartAttributes = array_filter($variationAttributesOptions->cart_attributes);

            $cartAttributesIds = array_filter($variationAttributesOptions->cart_attributes_ids);


        } else {
            $finallyProductQtyStock = $item->finStock;

            if (!$finallyProductQtyStock || (is_numeric($finallyProductQtyStock) && $countItems > $finallyProductQtyStock))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => is_numeric($finallyProductQtyStock) && $countItems > $finallyProductQtyStock ? __("front.select_qty_lower_than", ['qty' => $finallyProductQtyStock]) : __("front.product_out_of_stock"),
                        'type' => 'warning'
                    ]
                ]);

            $price = $item->sale_price ? $item->sale_price : $item->price;
        }

        if (is_null($price))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => __("front.product_dont_have_price"),
                    'type' => 'warning'
                ]
            ]);

        $userId = $this->userId();

        if (is_null($userId) || $buyInOneClick)
            $userId = bcrypt(time());

        $cart = null;

        if (!$buyInOneClick) {
            Cookie::queue('cart', $userId, $this->cartLifeTime);

            $cart = Cart::isInProgress()->personalOrCommon()
                //->where('user_id', $userId)
                //->where(function ($q){
                //      if($common_id=session('common_cart_id'))
                //        $q->where('common_id', $common_id);
                //      else
                //        $q->where('user_id', $this->userId());
                //  })
                  ->first();
        }

        if (is_null($cart))
            $cart = Cart::create(array_merge($clearStoreCartData, [
                'user_id' => $userId,
                'order_currency' => $buyInOneClick ? $this->currCurrency() : null,
            ]));

        $cartProduct = CartProducts::where('cart_id', $cart->id)
            ->where('products_item_id', $item->id)
            ->where('products_variation_item_id', @$variationsDetails->id)
            ->where('attributes_ids', json_encode($cartAttributesIds))
            //->where('user_id', $userId)
            ->first();

        if (is_null($cartProduct)) {
            $cartProduct = CartProducts::create([
                'cart_id' => $cart->id,
                'products_item_id' => $item->id,
                'products_variation_item_id' => @$variationsDetails->id,
                'quantity' => $countItems,
                'amount' => $price + $toppings_amount,
                'info' => null,
                'attributes' => $cartAttributes,
                'attributes_ids' => $cartAttributesIds,
                'toppings_ids' => isset($selected_toppings) && $selected_toppings->isNotEmpty() ? $selected_toppings->pluck('id')->toArray() : null,
                //'user_id' => $userId,
            ]);

        } else {
            $totalQty = $cartProduct->quantity + $countItems;

            if ($totalQty > $finallyProductQtyStock)
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => __("front.selected_max_qty", ['qty' => $finallyProductQtyStock]),
                        'type' => 'warning'
                    ]
                ]);

            $cartProduct->update([
                'quantity' => $totalQty,
                'amount' => $price + $toppings_amount,
            ]);
        }

        if (!$buyInOneClick) {
            $allCartProducts = OrdersHelpers::getCartProducts($cart);

            $cartCount = @$allCartProducts->count ?: 0;
            $cartTotal = @$allCartProducts->total ?: 0;
            $cartThumbnail = '';

            if (config('cms.cart.displayProductsHeader', true)) {
                $cartProducts = @$allCartProducts->products ?: collect();
                $remainCartCount = $cartCount && $cartCount > $cartProducts->count() ? $cartCount - $cartProducts->count() : 0;

                try {
                    $cartThumbnail = view('front.components.header.headerCartThumbnail', ['cartProducts' => $cartProducts, 'remainCartCount' => $remainCartCount, 'cartSubtotal' => ($cartTotal ? $cartTotal : '')])->render();
                } catch (\Throwable $e) {
                }
            }

            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => __("front.item_added_to_cart"),
                    'type' => 'info'
                ],
                'count' => $cartCount,
                'cartTotalFormatted' => formatPrice($cartTotal, getCurrentCurrency()),
                'thumbnail' => $cartThumbnail,
                'cartProductId' => $cartProduct->id,
            ]);
        } else {
            $cart->update([
                'order_id' => OrdersHelpers::orderIdGenerator($cart->id, $cart->order_id)
            ]);

            OrdersHelpers::updateProductsPriceOnCheckout($cart);
            OrdersHelpers::sendNotificationToAdminPanel($cart);

            return response()->json([
                'status' => true,
                'clearForm' => true,
                'msg' => [
                    'e' => __("front.seller_will_contact"),
                    'type' => 'info'
                ]
            ]);
        }

    }

    public function repeatOrder(Request $request)
    {
        $cart = Cart::where('order_id', $request->input('orderId'))
            ->first();

        $allCartProducts = OrdersHelpers::getCartProducts($cart, -1, false, false);
        $cartProducts = @$allCartProducts->products ?: collect();

        $productsToCart = [];
        foreach ($cartProducts as $product) {
            $productsToCart[] = [
                'item_id' => $product->products_item_id,
                'variation_id' => $product->products_variation_item_id,
                'toppings' => $product->toppings_ids,
                'attributes-number-of-pieces' => $product->attributes_ids ? current($product->attributes_ids) : null,
                "action" => "addToCart"
            ];
        }


        $success = false;
        foreach ($productsToCart as $productToCart) {
            try {
                $newRequest = new Request();
                $newRequest->merge($productToCart);
                $this->store($newRequest);

                $success = true;

            } catch (\Exception $e) {

            }
        }

        if ($success) {
            return response()->json([
                'status' => true,
                'redirect' => customUrl('cart')
            ]);
        } else {
            return response()->json([
                'status' => false,
                'msg' => [
                    'message' => 'Produsul adaugat nu mai este disponibil',
                    'type' => 'error',
                ]
            ]);
        }


    }

    public function update(Request $request)
    {

        $itemQty = $request->get('quantity', 1);
        $currCurrency = $this->currCurrency();

        $messages = [
            'quantity.required' => __("front.cart_qty_required"),
            'quantity.numeric' => __("front.cart_qty_numeric"),
            'quantity.min' => __("front.cart_qty_min", ['min' => 1]),
        ];

        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric|min:1'
        ], $messages);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->messages(),
                    'type' => 'error'
                ],
            ]);
        }

        $cartProduct = CartProducts::where('id', $request->get('itemId', null))
            ->whereHas('cart', function ($q) {
                //$q->where('user_id', $this->userId());
                $q->personalOrCommon();
            })
            ->with('cart')
            ->first();

        if (!$cartProduct)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => __("front.product_not_exist"),
                    'type' => 'error'
                ]
            ]);

        $product = @$cartProduct->product;

        if ($product) {
            if ($product->isVariationProduct && !$cartProduct->variation)
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => __("front.variation_not_exist"),
                        'type' => 'error'
                    ]
                ]);

            $productStock = @$product->isVariationProduct ? @$cartProduct->variation->finStock : @$product->finStock;

            $responseStockMsg = null;

            if (is_numeric($productStock) && $productStock < $itemQty)
                $responseStockMsg = __("front.product_qty_must_be_between", ['min' => 1, 'qty' => $productStock]);
            elseif (!is_numeric($productStock) && !$productStock)
                $responseStockMsg = __("front.product_out_of_stock");

            if ($responseStockMsg)
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => $responseStockMsg,
                        'type' => 'warning'
                    ]
                ]);
        }

        $cartProduct->update([
            'quantity' => $itemQty
        ]);

        $cart = @$cartProduct->cart;
        $cartDiscount = @$cart->discount;

        $allCartProducts = OrdersHelpers::getCartProducts($cart);

        $cartCount = @$allCartProducts->count ?: 0;
        $cartSubtotal = @$allCartProducts->total ?: 0;
        $cartProducts = @$allCartProducts->products ?: collect();
        $cartProductPrice = @$cartProduct->finPrice->minPrice ? @$cartProduct->finPrice->minPrice : @$cartProduct->finPrice->maxPrice;

        $cartDiscountAmount = 0;

        $validateCouponErrorMsg = '';
        $validateCoupon = null;

        if ($cartDiscount) {
            $validateCoupon = CouponsHelpers::validateCoupon(@$cartDiscount->code, $currCurrency, $cartSubtotal, false);

            if (is_array($validateCoupon))
                $validateCouponErrorMsg = $validateCoupon;
            else {
                if (!$validateCoupon->allow_free_shipping) {
                    if ($validateCoupon->type == 'fixed')
                        $cartDiscountAmount = $validateCoupon->amount;
                    else
                        $cartDiscountAmount = $cartSubtotal * $validateCoupon->amount / 100;
                }
            }

        }

        //$shippingMethod = ShippingHelpers::getShippingMethodById($request->get('shippingMethodId', null), $request->get('shippingZoneId', null), $validateCoupon);
        $shippingAmount = 0;//ShippingHelpers::getCurrentShippingMethodAmount($shippingMethod, true);

        $cartSubtotalWithCoupon = $cartSubtotal - $cartDiscountAmount;
        $cartSubtotalWithCoupon = $cartSubtotalWithCoupon >= 0 ? $cartSubtotalWithCoupon : 0;
        $cartTotal = $cartSubtotalWithCoupon + $shippingAmount;

        try {
            $cartThumbnail = view('front.components.header.headerCartThumbnail', ['cartProducts' => $cartProducts, 'cartSubtotal' => $cartSubtotal])->render();
        } catch (\Throwable $e) {
            $cartThumbnail = '';
        }

        $currCurrency = $this->currCurrency();

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => [__("front.qty_was_updated")],
                'type' => 'info'
            ],
            'cartCount' => $cartCount,
            'cartSubtotal' => $cartSubtotal,
            'cartTotal' => $cartTotal,
            'cartSubtotalStr' => formatPrice($cartSubtotal, $currCurrency),
            'cartTotalStr' => formatPrice($cartTotal, $currCurrency),
            'cartSubtotalWithCoupon' => $cartSubtotalWithCoupon,
            'cartSubtotalWithCouponStr' => formatPrice($cartSubtotalWithCoupon, $currCurrency),
            'productQty' => $cartProduct->quantity,
            'productQtyPriceStr' => formatPrice(((is_numeric($cartProductPrice) ? $cartProductPrice : 0) * $cartProduct->quantity), $currCurrency),
            'thumbnail' => $cartThumbnail,
            'couponMsg' => $validateCouponErrorMsg
        ]);

    }

    public function destroy(Request $request)
    {

        $currCurrency = $this->currCurrency();

        $cartProduct = CartProducts::whereHas('cart', function ($q) {
            //$q->where('user_id', $this->userId());
            $q->personalOrCommon();
        })->find($request->get('itemId', null));

        if (is_null($cartProduct))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => __("front.cart_product_not_exist"),
                    'type' => 'warning'
                ]
            ]);

        $cart = @$cartProduct->cart;
        $cartDiscount = @$cart->discount;

        $cartProduct->delete();

        $allCartProducts = OrdersHelpers::getCartProducts($cart);

        $cartCountProducts = @$allCartProducts->cartCountProducts ?: 0;
        $cartCount = @$allCartProducts->count ?: 0;
        $cartSubtotal = @$allCartProducts->total ?: 0;
        $cartProducts = @$allCartProducts->products ?: collect();
        $cartProductPrice = @$cartProduct->finPrice->minPrice ? @$cartProduct->finPrice->minPrice : @$cartProduct->finPrice->maxPrice;

        try {
            $cartThumbnail = view('front.components.header.headerCartThumbnail', ['cartProducts' => $cartProducts, 'cartSubtotal' => $cartSubtotal])->render();
        } catch (\Throwable $e) {
            $cartThumbnail = '';
        }

        $destroyCart = false;

        if ($cartCountProducts == 0) {
            Cart::where('user_id', $this->userId())->where('is_draft', 0)->forceDelete();
            Cookie::queue(Cookie::forget('cart'));
            $destroyCart = true;
        }

        $cartDiscountAmount = 0;

        $validateCouponErrorMsg = '';
        $validateCoupon = null;

        if ($cartDiscount) {
            $validateCoupon = CouponsHelpers::validateCoupon(@$cartDiscount->code, $currCurrency, $cartSubtotal, false);

            if (is_array($validateCoupon))
                $validateCouponErrorMsg = $validateCoupon;
            else {
                if (!$validateCoupon->allow_free_shipping) {
                    if ($validateCoupon->type == 'fixed')
                        $cartDiscountAmount = $validateCoupon->amount;
                    else
                        $cartDiscountAmount = $cartSubtotal * $validateCoupon->amount / 100;
                }
            }

        }

        //$shippingMethod = ShippingHelpers::getShippingMethodById($request->get('shippingMethodId', null), $request->get('shippingZoneId', null), $validateCoupon);
        //$shippingAmount = ShippingHelpers::getCurrentShippingMethodAmount($shippingMethod, true);
        $shippingAmount = 0;
        $cartSubtotalWithCoupon = $cartSubtotal - $cartDiscountAmount;
        $cartSubtotalWithCoupon = $cartSubtotalWithCoupon >= 0 ? $cartSubtotalWithCoupon : 0;
        $cartTotal = $cartSubtotalWithCoupon + $shippingAmount;

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => __("front.product_was_deleted_from_cart"),
                'type' => 'info'
            ],
            'cartCount' => $cartCount,
            'cartSubtotal' => $cartSubtotal,
            'cartTotal' => $cartTotal,
            'cartSubtotalStr' => formatPrice($cartSubtotal, $this->currCurrency()),
            'cartTotalStr' => formatPrice($cartTotal, $this->currCurrency()),
            'cartSubtotalWithCoupon' => $cartSubtotalWithCoupon,
            'cartSubtotalWithCouponStr' => formatPrice($cartSubtotalWithCoupon, $this->currCurrency()),
            'productQty' => $cartProduct->quantity,
            'productQtyPriceStr' => formatPrice(((is_numeric($cartProductPrice) ? $cartProductPrice : 0) * $cartProduct->quantity), $this->currCurrency()),
            'thumbnail' => $cartThumbnail,
            'destroyCart' => $destroyCart,
            'couponMsg' => $validateCouponErrorMsg
        ]);

    }

    public function checkoutv1(Request $request)
    {
        $post = [
            'command' => 'v',
            'amount' => '12350',
            'currency' => '498',
            'description' => 'description',
            'language' => 'ru',
            'client_ip_addr' => $_SERVER['SERVER_ADDR']
        ];

        // $addr = curl_init('https://maib.ecommerce.md:11440/ecomm01/MerchantHandler');
        // $addr = curl_init('https://maib.ecommerce.md:21440/ecomm/MerchantHandler');

        $addr = curl_init(env('MAIB_MERCHANT_HANDLER'));

        curl_setopt($addr, CURLOPT_CERTINFO, true);
        curl_setopt($addr, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($addr, CURLOPT_SSLCERT,  storage_path(env('MAIB_CERT')));
        curl_setopt($addr, CURLOPT_SSLCERTPASSWD, env('MAIB_CERT_PASS'));
        curl_setopt($addr, CURLOPT_POST, TRUE);
        curl_setopt($addr, CURLOPT_POSTFIELDS, http_build_query($post));
        $response = curl_exec($addr);
        $errors = curl_error($addr);
        curl_close($addr);

        var_dump($response);
        var_dump($errors);
        exit;
    }


    public function checkout(Request $request)
    {
        $currCurrency = $this->currCurrency();
        //$cart = Cart::where('user_id', $this->userId())->where('is_draft', 0)->first();
        $cart = Cart::personalOrCommon()->where('is_draft', 0)->first();
        $allCartProducts = OrdersHelpers::getCartProducts($cart, -1, true, true);
        $cartSubtotal = @$allCartProducts->total ?: 0;
        $cartProducts = $allCartProducts->products;

        $paymentMethod = $request->get('checkout_payment_method', 'cash');

        $shippingType = $request->get('checkout_type_shipping', 'shipping');
        $isShippingType = $shippingType == 'shipping';
        $isPickupType = !$isShippingType;

        //$shippingZoneId = $request->get('zone', null);

        if (is_null($cart))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("front.cart_not_exist")],
                    'type' => 'error'
                ]
            ]);

        //Validate discount
        $cartDiscount = @$cart->discount;
        $validateCoupon = null;

        if ($cartDiscount) {
            $validateCoupon = CouponsHelpers::validateCoupon(@$cartDiscount->code, $currCurrency, $cartSubtotal, false);

            if (is_array($validateCoupon))
                return response()->json([
                    'status' => false,
                    'isCouponError' => true,
                    'msg' => $validateCoupon
                ]);
        }

        if(intval($request->number_of_simple_sticks) == 0) $request->request->remove('number_of_simple_sticks');
        if(intval($request->number_of_beginner_sticks) == 0) $request->request->remove('number_of_beginner_sticks');

        $rules = [
            'name' => 'required|max:50',
            'phone' => 'required|regex:/^((\+)(\d+\s?)?(\(\d+\)\s?)?)?(\d+\s?\-?)+(\d)?$/|max:20',
            'email' => 'required|email',
            'accept_terms' => 'required',
            'number_of_simple_sticks' => 'required_without:number_of_beginner_sticks',
            'number_of_beginner_sticks' => 'required_without:number_of_simple_sticks',
        ];

        if ($isShippingType) {
            $rules = array_merge($rules, [
                'zone' => 'required',
                'address' => 'required',
                'comment' => 'nullable|max:300',
                'zip_code' => "nullable",
            ]);

        } else {
            $rules = array_merge($rules, [
                'restaurant' => 'required',
            ]);
        }

        $messages = [
            'name.required' => __("front.name_required"),
            'name.max' => __("front.name_max_chars", ['number' => 50]),
            'phone.required' => __("front.phone_required"),
            'phone.regex' => __("front.phone_format_invalid"),
            'phone.max' => __("front.phone_max_chars", ['number' => 20]),
            'email.required' => __("front.email_required"),
            'email.email' => __("front.email_valid"),
            'country.required' => __("front.country_required"),
            'region.required' => __("front.region_required"),
            'zone.required' => __("front.city_required"),
            'address.required' => __("front.address_required"),
            'address.max' => __("front.address_max_chars", ['number' => 100]),
            'comment.max' => __("front.comment_max_chars", ['number' => 300]),
            'zip_code.postal_code' => __("front.postal_code_valid"),
            'pickup_country.required' => __("front.pickup_country_required"),
            'pickup_region.required' => __("front.pickup_region_required"),
            'pickup_address.required' => __("front.pickup_address_required"),
            'accept_terms.required' => __("front.accept_terms_required"),
            'restaurant.required' => __("front.restaurant_required"),
            'number_of_simple_sticks.required_without' => __("front.number_of_sticks_required"),
            'number_of_beginner_sticks.required_without' => __("front.number_of_bsticks_required"),
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->errors(),
                    'type' => 'error'
                ]
            ]);
        }
        $shippingMethod = $shippingStreet = $restaurant = null;
        if ($isShippingType) {
            $shippingZone = ShippingZones::where('id', $request->get('zone'))->first();
            $shippingStreet = IikoStreets::where('id', $request->get('address'))->first();
            $restaurant = RestaurantsId::where('iiko_id', $shippingStreet->organization_id)->with('globalName')->first();
            if(is_null($restaurant))
                $restaurant = RestaurantsId::where('is_default', 1)->with('globalName')->first();

        }

        if ($isPickupType)
            $restaurant = RestaurantsId::where('id', $request->restaurant)->with('globalName')->first();

        $checkoutOnlinePayment = $request->get('checkout_online_payment', null);

        if ($cartProducts->isEmpty())
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Cart is empty.'],
                    'type' => 'warning'
                ]
            ]);

        if ($allCartProducts->outOfStockProductsIds->isNotEmpty())
            return response()->json([
                'status' => false,
                'scrollToProducts' => true,
                'outOfStockProducts' => $allCartProducts->outOfStockProductsIds,
                'msg' => [
                    'e' => [__("front.product_no_longer_in_stock")],
                    'type' => 'warning'
                ],
            ]);

        //OrdersHelpers::updateProductsPriceOnCheckout($cart, $cartProducts);

        //Shipping
        $shippingAmount = $cartShippingAmount = 0;
        if($isShippingType){
            $shippingAmount = floatval($shippingZone->shipping_amount);
            if(is_null($shippingZone->free_shipping_amount) || $cartSubtotal < $shippingZone->free_shipping_amount){
                $cartShippingAmount = $shippingAmount;
            }
        }

        //Discount
        $cartDiscountAmount = 0;
        if (@$validateCoupon) {
            if (!@$validateCoupon->allow_free_shipping) {
                if (@$validateCoupon->type == 'fixed')
                    $cartDiscountAmount = @$validateCoupon->amount;
                else
                    $cartDiscountAmount = $cartSubtotal * @$validateCoupon->amount / 100;
            } else
                $cartShippingAmount = 0;
        }

        //Discount

        $authUser = auth()->user();

        $cart->update([
            'auth_user_id' => auth()->check() ? $authUser->id : null,
            'order_id' => OrdersHelpers::orderIdGenerator($cart->id, $cart->order_id),
            'online_payment_name' => $checkoutOnlinePayment,
            'shipping_method_id' => null,//$request->get('zone', null),
            'discount_id' => @$validateCoupon->id,
            'payment_method' => $paymentMethod,
            'shipping_amount' => $cartShippingAmount > 0 ? $cartShippingAmount : null,
            'discount_amount' => $cartDiscountAmount > 0 ? $cartDiscountAmount : null,
            'email' => $request->get('email', null),
            'phone' => $request->get('phone', null),
            'city' => $isShippingType ? $shippingZone->name : null,
            'bank_id_transaction' => null,
            'shipping_pickup_address_id' => $shippingType == 'pickup' ? @$restaurant->id : $request->get('zone', null),
            'order_currency' => $currCurrency,
            'order_lang' => LANG,
            'details' => [
                'name' => $request->get('name', null),
                'comment' => $request->get('comment', null),
                'number_of_simple_sticks' => $request->get('number_of_simple_sticks', null),
                'number_of_beginner_sticks' => $request->get('number_of_beginner_sticks', null),
                'address' => $isShippingType ? @$shippingStreet->name : @$restaurant->globalName->address
            ],
            'shipping_details' => $isShippingType ? [
                'city' => $shippingZone->name,
                'shipping_zone_id' => $shippingZone->id,
                'iiko_service_id' => $shippingZone->iiko_service_id,
                'iiko_street_id' => $shippingStreet->id,
                'address' => @$shippingStreet->name,
                'section' => $request->get('section', null),
                'scale' => $request->get('scale', null),
                'apartment' => $request->get('apartment', null),
                'floor' => $request->get('floor', null),
                'intercom' => $request->get('intercom', null),
                'ground_house' => $request->get('ground_house', null),
                'house' => $request->get('house', null),
            ] : null,
            'pickup_details' => $isPickupType ? [
                'id' => @$restaurant->id,
                'name' => @$restaurant->globalName->name,
                'address' => @$restaurant->globalName->address,
            ] : null,
            //'discount_details' => $validateCoupon ? [
            //    'code' => $validateCoupon->code,
            //    'type' => $validateCoupon->type,
            //    'amount' => $validateCoupon->amount,
            //    'allow_free_shipping' => $validateCoupon->allow_free_shipping,
            //    'expiry_date' => $validateCoupon->expiry_date,
            //    'min_subtotal' => $validateCoupon->min_subtotal,
            //    'max_subtotal' => $validateCoupon->max_subtotal
            //] : null,
            'order_date' => now(),
            'user_ip' => $request->ip(),
            'is_fast_buy' => 0,
            'seen' => 0,
            'is_draft' => 0,
            'restaurant_id' => $restaurant->id,
            'amount' => $cartSubtotal,
        ]);

        OrdersHelpers::updateAuthUserData($authUser, $cart);

        session()->flash('userHasCart', $cart->user_id);

        $cartSubtotalWithCoupon = $cartSubtotal - $cartDiscountAmount;
        $cartSubtotalWithCoupon = $cartSubtotalWithCoupon >= 0 ? $cartSubtotalWithCoupon : 0;
        $cartTotal = $cartSubtotalWithCoupon + $cartShippingAmount;
        $sendMailData = [
            'item' => $cart,
            'cartProducts' => $cartProducts,
            'useShipping' => @$cart->shipping_details,
            'usePickup' => @$cart->pickup_details,
            'cartSubtotal' => $cartSubtotal,
            'cartSubtotalWithCoupon' => $cartSubtotalWithCoupon,
            'shippingAmount' => $cartShippingAmount,
            'cartTotal' => $cartTotal,
            'orderCurrency' => $cart->order_currency
        ];

        if ($paymentMethod == 'cash') {

            return $this->finishOrder($cart, $sendMailData);

        } elseif ($paymentMethod == 'card_curier') {

            return $this->finishOrder($cart, $sendMailData);

        } elseif ($paymentMethod == 'card') {

            $cart->update([
                'status' => 'processing'
            ]);

            try {
                return $this->getOnlineCardMaibPayment($cart, $cartTotal);
            }catch(\Exception $e){
                dd($e->getMessage());
            }
        }

        return response()->json([
            'status' => false,
            'redirect' => customUrl('cart')
        ]);

    }

    public function successMsg()
    {
        //Cart::where('user_id', session()->get('userHasCart', null))->where('is_draft', 0)->firstOrFail();

        return view("{$this->path}.successResponse");
    }

    public function errorMsg()
    {
        //Cart::where('user_id', session()->get('userHasCart', null))->where('is_draft', 0)->firstOrFail();

        return view("{$this->path}.errorResponse");
    }

    public function applyCoupon(Request $request)
    {
        $couponCode = $request->get('coupon_code', null);

        $validator = Validator::make($request->all(), [
            'coupon_code' => 'required'
        ], [
            'coupon_code.require' => __("front.coupon_required")
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->messages(),
                    'type' => 'error'
                ],
            ]);
        }

        $currCurrency = $this->currCurrency();

        $cart = Cart::where('user_id', $this->userId())->where('is_draft', 0)->first();

        if (is_null($cart))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("front.cart_not_exist")],
                    'type' => 'error'
                ]
            ]);

        if (@$cart->discount)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("front.already_applied_coupon", ['coupon' => $cart->discount->code])],
                    'type' => 'warning'
                ]
            ]);

        $allCartProducts = OrdersHelpers::getCartProducts($cart, -1, true, true);
        $cartSubtotal = @$allCartProducts->total ?: 0;

        $validateCoupon = CouponsHelpers::validateCoupon($couponCode, $currCurrency, $cartSubtotal);

        if (is_array($validateCoupon))
            return response()->json([
                'status' => false,
                'msg' => $validateCoupon
            ]);

        if ($validateCoupon->type == 'fixed')
            $cartSubtotalWithCoupon = $cartSubtotal - $validateCoupon->amount;
        else
            $cartSubtotalWithCoupon = $cartSubtotal - ($cartSubtotal * $validateCoupon->amount / 100);

        if ($validateCoupon->allow_free_shipping)
            $cartSubtotalWithCoupon = $cartSubtotal;

        $cartSubtotalWithCoupon = $cartSubtotalWithCoupon >= 0 ? $cartSubtotalWithCoupon : 0;

        $country = Countries::where('active', 1)
            ->countriesWithShipping()
            ->find($request->get('countryId', null));

        if (!$country)
            $country = $this->currLocation();

        $region = Regions::where('country_id', $country->id)
            ->where('active', 1)
            ->find($request->get('regionId', null));

        $shippingZone = ShippingHelpers::getShippingZoneByLocation($country, $cartSubtotal, $region, $validateCoupon);
        $shippingAmount = ShippingHelpers::getCurrentShippingMethodAmount($shippingZone);

        try {
            $errorsView = view("{$this->path}.templates.cartTabsShippingErrors", ['shippingZone' => $shippingZone])->render();
            $shippingMethodsView = view("{$this->path}.templates.cartTabsShippingMethods", ['shippingZone' => $shippingZone, 'cartSubtotal' => $cartSubtotal])->render();
        } catch (\Throwable $e) {
            $errorsView = '';
            $shippingMethodsView = '';
        }

        $cartTotal = $cartSubtotalWithCoupon + $shippingAmount;

        $cart->update([
            'discount_id' => $validateCoupon->id,
            'discount_details' => [
                'code' => $validateCoupon->code,
                'type' => $validateCoupon->type,
                'amount' => $validateCoupon->amount,
                'allow_free_shipping' => $validateCoupon->allow_free_shipping,
                'expiry_date' => $validateCoupon->expiry_date,
                'min_subtotal' => $validateCoupon->min_subtotal,
                'max_subtotal' => $validateCoupon->max_subtotal
            ]
        ]);

        return response()->json([
            'status' => true,
            'couponId' => $validateCoupon->id,
            'couponCode' => $validateCoupon->code,
            'cartOldSubtotal' => $cartSubtotal,
            'cartOldSubtotalStr' => formatPrice($cartSubtotal, $currCurrency),
            'cartSubtotal' => $cartSubtotalWithCoupon,
            'cartSubtotalStr' => formatPrice($cartSubtotalWithCoupon, $currCurrency),
            'cartTotal' => $cartTotal,
            'cartTotalStr' => formatPrice($cartTotal, $currCurrency),
            'errorsView' => $errorsView,
            'shippingMethodsView' => $shippingMethodsView,
            'shippingZoneId' => $shippingZone->zone ? $shippingZone->zone->id : null,
            'freeShippingCoupon' => $validateCoupon->allow_free_shipping ? true : false,
            'freeShippingCouponMsg' => __("front.free_shipping_coupon_msg")
        ]);
    }

    public function destroyCoupon(Request $request)
    {
        $couponId = $request->get('couponId', null);
        $currCurrency = $this->currCurrency();

        $cart = Cart::where('user_id', $this->userId())->where('is_draft', 0)->first();

        if (is_null($cart))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Cart not exist.'],
                    'type' => 'error'
                ]
            ]);

        $cartDiscount = @$cart->discount;

        if (@$cartDiscount->id != $couponId)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => __("front.cart_not_exist"),
                    'type' => 'warning'
                ]
            ]);

        $cart->where('discount_id', $couponId)->update([
            'discount_id' => null,
            'discount_details' => null
        ]);

        $allCartProducts = OrdersHelpers::getCartProducts($cart, -1, true, true);
        $cartSubtotal = @$allCartProducts->total ?: 0;

        $country = Countries::where('active', 1)
            ->countriesWithShipping()
            ->find($request->get('countryId', null));

        if (!$country)
            $country = $this->currLocation();

        $region = Regions::where('country_id', $country->id)
            ->where('active', 1)
            ->find($request->get('regionId', null));

        $shippingZone = ShippingHelpers::getShippingZoneByLocation($country, $cartSubtotal, $region);
        $shippingAmount = ShippingHelpers::getCurrentShippingMethodAmount($shippingZone);

        try {
            $errorsView = view("{$this->path}.templates.cartTabsShippingErrors", ['shippingZone' => $shippingZone])->render();
            $shippingMethodsView = view("{$this->path}.templates.cartTabsShippingMethods", ['shippingZone' => $shippingZone, 'cartSubtotal' => $cartSubtotal])->render();
        } catch (\Throwable $e) {
            $errorsView = '';
            $shippingMethodsView = '';
        }

        $cartTotal = $cartSubtotal + $shippingAmount;

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => __("front.coupon_was_removed"),
                'type' => 'info'
            ],
            'cartSubtotal' => $cartSubtotal,
            'cartSubtotalStr' => formatPrice($cartSubtotal, $currCurrency),
            'cartTotal' => $cartTotal,
            'cartTotalStr' => formatPrice($cartTotal, $currCurrency),
            'errorsView' => $errorsView,
            'shippingMethodsView' => $shippingMethodsView,
            'shippingZoneId' => $shippingZone->zone ? $shippingZone->zone->id : null,
        ]);
    }

    protected function userId()
    {
        return request()->cookie('cart', null);
    }

    private function getOnlineCardMaibPayment($cart, $amountToPay)
    {
        $string_cart_desc = 'sushi - Comanda ' . $cart->order_id;;

        $client = new MaibClient();
        $resp = $client->registerDmsAuthorization( numberFormat($cart->amount + $cart->shipping_amount), 498, $_SERVER['SERVER_ADDR'], htmlspecialchars_decode($string_cart_desc), LANG);

        if (isset($resp['TRANSACTION_ID']) && !empty($resp['TRANSACTION_ID']))
        {
            // $customer_url = 'https://maib.ecommerce.md/ecomm01/ClientHandler';//new
            // $customer_url = 'https://ecomm.maib.md/ecomm2/ClientHandler';
            // $customer_url = 'https://ecomm.maib.md:7443/ecomm2/ClientHandler';

            $customer_url = env('MAIB_CLIENT_HANDLER');

            $cart->update([
                'payment_bank_process' => 'A',
                'payment_bank_status' => 'OK',
                'status' => 'processing',
                'bank_id_transaction' => $resp['TRANSACTION_ID']
            ]);

            return response()->json([
                'resp' => $resp,
                'form' => [
                    'uri' => $customer_url,
                    'trans_id' => $resp['TRANSACTION_ID'],
                ],
                'status' => true,
            ]);

        } else {
            return response()->json([
                'status' => false,
                'message' => [
                    'content' => __('front.something_wrong') . ' ' . $resp['error'],
                    'type' => 'error'
                ],
            ]);
        }
    }

    public function maibPaymentCallback(Request $request)
    {
        LogsPayment::log('maibPaymentCallback 1', 0, json_encode($request->all()), $request->all());

        if (isset($request->trans_id) && !empty($request->trans_id)) {
            $cart = Cart::where('bank_id_transaction', $request->trans_id)->first();

            if (!is_null($cart)) {
                $client = new MaibClient();

                $response = $client->getTransactionResult($cart->bank_id_transaction, $_SERVER['SERVER_ADDR']);
                LogsPayment::log('maibPaymentCallback 2', $cart->id, 'trans_id: '.$cart->bank_id_transaction, $response);
                $string_cart_desc = 'sushi - Comanda ' . $cart->order_id;;


                if (isset($response['RESULT']) && $response['RESULT'] === "OK")
                {
                    // $response = $client->makeDMSTrans($cart->bank_id_transaction,  numberFormat($cart->amount + $cart->shipping_amount), 498, $_SERVER['REMOTE_ADDR'],htmlspecialchars_decode($string_cart_desc), LANG);
                    // var_dump($response);

                    $cart->update([
                        'payment_bank_process' => 'T',
                        'payment_bank_status' => $response['RESULT'],
                    ]);

                    // echo 'payment_bank_process - T';
                    // echo ' finishOrder ';

                    return $this->finishOrder($cart);

                }else{

                    // echo 'payment_bank_process - C';

                    $cart->update([
                        'payment_bank_process' => 'C',
                        'payment_bank_status' => $response['RESULT'],
                    ]);
                }
            }
        }

        return redirect()->to(customUrl('errorMsg'));
    }

    private function finishOrder(Cart $cart, $sendMailData = null)
    {
        OrdersHelpers::sendNotificationToAdminPanel($cart);

        $cart->update([
            'status' => 'checkout'
        ]);

        $restaurant = RestaurantsId::where('id', $cart->restaurant_id)->first();

        $iikoClient = new IikoClient($restaurant);
        $order = $iikoClient->createOrder($cart);

        if(isset($order['orderInfo']) && !empty($order['orderInfo']))
        {
            $cart->update([
                'iiko_id' => $order['orderInfo']['id'],
                'iiko_status' => $order['orderInfo']['creationStatus']
                ]);
            //if($order['orderInfo']['creationStatus'] === 'Success'){
            //if(isset($order['orderInfo']['id']) && !empty($order['orderInfo']['id']))
                //$iikoClient->closeOrder($cart);
            //}
        }

        if(is_null($sendMailData))
        {
            $cartDiscountAmount = $cart->discount_amount;
            $cartShippingAmount = $cart->shipping_amount;
            $allCartProducts = OrdersHelpers::getCartProducts($cart, -1, true, true);
            $cartSubtotal = @$allCartProducts->total ?: 0;
            $cartProducts = $allCartProducts->products;
            $cartSubtotalWithCoupon = $cartSubtotal - $cartDiscountAmount;
            $cartSubtotalWithCoupon = $cartSubtotalWithCoupon >= 0 ? $cartSubtotalWithCoupon : 0;
            $cartTotal = $cartSubtotalWithCoupon + $cartShippingAmount;
            $sendMailData = [
                'item' => $cart,
                'cartProducts' => $cartProducts,
                'useShipping' => @$cart->shipping_details,
                'usePickup' => @$cart->pickup_details,
                'cartSubtotal' => $cartSubtotal,
                'cartSubtotalWithCoupon' => $cartSubtotalWithCoupon,
                'shippingAmount' => $cartShippingAmount,
                'cartTotal' => $cartTotal,
                'orderCurrency' => $cart->order_currency
            ];
        }

        try {
            $adminEmailForOrders = helpers()::getSettingsField('order_email', parent::globalSettings());
            helpers()->sendMail($cart->email, $cart->details->name, $sendMailData, 'checkoutForm', parent::globalSettings(), __('front.email_checkout_subject'));
            helpers()->sendMail($adminEmailForOrders, $cart->details->name, $sendMailData, 'checkoutAdminForm', parent::globalSettings(), __('front.email_checkout_subject_admin'), true);
        } catch (\Exception $e) {
        }

        Cookie::queue(Cookie::forget('cart'));

        if(session('common_cart_id'))
            session(['common_cart_id' => null]);

        if (request()->ajax())
        {
            return response()->json([
                'status' => true,
                'redirect' => customUrl('successMsg')
            ]);
        } else {
            return redirect()->to(customUrl('successMsg'));
        }
    }

    public function createCommonCart(Request $request)
    {
        $cart = Cart::where('user_id', $this->userId())->where('is_draft', 0)->first();

        if($cart)
        {
            $uniqId = uniqid();
            $cart->update([
                'common_id' => $uniqId,
                'common_user_root' => $this->userId(),
                'common_created_at' => date('Y-m-d H:i:s')
            ]);

            //echo $uniqId;
            $resp['status'] = true;
            $resp['text'] = 'OK';
            $resp['common_id'] = $uniqId;
            $resp['common_url'] = customUrl(['cart', $uniqId]);

            session(['common_cart_id' => $uniqId]);
            //Cookie::queue('common_cart_id', $uniqId);

        }else{
            $resp['status'] = false;
            $resp['text'] = 'Cart is empty';
        }

        return response()->json($resp)->setStatusCode(200);
    }

    public function cancelCommonCart(Request $request)
    {
        $resp['status'] = false;

        if($common_id=session('common_cart_id'))
        {
            $cart = Cart::where('common_id', $common_id)->first();
            //if($cart && $cart->common_user_root == $this->userId())
            //{
                $cart->update([
                    'common_id' => null,
                    'common_user_root' => null,
                    'common_created_at' => null,
                ]);

                $resp['status'] = true;
                $resp['msg']['type'] = 'success';
                $resp['msg']['e'] = __('front.success_cancel_common_cart');
            //}else{
            //    $resp['msg']['type'] = 'error';
            //    $resp['msg']['e'] = __('front.cant_cancel_common_cart');
            //}
        }

        return response()->json($resp)->setStatusCode(200);
    }
}

