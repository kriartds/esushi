<?php

namespace App\Http\Controllers\Front;

use App\Http\Helpers\Helpers;
use App\IikoApi\IikoClient;
use Modules\Currencies\Models\Currencies;
use Illuminate\Pagination\Paginator;

use Illuminate\Support\Facades\Cookie;
use Modules\Pages\Models\PagesId;
use Modules\Products\Models\ProductsItemsId;
use Modules\Subscribers\Models\Subscribers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Products\Models\ProductsCategoriesId;

class IndexController extends DefaultController
{
    private $path;
    private $productsInstancesWiths;

    public function __construct()
    {
        $this->path = 'front';
        $this->productsInstancesWiths = ['file.file', 'products', 'globalName', 'products.globalName'];
    }

    public function index()
    {
        $mainPageId = Helpers::getSettingsField('home_page', $this->globalSettings());
        $page = PagesId::where('active', 1)->with('globalName')->find($mainPageId);
        $view = !is_null(@$page->template) ? "{$this->path}.pages.templates.{$page->template}" : "{$this->path}.index";

        $sectionCategories = ProductsCategoriesId::where('active', 1)
            ->where('show_on_main', 1)
            ->with($this->productsInstancesWiths)
            ->get();

        /**
         * Main wide slider
         */
        $sliderWideSlides = getSliderBySlug('main-slider');

        $response = [
            'path' => $this->path,
            'mainPage' => $page,
            'sliderWideSlides' => $sliderWideSlides,
            'sectionCategories' => $sectionCategories
        ];

        $response = array_merge($response, getItemMeta($page, 'globalName', $this->globalSettings(), true));

        return view($view, $response);

    }

    public function subscribe(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:subscribers,email'
        ];

        $messages = [
            'email.required' => __("front.email_required"),
            'email.email' => __("front.email_valid"),
            'email.unique' => __("front.email_unique"),
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->messages(),
                    'type' => 'error',
                ],
            ]);
        }

        Subscribers::create([
            'email' => $request->get('email', null),
            'ip' => $request->ip(),
            'unsubscribe_token' => str_random(60),
        ]);

        try {
//            helpers()->sendMail($subscriber->email, $subscriber->email, ['item' => $subscriber], 'subscribeForm', parent::globalSettings(), 'You was subscribe on ' . ' ' . config('app.url'));
        } catch (\Exception $e) {
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'message' => __("front.subscribed"),
                'type' => 'success'
            ]
        ]);
    }

    public function unsubscribe(Request $request, $lang, $token)
    {

        $subscriber = Subscribers::whereNotNull('unsubscribe_token')
            ->where('unsubscribe_token', $token)
            ->first();

        if (is_null($subscriber))
            abort(404);

        $subscriber->delete();

        return view("{$this->path}.components.subscribe.unsubscribe");
    }

    public function search(Request $request, $lang, $categorySlug = null)
    {
        $search = $request->get('q');
        $perPage = Helpers::getSettingsField('site_items_per_page', parent::globalSettings());
        $items = $itemsForResults = $categories = $categoriesForResults = collect();
        $searchUrl = null;

        if ($search) {
            $itemsBuilder = ProductsItemsId::whereHas('categories', function ($q) {
                $q->where('active', 1);
            })->where(function ($q) use ($search, $categorySlug, $request) {
                $this->searchQueries($q, $search, $request, false, $categorySlug);
            })
                ->with(['file.file', 'globalName', 'variationsDetails'])
                ->orderBy('position');
            $itemsForResultsBuilder = clone $itemsBuilder;
            $itemsForResults = $itemsForResultsBuilder->limit(3)->get();
            $items = $itemsBuilder->paginate($perPage);

            $searchUrl = customUrl(['search', (!is_null($categorySlug) ? $categorySlug : '')]) . "?q={$search}";
            $items->setPath($searchUrl);

            $categoriesBuilder = ProductsCategoriesId::where('active', 1)
                ->whereHas('products', function ($q) use ($search, $request) {
                    $this->searchQueries($q, $search, $request, true);
                })
                ->orWhere(function ($q) use ($search) {
                    $q->where('slug', 'like', "%{$search}%");
                    $q->orWhereHas('items', function ($q) use ($search) {
                        $q->where('name', 'like', "%{$search}%");
                        $q->orWhere('description', 'like', "%{$search}%");
                    });
                })
                ->withCount(['products' => function ($q) use ($search, $request, $categorySlug) {
                    $this->searchQueries($q, $search, $request, true);
                }])
                ->with(['globalName'])
                ->orderBy('position');

            $categoriesForResultsBuilder = clone $categoriesBuilder;
            $categoriesForResults = $categoriesForResultsBuilder->limit(3)->get();
            $categories = $categoriesBuilder->limit(20)->get();
        }

        $response = [
            'items' => $items,
            'categories' => $categories,
        ];

        if ($request->ajax() && $request->method() == 'POST') {
            $response = [
                'items' => $itemsForResults,
                'categories' => $categoriesForResults,
                'totalCount' => $items->total(),
                'displayCount' => $itemsForResults->count(),
                'searchUrl' => $searchUrl
            ];

            return response()->json([
                'status' => true,
                'view' => view("{$this->path}.components.header.search.headerSearchResults", $response)->render(),
                'categoriesCount' => $categories->count()
            ]);

            try {

            } catch (\Throwable $e) {
                return response()->json([
                    'status' => false,
                ]);
            }
        }

        return view("{$this->path}.search.search", $response);

    }

    public function setCurrency(Request $request)
    {
        $currencies = Currencies::where('active', 1)->get();
        $currCurrency = $currencies->find($request->get('itemId'));

        if (is_null($currCurrency))
            return response()->json([
                'status' => false
            ]);

        $defaultCurrency = @$currencies->where('is_default', 1)->first();
        $currBNMCurrency = @helpers()->getBnmCurrenciesByDate($currCurrency->slug);

        if (is_null($currBNMCurrency))
            $currBNMCurrency = @helpers()->getBnmCurrenciesByDate($defaultCurrency->slug);
//        $defaultBNMCurrency = Helpers::getBnmCurrenciesByDate($defaultCurrency->slug);

        $cookieCurrencies = (object)[
            'currCurrency' => $currBNMCurrency,
//            'defaultCurrency' => $defaultBNMCurrency,
            'isCustom' => true
        ];

        if ($request->cookie('currency') && $request->cookie('currency') != json_encode($cookieCurrencies))
            Cookie::queue(Cookie::forget('currency'));

        Cookie::queue(Cookie::forever('currency', json_encode($cookieCurrencies)));

        return response()->json([
            'status' => true
        ]);
    }

    public   function newProducts(Request $request)
    {
        $response = [
            'status' => false
        ];

        $newProductsPerPage = 8;
        $currentPage = $request->get('currPage', null);
        $lastPage = $request->get('lastPage', null);

        if ($request->ajax()) {
            if ($currentPage <= $lastPage && $currentPage > 0) {
                $currentPage = ($currentPage && $currentPage > 0 ? $currentPage : 1) + 1;

                Paginator::currentPageResolver(function () use ($currentPage) {
                    return $currentPage;
                });
            }
        }

        $newProducts = ProductsItemsId::where('active', 1)
            ->whereDate('created_at', '>=', now()->subWeeks(config('cms.product.isNew', 1))->format('Y-m-d'))
            ->orderBy('created_at', 'desc')
            ->with($this->productsInstancesWiths)
            ->paginate($newProductsPerPage);

        if ($request->ajax() && $currentPage <= $lastPage && $currentPage > 0) {
            try {
                $response = [
                    'status' => true,
                    'currentPage' => $newProducts->currentPage(),
                    'lastPage' => $newProducts->lastPage(),
                    'newProducts' => $newProducts
                ];

                $response['view'] = view("{$this->path}.components.main.templates.newProductsList", $response)->render();
            } catch (\Throwable $e) {
                abort(503);
            }

            return response()->json($response);
        }

        return $newProducts;
    }

    public function changeCatalogListView(Request $request)
    {

        $orientation = $request->get('orientation', null) == 'horizontal' ? 'horizontal' : 'vertical';

        if ($orientation) {
            if (!is_null(Cookie::get('catalog_view'))) {
                Cookie::queue(Cookie::forget('catalog_view'));
            }

            Cookie::queue('catalog_view', $orientation, 1440);

            return response()->json([
                'status' => true,
                'orientation' => $orientation
            ]);
        }

        Cookie::queue(Cookie::forget('catalog_view'));

        return response()->json([
            'status' => true,
            'orientation' => $orientation
        ]);
    }

    private function searchQueries($q, $search, $request, $isCategories = false, $categorySlug = null)
    {
        $q->where('active', 1);
        $q->where(function ($q) use ($search, $isCategories, $categorySlug, $request) {
            $q->where(function ($q) use ($search, $isCategories, $categorySlug) {
                $q->where('slug', 'like', "%{$search}%");
                $q->orWhereHas('items', function ($q) use ($search) {
                    $q->where('name', 'like', "%{$search}%");
                    $q->orWhere('description', 'like', "%{$search}%");
                });
            });
            if (!$isCategories) {
                if (!is_null($categorySlug) && $request->method() == 'GET')
                    $q->whereHas('categories', function ($q) use ($search, $categorySlug, $request) {
                        $q->where('active', 1);
                        $q->where('slug', $categorySlug);
                    });
                else
                    $q->orWhere(function ($q) use ($search, $categorySlug, $request) {
                        $q->whereHas('categories', function ($q) use ($search, $categorySlug, $request) {
                            $q->where('active', 1);
                            $q->where('slug', 'like', "%{$search}%");
                            $q->orWhereHas('items', function ($q) use ($search) {
                                $q->where('name', 'like', "%{$search}%");
                                $q->orWhere('description', 'like', "%{$search}%");
                            });
                        });
                    });
            }
        });

        return $q;
    }
}
