<?php

namespace App\Http\Controllers\Front;

use App\IikoApi\Models\IikoStreets;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\ShippingZones\Models\ShippingZones;

class AjaxController extends DefaultController
{
    public function getAddresses(Request $request)
    {
        if($request->ajax())
        {
            $shippingZone = ShippingZones::find($request->zone_id);
            if($shippingZone)
            {
                $addresses = IikoStreets::select('id', 'name')->where('shipping_zone_id', $shippingZone->id)->get();
                if($addresses->isNotEmpty())
                {
                    return response()->json([
                        'status' => true,
                        'addresses' => array_merge([['id' => '', 'name' => __('front.select_address')]], $addresses->toArray())
                    ]);
                }
            }
        }

        return response()->json([
            'status' => false,
            'message' => __('front.something_wrong')
        ]);
    }
}
