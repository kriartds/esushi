<?php

namespace App\Http\Controllers\Front;

use App\Http\Helpers\Helpers;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;;
use Modules\Orders\Http\Helpers\Helpers as OrdersHelpers;

class UsersController extends DefaultController
{

    private $path;

    public function __construct()
    {
        $this->path = 'front.auth.profile';
    }

    public function index()
    {

        $user = auth()->user();

        $orders = OrdersHelpers::getAuthUserOrdersProducts($user, 3);

        $userSubscribeOptions = $user->subscribe;

//        Meta
        $titlePage = __("front.dashboard");
//        Meta

        $response = [
            'path' => $this->path,
            'user' => $user,
            'orders' => @$orders->orders ?: collect(),
            'ordersCount' => @$orders->ordersCount ?: 0,
            'userSubscribeOptions' => $userSubscribeOptions,
            'currComponent' => $this->currComponent(),
            'titlePage' => $titlePage,
        ];

        return view("{$this->path}.dashboard", $response);

    }

    public function settings()
    {

        $user = auth()->user();

//        Meta
        $titlePage = __("front.settings");
//        Meta

        $response = [
            'user' => $user,
            'currComponent' => $this->currComponent(),
            'titlePage' => $titlePage,
        ];

        return view("{$this->path}.settings", $response);
    }

    public function ordersHistory(Request $request)
    {

        $perPage = Helpers::getSettingsField('site_items_per_page', parent::globalSettings());
        $user = auth()->user();

        $currentPage = $request->get('currPage', null);
        $lastPage = $request->get('lastPage', null);
        if ($request->ajax() && $request->method() == 'POST') {
            if ($currentPage <= $lastPage && $currentPage > 0) {
                $currentPage = ($currentPage && $currentPage > 0 ? $currentPage : 1) + 1;

                Paginator::currentPageResolver(function () use ($currentPage) {
                    return $currentPage;
                });
            }
        }

        $orders = OrdersHelpers::getAuthUserOrdersProducts($user, $perPage, true, true);

        $titlePage = __("front.orders-history");

        if ($request->ajax() && $request->method() == 'POST' && $currentPage <= $lastPage && $currentPage > 0) {
            $response = [
                'status' => false
            ];

            try {
                $response = [
                    'status' => true,
                    'currentPage' => $orders->currentPage,
                    'lastPage' => $orders->lastPage,
                    'orders' => @$orders->orders ?: collect(),
                    'fullOrders' => $orders,
                ];

                $response['view'] = view("front.auth.templates.orderHistoryItemsList", $response)->render();

            } catch (\Throwable $e) {
                abort(503);
            }

            return response()->json($response);
        }

        $response = [
            'path' => $this->path,
            'orders' => @$orders->orders ?: collect(),
            'fullOrders' => $orders,
            'titlePage' => $titlePage,
        ];

        return view("{$this->path}.orderHistory", $response);
    }

    public function editAccount(Request $request)
    {
        $user = auth()->user();

        $rules = [
            'name' => 'required|max:50',
            'email' => "required|email|unique:users,email,{$user->id}",
            /* 'phone' => 'required|regex:/^((\+)(\d+\s?)?(\(\d+\)\s?)?)?(\d+\s?\-?)+(\d)?$/|max:20',*/
            'phone' => 'required|regex:/[^0-9]/|max:20',
        ];

        $rulesMsg = [
            'name.required' => __("front.name_required"),
            'name.max' => __("front.name_max_chars", ['number' => 50]),
            'email.required' => __("front.email_required"),
            'email.email' => __("front.email_valid"),
            'email.unique' => __("front.email_unique"),
            'phone.required' => __("front.phone_required"),
            'phone.regex' => __("front.phone_format_invalid"),
            'phone.max' => __("front.phone_max_chars", ['number' => 20])
        ];

        $validator = Validator::make($request->all(), $rules, $rulesMsg);

        if ($validator->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->errors(),
                    'type' => 'error'
                ],
            ]);

        $is_check_phone = $user->is_check_phone;
        $phone = preg_replace("/[^0-9]/", "", $request->get('phone', null));
        if($user->phone != $phone)
        {
            $is_check_phone = 0;
        }

        $data = [
            'name' => $request->get('name', null),
            'email' => $request->get('email', null),
            'phone' => $phone,
            'is_check_phone' => $is_check_phone,
            'city' => $request->get('user_city', null),
            'address' => $request->get('user_address', null),
            'user_region' => $request->get('user_region', null),
            'user_ladder' => $request->get('user_ladder', null),
            'user_apartment' => $request->get('user_apartment', null),
            'user_floor' => $request->get('user_floor', null),
            'user_inter_phone' => $request->get('user_inter_phone', null),
        ];

        $user->update($data);

        return response()->json([
            'status' => true,
            'preventClearForm' => true,
            'msg' => [
                'message' => __("front.data_was_updated"),
                'type' => 'success'
            ]
        ]);

    }

    public function applyDiscountCard(Request  $request){

    }
}
