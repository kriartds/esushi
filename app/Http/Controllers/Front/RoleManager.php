<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\ComponentsId;
use App\Repositories\ComponentsIdRepository;
use Illuminate\Http\Request;

class RoleManager extends Controller
{

    public function routeResponder(Request $request, $lang, $component, $itemSlug = null, $itemVariation = null)
    {

        if ($request->ajax())
            $function = $itemSlug;
        else
            $function = !is_null($itemSlug) ? 'itemPage' : 'index';

        $componentsIdRepo = new ComponentsIdRepository();
        $items = $componentsIdRepo->all(['active' => 1]);

        $item = $items->where('slug', $component)->first();

        if (is_null($item) && is_null($itemSlug)) {
            $item = $items->where('controller', 'PagesController')->first();
            $itemSlug = $component;
        }
        $function = camel_case($function);
        if (!$item || !$item->controller)
            abort(404);

        if(!is_null($item->p_id) && @helpers()->getInactiveComponentParent($item->p_id, $items))
            abort(404);

        if(!is_null($itemVariation) && $item->controller !== 'ProductsController')
            abort(404);

        $namespace = "App\\Http\\Controllers\\Front\\{$item->controller}";

        $response = null;

        try {
            $response = app()->call("{$namespace}@{$function}", [$itemSlug, $itemVariation]);
        } catch (\Exception $e) {
            if (!$e instanceof \ReflectionException)
                $response = app()->call("{$namespace}@{$function}", [$itemSlug, $itemVariation]);
            else if (method_exists($e, 'getStatusCode'))
                abort($e->getStatusCode());
            else
                abort(404);
        }

        return $response;
    }

}
