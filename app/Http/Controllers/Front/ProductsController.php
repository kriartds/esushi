<?php

namespace App\Http\Controllers\Front;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
//use Illuminate\View\View;
use Modules\Products\Http\Helpers\Helpers;
use Modules\Products\Models\ProductsItemsId;
use Illuminate\Http\Request;
use Modules\Products\Models\ProductsItemsReviews;
use Illuminate\Support\Facades\View;
use Modules\Products\Models\ProductsToppingsId;

class ProductsController extends DefaultController
{
    private $path;

    public function __construct()
    {
        $this->path = 'front.products';
    }

    public function itemPage(Request $request, $itemSlug, $itemVariation)
    {

        $item = ProductsItemsId::where('active', 1)
            ->where('slug', $itemSlug)
            ->with(['files.file', 'globalName'])
            ->firstOrFail();

        $toppings = !empty($item->toppings_ids) ? ProductsToppingsId::where('active', 1)->whereIn('id', $item->toppings_ids)->with(['globalName'])->get() : collect();

        $lastCategory = helpers()->getLastItemCategory($item, 'products-categories');

        $defaultValuesFromVariations = defineProductVariations($item, $itemVariation, false);
        $defaultVariation = @$defaultValuesFromVariations['defaultVariation'];

        $characteristicsAttributes = Helpers::getProductSelectedAttributes($item, false, true);
        $deliveryAndPayment = getSettingsField('payment_delivery_desc', parent::globalSettings(), true);

        //  $reviews = $this->productReviews($request, $item->id);

        $similarProducts = similarProducts($item);
        $recommendsProducts = recommendedProducts($item);
        $recentProducts = recentViewedProducts($item);

        $itemMediumFiles = getItemFile($item, 'medium', true, 'allFiles');
        $itemSmallFiles = getItemFile($item, 'small', true, 'allFiles');

        if ($defaultVariation) {
            $defaultVariationMediumFiles = getItemFile($defaultVariation, 'medium', false, 'allFiles');
            $defaultVariationSmallFiles = getItemFile($defaultVariation, 'small', false, 'allFiles');

            if ($defaultVariationMediumFiles)
                $itemMediumFiles = $defaultVariationMediumFiles;

            if ($defaultVariationSmallFiles)
                $itemSmallFiles = $defaultVariationSmallFiles;
        }

        $sharerItems = config('cms.product.sharer', []);

        $response = [
            'path' => $this->path,
            'currComponent' => parent::currComponent(),
            'item' => $item,
            'lastCategory' => $lastCategory,
            'itemVariation' => $itemVariation,
            'defaultValuesFromVariations' => $defaultValuesFromVariations,
            'defaultVariation' => $defaultVariation,
            'displayStock' => @$defaultValuesFromVariations['displayStock'],
            'defaultVariationDetails' => @$defaultValuesFromVariations['allProductVariationsArr']['variations'][@$defaultVariation->id]['variation'],
            'characteristicsAttributes' => $characteristicsAttributes,
            'deliveryAndPayment' => $deliveryAndPayment,
            //'reviews' => $reviews,
            'similarProducts' => $similarProducts,
            'recommendsProducts' => $recommendsProducts,
            'recentProducts' => $recentProducts,
            'itemMediumFiles' => $itemMediumFiles,
            'itemSmallFiles' => $itemSmallFiles,
            'sharerItems' => $sharerItems,
            'toppings' => $toppings,
        ];

        $response = array_merge($response, getItemMeta($item, 'globalName', $this->globalSettings()));

        return view("{$this->path}.itemPage", $response);
    }

    public function productReviews(Request $request, $itemId = null)
    {

        $response = [
            'status' => false
        ];

        $newProductsPerPage = 3;
        $currentPage = $request->get('currPage', null);
        $lastPage = $request->get('lastPage', null);

        if ($request->ajax()) {
            if ($currentPage <= $lastPage && $currentPage > 0) {
                $currentPage = ($currentPage && $currentPage > 0 ? $currentPage : 1) + 1;

                Paginator::currentPageResolver(function () use ($currentPage) {
                    return $currentPage;
                });
            }
        }

        $reviews = ProductsItemsReviews::where('products_item_id', $request->get('itemId', $itemId))
            ->where('active', 1)
            ->orderBy('created_at', 'desc')
            ->with('user')
            ->paginate($newProductsPerPage);

        if ($request->ajax() && $currentPage <= $lastPage && $currentPage > 0) {
            try {
                $response = [
                    'status' => true,
                    'currentPage' => $reviews->currentPage(),
                    'lastPage' => $reviews->lastPage(),
                    'reviews' => $reviews
                ];

                $response['view'] = view("front.components.productPage.templates.productReviewsList", $response)->render();
            } catch (\Throwable $e) {
                abort(503);
            }

            return response()->json($response);
        }

        return $reviews;
    }

    public function addReviews(Request $request)
    {

        $rules = [
            'rating' => 'required|numeric|between:1,5',
            'advantage' => 'nullable|max:' . config('cms.product.reviews.limitAdvantages', 500),
            'disadvantage' => 'nullable|max:' . config('cms.product.reviews.limitDisadvantages', 500),
            'message' => 'nullable|max:' . config('cms.product.reviews.limitMessage', 500),
        ];

        $messages = [
            'rating.required' => __("front.rating_required"),
            'rating.numeric' => __("front.rating_number"),
            'rating.between' => __("front.rating_between", ['min' => 1, 'max' => 5]),
            'advantage.max' => __("front.advantage_max_chars", ['number' => config('cms.product.reviews.limitAdvantages', 500)]),
            'disadvantage.max' => __("front.disadvantage_max_chars", ['number' => config('cms.product.reviews.limitDisadvantages', 500)]),
            'message.max' => __("front.message_max_chars", ['number' => config('cms.product.reviews.limitMessage', 500)]),
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->messages(),
                    'type' => 'error',
                ],
            ]);
        }

//        $captchaResponse = getRecaptchaResponse($request->get('g-recaptcha-response'));
//        if (is_array($captchaResponse))
//            return response()->json($captchaResponse);

        $product = ProductsItemsId::where('active', 1)
            ->find($request->get('productId', null));

        if (is_null($product))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => __("front.product_not_exist"),
                    'type' => 'warning',
                ],
            ]);

        $message = array_filter([
            'advantage' => config('cms.product.reviews.addAdvantages', true) ? $request->get('advantage', null) : null,
            'disadvantage' => config('cms.product.reviews.addDisadvantages', true) ? $request->get('disadvantage', null) : null,
            'msg' => $request->get('message', null)
        ]);

        ProductsItemsReviews::create([
            'products_item_id' => $product->id,
            'user_id' => auth()->user()->id,
            'message' => $message ?: null,
            'active' => 0,
            'seen' => 0,
            'rating' => $request->get('rating', 1),
            'ip' => $request->ip()
        ]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => __("front.review_moderation"),
                'type' => 'info',
            ],
        ]);
    }

    public function getProductDetails(Request $request)
    {
        $item = ProductsItemsId::where('active', 1)
            ->where('slug', $request->product_slug)
            ->with(['files.file', 'globalName'])
            ->firstOrFail();

        //$toppings = !empty($item->toppings_ids) ? ProductsToppingsId::where('active', 1)->whereIn('id', $item->toppings_ids)->with(['globalName'])->get() : collect();
        //$res = defineProductVariations($item);
        //$res = getProductDefaultVariation($item);
        //dd($res['attributes']);
        //dd(json_decode($res['allProductVariations']));

        $view = view('front.products.productModal', [
            'product' => $item,
            //'toppings' => $toppings
        ])->render();

        echo $view;
    }
}
