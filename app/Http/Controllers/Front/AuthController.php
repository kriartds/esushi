<?php

namespace App\Http\Controllers\Front;

use Illuminate\Support\Facades\Cookie;
use Modules\Orders\Models\Cart;
use App\Models\ComponentsId;
use Modules\SiteUsers\Models\Users;
use Modules\SiteUsers\Models\UsersPasswordReset;
use App\Models\Wish;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Laravel\Socialite\Facades\Socialite;
use Modules\SiteUsers\Models\UsersProviders;

class AuthController extends DefaultController
{

    use AuthenticatesUsers;

    public function login(Request $request)
    {
        if (auth()->check())
            return redirect()->to(customUrl('dashboard'));

        session()->put('lastUrl', customUrl(getPreviousUrl(url()->previous())));

        if ($request->ajax())
            return $this->checkLogin($request);

        $response = [
            'titlePage' => __('front.login')
        ];

        return view('front.auth.login', $response);
    }

    public function register(Request $request)
    {
        if (auth()->check())
            return redirect()->to(customUrl('dashboard'));

        session()->put('lastUrl', customUrl(getPreviousUrl(url()->previous())));

        if ($request->ajax())
            return $this->checkRegister($request);

        $response = [
            'titlePage' => __('front.register')
        ];

        return view('front.auth.register', $response);
    }

    public function resetPassword(Request $request)
    {
        if (auth()->check())
            return redirect()->to(customUrl('dashboard'));

        session()->put('lastUrl', customUrl(getPreviousUrl(url()->previous())));

        if ($request->ajax())
            return $this->checkResetPassword($request);

        abort(404);
    }

    public function confirmResetPassword(Request $request, $lang, $token)
    {

        if (auth()->check())
            return redirect()->to(customUrl('dashboard'));

        $resetUserPass = UsersPasswordReset::whereNotNull('reset_token')
            ->where('reset_token', $token)
            ->first();

        $user = Users::where('email', $resetUserPass->email)->whereNull('provider_id')->first();

        if (is_null($resetUserPass) || is_null($user))
            abort(404);

        if ($request->ajax()) {
            $rules = [
                'password' => 'required|min:4',
                'register_repeat_password' => 'required|same:password',
            ];

            $messages = [
                'password.required' => __("front.pass_required"),
                'password.min' => __("front.pass_min_chars", ['number' => 4]),
                'register_repeat_password.required' => __("front.repeat_pass_required"),
                'register_repeat_password.same' => __("front.repeat_pass_same_pass"),
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'validator' => true,
                    'msg' => [
                        'e' => $validator->messages(),
                        'type' => 'error'
                    ],
                ]);
            }

            $user->update([
                'password' => bcrypt($request->get('password'))
            ]);

            $resetUserPass->update([
                'reset_token' => null
            ]);

            return response()->json([
                'status' => true,
                'redirect' => customUrl('login')
            ]);

        }

        $response = [
            'resetToken' => $resetUserPass->reset_token,
            'titlePage' => __('front.register')
        ];

        return view('front.auth.resetPassword', $response);

    }

    public function checkLogin($request)
    {
        $rules = [
            'login_email' => 'required|email',
            'login_password' => 'required',
        ];

        $messages = [
            'login_email.required' => __("front.email_required"),
            'login_email.email' => __("front.email_valid"),
            'login_password.required' => __("front.pass_required"),
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->messages(),
                    'type' => 'error'
                ],
            ]);
        }

        $item = Validator::make($request->all(), [
            'login_email' => 'required|email',
            'login_password' => 'required',
        ]);

        if ($item->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $item->messages(),
                    'type' => 'error'
                ],
            ]);

        $attempts = [
            'email' => $request->get('login_email'),
            'password' => $request->get('login_password'),
            'active' => 1,
        ];

        if ($this->hasTooManyLoginAttempts($request)) {
            $seconds = $this->limiter()->availableIn(
                $this->throttleKey($request)
            );

            return response()->json([
                'status' => false,
                'msg' => [
                    'message' => __("front.many_login_attempts", ['seconds' => $seconds]),
                    'type' => 'error'
                ],
            ], 423);
        }

        if (!auth()->attempt($attempts, true)) {
            $this->incrementLoginAttempts($request);

            return response()->json([
                'status' => false,
                'msg' => [
                    'message' => __("front.user_not_exist"),
                    'type' => 'error'
                ],
            ]);
        }

        $component = ComponentsId::where('slug', 'users')
            ->where('active', 1)
            ->first();

        if (!is_null($component))
            helpers()->logActions($component, '', '', 'auth');

        $redirect = session()->has('lastUrl') ? urldecode(url(session()->get('lastUrl'))) : customUrl('dashboard');

        if ($request->cookie('cart'))
            Cart::where('user_id', $request->cookie('cart'))
                ->whereNull('auth_user_id')
                ->update([
                    'auth_user_id' => auth()->user()->id
                ]);

        return response()->json([
            'status' => true,
            'msg' => [
                'message' => __("front.login_success"),
                'type' => 'success'
            ],
            'redirect' => $redirect
        ]);

    }

    public function checkRegister($request)
    {
        $rules = [
            'register_email' => "required|email|unique:users,email",
            'register_password' => 'required|min:4',
            'register_repeat_password' => 'required|same:register_password',
            'accept_terms' => 'required'
        ];

        $messages = [
            'register_email.required' => __("front.email_required"),
            'register_email.email' => __("front.email_valid"),
            'register_email.unique' => __("front.email_unique"),
            'register_password.required' => __("front.pass_required"),
            'register_password.min' => __("front.pass_min_chars", ['number' => 4]),
            'register_repeat_password.required' => __("front.repeat_pass_required"),
            'register_repeat_password.same' => __("front.repeat_pass_same_pass"),
            'accept_terms.required' => __("front.accept_terms_required"),
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->messages(),
                    'type' => 'error'
                ],
            ]);
        }

        $user = Users::create([
            'email' => $request->get('register_email', null),
            'password' => bcrypt($request->get('register_password')),
            'name' => null,
            'phone' => null,
            'country_id' => null,
            'region_id' => null,
            'city' => null,
            'address' => null,
            'zip_code' => null,
            'avatar' => null,
            'remember_token' => null,
            'active' => 1,
            'for_logout' => 0,
            'provider' => null,
            'provider_id' => null,
            'accept_terms' => $request->get('accept_terms', null) == 'on' ? 1 : 0
        ]);

        Auth::login($user, true);

        $redirect = session()->has('lastUrl') ? urldecode(url(session()->get('lastUrl'))) : customUrl('dashboard');

        return response()->json([
            'status' => true,
            'msg' => [
                'message' => __("front.register_success"),
                'type' => 'success'
            ],
            'redirect' => $redirect
        ]);

    }

    public function checkResetPassword($request)
    {

        $rules = [
            'email' => 'required|email',
        ];

        $messages = [
            'email.required' => __("front.email_required"),
            'email.email' => 'Email field must contain a valid email address',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->messages(),
                    'type' => 'error'
                ],
            ]);
        }

        $existUser = Users::where('email', $request->get('email'))
            ->whereNull('provider_id')
            ->first();

        if (is_null($existUser))
            return response()->json([
                'status' => false,
                'msg' => [
                    'message' => 'User not exist.',
                    'type' => 'error'
                ]
            ]);

        $passReset = UsersPasswordReset::create([
            'email' => $request->get('email'),
            'reset_token' => str_random(60)
        ]);


        try {
            helpers()->sendMail($existUser->email, $existUser->name, [
                'item' => $existUser,
                'resetPassUrl' => urlencode(customUrl(['password', 'reset-password', $passReset->reset_token]))
            ], 'resetPassword', parent::globalSettings(), __('front.resset_password_email_subject'));
        } catch (\Exception $e) {
        }

        $redirect = customUrl();

        return response()->json([
            'status' => true,
            'msg' => [
                'message' => 'Successful send email!',
                'type' => 'success'
            ],
            'redirect' => $redirect
        ]);

    }

    public function logout()
    {
        auth()->logout();
        return redirect()->to(customUrl());
    }

    public function setSocialEmail(Request $request)
    {

        $userData = $request->cookie('loginUser');
        if (!$userData || $userData && !is_object(json_decode($userData)))
            abort(404);

        if (auth()->check())
            return redirect()->to(customUrl('dashboard'));

        session()->put('lastUrl', customUrl(getPreviousUrl(url()->previous())));

        if ($request->ajax())
            return $this->checkSetSocialEmail($request);

        $response = [
            'titlePage' => 'Add email'
        ];

        return view('front.auth.setSocialEmail', $response);

    }

    public function checkSetSocialEmail($request)
    {
        $userData = $request->cookie('loginUser');
        if (!$userData || $userData && !is_object(json_decode($userData)) || auth()->check())
            abort(404);

        $userData = json_decode($userData);

        $rules = [
            'email' => "required|email|unique:users,email",
        ];

        $messages = [
            'email.required' => __("front.email_required"),
            'email.email' => __("front.email_valid"),
            'email.unique' => __("front.email_exist_go"),
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'message' => $validator->messages(),
                    'type' => 'error'
                ],
            ]);
        }

        $userData->email = $request->get('email');

        $authUser = $this->createUserWithProviders($userData, $userData->provider, false);
        Cookie::queue(Cookie::forget('loginUser'));
        Auth::login($authUser, true);

        return response()->json([
            'status' => true,
            'msg' => [
                'message' => __("front.email_added"),
                'type' => 'info'
            ],
            'redirect' => session()->has('lastUrl') ? urldecode(url(session()->get('lastUrl'))) : customUrl()
        ]);
    }

    public function socialLogin($lang, $social)
    {

        if (strpos(url()->previous(), 'admin') === false)
            session()->put('lastUrl', customUrl(getPreviousUrl(url()->previous())));
        else
            session()->put('lastUrl', customUrl('/'));

        return Socialite::driver($social)->redirect();

    }

    public function handleProviderCallback(Request $request, $lang, $provider)
    {

        if ($request->get('error') && $request->get('error_code') == 200)
            return redirect()->intended(session()->has('lastUrl') ? urldecode(url(session()->get('lastUrl'))) : customUrl());

        $user = Socialite::driver($provider)->user();

        if (!@$user->email) {
            $existProvider = UsersProviders::where('provider_id', $user->id)
                ->where('provider', $provider)
                ->first();

            if ($existProvider && $existProvider->user) {
                Auth::login($existProvider->user, true);
                return redirect()->intended(session()->has('lastUrl') ? urldecode(url(session()->get('lastUrl'))) : customUrl());
            }

            $userData = [
                'name' => $user->name ?: null,
                'provider' => $provider,
                'id' => $user->id
            ];

            Cookie::queue('loginUser', json_encode($userData), 24);

            return redirect()->intended(customUrl(['login', 'set-email']));
        }

        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);

        return redirect()->intended(session()->has('lastUrl') ? urldecode(url(session()->get('lastUrl'))) : customUrl());
    }

    public function findOrCreateUser($user, $provider)
    {

        if (!@$user->email) {
            $authUser = Users::whereHas('providers', function ($q) use ($user, $provider) {
                $q->where('provider_id', $user->id);
                $q->where('provider', $provider);
            })->first();

            if ($authUser)
                return $authUser;
            else
                $this->createUserWithProviders($user, $provider);
        } else {
            $authUser = Users::where('email', @$user->email)->first();

            if (is_null($authUser)) {
                $authUser = Users::create([
                    'name' => $user->name ?: null,
                    'email' => $user->email ?: null,
                    'active' => 1,
                    'accept_terms' => 1
                ]);

                UsersProviders::create([
                    'user_id' => $authUser->id,
                    'provider' => $provider,
                    'provider_id' => $user->id,
                ]);

                $this->uploadUserAvatar($user, $authUser);

            } else {
                $existProvider = $authUser->providers()
                    ->where('provider_id', $user->id)
                    ->where('provider', $provider)
                    ->first();

                if (!$existProvider)
                    UsersProviders::create([
                        'user_id' => $authUser->id,
                        'provider' => $provider,
                        'provider_id' => $user->id,
                    ]);

                if (!$authUser->avatar || ($authUser->avatar && !file_exists($authUser->avatar)))
                    $this->uploadUserAvatar($user, $authUser);
            }
        }

        return $authUser;
    }

    private function uploadUserAvatar($user, $authUser)
    {
        try {
            $destinationPath = 'files/' . date('Y-m-d') . '/images';

            if (!file_exists($destinationPath))
                File::makeDirectory($destinationPath, 0775, true, true);

            $path = !is_null(@$user->avatar_original) ? @$user->avatar_original : (!is_null(@$user->getAvatar()) ? @$user->getAvatar() : '');
            $fileName = str_slug($user->getName() . '-' . $user->getId()) . '.jpg';
            $fileLocation = "{$destinationPath}/{$fileName}";
            Image::make($path)->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($fileLocation);

            $authUser->avatar = $fileLocation;
            $authUser->save();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function createUserWithProviders($user, $provider, $withAvatar = false)
    {
        $authUser = Users::create([
            'name' => @$user->name ?: null,
            'email' => @$user->email ?: null,
            'active' => 1,
            'accept_terms' => 1
        ]);

        UsersProviders::create([
            'user_id' => $authUser->id,
            'provider' => $provider,
            'provider_id' => @$user->id,
        ]);

        if ($withAvatar)
            $this->uploadUserAvatar($user, $authUser);

        return $authUser;
    }

}
