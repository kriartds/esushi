<?php

namespace App\Http\Controllers\Admin;

use App\Models\Languages;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LabelsController extends DefaultController
{
    private $path;
    private $filesSystem;

    public function __construct()
    {
        $this->path = 'admin.labels';
        $this->filesSystem = new Filesystem();
    }

    public function index(Request $request)
    {

        $response = $this->filter($request);

        $response['path'] = $this->path;

        return view("{$this->path}.list", $response);
    }

    public function filter(Request $request)
    {

        $filterTableList = filterTableList($request, $request->except('page'));
        $filterParams = $filterTableList->filterParams;
        $pushUrl = $filterTableList->pushUrl;

        $items = collect();
        $countItems = 0;

        $findResultsKeys = [];
        if (array_key_exists('title', $filterParams) && !is_array($filterParams['title'])) {
            $allLangPaths = resource_path("lang");
            if ($this->filesSystem->exists($allLangPaths)) {
                $allFilesFromAllPath = $this->filesSystem->allFiles($allLangPaths);

                if (!empty($allFilesFromAllPath)) {
                    foreach ($allFilesFromAllPath as $value) {
                        $requireFiles = array_filter($this->filesSystem->getRequire($value), function ($q) use ($filterParams) {
                            if (is_array($q)) {
                                if (empty($q))
                                    return false;

                                foreach ($q as $val) {
                                    if (empty($q))
                                        return false;

                                    if (is_array($val))
                                        foreach ($val as $v) {
                                            return strpos(strtolower($v), strtolower($filterParams['title'])) !== false;
                                        }
                                    else
                                        return strpos(strtolower($val), strtolower($filterParams['title'])) !== false;

                                }
                            } else
                                return strpos(strtolower($q), strtolower($filterParams['title'])) !== false;

                            return true;
                        });

                        $findResultsKeys[] = array_keys($requireFiles);
                    }
                }
            }
        }

        if (!empty($findResultsKeys))
            $findResultsKeys = array_unique(array_filter(array_flatten($findResultsKeys)));

        $currLangPath = resource_path("lang/" . LANG);

        if ($this->filesSystem->exists($currLangPath)) {
            $allFilesFromPath = $this->filesSystem->files($currLangPath);

            if (!empty($allFilesFromPath))
                foreach ($allFilesFromPath as $value) {
                    $requireFiles = $this->filesSystem->getRequire($value);
                    $fileName = getCleanFileName($value->getBasename());
                    $tempRequireFiles = [];

                    if (array_key_exists('title', $filterParams) && !is_array($filterParams['title'])) {
                        foreach ($findResultsKeys as $findResultsKey) {
                            if (array_key_exists($findResultsKey, $requireFiles))
                                $tempRequireFiles[$findResultsKey] = @$requireFiles[$findResultsKey] ?: '';
                        }

                        $requireFiles = $tempRequireFiles;
                    }

                    if (auth()->guard('admin')->user()->root || !auth()->guard('admin')->user()->root && ($fileName == 'e' || $fileName == 'front')) {
                        $items->put($fileName == 'e' ? 'admin' : $fileName, $requireFiles);

                        $countItems += count($requireFiles, true);
                    }
                }
        }

        if ($items->isNotEmpty())
            $items = formatArrayRecursive($items);

        $items = $items->filter();

        $response = [
            'status' => true,
            'count' => $countItems,
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
                abort(503);
            }

            return response()->json($response);
        }

        return $response;
    }

    public function edit(Request $request, $id, $langId)
    {

        $editLang = Languages::where('active', 1)->findOrFail($langId);
        $id = $id == 'admin' ? 'e' : $id;
        $currLangFile = resource_path("lang/{$editLang->slug}/{$id}.php");
        $editKeyParent = $request->get('parent');
        $editKey = $request->get('item');



        if (!$this->filesSystem->exists($currLangFile))
            abort(404);

        $currLangFileContent = $this->filesSystem->getRequire($currLangFile);

        if (empty($currLangFileContent))
            abort(404);

        $currValue = null;

        if (!is_null($editKeyParent)) {
            $tempParent = @$currLangFileContent[$editKeyParent];
            if (!$tempParent)
                abort(404);

            $currValue = @$tempParent[$editKey];

            if (!$currValue && is_array($tempParent))
                foreach ($tempParent as $item) {
                    if (!@$item[$editKey])
                        continue;

                    $currValue = @$item[$editKey];
                }


        } else
            $currValue = @$currLangFileContent[$editKey];



//        if (!$currValue)
//            abort(404);

        $item = collect([
            'id' => $id,
            'parent' => $editKeyParent,
            'child' => $editKey,
            'value' => $currValue
        ]);

        $response = [
            'path' => $this->path,
            'label' => $item,
            'langId' => $langId,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function save(Request $request, $id, $langId)
    {

        $rules = [
            'lang' => 'required',
//            'title' => 'required',
        ];

        $item = Validator::make($request->all(), $rules);

        if ($item->fails()) {
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $item->messages(),
                    'type' => 'error'
                ],
            ]);
        }


        $lang = Languages::where('active', 1)
            ->find($request->get('lang'));

        if (is_null($lang)) {
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("e.lang_not_exist")],
                    'type' => 'warning'
                ]
            ]);
        }

        $langSlug = $lang->slug;


        $currLangFile = resource_path("lang/{$langSlug}/{$id}.php");
        $editKeyParent = $request->get('parent');
        $editKey = $request->get('item');
        $newTitle = $request->get('title', '');
        if (is_null($newTitle)){
            $newTitle = '';
        }


        if (!$this->filesSystem->exists($currLangFile))
            abort(404);

        $currLangFileContent = $this->filesSystem->getRequire($currLangFile);

        if (empty($currLangFileContent))
            abort(404);

        $currValue = null;

        if (!is_null($editKeyParent)) {
            $tempParent = @$currLangFileContent[$editKeyParent];
            if (!$tempParent)
                abort(404);

            if (!@$tempParent[$editKey])
                abort(404);

            @$currLangFileContent[$editKeyParent][$editKey] = $newTitle;

            if (!@$tempParent[$editKey] && is_array($tempParent))
                foreach ($tempParent as $k => $item) {
                    if (!@$currLangFileContent[$editKeyParent][$k][$editKey])
                        abort(404);

                    $currLangFileContent[$editKeyParent][$k][$editKey] = $newTitle;
                }
        } else {

            //if (is_null(@$currLangFileContent[$editKey]))
            //    abort(404);

            $currLangFileContent[$editKey] = $newTitle;
        }

        $newFileContent = "<?php \r\n\r\n return " . varExport($currLangFileContent, true) . ";" . PHP_EOL;
        $this->filesSystem->put($currLangFile, $newFileContent);

        $editedItemName = $editKeyParent ? "{$editKeyParent} | {$editKey}" : $editKey;

        helpers()->logActions(parent::currComponent(), [], $editedItemName, 'edit');

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => __("e.successful_edited", ['name' => $editedItemName]),
                'type' => 'success'
            ],
        ]);
    }

}