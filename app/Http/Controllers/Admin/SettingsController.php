<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\Helpers;
use App\Http\Requests\SettingsValidatiion;
use Illuminate\Filesystem\Filesystem;
use Modules\Pages\Models\PagesId;
use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Nwidart\Modules\Facades\Module;

class SettingsController extends DefaultController
{

    private $path;


    public function __construct()
    {
        $this->path = 'admin.settings';

    }

    public function index()
    {


        $items = Settings::get()->keyBy('slug');

        $fileSystem = new Filesystem();
        if (Module::has('Pages') && $fileSystem->exists(Module::getPath() . '/Pages/Models/PagesId.php'))
            $pages = PagesId::where('active', 1)->with('globalName')->get();
        else
            $pages = collect();

        $response = [
            'items' => $items,
            'path' => $this->path,
            'pages' => $pages,
            'working_hours' => isset($items['working_hours']->value) && !empty($items['working_hours']->value) ? json_decode($items['working_hours']->value, true) : []
        ];

        return view("{$this->path}.createEdit", $response);

    }

    public function save(SettingsValidatiion $request)
    {

        $currComponent = parent::currComponent();

        $allSettings = Settings::get();

        if (!$allSettings->isEmpty())
            foreach ($allSettings as $allSetting) {
                if (!in_array($allSetting->slug, array_keys($request->all())))
                    $allSetting->delete();
            }

        foreach ($request->all() as $key => $item) {
            $setting = Settings::updateOrCreate([
                'slug' => $key
            ], [
                'value' => is_array($item) ? json_encode($item) : $item
            ]);

            if (strpos($key, 'files') !== false)
                helpers()->uploadFiles($request->get($key), $setting->id, $currComponent->id, 'edit');
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => __("e.settings_successful_edited"),
                'type' => 'success'
            ],
            'redirect' => adminUrl([$currComponent->slug])
        ]);
    }

    public function testEmail(Request $request) {
        $test =  \helpers()->testEmail($request->get('config_email'),$request->get('config_email_host'), $request->get('config_email_port'),$request->get('config_email_pass'));

        if (!$test) {
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => __("e.settings_wrong_email"),
                    'type' => 'error'
                ],
            ]);
        }else{
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => 'Success test',
                    'type' => 'success'
                ],
            ]);
        }
    }
}