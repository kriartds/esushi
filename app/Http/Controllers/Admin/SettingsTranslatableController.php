<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\Helpers;
use App\Models\Languages;
use App\Models\SettingsTrans;
use App\Models\SettingsTransId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SettingsTranslatableController extends DefaultController
{

    private $path;


    public function __construct()
    {
        $this->path = 'admin.settings-trans';

    }

    public function index()
    {

        $items = SettingsTransId::with('itemByLang')->get()->keyBy('slug');

        $response = [
            'items' => $items,
            'path' => $this->path,
        ];

        return view("{$this->path}.createEdit", $response);

    }

    public function save(Request $request)
    {

        $currComponent = parent::currComponent();
        $rules = [];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->messages(),
                    'type' => 'error'
                ],
            ]);

        if (empty($request->all()))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("e.min_one_field_settings")],
                    'type' => 'warning'
                ]
            ]);


        $lang = Languages::where('active', 1)->find($request->get('lang'));

        if (is_null($lang))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Language not exist'],
                    'type' => 'warning'
                ]
            ]);

        $items = SettingsTransId::get();

        if ($items->isNotEmpty())
            foreach ($items as $item) {
                if (!in_array($item->slug, array_keys($request->all())))
                    $item->delete();
            }

        foreach ($request->except('lang', 'langId') as $key => $item) {

            $settingId = SettingsTransId::updateOrCreate([
                'slug' => $key
            ]);

            SettingsTrans::updateOrCreate([
                'setting_trans_id' => $settingId->id,
                'lang_id' => $lang->id
            ], [
                'value' => is_array($item) ? json_encode($item) : $item
            ]);

            if (strpos($key, 'files') !== false)
                helpers()->uploadFiles($request->get($key), $settingId->id, $currComponent->id, 'edit', $lang->id);
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => __("e.settings_successful_edited"),
                'type' => 'success'
            ],
            'redirect' => adminUrl([$currComponent->slug]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')
        ]);
    }
}