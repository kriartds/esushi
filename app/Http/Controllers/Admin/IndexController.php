<?php

namespace App\Http\Controllers\Admin;

use Modules\Orders\Models\Cart;
use App\Repositories\ComponentsIdRepository;
use Modules\Products\Models\ProductsItemsReviews;
use Modules\Reviews\Models\Reviews;
use Carbon\Carbon;
//use Analytics;
use Illuminate\Filesystem\Filesystem;
use Nwidart\Modules\Facades\Module;
//use Spatie\Analytics\Period;

class IndexController extends DefaultController
{

    public function index()
    {
        $defaultCurrency = getDefaultCurrency();
        $fileSystem = new Filesystem();
        $carbon = new Carbon();

        $reviews = collect();
        $productsReviews = collect();
        $orders = collect();

        $reviewsCount = $productsReviewsCount = 0;

        if (Module::has('Reviews') && $fileSystem->exists(module_path('Reviews') . '/Models/Reviews.php') && $this->checkUserRightsToModule('reviews')) {
            $reviews = Reviews::where('seen', 0)->limit(10)->get();
            $reviewsCount = Reviews::where('seen', 0)->count();
        }

        if (Module::has('Products') && $fileSystem->exists(module_path('Products') . '/Models/ProductsItemsReviews.php') && $this->checkUserRightsToModule('products')) {
            $productsReviews = ProductsItemsReviews::where('seen', 0)->limit(10)->with('user')->get();
            $productsReviewsCount = ProductsItemsReviews::where('seen', 0)->count();
        }

        $componentsIdRepo = new ComponentsIdRepository();
        $statistics = $componentsIdRepo->all(['active' => 1, 'for_statistics' => 1], 0, ['globalName', 'file.file'], ['componentsWithPermissions']);
        $filteredStatistics = collect();
        if ($statistics->isNotEmpty())
            $statistics->map(function ($item) use ($filteredStatistics) {
                if ($countForWidgets = $item->componentsCountForWidgets) {
                    $item->countForWidgets = $countForWidgets;
                    $filteredStatistics->push($item);
                }
            });

        $startOrdersMonth = $carbon->now()->startOfMonth();
        $endOrdersMonth = $carbon->now()->endOfMonth();

        if (Module::has('Orders') && $fileSystem->exists(module_path('Orders') . '/Models/CartProducts.php') && $this->checkUserRightsToModule('orders'))
            $orders = Cart::where('is_draft', 0)
                ->whereDate('order_date', '>=', $startOrdersMonth)
                ->whereDate('order_date', '<=', $endOrdersMonth)
                ->get()
                ->groupBy('status');

        $ordersLabels = [];
        $ordersPaidAmount = 0;
        $orderWaitingPaymentAmount = 0;

        if ($orders->isNotEmpty())
            foreach ($orders as $key => $order) {
                $ordersLabels[trans("e.{$key}")]['count'] = $order->count();

                if ($key == 'checkout') {
                    $ordersLabels[trans("e.{$key}")]['color'] = '#17a2b8';

                    $orderWaitingPaymentAmount += $order->sum(function ($item) {
                        return $item->amount + $item->delivery_amount;
                    });
                } elseif ($key == 'cancelled')
                    $ordersLabels[trans("e.{$key}")]['color'] = '#dc3545';
                elseif ($key == 'approved') {
                    $ordersLabels[trans("e.{$key}")]['color'] = '#007bff';

                    $orderWaitingPaymentAmount += $order->sum(function ($item) {
                        return $item->total;
                    });
                } else {
                    $ordersLabels[trans("e.{$key}")]['color'] = '#28a745';

                    $ordersPaidAmount += $order->sum(function ($item) {
                        return $item->total;
                    });

                }
            }


//        try {
//
//            $analyticsTotalVisitorsAndPageViews = Analytics::performQuery(
//                Period::months(1),
//                'ga:users,ga:pageviews',
//                ['dimensions' => 'ga:date']
//            );
//
//            $analyticsTotalVisitorsAndPageViews = collect($analyticsTotalVisitorsAndPageViews['rows'] ?? [])->map(function (array $dateRow) {
//                return [
//                    'date' => Carbon::createFromFormat('Ymd', $dateRow[0])->format('d.m.y'),
//                    'visitors' => (int)$dateRow[1],
//                    'pageViews' => (int)$dateRow[2],
//                ];
//            });
//
//            $analyticsMostVisitedPages = Analytics::fetchMostVisitedPages(Period::months(1));
//            $analyticsTopReferrers = Analytics::fetchTopReferrers(Period::months(1));
//            $analyticsUserTypes = Analytics::fetchUserTypes(Period::months(1));
//            $analyticsTopBrowsers = Analytics::fetchTopBrowsers(Period::years(1));
//
//        } catch (\Exception $e) {
//            $analyticsTotalVisitorsAndPageViews = $analyticsMostVisitedPages = $analyticsTopReferrers = $analyticsUserTypes = $analyticsTopBrowsers = collect();
//        }

        $response = [
            'reviews' => $reviews,
            'statistics' => $filteredStatistics,
            'productsReviews' => $productsReviews,
            'reviewsCount' => $reviewsCount,
            'productsReviewsCount' => $productsReviewsCount,
            'ordersLabels' => $ordersLabels,
            'ordersPaidAmount' => $ordersPaidAmount,
            'orderWaitingPaymentAmount' => $orderWaitingPaymentAmount,
            'startOrdersMonth' => $startOrdersMonth,
            'endOrdersMonth' => $endOrdersMonth,
//            'analyticsTotalVisitorsAndPageViews' => @$analyticsTotalVisitorsAndPageViews,
//            'analyticsMostVisitedPages' => @$analyticsMostVisitedPages,
//            'analyticsTopReferrers' => @$analyticsTopReferrers,
//            'analyticsUserTypes' => @$analyticsUserTypes,
//            'analyticsTopBrowsers' => @$analyticsTopBrowsers,
            'analyticsSubMoth' => $carbon->now()->subMonth(),
            'analyticsSubYear' => $carbon->now()->subYear(),
            'analyticsCurrDate' => $carbon->now(),
            'defaultCurrency' => $defaultCurrency
        ];

        return view('admin.index', $response);

    }
}
