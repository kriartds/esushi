<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\Helpers;
use App\Repositories\ComponentsIdRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    use AuthenticatesUsers;

    public function login()
    {

        if (auth()->guard('admin')->check())
            return redirect()->to(adminUrl());

        session()->put('adminLastUrl', url(LANG, getPreviousUrl(url()->previous())));

        return view('admin.auth.login');
    }

    public function checkLogin(Request $request)
    {

        $item = Validator::make($request->all(), [
            'login' => 'required',
            'password' => 'required',
        ]);

        if ($item->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $item->messages(),
                    'type' => 'error'
                ],
            ]);

        $attempts = [
            'login' => $request->get('login'),
            'password' => $request->get('password'),
            'active' => 1,
        ];

        if ($this->hasTooManyLoginAttempts($request)) {
            $seconds = $this->limiter()->availableIn(
                $this->throttleKey($request)
            );

            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => __("e.many_login_attempts", ['seconds' => $seconds]),
                    'type' => 'error'
                ],
            ], 423);
        }

        if (!auth()->guard('admin')->attempt($attempts, $request->get('remember', null) == 'on')) {
            $this->incrementLoginAttempts($request);

            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => __("e.user_exist"),
                    'type' => 'error'
                ],
            ]);
        }

        $componentIdRepo = new ComponentsIdRepository();

        $component = $componentIdRepo->first([
            'slug' => 'users',
            'active' => 1
        ]);

            helpers()->logActions($component, '', 'admin', 'auth');

        if (auth()->guard('admin')->check()) {
            $expiresAt = Carbon::now()->addMinutes(config('session.lifetime'));
            $loggedUsers = Cache::get('user-is-online-' . auth()->guard('admin')->user()->id);

            Cache::put('user-is-online-' . auth()->guard('admin')->user()->id, $loggedUsers && is_numeric($loggedUsers) ? $loggedUsers + 1 : 1, $expiresAt);
        }

        if (session()->has('adminLastUrl')) {
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => __("e.login_success"),
                    'type' => 'success'
                ],
                'redirect' => urldecode(url(session()->get('adminLastUrl'))),
            ]);
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => __("e.login_success"),
                'type' => 'success'
            ],
            'redirect' => adminUrl()
        ]);

    }

    public function logout()
    {

        $authUsers = Cache::get('user-is-online-' . @auth()->guard('admin')->user()->id);

        if ($authUsers > 1) {
            $expiresAt = Carbon::now()->addMinutes(config('session.lifetime'));
            Cache::put('user-is-online-' . @auth()->guard('admin')->user()->id, $authUsers - 1, $expiresAt);
        } else
            Cache::forget('user-is-online-' . @auth()->guard('admin')->user()->id);

        auth()->guard('admin')->logout();

        return redirect()->to(adminUrl(['auth', 'login']));
    }

}
