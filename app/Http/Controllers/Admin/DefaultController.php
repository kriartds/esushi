<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\Helpers;
use App\Models\AdminUsersPermissions;
use App\Repositories\SettingsRepository;
use App\Repositories\SettingsTransIdRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class DefaultController extends RoleManager
{

    static $allSettings;

    public function currComponent()
    {

        $currComponent = $request = null;

        if (request()->segment(3) && request()->segment(2) === 'admin')
            $request = request()->segment(3);
        elseif (request()->segment(2) && request()->segment(2) !== 'admin')
            $request = request()->segment(2);

        if ($request)
            $currComponent = Helpers::getComponentIdBySlug($request, true);

        return $currComponent;
    }

    public function hideMenu(Request $request)
    {

        if ($request->get('isClose')) {
            if (!is_null(Cookie::get('sidebar'))) {
                Cookie::queue(Cookie::forget('sidebar'));
            }

            Cookie::queue('sidebar', $request->get('isClose'), 45000);

            return response()->json([
                'status' => true
            ]);
        }

        Cookie::queue(Cookie::forget('sidebar'));

        return response()->json([
            'status' => false,
            'isClose' => $request->get('isClose')
        ]);
    }

    public function globalSettings()
    {
        $settingsRepo = new SettingsRepository();
        $settingsTransIdRepo = new SettingsTransIdRepository();

        if (!isset(static::$allSettings) || is_null(static::$allSettings))
            static::$allSettings = (object)[
                'simple' => $settingsRepo->all(),
                'trans' => $settingsTransIdRepo->all([], 0, ['globalName'])
            ];

        return static::$allSettings;
    }

    public function createDestroyRepeatField(Request $request)
    {
        return Helpers::createDestroyRepeatField($request);
    }

    protected function checkUserRightsToModule($moduleSlug)
    {
        $productsUserPermissions = AdminUsersPermissions::where('admin_user_group_id', auth()->guard('admin')->user()->admin_user_group_id)
            ->whereHas('componentId', function ($q) use ($moduleSlug) {
                $q->where('slug', $moduleSlug);
            })
            ->first();

        return !is_null($productsUserPermissions);
    }

    public function checkUserNotifications(Request $request)
    {

        if (!$request->ajax())
            abort(404);

        $authUser = auth()->guard('admin')->user();
        $unreadNotifications = $authUser->unreadNotifications->sortByDesc('created_at')->take(3);

        try {
            $view = view('admin.templates.notifications', ['items' => $unreadNotifications])->render();
        } catch (\Throwable $e) {
            $view = '';
        }

        return response()->json([
            'status' => true,
            'notifications' => $view
        ]);
    }

    public function markNotificationAsRead(Request $request)
    {

        if (!$request->ajax())
            abort(404);

        $notificationId = $request->get('id', null);

        $authUser = auth()->guard('admin')->user();
        $unreadNotification = $authUser->unreadNotifications()->find($notificationId);

        if (is_null($unreadNotification))
            return response()->json([
                'status' => true,
            ]);

        $unreadNotification->markAsRead();

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => [__("e.notification_was_mark_read")],
                'type' => 'info'
            ]
        ]);
    }
}
