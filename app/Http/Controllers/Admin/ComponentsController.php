<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\Helpers;
use App\Models\AdminUsersPermissions;
use App\Models\ComponentsId;
use App\Models\Languages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Nwidart\Modules\Facades\Module;

class ComponentsController extends DefaultController
{

    private $path;


    public function __construct()
    {
        $this->path = 'admin.components';

    }

    public function index(Request $request)
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = ComponentsId::where(function ($q) use ($request) {
            if ($request->get('parent'))
                $q->where('p_id', $request->get('parent'));
            else
                $q->whereNull('p_id');
        })
            ->componentsWithPermissions()
            ->with(['globalName', 'items', 'children' => function ($q) {
                $q->withTrashed();
            }])
            ->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path
        ];

        return view("{$this->path}.list", $response);

    }

    public function create()
    {

        $parents = ComponentsId::whereNull('p_id')
            ->componentsWithPermissions()
            ->with(['globalName', 'recursiveChildren', 'recursiveChildren.globalName'])
            ->get();

        $response = [
            'path' => $this->path,
            'parents' => $parents
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id, $langId)
    {

        $item = ComponentsId::with('files.file')->findOrFail($id);

        $parents = ComponentsId::whereNull('p_id')
            ->componentsWithPermissions()
            ->with(['globalName', 'recursiveChildren', 'recursiveChildren.globalName'])
            ->get();

        $response = [
            'path' => $this->path,
            'item' => $item,
            'parents' => $parents,
            'langId' => $langId
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = ComponentsId::componentsWithPermissions()->onlyTrashed()->with(['globalName'])->get();

        $response = [
            'items' => $items
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(Request $request, $id, $langId)
    {

        $currComponent = parent::currComponent();

        $rules = [
            'lang' => 'required',
            'name' => 'required',
            'slug' => "required|unique:components_id,slug,{$id}",
            'name_category' => "nullable|unique:components,name_category,{$id}",
            'controller' => "required|not_in:controller|not_in:Controller|min:10|unique:components_id,controller,{$id}",
        ];

        $item = Validator::make($request->all(), $rules);

        if ($item->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $item->messages(),
                    'type' => 'error'
                ],
            ]);

        $authUser = auth()->guard('admin')->user();

        $parent = ComponentsId::where('active', 1)
            ->componentsWithPermissions()
            ->find($request->get('parent'));

        if (is_null($parent) && !is_null($request->get('parent')))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("e.component_not_exist")],
                    'type' => 'warning'
                ]
            ]);

        if (is_null($id) && is_null($langId)) {
            $lang = Languages::where('active', 1)
                ->find($request->get('lang'));

            if (is_null($lang))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => [__("e.lang_not_exist")],
                        'type' => 'warning'
                    ]
                ]);

            $langId = $lang->id;
        }

        $item = ComponentsId::find($id);

        if (is_null($item)) {
            $item = new ComponentsId();

            $nextPosition = Helpers::getNextItemPosition($item);
            $item->position = $nextPosition;
            $item->active = 1;
        } else {

            if (!$authUser->group->isRoot && $item->for_root)
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => [__("e.cannot_edit_item")],
                        'type' => 'warning'
                    ]
                ]);
        }


        $nextLevel = Helpers::getNextItemLevel(@$parent->id, $item);

        $item->p_id = @$parent->id;
        $item->slug = $request->get('slug', null);
        $item->module = $request->get('module', null);
        $item->controller = $request->get('controller', null);
        $item->level = $nextLevel;

        $item->for_count = $request->get('for_count') == 'on' ? 1 : 0;
        $item->for_sidebar = $request->get('for_sidebar') == 'on' ? 1 : 0;
        $item->for_root = $request->get('for_root') == 'on' ? 1 : 0;
        $item->for_menu = $request->get('for_menu') == 'on' ? 1 : 0;
        $item->for_statistics = $request->get('for_statistics') == 'on' ? 1 : 0;
        $item->for_sitemap = $request->get('for_sitemap') == 'on' ? 1 : 0;

        $item->save();

        $item->itemByLang()->updateOrCreate([
            'component_id' => $item->id,
            'lang_id' => $langId
        ], [
            'name' => $request->get('name', null),
            'name_category' => $request->get('name_category', null)
        ]);

        if ($authUser->root && $authUser->group->isRoot)
            AdminUsersPermissions::create([
                'admin_user_group_id' => $authUser->group->id,
                'component_id' => $item->id,
                'for_create' => 1,
                'for_edit' => 1,
                'for_delete' => 1,
                'for_view' => 1,
                'for_active' => 1
            ]);

        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => __("e.successful_created"),
                    'type' => 'success'
                ],
                'redirect' => adminUrl([$currComponent->slug]) . (!is_null(@$parent->id) ? '?parent=' . @$parent->id : '')
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => __("e.successful_edited", ['name' => $item->globalName->name]),
                    'type' => 'success'
                ],
                'redirect' => adminUrl([$currComponent->slug, 'edit', $item->id, $langId]) . (!is_null(@$parent->id) ? '?parent=' . @$parent->id : '')
            ]);
    }

    public function destroy(Request $request)
    {

        $deletedItemsId = substr($request->get('id'), 1, -1);

        if (empty($deletedItemsId))
            return response()->json([
                'status' => false
            ]);

        if ($request->get('event') != 'to-trash' && $request->get('event') != 'from-trash' && $request->get('event') != 'restore')
            return response()->json([
                'status' => false
            ]);

        $deletedItemsIds = explode(',', $deletedItemsId);
        $currComponent = parent::currComponent();

        if ($request->get('event') == 'to-trash') {
            $items = ComponentsId::componentsWithPermissions()->whereIn('id', $deletedItemsIds)->with(['globalName', 'children' => function ($q) {
                $q->withTrashed();
            }])->get();
        } else {
            $items = ComponentsId::componentsWithPermissions()->onlyTrashed()->whereIn('id', $deletedItemsIds)->with(['globalName', 'children' => function ($q) {
                $q->withTrashed();
            }])->get();
        }

        $cartMessage = $responseMsg = '';

        if (!$items->isEmpty()) {
            foreach ($items as $item) {

                if (!auth()->guard('admin')->user()->root && $item->for_root || $item->children->isNotEmpty())
                    return response()->json([
                        'status' => false
                    ]);

                if (!is_null($item->globalName))
                    $cartMessage .= $item->globalName->name . ', ';

                if ($request->get('event') == 'to-trash' && !$item->trashed()) {
                    $item->delete();
                    $responseMsg = !empty($cartMessage) ? __("e.items_added_to_trash", ['items' => substr($cartMessage, 0, -2)]) : '';
                    helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-to-trash');
                } elseif ($request->get('event') == 'from-trash') {
                    $item->files()->delete();
                    $item->permissions()->delete();
                    recursiveChildrenDestroy($item);
                    $item->forceDelete();
                    $responseMsg = !empty($cartMessage) ? __("e.items_removed_from_trash", ['items' => substr($cartMessage, 0, -2)]) : '';
                    helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-from-trash');
                } elseif ($request->get('event') == 'restore') {
                    $item->restore();
                    $responseMsg = !empty($cartMessage) ? __("e.items_restored_from_trash", ['items' => substr($cartMessage, 0, -2)]) : '';
                    helpers()->logActions($currComponent, $item, @$item->globalName->name, 'restored-from-trash');
                }

            }

            return response()->json([
                'status' => true,
                'cart_messages' => $responseMsg,
                'items' => $deletedItemsIds
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }

    public function updatePosition(Request $request)
    {

        $order = $request->get('order');

        if (!is_array($order))
            return response()->json([
                'status' => false
            ]);

        $itemsPositions = [];
        foreach ($order as $id) {
            $item = ComponentsId::componentsWithPermissions()->find($id);
            $itemsPositions[] = @$item->position;
        }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($order as $key => $id) {
            ComponentsId::where('id', $id)->update(['position' => $itemsPositions[$key]]);
        }

        return response()->json([
            'status' => true
        ]);
    }

    public function activateItem(Request $request)
    {

        $item = ComponentsId::componentsWithPermissions()->findOrFail($request->get('id'));
        $currComponent = parent::currComponent();

        if ($item->for_root)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("e.cannot_activate_inactivate_item")],
                    'type' => 'warning'
                ]
            ]);

        if ($request->get('active')) {
            $status = 0;
            $msg = __("e.element_is_inactive", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("e.element_is_active", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'activate');
        }

        $item->update(['active' => $status]);

        if ($item->module) {
            $module = Module::find($item->module);
            if ($module) {
                if ($item->active)
                    $module->enable();
                else
                    $module->disable();
            }
        }


        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

}