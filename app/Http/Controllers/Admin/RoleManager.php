<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\ComponentsIdRepository;
use App\Repositories\LanguagesRepository;
use Nwidart\Modules\Facades\Module;

class RoleManager extends Controller
{

    public function routeResponder($lang, $component, $function = 'index', $id = null, $langId = null)
    {
        $componentsIdRepo = new ComponentsIdRepository();
        $languagesRepo = new LanguagesRepository();
        $authUser = auth()->guard('admin')->user();

        $items = $componentsIdRepo->all(['active' => 1], 0, [
            'permissions' => function ($q) use ($authUser) {
                $q->where('admin_user_group_id', $authUser->admin_user_group_id);
            }
        ], ['componentsWithPermissions']);

        $item = $items->where('slug', $component)->first();

        $function = camel_case($function);


//        dd($item->controller, $function);
        if (!$item || !$item->controller || $this->hasUserRight($item, $item->for_root, $authUser, $function))
            abort(404);


        if (!is_null($item->p_id) && @helpers()->getInactiveComponentParent($item->p_id, $items))
            abort(404);


        if ($item->module) {
            if (!Module::has($item->module))
                abort(404);


            $moduleRequires = Module::find($item->module)->getRequires();
            if (!empty($moduleRequires))
                foreach ($moduleRequires as $moduleRequire) {
                    if (!Module::findByAlias($moduleRequire))
                        abort(404);
                }
        }
//        dd('ee');
        if($this->checkFunctionRequestMethod($function))
            abort(405);





        if ($item->module)
            $namespace = "Modules\\{$item->module}\\Http\\Controllers\\{$item->controller}";
        else
            $namespace = "App\\Http\\Controllers\\Admin\\{$item->controller}";

        if (!is_null($langId))
            $languagesRepo->findOrFail($langId, ['active' => 1]);

        $response = null;

        try {
            $response = app()->call("{$namespace}@{$function}", [$id, $langId]);
        } catch (\Exception $e) {
            if (!$e instanceof \ReflectionException)
                $response = app()->call("{$namespace}@{$function}", [$id, $langId]);
            else if (method_exists($e, 'getStatusCode'))
                abort($e->getStatusCode());
            else
                abort(404);
        }

        return $response;
    }

    private function hasUserRight($component, $forRoot, $authUser, $function)
    {

        $adminUserPermissions = $component->permissions->first();



        $response = !is_null($adminUserPermissions);

        $forbidden = false;

        if ($forRoot && $authUser->group->isRoot)
            $response = true;

        if (!@$adminUserPermissions->for_create && $function == 'create')
            $forbidden = true;

        if (!@$adminUserPermissions->for_edit && $function == 'edit')
            $forbidden = true;

        if (!@$adminUserPermissions->for_create && !@$adminUserPermissions->for_edit && $function == 'save')
            $forbidden = true;

        if (!@$adminUserPermissions->for_delete && $function == 'destroy')
            $forbidden = true;

        if (!@$adminUserPermissions->for_active && $function == 'activateItem')
            $forbidden = true;

        if (!@$adminUserPermissions->for_view)
            $forbidden = true;

        if ($forbidden)
            abort(403);

        return !$response;

    }

    private function checkFunctionRequestMethod($function)
    {
        $postMethods = [
            'save',
            'destroy',
            'updatePosition',
            'activateItem'
        ];

        if(in_array($function, $postMethods) && request()->method() != "POST")
            return true;

        return false;
    }
}
