<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class SitemapController extends DefaultController
{

    public function index(Request $request)
    {

        if (!$request->ajax())
            abort(404);

        $response = helpers()->generateSitemap();

        return response()->json([
            'status' => $response,
            'type' => $response ? 'info' : 'warning',
            'messages' => [$response ? __('e.sitemap_msg') : __('e.something_wrong')]
        ]);
    }

}