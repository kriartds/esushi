<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\Helpers;
use App\Models\Languages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LanguagesController extends DefaultController
{
    private $path;

    public function __construct()
    {
        $this->path = 'admin.languages';
    }

    public function index()
    {

        $items = Languages::orderBy('position')->get();

        $response = [
            'items' => $items,
            'path' => $this->path
        ];

        return view("{$this->path}.list", $response);

    }

    public function create()
    {

        $response = [
            'path' => $this->path,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id)
    {

        $item = Languages::findOrFail($id);

        $response = [
            'path' => $this->path,
            'item' => $item,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = Languages::onlyTrashed()->with(['files.file'])->get();

        $response = [
            'items' => $items
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(Request $request, $id)
    {

        $currComponent = parent::currComponent();

        $rules = [
            'name' => 'required',
            'slug' => "required|unique:languages,slug,{$id}|min:2|max:2",
        ];

        $item = Validator::make($request->all(), $rules);

        if ($item->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $item->messages(),
                    'type' => 'error'
                ],
            ]);

        $item = Languages::find($id);

        if (is_null($item)) {
            $item = new Languages();

            $nextPosition = Helpers::getNextItemPosition($item);
            $item->position = $nextPosition;
            $item->is_default = 0;
            $item->active = 1;
        } else {
            if ($request->get('slug') != $item->slug && ($item->slug != 'en'))
                Helpers::createLanguageFiles($request->get('slug'), $item->slug);
        }

        if ($item->slug != 'en')
            $item->slug = $request->get('slug', null);

        $item->name = $request->get('name', null);

        $item->save();

        if (is_null($id))
            Helpers::createLanguageFiles($item->slug);

        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');

        helpers()->logActions($currComponent, $item, $item->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => __("e.successful_created"),
                    'type' => 'success'
                ],
                'redirect' => adminUrl([$currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => __("e.successful_edited", ['name' => $item->name]),
                    'type' => 'success'
                ],
                'redirect' => adminUrl([$currComponent->slug, 'edit', $item->id])
            ]);
    }

    public function updatePosition(Request $request)
    {

        $order = $request->get('order');

        if (!is_array($order))
            return response()->json([
                'status' => false
            ]);

        $itemsPositions = [];
        foreach ($order as $id) {
            $item = Languages::find($id);
            $itemsPositions[] = @$item->position;
        }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($order as $key => $id) {
            Languages::where('id', $id)->update(['position' => $itemsPositions[$key]]);
        }

        return response()->json([
            'status' => true
        ]);
    }

    public function destroy(Request $request)
    {

        if (Languages::withTrashed()->count() == 1 && $request->get('event') != 'restore')
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => __("e.min_one_language"),
                    'type' => 'warning'
                ]
            ]);

        $deletedItemsId = substr($request->get('id'), 1, -1);

        if (empty($deletedItemsId))
            return response()->json([
                'status' => false
            ]);

        if ($request->get('event') != 'to-trash' && $request->get('event') != 'from-trash' && $request->get('event') != 'restore')
            return response()->json([
                'status' => false
            ]);

        $currComponent = parent::currComponent();

        $deletedItemsIds = explode(',', $deletedItemsId);

        if ($request->get('event') == 'to-trash') {
            $items = Languages::whereIn('id', $deletedItemsIds)->get();
        } else {
            $items = Languages::onlyTrashed()->whereIn('id', $deletedItemsIds)->get();
        }

        $cartMessage = $responseMsg = '';

        if (!$items->isEmpty()) {
            foreach ($items as $item) {

                $cartMessage .= $item->name . ', ';

                if ($request->get('event') == 'to-trash' && !$item->trashed()) {
                    $item->delete();
                    $responseMsg = !empty($cartMessage) ? __("e.items_added_to_trash", ['items' => substr($cartMessage, 0, -2)]) : '';
                    helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                } elseif ($request->get('event') == 'from-trash') {
                    $item->files()->delete();
                    $item->forceDelete();
                    $responseMsg = !empty($cartMessage) ? __("e.items_removed_from_trash", ['items' => substr($cartMessage, 0, -2)]) : '';
                    helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                } elseif ($request->get('event') == 'restore') {
                    $item->restore();
                    $responseMsg = !empty($cartMessage) ? __("e.items_restored_from_trash", ['items' => substr($cartMessage, 0, -2)]) : '';
                    helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                }

            }
        }

        return response()->json([
            'status' => false
        ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();

        if (Languages::withTrashed()->count() == 1 && $request->get('event') != 'restore')
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => __("e.min_one_language"),
                    'type' => 'warning'
                ]
            ]);

        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                Languages::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = Languages::whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = Languages::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->files()->delete();
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = Languages::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                    }
                }
                break;
            default:
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }


    public function activateItem(Request $request)
    {

        $currComponent = parent::currComponent();
        $item = Languages::findOrFail($request->get('id'));

        $errorMsg = null;

        if ($request->get('action') == 'default' && !$item->active)
            $errorMsg = __("e.cant_inactivate_language");
        elseif ($request->get('active') && !$request->get('action') && $item->is_default)
            $errorMsg = __("e.cant_inactivate_def_language");

        if ($errorMsg)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => $errorMsg,
                    'type' => 'warning',
                ]
            ]);


        if ($request->get('active')) {
            $status = 0;
            $msg = __("e.element_is_inactive", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("e.element_is_active", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'activate');
        }

        if ($request->get('action') == 'default') {
            Languages::query()->update(['is_default' => 0]);
            $item->update(['is_default' => 1]);
        } else
            $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function changeDefaultLang(Request $request)
    {
        $language = Languages::find($request->get('id'));

        if ($language && $language->active) {
            Languages::where('is_default', 1)->update(['is_default' => 0]);
            $changed = Languages::find($request->get('id'))->update(['is_default' => 1]);
        } else {
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => 'You can\'t set default inactive language!',
                    'type' => 'info'
                ],
            ]);
        }
        return response()->json([
            'status' => true,
            'msg' => [
                'e' => __("e.successful_edited", ['name' => @$changed->name]),
                'type' => 'success'
            ],
        ]);
    }


}
