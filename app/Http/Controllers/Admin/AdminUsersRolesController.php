<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\Helpers;
use App\Models\AdminUserGroup;
use App\Repositories\AdminUserGroupRepository;
use App\Repositories\AdminUsersPermissionsRepository;
use App\Repositories\ComponentsIdRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;


class AdminUsersRolesController extends DefaultController
{

    private $path;
    private $adminUserGroupRepo;
    private $adminUserPermissionsRepo;
    private $componentsIdRepo;

    public function __construct()
    {
        $this->path = 'admin.roles';
        $this->adminUserGroupRepo = new AdminUserGroupRepository();
        $this->adminUserPermissionsRepo = new AdminUsersPermissionsRepository();
        $this->componentsIdRepo = new ComponentsIdRepository();


    }

    public function index()
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = $this->adminUserGroupRepo->all([
            'slug' => ['!=', str_slug(customToken('U2J3bHkgeXZ2YSBueXZidw=='))],
        ], $perPage, ['users' => function ($q) {
            $q->withTrashed();
        }]);

        $response = [
            'items' => $items,
            'path' => $this->path
        ];

        return view("{$this->path}.list", $response);
    }

    public function create()
    {

        $response = [
            'path' => $this->path,
            'rights' => [],
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id)
    {

        $item = $this->adminUserGroupRepo->findOrFail($id, [
            'slug' => ['!=', str_slug(customToken('U2J3bHkgeXZ2YSBueXZidw=='))]
        ], ['permissions']);

        $rights = [];

        if (!$item->permissions->isEmpty())
            foreach ($item->permissions as $permission) {
                $rights[$permission->component_id] = [
                    'for_create' => $permission->for_create,
                    'for_edit' => $permission->for_edit,
                    'for_delete' => $permission->for_delete,
                    'for_view' => $permission->for_view,
                    'for_active' => $permission->for_active
                ];
            }

        $response = [
            'path' => $this->path,
            'item' => $item,
            'rights' => $rights,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = $this->adminUserGroupRepo->all([
            'slug' => ['!=', str_slug(customToken('U2J3bHkgeXZ2YSBueXZidw=='))],
        ], 0, [], ['onlyTrashed']);

        $response = [
            'items' => $items
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(Request $request, $id)
    {

        $rules = [
            'name' => 'required',
        ];

        $item = Validator::make($request->all(), $rules);

        if ($item->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $item->messages(),
                    'type' => 'error'
                ],
            ]);

        $rights = $request->get('rights');

        if (is_null($rights))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("e.can_have_min_one_right")],
                    'type' => 'warning'
                ]
            ]);

        $authUser = auth()->guard('admin')->user();
        $currComponent = parent::currComponent();

        $item = $this->adminUserGroupRepo->find($id);

        if (is_null($item)) {
            $item = $this->adminUserGroupRepo->init();
            $item->root = 0;
            $item->active = 1;
        } else {

            if ($item->isRoot)
                $item->root = 1;
            else
                $item->root = 0;

            if (!$authUser->group->isRoot && $item->isRoot)
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => [__("e.cannot_edit_right")],
                        'type' => 'warning'
                    ]
                ]);
        }

        $item->name = $request->get('name', null);
        $item->slug = str_slug($request->get('name', null));

        $item->save();

        if (!is_null($rights)) {

            $item->permissions()->delete();

            foreach ($rights as $componentId => $right) {

                if (!empty(array_filter($right))) {

                    $forCreate = $forEdit = $forDelete = $forView = $forActive = 0;
                    if (array_key_exists('for_create', array_filter($right)))
                        $forCreate = 1;
                    if (array_key_exists('for_edit', array_filter($right)))
                        $forEdit = 1;
                    if (array_key_exists('for_delete', array_filter($right)))
                        $forDelete = 1;
                    if (array_key_exists('for_view', array_filter($right)))
                        $forView = 1;
                    if (array_key_exists('for_active', array_filter($right)))
                        $forActive = 1;

                    $this->adminUserPermissionsRepo->create([
                        'admin_user_group_id' => $item->id,
                        'component_id' => $componentId,
                        'for_create' => $forCreate,
                        'for_edit' => $forEdit,
                        'for_delete' => $forDelete,
                        'for_view' => $forView,
                        'for_active' => $forActive
                    ]);
                }
            }

        }

        helpers()->logActions($currComponent, $item, $item->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => __("e.successful_created"),
                    'type' => 'success'
                ],
                'redirect' => adminUrl([$currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => __("e.successful_edited", ['name' => $item->name]),
                    'type' => 'success'
                ],
                'redirect' => adminUrl([$currComponent->slug, 'edit', $item->id])
            ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                AdminUserGroup::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;

            case 'delete-to-trash':
                $items = AdminUserGroup::whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        if (!$item->isRoot) {
                            $item->delete();
                            helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                        }

                    }
                }
                break;
            case 'delete-from-trash':
                $items = AdminUserGroup::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        if (!$item->isRoot) {
                            $item->permissions()->delete();
                            $item->forceDelete();
                            helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                        }
                    }
                }
                break;
            case 'restore-from-trash':
                $items = AdminUserGroup::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                    }
                }
                break;

            default:
                break;
        }
        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function activateItem(Request $request)
    {

        $item = $this->adminUserGroupRepo->findOrFail($request->get('id'), [
            'slug' => ['!=', str_slug(customToken('U2J3bHkgeXZ2YSBueXZidw=='))]
        ]);

        if ($item->isRoot)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("e.cannot_activate_inactivate_group")],
                    'type' => 'warning'
                ]
            ]);

        $currComponent = parent::currComponent();

        if ($request->get('active')) {
            $status = 0;
            $msg = __("e.element_is_inactive", ['name' => @$item->name]);
            helpers()->logActions($currComponent, $item, @$item->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("e.element_is_active", ['name' => @$item->name]);
            helpers()->logActions($currComponent, $item, @$item->name, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }
}
