<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helpers;
use App\Models\ComponentsId;
use App\Models\Files;
use App\Models\FilesItems;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Intervention\Image\Gd\Decoder;

class FileController extends Controller
{

    public function getGalleryFiles(Request $request)
    {

        $filesTypes = $request->get('types', null);
        $search = $request->get('search', null);
        $files = $items = [];
        $componentId = $request->get('componentId', null);
        $currentPage = !is_null($request->get('page')) && $request->get('page') > 0 ? $request->get('page') : 1;

        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        $items = Files::where(function ($q) use ($componentId, $filesTypes, $search) {
            if ($filesTypes)
                $q->where('type', $filesTypes);
            if ($search)
                $q->where('original_name', 'LIKE', "%{$search}%");
        })->whereHas('filesItems', function ($q) use ($componentId) {
            $q->whereHas('component', function ($q) use ($componentId) {
                $q->where('active', 1);
                $q->componentsWithPermissions();

                if ($componentId)
                    $q->where('id', $componentId);
            });
            if (!$componentId)
                $q->orWhereNull('component_id');
        })->orderBy('created_at', 'desc')->paginate(50);

        if ($items->isNotEmpty()) {
            foreach ($items as $item) {

                if (file_exists($item->file)) {
                    $fileLocations = getAllFileLocationsByUrl($item->type == 'image' ? $item->fileLocation : $item->file, 'magic');
                    $smallFile = 'admin-assets/img/no-image.png';

                    if (!empty($fileLocations)) {
                        if ($item->type == 'image')
                            $smallFile = array_key_exists('small', $fileLocations) ? $fileLocations['small'] : $fileLocations['original'];
                        else {
                            $noImageFileLocations = getAllFileLocationsByUrl($item->fileLocation, 'magic');
                            if (!empty($noImageFileLocations))
                                $smallFile = @$noImageFileLocations['original'] ?: 'admin-assets/img/no-image.png';
                        }

                        $files[] = [
                            'fileId' => $item->id,
                            'name' => $item->original_name,
                            'size' => $item->size,
                            'type' => $item->mime_type,
                            'url' => @asset($item->file),
                            'active' => $item->active,
                            'accepted' => true,
                            'cleanName' => getCleanFileName($item->originalFileName),
                            'slug' => str_slug(getCleanFileName($item->originalFileName)),
                            'fileLocations' => $fileLocations,
                            'smallFile' => asset(urldecode($smallFile)),
                            'createdAt' => strtotime($item->created_at) ? date('d.m.Y', strtotime($item->created_at)) : '',
                            'resolution' => $item->resolution,
                            'itemSize' => formatSizeUnits($item->size),
                            'itemType' => $item->type,
                        ];
                    }
                }
            }
        }

        return response()->json([
            'status' => true,
            'files' => $files,
            'componentId' => $request->get('componentId', null),
            'itemId' => $request->get('itemId', null),
            'filesTypes' => $filesTypes,
            'currPage' => $items->currentPage(),
            'lastPage' => $items->lastPage()
        ]);
    }

    public function uploadFile(Request $request)
    {

        if (empty($request->file()) || !array_key_exists('file', $request->file()))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("e.something_wrong")],
                    'type' => 'error'
                ]
            ]);

        $itemIds = $items = $errorResponse = [];
        $item = null;
        $files = [];

        $component = ComponentsId::where('active', 1)
            ->componentsWithPermissions()
            ->find($request->get('componentId'));

        $componentId = @$component->id;

        $allFiles = $request->file()['file'];

        foreach ($allFiles as $key => $oneFile) {

            $extension = $oneFile->getClientOriginalExtension();
            $originalFileName = $oneFile->getClientOriginalName();
            $fileType = $oneFile->getClientMimeType();
            $fileSize = $oneFile->getClientSize();

            if (strpos($fileType, 'image') && strtolower($extension) != 'svg')
                try {
                    $decoder = new Decoder();
                    $decoder->initFromPath($oneFile);
                } catch (\Exception $e) {
                    return response()->json([
                        'status' => false,
                        'msg' => [
                            'e' => [$e->getMessage()],
                            'type' => 'error'
                        ]
                    ], 500);
                }

            if (strpos($fileType, 'image') !== false && $fileSize > 10485760) { // 10 MB
                $errorResponse = [
                    'status' => false,
                    'msg' => [
                        'e' => [__("e.image_max_size", ['size' => 10])],
                        'type' => 'warning'
                    ]
                ];

                $items[] = $itemIds[] = null;

                continue;
            }

            $fileName = getCleanFileName($originalFileName) . '-_-' . time() . '.' . $extension;

            switch (strtolower($extension)) {
                case 'gif':
                case 'ico':
                case 'jpg':
                case 'jpeg':
                case 'png':
                case 'svg':
                case 'doc':
                case 'docx':
                case 'pdf':
                case 'ppt':
                case 'pptx':
                case 'rar':
                case 'xls':
                case 'xlsx':
                case 'zip':
                case '7z':
                case 'mp3':
                case 'mp4':
                case 'wav':
                case 'midi':
                case 'avi':
                case 'mkv':
                case 'flv':
                case 'mov':
                case 'moov':
                case 'mpg':
                case 'vdo':
                case 'txt':
                case 'xml':
                case 'webm':
                {
                    $imageFolder = '';

                    if (strpos($fileType, 'image') !== false)
                        $imageFolder = '/images';

                    $destinationPath = 'files/' . date('Y-m-d') . $imageFolder;

                    break;
                }
                default :
                {
                    return response()->json([
                        'status' => false,
                        'msg' => [
                            'e' => [__("e.invalid_file_format")],
                            'type' => 'error'
                        ]
                    ]);
                    break;
                }
            }

            if ($fileSize <= 67108864) { // 64 MB
                $resolution = [];
                $oneFile->move($destinationPath, $fileName);
                $originalPath = "{$destinationPath}/";

                if ($imageFolder == '/images') {

                    if ($extension != 'svg' && $extension != 'ico') {

                        $largePath = "{$destinationPath}/large/";
                        $mediumPath = "{$destinationPath}/medium/";
                        $smallPath = "{$destinationPath}/small/";

                        File::makeDirectory($largePath, 0775, true, true);
                        File::makeDirectory($mediumPath, 0775, true, true);
                        File::makeDirectory($smallPath, 0775, true, true);

                        Image::make($originalPath . $fileName)->resize(1200, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save($largePath . $fileName);

                        Image::make($originalPath . $fileName)->resize(500, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save($mediumPath . $fileName);

                        Image::make($originalPath . $fileName)->resize(150, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save($smallPath . $fileName);

                        $originalImageResolution = getImageResolution($originalPath . $fileName);

                        $resolution = array_filter([
                            'width' => @$originalImageResolution->width,
                            'height' => @$originalImageResolution->height
                        ]);
                    }
                }

                $fileInstance = new Files();
                $nextFilesPosition = Helpers::getNextItemPosition($fileInstance);

                $item = $fileInstance->create([
                    'file' => $originalPath . $fileName,
                    'original_name' => $originalFileName,
                    'active' => 1,
                    'type' => substr($fileType, 0, strpos($fileType, '/')),
                    'mime_type' => $fileType,
                    'size' => $fileSize,
                    'resolution' => $resolution,
                    'position' => $nextFilesPosition
                ]);

                $items[] = $item;
                $itemIds[] = $item->id;

                $fileItems = new FilesItems();
                $nextPosition = Helpers::getNextItemPosition($fileItems);

                $fileItems->create([
                    'file_id' => $item->id,
                    'component_id' => $componentId,
                    'item_id' => null,
                    'variation_id' => null,
                    'lang_id' => null,
                    'active' => 1,
                    'position' => $nextPosition

                ]);

                if (file_exists($item->file)) {
                    $fileLocations = getAllFileLocationsByUrl($item->type == 'image' ? $item->fileLocation : $item->file, 'magic');
                    $smallFile = 'admin-assets/img/no-image.png';

                    if (!empty($fileLocations)) {
                        if ($item->type == 'image')
                            $smallFile = array_key_exists('small', $fileLocations) ? $fileLocations['small'] : $fileLocations['original'];
                        else {
                            $noImageFileLocations = getAllFileLocationsByUrl($item->fileLocation, 'magic');
                            if (!empty($noImageFileLocations))
                                $smallFile = @$noImageFileLocations['original'] ?: 'admin-assets/img/no-image.png';
                        }

                        $files[] = [
                            'fileId' => $item->id,
                            'name' => $item->original_name,
                            'size' => $item->size,
                            'type' => $item->mime_type,
                            'url' => @asset($item->file),
                            'active' => $item->active,
                            'accepted' => true,
                            'cleanName' => getCleanFileName($item->originalFileName),
                            'slug' => str_slug(getCleanFileName($item->originalFileName)),
                            'fileLocations' => $fileLocations,
                            'smallFile' => asset(urldecode($smallFile)),
                            'createdAt' => strtotime($item->created_at) ? date('d.m.Y', strtotime($item->created_at)) : '',
                            'resolution' => $item->resolution,
                            'itemSize' => formatSizeUnits($item->size),
                            'itemType' => $item->type,
                        ];
                    }
                }
            }

        }

        if ($item)
            $item->accepted = true;

        return response()->json([
            'status' => true,
            'error' => $errorResponse,
            'files' => $files,
        ]);


    }

    public function destroyFile(Request $request)
    {

        if ($request->get('isAttachment') == 'true') {
            $item = FilesItems::find($request->get('fileId'));

            if (!is_null($item))
                $item->delete();
        } else {
            $item = Files::find($request->get('fileId'));

            if (is_null($item))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => [__("e.file_not_exist")],
                        'type' => 'warning'
                    ]
                ]);

            File::delete(getAllFileLocationsByUrl($item->file));

            $item->filesItems()->delete();
            $item->delete();
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => [__("e.file_was_deleted")],
                'type' => 'info'
            ]
        ]);
    }

    public function activateFile(Request $request)
    {

        if ($request->get('isAttachment') == 'true')
            $item = FilesItems::find($request->get('fileId'));
        else
            $item = Files::find($request->get('fileId'));

        if (is_null($item))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("e.file_not_exist")],
                    'type' => 'warning'
                ]
            ]);

        if ($request->get('isAttachment') == 'true' && $item->file->active == 0)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => [__("e.file_global_inactive")],
                    'type' => 'warning'
                ]
            ]);

        if ($request->get('active')) {
            $status = 0;
            $msg = __("e.file_inactive");
        } else {
            $status = 1;
            $msg = __("e.file_active");
        }

        $item->update(['active' => $status]);

        if ($request->get('isAttachment') == 'false')
            if ($item->active == 0)
                FilesItems::where('file_id', $item->id)->update(['active' => 0]);
            else
                FilesItems::where('file_id', $item->id)->update(['active' => 1]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => [$msg],
                'type' => 'info'
            ],
            'isAttachment' => $request->get('isAttachment') == 'true',
            'itemId' => $item->id

        ]);

    }

    public function changeFilePosition(Request $request)
    {

        $positionIds = $request->get('positionIds');

        foreach ($positionIds as $key => $positionId)
            FilesItems::where('id', $positionId)->update(['position' => $key + 1]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => [__("e.file_position_update")],
                'type' => 'info'
            ]
        ]);
    }
}