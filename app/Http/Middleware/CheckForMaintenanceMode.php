<?php

namespace App\Http\Middleware;

use Closure;

class CheckForMaintenanceMode
{

    protected $except = [
        LANG . '/admin',
        LANG . '/admin/*',
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $validateExceptions = false;

        foreach ($this->except as $except) {
            if ($except !== '/')
                $except = trim($except, '/');

            if ($request->fullUrlIs($except) || $request->is($except))
                $validateExceptions = true;
        }

        if (app()->isDownForMaintenance() && !$validateExceptions && !auth()->guard('admin')->check()) {
            abort(503);
        }

        return $next($request);
    }
}
