<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateFront
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'web')
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest(customUrl());
            }
        }

        $authUser = Auth::guard($guard)->user();

        if (!$authUser->active)
            return redirect()->to(customUrl('logout'));

        if ($authUser->for_logout) {
            $authUser->for_logout = 0;
            $authUser->save();

            return redirect()->to(customUrl('logout'));
        }

        return $next($request);
    }
}
