<?php

namespace App\Http\Middleware;

use App\Models\AdminUsers;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthenticateAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin')
    {

        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('en/admin/auth/login');
            }
        }

        $authUser = Auth::guard($guard)->user();


        if (!$authUser->active)
            return redirect()->to('en/admin/auth/logout');

        if ($authUser->for_logout) {
            $authUser->for_logout = 0;
            $authUser->save();

            return redirect()->to('en/admin/auth/logout');
        }

        return $next($request);
    }
}
