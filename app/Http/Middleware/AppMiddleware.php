<?php

namespace App\Http\Middleware;

use App\Models\Languages;
use App\Repositories\LanguagesRepository;
use Closure;
use Illuminate\Support\Facades\App;

class AppMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

        if ($request->segment(1) === 'admin')
            return redirect()->guest('en' . '/admin');

//        Maintenance mode
        if (config('app.maintenance') && !app()->isDownForMaintenance())
            touch(storage_path() . '/framework/down');
        elseif (!config('app.maintenance') && app()->isDownForMaintenance())
            @unlink(storage_path() . '/framework/down');
//        Maintenance mode


        $exceptsRoutes = [
            LANG,
            'admin',
            '__clockwork',
            'magic-token',
            'maibPaymentCallback'
        ];

        if (request()->segment(1) != 'en' && request()->segment(2) == 'admin'){
            $segment =  request()->segments();
            $segment[0] = 'en';
            return redirect()->to(url('',$segment));
        }

        if (request()->segment(1) && !in_array(request()->segment(1), $exceptsRoutes)) {
            return redirect()->to(url(LANG, request()->segments()));
        }

        return $next($request);
    }
}
