<?php

namespace App\Console\Commands;

use App\MaibApi\MaibClient;
use App\Models\LogsPayment;
use Illuminate\Console\Command;

class MaibCloseDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maib:closeday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close business day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        LogsPayment::log('Closeday begin', 0, 'store_id: 1', []);
        $client = new MaibClient();
        $resp = $client->closeDay();
        LogsPayment::log('Closeday end', 0, 'store_id: 1', [$resp]);
    }
}
