<?php

namespace App\Console\Commands;

use App\Models\ComponentsId;
use Illuminate\Console\Command;

class GenerateProductsCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:products-categories {count?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate products categories (count is number of generated products categories)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $productsCategoriesComponent = ComponentsId::where('slug', 'products-categories')->first();

        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));

//        Languages
        $languages = \App\Models\Languages::where('active', 1)->get();
//        Languages

        $categoriesLimit = (int)($this->argument('count') ?? 1);
        $categoriesLimitLvl2 = $categoriesLimit / 2;
        $categoriesLimitLvl3 = $categoriesLimitLvl2 - 2;

        for ($i = 0; $i < $categoriesLimit; $i++) {
            $this->info("Building {$i}!");

            $categoryName = $faker->unique()->productName . '-category';

            $category = \Modules\Products\Models\ProductsCategoriesId::create([
                'p_id' => null,
                'slug' => str_slug($categoryName),
                'position' => $i,
                'level' => 1,
                'active' => 1,
                'show_on_main' => (($i < $categoriesLimit / 2) ? 1 : 0),
            ]);

            $this->addImages($category, $i, $productsCategoriesComponent);

            foreach ($languages as $lang) {
                \Modules\Products\Models\ProductsCategories::create([
                    'products_category_id' => $category->id,
                    'lang_id' => $lang->id,
                    'name' => "{$categoryName}({$lang->slug})",
                    'description' => $faker->text(500),
                    'meta_title' => "{$categoryName}({$lang->slug})",
                    'meta_keywords' => $faker->sentence,
                    'meta_description' => $faker->text(200)
                ]);
            }

            for ($j = 0; $j < $categoriesLimitLvl2; $j++) {
                $this->info("Building {$i}-{$j}!");

                $categoryName = $faker->unique()->productName . '-category-lvl2';

                $category_lvl2 = \Modules\Products\Models\ProductsCategoriesId::create([
                    'p_id' => $category->id,
                    'slug' => str_slug($categoryName),
                    'position' => $categoriesLimit + $j,
                    'level' => 2,
                    'active' => 1,
                    'show_on_main' => (($j < floor($categoriesLimitLvl2 / 2)) ? 1 : 0),
                ]);

                $this->addImages($category_lvl2, $j, $productsCategoriesComponent);

                foreach ($languages as $lang) {
                    \Modules\Products\Models\ProductsCategories::create([
                        'products_category_id' => $category_lvl2->id,
                        'lang_id' => $lang->id,
                        'name' => "{$categoryName}({$lang->slug})",
                        'description' => $faker->text(500),
                        'meta_title' => "{$categoryName}({$lang->slug})",
                        'meta_keywords' => $faker->sentence,
                        'meta_description' => $faker->text(200)
                    ]);
                }

                for ($k = 0; $k < $categoriesLimitLvl3; $k++) {
                    $this->info("Building {$i}-{$j}-{$k}!");

                    $categoryName = $faker->unique()->productName . '-category-lvl3';

                    $category_lvl3 = \Modules\Products\Models\ProductsCategoriesId::create([
                        'p_id' => $category_lvl2->id,
                        'slug' => str_slug($categoryName),
                        'position' => $categoriesLimit + $categoriesLimitLvl2 + $k,
                        'level' => 3,
                        'active' => 1,
                        'show_on_main' => (($k < ceil($categoriesLimitLvl3 / 2)) ? 1 : 0),
                    ]);

                    $this->addImages($category_lvl3, $k, $productsCategoriesComponent);

                    foreach ($languages as $lang) {
                        \Modules\Products\Models\ProductsCategories::create([
                            'products_category_id' => $category_lvl3->id,
                            'lang_id' => $lang->id,
                            'name' => "{$categoryName}({$lang->slug})",
                            'description' => $faker->text(500),
                            'meta_title' => "{$categoryName}({$lang->slug})",
                            'meta_keywords' => $faker->sentence,
                            'meta_description' => $faker->text(200)
                        ]);
                    }
                }
            }
        }
    }

    private function addImages($item, $iterator, $productsCategoriesComponent)
    {
        if (is_null($productsCategoriesComponent))
            return false;

//        Images
        $images = \App\Models\Files::where('mime_type', 'image/jpeg')
            ->whereHas('filesItems', function ($q) use ($productsCategoriesComponent) {
                $q->where('component_id', $productsCategoriesComponent->id);
            })
            ->where('active', 1)
            ->limit(10)
            ->get();

        $images1 = $images->slice(0, 3)->pluck('id')->toArray();
        $images2 = $images->slice(3, 2)->pluck('id')->toArray();
        $images3 = $images->slice(5, 3)->pluck('id')->toArray();
        $images4 = $images->slice(8, 2)->pluck('id')->toArray();
//        Images

        if ($iterator % 11 == 0)
            foreach ($images1 as $image) {
                \App\Models\FilesItems::create([
                    'file_id' => $image,
                    'component_id' => $productsCategoriesComponent->id,
                    'item_id' => $item->id,
                    'variation_id' => null,
                    'lang_id' => null,
                    'active' => 1,
                    'position' => 0,
                ]);
            }
        elseif ($iterator % 7 == 0)
            foreach ($images2 as $image) {
                \App\Models\FilesItems::create([
                    'file_id' => $image,
                    'component_id' => $productsCategoriesComponent->id,
                    'item_id' => $item->id,
                    'variation_id' => null,
                    'lang_id' => null,
                    'active' => 1,
                    'position' => 0,
                ]);
            }
        elseif ($iterator % 5 == 0)
            foreach ($images3 as $image) {
                \App\Models\FilesItems::create([
                    'file_id' => $image,
                    'component_id' => $productsCategoriesComponent->id,
                    'item_id' => $item->id,
                    'variation_id' => null,
                    'lang_id' => null,
                    'active' => 1,
                    'position' => 0,
                ]);
            }
        else
            foreach ($images4 as $image) {
                \App\Models\FilesItems::create([
                    'file_id' => $image,
                    'component_id' => $productsCategoriesComponent->id,
                    'item_id' => $item->id,
                    'variation_id' => null,
                    'lang_id' => null,
                    'active' => 1,
                    'position' => 0,
                ]);
            }
    }
}
