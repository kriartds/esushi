<?php

namespace App\Console\Commands;

use App\Http\Controllers\Admin\SitemapController;
use App\Models\ComponentsId;
use Illuminate\Console\Command;
use Modules\Blog\Models\BlogCategoriesId;
use Modules\Blog\Models\BlogItems;
use Modules\Blog\Models\BlogItemsCategories;
use Modules\Blog\Models\BlogItemsId;
use Modules\Products\Models\ProductsAttributesId;
use Modules\Products\Models\ProductsItemsId;
use Modules\Products\Models\ProductsItemsReviews;
use Modules\Products\Models\ProductsItemsVariations;
use Modules\Products\Models\ProductsItemsVariationsDetails;
use Modules\Products\Models\ProductsItemsVariationsDetailsId;

class GenerateBlog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:blog {count?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate blog (count is number of generated articles)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $component = ComponentsId::where('slug', 'blog')->first();

        $faker = \Faker\Factory::create();

//        Languages
        $languages = \App\Models\Languages::where('active', 1)->get();
//        Languages

        $blogLimit = (int)($this->argument('count') ?? 1);

        for ($i = 0; $i < $blogLimit; $i++) {

            $blogName = $faker->unique(true)->name . '(' . time() . ')';

            $itemId = BlogItemsId::create([
                'slug' => str_slug($blogName . '-' . $i),
                'position' => $i,
                'active' => 1,
                'publish_date' => now()->subWeeks($i),
            ]);

            $this->addCategories($itemId, $i);
            $this->addImages($itemId, $i, $component);

            foreach ($languages as $lang) {
                BlogItems::create([
                    'blog_item_id' => $itemId->id,
                    'lang_id' => $lang->id,
                    'name' => "{$blogName}({$lang->slug})",
                    'description' => $faker->text(500),
                    'meta_title' => "{$blogName}({$lang->slug})",
                    'meta_keywords' => $faker->sentence,
                    'meta_description' => $faker->text(200)
                ]);
            }

            $this->info("Building {$i}");
        }
    }

    private function addImages($item, $iterator, $component = null)
    {
        if (is_null($component))
            return false;

//        Images
        $images = \App\Models\Files::where('mime_type', 'image/jpeg')
            ->whereHas('filesItems', function ($q) use ($component) {
                $q->where('component_id', $component->id);
            })
            ->where('active', 1)
            ->limit(10)
            ->get();

        $images1 = $images->slice(0, 3)->pluck('id')->toArray();
        $images2 = $images->slice(3, 2)->pluck('id')->toArray();
        $images3 = $images->slice(5, 3)->pluck('id')->toArray();
        $images4 = $images->slice(8, 2)->pluck('id')->toArray();
//        Images

        if ($iterator > 0 && $iterator % 11 == 0)
            foreach ($images1 as $image) {
                \App\Models\FilesItems::create([
                    'file_id' => $image,
                    'component_id' => $component->id,
                    'item_id' => $item->id,
                    'variation_id' => null,
                    'lang_id' => null,
                    'active' => 1,
                    'position' => $iterator,
                ]);
            }
        elseif ($iterator > 0 && $iterator % 7 == 0)
            foreach ($images2 as $image) {
                \App\Models\FilesItems::create([
                    'file_id' => $image,
                    'component_id' => $component->id,
                    'item_id' => $item->id,
                    'variation_id' => null,
                    'lang_id' => null,
                    'active' => 1,
                    'position' => $iterator,
                ]);
            }
        elseif ($iterator > 0 && $iterator % 5 == 0)
            foreach ($images3 as $image) {
                \App\Models\FilesItems::create([
                    'file_id' => $image,
                    'component_id' => $component->id,
                    'item_id' => $item->id,
                    'variation_id' => null,
                    'lang_id' => null,
                    'active' => 1,
                    'position' => $iterator,
                ]);
            }
        else
            foreach ($images4 as $image) {
                \App\Models\FilesItems::create([
                    'file_id' => $image,
                    'component_id' => $component->id,
                    'item_id' => $item->id,
                    'variation_id' => null,
                    'lang_id' => null,
                    'active' => 1,
                    'position' => $iterator,
                ]);
            }
    }

    private function addCategories($item, $iterator)
    {
//        Categories
        $categories = BlogCategoriesId::where('active', 1)->get();

        $count = $categories->count();
        $limitCategories = floor($count / 4);

        if ($limitCategories < 1)
            $limitCategories = 1;

        $categories1 = $categories->slice(0, $limitCategories);
        $categories2 = $categories->slice($limitCategories, 3);

//        Categories

        if ($iterator > 0 && $iterator % 11 == 0)
            foreach ($categories1 as $category11) {
                BlogItemsCategories::create([
                    'blog_item_id' => $item->id,
                    'blog_category_id' => $category11->id,
                ]);
            }
        else
            foreach ($categories2 as $category11) {
                BlogItemsCategories::create([
                    'blog_item_id' => $item->id,
                    'blog_category_id' => $category11->id,
                ]);
            }
    }
}
