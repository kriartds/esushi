<?php

namespace App\Console\Commands;

use App\Models\ComponentsId;
use Illuminate\Console\Command;
use Modules\Products\Models\ProductsAttributesId;
use Modules\Products\Models\ProductsItemsId;
use Modules\Products\Models\ProductsItemsReviews;
use Modules\Products\Models\ProductsItemsVariations;
use Modules\Products\Models\ProductsItemsVariationsDetails;
use Modules\Products\Models\ProductsItemsVariationsDetailsId;

class GenerateProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:products {count?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate products (count is number of generated products)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $productsComponent = ComponentsId::where('slug', 'products')->first();

        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));



//        Languages
        $languages = \App\Models\Languages::where('active', 1)->get();
//        Languages

        $productsLimit = (int)($this->argument('count') ?? 1);

        for ($i = 0; $i < $productsLimit; $i++) {

            $itemType = $faker->randomElement([1, 2]);

//            Price
            $price = $salePrice = null;
            if ($itemType == 1) {
                $price = $faker->randomNumber($faker->randomElement([2, 4]), true);
                $salePrice = null;

                if (strlen($price) > 2)
                    $salePrice = $price - (($faker->randomElement([5, 10, 25, 33, 48, 75, 83]) * $price) / 100);
            }
//            Price

//            Stock
            $useStock = $faker->randomElement([0, 1]);
            $stockQuantity = null;
            $status = 'in_stock';

            if ($itemType == 1) {
                if ($useStock == 1) {
                    $stockQuantity = $faker->randomNumber(2);
                    $status = $faker->randomElement(['in_stock', 'out_of_stock']);
                }
            }
//            Stock

//            Attributes
            $attributes = ProductsAttributesId::where('active', 1)->whereNull('p_id')->with('children')->get();
//            Attributes

            $productName = $faker->unique(true)->productName . '(' . time() . ')';

            $itemId = ProductsItemsId::create([
                'slug' => str_slug($productName . '-' . $i),
                'item_type_id' => $itemType,
                'price' => $price,
                'sale_price' => $salePrice,
                'use_stock' => $useStock,
                'stock_quantity' => $stockQuantity,
                'status' => $status,
                'weight' => null,
                'length' => null,
                'width' => null,
                'height' => null,
                'sku' => $faker->unique(true)->ean13,
                'position' => $i,
                'active' => 1,
                'show_on_main' => (($i < ceil($productsLimit / 200)) ? 1 : 0),
                'popularity_sales' => $faker->numberBetween(0, 10000),
            ]);

            $this->addCategories($itemId, $i);
            $this->addImages($itemId, $i, null, $productsComponent);

            foreach ($languages as $lang) {
                \Modules\Products\Models\ProductsItems::create([
                    'products_item_id' => $itemId->id,
                    'lang_id' => $lang->id,
                    'name' => "{$productName}({$lang->slug})",
                    'description' => $faker->text(500),
                    'meta_title' => "{$productName}({$lang->slug})",
                    'meta_keywords' => $faker->sentence,
                    'meta_description' => $faker->text(200)
                ]);
            }

//            Variations

            $attributeUserVariation = $faker->randomElement([0, 1]);

            foreach ($attributes as $key => $attribute) {
                $itemAttribute = \Modules\Products\Models\ProductsItemsAttributes::create([
                    'products_item_id' => $itemId->id,
                    'products_parent_attribute_id' => $attribute->id,
                    'visible_on_product_page' => $faker->randomElement([0, 1]),
                    'for_variation' => $itemType == 2 ? ($key == 0 ? 1 : $attributeUserVariation) : 0
                ]);

                if ($attribute->children->isNotEmpty()) {
                    $attributeChildrenCount = $faker->randomElement([1, 2]);
                    $children = @$faker->randomElements($attribute->children->pluck('id')->toArray(), ($attribute->children->count() >= $attributeChildrenCount ? $attributeChildrenCount : 1));
                    if (!$children)
                        $children = $attribute->children->pluck('id')->toArray();

                    foreach ($children as $child) {
                        \Modules\Products\Models\ProductsItemsAttributesOptions::create([
                            'products_items_attribute_id' => $itemAttribute->id,
                            'products_attribute_id' => $child
                        ]);
                    }
                }
            }

//            Variations
            if ($itemType == 2) {
                $this->addVariations($itemId, $faker, $productsLimit, $i, $languages, $productsComponent);
            }
//            Variations


//            Reviews
            $this->addReviews($itemId, $faker, $i);
//            Reviews

            $this->info("Building {$i}-" . ($itemId->item_type_id == 1 ? 'simple' : 'variable'));
        }
    }

    private function addImages($item, $iterator, $variationDetailsId = null, $productsComponent = null)
    {
        if (is_null($productsComponent))
            return false;

//        Images
        $images = \App\Models\Files::where('mime_type', 'image/jpeg')
            ->whereHas('filesItems', function ($q) use ($productsComponent) {
                $q->where('component_id', $productsComponent->id);
            })
            ->where('active', 1)
            ->limit(10)
            ->get();

        $images1 = $images->slice(0, 3)->pluck('id')->toArray();
        $images2 = $images->slice(3, 2)->pluck('id')->toArray();
        $images3 = $images->slice(5, 3)->pluck('id')->toArray();
        $images4 = $images->slice(8, 2)->pluck('id')->toArray();
//        Images

        if ($iterator > 0 && $iterator % 11 == 0)
            foreach ($images1 as $image) {
                \App\Models\FilesItems::create([
                    'file_id' => $image,
                    'component_id' => $productsComponent->id,
                    'item_id' => $item->id,
                    'variation_id' => $variationDetailsId,
                    'lang_id' => null,
                    'active' => 1,
                    'position' => 0,
                ]);
            }
        elseif ($iterator > 0 && $iterator % 7 == 0)
            foreach ($images2 as $image) {
                \App\Models\FilesItems::create([
                    'file_id' => $image,
                    'component_id' => $productsComponent->id,
                    'item_id' => $item->id,
                    'variation_id' => $variationDetailsId,
                    'lang_id' => null,
                    'active' => 1,
                    'position' => 0,
                ]);
            }
        elseif ($iterator > 0 && $iterator % 5 == 0)
            foreach ($images3 as $image) {
                \App\Models\FilesItems::create([
                    'file_id' => $image,
                    'component_id' => $productsComponent->id,
                    'item_id' => $item->id,
                    'variation_id' => $variationDetailsId,
                    'lang_id' => null,
                    'active' => 1,
                    'position' => 0,
                ]);
            }
        else
            foreach ($images4 as $image) {
                \App\Models\FilesItems::create([
                    'file_id' => $image,
                    'component_id' => $productsComponent->id,
                    'item_id' => $item->id,
                    'variation_id' => $variationDetailsId,
                    'lang_id' => null,
                    'active' => 1,
                    'position' => 0,
                ]);
            }
    }

    private function addCategories($item, $iterator)
    {
//        Categories
        $categories = \Modules\Products\Models\ProductsCategoriesId::where('active', 1)->whereNull('p_id')->with('children')->get();
        $count = $categories->count();
        $limitCategories = floor($count / 4);

        if ($limitCategories < 1)
            $limitCategories = 1;

        $categories1 = $categories->slice(0, $limitCategories);
        $categories2 = $categories->slice($limitCategories, 3);
        $categories3 = $categories->slice(($limitCategories + 3), 3);
        $categories4 = $categories->slice(($limitCategories + 6));

//        Categories

        if ($iterator > 0 && $iterator % 11 == 0)
            foreach ($categories1 as $category11) {
                \Modules\Products\Models\ProductsItemsCategories::create([
                    'products_item_id' => $item->id,
                    'products_category_id' => $category11->id,
                ]);

                if ($category11->children->isNotEmpty())
                    foreach ($category11->children->take(5) as $child) {
                        \Modules\Products\Models\ProductsItemsCategories::create([
                            'products_item_id' => $item->id,
                            'products_category_id' => $child->id,
                        ]);

                        if ($child->children->isNotEmpty())
                            foreach ($child->children->take(2) as $child_lvl2) {
                                \Modules\Products\Models\ProductsItemsCategories::create([
                                    'products_item_id' => $item->id,
                                    'products_category_id' => $child_lvl2->id,
                                ]);
                            }
                    }
            }
        elseif ($iterator > 0 && $iterator % 7 == 0)
            foreach ($categories2 as $category11) {
                \Modules\Products\Models\ProductsItemsCategories::create([
                    'products_item_id' => $item->id,
                    'products_category_id' => $category11->id,
                ]);

                if ($category11->children->isNotEmpty())
                    foreach ($category11->children->take(5) as $child) {
                        \Modules\Products\Models\ProductsItemsCategories::create([
                            'products_item_id' => $item->id,
                            'products_category_id' => $child->id,
                        ]);
                    }
            }
        elseif ($iterator > 0 && $iterator % 5 == 0)
            foreach ($categories3 as $category11) {
                \Modules\Products\Models\ProductsItemsCategories::create([
                    'products_item_id' => $item->id,
                    'products_category_id' => $category11->id,
                ]);
            }
        else
            if ($categories4->isNotEmpty())
                foreach ($categories4 as $category11) {
                    \Modules\Products\Models\ProductsItemsCategories::create([
                        'products_item_id' => $item->id,
                        'products_category_id' => $category11->id,
                    ]);
                }
            else
                foreach ($categories1 as $category11) {
                    \Modules\Products\Models\ProductsItemsCategories::create([
                        'products_item_id' => $item->id,
                        'products_category_id' => $category11->id,
                    ]);
                }

    }

    private function addVariations($item, $faker, $productsLimit, $iterator, $languages, $productsComponent)
    {
        $variationPrice = $faker->randomNumber($faker->randomElement([2, 4]), true);
        $variationSalePrice = null;

        if (strlen($variationPrice) > 2)
            $variationSalePrice = $variationPrice - (($faker->randomElement([5, 10, 25, 33, 48, 75, 83]) * $variationPrice) / 100);


        $useStock = $faker->randomElement([0, 1]);
        $stockQuantity = null;
        $status = 'in_stock';

        if ($useStock == 1) {
            $stockQuantity = $faker->randomNumber(2);
            $status = $faker->randomElement(['in_stock', 'out_of_stock']);
        }

        $variationSelectedAttributes = $item->productItemsOptions()->whereHas('productsAttr', function ($q) {
            $q->where('for_variation', 1);
        })
            ->with(['attribute'])
            ->get();

        $allAttributes = ProductsAttributesId::where('active', 1)
            ->orderBy('position')
            ->with(['globalName', 'children', 'children.globalName', 'children.parent'])
            ->get();

        $attributes = $allAttributes->where('p_id', null);

        $variationAttributes = collect();

        if ($variationSelectedAttributes->isNotEmpty()) {

            $attrIds = $variationSelectedAttributes->pluck('products_attribute_id')->toArray();
            $attrIds = array_unique($attrIds);

            $groupedVariationAttributes = $allAttributes->whereIn('id', $attrIds)->groupBy('p_id');

            if ($groupedVariationAttributes->isNotEmpty())
                foreach ($groupedVariationAttributes as $p_id => $groupedVariationAttribute)
                    $variationAttributes->put($p_id, collect([
                        'parent' => $attributes->find($p_id),
                        'children' => $groupedVariationAttribute
                    ]));

            $variationAttributes = $variationAttributes->sortBy(function ($attr) {
                return $attr['parent']->position;
            })->unique();
        }

        if ($variationAttributes->isNotEmpty()) {
            $multipleAttributesIds = [];

            foreach ($variationAttributes as $children) {
                if ($children['children']->isNotEmpty())
                    foreach ($children['children'] as $child) {
                        $multipleAttributesIds[$children['parent']->id][] = $child->id;
                    }
            }

            $multipleAttributesIds = arrayCartesian($multipleAttributesIds);

            foreach ($multipleAttributesIds as $key => $multipleAttributesId) {
                $variationDetailsId = ProductsItemsVariationsDetailsId::create([
                    'products_item_id' => $item->id,
                    'price' => $variationPrice,
                    'sale_price' => $variationSalePrice,
                    'use_stock' => $useStock,
                    'stock_quantity' => $stockQuantity,
                    'status' => $status,
                    'weight' => null,
                    'length' => null,
                    'width' => null,
                    'height' => null,
                    'sku' => $faker->unique(true)->ean13 . '-' . $item->id,
                    'is_default' => (($iterator < ceil($productsLimit / 500) && $key == 0) ? 1 : 0),
                ]);

                $this->addImages($item, $iterator * 3, $variationDetailsId->id, $productsComponent);

                foreach ($languages as $lang) {
                    ProductsItemsVariationsDetails::create([
                        'products_items_variations_detail_id' => $variationDetailsId->id,
                        'lang_id' => $lang->id,
                        'description' => $faker->text(500)
                    ]);
                }

                foreach ($multipleAttributesId as $parent => $child) {
                    ProductsItemsVariations::create([
                        'products_items_variations_details_id' => $variationDetailsId->id,
                        'products_attribute_parent_id' => $parent,
                        'products_attribute_child_id' => $child,
                    ]);
                }
            }
        }
    }

    private function addReviews($itemId, $faker, $iterator)
    {
        if ($iterator > 0 && $iterator % 11 == 0)
            $countReviews = 15;
        elseif ($iterator > 0 && $iterator % 7 == 0)
            $countReviews = 10;
        elseif ($iterator > 0 && $iterator % 5 == 0)
            $countReviews = 5;
        else
            $countReviews = 3;

        for ($i = 0; $i < $countReviews; $i++)
            ProductsItemsReviews::create([
                'products_item_id' => $itemId->id,
                'user_id' => null,
                'first_name' => $faker->unique(true)->firstName,
                'last_name' => $faker->unique(true)->lastName,
                'email' => $faker->unique(true)->email,
                'message' => $faker->text($faker->randomElement([50, 100, 200, 300, 400, 500])),
                'active' => $faker->randomElement([0, 1]),
                'seen' => $faker->randomElement([0, 1]),
                'rating' => $faker->numberBetween(1, 5),
                'ip' => $faker->ipv4
            ]);
    }
}
