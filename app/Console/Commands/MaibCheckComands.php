<?php

namespace App\Console\Commands;

use App\IikoApi\IikoClient;
use App\MaibApi\MaibClient;
use App\Models\LogsPayment;
use Illuminate\Console\Command;
use Modules\Orders\Http\Helpers\Helpers as OrdersHelpers;
use Modules\Orders\Models\Cart;
use Modules\Restaurants\Models\RestaurantsId;

class MaibCheckComands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maibcheckcomands:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check commands if is not close';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $carts = Cart::where('bank_id_transaction', '!=', null)
            ->where('status', 'processing')
            ->where(function ($q) {
                $q->where('payment_bank_process', 'A');
                $q->where('payment_bank_status', 'OK');
            })->orWhere(function ($q) {
                $q->where('payment_bank_process', 'C');
                $q->where('payment_bank_status', 'OK');
            })->orWhere(function ($q) {
                $q->where('payment_bank_process', 'C');
                $q->where('payment_bank_status', 'CREATED');
            })
            ->get();
        
        try {
            foreach ($carts as $cart) {
                $client = new MaibClient();
                $string_cart_desc = 'sushi - Comanda ' . $cart->order_id;
                $response = $client->getTransactionResult($cart->bank_id_transaction, $cart->user_ip);
                if (isset($response['RESULT']) && $response['RESULT'] === "OK") {

                    $response = $client->makeDMSTrans($cart->bank_id_transaction, numberFormat($cart->amount + $cart->shipping_amount), 498, $_SERVER['REMOTE_ADDR'], htmlspecialchars_decode($string_cart_desc), LANG);
                    if (isset($response['RESULT']) && $response['RESULT'] === "OK") {
                        $cart->update([
                            'status' => 'checkout',
                            'payment_bank_process' => 'T',
                            'payment_bank_status' => $response['RESULT'],
                        ]);

                        $restaurant = RestaurantsId::where('id', $cart->restaurant_id)->first();

                        $iikoClient = new IikoClient($restaurant);
                        $order = $iikoClient->createOrder($cart);

                        if (isset($order['orderInfo']) && !empty($order['orderInfo'])) {
                            $cart->update([
                                'iiko_id' => $order['orderInfo']['id'],
                                'iiko_status' => $order['orderInfo']['creationStatus']
                            ]);
                        }

                        $cartDiscountAmount = $cart->discount_amount;
                        $cartShippingAmount = $cart->shipping_amount;
                        $allCartProducts = OrdersHelpers::getCartProducts($cart, -1, true, true);
                        $cartSubtotal = @$allCartProducts->total ?: 0;
                        $cartProducts = $allCartProducts->products;
                        $cartSubtotalWithCoupon = $cartSubtotal - $cartDiscountAmount;
                        $cartSubtotalWithCoupon = $cartSubtotalWithCoupon >= 0 ? $cartSubtotalWithCoupon : 0;
                        $cartTotal = $cartSubtotalWithCoupon + $cartShippingAmount;
                        $sendMailData = [
                            'item' => $cart,
                            'cartProducts' => $cartProducts,
                            'useShipping' => @$cart->shipping_details,
                            'usePickup' => @$cart->pickup_details,
                            'cartSubtotal' => $cartSubtotal,
                            'cartSubtotalWithCoupon' => $cartSubtotalWithCoupon,
                            'shippingAmount' => $cartShippingAmount,
                            'cartTotal' => $cartTotal,
                            'orderCurrency' => $cart->order_currency
                        ];


                        try {
                            $adminEmailForOrders = helpers()::getSettingsField('order_email', parent::globalSettings());
                            helpers()->sendMail($cart->email, $cart->details->name, $sendMailData, 'checkoutForm', parent::globalSettings(), __('front.email_checkout_subject'));
                            helpers()->sendMail($adminEmailForOrders, $cart->details->name, $sendMailData, 'checkoutAdminForm', parent::globalSettings(), __('front.email_checkout_subject_admin'), true);
                        } catch (\Exception $e) {
                        }

                    } else {
                        $cart->update([
                            'payment_bank_process' => 'T',
                            'payment_bank_status' => $response['RESULT'],
                        ]);
                    }
                } else {
                    $cart->update([
                        'status' => 'failed',
                        'payment_bank_process' => 'C',
                        'payment_bank_status' => $response['RESULT'],
                    ]);
                }
            }
        } catch (\Exception $e) {
            LogsPayment::log('Closeday end', 0, 'MaibCheckCommands Error Exception', [$e]);
        }
    }
}
