<?php

namespace App\Repositories;

use App\Models\SettingsTrans as Model;

class SettingsTransRepository extends BaseRepository
{

    protected $model;

    public function __construct()
    {
        parent::__construct(new Model());
    }
}