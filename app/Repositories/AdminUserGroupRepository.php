<?php

namespace App\Repositories;

use App\Models\AdminUserGroup as Model;

class AdminUserGroupRepository extends BaseRepository
{

    protected $model;

    public function __construct()
    {
        parent::__construct(new Model());
    }
}