<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

//use Jsdecena\Baserepo\BaseRepository;


//class CustomBaseRepository extends BaseRepository
class BaseRepository
{
    protected $model;

    /**
     * BaseRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
//        parent::__construct($model);
    }

    public function init()
    {
        return new $this->model;
    }

    public function all(array $whereConditions = [], $perPage = 0, $withs = [], $scopes = [], $columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc')
    {

        $items = $this->model;

        if (!empty($scopes))
            foreach ($scopes as $scope) {
                $items = $items->$scope();
            }

        $items = $items->where(function ($q) use ($whereConditions) {
            $this->whereConditions($whereConditions, $q);
        })->orderBy($orderBy, $sortBy)->with($withs);

        if ((int)$perPage > 0)
            $items = $items->paginate($perPage, $columns);
        else
            $items = $items->get($columns);

        return $items;

    }

    public function limit(array $whereConditions = [], $limit = 1, $withs = [], $scopes = [], $columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc')
    {

        $items = $this->model;

        if (!empty($scopes))
            foreach ($scopes as $scope) {
                $items = $items->$scope();
            }

        $items = $items->where(function ($q) use ($whereConditions) {
            $this->whereConditions($whereConditions, $q);
        })->orderBy($orderBy, $sortBy)->with($withs);


        $items = $items->limit($limit)->get($columns);

        return $items;

    }

    public function first(array $whereConditions = [], $withs = [], $scopes = [], $columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc')
    {

        $item = $this->model;

        if (!empty($scopes))
            foreach ($scopes as $scope) {
                $item = $item->$scope();
            }

        $item = $item->where(function ($q) use ($whereConditions) {
            $this->whereConditions($whereConditions, $q);
        })->orderBy($orderBy, $sortBy)->with($withs)->first($columns);

        return $item;
    }

    public function find($id, array $whereConditions = [], $withs = [], $scopes = [])
    {

        $item = $this->model;

        if (!empty($scopes))
            foreach ($scopes as $scope) {
                $item = $item->$scope();
            }

        $item = $item->where(function ($q) use ($whereConditions) {
            $this->whereConditions($whereConditions, $q);
        })->with($withs)->find($id);

        return $item;
    }

    public function findOrFail($id, array $whereConditions = [], $withs = [], $scopes = [])
    {

        $item = $this->model;

        if (!empty($scopes))
            foreach ($scopes as $scope) {
                $item = $item->$scope();
            }

        $item = $item->where(function ($q) use ($whereConditions) {
            $this->whereConditions($whereConditions, $q);
        })->with($withs)->findOrFail($id);

        return $item;
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function delete(array $whereConditions = [], $forceDelete = false)
    {
        $item = $this->model;

        $item = $item->where(function ($q) use ($whereConditions) {
            $this->whereConditions($whereConditions, $q);
        });

        if ($forceDelete)
            return $item->forceDelete();

        return $item->delete();
    }

    protected function whereConditions(array $whereConditions, $q)
    {
        if (!empty($whereConditions))
            foreach ($whereConditions as $row => $condition)
                switch ($row) {
                    case 'function' :

                        if (is_callable($condition))
                            $condition($q);

                        break;
                    case 'whereIn' :

                        if (is_array($condition))
                            $q->whereIn($condition[0], $condition[1]);

                        break;
                    case 'whereHas' :

                        if (is_array($condition) && is_callable($condition[1]))
                            $q->whereHas($condition[0], $condition[1]);

                        break;
                    case 'whereNull' :

                        if (!is_array($condition))
                            $q->whereNull($condition);

                        break;
                    default :

                        if (is_array($condition))
                            $q->where($row, $condition[0], $condition[1]);
                        else
                            if (is_null($condition))
                                $q->whereNull($row);
                            else
                                $q->where($row, $condition);

                        break;
                }
    }

}