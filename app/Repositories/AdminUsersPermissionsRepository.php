<?php

namespace App\Repositories;

use App\Models\AdminUsersPermissions as Model;

class AdminUsersPermissionsRepository extends BaseRepository
{

    protected $model;

    public function __construct()
    {
        parent::__construct(new Model());
    }

}