<?php

namespace App\Repositories;

use App\Models\AdminUsers as Model;

class AdminUsersRepository extends BaseRepository
{

    protected $model;

    public function __construct()
    {
        parent::__construct(new Model());
    }

}