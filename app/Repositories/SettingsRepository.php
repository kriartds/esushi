<?php

namespace App\Repositories;

use App\Models\Settings as Model;

class SettingsRepository extends BaseRepository
{

    protected $model;

    public function __construct()
    {
        parent::__construct(new Model());
    }
}