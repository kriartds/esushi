<?php

namespace App\Repositories;

use App\Models\SettingsTransId as Model;

class SettingsTransIdRepository extends BaseRepository
{

    protected $model;

    public function __construct()
    {
        parent::__construct(new Model());
    }
}