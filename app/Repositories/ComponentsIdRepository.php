<?php

namespace App\Repositories;

use App\Models\ComponentsId as Model;

class ComponentsIdRepository extends BaseRepository
{

    protected $model;

    public function __construct()
    {
        parent::__construct(new Model());
    }

    public function all(array $whereConditions = [], $perPage = 0, $withs = [], $scopes = [], $columns = ['*'], string $orderBy = 'position', string $sortBy = 'asc')
    {

        return parent::all($whereConditions, $perPage, $withs, $scopes, $columns, $orderBy, $sortBy);

    }

}