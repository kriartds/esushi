<?php

namespace App;

use App\Models\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Sms extends Model
{
    protected $table = 'sms';
    public $timestamps = false;
    protected $guarded = [];

    const OBJECT_TYPE_USER = 1;

    public static function newCode($criteria, $data=[])
    {
        $where = [
            ['object_type_id', '=', $criteria['object_type_id']],
            ['object_id', '=', $criteria['object_id']],
            ['used_at', '=', null]
        ];

        DB::table('sms')->where($where)->update([
            'used_at' => time(),
            'is_reject' => 1
        ]);

        $smsModel = Sms::add([
            'phone'=>$criteria['phone'],
            'object_type_id' => $criteria['object_type_id'],
            'object_id' => $criteria['object_id'],
            'user_id' => $criteria['user_id']
        ]);


        return $smsModel;


        /*
        $smsLimit = \common\models\common\Sms::smsLimit($criteria);
        $returnData = [];
        $smsModel = [];

        if(empty($smsLimit['smsLast'])){

            $insertData = array_merge($criteria, ['data' => json_encode($data)]);

            $smsModel = Sms::add($insertData);

            $returnData = [
                'status'=>true,
                'message'=>Sms::alert(Sms::ALERT_ERROR_CODE_NEW_CODE_SENDED),
                'model'=>$smsModel,
            ];
        }
        else{
            if($smsLimit['smsLast']->created_at + \Yii::$app->params['smsLimit']['timeout'] < time()){
                if($smsLimit['smsCountInDay'] < \Yii::$app->params['smsLimit']['smsCountInDay'])
                {
                    if(!empty($data))
                        $insertData = array_merge($criteria, ['data' => json_encode($data)]);
                    else
                        $insertData = array_merge($criteria, ['data' => $smsLimit['smsLast']->data]);

                    $smsModel = Sms::add($insertData);
                    $returnData = [
                        'status'=>true,
                        'message'=>Sms::alert(Sms::ALERT_ERROR_CODE_NEW_CODE_SENDED),
                        'model'=>$smsModel,
                    ];
                }
                else{
                    $returnData = [
                        'status'=>false,
                        'message'=>Sms::alert(Sms::ALERT_ERROR_CODE_SMS_LIMIT)
                    ];
                }
            }
            else{
                $returnData = [
                    'status'=>false,
                    'message'=>Sms::alert(Sms::ALERT_ERROR_CODE_SMS_LIMIT_TIMEOUT)
                ];
            }
        }
        */
        return $returnData;

    }


    public static function add($data)
    {

        $field['sms_code'] = rand(10000, 99999);
        $field['data'] = 'Код: ' . $field['sms_code'];
        $field['object_type_id'] = $data['object_type_id'];
        $field['object_id'] = $data['object_id'];
        $field['user_id'] = $data['user_id'];
        $field['phone'] = $data['phone'];
        $field['created_at'] = time();

        $model = Sms::create(
            $field
        );

        $response = '';
        Sms::send($model->phone, $model->data, $response);

        $model->update([
            'return_data'=>$response
        ]);


        /*
        if (!empty($resultXml)) {
            $xml = simplexml_load_string($resultXml) or die("Error: Cannot create object");
            if (!empty($xml->information['id_sms'][0]))
                $service_id_sms = (string)$xml->information['id_sms'][0];
        }

        $model->service_id_sms = $service_id_sms;
        $model->return_data = $resultXml;
        $model->save();

        $model->sms_code = '';
        */

        return $model;
    }

    public static function send($phone, $message, &$response){

        // $response = '1 result '. rand(1000000000, 9999999999);
        // return;

        $smsData = [
            'client_id'=>env('SMS_CLIENT_ID'),
            'secret'=>env('SMS_SECRET'),
            'expires_in'=>600
        ];

        $curl1 = curl_init();
        curl_setopt_array($curl1, array(
            CURLOPT_URL => "https://auth.sms.to/oauth/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>json_encode($smsData),
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));

        $response1 = curl_exec($curl1);
        curl_close($curl1);

        if(!empty($response1)){
            $response1 = json_decode($response1, true);

            if(!empty($response1['jwt']))
            {
                // $phone = '37494211938';
                $smsData = [
                    'message'=>$message,
                    'to'=>$phone,
                    'sender_id'=>env('SMS_SENDER_ID')
                ];

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://api.sms.to/sms/send",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS =>json_encode($smsData),
                    CURLOPT_HTTPHEADER => array(
                        "Content-Type: application/json",
                        "Accept: application/json",
                        "Authorization: Bearer ".$response1['jwt']  /*env('SMS_API_KEY')*/
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
            }
        }

        return;
    }



    public static function check($request)
    {
        $sms_id = $request->request->get('sms_id');
        $sms_code = trim($request->request->get('sms_code'));
        $user = auth()->user();

        $where = [
            ['id', '=', $sms_id],
            ['used_at', '=', null],
            ['sms_code', '=', $sms_code],
            ['user_id', '=', $user->id]
        ];

        $model = Sms::where($where)->first();

        if (!empty($model)) {

            $model->update(['used_at'=>time()]);

            $user->update(['is_check_phone'=>1]);

            // --конвертируем в массив чтобы потом использовать
            // $jsonData = json_decode($model->data, true);
            // $model->data = $jsonData;

            $model->sms_code = '';
            return $model;
        }

        return false;
    }

    public static function getLast($data)
    {
        $model = Sms::where($data)
            ->where([
                ['used_at', '=', null],
                ['created_at', '>', (time() - env('SMS_ACTUAL_TIME'))]
            ])
            ->orderBy('id','DESC')
            ->first();
        return $model;
    }

    public static function getCountInDay($data)
    {
        $count = Sms::where($data)
            ->where([
                ['created_at', '>', (time() - env('SMS_ACTUAL_TIME'))]
            ])
            ->count();

        return $count;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function smsLimit($data)
    {

        $smsLast = Sms::getLast($data);

        $smsCountInDay = Sms::getCountInDay($data);

        return [
            'smsLast'=>$smsLast,
            'smsCountInDay'=>$smsCountInDay,
        ];
    }

}
