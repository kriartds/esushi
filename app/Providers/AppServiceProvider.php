<?php

namespace App\Providers;

use App\Models\Wish;
use Modules\Currencies\Models\Currencies;
use Modules\Orders\Models\Cart;
use App\Repositories\AdminUsersPermissionsRepository;
use App\Repositories\ComponentsIdRepository;
use App\Repositories\LanguagesRepository;
use App\Repositories\SettingsRepository;
use App\Repositories\SettingsTransIdRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Modules\Products\Models\ProductsItemsId;
use Modules\Restaurants\Models\RestaurantsId;
use Nwidart\Modules\Facades\Module;
use Modules\Orders\Http\Helpers\Helpers as OrdersHelpers;


class AppServiceProvider extends ServiceProvider
{

    static $components;
    static $currComponent;
    static $groupPermission;
    static $cart;


    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        if (!(app()->runningInConsole() && (in_array('migrate', request()->server('argv')) || (in_array('artisan', request()->server('argv')) && (in_array('key:generate', request()->server('argv')) || in_array('db:seed', request()->server('argv'))))))) {
            $langListRepo = new LanguagesRepository();

            $langList = $langListRepo->all(['active' => 1]);
            $currLocale = App::getLocale();
            $defaultLang = $langList->where('is_default', 1)->first();
            $langId = null;
            $redirectToExistLang = null;


            if (!is_null($defaultLang) && $currLocale != $defaultLang->slug)
                App::setLocale($defaultLang->slug);


            $localeSlug = request()->segment(1);
            $existLang = $langList->where('slug', $localeSlug)->first();

            if (!is_null($existLang)) {
                session()->put('applocale', $existLang->slug);
                $langId = $existLang->id;
            }

            if (session()->has('applocale')) {
                App::setLocale(session()->get('applocale'));
            } else if (!is_null($defaultLang)) {
                App::setLocale($defaultLang->slug);
                $langId = $defaultLang->id;
            }


            if (array_search("admin", request()->segments())) {
                App::setLocale('en');
                $langId = 1;
            }


            if(!defined('LANG')) define('LANG', App::getLocale());
            if(!defined('LANG_ID')) define('LANG_ID', $langId);
            if(!defined('DEF_LANG_ID')) define('DEF_LANG_ID', @$defaultLang->id);
            if(!defined('DEF_LANG')) define('DEF_LANG', @$defaultLang->slug);

            // Define languages


            Validator::extend('lower_than_field', function ($attribute, $value, $parameters, $validator) {
                $min_field = $parameters[0];
                $data = $validator->getData();
                $min_value = $data[$min_field];

                return $value != 0 && $min_value != 0 ? $value > $min_value : true;
            });
            Validator::replacer('lower_than_field', function ($message, $attribute, $rule, $parameters) {
                $max_field = str_replace('_', ' ', $parameters[0]);
                return str_replace(':field', $max_field, $message);
            });

            $settingsRepo = new SettingsRepository();
            $settingsTransIdRepo = new SettingsTransIdRepository();

            $allSettings = (object)[
                'simple' => $settingsRepo->all(),
                'trans' => $settingsTransIdRepo->all([], 0, ['globalName'])
            ];
            setEmailConfigurations($allSettings);

            config(['app.maintenance' => @helpers()->getSettingsField('maintenance_mode', $allSettings)]);

            view()->share('allSettings', $allSettings);
            view()->share('langList', $langList);

            $componentsIdRepo = new ComponentsIdRepository();
            $adminUsersPermissionsRepo = new AdminUsersPermissionsRepository();

            $shareViews = [
                'admin/*'
            ];

            $modules = Module::getByStatus(1);

            if (!empty($modules))
                foreach ($modules as $module) {
                    $shareViews[] = $module->getLowerName() . '::*';
                }

            $shareViews = array_unique(array_filter($shareViews));
            $currencies = Currencies::where('active', 1)->get();
            $defaultCurrency = @$currencies->where('is_default', 1)->first();

            view()->composer($shareViews, function ($view) use ($componentsIdRepo, $adminUsersPermissionsRepo, $defaultCurrency) {
                if (auth()->guard('admin')->check()) {

//                Components list

                    if (!isset(static::$components) || is_null(static::$components))
                        static::$components = $componentsIdRepo->all(['active' => 1, 'p_id' => null], 0, ['globalName', 'children.globalName', 'file.file', 'children.file.file', 'recursiveChildren', 'recursiveChildren.globalName', 'children' => function ($q) {
                            $q->where('active', 1);
                            $q->orderBy('position');
                        }], ['componentsWithPermissions'], ['*'], 'position');


                    $sidebarComponents = static::$components->where('for_sidebar', 1);

                    if ((!isset(static::$currComponent) || is_null(static::$currComponent)) && !is_null(request()->segment(3)))
                        static::$currComponent = $componentsIdRepo->first(['active' => 1, 'slug' => request()->segment(3)], ['globalName'], ['componentsWithPermissions']);

//                Components list

                    $permissions = (object)[
                        'create' => 0,
                        'edit' => 0,
                        'delete' => 0,
                        'view' => 0,
                        'active' => 0
                    ];

                    if (!is_null(static::$currComponent)) {

                        if (!isset(static::$groupPermission) || is_null(static::$groupPermission))
                            static::$groupPermission = $adminUsersPermissionsRepo->first(['admin_user_group_id' => auth()->guard('admin')->user()->admin_user_group_id, 'component_id' => static::$currComponent->id]);

                        if (!is_null(static::$groupPermission))
                            $permissions = (object)[
                                'create' => static::$groupPermission->for_create,
                                'edit' => static::$groupPermission->for_edit,
                                'delete' => static::$groupPermission->for_delete,
                                'view' => static::$groupPermission->for_view,
                                'active' => static::$groupPermission->for_active
                            ];
                    }

                    $view->components = static::$components;
                    $view->sidebarComponents = $sidebarComponents;
                    $view->currComponent = static::$currComponent;
                    $view->permissions = $permissions;
                    $view->defaultCurrency = $defaultCurrency;
                }
            });

            $currBNMCurrency = @helpers()->getBnmCurrenciesByDate($defaultCurrency->slug, $currencies);

            $cookieCurrencies = (object)[
                'currCurrency' => $currBNMCurrency,
                'isCustom' => false
            ];

            view()->share('cookieCurrencies', $cookieCurrencies);

            view()->composer('front/*', function ($view) use ($cookieCurrencies) {
                $view->currCurrency = getCurrentCurrency($cookieCurrencies);
            });

            view()->composer(['front/footer'], function ($view) {
                $ourRestaurants = RestaurantsId::with('globalName')
                    ->where('active', 1)
                    ->get();

                $view->ourRestaurants = $ourRestaurants;
            });

            view()->composer('front/header', function ($view) use ($currencies) {
//                if($common_id=session('common_cart_id')){
//                    $commonCart = Cart::isInProgress()->where('common_id', $common_id)->first();
//                    if(is_null($commonCart)){
//                        session(['common_cart_id' => null]);
//                    }
//                }
                //$userId = request()->cookie('cart', null);

                //if (!@static::$cart && !is_null($userId))
                //    static::$cart = Cart::where('user_id', $userId)->first();

                //$allCartProducts = OrdersHelpers::getCartProducts(static::$cart);
                $allCartProducts = OrdersHelpers::getCartProducts();

                $cartCount = @$allCartProducts->count ?: 0;
                $cartSubtotal = @$allCartProducts->total ?: 0;
                $cartProducts = @$allCartProducts->products ?: collect();


                $view->currencies = $currencies;
                $view->cartCount = $cartCount;
                $view->cartSubtotal = $cartSubtotal;
                $view->cartProducts = $cartProducts;
            });
        }

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
