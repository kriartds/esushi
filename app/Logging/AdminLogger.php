<?php

namespace App\Logging;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class AdminLogger
{

    public function __invoke(array $config)
    {

        try {
            $handlers = [new StreamHandler($config['path'], $config['level'])];
        } catch (\Exception $e) {
            $handlers = [];
        }

        return new Logger($config['name'], $handlers);
    }

}