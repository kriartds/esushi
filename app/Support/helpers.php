<?php

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Modules\Menus\Http\Helpers\Helpers as MenuHelpers;
use Modules\Orders\Models\Cart;
use Modules\Sliders\Http\Helpers\Helpers as SlidersHelpers;
use Modules\Products\Http\Helpers\Helpers as ProductsHelpers;
use Modules\Orders\Http\Helpers\Helpers as OrdersHelpers;
use Nwidart\Modules\Facades\Module;
use Stevebauman\Location\Facades\Location;
use Modules\CountriesRegion\Models\Countries;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cookie;
use Modules\Currencies\Models\Currencies;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Arr;

if (!function_exists('helpers')) {
    /**
     * @return Helpers
     */
    function helpers()
    {
        return new Helpers();
    }
}

if (!function_exists('carbon')) {
    /**
     * @param null $datetime
     * @param null $tz
     * @return Carbon
     */
    function carbon($datetime = null, $tz = null)
    {
        if ($datetime instanceof \DateTime) {
            return Carbon::instance($datetime)->setTimezone($tz);
        }

        return Carbon::parse($datetime, $tz);
    }
}

if (!function_exists('getSettingsField')) {
    /**
     * @param $field
     * @param $allSettings
     * @param bool $trans
     * @return mixed
     */
    function getSettingsField($field, $allSettings, $trans = false)
    {
        return helpers()->getSettingsField($field, $allSettings, $trans);
    }
}

if (!function_exists('getDynamicField')) {
    /**
     * @param $fieldSlug
     * @param $componentSlug
     * @param $itemId
     * @param array $allDynamicFields
     * @return array|bool
     */
    function getDynamicField($fieldSlug, $componentSlug, $itemId, $allDynamicFields = [])
    {
        return helpers()->getDynamicField($fieldSlug, $componentSlug, $itemId, $allDynamicFields);
    }
}

if (!function_exists('getSliderBySlug')) {
    /**
     * @param $slug
     * @return mixed
     */
    function getSliderBySlug($slug)
    {
        if (!Module::has('Sliders') || !file_exists(module_path('Sliders') . "/Http/Helpers/Helpers.php"))
            return collect();

        return SlidersHelpers::getSliderBySlug($slug);
    }
}

if (!function_exists('checkWishList')) {
    /**
     * @param $itemId
     * @return bool
     */
    function checkWishList($itemId)
    {
        return helpers()->checkWishList($itemId);
    }
}

//*********************** Start Menus functions ***********************//

if (!function_exists('getMenuRecursive')) {
    /**
     * @param $typeId
     * @param int $levels
     * @return \Illuminate\Support\Collection
     */
    function getMenuRecursive($typeId, $levels = 1)
    {

        if (!Module::has('Menus') || !file_exists(module_path('Menus') . "/Http/Helpers/Helpers.php"))
            return collect();

        return MenuHelpers::getMenuRecursive($typeId, $levels);
    }
}

if (!function_exists('updateMenuItems')) {
    /**
     * @param $currComponent
     * @param $item
     * @param null $langId
     * @param string $action
     * @param null $destroyEvent
     * @return bool
     */
    function updateMenuItems($currComponent, $item, $langId = null, $action = 'save', $destroyEvent = null)
    {
        if (!Module::has('Menus') || !file_exists(module_path('Menus') . "/Http/Helpers/Helpers.php"))
            return false;

        return MenuHelpers::updateMenuItems($currComponent, $item, $langId, $action, $destroyEvent);
    }
}

//*********************** End Menus functions ***********************//

//*********************** Start Products functions ***********************//

if (!function_exists('makeFilter')) {
    /**
     * @param $request
     * @param $currComponent
     * @param $attributes
     * @param $filteredProductsPricesRange
     * @param null $currCurrency
     * @param null $currCategory
     * @return string|null
     */
    function makeFilter($request, $currComponent, $attributes, $filteredProductsPricesRange, $currCurrency = null, $currCategory = null)
    {
        return ProductsHelpers::makeFilter($request, $currComponent, $attributes, $filteredProductsPricesRange, $currCurrency, $currCategory);
    }
}

if (!function_exists('filterProducts')) {
    /**
     * @param $request
     * @param $currComponent
     * @param $perPage
     * @param null $currCurrency
     * @param null $category
     * @param array $excepts
     * @return array
     */
    function filterProducts($request, $currComponent, $perPage, $currCurrency = null, $category = null, $excepts = ['page'])
    {
        return ProductsHelpers::filterProducts($request, $currComponent, $perPage, $currCurrency, $category, $excepts);
    }
}

if (!function_exists('makeSortFilter')) {
    /**
     * @param $request
     * @param $currComponent
     * @param null $category
     * @return string|null
     */
    function makeSortFilter($request, $currComponent, $category = null)
    {
        return ProductsHelpers::makeSortFilter($request, $currComponent, $category);
    }
}

if (!function_exists('makeProductVariations')) {
    /**
     * @param $product
     * @param null $itemVariation
     * @param array $defaultValuesFromVariations
     * @param string $scope (product_page, product_popup, product_card)
     * @return string|null
     */
    function makeProductVariations($product, $itemVariation = null, $defaultValuesFromVariations = [], $scope='product_page', $options=[])
    {
        return ProductsHelpers::makeProductVariations($product, $itemVariation, $defaultValuesFromVariations, $scope, $options);
    }
}

if (!function_exists('defineProductVariations')) {
    /**
     * @param $product
     * @param null $itemVariation
     * @param bool $forMakeProductVariations
     * @return array|null
     */
    function defineProductVariations($product, $itemVariation = null, $forMakeProductVariations = true)
    {
        return ProductsHelpers::defineProductVariations($product, $itemVariation, $forMakeProductVariations);
    }
}

if (!function_exists('getProductDefaultVariation')) {
    /**
     * @param $product
     * @param null $variationId
     * @param null $allProductVariations
     * @return mixed
     */
    function getProductDefaultVariation($product, $variationId = null, $allProductVariations = null)
    {
        return ProductsHelpers::getProductDefaultVariation($product, $variationId, $allProductVariations);
    }
}

if (!function_exists('recentViewedProducts')) {
    /**
     * @param null $item
     * @param int $countItems
     * @return mixed
     */
    function recentViewedProducts($item = null, $countItems = 12)
    {
        return ProductsHelpers::recentViewedProducts($item, $countItems);
    }
}

if (!function_exists('similarProducts')) {
    /**
     * @param null $item
     * @param int $countItems
     * @return mixed
     */
    function similarProducts($item, $countItems = 12)
    {
        return ProductsHelpers::similarProducts($item, $countItems);
    }
}

if (!function_exists('recommendedProducts')) {
    /**
     * @param null $item
     * @param int $countItems
     * @return mixed
     */
    function recommendedProducts($item, $countItems = 12)
    {
        return ProductsHelpers::recommendedProducts($item, $countItems);
    }
}

if (!function_exists('exportProductsData')) {
    /**
     * @param $product
     * @param $attributes
     * @return object
     */
    function exportProductsData($product, $attributes)
    {
        return ProductsHelpers::exportProductsData($product, $attributes);
    }
}

//*********************** End Products functions ***********************//


//*********************** Start Orders functions ***********************//

if (!function_exists('getProductCartAttributes')) {
    /**
     * @param $product
     * @param $attributes
     * @return \Illuminate\Support\Collection
     */
    function getProductCartAttributes($product, $attributes)
    {
        return OrdersHelpers::getProductCartAttributes($product, $attributes);
    }
}
if (!function_exists('displayProductAttributes')) {
    /**
     * @param $product
     * @return string
     */
    function displayProductAttributes($product)
    {
        return OrdersHelpers::displayProductAttributes($product);
    }
}

//*********************** End Orders functions ***********************//

/**
 * @return string
 */
function getAppDomain()
{
    return @parse_url(config('app.url'), PHP_URL_HOST) ?: '';
}

/**
 * @param $string
 * @return string
 */
function trimString($string)
{
    return trim(preg_replace("/\s*(?:\*\/|\?>).*/", '', $string));
}

/**
 * @param $object
 * @return array
 */
function objectToArray($object)
{

    if (is_object($object))
        $object = get_object_vars($object);

    return is_array($object) ? array_map(__FUNCTION__, $object) : $object;
}

/**
 * @param $array
 * @return object
 */
function arrayToObject($array)
{
    return is_array($array) ? (object)array_map(__FUNCTION__, $array) : $array;
}

/**
 * @param $value
 * @param int $decimals
 * @param string $decPoint
 * @param string $separator
 * @param bool $ignoreZero
 * @return float
 */
function numberFormat($value, $decimals = 2, $decPoint = '.', $separator = '', $ignoreZero = false)
{
    return $value > 0 || $ignoreZero ? number_format((float)$value, $decimals, $decPoint, $separator) : 0;
}

/**
 * @param array $except
 * @return bool|null|string
 */
function queryToString($except = [])
{
    $response = null;

    if (!request()->ajax()) {
        if (empty($except))
            $response = request()->getQueryString();
        else {
            $rq = array_filter(request()->except($except));
            if (!empty($rq)) {
                foreach ($rq as $key => $q) {
                    if (is_string($q))
                        $response .= "&{$key}={$q}";
                    else if (is_array($q))
                        $response .= "&{$key}=[" . implode(',', $q) . "]";
                }

                $response = substr($response, 1);
            }
        }
    }

    return $response;
}

/**
 * @param $phone
 * @return bool|null|string|string[]
 */
function clearPhone($phone)
{
    if (is_null($phone) || empty($phone))
        return false;

    return preg_replace('/[^\+?0-9]/', '', $phone);
}

/**
 * @param $phones
 * @param bool $returnHtml
 * @param string $delimiter
 * @return array|string
 */
function parseManyPhones($phones, $returnHtml = true, $delimiter = "&nbsp;&#47;&nbsp;")
{
    $response = [];
    if (is_null($phones) || empty($phones))
        return $response;

    $phonesArr = explode(',', $phones);

    if ($returnHtml && !empty($phonesArr)) {
        $response = '';
        foreach ($phonesArr as $key => $phone) {
            $response .= "<a href='tel:" . clearPhone($phone) . "'>{$phone}</a>" . (count($phonesArr) != $key + 1 ? "{$delimiter}" : "");
        }
    } else
        $response = $phonesArr;

    return $response;
}

/**
 * @param $url
 * @return array
 */
function getPreviousUrl($url)
{
    $lastUrl = str_replace(url('/'), '', $url);

    $lastUrlArr = explode('/', substr($lastUrl, 1));

    if (request()->segment(2) == 'admin') {
        if (!in_array('logout', $lastUrlArr) && !in_array('login', $lastUrlArr) && in_array('admin', $lastUrlArr) && count($lastUrlArr) > 1) {
            if (!empty($lastUrlArr) && array_key_exists(0, $lastUrlArr))
                unset($lastUrlArr[0]);
        } else
            $lastUrlArr = ['admin'];
    } else {
        if (!in_array('logout', $lastUrlArr) && !in_array('login', $lastUrlArr)) {
            if (!empty($lastUrlArr) && array_key_exists(0, $lastUrlArr))
                unset($lastUrlArr[0]);
        } else
            $lastUrlArr = [];
    }

    return $lastUrlArr;
}

/**
 * @param $string
 * @param $count
 * @return string
 */
function subStrText($string, $count)
{
    if (strlen($string) > $count) {
        $finStringCount = strpos($string, ' ', $count);
        $response = substr($string, 0, $finStringCount) . '...';
    } else
        $response = $string;

    return $response;
}

/**
 * @param $string
 * @param $count
 * @param string $needle
 * @return string
 */
function subStrTextNeedle($string, $count, $needle = '.')
{

    if (strlen($string) > $count) {
        $response = strip_tags(substr($string, 0, $count));
        if (($string = strstr($response, $needle, true)) !== false)
            $response = $string . '.';
    } else
        $response = $string;

    $response = html_entity_decode($response);

    return $response;
}


/**
 * @param $bytes
 * @return string
 */
function formatSizeUnits($bytes)
{

    if ($bytes >= 1073741824)
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    elseif ($bytes >= 1048576)
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    elseif ($bytes >= 1024)
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    elseif ($bytes > 1)
        $bytes = $bytes . ' bytes';
    elseif ($bytes == 1)
        $bytes = $bytes . ' byte';
    else
        $bytes = '0 bytes';

    return $bytes;
}

/**
 * @param $files
 * @return array
 */
function getAllFilesLocation($files)
{

    $response = [];

    if (!$files->isEmpty()) {
        foreach ($files as $file) {

            if (!empty($paths = getAllFileLocationsByUrl(@$file->file->file)))
                $response = array_merge($response, $paths);
        }
    }

    return $response;
}

/**
 * @param $fileLocation
 * @param string $responseType
 * @return array
 */
function getAllFileLocationsByUrl($fileLocation, $responseType = null)
{
    $response = [];

    if (!is_null($fileLocation) && !empty($fileLocation)) {
        $fileLocation = @parse_url(urlencode($fileLocation))['path'];
        $fileExtension = strtolower(pathinfo($fileLocation, PATHINFO_EXTENSION));

        if (substr($fileLocation, 0, 1) == '/')
            $fileLocation = substr($fileLocation, 1);

        $filePath = explode('/', urldecode($fileLocation));

        if (array_key_exists(2, $filePath) && array_key_exists(0, $filePath) && $filePath[0] != 'admin-assets') {
            if ($filePath[2] == 'images') {

                $destinationPath = "{$filePath[0]}/{$filePath[1]}/{$filePath[2]}";

                if ($responseType == 'magic') {
                    if ($fileExtension != 'svg' && $fileExtension != 'ico')
                        $response = [
                            'small' => "{$destinationPath}/small/{$filePath[3]}",
                            'medium' => "{$destinationPath}/medium/{$filePath[3]}",
                            'large' => "{$destinationPath}/large/{$filePath[3]}",
                            'original' => "{$destinationPath}/{$filePath[3]}",
                        ];
                    else
                        $response = [
                            'original' => "{$destinationPath}/{$filePath[3]}",
                        ];


                } else {
                    if ($fileExtension != 'svg' && $fileExtension != 'ico')
                        $response = [
                            "{$destinationPath}/small/{$filePath[3]}",
                            "{$destinationPath}/medium/{$filePath[3]}",
                            "{$destinationPath}/large/{$filePath[3]}",
                            "{$destinationPath}/{$filePath[3]}",
                        ];
                    else
                        $response = [
                            "{$destinationPath}/{$filePath[3]}",
                        ];
                }

            } else {
                $destinationPath = "{$filePath[0]}/{$filePath[1]}";

                if ($responseType == 'magic')
                    $response = [
                        'original' => "{$destinationPath}/{$filePath[2]}"
                    ];
                else
                    $response = [
                        "{$destinationPath}/{$filePath[2]}"
                    ];
            }
        } else {
            if ($responseType == 'magic')
                $response = [
                    'original' => $fileLocation
                ];
            else
                $response = [
                    $fileLocation
                ];
        }
    }

    return $response;
}

/**
 * @param $fileLocation
 * @return mixed
 */
function getCleanFileName($fileLocation)
{
    $extension = pathinfo($fileLocation, PATHINFO_EXTENSION);
    $fileName = basename($fileLocation);

    return str_replace(".{$extension}", "", snake_case($fileName));
}

/**
 * @param null $path
 * @param array $qs
 * @param null $secure
 * @return string
 */

function qsUrl($path = null, $qs = [], $secure = null)
{
    $url = app('url')->to($path, $secure);
    if (count($qs)) {
        foreach ($qs as $key => $value) {
            $qs[$key] = sprintf('%s=%s', $key, urlencode($value));
        }
        $url = sprintf('%s?%s', $url, implode('&', $qs));
    }
    return $url;
}

/**
 * @param $phone
 * @return string
 */
function formatPhoneNumbers($phone)
{
    if (preg_match('/^(\+)?(\d{3})(\d{2})(\d{2,})$/', $phone, $matches))
        $result = $matches[1] . $matches[2] . '-' . $matches[3] . '-' . $matches[4];
    else
        $result = $phone;

    return $result;
}

/**
 * @param $file
 * @return bool|string
 */
function svgFileToCode($file)
{

    $file = @substr(parse_url($file)['path'], 1);

    if (file_exists($file) && pathinfo(basename($file), PATHINFO_EXTENSION) == 'svg')
        $response = @file_get_contents($file);
    else
        $response = @file_get_contents('admin-assets/img/small-icons/side-no-icon.svg');

    $find_string = '<svg';
    $position = strpos($response, $find_string);

    $response = @preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", substr($response, $position));

    return $response;

}

/**
 * @param $file
 * @return string
 */
function getFileType($file)
{
    return @strpos(mime_content_type($file), 'image') !== false || pathinfo($file, PATHINFO_EXTENSION) == 'svg' || pathinfo($file, PATHINFO_EXTENSION) == 'ico' ? 'image' : 'file';
}

/**
 * @param $item
 * @param string $size
 * @param string $fileAppend
 * @param bool $showNoImg
 * @return array|bool
 */
function getItemFile($item, $size = 'original', $showNoImg = true, $fileAppend = 'firstFile')
{
    $response = false;
    $files = @$item->$fileAppend;

    if (is_null($item) || !$files)
        return $response;

    try {
        if (is_array($files)) {
            foreach ($files as $file) {
                $customFile = @$file->$size ? $file->$size : @$file->original;

                $isNoImg = strpos($customFile, 'no-image') === false;

                if ($customFile && ($isNoImg || (!$isNoImg && $showNoImg)))
                    $response[] = $customFile;
            }
        } elseif (is_object($files)) {
            $customFile = @$files->$size ? $files->$size : @$files->original;

            $isNoImg = strpos($customFile, 'no-image') === false;

            if ($customFile && ($isNoImg || (!$isNoImg && $showNoImg)))
                $response = $customFile;

        }
    } catch (\Exception $e) {
    }

    return $response;
}

/**
 * @param $string
 * @return string
 */
function customToken($string)
{
    $alphas = range('a', 'z');
    $numbers = range('0', '9');
    $concat = (count($alphas) - count($numbers)) / 2 - 1;
    $convertedStr = '';
    $string = base64_decode($string);

    foreach (str_split($string) as $item) {

        if (is_numeric($item)) {

            $existKey = array_search($item, $numbers);

            if ($existKey || $existKey === 0) {
                $concatKey = $existKey - $concat;

                if ((count($numbers) - 1) > $concatKey)
                    $nextKey = (count($numbers) + $concatKey);
                else
                    $nextKey = $concatKey;

                $nextKey = $nextKey < 0 ? $nextKey * (-1) : $nextKey;

                if ((count($numbers) - 1) < $nextKey)
                    $nextKey = (count($numbers) - $nextKey) * (-1);

                $convertedStr .= $numbers[$nextKey];
            } else
                $convertedStr .= $item;

        } elseif (is_string($item)) {
            $existKey = array_search($item, $alphas);

            if ($existKey || $existKey === 0) {
                $concatKey = $existKey - $concat;

                if ((count($alphas) - 1) > $concatKey)
                    $nextKey = (count($alphas) + $concatKey);
                else
                    $nextKey = $concatKey;

                $nextKey = $nextKey < 0 ? $nextKey * (-1) : $nextKey;

                if ((count($alphas) - 1) < $nextKey)
                    $nextKey = (count($alphas) - $nextKey) * (-1);

                $convertedStr .= $alphas[$nextKey];
            } else
                $convertedStr .= $item;
        }

    }

    return $convertedStr;
}

function getAdminTabName($currComponent)
{
    $isAdminPanel = request()->segment(2) == 'admin';
    $action = request()->segment(4);

    if (!$isAdminPanel || !@$currComponent->globalName)
        return false;

    switch ($action) {
        case 'create':
            $response = __("e.nav_create_item") . " | {$currComponent->globalName->name}";
            break;
        case 'edit':
            $response = __("e.nav_edit_item") . " | {$currComponent->globalName->name}";
            break;
        case 'view':
            $response = __("e.nav_view_item") . " | {$currComponent->globalName->name}";
            break;
        case 'trash':
            $response = __("e.nav_trash_item") . " | {$currComponent->globalName->name}";
            break;
        default:
            $response = $currComponent->globalName->name;
    }

    return $response;
}


/**
 * @param $parents
 * @param null $currId
 * @param null $currItemId
 * @return string
 */
function getParentChildrenInSelect($parents, $currId = null, $currItemId = null)
{

    $response = "";
    if ($parents && $parents->isNotEmpty()) {
        foreach ($parents as $parent) {
            $selected = (is_array($currId) && in_array($parent->id, $currId)) || $parent->id == $currId ? 'selected' : '';
            $repeatSymbol = $parent->level > 1 ? "&nbsp;–" : "";

            if (((!is_null($currItemId) && $parent->id != $currItemId) || is_null($currItemId))) {
                $recursiveChildren = $parent->recursiveChildren;
                $children = $recursiveChildren ? $recursiveChildren->where('active', 1) : collect();
                $response .= "<option value='{$parent->id}' {$selected}>" . str_repeat($repeatSymbol, ($parent->level ?: 1) - 1) . "&nbsp;" . @$parent->globalName->name . "</option>" . getParentChildrenInSelect($children, $currId);
            }

        }
    }

    return $response;
}

/**
 * @param $parents
 * @param $rights
 * @return string
 */
function getParentChildrenInTable($parents, $rights)
{

    $response = "";
    if (!$parents->isEmpty()) {
        foreach ($parents as $parent) {
            $repeatSymbol = $parent->level > 1 ? "&nbsp;&nbsp;&#8211;" : "";
            $response .= "<tr id='{$parent->id}' class='inheritBackground'><td class='regular left'><span>" . str_repeat($repeatSymbol, $parent->level - 1) . "&nbsp;" . @$parent->globalName->name . "</span></td><td class='labelForCheckAll'><label class=\"checkboxLabel\" for='check-all-rights-" . $parent->id . "'>Check all <input autocomplete='off' type='checkbox' name='check-all-rights-" . $parent->id . "' id='check-all-rights-" . $parent->id . "' class='check-all-rights check' " . (array_key_exists($parent->id, $rights) && $rights[$parent->id]['for_create'] && $rights[$parent->id]['for_edit'] && $rights[$parent->id]['for_delete'] ? 'checked' : '') . "><span class=\"check\"></span></label></td >";
            $response .= "<td class='checkItems'><input autocomplete='off' type='checkbox' name='rights[{$parent->id}][for_create]' id='rights[{$parent->id}][for_create]' class='for_create checkItem' " . (array_key_exists($parent->id, $rights) && $rights[$parent->id]['for_create'] ? 'checked' : '') . "><label for='rights[{$parent->id}][for_create]'></label></td>";
            $response .= "<td class='checkItems'><input autocomplete='off' type='checkbox' name='rights[{$parent->id}][for_edit]' id='rights[{$parent->id}][for_edit]' class='for_edit checkItem'" . (array_key_exists($parent->id, $rights) && $rights[$parent->id]['for_edit'] ? 'checked' : '') . "><label for='rights[{$parent->id}][for_edit]'></label></td>";
            $response .= "<td class='checkItems'><input autocomplete='off' type='checkbox' name='rights[{$parent->id}][for_delete]' id='rights[{$parent->id}][for_delete]' class='for_delete checkItem'" . (array_key_exists($parent->id, $rights) && $rights[$parent->id]['for_delete'] ? 'checked' : '') . "><label for='rights[{$parent->id}][for_delete]'></label></td>";
            $response .= "<td class='checkItems'><input autocomplete='off' type='checkbox' name='rights[{$parent->id}][for_view]' id='rights[{$parent->id}][for_view]' class='for_view checkItem'" . (array_key_exists($parent->id, $rights) && $rights[$parent->id]['for_view'] ? 'checked' : '') . "><label for='rights[{$parent->id}][for_view]'></label></td>";
            $response .= "<td class='checkItems'><input autocomplete='off' type='checkbox' name='rights[{$parent->id}][for_active]' id='rights[{$parent->id}][for_active]' class='for_active checkItem'" . (array_key_exists($parent->id, $rights) && $rights[$parent->id]['for_active'] ? 'checked' : '') . "><label for='rights[{$parent->id}][for_active]'></label></td>";
            $response .= "</tr>";
            $response .= getParentChildrenInTable($parent->recursiveChildren, $rights);
        }
    }

    return $response;
}

/**
 * @param $path
 * @param $extension
 * @param string $search
 * @return array|bool
 */
function parseFilesInPath($path, $extension, $search = '*')
{

    $path = str_replace('.', '/', $path);
    $response = [];

    if (empty($path))
        return false;

    $filesPath = "views/{$path}/{$search}{$extension}";
    $files = glob(resource_path($filesPath));

    if (empty($files))
        return false;

    foreach ($files as $file) {
        $template_data = implode('', file($file));

        if (preg_match("|Template name:(.*)$|mi", $template_data, $name))
            $response[] = (object)[
                'key' => @str_replace('.blade', '', pathinfo($file, PATHINFO_FILENAME)),
                'value' => trimString(@$name[1])
            ];

    }

    return $response;
}

/**
 * @param $path
 * @param $fileName
 * @return bool|mixed|null
 */
function findFileInPath($path, $fileName)
{

    $path = str_replace('.', '/', $path);

    if (empty($path) || is_null($fileName))
        return null;

    $filesPath = "views/{$path}/{$fileName}.blade.php";

    return file_exists(resource_path($filesPath)) ? str_replace('.blade.php', '', $fileName) : null;
}

/**
 * @param $path
 * @param $moduleName
 * @param $items
 * @return string
 * @throws Throwable
 */
function getSortableMenu($path, $moduleName, $items)
{

    $response = "";
    if ($items->isNotEmpty()) {
        foreach ($items as $key => $item) {
            $children = $item->recursiveChildren;
            $countChild = $children->count();

            $response .= view("{$path}.sortableMenuList", ['item' => $item, 'path' => $path, 'moduleName' => $moduleName])->render();

            if ($countChild > 0) {
                $response .= "<ul class=\"sortable-menu-list\">";
                $response .= getSortableMenu($path, $moduleName, $children);
            }

            if ($key == count($items) - 1)
                $response .= "</ul>";
        }
        $response .= "</li>";

    }

    return $response;
}

/**
 * @param $item
 * @return null
 */
function getMenuCurrName($item)
{
    $response = null;

    $itemGlobalName = $item->globalName;
    if (!is_null($itemGlobalName) && !is_null(@$itemGlobalName->name))
        $response = @$itemGlobalName->name;

    if (is_null($item->component_id) && is_null($item->item_id)) {
        $itemByLang = $item->itemByLang;

        if (!is_null($itemByLang))
            $response = @$itemByLang->name;
    }

    if (!is_null($item->component_id)) {
        $component = $item->componentId;

        if (!is_null($component) && $component->for_menu)
            if (is_null($item->item_id))
                if (is_null($response))
                    $response = @$component->globalName->name;
    }

    return $response;
}

/**
 * @param $url
 * @return string
 */
function correctCustomMenuLink($url)
{
    $url = trim($url, '/');

    if (!preg_match('#^http(s)?://#', $url)) {
        $url = 'http://' . $url;
    }

    return $url;
}

/**
 * @param $array
 * @return array
 */
function arrayCartesian($array)
{
    $array = array_filter($array);
    $results = $indexes = [];
    $index = 0;

    // Generate indexes from keys and values so we have a logical sort order.
    foreach ($array as $key => $values) {
        foreach ($values as $value) {
            $indexes[$key][$value] = $index++;
        }
    }

    // Loop over the 2D array of indexes and generate all combinations.
    foreach ($indexes as $key => $values) {
        // When result is empty, fill with the values of the first looped array.
        if (empty($results)) {
            foreach ($values as $value) {
                $results[] = [$key => $value];
            }
        } else {
            // Second and subsequent input sub-array merging.
            foreach ($results as $result_key => $result) {
                foreach ($values as $value) {
                    // If the key is not set, we can set it.
                    if (!isset($results[$result_key][$key])) {
                        $results[$result_key][$key] = $value;
                    } else {
                        // If the key is set, we can add a new combination to the results array.
                        $new_combination = $results[$result_key];
                        $new_combination[$key] = $value;
                        $results[] = $new_combination;
                    }
                }
            }
        }
    }

    // Sort the indexes.
    arsort($results);

    // Convert indexes back to values.
    foreach ($results as $result_key => $result) {
        $converted_values = [];

        // Sort the values.
        arsort($results[$result_key]);

        // Convert the values.
        foreach ($results[$result_key] as $key => $value) {
            $converted_values[$key] = array_search($value, $indexes[$key], true);
        }

        $results[$result_key] = $converted_values;
    }

    return array_reverse($results);
}

/**
 * @param $aArray1
 * @param $aArray2
 * @return array
 */
function arrayDiff($aArray1, $aArray2)
{
    $response = [];

    foreach ($aArray1 as $mKey => $mValue) {

        if (is_array($mValue))
            if (!in_array($mValue, $aArray2)) {
                $response[$mKey] = $mValue;
            }
    }

    return $response;
}

/**
 * @param $text
 * @param int $length
 * @param string $ending
 * @param bool $exact
 * @param bool $considerHtml
 * @return bool|string
 */
function truncate($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true)
{
    if ($considerHtml) {
        // if the plain text is shorter than the maximum length, return the whole text
        if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
            return $text;
        }
        // splits all html-tags to scanable lines
        preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
        $total_length = strlen($ending);
        $open_tags = array();
        $truncate = '';
        foreach ($lines as $line_matchings) {
            // if there is any html-tag in this line, handle it and add it (uncounted) to the output
            if (!empty($line_matchings[1])) {
                // if it's an "empty element" with or without xhtml-conform closing slash
                if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                    // do nothing
                    // if tag is a closing tag
                } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                    // delete tag from $open_tags list
                    $pos = array_search($tag_matchings[1], $open_tags);
                    if ($pos !== false) {
                        unset($open_tags[$pos]);
                    }
                    // if tag is an opening tag
                } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                    // add tag to the beginning of $open_tags list
                    array_unshift($open_tags, strtolower($tag_matchings[1]));
                }
                // add html-tag to $truncate'd text
                $truncate .= $line_matchings[1];
            }
            // calculate the length of the plain text part of the line; handle entities as one character
            $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));

            if ($total_length + $content_length > $length) {
                // the number of characters which are left
                $left = $length - $total_length;
                $entities_length = 0;
                // search for html entities
                if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                    // calculate the real length of all entities in the legal range
                    foreach ($entities[0] as $entity) {
                        if ($entity[1] + 1 - $entities_length <= $left) {
                            $left--;
                            $entities_length += strlen($entity[0]);
                        } else {
                            // no more characters left
                            break;
                        }
                    }
                }
                $truncate .= substr($line_matchings[2], 0, $left + $entities_length);
                // maximum lenght is reached, so get off the loop
                break;
            } else {
                $truncate .= $line_matchings[2];
                $total_length += $content_length;
            }
            // if the maximum length is reached, get off the loop
            if ($total_length >= $length) {
                break;
            }
        }
    } else {
        if (strlen($text) <= $length) {
            return $text;
        } else {
            $truncate = substr($text, 0, $length - strlen($ending));
        }
    }
    // if the words shouldn't be cut in the middle...
    if (!$exact) {
        // ...search the last occurance of a space...
        $spacepos = strrpos($truncate, ' ');
        if (isset($spacepos)) {
            // ...and cut the text in this position
            $truncate = substr($truncate, 0, $spacepos);
        }
    }
    // add the defined ending to the text

    $truncate .= $ending;
    if ($considerHtml) {
        // close all unclosed html-tags
        foreach ($open_tags as $tag) {
            $truncate .= '</' . $tag . '>';
        }
    }
    return $truncate;
}

/**
 * @param array $array
 * @param callable|null $callback
 * @return array
 */
function arrayFilterRecursive(array $array, callable $callback = null)
{

    $array = is_callable($callback) ? array_filter($array, $callback) : array_filter($array);
    foreach ($array as &$value) {
        if (is_array($value)) {
            $value = call_user_func(__FUNCTION__, $value, $callback);
        }
    }

    return $array;
}

/**
 * @param $request
 * @param $requestAll
 * @param bool $useGetParams
 * @return object
 */
function filterTableList($request, $requestAll, $useGetParams = false)
{

    $filterParams = array_filter($requestAll);

    $filterParams = array_map(function ($val) {
        return is_array($val) ? array_filter($val) : $val;
    }, $filterParams);

    if ((!$request->ajax() && !$useGetParams) || ($request->ajax() && $useGetParams))
        $filterParams = parseArrayQueryParams($filterParams);

    $pushUrl = '';

    if (!empty($filterParams)) {
        foreach ($filterParams as $key => $one_filter_el) {

            if (is_array($one_filter_el)) {
                $pushUrlArr = '';
                foreach ($one_filter_el as $k => $filter_el) {
                    $pushUrlArr .= $filter_el . ',';
                }

                $pushUrl .= $key . '=[' . strip_tags(substr($pushUrlArr, 0, -1)) . ']&';
            } else {
                $pushUrl .= $key . '=' . strip_tags(str_replace('[]', '', $one_filter_el)) . '&';
            }
        }

        $pushUrl = '?' . substr($pushUrl, 0, -1);
    }

    $response = [
        'filterParams' => $filterParams,
        'pushUrl' => $pushUrl
    ];

    return (object)$response;
}

/**
 * @param $requestParams
 * @param string $getSpecificKey
 * @return array
 */
function parseArrayQueryParams($requestParams, $getSpecificKey = '')
{
    $params = array_filter($requestParams);

    $params = array_map(function ($val) {
        return is_array($val) ? array_filter($val) : $val;
    }, $params);

    $newParams = [];

    if (!empty($params) && count($params) > 0) {
        foreach ($params as $key => $one_filter_elem) {
            $newParams[$key] = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $one_filter_elem);
            if (!is_array($one_filter_elem)) {
                if(strpos($one_filter_elem, '[') !== false || strpos($one_filter_elem, ']') !== false)
                    $newParams[$key] = explode(',', substr($params[$key], 1, -1));
            } else
                $newParams[$key] = $one_filter_elem;
        }
    }

    return $getSpecificKey ? (array)@$newParams[$getSpecificKey] : $newParams;
}

/**
 * @param $allSettings
 * @param string $class
 * @return string
 */
function getLogo($allSettings, $class = '')
{
    return "<img src='" . (!is_null(@helpers()->getSettingsField("files", $allSettings)->firstFile->original) ? helpers()->getSettingsField("files", $allSettings)->firstFile->original : asset("assets/img/logo.png")) . "' alt='" . @getSettingsField('site_title', $allSettings, true) . "' title='" . @getSettingsField('site_title', $allSettings, true) . "' " . ($class ? "class='$class'" : "") . ">";
}

/**
 * @param $langList
 * @param null $exceptId
 * @return string
 */
function getLanguagesList($langList, $exceptId = null)
{
    $response = '';
    if (!is_null($exceptId))
        $langList = $langList->where('id', '!=', $exceptId);
    if ($langList->isNotEmpty())
        foreach ($langList as $lang) {
            $response .= "<a class='dropdown__item' href='".url($lang->slug, Arr::except(request()->segments(), 0)) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')."'>";
            $response .= "<div class='img-container'>";
                $response .= "<img src='".asset(@$lang->file->file->file)."' alt=''>";
            $response .= "</div>";
            $response .= "<span>".$lang->name."</span>";
            $response .= "</a>";
        }
    return $response;
}

/**
 * @param $product
 * @param null $currCurrency
 * @param bool $priceForCart
 * @param array $options
 * @param bool $useItemProps
 * @return string
 */
function getProductPrices($product, $currCurrency = null, $priceForCart = false, $options = [], $useItemProps = false)
{
    $response = '';

    $finPrice = @$product->finPrice;

    $defaultOptions = [
        'priceClass' => 'price',
        'priceTag' => 'div',
        'salePriceClass' => 'oldPrice',
        'salePriceTag' => 'div',
    ];

    if (!$product || !$finPrice)
        return $response;

    if (empty($options))
        $options = $defaultOptions;
    else
        $options = array_merge($defaultOptions, $options);

    if (@$finPrice->minPrice != @$finPrice->maxPrice) {
        if (@$product->isVariationProduct && !$priceForCart)
            $response .= "<{$options['priceTag']} class='{$options['priceClass']}' content='" . convertPrice($finPrice->minPrice, $currCurrency) . "'>" . formatPrice($finPrice, $currCurrency, true, $useItemProps) . "</{$options['priceTag']}>";
        else
            if (@$finPrice->minPrice) {
                $response .= "<{$options['priceTag']} class='{$options['priceClass']} ' content='" . convertPrice($finPrice->minPrice, $currCurrency) . "'>" . formatPrice($finPrice->minPrice, $currCurrency, true, $useItemProps) . "</{$options['priceTag']}>";
                $response .= "<{$options['salePriceTag']} class='{$options['salePriceClass']}' content='" . convertPrice($finPrice->maxPrice, $currCurrency) . "'>" . formatPrice($finPrice->maxPrice, $currCurrency, true, $useItemProps) . "</{$options['salePriceTag']}>";
            } elseif (@$finPrice->maxPrice)
                $response .= "<{$options['priceTag']} class='{$options['priceClass']}' content='" . convertPrice($finPrice->maxPrice, $currCurrency) . "'>" . formatPrice($finPrice->maxPrice, $currCurrency, true, $useItemProps) . "</{$options['priceTag']}>";

    } else
        $response .= "<{$options['priceTag']} class='{$options['priceClass']}' content='" . convertPrice($finPrice->maxPrice, $currCurrency) . "'>" . formatPrice($finPrice->maxPrice, $currCurrency, true, $useItemProps) . "</{$options['priceTag']}>";

    $responseWrap = '';
    //if (is_object($finPrice) && @$finPrice->minPrice && @$finPrice->maxPrice && config('cms.product.displayRangePrice', false))
    //    $responseWrap .= "<span itemprop='offers' itemscope itemtype='http://schema.org/AggregateOffer'>";
    //else
    //    $responseWrap .= "<span itemprop='offers' itemscope itemtype='http://schema.org/Offer'>";

    $responseWrap .= "{$response}";
    //$responseWrap .= "</span>";

    return $responseWrap;
}

/**
 * @param array $parameters
 * @param null $secure
 * @param bool $useLang
 * @return \Illuminate\Contracts\Routing\UrlGenerator|string
 */
function customUrl($parameters = [], $secure = null, $useLang = true)
{
    if (is_string($parameters))
        $parameters = explode('/', $parameters);

    return url(($useLang ? LANG : ''), $parameters, $secure);
}

/**
 * @param array $parameters
 * @param null $secure
 * @param bool $userLang
 * @return \Illuminate\Contracts\Routing\UrlGenerator|string
 */
function adminUrl($parameters = [], $secure = null, $userLang = true)
{
    if (is_string($parameters))
        $parameters = explode('/', $parameters);

    array_unshift($parameters, 'admin');

    return url(($userLang ? LANG : ''), $parameters, $secure);
}


/**s
 * @param $url
 * @return bool|string
 */
function getYoutubeCode($url)
{

    $response = false;

    if ($youtubePos = strpos($url, "youtube.com/v/")) {
        $response = substr($url, $youtubePos + 14, 11);
    } elseif ($youtubePos = strpos($url, "youtube.com/watch?v="))
        $response = substr($url, $youtubePos + 20, 11);
    elseif ($youtubePos = strpos($url, "youtube.com/embed/"))
        $response = substr($url, $youtubePos + 18, 11);
    elseif ($youtubePos = strpos($url, "youtube-nocookie.com/embed/"))
        $response = substr($url, $youtubePos + 27, 11);
    elseif ($youtubePos = strpos($url, "youtu.be/"))
        $response = substr($url, $youtubePos + 9, 11);

    return $response;
}

/**
 * @param $status
 * @return mixed
 */
function transCartStatus($status)
{

    $response = $status;

    if ($status == 'checkout')
        $response = $status;
    elseif ($status == 'cancelled')
        $response = $status;
    elseif ($status == 'approved')
        $response = $status;

    return $response;
}

/**
 * @param $items
 * @param bool $isRecursion
 * @return \Illuminate\Support\Collection
 */
function formatArrayRecursive($items, $isRecursion = false)
{

    $response = $children = collect();

    if (!empty($items))
        foreach ($items as $key => $subItems) {
            $pushCollection = [];

            if ($isRecursion && !is_array($subItems))
                $pushCollection = (object)[
                    'id' => $key,
                    'value' => $subItems,
                    'children' => collect()
                ];

            if (is_array($subItems) && !empty($subItems))
                foreach ($subItems as $k => $subItem) {

                    $pushCollection[$k] = (object)[
                        'id' => $k,
                        'value' => $subItem,
                        'children' => collect()
                    ];

                    if (is_array($subItem)) {
                        $children = formatArrayRecursive($subItem, true);
                        $pushCollection[$k]->children = $children;
                    }
                }

            $response->put($key, $pushCollection);

        }

    return $response;
}

/**
 * @param $expression
 * @param bool $return
 * @return mixed|string|string[]|null
 */
function varExport($expression, $return = FALSE)
{
    $export = var_export($expression, TRUE);
    $export = preg_replace("/^([ ]*)(.*)/m", '$1$1$2', $export);
    $array = preg_split("/\r\n|\n|\r/", $export);
    $array = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [NULL, ']$1', ' => ['], $array);
    $export = join(PHP_EOL, array_filter(["["] + $array));
    if ((bool)$return)
        return $export;
    else
        echo $export;
}

/**
 * @param $item
 * @param bool $isRecursion
 * @return bool
 */
function recursiveChildrenDestroy($item, $isRecursion = false)
{

    $children = @$item->recursiveChildren;

    if ($children && $children->isNotEmpty())
        foreach ($children as $child) {
            if ($child->recursiveChildren->isNotEmpty())
                recursiveChildrenDestroy($child, true);

            $child->items()->delete();
            $child->delete();
        }

    if ($isRecursion) {
        $item->items()->delete();
        $item->delete();
    }

    return true;
}

/**
 * @param $ip
 * @return null|object
 */
function getLocation($ip = '')
{
    $position = Location::get($ip);
    $response = Countries::where('iso', $position->countryCode)->first();

    return $response;
}

/**
 * @param null $url
 * @return mixed
 */
function parseURL($url = null)
{

    if (is_null($url))
        $url = url()->current();

    $url = substr($url, 0, 4) == 'http' ? $url : 'http://' . $url;
    if ($urlData = parse_url(str_replace('&amp;', '&', $url))) {
        $path_parts = pathinfo($urlData['host']);
        $tmp = explode('.', $urlData['host']);
        $n = count($tmp);
        if ($n >= 2) {
            if ($n == 4 || ($n == 3 && strlen($tmp[($n - 2)]) <= 3)) {
                $urlData['domain'] = $tmp[($n - 3)] . "." . $tmp[($n - 2)] . "." . $tmp[($n - 1)];
                $urlData['tld'] = $tmp[($n - 2)] . "." . $tmp[($n - 1)];
                $urlData['root'] = $tmp[($n - 3)];
                $urlData['subdomain'] = (($n == 4) ? $tmp[0] : ($n == 3 && strlen($tmp[($n - 2)]) <= 3)) ? $tmp[0] : '';
            } else {
                $urlData['domain'] = $tmp[($n - 2)] . "." . $tmp[($n - 1)];
                $urlData['tld'] = $tmp[($n - 1)];
                $urlData['root'] = $tmp[($n - 2)];
                $urlData['subdomain'] = $n == 3 ? $tmp[0] : '';
            }
        }
        //$urlData['dirname'] = $path_parts['dirname'];
        $urlData['basename'] = $path_parts['basename'];
        $urlData['filename'] = $path_parts['filename'];
        $urlData['extension'] = $path_parts['extension'];
        $urlData['base'] = $urlData['scheme'] . "://" . $urlData['host'];
        $urlData['abs'] = (isset($urlData['path']) && strlen($urlData['path'])) ? $urlData['path'] : '/';
        $urlData['abs'] .= (isset($urlData['query']) && strlen($urlData['query'])) ? '?' . $urlData['query'] : '';

        $urlData = (object)$urlData;
    }

    return $urlData;
}

/**
 * @param $itemPrice
 * @param null $currency
 * @param bool $convertPrice
 * @param bool $useItemProps
 * @return float|int|string|null
 */
function formatPrice($itemPrice, $currency = null, $convertPrice = true, $useItemProps = false)
{

    $response = null;

    if (!is_null($itemPrice)) {
        $displayRangePrice = config('cms.product.displayRangePrice', false);

        if (is_object($itemPrice) && @$itemPrice->minPrice && @$itemPrice->maxPrice)
            if ($displayRangePrice) {
                $clearMinPrice = numberFormat($convertPrice ? convertPrice($itemPrice->minPrice, $currency) : $itemPrice->minPrice);
                $clearMaxPrice = numberFormat($convertPrice ? convertPrice($itemPrice->maxPrice, $currency) : $itemPrice->maxPrice);
                $price = ($useItemProps ? "<span itemprop='lowPrice'>{$clearMinPrice}</span>" : $clearMinPrice) . '&nbsp;-&nbsp;' . ($useItemProps ? "<span itemprop='highPrice'>" . $clearMaxPrice . "</span>" : $clearMaxPrice);
            } else {
                $clearMinPrice = numberFormat($convertPrice ? convertPrice($itemPrice->minPrice, $currency) : $itemPrice->minPrice);
                $price = $useItemProps ? "<span itemprop='price'>{$clearMinPrice}</span>" : $clearMinPrice;
            }
        else {
            $clearPrice = numberFormat($convertPrice ? convertPrice($itemPrice, $currency) : $itemPrice);
            $price = $useItemProps ? "<span itemprop='price'>{$clearPrice}</span>" : $clearPrice;
        }

        if ($currency) {
            $currencySymbol = $currency->symbol ? $currency->symbol : $currency->name;
            if (@$currency->show_on_left)
                $response = ($useItemProps ? "<span itemprop='priceCurrency'>{$currencySymbol}</span>" : $currencySymbol) . $price;
            else
                $response = $price . ($useItemProps ? "<span itemprop='priceCurrency'>{$currencySymbol}</span>" : $currencySymbol);
        } else
            $response = $price;

        if (!$displayRangePrice && is_object($itemPrice) && @$itemPrice->minPrice && @$itemPrice->maxPrice)
            $response = __('front.from_price') . '&nbsp;' . $response;
    }

    return $response;

}

/**
 * @param $price
 * @param $currency
 * @param bool $revers
 * @return float|int
 */
function convertPrice($price, $currency, $revers = false)
{
    $defaultCurrencyCode = config('cms.currency.defaultCurrencyCode', 498);

    if (is_null($currency) || ($currency && $currency->is_default) || !@$currency->default)
        return $price;

    if ($revers) {
        if ($currency->default->code == $defaultCurrencyCode)
            $price = $price * $currency->value;
        elseif ($currency->code == $defaultCurrencyCode)
            $price = $price / $currency->default->value;
        else
            $price = $price * $currency->value / $currency->default->value;
    } else {
        if ($currency->default->code == $defaultCurrencyCode)
            $price = $price / $currency->value;
        elseif ($currency->code == $defaultCurrencyCode)
            $price = $price * $currency->default->value;
        else
            $price = $price * $currency->default->value / $currency->value;
    }

    return numberFormat($price);
}

/**
 * @param $price
 * @param $currency
 * @param $convertTo
 * @return float|int
 */
function convertPriceToCustomCurrency($price, $currency, $convertTo)
{
    $defaultCurrencyCode = config('cms.currency.defaultCurrencyCode', 498);
    $response = $price;

    if ($currency->name == $convertTo['name'])
        return $response;

    $convertToCurrencyValue = $currency->default->code == $convertTo['name'] ? $currency->default->value : $convertTo['value'];

    if (!$convertToCurrencyValue)
        return $response;


    if ($currency->code == $defaultCurrencyCode)
        $response = $price / $convertToCurrencyValue;
    else
        $response = $price * $currency->value / $convertToCurrencyValue;


    return round($response, 2);
}

/**
 * @param null $cookieCurrencies
 * @return |null
 */
function getCurrentCurrency($cookieCurrencies = null)
{
    if (is_null($cookieCurrencies)) {
        $currencies = Currencies::where('active', 1)->get();
        $defaultCurrency = @$currencies->where('is_default', 1)->first();
        $currBNMCurrency = @helpers()->getBnmCurrenciesByDate($defaultCurrency->slug, $currencies);

        $cookieCurrencies = (object)[
            'currCurrency' => $currBNMCurrency,
            'isCustom' => false
        ];
    }


    try {
        if (request()->session()->has('currency'))
            request()->session()->forget('currency');

        if (!request()->cookie('currency')) {
            request()->session()->put('currency', json_encode($cookieCurrencies));

            Cookie::queue(Cookie::forever('currency', json_encode($cookieCurrencies)));
        }

        if (request()->cookie('currency') && request()->cookie('currency') != json_encode($cookieCurrencies) && !@json_decode(request()->cookie('currency'))->isCustom) {
            Cookie::queue(Cookie::forget('currency'));

            request()->session()->put('currency', json_encode($cookieCurrencies));
            Cookie::queue(Cookie::forever('currency', json_encode($cookieCurrencies)));
        }

        if (session('currency'))
            $currCurrency = json_decode(session('currency'))->currCurrency;
        else
            $currCurrency = request()->cookie('currency', null) ? json_decode(request()->cookie('currency'))->currCurrency : null;

    } catch (\Exception $e) {
        $currCurrency = null;
    }

    return $currCurrency;
}

/**
 * @return |null
 */
function getDefaultCurrency()
{

    $cookieCurrencies = null;

    if (!request()->cookie('defaultCurrency') && !session('defaultCurrency')) {
        $currencies = Currencies::get();
        $defaultCurrency = @$currencies->where('is_default', 1)->first();

        $currBNMCurrency = @helpers()->getBnmCurrenciesByDate($defaultCurrency->slug, $currencies);

        $cookieCurrencies = (object)[
            'currCurrency' => $currBNMCurrency,
            'isCustom' => false
        ];
    }

    try {
        if (request()->session()->has('defaultCurrency'))
            request()->session()->forget('defaultCurrency');

        if ($cookieCurrencies && !request()->cookie('defaultCurrency')) {
            request()->session()->put('defaultCurrency', json_encode($cookieCurrencies));

            Cookie::queue(Cookie::forever('defaultCurrency', json_encode($cookieCurrencies)));
        }

        if ($cookieCurrencies && request()->cookie('defaultCurrency') && request()->cookie('defaultCurrency') != json_encode($cookieCurrencies) && !json_decode(request()->cookie('defaultCurrency'))->isCustom) {
            Cookie::queue(Cookie::forget('defaultCurrency'));

            request()->session()->put('defaultCurrency', json_encode($cookieCurrencies));
            Cookie::queue(Cookie::forever('defaultCurrency', json_encode($cookieCurrencies)));
        }

        if (session('defaultCurrency'))
            $currCurrency = json_decode(session('defaultCurrency'))->currCurrency;
        else
            $currCurrency = request()->cookie('defaultCurrency', null) ? json_decode(request()->cookie('defaultCurrency'))->currCurrency : null;

    } catch (\Exception $e) {
        $currCurrency = null;
    }

    return $currCurrency;

}

/**
 * @param $request
 * @param $items
 * @param string $responseItemsTextField
 * @param string $responseItemsTextRelation
 * @param bool $displayId
 * @return array|\Illuminate\Http\JsonResponse
 */
function selectAjaxSearchItems($request, $items, $responseItemsTextField = 'name', $responseItemsTextRelation = 'globalName', $displayId = true)
{
    $currentPage = (int)$request->get('page', 1);

    if (!$request->get('q', null))
        return response()->json([
            'status' => false,
            'total' => 0,
        ]);

    if ($items instanceof LengthAwarePaginator) {
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        $total = $items->total();
    } else {
        $total = $items['total'] ?: 0;
        $items = $items['items'];
    }

    return [
        'status' => true,
        'items' => $items->map(function ($item) use ($responseItemsTextField, $responseItemsTextRelation, $displayId) {
            return [
                'id' => $item->id,
                'text' => ($displayId ? "#{$item->id} | " : '') . ($responseItemsTextRelation ? @$item->$responseItemsTextRelation->$responseItemsTextField : @$item->$responseItemsTextField),
            ];
        }),
        'total' => $total,
        'page' => $currentPage,
    ];
}

/**
 * @param $imageLocation
 * @return array|object
 */
function getImageResolution($imageLocation)
{
    if (!file_exists($imageLocation))
        return [];

    list($width, $height) = getimagesize($imageLocation);

    return (object)[
        'width' => $width,
        'height' => $height
    ];
}

/**
 * @param $index
 * @param string $responseType
 * @return array|string
 */
function getUrlSegments($index, $responseType = 'string')
{
    $response = [];
    $segments = request()->segments();

    if (!empty($segments))
        foreach ($segments as $key => $segment) {
            if ($key > 0 && $key + 1 <= $index)
                $response[] = $segment;
        }

    return $responseType == 'string' ? implode('/', $response) : $response;
}

/**
 * @param $item
 * @return string
 */
function getSharerOptions($item)
{
    $response = '';
    $sharerItems = config('cms.product.sharer', []);

    if (empty($sharerItems))
        return $response;

    $currItemName = @$item->globalName->name;
    $currItemUrl = customUrl(['products', $item->slug, (@$item->productPromotion && @$item->productPromotion['variationId'] ? $item->productPromotion['variationId'] : "")]);
    $messengerAppId = config('services.facebook.client_id', null);

    foreach ($sharerItems as $sharerItem) {
        switch ($sharerItem) {
            case('facebook'):
            case('linkedin'):
            case('pinterest'):
            case('reddit'):
                $response .= "<div class='icon {$sharerItem}' data-sharer='{$sharerItem}' data-url='{$currItemUrl}'></div>";
                break;
            case('twitter'):
            case('whatsapp'):
            case('telegram'):
            case('viber'):
            case('vk'):
            case('tumblr'):
            case('okru'):
            case('mailru'):
                $response .= "<div class='icon {$sharerItem}' data-sharer='{$sharerItem}' data-title='{$currItemName}' data-url='{$currItemUrl}'></div>";
                break;
            case('messenger'):
                $response .= "<div class='icon {$sharerItem}' data-sharer='{$sharerItem}' data-app-id='{$messengerAppId}' data-redirect-uri='{$currItemUrl}' data-url='{$currItemUrl}'></div>";
                break;
            case('email'):
                $response .= "<div class='icon {$sharerItem}' data-sharer='{$sharerItem}' data-title='{$currItemName}' data-subject='{$currItemName}' data-url='{$currItemUrl}'></div>";
                break;
        }
    }

    return $response;
}

/**
 * @param $item
 * @param string $relation
 * @param array $globalSettings
 * @param bool $isMainPage
 * @return array
 */
function getItemMeta($item, $relation = 'globalName', $globalSettings = [], $isMainPage = false)
{

    $itemRelation = @$item->{$relation} ?: $item;
    $titlePage = $isMainPage ? @$itemRelation->meta_title : (@$itemRelation->meta_title ?: @$itemRelation->name);
    $keywordsPage = @$itemRelation->meta_keywords ?: '';
    $descriptionPage = @$itemRelation->meta_description ?: subStrTextNeedle(strip_tags(@$itemRelation->description), 500);
    $imagePage = getItemFile($item, 'medium', false);

    if (!empty($globalSettings)) {
        if (!$titlePage)
            $titlePage = getSettingsField('meta_title', $globalSettings, true) ?: config('app.name');

        if (!$keywordsPage)
            $keywordsPage = getSettingsField('meta_keywords', $globalSettings, true);

        if (!$descriptionPage)
            $descriptionPage = subStrTextNeedle(strip_tags(getSettingsField('meta_description', $globalSettings, true)), 500);
    }

    return [
        'titlePage' => $titlePage,
        'keywordsPage' => $keywordsPage,
        'descriptionPage' => $descriptionPage,
        'imagePage' => $imagePage,
    ];
}

/**
 * @param $allSettings
 * @return bool
 */
function setEmailConfigurations($allSettings)
{
    $email = getSettingsField('config_email', $allSettings);
    $emailPass = getSettingsField('config_email_pass', $allSettings);
    $emailHost = getSettingsField('config_email_host', $allSettings);
    $emailPort = getSettingsField('config_email_port', $allSettings);

    if ($email)
        $config = [
            'host' => $emailHost,
            'port' => $emailPort,
            'from' => ['address' => env('MAIL_FROM_ADDRESS'), 'name' => env('MAIL_FROM_NAME')],
            'username' => $email,
            'password' => $emailPass
        ];
    else
        $config = [
            'driver' => config('mail.driver'),
            'host' => config('mail.host'),
            'port' => config('mail.port'),
            'from' => ['address' => env('MAIL_FROM_ADDRESS'), 'name' => env('MAIL_FROM_NAME')],
            'username' => config('mail.username'),
            'password' => config('mail.password'),
            'encryption' => config('mail.encryption'),
        ];

    $defaultConfig = config()->get('mail');

    $newConfig = array_merge($defaultConfig, $config);

    config()->set('mail', $newConfig);

    return true;
}

/**
 * @param array $array
 * @param string $needle
 * @return array
 */
function filterArrayKeys(array $array, string $needle)
{
    $response = [];

    if (!empty($array))
        foreach ($array as $key => $value)
            if ((substr($key, 0, strlen($needle)) === $needle))
                $response[$key] = $value;

    return $response;
}

function getRecaptchaResponse($captcha)
{

    $errorResponse = [
        'status' => false,
        'msg' => [
            'e' => ['Recaptcha is invalid! Please refresh page!'],
            'type' => 'warning'
        ]
    ];

    if (!$captcha)
        return $errorResponse;

    $secretKey = env('RECAPTCHA_SECRET_KEY');

    $ip = request()->ip();
    $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secretKey . "&response=" . $captcha . "&remoteip=" . $ip);
    $responseKeys = json_decode($response, true);

    if (!@$responseKeys["success"])
        return $errorResponse;

    return true;
}

function getCurrentCart()
{
    $cart = Cart::isInProgress()->personalOrCommon()->first();

    return $cart;
}

function isAvailableCommonCart($currentCart=null)
{
    if(is_null($currentCart))
        $currentCart = getCurrentCart();

    $commonLife = 30;//30 minutes

    if(!is_null($currentCart))
    {
        if(!empty($currentCart->common_id) && !empty($currentCart->common_user_root) && !empty($currentCart->common_created_at))
        {
            $start_date = new DateTime($currentCart->common_created_at);
            $end_date = new DateTime();
            $interval = $start_date->diff($end_date);
            $hours   = $interval->format('%h');
            $minutes = $interval->format('%i');
            $diff = intval($hours) * 60 + intval($minutes);

            //if($diff <= $commonLife) return true;
            return true;
        }
    }

    return false;
}