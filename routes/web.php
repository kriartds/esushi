<?php

Route::get('/magic-token', function () {
    if (!request()->ajax())
        abort(404);

    return response()->json([
        'status' => true,
        'token' => csrf_token()
    ]);
});

Route::match(['get', 'post'], '/maibPaymentCallback', 'Front\CartController@maibPaymentCallback');

Route::prefix('{lang?}')->group(function() {
    /** Auth */

    Route::post('/login', 'Front\AuthController@login');
    Route::get('/register', 'Front\AuthController@register');
    Route::post('/register', 'Front\AuthController@register');
    Route::get('/logout', 'Front\AuthController@logout');
    Route::get('/password/reset-password', 'Front\AuthController@resetPassword');
    Route::post('/password/reset-password', 'Front\AuthController@resetPassword');
    Route::get('/password/reset-password/{token}', 'Front\AuthController@confirmResetPassword');
    Route::post('/password/reset-password/{token}', 'Front\AuthController@confirmResetPassword');
    Route::get('/login/{social}', 'Front\AuthController@socialLogin')->where('social', 'facebook|google');
    Route::get('/login/{social}/callback', 'Front\AuthController@handleProviderCallback')->where('social', 'facebook|google');
    Route::get('/login/set-email', 'Front\AuthController@setSocialEmail');
    Route::post('/login/set-email', 'Front\AuthController@setSocialEmail');

    Route::post('/getAddresses', 'Front\AjaxController@getAddresses');

    /** Auth */

    /** Form */
    Route::post('/send-cv-form', 'Front\PagesController@sendCvForm');
    Route::post('/send-contact-form', 'Front\PagesController@sendContactForm');
    Route::post('/send-reservation-form', 'Front\PagesController@sendReservationForm');
    Route::post('/send-sushi-party-form', 'Front\PagesController@sendSushiPartyForm');
    /** Form */

    Route::post('/contactForm', 'Front\IndexController@contactForm');
    Route::post('/setCurrency', 'Front\IndexController@setCurrency');
    Route::post('/catalog-list-view', 'Front\IndexController@changeCatalogListView');

    Route::get('/', 'Front\IndexController@index');
    Route::get('/party/{party}', 'Front\PagesController@getParty');
    Route::get('/promotion/{promotion}', 'Front\PagesController@getPromotion');
    Route::get('/restaurant/{party}', 'Front\PagesController@getRestaurant');

    /** Subscribe */
    Route::get('/unsubscribe/{token}', 'Front\IndexController@unsubscribe');
    Route::post('/subscribe', 'Front\IndexController@subscribe');
    /** Subscribe */

    Route::post('/search', 'Front\IndexController@search');
    Route::get('/search/{categorySlug?}', 'Front\IndexController@search');

    /** Load more */

    Route::post('/new-products', 'Front\IndexController@newProducts');
    Route::get('/getProductDetails', 'Front\ProductsController@getProductDetails');
    /** Cart */

    Route::post('/getDeliveryAmount', 'Front\CartController@getDeliveryAmount');
    Route::post('/addToCart', 'Front\CartController@store');
    Route::post('/buyInOneClick', 'Front\CartController@store');
    Route::post('/repeat-order', 'Front\CartController@repeatOrder');
    Route::post('/updateCart', 'Front\CartController@update');
    Route::post('/checkout', 'Front\CartController@checkout');
    Route::post('/destroyCartProduct', 'Front\CartController@destroy');
    Route::post('/getShippingMethods', 'Front\CartController@getShippingMethods');
    Route::post('/getShippingMethod', 'Front\CartController@getShippingMethod');
    Route::post('/pickupStores', 'Front\CartController@getPickupRegionsStores');
    Route::post('/useCoupon', 'Front\CartController@applyCoupon');
    Route::post('/destroyCoupon', 'Front\CartController@destroyCoupon');
    Route::get('/payPalCallback', 'Front\CartController@payPalPaymentCallback');
    Route::get('/cart/{common_id?}', 'Front\CartController@index');
    Route::get('/successMsg', 'Front\CartController@successMsg');
    Route::get('/errorMsg', 'Front\CartController@errorMsg');
    Route::post('/createCommonCart', 'Front\CartController@createCommonCart');
    Route::post('/cancelCommonCart', 'Front\CartController@cancelCommonCart');


    Route::match(['get', 'post'], '/sms-v1', 'SmsController@index');
    Route::match(['get', 'post'], '/sms-v1/code', 'SmsController@code');
    Route::match(['get', 'post'], '/sms-v1/check', 'SmsController@check');

    Route::get('/checkoutv1', 'Front\CartController@checkoutv1');

    /** Cart */

    /** Auth routes */

    Route::group([
        'middleware' => 'auth.web'
    ], function () {
        Route::group([
            'prefix' => 'dashboard',
        ], function () {
            Route::get('/', 'Front\UsersController@index');
            Route::get('/settings', 'Front\UsersController@settings');
            Route::post('/edit-account', 'Front\UsersController@editAccount');
            Route::get('/orders-history', 'Front\UsersController@ordersHistory');
            Route::post('/orders-history', 'Front\UsersController@ordersHistory');
            Route::post('/apply-discount-card ', 'Front\UsersController@applyDiscountCard');
        });
    });

    /** Auth routes */

    Route::any('/{component}/{itemSlug?}/{itemVariation?}', 'Front\RoleManager@routeResponder');
});
