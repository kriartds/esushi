<?php

Route::get('/auth/login', 'Admin\AuthController@login');
Route::post('/auth/login', 'Admin\AuthController@checkLogin');
Route::get('/auth/logout', 'Admin\AuthController@logout');

Route::group(['middleware' => 'auth.admin'], function () {
    Route::post('/hideMenu', 'Admin\DefaultController@hideMenu');
    Route::post('/uploadFile', 'Admin\FileController@uploadFile');
    Route::post('/destroyFile', 'Admin\FileController@destroyFile');
    Route::post('/activateFile', 'Admin\FileController@activateFile');
    Route::post('/changeFilePosition', 'Admin\FileController@changeFilePosition');
    Route::post('/getGallery', 'Admin\FileController@getGalleryFiles');
    Route::post('/createDestroyRepeatField', 'Admin\DefaultController@createDestroyRepeatField');
    Route::post('/checkUserNotifications', 'Admin\DefaultController@checkUserNotifications');
    Route::post('/markNotificationAsRead', 'Admin\DefaultController@markNotificationAsRead');

    Route::get('/', 'Admin\IndexController@index');
    Route::post('/testEmail', 'Admin\SettingsController@testEmail');
    Route::any('/{component}/{function?}/{id?}/{langId?}', 'Admin\RoleManager@routeResponder');
});