<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Filesystem\Filesystem;
use App\Models\ComponentsId;
use App\Models\Languages;
use App\Models\Files;
use App\Models\FilesItems;

class FilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filesSystem = new Filesystem();
        $allComponentsFiles = $filesSystem->allFiles(storage_path('app/seeds/icons/components'));
        $allLanguagesFiles = $filesSystem->allFiles(storage_path('app/seeds/icons/languages'));

        $originalPath = 'files/' . date('Y-m-d') . '/images';
        $destinationPath = public_path($originalPath);
        $fileIteration = 0;

        if (!file_exists($destinationPath))
            File::makeDirectory($destinationPath, 0775, true, true);

        $allComponents = ComponentsId::get();

        if (!empty($allLanguagesFiles)) {
            $languageComponentId = $allComponents->where('slug', 'languages')->first();
            $allLanguages = Languages::get();
            if ($allLanguages->isNotEmpty() && !is_null($languageComponentId))
                foreach ($allLanguages as $allLanguage) {
                    $fileIteration++;
                    foreach ($allLanguagesFiles as $key => $allFile) {
                        $existFileInDB = Files::where('original_name', $allFile->getFilename())->first();
                        if (!$existFileInDB) {
                            $extension = $allFile->getExtension();
                            $originalFileName = pathinfo($allFile->getFilename(), PATHINFO_FILENAME);
                            $fileName = $originalFileName . '-_-' . time() . '.' . $extension;
                            $fileSize = $allFile->getSize();
                            $fileMimeType = mime_content_type($allFile->getPathname());
                            $fileType = substr($fileMimeType, 0, strpos($fileMimeType, '/'));

                            if ($originalFileName == $allLanguage->slug) {
                                copy($allFile->getPathname(), "$destinationPath/{$fileName}");

                                $file = Files::create([
                                    'file' => "{$originalPath}/{$fileName}",
                                    'original_name' => $allFile->getFilename(),
                                    'active' => 1,
                                    'position' => $fileIteration,
                                    'type' => $fileType,
                                    'mime_type' => $fileMimeType,
                                    'size' => $fileSize,
                                    'resolution' => null,
                                ]);

                                FilesItems::create([
                                    'file_id' => $file->id,
                                    'component_id' => $languageComponentId->id,
                                    'item_id' => $allLanguage->id,
                                    'variation_id' => null,
                                    'lang_id' => null,
                                    'active' => 1,
                                    'position' => $fileIteration

                                ]);
                            }
                        }
                    }
                }
        }

        if (!empty($allComponentsFiles)) {
            $mainComponentId = $allComponents->where('slug', 'components')->first();
            if ($allComponents->isNotEmpty() && $mainComponentId)
                foreach ($allComponents as $allComponent) {
                    $fileIteration++;
                    foreach ($allComponentsFiles as $key => $allFile) {
                        $existFileInDB = Files::where('original_name', $allFile->getFilename())->first();
                        if (!$existFileInDB) {
                            $extension = $allFile->getExtension();
                            $originalFileName = pathinfo($allFile->getFilename(), PATHINFO_FILENAME);
                            $fileName = $originalFileName . '-_-' . time() . '.' . $extension;
                            $fileSize = $allFile->getSize();
                            $fileMimeType = mime_content_type($allFile->getPathname());
                            $fileType = substr($fileMimeType, 0, strpos($fileMimeType, '/'));

                            if ($originalFileName == $allComponent->slug) {
                                copy($allFile->getPathname(), "$destinationPath/{$fileName}");

                                $file = Files::create([
                                    'file' => "{$originalPath}/{$fileName}",
                                    'original_name' => $allFile->getFilename(),
                                    'active' => 1,
                                    'position' => $fileIteration,
                                    'type' => $fileType,
                                    'mime_type' => $fileMimeType,
                                    'size' => $fileSize,
                                    'resolution' => null,
                                ]);

                                FilesItems::create([
                                    'file_id' => $file->id,
                                    'component_id' => $mainComponentId->id,
                                    'item_id' => $allComponent->id,
                                    'variation_id' => null,
                                    'lang_id' => null,
                                    'active' => 1,
                                    'position' => $fileIteration

                                ]);
                            }
                        }
                    }
                }
        }
    }
}
