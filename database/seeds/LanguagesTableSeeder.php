<?php

use Illuminate\Database\Seeder;
use App\Models\Languages;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Languages::updateOrCreate([
            'slug' => 'en'
        ], [
            'name' => 'English',
            'position' => 1,
            'is_default' => 1,
            'active' => 1
        ]);

        Languages::updateOrCreate([
            'slug' => 'ro'
        ], [
            'name' => 'Romana',
            'position' => 2,
            'is_default' => 0,
            'active' => 1
        ]);

        Languages::updateOrCreate([
            'slug' => 'ru'
        ], [
            'name' => 'Русский',
            'position' => 3,
            'is_default' => 0,
            'active' => 0
        ]);
    }
}
