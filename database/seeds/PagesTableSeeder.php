<?php

use Illuminate\Database\Seeder;
use Modules\Pages\Models\PagesId;
use Modules\Pages\Models\Pages;
use App\Models\Languages;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = Languages::where('active', 1)->get();

        if ($languages->isNotEmpty()) {
            $item = PagesId::firstOrCreate([
                'slug' => 'main-page'
            ], [
                'template' => 'mainPage',
                'active' => 1
            ]);

            foreach ($languages as $language) {
                Pages::updateOrCreate([
                    'page_id' => $item->id,
                    'lang_id' => $language->id,
                ], [
                    'name' => $language->slug == 'en' ? 'Main page' : 'Prima pagina',
                    'description' => null,
                    'meta_title' => null,
                    'meta_keywords' => null,
                    'meta_description' => null,
                ]);
            }
        }
    }
}
