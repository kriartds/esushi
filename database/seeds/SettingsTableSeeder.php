<?php

use Illuminate\Database\Seeder;
use Illuminate\Filesystem\Filesystem;
use App\Models\Settings;
use Nwidart\Modules\Facades\Module;
use Modules\Pages\Models\PagesId;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settings::firstOrCreate([
            'slug' => 'cms_items_per_page',
            'value' => 100
        ]);

        Settings::firstOrCreate([
            'slug' => 'site_items_per_page',
            'value' => 12
        ]);

        Settings::firstOrCreate([
            'slug' => 'maintenance_mode',
            'value' => null
        ]);

        $fileSystem = new Filesystem();
        if (Module::has('Pages') && $fileSystem->exists(Module::getPath() . '/Pages/Models/PagesId.php')) {
            $mainPage = PagesId::where('slug', 'main-page')->first();

            if (!is_null($mainPage))
                Settings::firstOrCreate([
                    'slug' => 'home_page',
                    'value' => $mainPage->id
                ]);
        }
    }
}
