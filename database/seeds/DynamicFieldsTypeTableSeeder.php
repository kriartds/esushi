<?php

use Illuminate\Database\Seeder;
use App\Models\DynamicFieldsTypes;

class DynamicFieldsTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'slug' => 'input',
                'with_many_values' => 0
            ],
            [
                'slug' => 'checkbox',
                'with_many_values' => 1
            ],
            [
                'slug' => 'radio',
                'with_many_values' => 1
            ],
            [
                'slug' => 'textarea',
                'with_many_values' => 0
            ],
            [
                'slug' => 'ckeditor',
                'with_many_values' => 0
            ],
            [
                'slug' => 'files',
                'with_many_values' => 1
            ],
            [
                'slug' => 'select',
                'with_many_values' => 1
            ],
            [
                'slug' => 'multiple-select',
                'with_many_values' => 1
            ],
            [
                'slug' => 'repeater',
                'with_many_values' => 0
            ]
        ];

        foreach ($types as $type) {
            DynamicFieldsTypes::firstOrCreate($type);
        }
    }
}
