<?php

use Illuminate\Database\Seeder;
use Illuminate\Filesystem\Filesystem;
use Modules\Pages\Database\Seeders\PagesTableSeeder;
use Nwidart\Modules\Facades\Module;
use Modules\Currencies\Database\CurrenciesTableSeeder;
use Modules\Products\Database\Seeders\ProductsItemsTypesTableSeeder;
use Modules\Products\Database\Seeders\ProductsAttributesTypesTableSeeder;
use Modules\CountriesRegions\Database\Seeders\CountriesRegionsTableSeeder;
use Modules\ShippingZones\Database\Seeders\ShippingZonesDatabaseSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $fileSystem = new Filesystem();

        $seedsClasses = [
            LanguagesTableSeeder::class,
            ComponentsTableSeeder::class,
            FilesTableSeeder::class,
            AdminUsersTableSeeder::class,
        ];

        if (Module::has('Currencies') && $fileSystem->exists(Module::getPath() . '/Currencies/Database/Seeders/CurrenciesTableSeeder.php')) {
            $seedsClasses = array_merge($seedsClasses, [
                CurrenciesTableSeeder::class,
            ]);
        }

        if (Module::has('Pages') && $fileSystem->exists(Module::getPath() . '/Pages/Database/Seeders/PagesTableSeeder.php')) {
            $seedsClasses = array_merge($seedsClasses, [
                PagesTableSeeder::class
            ]);
        }

        if (Module::has('Products')) {
            if ($fileSystem->exists(Module::getPath() . '/Products/Database/Seeders/ProductsItemsTypesTableSeeder.php') && $fileSystem->exists(Module::getPath() . '/Products/Database/Seeders/ProductsAttributesTypesTableSeeder.php'))
                $seedsClasses = array_merge($seedsClasses, [
                    ProductsItemsTypesTableSeeder::class,
                    ProductsAttributesTypesTableSeeder::class,
                ]);
        }

        $seedsClasses = array_merge($seedsClasses, [
            SettingsTableSeeder::class,
            DynamicFieldsTypeTableSeeder::class,
        ]);

        if (Module::has('ShippingZones')) {
            if ($fileSystem->exists(Module::getPath() . '/ShippingZones/Database/Seeders/ShippingZonesDatabaseSeeder.php'))
                $seedsClasses = array_merge($seedsClasses, [
                    ShippingZonesDatabaseSeeder::class,
                ]);
        }

        if (Module::has('CountriesRegions')) {
            if ($fileSystem->exists(Module::getPath() . '/CountriesRegions/Database/Seeders/CountriesRegionsTableSeeder.php'))
                $seedsClasses = array_merge($seedsClasses, [
                    CountriesRegionsTableSeeder::class,
                ]);
        }

        $this->call($seedsClasses);
    }
}
