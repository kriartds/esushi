<?php

use Illuminate\Database\Seeder;
use App\Models\Languages;
use App\Models\ComponentsId;
use App\Models\Components;

class ComponentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $componentsFile = storage_path('app/seeds/components.json');
        if (file_exists($componentsFile)) {
            $components = json_decode(file_get_contents($componentsFile), true) ?: [];

            $languages = Languages::where('active', 1)->get();
            $position = 0;

            if (!empty($components) && $languages->isNotEmpty()) {
                foreach ($components as $component) {
                    $position++;

                    $item = ComponentsId::firstOrCreate([
                        'slug' => @$component['slug'] ?: str_slug(@$component['name']['en']),
                        'controller' => @$component['controller'],
                    ], [
                        'p_id' => null,
                        'position' => $position,
                        'module' => @$component['module'],
                        'level' => 1,
                        'active' => !is_null(@$component['options']['active']) ? @$component['options']['active'] : 1,
                        'for_root' => !is_null(@$component['options']['for_root']) ? @$component['options']['for_root'] : 0,
                        'for_count' => !is_null(@$component['options']['for_count']) ? @$component['options']['for_count'] : 0,
                        'for_sidebar' => !is_null(@$component['options']['for_sidebar']) ? @$component['options']['for_sidebar'] : 1,
                        'for_menu' => !is_null(@$component['options']['for_menu']) ? @$component['options']['for_menu'] : 0,
                        'for_statistics' => !is_null(@$component['options']['for_statistics']) ? @$component['options']['for_statistics'] : 0,
                        'for_sitemap' => !is_null(@$component['options']['for_sitemap']) ? @$component['options']['for_sitemap'] : 0,
                    ]);

                    foreach ($languages as $language) {
                        if (@$component['name'][$language->slug])
                            Components::updateOrCreate([
                                'component_id' => $item->id,
                                'lang_id' => $language->id,
                            ], [
                                'name' => @$component['name'][$language->slug]
                            ]);
                    }

                    if (@$component['children'] && !empty($component['children']))
                        foreach ($component['children'] as $k => $child) {
                            $position++;

                            $childItem = ComponentsId::firstOrCreate([
                                'slug' => @$child['slug'] ?: str_slug(@$child['name']['en']),
                                'controller' => @$child['controller'],
                            ], [
                                'p_id' => $item->id,
                                'position' => $position,
                                'module' => @$child['module'],
                                'level' => 2,
                                'active' => !is_null(@$child['options']['active']) ? @$child['options']['active'] : 1,
                                'for_root' => !is_null(@$child['options']['for_root']) ? @$child['options']['for_root'] : 0,
                                'for_count' => !is_null(@$child['options']['for_count']) ? @$child['options']['for_count'] : 0,
                                'for_sidebar' => !is_null(@$child['options']['for_sidebar']) ? @$child['options']['for_sidebar'] : 1,
                                'for_menu' => !is_null(@$child['options']['for_menu']) ? @$child['options']['for_menu'] : 0,
                                'for_statistics' => !is_null(@$child['options']['for_statistics']) ? @$child['options']['for_statistics'] : 0,
                                'for_sitemap' => !is_null(@$child['options']['for_sitemap']) ? @$child['options']['for_sitemap'] : 0,
                            ]);

                            foreach ($languages as $language) {
                                if (@$child['name'][$language->slug])
                                    Components::updateOrCreate([
                                        'component_id' => $childItem->id,
                                        'lang_id' => $language->id,
                                    ], [
                                        'name' => @$child['name'][$language->slug]
                                    ]);
                            }
                        }
                }
            }
        }
    }
}
