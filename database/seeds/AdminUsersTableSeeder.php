<?php

use Illuminate\Database\Seeder;
use App\Models\ComponentsId;
use App\Models\AdminUserGroup;
use App\Models\AdminUsers;
use App\Models\AdminUsersPermissions;

class AdminUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $components = ComponentsId::get();

        if ($components->isNotEmpty()) {

            $adminUserGroup = AdminUserGroup::firstOrCreate([
                'slug' => 'root',
                'root' => 1,
            ], [
                'name' => 'Root',
                'active' => 1
            ]);

            AdminUsers::firstOrCreate([
                'admin_user_group_id' => $adminUserGroup->id,
                'login' => 'root',
                'root' => 1,
            ], [
                'name' => 'Root',
                'email' => null,
                'password' => bcrypt(config('cms.admin.password', 'q1w2e3r4')),
                'active' => 1,
                'for_logout' => 0,
                'has_exported_products' => 0,
                'has_imported_products' => 0,
            ]);

            foreach ($components as $component) {
                AdminUsersPermissions::firstOrCreate([
                    'admin_user_group_id' => $adminUserGroup->id,
                    'component_id' => $component->id
                ], [
                    'for_create' => 1,
                    'for_edit' => 1,
                    'for_delete' => 1,
                    'for_view' => 1,
                    'for_active' => 1,
                ]);
            }
        }

    }
}
