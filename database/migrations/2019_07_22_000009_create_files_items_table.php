<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('file_id');
            $table->unsignedInteger('component_id')->nullable();
            $table->unsignedInteger('item_id')->nullable();
            $table->unsignedInteger('variation_id')->nullable();
            $table->unsignedInteger('lang_id')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->bigInteger('position')->default(0);
            $table->timestamps();

            $table->foreign('file_id')->references('id')->on('files')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('component_id')->references('id')->on('components_id')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('set null')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files_items');
    }
}
