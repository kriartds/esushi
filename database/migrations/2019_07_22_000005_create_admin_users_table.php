<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('admin_user_group_id');
            $table->string('name')->nullable();
            $table->string('login')->unique();
            $table->string('email')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->tinyInteger('root')->default(0);
            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('for_logout')->default(0);
            $table->tinyInteger('has_exported_products')->default(0);
            $table->tinyInteger('has_imported_products')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('admin_user_group_id')->references('id')->on('admin_user_group')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_users');
    }
}
