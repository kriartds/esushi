<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicFieldsIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_fields_id', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('component_id')->nullable();
            $table->unsignedInteger('item_id')->nullable();
            $table->unsignedInteger('p_id')->nullable();
            $table->unsignedInteger('field_type_id')->nullable();
            $table->string('slug')->nullable();
            $table->string('label_name')->nullable();
            $table->unsignedInteger('repeater_group')->nullable();
            $table->string('class_name')->nullable();
            $table->string('attributes')->nullable();
            $table->tinyInteger('is_required')->default(0);
            $table->timestamps();

            $table->foreign('component_id')->references('id')->on('components_id')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('field_type_id')->references('id')->on('dynamic_fields_types')->onDelete('cascade')->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_fields_id');
    }
}
