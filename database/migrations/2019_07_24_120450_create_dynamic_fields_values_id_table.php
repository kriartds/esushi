<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicFieldsValuesIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_fields_values_id', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dynamic_field_id');
            $table->string('input_key')->nullable();
            $table->tinyInteger('is_selected')->default(0);
            $table->timestamps();

            $table->foreign('dynamic_field_id')->references('id')->on('dynamic_fields_id')->onDelete('cascade')->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_fields_values_id');
    }
}
