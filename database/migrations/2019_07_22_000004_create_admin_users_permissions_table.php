<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminUsersPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_users_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('admin_user_group_id');
            $table->unsignedInteger('component_id');
            $table->tinyInteger('for_create')->default(1);
            $table->tinyInteger('for_edit')->default(1);
            $table->tinyInteger('for_delete')->default(1);
            $table->tinyInteger('for_view')->default(1);
            $table->tinyInteger('for_active')->default(1);
            $table->timestamps();

            $table->foreign('admin_user_group_id')->references('id')->on('admin_user_group')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('component_id')->references('id')->on('components_id')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_users_permissions');
    }
}
