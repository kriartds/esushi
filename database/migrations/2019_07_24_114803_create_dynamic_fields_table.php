<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dynamic_field_id');
            $table->unsignedInteger('lang_id')->nullable();
            $table->string('name')->nullable();
            $table->string('placeholder')->nullable();
            $table->text('value')->nullable();
            $table->timestamps();

            $table->foreign('dynamic_field_id')->references('id')->on('dynamic_fields_id')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('set null')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_fields');
    }
}
