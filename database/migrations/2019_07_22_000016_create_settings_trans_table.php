<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_trans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('setting_trans_id');
            $table->unsignedInteger('lang_id')->nullable();
            $table->text('value')->nullable();
            $table->timestamps();

            $table->foreign('setting_trans_id')->references('id')->on('settings_trans_id')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('set null')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_trans');
    }
}
