<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('phone', 11);
            $table->integer('object_id');
            $table->tinyInteger('object_type_id');

            $table->integer('sms_code');
            $table->string('data');
            $table->string('return_data');

            $table->tinyInteger('is_reject');
            $table->integer('used_at')->nullable();
            $table->integer('created_at');

            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms');
    }
}
