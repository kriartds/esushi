<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWishProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wish_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('wish_id');
            $table->unsignedInteger('products_item_id')->nullable();
            $table->timestamps();

            $table->foreign('wish_id')->references('id')->on('wish')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('products_item_id')->references('id')->on('products_items_id')->onDelete('cascade')->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wish_products');
    }
}
