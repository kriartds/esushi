<?php

namespace Modules\Reviews\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reviews extends Model
{

    use SoftDeletes;
    use FilesRelation;

    protected $table = 'reviews';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'phone',
        'email',
        'tema',
        'message',
        'ip',
        'seen',
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'reviews';

    }

}
