<div class="reviews">
    <form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
          id="{{!is_null($item) ? 'edit' : 'create'}}-form"
          enctype="multipart/form-data">
        <div class="background">
            <div class="small-content">
                <div class="top-input">
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="first-name">{{__("{$moduleName}::e.name")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <input name="first_name" id="first-name" value="{{@$item->first_name}}">
                        </div>
                    </div>
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="last-name">{{__("{$moduleName}::e.surname")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <input name="last_name" id="last-name" value="{{@$item->last_name}}">
                        </div>
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="email">{{__("{$moduleName}::e.email")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="email" id="email" value="{{@$item->email}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="message">{{__("{$moduleName}::e.comment")}}*</label>
                    </div>
                    <div class="field-wrap">
                <textarea name="message" id="message" class="limit-characters"
                          data-max-characters="300">{{@$item->message}}</textarea>
                    </div>
                </div>
                <div class="field-row">
                    @include('admin.templates.uploadFile', [
                        'item' => @$item,
                        'options' => [
                            'data-component-id' => $currComponent->id,
                            'data-types' => 'image'
                        ]
                    ])
                </div>
                <div class="small-container">
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="date">{{__("{$moduleName}::e.date")}}</label>
                        </div>
                        <div class="field-wrap">
                            <input name="date" id="date" class="datetimepicker"
                                   value="{{strtotime(@$item->created_at) ? date('d-m-Y', strtotime($item->created_at)) : date('d-m-Y')}}">
                        </div>
                    </div>
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="ip">{{__("{$moduleName}::e.ip")}}</label>
                        </div>
                        <div class="field-wrap">
                            <input id="ip" value="{{!is_null(@$item->ip) ? $item->ip : '-'}}" disabled>
                        </div>
                    </div>
                    <div class="field-row inline  white-bg">
                        <div class="label-wrap">
                            <label for="show_on_main">{{__("{$moduleName}::e.show_on_main")}}</label>
                        </div>
                        <div class="checkbox-switcher">
                            <input type="checkbox" name="show_on_main"
                                   id="show_on_main" {{@$item->show_on_main ? 'checked' : ''}}>
                            <label for="show_on_main"></label>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <button class="btn submit-form-btn"
                data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
    </form>
</div>
