<?php

return [

    'element_is_active' => 'The element :name is active!',
    'element_is_inactive' => 'The element :name is inactive!',
    'element_is_hidden' => 'The element :name is hidden!',
    'element_is_visible' => 'The element :name is visible!',

    'title_table' => 'Title',
    'edit_table' => 'Edit',
    'active_table' => 'Status ',
    'delete_table' => 'Delete',
    'reestablish_table' => 'Reestablish',
    'id_table' => 'ID',
    'list_is_empty' => 'List is empty!',
    'save_it' => 'Save',

    'name' => "Name",
    'surname' => 'Surname',
    'email' => 'Email',
    'comment' => 'Comment',
    'date' => 'Date',
    'ip' => 'Client IP',
    'show_on_main' => 'Show on main',
    'phone' => 'Phone',
    'tema' => 'Theme',

];