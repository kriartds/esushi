<?php

namespace Modules\Reviews\Repositories;

use App\Repositories\BaseRepository;
use Modules\Reviews\Models\Reviews as Model;

class ReviewsRepository extends BaseRepository
{

    protected $model;

    public function __construct()
    {
        parent::__construct(new Model());
    }

}