<?php

namespace Modules\Reviews\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use Modules\Reviews\Http\Requests\ReviewsRequest;
use Modules\Reviews\Models\Reviews;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReviewsController extends DefaultController
{

    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'reviews';
        $this->path = "{$this->moduleName}::reviews";
    }
    public function index()
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = Reviews::orderBy('created_at', 'desc')->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.list", $response);

    }

    public function view($id)
    {

        $item = Reviews::findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
        ];

        return view("{$this->path}.createEdit", $response);
    }
    public function trash()
    {
        $items = Reviews::onlyTrashed()->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }
    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                Reviews::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = Reviews::whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                            $item->delete();
                            helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = Reviews::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                            $item->files()->delete();
                            $item->forceDelete();
                            helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = Reviews::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                            $item->restore();
                            helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                    }
                }
                break;
            default:
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }


}