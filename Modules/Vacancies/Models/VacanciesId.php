<?php

namespace Modules\Vacancies\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VacanciesId extends Model
{

    use SoftDeletes;
    use FilesRelation;

    protected $table = 'vacancies_id';
    protected $fillable = [
        'active',
        'position',
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'vacancies';
        self::$globalLangId = request()->segment(6, null);

    }

    public function globalName()
    {
        return $this->hasOne(Vacancies::class, 'vacancies_id', 'id')->whereIn('vacancies.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(vacancies.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('Modules\Vacancies\Models\Vacancies', 'vacancies_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('Modules\Vacancies\Models\Vacancies', 'vacancies_id', 'id');
    }
}
