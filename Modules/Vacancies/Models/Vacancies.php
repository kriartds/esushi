<?php

namespace Modules\Vacancies\Models;

use Illuminate\Database\Eloquent\Model;

class Vacancies extends Model
{

    protected $table = 'vacancies';

    protected $fillable = [
        'name',
        'lang_id'
    ];


}
