<?php

namespace Modules\Vacancies\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\Models\Languages;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Vacancies\Http\Requests\VacanciesRequest;
use Modules\Vacancies\Models\Vacancies;
use Modules\Vacancies\Models\VacanciesId;

class VacanciesController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'vacancies';
        $this->path = "{$this->moduleName}::vacancies";
    }

    public function index(Request $request)
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = VacanciesId::orderBy('position')
            ->with(['globalName', 'items'])
            ->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.list", $response);

    }

    public function create(Request $request)
    {

        $parent = VacanciesId::where('active', 1)
            ->find($request->get('parent'));

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'parent' => $parent
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit(Request $request, $id, $langId)
    {

        $item = VacanciesId::findOrFail($id);

        $parent = VacanciesId::where('active', 1)
            ->find($request->get('parent'));

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'langId' => $langId,
            'parent' => $parent
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = VacanciesId::onlyTrashed()->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(VacanciesRequest $request, $id, $langId)
    {

        if (is_null($id) && is_null($langId)) {
            $lang = Languages::where('active', 1)
                ->find($request->get('lang'));

            if (is_null($lang))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Lang not exist!'],
                        'type' => 'warning'
                    ]
                ]);

            $langId = $lang->id;
        }

        $parent = VacanciesId::where('active', 1)
            ->find($request->get('parent'));

        if (is_null($parent) && $request->get('parent'))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Parent not exist!'],
                    'type' => 'warning'
                ]
            ]);

        $item = VacanciesId::find($id);

        if (is_null($item)) {
            $item = new VacanciesId();

            $nextPosition = Helpers::getNextItemPosition($item);
            $item->position = $nextPosition;
            $item->active = 1;
        }
        $item->save();

        $item->itemByLang()->updateOrCreate([
            'vacancies_id' => $item->id,
            'lang_id' => $langId
        ], [
            'name' => $request->get('name', null),
        ]);
        helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', parent::currComponent()->slug]) . (!is_null(@$parent->id) ? '?parent=' . @$parent->id : '')
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->globalName->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', parent::currComponent()->slug, 'edit', $item->id, $langId]) . (!is_null(@$parent->id) ? '?parent=' . @$parent->id : '')
            ]);
    }

    public function actionsCheckbox(Request $request)
    {

        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                VacanciesId::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = VacanciesId::whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = VacanciesId::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->files()->delete();
                        $item->items()->delete();
                        $item->forceDelete();
                        helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = VacanciesId::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'restored-from-trash');
                        $item->restore();
                    }
                }
                break;
            default:
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function updatePosition(Request $request)
    {

        $order = $request->get('order');

        if (!is_array($order))
            return response()->json([
                'status' => false
            ]);

        $itemsPositions = [];
        foreach ($order as $id) {
            $item = VacanciesId::find($id);
            $itemsPositions[] = @$item->position;
        }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($order as $key => $id) {
            VacanciesId::where('id', $id)->update(['position' => $itemsPositions[$key]]);
        }

        return response()->json([
            'status' => true
        ]);
    }

    public function activateItem(Request $request)
    {

        $currComponent = parent::currComponent();
        $item = VacanciesId::findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->id]);
            helpers()->logActions($currComponent, $item, @$item->id, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->id]);
            helpers()->logActions($currComponent, $item, @$item->id, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }
}
