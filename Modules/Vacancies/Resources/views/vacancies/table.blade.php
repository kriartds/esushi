@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th>{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.title_table")}}</th>

            @if($permissions->active)
                <th>{{__("{$moduleName}::e.active_table")}}</th>
            @endif

            <th class="align-center">Languages</th>

            @if(request()->get('parent'))
                <th>{{__("{$moduleName}::e.position_table")}}</th>
            @endif
            <th class="checkbox-all align-center">
                <div>Check all</div>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="p-center id"><p>{{$item->id}}</p></td>
                <td class="left  prod-name"><a
                            href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id,DEF_LANG_ID]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}">{{@$item->globalName->name}}</a>
                </td>
                @if($permissions->active)
                    <td class="status align-center">
                        <span class="activate-item {{$item->active ? 'active' : ''}}"
                              data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                              data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>
                    </td>
                @endif
                @if($permissions->edit)
                    <td class="td-link">
                        @foreach($langList as $lang)
                            <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $lang->id]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}" {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                        @endforeach
                    </td>
                @endif
                @if(request()->get('parent'))
                    <td class="position">
                        <img src="{{asset('admin-assets/img/small-icons/position.svg')}}" alt="">
                    </td>
                @endif
                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}">
                        <label for="{{$item->id}}">
                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
