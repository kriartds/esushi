<div class="form-menu slider-create">
    <form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
          id="{{!is_null($item) ? 'edit' : 'create'}}-form"
          enctype="multipart/form-data">
        <div class="create-menus">
            <div class="select-language">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="lang">{{__("{$moduleName}::e.lang")}}*</label>
                    </div>
                    <div class="field-wrap">
                        @if(!$langList->isEmpty())
                            <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                                @foreach($langList as $lang)
                                    <option value="{{$lang->id}}" {{ (is_null(@$langId) && $lang->id == LANG_ID ? 'selected' : $lang->id == @$langId) ? 'selected' : ''}}>{{$lang->name ?: ''}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>
            </div>
            <div class="title-slug">

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__("{$moduleName}::e.title_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$item->itemByLang->name}}">
                    </div>
                </div>

            </div>
        </div>
        <div class="button-div">
            <button class="btn full submit-form-btn"
                    data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
        </div>

    </form>

</div>