<?php

namespace Modules\Pages\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\Models\Languages;
use Modules\Pages\Http\Requests\HomeBlockRequest;
use Modules\Pages\Models\HomeBlockId;
use Modules\Pages\Models\HomeBlockProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Products\Models\ProductsItemsId;

class HomeProductsController extends DefaultController
{

    private $moduleName;
    private $path;
    private $frontPath;

    public function __construct()
    {

        $this->moduleName = 'pages';
        $this->path = "{$this->moduleName}::homeProducts";
        $this->frontPath = 'front.homeProducts';
    }

    public function index()
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = HomeBlockId::with(['globalName', 'items'])->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'mainPageId' => Helpers::getSettingsField('home_page', parent::globalSettings())
        ];

        return view("{$this->path}.list", $response);

    }

    public function create()
    {

        $templates = parseFilesInPath("{$this->frontPath}.templates", '.blade.php');

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'templates' => $templates,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id, $langId)
    {

        $item = HomeBlockId::with('homeBlockProducts', 'homeBlockProducts.globalName', 'homeBlockProducts.files.file')->findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'langId' => $langId
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = HomeBlockId::onlyTrashed()->with(['globalName'])->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(Request  $request, $id, $langId)
    {
        $rules =  [
            'lang' => 'required',
            'name' => 'required',
            'slug' => "required|unique:home_block_id,slug,{$id}",
        ];

        $valid = Validator::make($request->all(), $rules);

        if ($valid->fails()){
            return [
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $valid->messages(),
                    'type' => 'error'
                ],
            ];
        }
        $currComponent = parent::currComponent();
        if (is_null($id) && is_null($langId)) {
            $lang = Languages::where('active', 1)
                ->find($request->get('lang'));

            if (is_null($lang))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Lang not exist!'],
                        'type' => 'warning'
                    ]
                ]);

            $langId = $lang->id;
        }



        if (is_null($id)) {
            $item = new HomeBlockId();
            $item->active = 1;
        }else{
            $item = HomeBlockId::find($id);
        }

        $item->slug = $request->get('slug', null);
        $item->save();


        $item->itemByLang()->updateOrCreate([
            'home_block_id' => $item->id,
            'lang_id' => $langId
        ], [
            'name' => $request->get('name', null),
        ]);

        HomeBlockProduct::where('home_block_id',$item->id)->delete();

        if ($request->has('producstsIds') && is_array($request->input('producstsIds'))){
            foreach ($request->input('producstsIds') as $prodItem){
                $product = new  HomeBlockProduct();
                $product->product_id = $prodItem;
                $product->home_block_id = $item->id;
                $product->save();
            }
        }

        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->globalName->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId])
            ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');
        $mainPageId = Helpers::getSettingsField('home_page', parent::globalSettings());

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                HomeBlockId::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = HomeBlockId::whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        if ($mainPageId == $item->id) {
                            return response()->json([
                                'status' => false
                            ]);
                        }
                        $item->delete();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = HomeBlockId::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        if ($mainPageId == $item->id)
                            return response()->json([
                                'status' => false
                            ]);

                        $item->files()->delete();
                        $item->items()->delete();
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-from-trash');

                    }
                }
                break;
            case 'restore-from-trash':
                $items = HomeBlockId::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        if ($mainPageId == $item->id)
                            return response()->json([
                                'status' => false
                            ]);
                        $item->restore();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'restored-from-trash');
                    }
                }
                break;
            default:
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function activateItem(Request $request)
    {
        $currComponent = parent::currComponent();
        $mainPageId = Helpers::getSettingsField('home_page', parent::globalSettings());
        $item = HomeBlockId::findOrFail($request->get('id'));

        if ($mainPageId == $item->id)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => 'You can not activate or inactivate main page',
                    'type' => 'warning'
                ]
            ]);

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'activate');
        }

        $item->update(['active' => $status]);


        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function findProducts(Request $request)
    {
        $items = ProductsItemsId::where(function ($q) use ($request) {
                $q->where('id', $request->get('q'));
                $q->orWhere('slug', $request->get('q'));
                $q->orWhere('sku', $request->get('q'));
                $q->orWhereHas('globalName', function ($q) use ($request) {
                    $q->where('name', 'LIKE', "%{$request->get('q')}%");
                });
                $q->orWhereHas('variationsDetails', function ($q) use ($request) {
                    $q->where('id', $request->get('q'));
                    $q->orWhere('sku', $request->get('q'));
                });
            })
            ->with('globalName')
            ->orderBy('position')
            ->paginate(50);

        $response = selectAjaxSearchItems($request, $items);

        return $response;
    }

    public function getProductContent(Request $request)
    {
        $productId = $request->get('productId', null);
        $count = $request->get('productsCount', 0);
        $item = ProductsItemsId::find($productId);

        if (is_null($item))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => 'Product not exist.',
                    'type' => 'error'
                ]
            ]);

        $response = defineProductVariations($item);

        $response = array_merge($response, [
            'defaultCurrency' => getDefaultCurrency(),
            'increment' => $count + 1
        ]);

        try {
            $view = view("{$this->path}.templates.productList", $response)->render();
        } catch (\Throwable $e) {
            $view = '';
        }

//        dd($view);
        return response()->json([
            'status' => true,
            'view' => $view,
            'productId' => $item->id,
            'increment' => $count + 1
        ]);
    }

    public function addHomeProducts(Request  $request){
        dd($request->all());
    }

    public function productdestroy(Request  $request){
        if ($request->has('productId')){
            HomeBlockProduct::where('product_id', $request->input('productId'))->delete();
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => "Item was successful deleted!",
                'type' => 'success'
            ]
        ]);
    }



}