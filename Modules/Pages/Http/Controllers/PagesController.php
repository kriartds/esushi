<?php

namespace Modules\Pages\Http\Controllers;


use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\Models\Languages;
use Modules\Pages\Http\Requests\PagesRequest;
use Modules\Pages\Models\PagesId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PagesController extends DefaultController
{

    private $moduleName;
    private $path;
    private $frontPath;

    public function __construct()
    {
        $this->moduleName = 'pages';
        $this->path = "{$this->moduleName}::pages";
        $this->frontPath = 'front.pages';
    }

    public function index()
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = PagesId::with(['globalName', 'items'])->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'mainPageId' => Helpers::getSettingsField('home_page', parent::globalSettings())
        ];

        return view("{$this->path}.list", $response);

    }

    public function create()
    {

        $templates = parseFilesInPath("{$this->frontPath}.templates", '.blade.php');

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'templates' => $templates,
            'mainPageId' => Helpers::getSettingsField('home_page', parent::globalSettings())
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id, $langId)
    {

        $item = PagesId::findOrFail($id);

        $templates = parseFilesInPath("{$this->frontPath}.templates", '.blade.php');

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'langId' => $langId,
            'templates' => $templates,
            'mainPageId' => Helpers::getSettingsField('home_page', parent::globalSettings())
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = PagesId::onlyTrashed()->with(['globalName'])->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(PagesRequest $request, $id, $langId)
    {
        $currComponent = parent::currComponent();
        if (is_null($id) && is_null($langId)) {
            $lang = Languages::where('active', 1)
                ->find($request->get('lang'));

            if (is_null($lang))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Lang not exist!'],
                        'type' => 'warning'
                    ]
                ]);

            $langId = $lang->id;
        }


        $item = PagesId::find($id);

        if (is_null($item)) {
            $item = new PagesId();

            $item->active = 1;
        }

        $template = findFileInPath("{$this->frontPath}.templates", $request->get('template', null));

        $item->slug = $request->get('slug', null);
        $item->template = $template;

        $item->save();

        $item->itemByLang()->updateOrCreate([
            'page_id' => $item->id,
            'lang_id' => $langId
        ], [
            'name' => $request->get('name', null),
            'description' => $request->get('description', null),
            'meta_title' => $request->get('meta_title', null),
            'meta_keywords' => $request->get('meta_keywords', null),
            'meta_description' => $request->get('meta_description', null)
        ]);

        Helpers::updateValidateDynamicField($request->all(), $currComponent, $id, $langId, is_null($id) ? 'create' : 'edit');
        updateMenuItems($currComponent, $item, $langId);
        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->globalName->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId])
            ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');
        $mainPageId = Helpers::getSettingsField('home_page', parent::globalSettings());

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                PagesId::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = PagesId::whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        if ($mainPageId == $item->id) {
                            return response()->json([
                                'status' => false
                            ]);
                        }

                        $item->delete();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-to-trash');
                        updateMenuItems($currComponent, $item, null, 'destroy', $request->get('event'));
                    }
                }
                break;
            case 'delete-from-trash':
                $items = PagesId::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        if ($mainPageId == $item->id)
                            return response()->json([
                                'status' => false
                            ]);

                        $item->files()->delete();
                        $item->items()->delete();
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-from-trash');

                    }
                }
                break;
            case 'restore-from-trash':
                $items = PagesId::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        if ($mainPageId == $item->id)
                            return response()->json([
                                'status' => false
                            ]);
                        $item->restore();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'restored-from-trash');
                    }
                }
                break;
            default:
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function activateItem(Request $request)
    {
        $currComponent = parent::currComponent();
        $mainPageId = Helpers::getSettingsField('home_page', parent::globalSettings());
        $item = PagesId::findOrFail($request->get('id'));

        if ($mainPageId == $item->id)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => 'You can not activate or inactivate main page',
                    'type' => 'warning'
                ]
            ]);

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'activate');
        }

        $item->update(['active' => $status]);

        updateMenuItems($currComponent, $item, null, 'activate');

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function widgetsCountItems()
    {
        return PagesId::count();
    }

//    Methods for menu

    public function getItems()
    {
        $items = PagesId::where('active', 1)
            ->where('slug', '!=', 'main-page')
            ->with(['globalName'])
            ->get();

        return $items;
    }

    public function getItem($id)
    {
        $item = PagesId::where('active', 1)
            ->with(['globalName', 'items'])
            ->find($id);

        $response = [];

        if (!is_null($item))
            $response = (object)[
                'id' => $item->id,
                'name' => !is_null(@$item->globalName->name) ? $item->globalName->name : null,
                'slug' => $item->slug,
                'allInfo' => $item
            ];

        return $response;
    }

//    Methods for menu

}