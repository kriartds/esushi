<div class="create-new-pages-container">
    <form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
          id="{{!is_null($item) ? 'edit' : 'create'}}-form"
          enctype="multipart/form-data">
        <div class="background">
            <div class="top-container">
                <div class="field-row language">
                    <div class="label-wrap">
                        <label for="lang">{{__("{$moduleName}::e.lang")}}*</label>
                    </div>
                    <div class="field-wrap">
                        @if(!$langList->isEmpty())
                            <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                                @foreach($langList as $lang)
                                    <option value="{{$lang->id}}" {{ (is_null(@$langId) && $lang->id == LANG_ID ? 'selected' : $lang->id == @$langId ) ? 'selected' : ''}}>{{$lang->name ?: ''}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__("{$moduleName}::e.title_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$item->itemByLang->name}}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="description">{{__("{$moduleName}::e.description")}}</label>
                    </div>

                    <div class="field-wrap">
                        <textarea name="description" id="description">{{@$item->itemByLang->description}}</textarea>
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="slug">{{__("{$moduleName}::e.slug_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="slug" id="slug" value="{{@$item->slug}}">
                    </div>
                </div>
            </div>
            <div class="upload-file">
                <div class="field-row">
                    @include('admin.templates.uploadFile', [
               'item' => @$item,
               'options' => [
                   'data-component-id' => $currComponent->id,
                   'data-types' => 'image'
               ]
           ])
                </div>
            </div>

            <div class="upload-file">
                <div class="add-product-container">
                    <button type="button" class="button white-blue-gray bold full getModal" id="add-products-btn"
                            data-modal="#modal-add-home-products">Add Products
                    </button>
                </div>
            </div>
        </div>

        <div class="order-products-block" id="homeProductsBlock">
            @if(@$item->sushiPartyBlockProducts && @$item->sushiPartyBlockProducts->isNotEmpty())
                @foreach(@$item->sushiPartyBlockProducts as $product)
                    <div class="order-products-block home-products-block">
                        <table id="order-products-table">
                            <tbody>
                            <tr data-order-product-id="{{$product->id}}" class="">
                                <td colspan="2" style="width: 95%">
                                    <div class="product-info direction-column">
                                        <div class="product-name-img">
                                            <div class="product-id">
                                                <span class="tooltip" data-tippy="" data-original-title="Product ID">#{{$product->id}}</span>
                                            </div>
                                            <input type="hidden" hidden name="producstsIds[]" value="{{$product->id}}">
                                            <div class="product-img">
                                                <a href="{{asset(@$product->file->file->file)}}" data-fancybox="thumbnail-{{$product->id}}">
                                                    <img src="{{asset(@$product->file->file->file)}}">
                                                </a>
                                            </div>
                                            <div class="product-name">
                                                <a href="{{customUrl(['products', @$product->slug])}}" target="_blank" class="view-product-on-site"></a>
                                                <a href="{{customUrl(['admin', 'products', 'edit', @$product->id, LANG_ID])}}" target="_blank">{{$product->globalname->name}}</a>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                                <td>
                                    <div class="actions">
                                        <div class="destroy-home-product" data-added="true" data-url="{{customUrl(['admin', 'sushi-party', 'productdestroy'])}}"></div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                @endforeach
            @endif
        </div>

        <div class="buttonSetings">
            <button class="btn submit-form-btn"
                    data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
        </div>
    </form>
</div>


<div style="display: none;">
    <div class="modal modal-add-order-products" id="modal-add-home-products">
        <div class="modal-top">
            <div class="modal-title">Add products</div>
            <div class="modal-close arcticmodal-close"></div>
        </div>
        <div class="modal-center">
            <div class="products-items">
                <div class="add-products-header">
                    <select name="chose_home_products" id="chose-home-products" class="select2 ajax"
                            data-url="{{customUrl(['admin', $currComponent->slug, 'findProducts'])}}"
                            data-product-content-url="{{customUrl(['admin', $currComponent->slug, 'getProductContent'])}}">
                        <option value="">Chose product</option>
                    </select>
                </div>
                <div class="add-products-content" id="add-home-products-form">

                    <div class="empty-home-product-list">Chose products</div>
                </div>
            </div>
            <div class="add-one-more-product">
                <div class="icon"></div>
                ADD ANOTHER PRODUCT
            </div>
        </div>
        <div class="modal-bottom">
            <button type="button" id="add-home-products" class="button white-blue-gray bold" disabled
                    data-url="{{customUrl(['admin', $currComponent->slug, 'addHomeProducts', @$item->id])}}">Add
                products
            </button>
        </div>
    </div>
</div>

