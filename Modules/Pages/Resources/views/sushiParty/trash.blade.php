@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'trash'], 'item' => null,  'currentPage'=> 'trash'])
        <div class="container-pages trash">
            <div class="table-block">
                @if(!$items->isEmpty())
                    <table class="table">
                        <thead>
                        <tr>
                            <th class="id-center">{{__("{$moduleName}::e.id_table")}}</th>
                            <th>{{__("{$moduleName}::e.title_table")}}</th>
                            @if($permissions->edit)
                                <th>Slug</th>
                            @endif
                            <th>Template type</th>
                            <th class="align-center">Languages</th>

                            <th class="checkbox-all align-center" >
                                <div>Check all</div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr id="{{$item->id}}">
                                <td class="p-center">
                                    <p>{{$item->id}}</p>
                                </td>
                                <td class="width-row">
                                    <span> {{@$item->globalName->name}}</span>
                                </td>
                                <td class="width-row">
                                    <span>{{$item->slug}}</span>
                                </td>
                                <td class="width-row">
                                    <span>{{$item->template ? $item->template: '-' }}</span>
                                </td>

                                @if($permissions->edit)
                                    <td class=" align-center">
                                        @foreach($langList as $lang)
                                            <a  {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                                        @endforeach
                                    </td>
                                @endif
                                @if($permissions->delete)

                                    <td class="checkbox-items">
                                        <input autocomplete="off" type="checkbox" class="checkbox-item"
                                               id="{{$item->id}}"
                                               name="checkbox_items[{{$item->id}}]"
                                               value="{{$item->id}}" >
                                        <label for="{{$item->id}}">

                                        </label>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
                @endif
            </div>
        </div>
    </div>
@stop

@include('admin.footer')