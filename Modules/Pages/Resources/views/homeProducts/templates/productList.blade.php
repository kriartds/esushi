<div class="order-products-block home-products-block">
    <table id="order-products-table">
        <tbody>
            <tr data-order-product-id="{{$product->id}}" class="">
            <td colspan="2" style="width: 95%">
                <div class="product-info direction-column">
                    <div class="product-name-img">
                        <div class="product-id">
                            <span class="tooltip" data-tippy="" data-original-title="Product ID">#{{$product->id}}</span>
                        </div>
                        <input type="hidden" hidden name="producstsIds[]" value="{{$product->id}}">
                        <div class="product-img">
                            <a href="{{asset(@$product->file->file->file)}}" data-fancybox="thumbnail-{{$product->id}}">
                                <img src="{{asset(@$product->file->file->file)}}">
                            </a>
                        </div>
                        <div class="product-name">
                            <a href="{{customUrl(['products', @$product->slug])}}" target="_blank" class="view-product-on-site"></a>
                            <a href="{{customUrl(['admin', 'products', 'edit', @$product->id, LANG_ID])}}" target="_blank">{{$product->globalname->name}}</a>
                        </div>
                    </div>
                </div>
            </td>
            <td>
                <div class="actions">
                    <div class="destroy-product-order" data-url="{{customUrl(['admin', 'home-products', 'productdestroy', @$product->id, LANG_ID])}}"></div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>

