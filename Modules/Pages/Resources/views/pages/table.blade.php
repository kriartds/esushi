@if(!$items->isEmpty())
    <div class="container-pages">
        <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
            <thead>
            <tr>
                <th class="id-center">{{__("{$moduleName}::e.id_table")}}</th>
                <th>{{__("{$moduleName}::e.title_table")}}</th>
                @if($permissions->edit)
                    <th>Slug</th>
                @endif
                <th>Template type</th>
                <th class="align-center">Languages</th>
                <th class="align-center">Status</th>
                <th class="checkbox-all align-center">
                    <div>Check all</div>
                </th>
            </tr>
            </thead>

            <tbody>
            @foreach($items as $item)
                <tr id="{{$item->id}}">
                    <td class="p-center">
                        <p>{{$item->id}}</p>
                    </td>
                    <td class="prod-name left">
                        <a class="{{@$isTrash ? 'trash' : ''}}" @if(!@$isTrash) href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, DEF_LANG_ID]) . @$requestQueryString}}" @endif
                        title="{{@$item->globalName->name}}">
                            {{@$item->globalName->name}}
                        </a>
                    </td>

                    <td class="width-row">
                        <span>{{$item->slug}}</span>
                    </td>
                    <td class="width-row">
                        <span>{{$item->template}}</span>
                    </td>
                    @if($permissions->edit)
                        <td class="td-link align-center">
                            @foreach($langList as $lang)
                                <a @if(!@$isTrash) href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $lang->id])}}" @endif {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                            @endforeach
                        </td>
                    @endif

                    @if($permissions->active)
                        <td class="status align-center">
                            @if($item->id != $mainPageId)
                                <span class="activate-item {{$item->active ? 'active' : ''}}"
                                      data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                                      data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>
                            @else
                                -
                            @endif
                        </td>
                    @endif
                    @if($permissions->delete)
                        <td class="checkbox-items align-center">
                            @if($item->id != $mainPageId)
                                <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                       name="checkbox_items[{{$item->id}}]"
                                       value="{{$item->id}}">
                                <label for="{{$item->id}}">

                                </label>
                            @else
                                <span class="align-center">
                                 <svg class="tooltip" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                      viewBox="0 0 20 20" data-tippy="" data-original-title="This item has children!">
                                <g id="Group_360" data-name="Group 360" transform="translate(-1400 -1236)">
                                    <circle id="Ellipse_22" data-name="Ellipse 22" cx="10" cy="10" r="10"
                                            transform="translate(1400 1236)" fill="#9f9f9f"></circle>
                                    <path id="Path_79" data-name="Path 79"
                                          d="M2.769-3.056,3-9.953H.9l.232,6.9Zm-.82,1.08a1.091,1.091,0,0,0-.8.3,1,1,0,0,0-.3.749.992.992,0,0,0,.3.745,1.1,1.1,0,0,0,.8.294,1.107,1.107,0,0,0,.8-.294.992.992,0,0,0,.3-.745,1,1,0,0,0-.3-.752A1.107,1.107,0,0,0,1.948-1.976Z"
                                          transform="translate(1408 1251)" fill="#fff"></path>
                                </g>
                            </svg>
                            </span>
                            @endif
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
            @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
                <tfoot>
                <tr>
                    <td colspan="10">
                        @include('admin.templates.pagination', ['pagination' => $items])
                    </td>
                </tr>
                </tfoot>
            @endif
        </table>
    </div>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
