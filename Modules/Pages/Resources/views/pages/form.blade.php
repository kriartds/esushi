<div class="create-new-pages-container">
    <form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
          id="{{!is_null($item) ? 'edit' : 'create'}}-form"
          enctype="multipart/form-data">
        <div class="background">
            <div class="top-container">
                <div class="field-row language">
                    <div class="label-wrap">
                        <label for="lang">{{__("{$moduleName}::e.lang")}}*</label>
                    </div>
                    <div class="field-wrap">
                        @if(!$langList->isEmpty())
                            <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                                @foreach($langList as $lang)
                                    <option value="{{$lang->id}}" {{ (is_null(@$langId) && $lang->id == LANG_ID ? 'selected' : $lang->id == @$langId ) ? 'selected' : ''}}>{{$lang->name ?: ''}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="template">{{__("{$moduleName}::e.templates")}}</label>
                    </div>
                    <div class="field-wrap">
                        <select autocomplete="off" name="template" id="template" class="select2">
                            <option value="">{{__("{$moduleName}::e.default_templates")}}</option>
                            @if(!empty($templates))
                                @foreach($templates as $template)
                                    @if(is_null($item))
                                        @if(is_null($mainPageId) || (!is_null($mainPageId) && $template->key != 'mainPage'))
                                            <option value="{{$template->key}}" {{$template->key == @$item->template ? 'selected' : ''}}>{{$template->value}}</option>
                                        @endif
                                    @else
                                        <option value="{{$template->key}}" {{$template->key == @$item->template ? 'selected' : ''}}>{{$template->value}}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__("{$moduleName}::e.title_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$item->itemByLang->name}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="slug">{{__("{$moduleName}::e.slug_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="slug" id="slug" value="{{@$item->slug}}">
                    </div>
                </div>
            </div>


            <div class="field-row">
                <div class="label-wrap">
                    <label for="description">{{__("{$moduleName}::e.description")}}</label>
                </div>
                <div class="field-wrap">
            <textarea name="description" id="description"
                      data-type="ckeditor">{!! @$item->itemByLang->description !!}</textarea>
                </div>
            </div>
            <div class="upload-file">
                <div class="field-row">
                    @include('admin.templates.uploadFile', [
               'item' => @$item,
               'options' => [
                   'data-component-id' => $currComponent->id,
                   'data-types' => 'image'
               ]
           ])
                </div>
            </div>
            <div class="meta-container">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="meta_title">{{__("{$moduleName}::e.meta_title_page")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="meta_title" id="meta_title" value="{{@$item->itemByLang->meta_title}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="meta_keywords">{{__("{$moduleName}::e.meta_keywords_page")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="meta_keywords" id="meta_keywords" value="{{@$item->itemByLang->meta_keywords}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="meta_description">{{__("{$moduleName}::e.meta_description_page")}}</label>
                    </div>
                    <div class="field-wrap">
            <textarea name="meta_description"
                      id="meta_description">{!! @$item->itemByLang->meta_description !!}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="buttonSetings">
            <button class="btn submit-form-btn"
                    data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
        </div>
    </form>
</div>
