<?php

namespace Modules\Pages\Models;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table = 'pages';

    protected $fillable = [
        'page_id',
        'lang_id',
        'name',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

    public function setMetaDescriptionAttribute($value)
    {
        $this->attributes['meta_description'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

}
