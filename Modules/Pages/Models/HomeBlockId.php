<?php

namespace Modules\Pages\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Products\Models\ProductsItemsId;

class HomeBlockId extends Model
{

    use SoftDeletes;
    use FilesRelation;

    protected $table = 'home_block_id';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'slug',
        'active',
        'position',
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'home-products';
        self::$globalLangId = request()->segment(6, null);

    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function globalName()
    {
        return $this->hasOne(HomeBlock::class, 'home_block_id', 'id')->whereIn('home_block.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(home_block.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('Modules\Pages\Models\HomeBlock', 'home_block_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('Modules\Pages\Models\HomeBlock', 'home_block_id', 'id');
    }

    public function homeBlockProducts(){
        return $this->hasManyThrough(ProductsItemsId::class,HomeBlockProduct::class, 'home_block_id','id','id','product_id');

    }

}
