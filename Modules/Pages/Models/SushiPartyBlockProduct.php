<?php

namespace Modules\Pages\Models;

use Illuminate\Database\Eloquent\Model;

class SushiPartyBlockProduct extends Model
{
    protected $table = 'sushi_party_block_product';

    protected $fillable = [
        'sushi_party_block_id',
        'product_id',
    ];
}
