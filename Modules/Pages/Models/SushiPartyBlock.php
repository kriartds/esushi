<?php

namespace Modules\Pages\Models;

use Illuminate\Database\Eloquent\Model;

class SushiPartyBlock extends Model
{
    protected $table = 'sushi_party_block';

    protected $fillable = [
        'sushi_party_block_id',
        'lang_id',
        'name',
        'description',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

}
