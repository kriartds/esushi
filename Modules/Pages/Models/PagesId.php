<?php

namespace Modules\Pages\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PagesId extends Model
{

    use SoftDeletes;
    use FilesRelation;

    protected $table = 'pages_id';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'slug',
        'template',
        'active'
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'pages';
        self::$globalLangId = request()->segment(6, null);

    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function globalName()
    {
        return $this->hasOne(Pages::class, 'page_id', 'id')->whereIn('pages.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(pages.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('Modules\Pages\Models\Pages', 'page_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('Modules\Pages\Models\Pages', 'page_id', 'id');
    }

}
