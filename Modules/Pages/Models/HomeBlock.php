<?php

namespace Modules\Pages\Models;

use Illuminate\Database\Eloquent\Model;

class HomeBlock extends Model
{
    protected $table = 'home_block';

    protected $fillable = [
        'home_block_id',
        'lang_id',
        'name',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

}
