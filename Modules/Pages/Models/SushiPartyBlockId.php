<?php

namespace Modules\Pages\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Products\Models\ProductsItemsId;

class SushiPartyBlockId extends Model
{

    use SoftDeletes;
    use FilesRelation;

    protected $table = 'sushi_party_block_id';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'slug',
        'active',
        'position',
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'sushi-party-components';
        self::$globalLangId = request()->segment(6, null);

    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function globalName()
    {
        return $this->hasOne(SushiPartyBlock::class, 'sushi_party_block_id', 'id')->whereIn('sushi_party_block.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(sushi_party_block.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('Modules\Pages\Models\SushiPartyBlock', 'sushi_party_block_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('Modules\Pages\Models\SushiPartyBlock', 'sushi_party_block_id', 'id');
    }

    public function SushiPartyBlockProducts(){
        return $this->hasManyThrough(ProductsItemsId::class,SushiPartyBlockProduct::class, 'sushi_party_block_id','id','id','product_id');

    }

}
