<?php

namespace Modules\Pages\Models;

use Illuminate\Database\Eloquent\Model;

class HomeBlockProduct extends Model
{
    protected $table = 'home_block_product';

    protected $fillable = [
        'home_block_id',
        'product_id',
    ];
}
