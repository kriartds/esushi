<?php

return [

    'element_is_active' => 'The element :name is active!',
    'element_is_inactive' => 'The element :name is inactive!',
    'element_is_hidden' => 'The element :name is hidden!',
    'element_is_visible' => 'The element :name is visible!',

    'title_table' => 'Title',
    'edit_table' => 'Edit',
    'active_table' => 'Status ',
    'position_table' => 'Position',
    'delete_table' => 'Delete',
    'reestablish_table' => 'Reestablish',
    'id_table' => 'ID',
    'list_is_empty' => 'List is empty!',
    'save_it' => 'Save',
    'lang' => 'Lang',

    'countries' => 'Countries',
    'regions' => 'Regions',
    'address' => 'Address',
    'latitude' => 'Latitude',
    'longitude' => 'Longitude',
    'for_pickup' => 'Show in pickup delivery (on checkout)',
    'use_for_pickup' => 'For pickup',
    'yes' => 'Yes',
    'no' => 'No',

];