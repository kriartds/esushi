<form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form"
      enctype="multipart/form-data">
    <div class="background">
        <div class="top-container smallContainer">
            <div class="field-row language">
                <div class="label-wrap">
                    <label for="lang">{{__("{$moduleName}::e.lang")}}*</label>
                </div>
                <div class="field-wrap">
                    @if(!$langList->isEmpty())
                        <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                            @foreach($langList as $lang)
                                <option value="{{$lang->id}}" {{ (is_null(@$langId) && $lang->id == LANG_ID ? 'selected' : $lang->id == @$langId ) ? 'selected' : ''}}>
                                    {{$lang->name ?: ''}}
                                </option>
                            @endforeach
                        </select>
                    @endif
                </div>
            </div>
            <div class="field-row title">
                <div class="label-wrap">
                    <label for="name">{{__("{$moduleName}::e.title_table")}}*</label>
                </div>
                <div class="field-wrap">
                    <input name="name" id="name" value="{{@$item->itemByLang->name}}">
                </div>
            </div>

            <div class="field-row smallSelect">
                <div class="label-wrap">
                    <label for="country">{{__("{$moduleName}::e.countries")}}*</label>
                </div>
                <div class="field-wrap">
                    <select autocomplete="off" name="country" id="country"
                            class="select2 ajax countries-list"
                            data-url="{{customUrl(['admin', $currComponent->slug, 'getCountries'])}}">
                        @if(@$item->country)
                            <option value="{{$item->country_id}}"
                                    selected>{{"#{$item->country_id} | {$item->country->name}"}}</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="field-row smallSelect">
                <div class="label-wrap">
                    <label for="region">{{__("{$moduleName}::e.regions")}}*</label>
                </div>
                <div class="field-wrap">
                    <select autocomplete="off" name="region" id="region" class="select2 ajax regions-list"
                            data-url="{{customUrl(['admin', $currComponent->slug, 'getRegions']) . (@$item ? "?countryId={$item->country_id}" : '')}}">
                        @if(@$item->region)
                            <option value="{{$item->region_id}}"
                                    selected>{{"#{$item->region_id} | {$item->region->name}"}}</option>
                        @endif
                    </select>
                </div>
            </div>



            <div class="field-row">
                <div class="label-wrap">
                    <label for="city">City*</label>
                </div>
                <div class="field-wrap">
                    <input name="city" id="city" value="{{ @$item->itemByLang->city }}">
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="address">{{__("{$moduleName}::e.address")}}*</label>
                </div>
                <div class="field-wrap">
                    <input name="address" id="address" value="{{ @$item->itemByLang->address }}">
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="latitude">{{__("{$moduleName}::e.latitude")}}</label>
                </div>
                <div class="field-wrap">
                    <input name="latitude" id="latitude" value="{{@$item->location->lat}}">
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="longitude">{{__("{$moduleName}::e.longitude")}}</label>
                </div>
                <div class="field-wrap">
                    <input name="longitude" id="longitude" value="{{@$item->location->lng}}">
                </div>
            </div>
            <div class="field-row selectImg">
                @include('admin.templates.uploadFile', [
                    'item' => @$item,
                    'options' => [
                        'data-component-id' => $currComponent->id,
                        'data-types' => 'image'
                    ]
                ])
            </div>

            <div class="field-row switcher-container">
                <div class="label-wrap">
                    <label for="display_in_filter">Show in pickup delivery (on checkout)</label>
                </div>

                <div class="checkbox-switcher">
                    <input type="checkbox" name="for_pickup"
                           id="for_pickup" {{@$item->for_pickup ? 'checked' : ''}}>
                    <label for="for_pickup"></label>
                </div>
            </div>
        </div>
    </div>
    <div class="m-left">
        <button class="btn medium submit-form-btn"
                data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
    </div>
</form>

