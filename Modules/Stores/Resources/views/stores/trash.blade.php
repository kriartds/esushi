@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'trash'], 'item' => null, 'currentPage' => 'trash'])

        <div class="table-block">
            @if(!$items->isEmpty())
                <table class="table trash">
                    <thead>
                    <tr>
                        <th>{{__("{$moduleName}::e.id_table")}}</th>
                        <th class="left">{{__("{$moduleName}::e.title_table")}}</th>
                        <th class="left">Address</th>
                        <th class="left">Latitude / Longitude</th>
                        <th>Languages</th>
                        <th class="checkbox-all align-center" >
                            <div>Check all</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td class="id">
                                <p>{{$item->id}}</p>
                            </td>
                            <td class="left">
                                <a>{{$item->globalName->name}}</a>
                            </td>
                            <td class="medium left">
                                <p>{{@$item->country->name .', '. @$item->region->name.', '. @$item->globalName->city.', '. @$item->globalName->address}}</p>
                            </td>

                            <td class="medium left">{{$item->location->lat}} / {{$item->location->lng}}</td>
                            @if($permissions->edit)
                                <td class=" ">
                                    @foreach($langList as $lang)
                                        <a {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                                    @endforeach
                                </td>
                            @endif
                            @if($permissions->delete)
                                <td class="checkbox-items">
                                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                           name="checkbox_items[{{$item->id}}]"
                                           value="{{$item->id}}">
                                    <label for="{{$item->id}}">
                                    </label>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')