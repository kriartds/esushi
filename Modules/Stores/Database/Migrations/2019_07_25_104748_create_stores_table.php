<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('store_id');
            $table->unsignedInteger('lang_id')->nullable();
            $table->string('name')->nullable();
            $table->string('address', 1024)->nullable();
            $table->timestamps();

            $table->foreign('store_id')->references('id')->on('stores_id')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('set null')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
