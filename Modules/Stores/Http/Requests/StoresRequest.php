<?php

namespace Modules\Stores\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoresRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $countryId = request()->get('country', null);
        $regionId = request()->get('region', null);
        $latitude = request()->get('latitude', null);
        $longitude = request()->get('longitude', null);

        $countryRules = Rule::exists('countries', 'id')->where(function ($q) use ($countryId) {
            $q->where('id', $countryId);
            $q->where('active', 1);
        });

        $regionRules = Rule::exists('regions', 'id')->where(function ($q) use ($countryId, $regionId) {
            $q->where('id', $regionId);
            $q->where('active', 1);
            $q->where('country_id', $countryId);
        });

        $rules = [
            'lang' => 'required',
            'name' => 'required',
            'address' => 'required',
            'country' => [
                'required',
                $countryRules,
            ],
            'region' => [
                'required',
                $regionRules,
            ]
        ];

        if (($latitude && !$longitude) || (!$latitude && $longitude) || ($latitude && $longitude))
            $rules = array_merge($rules, [
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric'
            ]);
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
