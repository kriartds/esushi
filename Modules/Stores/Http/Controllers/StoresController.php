<?php

namespace Modules\Stores\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\IikoApi\IikoClient;
use App\IikoApi\Models\IikoCities;
use App\IikoApi\Models\IikoStreets;
use App\Models\Languages;
use Illuminate\Support\Facades\DB;
use Modules\CountriesRegion\Models\Countries;
use Modules\CountriesRegion\Models\Regions;
use App\Repositories\LanguagesRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\Brands\Models\Brands;
use Modules\Products\Models\ProductsAdditionsId;
use Modules\Products\Models\ProductsItemsId;
use Modules\Products\Models\ProductsToppingsId;
use Modules\Stores\Http\Requests\StoresRequest;
use Modules\Stores\Models\StoresId;

class StoresController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'stores';
        $this->path = "{$this->moduleName}::stores";
    }

    public function showOnPickup(Request $request)
    {
        $currComponent = parent::currComponent();
        $item = StoresId::findOrFail($request->get('id'));

        $item->update(['for_pickup' => !(int)$request->get('active')]);

        if ($request->get('active')) {
            $msg = __("{$this->moduleName}::e.element_is_hidden", ['name' => @$item->globalName->name]);
        } else {
            $msg = __("{$this->moduleName}::e.element_is_visible", ['name' => @$item->globalName->name]);
        }

        try {
            helpers()->logActions($currComponent, $item, @$item->globalName->name, $request->get('action'));
        }catch(\Throwable $e) {

        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function index()
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', $this->globalSettings());

        $items = StoresId::with(['globalName', 'items'])
            ->orderBy('position')
            ->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.list", $response);

    }

    public function create()
    {

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id, $langId)
    {

        $item = StoresId::findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'langId' => $langId,
            'item' => $item,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = StoresId::onlyTrashed()->with(['globalName'])->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(StoresRequest $request, $id, $langId)
    {

        $currComponent = $this->currComponent();

        $countryId = $request->get('country', null);
        $regionId = $request->get('region', null);
        $latitude = $request->get('latitude', null);
        $longitude = $request->get('longitude', null);

        $countryRules = Rule::exists('countries', 'id')->where(function ($q) use ($countryId) {
            $q->where('id', $countryId);
            $q->where('active', 1);
        });

        $regionRules = Rule::exists('regions', 'id')->where(function ($q) use ($request, $countryId, $regionId) {
            $q->where('id', $regionId);
            $q->where('active', 1);
            $q->where('country_id', $countryId);
        });

        $rules = [
            'lang' => 'required',
            'name' => 'required',
            'city' => 'required',
            'address' => 'required',
            'country' => [
                'required',
                $countryRules,
            ],
            'region' => [
                'required',
                $regionRules,
            ]
        ];

        if (($latitude && !$longitude) || (!$latitude && $longitude) || ($latitude && $longitude))
            $rules = array_merge($rules, [
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric'
            ]);

        $item = Validator::make($request->all(), $rules);

        if ($item->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $item->messages(),
                    'type' => 'error'
                ],
            ]);

        if (is_null($id) && is_null($langId)) {
            $languagesRepo = new LanguagesRepository();
            $lang = $languagesRepo->find($request->get('lang'), ['active' => 1]);

            if (is_null($lang))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Lang not exist!'],
                        'type' => 'warning'
                    ]
                ]);

            $langId = $lang->id;
        }

        $item = StoresId::find($id);

        if (is_null($item)) {
            $item = new StoresId();

            $nextPosition = Helpers::getNextItemPosition($item);
            $item->position = $nextPosition;
            $item->active = 1;
        }

        $item->location = $latitude && $longitude ? [
            'lat' => $latitude,
            'lng' => $longitude
        ] : null;
        $item->country_id = $countryId;
        $item->region_id = $regionId;
        $item->for_pickup = $request->get('for_pickup', null) == 'on' ? 1 : 0;

        $item->save();

        $item->itemByLang()->updateOrCreate([
            'store_id' => $item->id,
            'lang_id' => $langId
        ], [
            'name' => $request->get('name', null),
            'address' => $request->get('address', null),
            'city' => $request->get('city', null),
        ]);

        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->globalName->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId])
            ]);
    }

    public function actionsCheckbox(Request $request)
    {

        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                StoresId::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = StoresId::whereIn('id', $ItemsId)->with('globalName')->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                  $items = StoresId::onlyTrashed()->whereIn('id', $ItemsId)->with('globalName')->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->files()->delete();
                        $item->items()->delete();
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                  $items = StoresId::onlyTrashed()->whereIn('id', $ItemsId)->with('globalName')->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                    }
                }
                break;
            default:
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function updatePosition(Request $request)
    {

        $order = $request->get('order');

        if (!is_array($order))
            return response()->json([
                'status' => false
            ]);

        $itemsPositions = [];
        foreach ($order as $id) {
            $item = StoresId::find($id);
            $itemsPositions[] = @$item->position;
        }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($order as $key => $id) {
            StoresId::where('id', $id)->update(['position' => $itemsPositions[$key]]);
        }

        return response()->json([
            'status' => true
        ]);
    }

    public function activateItem(Request $request)
    {

        $currComponent = $this->currComponent();
        $item = StoresId::findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function getCountries(Request $request)
    {

        if (!$request->ajax())
            abort(404);

        $items = Countries::where('active', 1)
            ->where(function ($q) use ($request) {
                $q->where('name', 'LIKE', "%{$request->get('q')}%");
                $q->orWhere('iso', 'LIKE', "%{$request->get('q')}%");
            })
            ->whereHas('regions', function ($q) {
                $q->where('active', 1);
            })
            ->orderBy('name')
            ->paginate(10);

        $response = selectAjaxSearchItems($request, $items, 'name', null);

        return $response;
    }

    public function getRegions(Request $request)
    {

        if (!$request->ajax())
            abort(404);

        $countryId = json_decode($request->get('countryId', null));

        $items = Regions::where('active', 1)
            ->where('country_id', $countryId)
            ->where('name', 'LIKE', "%{$request->get('q')}%")
            ->whereHas('country', function ($q) {
                $q->where('active', 1);
            })
            ->orderBy('name')
            ->paginate(10);

        $response = selectAjaxSearchItems($request, $items, 'name', null);

        return $response;
    }

    public function synchronizeIiko($storeid, $langid)
    {
        $store = StoresId::findOrFail($storeid);

        $iiko = new IikoClient($store);

        $resp = $iiko->getMenu();

        if(isset($resp['products']) && !empty($resp['products']))
        {
            //dd($resp['products']);
            foreach ($resp['products'] as $product)
            {
                if($product['type'] == 'dish')
                {
                    $this->syncProducts($product, $storeid);
                }
                elseif($product['type'] == 'modifier')
                {
                    $this->syncAdditions($product);
                }

                $this->synchronizeIikoProducts($product, $storeid);//pentru analiza si studiere iiko
            }

            //find accept modifiers and set
            $this->setAcceptModifiers($resp['products']);
        }

        if(isset($resp['groups']) && !empty($resp['groups']))
        {
            foreach ($resp['groups'] as $group)
            {
                $savedgroup = DB::table('iiko_groups')->where('iiko_id', $group['id'])->first();
                if(is_null($savedgroup))
                {
                    DB::table('iiko_groups')->insert(['iiko_id' => $group['id'], 'name' => $group['name']]);
                }
            }
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => "Sincronizarea a avut loc cu success",
                'type' => 'info',
            ]
        ]);
    }

    private function syncProducts($productIiko, $storeid)
    {
        $item = ProductsItemsId::where('iiko_id', $productIiko['id'])->orWhere('slug', \Illuminate\Support\Str::slug($productIiko['name']))->withTrashed()->first();

        //if($productIiko['id'] == '6bbf7137-2c81-42b2-bdf7-a2f5233ddd79')
        //{
        //dd($productIiko);
        //}

        if(is_null($item))
        {
            $item = new ProductsItemsId();
            $item->slug = $productIiko['name'];
            $item->iiko_id = $productIiko['id'];
            $item->iiko_group_id = $productIiko['parentGroup'];
            $item->item_type_id = 1;
            $item->brand_id = null;
            $item->price = $productIiko['price'];
            $item->sale_price = 0;
            $item->giant_price = 0;
            $item->use_stock = 0;
            $item->stock_quantity = null;
            $item->status = 'in_stock';
            $item->weight = $productIiko['weight'];
            $item->length = null;
            $item->width = null;
            $item->height = null;
            $item->sku = null;
            $item->incomplete_data = 0;
            $item->show_on_main = 0;
            $item->recommended_by_categories_ids = [];
            $item->category_id = 0;
            $item->store_id = $storeid;
            $item->only_in_store = 0;
            $item->recommended = 0;
            $item->to_gift = 0;
            $item->tags = null;
            $item->quantity_label = null;
            $item->promotion = null;
            $item->save();

            $langs = Languages::where('active', 1)->get();
            foreach ($langs as $lang)
            {
                $item->itemByLang()->updateOrCreate([
                    'products_item_id' => $item->id,
                    'lang_id' => $lang->id
                ], [
                    'name' => $productIiko['name'],
                    'description' => $productIiko['name'],
                    'meta_title' => $productIiko['name'],
                    'meta_keywords' => null,
                    'meta_description' => null
                ]);
            }

        }else{
            //product exist and can be update
            $item->price = $productIiko['price'];
            $item->weight = $productIiko['weight'];

            if(empty($item->iiko_id))
                $item->iiko_id = $productIiko['id'];

            if(empty($item->iiko_code))
                $item->iiko_code = $productIiko['code'];

            if(empty($item->iiko_group_id))
                $item->iiko_group_id = $productIiko['parentGroup'];

            $item->save();
        }
    }

    private function syncAdditions($productIiko)
    {
        $item = ProductsAdditionsId::where('iiko_id', $productIiko['id'])->withTrashed()->first();

        if(is_null($item))
        {
            $item = new ProductsAdditionsId();
            $item->slug = $productIiko['name'];
            $item->price = $productIiko['price'];
            $item->iiko_id = $productIiko['id'];
            $item->iiko_group_id = $productIiko['parentGroup'];

            if(strpos($productIiko['name'], '++ ') !== false) $item->modifier_type = 2;
            elseif(strpos($productIiko['name'], '+ ') !== false) $item->modifier_type = 1;
            elseif(strpos($productIiko['name'], '- ') !== false) $item->modifier_type = 3;

            $item->save();

            $langs = Languages::where('active', 1)->get();
            foreach ($langs as $lang)
            {
                $item->itemByLang()->updateOrCreate([
                    'products_addition_id' => $item->id,
                    'lang_id' => $lang->id
                ], [
                    'name' => $productIiko['name'],
                    'description' => $productIiko['name'],
                    'meta_title' => $productIiko['name'],
                    'meta_keywords' => null,
                    'meta_description' => null
                ]);
            }

        }else{
            //product exist and can be update
            $item->price = $productIiko['price'];
            $item->iiko_group_id = $productIiko['groupId'];

            if(empty($item->iiko_id))
                $item->iiko_id = $productIiko['id'];

            if(empty($item->iiko_code))
                $item->iiko_code = $productIiko['code'];

//            if(strpos($productIiko['name'], '++') !== false) $item->modifier_type = 2;
//            elseif(strpos($productIiko['name'], '+ ') !== false) $item->modifier_type = 1;
//            elseif(strpos($productIiko['name'], '- ') !== false) $item->modifier_type = 3;

            $item->save();
        }
    }

    public function synchronizeIikoAddress($storeid, $langid)
    {
        $store = StoresId::findOrFail($storeid);

        $iiko = new IikoClient($store);

        $resp = $iiko->getPaymentTypes();
        dd($resp);

        $resp = $iiko->getCitiesWithStreets();
        //dump($resp);
        if(isset($resp[0]['city']) && !empty($resp[0]['city']))
        {
            foreach ($resp as $item)
            {
                //dd($item['city']);
                $ic = IikoCities::where('iiko_id', $item['city']['id'])->first();
                if(is_null($ic)){
                    $ic = new IikoCities();
                    $ic->iiko_id = $item['city']['id'];
                    $ic->externalRevision = $item['city']['externalRevision'];
                    $ic->store_id = $storeid;
                }

                $ic->name = $item['city']['name'];
                $ic->save();

                if(isset($item['streets']) && !empty($item['streets']))
                {
                    foreach ($item['streets'] as $street)
                    {
                        $is = IikoStreets::where('iiko_id', $street['id'])->first();
                        if(is_null($is)){
                            $is = new IikoStreets();
                            $is->iiko_city_id = $item['city']['id'];
                            $is->iiko_id = $street['id'];
                            $is->externalRevision = $street['externalRevision'];
                            $is->store_id = $storeid;
                        }
                        $is->name = $street['name'];
                        $is->save();
                    }
                }


            }
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => "Sincronizarea a avut loc cu success",
                'type' => 'info',
            ]
        ]);
    }

    public function synchronizeIikoProducts($productIiko, $storeid)
    {
        $iikoProduct = \App\IikoApi\Models\IikoProducts::where('iiko_id', $productIiko['id'])->first();
        if(is_null($iikoProduct))
        {
            $iikoProduct = new \App\IikoApi\Models\IikoProducts();
            $iikoProduct->iiko_id = $productIiko['id'];
            $iikoProduct->store_id = $storeid;
            $iikoProduct->code = $productIiko['code'];
            $iikoProduct->name = $productIiko['name'];
            $iikoProduct->group_id = $productIiko['groupId'];
            $iikoProduct->group_modifiers = $productIiko['groupModifiers'];
            $iikoProduct->modifiers = $productIiko['modifiers'];
            $iikoProduct->price = $productIiko['price'];
            $iikoProduct->type = $productIiko['type'];
            $iikoProduct->parent_group = $productIiko['parentGroup'];
            $iikoProduct->order = $productIiko['order'];
            $iikoProduct->is_included_in_menu = $productIiko['isIncludedInMenu'];
        }
        else
        {
            if(is_null($iikoProduct->store_id)) $iikoProduct->store_id = $storeid;
            elseif($iikoProduct->store_id != $storeid) $iikoProduct->store_id = 0;

            $iikoProduct->group_id = $productIiko['groupId'];
            $iikoProduct->group_modifiers = $productIiko['groupModifiers'];
            $iikoProduct->modifiers = $productIiko['modifiers'];
            $iikoProduct->price = $productIiko['price'];
            $iikoProduct->type = $productIiko['type'];
            $iikoProduct->parent_group = $productIiko['parentGroup'];
            $iikoProduct->order = $productIiko['order'];
            $iikoProduct->is_included_in_menu = $productIiko['isIncludedInMenu'];
        }

        $iikoProduct->save();
    }

    private function setAcceptModifiers($products)
    {
        foreach ($products as $product)
        {
            if($product['type'] == 'dish' && isset($product['groupModifiers']) && !empty($product['groupModifiers']))
            {
                $item = ProductsItemsId::where('iiko_id', $product['id'])->withTrashed()->first();
                if(!is_null($item))
                {
                    $additionsArr = [];
                    foreach ($product['groupModifiers'] as $groupModifier)
                    {

                        if(isset($groupModifier['childModifiers']) && !empty($groupModifier['childModifiers']))
                        {
                            foreach ($groupModifier['childModifiers'] as $modifier)
                            {
                                $addition = ProductsToppingsId::where('iiko_id', $modifier['modifierId'])->first();

                                if($addition)
                                {
                                    $additionsArr[] = $addition->id;
                                }
                            }
                        }else{
                            $addition = ProductsToppingsId::where('iiko_id', $groupModifier['modifierId'])->first();
                            if($addition)
                            {
                                $additionsArr[] = $addition->id;
                            }
                        }
                    }
                    $item->accept_modifiers = $additionsArr;
                    $item->save();
                }
            }
        }
    }
}
