<?php

namespace Modules\Stores\Models;

use Modules\CountriesRegion\Models\Countries;
use Modules\CountriesRegion\Models\Regions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FilesRelation;

class StoresId extends Model
{

    use SoftDeletes;
    use FilesRelation;

    protected $table = 'stores_id';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'country_id',
        'region_id',
        'location',
        'active',
        'position',
        'for_pickup',

    ];

    protected $casts = [
        'location' => 'object'
    ];


    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        self::$component = 'stores';
        self::$globalLangId = request()->segment(6, null);
    }

    public function scopeAvailableStoreForPickup($q)
    {
        return $q->where('active', 1)
            ->where('for_pickup', 1);
    }

    public function globalName()
    {
        return $this->hasOne(Stores::class, 'store_id', 'id')->whereIn('stores.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(stores.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne(Stores::class, 'store_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany(Stores::class, 'store_id', 'id');
    }

    public function country()
    {
        return $this->hasOne(Countries::class, 'id', 'country_id');
    }

    public function region()
    {
        return $this->hasOne(Regions::class, 'id', 'region_id');
    }
}
