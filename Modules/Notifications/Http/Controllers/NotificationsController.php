<?php

namespace Modules\Notifications\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use Illuminate\Http\Request;

class NotificationsController extends DefaultController
{
    private $moduleName;
    private $path;
    private $authUser;

    public function __construct()
    {
        $this->moduleName = 'notifications';
        $this->path = "{$this->moduleName}::notifications";
        $this->authUser = auth()->guard('admin')->user();
    }

    public function index(Request $request)
    {


        $response = $this->filter($request);
        $response['path'] = $this->path;

        return view("{$this->path}.list", $response);
    }

    public function filter(Request $request)
    {
        $perPage = helpers()->getSettingsField('cms_items_per_page', $this->globalSettings());

        $filterParams = array_filter($request->except('page'));

        if (!$request->ajax()) {
            $newFiltersParams = [];

            if (!empty($filterParams) && count($filterParams) > 0) {

                foreach ($filterParams as $key => $one_filter_elem) {
                    $newFiltersParams[$key] = $one_filter_elem;
                    if(strpos($one_filter_elem, '[') !== false || strpos($one_filter_elem, ']') !== false) {
                        $newFiltersParams[$key] = explode(',', substr($filterParams[$key], 1, -1));
                    }
                }
            }

            $filterParams = $newFiltersParams;
        }

        $pushUrl = '';

        if (!empty($filterParams)) {
            foreach ($filterParams as $key => $one_filter_el) {

                if (is_array($one_filter_el)) {
                    $pushUrlArr = '';
                    foreach ($one_filter_el as $k => $filter_el) {
                        $pushUrlArr .= $filter_el . ',';
                    }

                    $pushUrl .= $key . '=[' . strip_tags(substr($pushUrlArr, 0, -1)) . ']&';
                } else {
                    $pushUrl .= $key . '=' . strip_tags(str_replace('[]', '', $one_filter_el)) . '&';
                }
            }

            $pushUrl = '?' . substr($pushUrl, 0, -1);
        }

        $items = $this->authUser->notifications()->where(function ($q) use ($filterParams, $request) {
            if (array_key_exists('id', $filterParams) && !is_array($filterParams['id']))
                $q->where('id', $filterParams['id']);

            if (array_key_exists('type', $filterParams) && !is_array($filterParams['type']))
                $q->where(function ($q) use ($filterParams) {
                    if ($filterParams['type'] == 'read')
                        $q->whereNotNull('read_at');
                    elseif ($filterParams['type'] == 'unread')
                        $q->whereNull('read_at');
                });

            if (array_key_exists('start_date', $filterParams) && !array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']))
                $q->whereDate('created_at', '>=', date('Y-m-d', strtotime($filterParams['start_date'])));
            elseif (!array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['end_date']))
                $q->whereDate('created_at', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));
            elseif (array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']) && !is_array($filterParams['end_date']))
                $q->whereDate('created_at', '>=', date('Y-m-d', strtotime($filterParams['start_date'])))->whereDate('created_at', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));

        })
            ->orderBy('read_at')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);

        $items->setPath(url(LANG, ['admin', $this->currComponent()->slug]) . $pushUrl);

        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
                abort(503);
            }

            return response()->json($response);
        }

        return $response;

    }

    public function view($id)
    {

        $item = $this->authUser->notifications()->findOrFail($id);

        if (!$item->read_at)
            $item->markAsRead();

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item
        ];

        return view("{$this->path}.view", $response);
    }

    public function destroy(Request $request)
    {

        $deletedItemsId = substr($request->get('id'), 1, -1);

        if (empty($deletedItemsId))
            return response()->json([
                'status' => false
            ]);

        if ($request->get('event') != 'to-trash' && $request->get('event') != 'from-trash' && $request->get('event') != 'restore')
            return response()->json([
                'status' => false
            ]);

        $deletedItemsIds = explode(',', $deletedItemsId);

        $items = $this->authUser->notifications()->whereIn('id', $deletedItemsIds)->get();

        $cartMessage = $responseMsg = '';

        if (!$items->isEmpty()) {
            foreach ($items as $item) {

                $cartMessage .= $item->id . ', ';

                if ($request->get('event') == 'from-trash') {
                    $item->delete();
                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' was removed' : '';
                }

            }

            if (!empty($cartMessage)) {
                $cartMessage = substr($cartMessage, 0, -2);
                helpers()->logActions($this->currComponent(), $cartMessage, 'notifications', 'deleted');
            }

            return response()->json([
                'status' => true,
                'cart_messages' => $responseMsg,
                'items' => $deletedItemsIds
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }

    public function widgetsCountItems()
    {
        return $this->authUser->notifications()->whereNull('read_at')->count();
    }

    public function countItems()
    {
        return $this->authUser->notifications()->whereNull('read_at')->count();
    }
}
