@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">

        @include('admin.templates.pageTopButtons', ['action' => ['list' , !is_null(@$item) ? 'view' : ''], 'item' => @$item, 'langId' => @$langId])

        <div class="notification-page">
            <div class="title">{{@$item->data['title']}}</div>

            @if(@$item->data['url'] && !@$item->data['errorMsg'])
                <div class="link">
                    {{__("{$moduleName}::e.access_link")}} <a
                            href="{{$item->data['url']}}">{{@$item->data['linkText']}}</a>
                </div>
            @endif

            @if(@$item->data['errorMsg'])
                <div class="error-messages-title">{{__("{$moduleName}::e.error_messages")}}</div>
                <div class="error-messages">
                    @foreach($item->data['errorMsg'] as $line => $errors)
                        <div class="row-title">Row {{$line}}</div>
                        <div class="error-items">
                            @foreach($errors as $error)
                                <div class="error-item">{{$error}}</div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>

@stop

@include('admin.footer')

