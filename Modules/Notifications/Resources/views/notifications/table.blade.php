@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="medium">{{__("{$moduleName}::e.id_table")}}</th>
            <th class="medium left">{{__("{$moduleName}::e.title")}}</th>
            <th class="left">{{__("{$moduleName}::e.read_at")}}</th>
            <th>{{__("{$moduleName}::e.date")}}</th>
            <th></th>
            @if($permissions->delete)
                <th class="checkbox-all" >
                    <div>Select All</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}" class="{{is_null($item->read_at) ? 'unread' : ''}}">
                <td class="medium id">
                    <p>{{@$item->id}}</p>
                </td>
                <td class="medium left td-link">
                    <a href="">{{@$item->data['title'] ?: '-'}}</a>
                </td>
                <td class="left">{{strtotime($item->read_at) ? date('d-m-Y H:i:s', strtotime($item->read_at)): '-'}}</td>
                <td>{{strtotime($item->created_at) ? date('d-m-Y H:i:s', strtotime($item->created_at)): '-'}}</td>
{{--                @if($permissions->view)--}}
                {{--                    <td class="td-link">--}}
                {{--                        <a href="{{url(LANG, ['admin', $currComponent->slug, 'view', $item->id])}}">{{__("{$moduleName}::e.view")}}</a>--}}
                {{--                    </td>--}}
                {{--                @endif--}}
                <td class="empty-td"></td>
                @if($permissions->delete)
                    <td class="checkbox-items">

                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}">

                        <label for="{{$item->id}}">

                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
