@if(isset($filterParams))
    <div class="right-col">
        <button type="button" class="button gray filter-btn">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12">
                <g>
                    <g>
                        <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2"
                              d="M1 1h14"></path>
                    </g>
                    <g>
                        <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2"
                              d="M4 6h8"></path>
                    </g>
                    <g>
                        <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2"
                              d="M6 11h4"></path>
                    </g>
                </g>
            </svg>
            Filters
            <span class="count">{{$count}}</span>
        </button>
    </div>

    <div class="bacgkground-filter">
        <div class="filter form-block">
            <form method="post" action="{{url(LANG, ['admin', $currComponent->slug, 'filter'])}}"
                  class="filter-form"
                  id="filter-form">
                <div class="field-row-wrap col">
                    <div class="flex">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="id">{{__("{$moduleName}::e.id_table")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input name="id" id="id"
                                       value="{{ !empty($filterParams) && array_key_exists('id', $filterParams) ? $filterParams['id'] : '' }}">
                            </div>
                        </div>
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="type">{{__("{$moduleName}::e.type")}}</label>
                            </div>
                            <div class="field-wrap">
                                <select name="type" id="type" class="select2 no-search">
                                    <option value="">-</option>
                                    <option value="read" {{!empty($filterParams) && array_key_exists('type', $filterParams) && $filterParams['type'] == 'read' ? 'selected' : ''}}>
                                        {{__("{$moduleName}::e.read")}}
                                    </option>
                                    <option value="unread" {{!empty($filterParams) && array_key_exists('type', $filterParams) && $filterParams['type'] == 'unread' ? 'selected' : ''}}>
                                        {{__("{$moduleName}::e.unread")}}
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="flex row-flex">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="start_date">{{__("{$moduleName}::e.from_date")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="start_date" id="start_date" class="datetimepicker"
                                       value="{{ !empty($filterParams) && array_key_exists('start_date', $filterParams) ? date('d-m-Y', strtotime($filterParams['start_date'])) : '' }}">
                            </div>
                        </div>
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="end_date">{{__("{$moduleName}::e.to_date")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="end_date" id="end_date" class="datetimepicker"
                                       value="{{ !empty($filterParams) && array_key_exists('end_date', $filterParams) ? date('d-m-Y', strtotime($filterParams['end_date'])) : '' }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field-btn">
                    <button class="btn btn-inline half submit-form-btn" data-form-id="filter-form"
                            data-form-event="submit-form">{{__("{$moduleName}::e.filter")}}
                    </button>
                    <button class="btn btn-inline half borderButton submit-form-btn" data-form-id="filter-form"
                            data-form-event="refresh-form">{{__("{$moduleName}::e.reset")}}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endif