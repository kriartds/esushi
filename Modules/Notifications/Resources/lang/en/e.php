<?php

return [
    'id_table' => 'ID',
    'delete_table' => 'Delete',
    'list_is_empty' => 'List is empty!',
    'from_date' => 'From date',
    'to_date' => 'To date',
    'filter' => 'Filter',
    'reset' => 'Reset',
    'date' => 'Date',

    'view' => 'View',
    'message' => 'Message',
    'type' => 'Type',
    'read_at' => 'Read at',
    'read' => 'Read',
    'unread' => 'Unread',
    'title' => 'Title',
    'error_messages' => 'Error messages',
    'access_link' => 'Access link',

];