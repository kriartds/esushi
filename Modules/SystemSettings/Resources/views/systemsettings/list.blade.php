@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')


@section('container')
    <div class="container">
        <div class="breadcrumbs">
            <div class="linkContainer"> <a>System Settings</a></div>
        </div>

        <h1 class="component-title">{{@$currComponent->globalName->name_category}}</h1>

        <div class="middleContainerSystemSettings">
            <div class="flex">
                {{--TODO: Add permison functional adn if exist Module --}}

                @foreach($childrenComponent  as $children )
                    <a href="{{adminUrl($children->slug)}}">
                        @if($children->slug != 'sitemap')
                            <div class="cartSettings">
                                <div class="cartGroup">
                                    <div class="imgSettings">
                                        <img src="{{$children->firstFile->original}}" alt="{{@$children->globalName->name}}">
                                    </div>
                                    <div class="textSettings">
                                        {{@$children->globalName->name}}
                                    </div>
                                </div>
                            </div>

                        @else
                            <div class="buttonSettings" id="sitemap">
                                <button class="btn">
                                    <img src="{{$children->firstFile->original}}" alt="{{@$children->globalName->name}}">
                                    {{@$children->globalName->name}}
                                </button>
                            </div>
                        @endif
                    </a>
                @endforeach

{{--                    <div class="cartSettings">--}}
{{--                    <div class="cartGroup">--}}
{{--                        <div class="imgSettings">--}}
{{--                            <img src="{{asset('admin-assets/img/small-icons/users.svg')}}" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="textSettings">--}}
{{--                            Users--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                    <div class="cartSettings">--}}
{{--                        <div class="cartGroup">--}}
{{--                            <div class="imgSettings">--}}
{{--                                <img src="{{asset('admin-assets/img/small-icons/roles.svg')}}" alt="">--}}
{{--                            </div>--}}
{{--                            <div class="textSettings">--}}
{{--                                Roles--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="cartSettings">--}}
{{--                        <div class="cartGroup">--}}
{{--                            <div class="imgSettings">--}}
{{--                                <img src="{{asset('admin-assets/img/small-icons/logs.svg')}}" alt="">--}}
{{--                            </div>--}}
{{--                            <div class="textSettings">--}}
{{--                                Logs--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="cartSettings">--}}
{{--                        <div class="cartGroup">--}}
{{--                            <div class="imgSettings">--}}
{{--                                <img src="{{asset('admin-assets/img/small-icons/notification.svg')}}" alt="">--}}
{{--                            </div>--}}
{{--                            <div class="textSettings">--}}
{{--                                NOTIFICATIONS--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="cartSettings">--}}
{{--                        <div class="cartGroup">--}}
{{--                            <div class="imgSettings">--}}
{{--                                <img src="{{asset('admin-assets/img/small-icons/currencies.svg')}}" alt="">--}}
{{--                            </div>--}}
{{--                            <div class="textSettings">--}}
{{--                                CURRENCIES--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                <div class="buttonSettings">--}}
{{--                    <button class="btn">--}}
{{--                        <img src="{{asset('admin-assets/img/small-icons/sistemap.svg')}}" alt="">--}}
{{--                        GENERATE SITEMAP1--}}
{{--                    </button>--}}
{{--                </div>--}}

            </div>
        </div>
    </div>
@stop


@include('admin.footer')