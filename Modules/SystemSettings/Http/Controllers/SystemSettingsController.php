<?php

namespace Modules\SystemSettings\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class SystemSettingsController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->data = [];
        $this->moduleName = 'systemsettings';
        $this->path = "{$this->moduleName}::systemsettings";
    }

    public function index(){

        $currComponent = parent::currComponent();

        $this->data = [
            'currComponent' => $currComponent,
            'childrenComponent' => $currComponent->children
        ];

        return view($this->path.'.list', $this->data);
   }
}
