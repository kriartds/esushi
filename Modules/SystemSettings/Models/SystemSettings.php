<?php

namespace Modules\SystemSettings\Models;

use Illuminate\Database\Eloquent\Model;

class SystemSettings extends Model
{
    protected $table = 'system-settings';

    protected $fillable = [''];
}
