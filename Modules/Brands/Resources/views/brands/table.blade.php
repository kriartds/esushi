@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="align-center">{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.name")}}</th>
            <th class="left">Brand's Products</th>
            @if($permissions->active)
                <th class="align-center">{{__("{$moduleName}::e.active_table")}}</th>
            @endif
            @if($permissions->delete)
                <th class="checkbox-all align-center">
                    <div>Select All</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>
                <td class="title prod-name left ">
                    <a @if($permissions->edit) href="{{adminUrl([$currComponent->slug, 'edit', $item->id])}}" @endif>{{$item->name}}</a>
                </td>
                <td class="title categories left">
                    @if($item->countProducts())
                        <p><a href="{{adminUrl(['products']).'?brands=['.$item->id.']'}}">{{$item->countProducts()}} Products</a><p>
                    @else
                                -
                    @endif
                </td>
{{--                @if($permissions->edit)--}}
{{--                    <td class="td-link">--}}
{{--                        <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id])}}">{{__("{$moduleName}::e.edit_table")}}</a>--}}
{{--                    </td>--}}
{{--                @endif--}}
                @if($permissions->active)
                    <td class="status">
                        <span class="activate-item {{$item->active ? 'active' : ''}}"
                              data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                              data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>
                    </td>
                @endif
                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}">
                        <label for="{{$item->id}}">
                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
