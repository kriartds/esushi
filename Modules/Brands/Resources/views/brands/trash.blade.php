@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'trash'], 'item' => null, 'currentPage'=> 'trash'])


        <div class="table-block">
            @if(!$items->isEmpty())
                <table class="table">
                    <thead>
                    <tr>
                        <th class="align-center">{{__("{$moduleName}::e.id_table")}}</th>
                        <th class="left">{{__("{$moduleName}::e.name")}}</th>

                    @if($permissions->delete)
                            <th class="checkbox-all align-center" >
                                <div>Select All</div>
                            </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td class="id">
                                <p>{{$item->id}}</p> 
                            </td>
                            <td class="title left">
                                <p>{{$item->name}}</p>
                            </td>

                            <td>
                                <p>{{$item->slug}}</p>
                            </td>

                            @if($permissions->delete)
                                <td class="checkbox-items">
                                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                           name="checkbox_items[{{$item->id}}]"
                                           value="{{$item->id}}">
                                    <label for="{{$item->id}}">

                                    </label>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')