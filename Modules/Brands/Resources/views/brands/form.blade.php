<form class="brands" method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}" id="{{!is_null($item) ? 'edit' : 'create'}}-form"
      enctype="multipart/form-data">
    <div class="container">
        <div class="row">
            <div class="col middle">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__("{$moduleName}::e.name")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$item->name}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="field-row">
            @include('admin.templates.uploadFile', [
                'item' => @$item,
                'options' => [
                    'data-component-id' => $currComponent->id,
                    'data-types' => 'image'
                ]
            ])
        </div>
    </div>

    <div class="container middle transparent-bg">
        <div class="row">
            <div class="col flex-1">
                <div class="field-row inline  white-bg">
                    <div class="label-wrap">
                        <label for="show_on_main">Show on main</label>
                    </div>
                    <div class="checkbox-switcher">
                        <input type="checkbox" name="show_on_main" id="show_on_main" {{@$item->show_on_main ? 'checked' : ''}}>
                        <label for="show_on_main"></label>
                    </div>
                </div>
            </div>
            <div class="col flex-1"></div>
            <div class="col flex-1"></div>
        </div>
    </div>

    

    <button class="button blue submit-form-btn" data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
</form>

