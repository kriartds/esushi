<?php

namespace Modules\Brands\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Brands\Http\Requests\SaveBrandsRequest;
use Modules\Brands\Models\Brands;

class BrandsController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'brands';
        $this->path = "{$this->moduleName}::brands";
    }

    public function index()
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = Brands::paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.list", $response);

    }

    public function create()
    {

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id)
    {

        $item = Brands::findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = Brands::onlyTrashed()->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(SaveBrandsRequest $request, $id)
    {

        $currComponent = parent::currComponent();

        $item = Brands::find($id);

        if (is_null($item)) {
            $item = new Brands();

            $item->active = 1;
        }

        $item->name = $request->get('name', null);

        $item->save();

        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id])
            ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                Brands::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = Brands::whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = Brands::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->files()->delete();
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = Brands::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                    }
                }
                break;

            default:

                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function activateItem(Request $request)
    {

        $currComponent = parent::currComponent();
        $item = Brands::findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->id]);
            helpers()->logActions($currComponent, $item, @$item->id, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->id]);
            helpers()->logActions($currComponent, $item, @$item->id, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

}
