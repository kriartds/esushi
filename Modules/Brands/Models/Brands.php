<?php

namespace Modules\Brands\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FilesRelation;

class Brands extends Model
{

    use SoftDeletes;
    use FilesRelation;

    protected $table = 'brands';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'active',
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'brands';

    }

    public function products()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsId', 'brand_id', 'id');
    }

    public function countProducts()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsId', 'brand_id', 'id')->count();
    }
}
