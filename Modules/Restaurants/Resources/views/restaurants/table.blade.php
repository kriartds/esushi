@if(!$items->isEmpty())
    <div class="container-pages">
        <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
            <thead>
            <tr>
                <th class="id-center">{{__("{$moduleName}::e.id_table")}}</th>
                <th>{{__("{$moduleName}::e.title_table")}}</th>
                <th>IikoId</th>
                <th>Address</th>
                <th>Opening Hours</th>
                <th class="align-center">Languages</th>
                <th class="align-center">Status</th>
                <th class="checkbox-all align-center">
                    <div>Check all</div>
                </th>
            </tr>
            </thead>

            <tbody>
            @foreach($items as $item)
                <tr id="{{$item->id}}">
                    <td class="p-center">
                        <p>{{$item->id}}</p>
                    </td>
                    <td class="prod-name left">
                        <a class="{{@$isTrash ? 'trash' : ''}}"
                           @if(!@$isTrash) href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, DEF_LANG_ID]) . @$requestQueryString}}"
                           @endif
                           title="{{@$item->globalName->name}}">
                            {{@$item->globalName->name}}
                        </a>
                    </td>

                    <td class="width-row">
                        <span>{{$item->iiko_id}}</span>
                    </td>
                    <td class="width-row">
                        <span>{{$item->globalName->address}}</span>
                    </td>
                    <td class="width-row">
                        <span>{!! nl2br($item->globalName->working_hour) !!}</span>
                    </td>
                    @if($permissions->edit)
                        <td class="td-link align-center">
                            @foreach($langList as $lang)
                                <a @if(!@$isTrash) href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $lang->id])}}" @endif {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                            @endforeach
                        </td>
                    @endif

                    @if($permissions->active)
                        <td class="status align-center">
                                <span class="activate-item {{$item->active ? 'active' : ''}}"
                                      data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                                      data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span></td>
                    @endif
                    @if($permissions->delete)
                        <td class="checkbox-items align-center">
                            <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                   name="checkbox_items[{{$item->id}}]"
                                   value="{{$item->id}}">
                            <label for="{{$item->id}}">
                            </label>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
            @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
                <tfoot>
                <tr>
                    <td colspan="10">
                        @include('admin.templates.pagination', ['pagination' => $items])
                    </td>
                </tr>
                </tfoot>
            @endif
        </table>
    </div>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
