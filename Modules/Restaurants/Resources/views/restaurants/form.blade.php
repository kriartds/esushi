<div class="create-new-pages-container">
    <form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
          id="{{!is_null($item) ? 'edit' : 'create'}}-form"
          enctype="multipart/form-data">
        <div class="background">
            <div class="top-container">
                <div class="field-row language">
                    <div class="label-wrap">
                        <label for="lang">{{__("{$moduleName}::e.lang")}}*</label>
                    </div>
                    <div class="field-wrap">
                        @if(!$langList->isEmpty())
                            <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                                @foreach($langList as $lang)
                                    <option value="{{$lang->id}}" {{ (is_null(@$langId) && $lang->id == LANG_ID ? 'selected' : $lang->id == @$langId ) ? 'selected' : ''}}>{{$lang->name ?: ''}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="iiko_terminal">{{__("{$moduleName}::e.iiko_terminal")}}*</label>
                    </div>
                    <div class="field-wrap">
                        @if($iiko_terminals->isNotEmpty())
                            <select autocomplete="off" name="iiko_terminal" id="iiko_terminal" class="select2 no-search">
                                <option>Select iiko terminal</option>
                                @foreach($iiko_terminals as $terminal)
                                    <option value="{{$terminal->id}}" {{ (@$item->iiko_terminal_id == $terminal->id) ? 'selected' : ''}}>{{$terminal->name ?: ''}} - {{$terminal->id}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__("{$moduleName}::e.title_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$item->itemByLang->name}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="working_hour">{{__("{$moduleName}::e.working_hour")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <textarea name="working_hour" id="working_hour">{{@$item->itemByLang->working_hour}}</textarea>
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="slug">{{__("{$moduleName}::e.slug_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="slug" id="slug" value="{{@$item->slug}}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="address">{{__("{$moduleName}::e.title_adress")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="address" id="address" value="{{@$item->itemByLang->address}}">
                    </div>
                </div>
            </div>
            <div class="upload-file">
                <div class="field-row">
                    @include('admin.templates.uploadFile', [
               'item' => @$item,
               'options' => [
                   'data-component-id' => $currComponent->id,
                   'data-types' => 'image'
               ]
           ])
                </div>
            </div>
            <div class="meta-container">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="delivery_operator">Operator livrare</label>
                    </div>
                    <div class="field-wrap">
                        <input name="delivery_operator" id="delivery_operator" value="{{@$item->delivery_operator}}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="restaurant_reservations">Rezervări restaurant</label>
                    </div>
                    <div class="field-wrap">
                        <input name="restaurant_reservations" id="restaurant_reservations" value="{{@$item->restaurant_reservations}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="email">Email</label>
                    </div>
                    <div class="field-wrap">
                        <input name="email" id="email" value="{{@$item->email}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="meta_title">{{__("{$moduleName}::e.meta_title_page")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="meta_title" id="meta_title" value="{{@$item->itemByLang->meta_title}}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="meta_keywords">{{__("{$moduleName}::e.meta_keywords_page")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="meta_keywords" id="meta_keywords" value="{{@$item->itemByLang->meta_keywords}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="meta_description">{{__("{$moduleName}::e.meta_description_page")}}</label>
                    </div>
                    <div class="field-wrap">
            <textarea name="meta_description"
                      id="meta_description">{!! @$item->itemByLang->meta_description !!}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="buttonSetings">
            <button class="btn submit-form-btn"
                    data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
        </div>
    </form>
</div>
