<?php

return [

    'element_is_active' => 'The element :name is active!',
    'element_is_inactive' => 'The element :name is inactive!',

    'slug_table' => 'Slug',
    'title_table' => 'Title',
    'edit_table' => 'Edit',
    'active_table' => 'Status ',
    'delete_table' => 'Delete',
    'reestablish_table' => 'Reestablish',
    'id_table' => 'ID',
    'list_is_empty' => 'List is empty!',
    'save_it' => 'Save',
    'lang' => 'Lang',
    'description' => 'Description',
    'meta_title_page' => 'Meta title',
    'meta_keywords_page' => 'Meta keywords',
    'meta_description_page' => 'Meta description',

    'cant_delete_main_page' => "You can't delete main page!",
    'templates' => 'Templates',
    'default_templates' => 'Default template',


];