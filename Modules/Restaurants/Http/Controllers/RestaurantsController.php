<?php

namespace Modules\Restaurants\Http\Controllers;


use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\Models\Languages;
use Modules\Iiko\Models\IikoTerminal;
use Modules\Restaurants\Http\Requests\RestaurantRequest;
use Modules\Restaurants\Models\RestaurantsId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RestaurantsController extends DefaultController
{

    private $moduleName;
    private $path;
    private $frontPath;

    public function __construct()
    {
        $this->moduleName = 'restaurants';
        $this->path = "{$this->moduleName}::restaurants";
        $this->frontPath = 'front.restaurants';
    }

    public function index()
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = RestaurantsId::with(['globalName', 'items'])->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName
        ];

        return view("{$this->path}.list", $response);
    }

    public function create()
    {
        $iiko_terminals = IikoTerminal::where('active', 1)->get();
        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'iiko_terminals' => $iiko_terminals,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id, $langId)
    {
        $item = RestaurantsId::findOrFail($id);

        $iiko_terminals = IikoTerminal::where('active', 1)->get();
        
        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'langId' => $langId,
            'iiko_terminals' => $iiko_terminals,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = RestaurantsId::onlyTrashed()->with(['globalName'])->get();
        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(RestaurantRequest $request, $id, $langId)
    {
        $currComponent = parent::currComponent();
        if (is_null($id) && is_null($langId)) {
            $lang = Languages::where('active', 1)
                ->find($request->get('lang'));

            if (is_null($lang))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Lang not exist!'],
                        'type' => 'warning'
                    ]
                ]);

            $langId = $lang->id;
        }


        $item = RestaurantsId::find($id);
        if (is_null($item)) {
            $item = new RestaurantsId();

            $item->active = 1;
        }
        $item->slug = $request->get('slug', null);
        $item->restaurant_reservations = $request->get('restaurant_reservations', null);
        $item->delivery_operator = $request->get('delivery_operator', null);
        $item->email = $request->get('email', null);

        if($request->get('iiko_terminal')){
            $iiko_terminal = IikoTerminal::find($request->get('iiko_terminal'));
            if(!is_null($iiko_terminal)){
                $item->iiko_id = $iiko_terminal->organization_id;
                $item->iiko_terminal_id = $iiko_terminal->id;
            }
        }

        $item->save();
        $item->itemByLang()->updateOrCreate([
            'restaurant_id' => $item->id,
            'lang_id' => $langId
        ], [
            'name' => $request->get('name', null),
            'address' => $request->get('address', null),
            'working_hour' => $request->get('working_hour', null),
            'meta_title' => $request->get('meta_title', null),
            'meta_keywords' => $request->get('meta_keywords', null),
            'meta_description' => $request->get('meta_description', null)
        ]);

        updateMenuItems($currComponent, $item, $langId);
        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->globalName->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId])
            ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                RestaurantsId::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = RestaurantsId::whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-to-trash');
                        updateMenuItems($currComponent, $item, null, 'destroy', $request->get('event'));
                    }
                }
                break;
            case 'delete-from-trash':
                $items = RestaurantsId::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->files()->delete();
                        $item->items()->delete();
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-from-trash');

                    }
                }
                break;
            case 'restore-from-trash':
                $items = RestaurantsId::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'restored-from-trash');
                    }
                }
                break;
            default:
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function activateItem(Request $request)
    {
        $currComponent = parent::currComponent();
        $item = RestaurantsId::findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'activate');
        }

        $item->update(['active' => $status]);

        updateMenuItems($currComponent, $item, null, 'activate');

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function widgetsCountItems()
    {
        return RestaurantsId::count();
    }

//    Methods for menu
    public function getItems()
    {
        $items = RestaurantsId::where('active', 1)
            ->with(['globalName'])
            ->get();

        return $items;
    }

    public function getItem($id)
    {
        $item = RestaurantsId::where('active', 1)
            ->with(['globalName', 'items'])
            ->find($id);

        $response = [];

        if (!is_null($item))
            $response = (object)[
                'id' => $item->id,
                'name' => !is_null(@$item->globalName->name) ? $item->globalName->name : null,
                'slug' => $item->slug,
                'allInfo' => $item
            ];

        return $response;
    }
//    Methods for menu
}