<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit25e4a5a5e7b5f0fbf95c7ceda7016606
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'Modules\\Restaurants\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Modules\\Restaurants\\' => 
        array (
            0 => __DIR__ . '/../..' . '/',
        ),
    );

    public static $classMap = array (
        'Modules\\Restaurants\\Http\\Controllers\\RestaurantsController' => __DIR__ . '/../..' . '/Http/Controllers/RestaurantsController.php',
        'Modules\\Restaurants\\Http\\Requests\\RestaurantRequest' => __DIR__ . '/../..' . '/Http/Requests/RestaurantRequest.php',
        'Modules\\Restaurants\\Models\\Restaurants' => __DIR__ . '/../..' . '/Models/Restaurants.php',
        'Modules\\Restaurants\\Models\\RestaurantsId' => __DIR__ . '/../..' . '/Models/RestaurantsId.php',
        'Modules\\Restaurants\\Providers\\RestaurantsServiceProvider' => __DIR__ . '/../..' . '/Providers/RestaurantsServiceProvider.php',
        'Modules\\Restaurants\\Providers\\RouteServiceProvider' => __DIR__ . '/../..' . '/Providers/RouteServiceProvider.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit25e4a5a5e7b5f0fbf95c7ceda7016606::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit25e4a5a5e7b5f0fbf95c7ceda7016606::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit25e4a5a5e7b5f0fbf95c7ceda7016606::$classMap;

        }, null, ClassLoader::class);
    }
}
