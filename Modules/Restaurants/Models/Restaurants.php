<?php

namespace Modules\Restaurants\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurants extends Model
{
    protected $table = 'restaurants';

    protected $fillable = [
        'restaurant_id',
        'lang_id',
        'name',
        'working_hour',
        'address',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

    public function setMetaDescriptionAttribute($value)
    {
        $this->attributes['meta_description'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

}
