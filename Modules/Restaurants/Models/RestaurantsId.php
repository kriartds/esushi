<?php

namespace Modules\Restaurants\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantsId extends Model
{

    use SoftDeletes;
    use FilesRelation;

    protected $table = 'restaurant_id';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'slug',
        'template',
        'active',
        'iiko_id',
        'iiko_terminal_id',
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'restaurant-component';
        self::$globalLangId = request()->segment(6, null);

    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function globalName()
    {
        return $this->hasOne(Restaurants::class, 'restaurant_id', 'id')->whereIn('restaurants.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(restaurants.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('Modules\Restaurants\Models\Restaurants', 'restaurant_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('Modules\Restaurants\Models\Restaurants', 'restaurant_id', 'id');
    }

}
