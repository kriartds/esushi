<?php

namespace Modules\Employees\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\Repositories\LanguagesRepository;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use Modules\Employees\Http\Requests\EmployeesRequest;
use Modules\Employees\Models\Employees;

class EmployeesController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'employees';
        $this->path = "{$this->moduleName}::employees";
    }

    public function index()
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', $this->globalSettings());

        $items = Employees::orderBy('position')
            ->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.list", $response);

    }

    public function create()
    {

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id, $langId)
    {

        $item = Employees::findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'langId' => $langId,
            'item' => $item,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = Employees::onlyTrashed()->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(EmployeesRequest $request, $id, $langId)
    {

        $currComponent = $this->currComponent();

        $rules = [
            'name' => 'required',
            'job' => 'required',
        ];


        $item = Validator::make($request->all(), $rules);

        if ($item->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $item->messages(),
                    'type' => 'error'
                ],
            ]);

        $item = Employees::find($id);

        if (is_null($item)) {
            $item = new Employees();

            $nextPosition = Helpers::getNextItemPosition($item);
            $item->position = $nextPosition;
            $item->active = 1;
        }

        $item->name = $request->input('name');
        $item->job = $request->input('job');

        $item->save();

        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions($currComponent, $item, @$item->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId])
            ]);
    }

    public function actionsCheckbox(Request $request)
    {

        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                Employees::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = Employees::whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = Employees::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->files()->delete();
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = Employees::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                    }
                }
                break;
            default:
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function updatePosition(Request $request)
    {

        $order = $request->get('order');

        if (!is_array($order))
            return response()->json([
                'status' => false
            ]);

        $itemsPositions = [];
        foreach ($order as $id) {
            $item = Employees::find($id);
            $itemsPositions[] = @$item->position;
        }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($order as $key => $id) {
            Employees::where('id', $id)->update(['position' => $itemsPositions[$key]]);
        }

        return response()->json([
            'status' => true
        ]);
    }

    public function activateItem(Request $request)
    {

        $currComponent = $this->currComponent();
        $item = Employees::findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->name]);
            helpers()->logActions($currComponent, $item, @$item->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->name]);
            helpers()->logActions($currComponent, $item, @$item->name, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }
}
