<?php

namespace Modules\Employees\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employees extends Model
{
    use SoftDeletes;
    use FilesRelation;

    protected $fillable = [
        'id',
        'name',
        'job',
        'position',
        'active'
    ];

    protected $dates = ['deleted_at'];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        self::$component = 'employees';

    }
}
