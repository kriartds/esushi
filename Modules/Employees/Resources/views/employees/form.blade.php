<form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form"
      enctype="multipart/form-data">
    <div class="background">
        <div class="top-container smallContainer">


            <div class="field-row">
                <div class="label-wrap">
                    <label for="name">Name*</label>
                </div>
                <div class="field-wrap">
                    <input name="name" id="name" value="{{ @$item->name }}">
                </div>
            </div>

            <div class="field-row">
                <div class="label-wrap">
                    <label for="job">{{__("{$moduleName}::e.position")}}*</label>
                </div>
                <div class="field-wrap">
                    <input name="job" id="job" value="{{ @$item->job }}">
                </div>
            </div>

            <div class="field-row selectImg">
                @include('admin.templates.uploadFile', [
                    'item' => @$item,
                    'options' => [
                        'data-component-id' => $currComponent->id,
                        'data-types' => 'image'
                    ]
                ])
            </div>
        </div>
    </div>
    <div class="m-left">
        <button class="btn medium submit-form-btn"
                data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
    </div>
</form>

