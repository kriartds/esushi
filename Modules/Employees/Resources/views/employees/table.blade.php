@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th>{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.photo")}}</th>
            <th class="left">{{__("{$moduleName}::e.name")}}</th>
            <th class="left">{{__("{$moduleName}::e.job")}}</th>
            <th class="align-center">{{__("{$moduleName}::e.position")}}</th>

            @if($permissions->active)
                <th>{{__("{$moduleName}::e.active_table")}}</th>
            @endif
            <th class="checkbox-all align-center" data-event="{{@$isTrash ? 'from' : 'to'}}-trash">
                <div>Select All</div>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>

                <td class="medium left img-small-round">
                    <img src="{{@$item->firstFile->small}}" alt="">
                </td>

                <td class="medium left prod-name">
                    <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id])}}">{{$item->name}}</a>
                </td>

                <td class="medium left prod-name">
                    {{$item->job}}
                </td>

                <td class="position">
                    <img src="{{asset('admin-assets/img/small-icons/position.svg')}}" alt="">
                </td>


                @if($permissions->active)
                    <td class="status">
                        <span class="activate-item {{$item->active ? 'active' : ''}}"
                              data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                              data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>
                    </td>
                @endif
                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}"

                        >
                        <label for="{{$item->id}}">
                        </label>
                    </td>
                @endif

            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
