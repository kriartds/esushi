<?php
return [

    'element_is_active' => 'The element :name is active!',
    'element_is_inactive' => 'The element :name is inactive!',

    'title_table' => 'Title',
    'slug_table' => 'Slug',
    'edit_table' => 'Edit',
    'active_table' => 'Status ',
    'delete_table' => 'Delete',
    'id_table' => 'ID',
    'photo' => 'Photo',
    'name' => 'Name',
    'job' => 'Job',
    'list_is_empty' => 'List is empty!',
    'save_it' => 'Save',
    'position' => 'Position',

];