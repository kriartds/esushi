<?php

namespace Modules\SushiPartyOrder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SushiPartyOrder extends Model
{
    use SoftDeletes;

    protected $table = 'sushi_party_order';

    protected $fillable = [
        'name',
        'phone',
        'email',
        'number_of_people',
        'time',
        'restaurant',
        'message',
        'status',
        'regions',
    ];
}
