<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite36a5919a04939f32f5f4898a41001f0
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'Modules\\Reservation\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Modules\\Reservation\\' => 
        array (
            0 => __DIR__ . '/../..' . '/',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite36a5919a04939f32f5f4898a41001f0::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite36a5919a04939f32f5f4898a41001f0::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
