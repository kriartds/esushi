<?php

namespace Modules\SushiPartyOrder\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Pages\Models\SushiPartyBlockId;
use Modules\SushiPartyOrder\Models\SushiPartyOrder;


class SushiPartyOrderController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'sushipartyorder';
        $this->path = "{$this->moduleName}::sushipartyorder";
    }

    public function index(Request $request)
    {
        $ourSushiParty = SushiPartyBlockId::with('globalName')->where('active',1)->get();
        $response = $this->filter($request);

        $response['path'] = $this->path;
        $response['ourSushiParty'] = $ourSushiParty;

        return view("{$this->path}.list", $response);
    }

    public function filter(Request $request, $onlyOrders = false)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $filterParams = array_filter($request->except('page'));

        if (!$request->ajax()) {
            $newFiltersParams = [];

            if (!empty($filterParams) && count($filterParams) > 0) {

                foreach ($filterParams as $key => $one_filter_elem) {
                    $newFiltersParams[$key] = $one_filter_elem;
                    if(strpos($one_filter_elem, '[') !== false || strpos($one_filter_elem, ']') !== false) {
                        $newFiltersParams[$key] = explode(',', substr($filterParams[$key], 1, -1));
                    }
                }
            }

            $filterParams = $newFiltersParams;
        }

        $pushUrl = '';

        if (!empty($filterParams)) {
            foreach ($filterParams as $key => $one_filter_el) {

                if (is_array($one_filter_el)) {
                    $pushUrlArr = '';
                    foreach ($one_filter_el as $k => $filter_el) {
                        $pushUrlArr .= $filter_el . ',';
                    }

                    $pushUrl .= $key . '=[' . strip_tags(substr($pushUrlArr, 0, -1)) . ']&';
                } else {
                    $pushUrl .= $key . '=' . strip_tags(str_replace('[]', '', $one_filter_el)) . '&';
                }
            }

            $pushUrl = '?' . substr($pushUrl, 0, -1);
        }

        $items = SushiPartyOrder::where(function ($q) use ($filterParams, $request) {
            if (array_key_exists('status', $filterParams) && is_array($filterParams['status'])){
                $q->where(function ($q) use ($filterParams) {
                    $q->whereIn('status', $filterParams['status']);
                });
            }
            if (array_key_exists('name', $filterParams) && !is_array($filterParams['name']))
                $q->where('name','like', "%{$filterParams['name']}%");

            if (array_key_exists('phone', $filterParams) && !is_array($filterParams['phone']))
                $q->where('phone','like', "%{$filterParams['phone']}%");

            if (array_key_exists('reservation_id', $filterParams) && !is_array($filterParams['reservation_id']))
                $q->where('id', $filterParams['reservation_id']);

            if (array_key_exists('client_email', $filterParams) && !is_array($filterParams['client_email']))
                $q->where('email', $filterParams['client_email']);


            if (array_key_exists('start_date', $filterParams) && !array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']))
                $q->whereDate('time', '>=', date('Y-m-d', strtotime($filterParams['start_date'])));
            elseif (!array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['end_date']))
                $q->whereDate('time', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));
            elseif (array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']) && !is_array($filterParams['end_date']))
                $q->whereDate('time', '>=', date('Y-m-d', strtotime($filterParams['start_date'])))->whereDate('time', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));

        })
            ->orderBy('id', 'desc')
            ->paginate($perPage);

        $items->setPath(url(LANG, ['admin', parent::currComponent()->slug]) . $pushUrl);

        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
            }

            return response()->json($response);
        }

        return $response;

    }

    public function create()
    {

        $ourSushiParty = SushiPartyBlockId::with('globalName')->where('active',1)->get();
        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'ourSushiParty' => $ourSushiParty,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id)
    {
        $item = SushiPartyOrder::findOrFail($id);
        $ourSushiParty = SushiPartyBlockId::with('globalName')->where('active',1)->get();

        if ($item->status && !$item->seen)
            $item->update(['seen' => 1]);

        $unreadNotification = auth()->guard('admin')->user()->unreadNotifications->where('data.order_id', $item->id)->first();

        if ($unreadNotification)
            $unreadNotification->markAsRead();

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'ourSushiParty' => $ourSushiParty,
        ];


        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = SushiPartyOrder::onlyTrashed()->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(Request $request, $id)
    {
        $rules = [
            'status' => 'required|in:confirmed,unconfirmed,canceled',
            'time' => 'required|date|date_format:d-m-Y H:i',
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'message' => 'nullable|max:1024',
            'number_of_people' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->errors(),
                    'type' => 'error'
                ],
            ]);
        }

        if ($id){
            $party = SushiPartyOrder::findOrFail($id);
        }else{
            $party = new SushiPartyOrder();
        }

        $party->name = $request->input('name');
        $party->phone = $request->input('phone');
        $party->email = $request->input('email');
        $party->party = $request->input('party');
        $party->regions = $request->input('regions');
        $party->message = $request->input('message');
        $party->number_of_people = $request->input('number_of_people');
        $party->status = $request->input('status');
        $party->time = carbon($request->input('time'));
        $party->save();

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':

                SushiPartyOrder::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = SushiPartyOrder::whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = SushiPartyOrder::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = SushiPartyOrder::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                    }
                }
                break;
            default:

                break;
        }
        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function destroy(Request $request)
    {

        $deletedItemsId = substr($request->get('id'), 1, -1);

        if (empty($deletedItemsId))
            return response()->json([
                'status' => false
            ]);

        if ($request->get('event') != 'to-trash' && $request->get('event') != 'from-trash' && $request->get('event') != 'restore')
            return response()->json([
                'status' => false
            ]);

        $currComponent = parent::currComponent();
        $deletedItemsIds = explode(',', $deletedItemsId);

        if ($request->get('event') == 'to-trash') {
            $items = SushiPartyOrder::whereIn('id', $deletedItemsIds)->get();
        } else {
            $items = SushiPartyOrder::onlyTrashed()->whereIn('id', $deletedItemsIds)->get();
        }

        $cartMessage = $responseMsg = '';

        if (!$items->isEmpty()) {
            foreach ($items as $item) {

                $cartMessage .= $item->id . ', ';

                if ($request->get('event') == 'to-trash' && !$item->trashed()) {
                    $item->delete();
                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' added to trash' : '';
                    helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                } elseif ($request->get('event') == 'from-trash') {
                    $item->forceDelete();
                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' remove from trash' : '';
                    helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                } elseif ($request->get('event') == 'restore') {
                    $item->restore();
                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' restored from trash' : '';
                    helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                }

            }

            return response()->json([
                'status' => true,
                'cart_messages' => $responseMsg,
                'items' => $deletedItemsIds
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }

}
