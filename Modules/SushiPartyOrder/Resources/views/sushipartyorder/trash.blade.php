@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'trash'], 'item' => null, 'currentPage'=> 'trash'])
        <div class="table-block trash">
            @if(!$items->isEmpty())
                <table class="table">
                    <thead>
                    <tr>
                        <th class="align-center">{{__("{$moduleName}::e.party_id")}}</th>
                        <th class="align-center">{{__("{$moduleName}::e.name")}}</th>
                        <th class="align-center">{{__("{$moduleName}::e.phone")}}</th>
                        <th class="align-center">{{__("{$moduleName}::e.status")}}</th>
                        <th class="align-center">{{__("{$moduleName}::e.date")}}</th>
                        @if($permissions->delete)
                            <th class="checkbox-all align-center" >
                                <div>Check all</div>
                            </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td class="id">
                                <p>{{$item->id}}</p>
                            </td>
                            <td class="title center"><a class="trash" href="">{{@$item->name ?: '-'}}</a></td>
                            <td class="center">
                                <p>{{@$item->phone}}</p>
                            </td>


                            <td class="order-status">
                                <div class="trash {{$item->status}}">{{ucfirst($item->status)}}</div> <!-- Class names for this div: checkout, canceled, processing, approved, finished, failed -->
                            </td>
                            <td class="align-center">{{strtotime($item->time) ? date('d-m-Y H:i', strtotime($item->time)) : '-'}}</td>

                            @if($permissions->delete)
                                <td class="checkbox-items">
                                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                           name="checkbox_items[{{$item->id}}]"
                                           value="{{$item->id}}">
                                    <label for="{{$item->id}}"></label>
                                </td>
                            @endif

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')