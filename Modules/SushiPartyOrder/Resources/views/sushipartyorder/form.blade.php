<form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form" enctype="multipart/form-data" class="orders-form">
    <div class="row">
        <div class="col">
            <div class="order-description-block">
                @if(@$item)
                    <div class="orders-header">
                        <div class="row">
                            <div class="title">
                                <span>{{__("{$moduleName}::e.reservation")}} #</span><span
                                        id="generated-reservation-id">{{@$item->id}}</span>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="field-row order-info-row">
                    <div class="field-wrap field-wrap-children">
                        <div class="container">
                            <div class="row">
                                <div class="col extra-large">
                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="status">{{__("{$moduleName}::e.status")}}</label>
                                        </div>
                                        <div class="field-wrap">
                                            <select name="status" id="status" class="select2 no-search">
                                                <option value="confirmed" {{@$item->status == 'confirmed' ? 'selected' : ''}}>
                                                    {{__("{$moduleName}::e.confirmed")}}
                                                </option>
                                                <option value="unconfirmed" {{@$item->status == 'unconfirmed' ? 'selected' : ''}}>
                                                    {{__("{$moduleName}::e.unconfirmed")}}
                                                </option>
                                                <option value="canceled" {{@$item->status == 'canceled' ? 'selected' : ''}}>
                                                    {{__("{$moduleName}::e.canceled")}}
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col extra-large">
                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="date">{{__("{$moduleName}::e.date")}}</label>
                                        </div>
                                        <div class="field-wrap">
                                            <input id="date" class="datetimepicker time" name="time"
                                                   value="{{strtotime(@$item->time) ? date('d-m-Y H:i', strtotime($item->time)) : date('d-m-Y H:i')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hidden-content">
                                <div id="edit-client-info-block" class="">
                                    <div class="row">
                                        <div class="col extra-large ">
                                            <div class="field-row">
                                                <div class="label-wrap">
                                                    <label for="name">{{__("{$moduleName}::e.name")}}</label>
                                                </div>
                                                <div class="field-wrap">
                                                    <input id="name" name="name" value="{{@$item->name}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col extra-large">
                                            <div class="field-row">
                                                <div class="label-wrap">
                                                    <label for="number_of_people">{{__("{$moduleName}::e.number_of_people")}}</label>
                                                </div>
                                                <div class="field-wrap">
                                                    <input name="number_of_people" id="number_of_people" value="{{@$item->number_of_people}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col extra-large">
                                            <div class="field-row">
                                                <div class="label-wrap">
                                                    <label for="phone">{{__("{$moduleName}::e.phone")}}</label>
                                                </div>
                                                <div class="field-wrap">
                                                    <input name="phone" id="phone" value="{{@$item->phone}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col extra-large">
                                            <div class="field-row">
                                                <div class="label-wrap">
                                                    <label for="email">{{__("{$moduleName}::e.email")}}</label>
                                                </div>
                                                <div class="field-wrap">
                                                    <input name="email" id="email" value="{{@$item->email}}">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="message">{{__("{$moduleName}::e.party")}}</label>
                                        </div>

                                        <div class="field-wrap">
                                            <select class="select select2 no-search" name="party" id="">
                                                @foreach($ourSushiParty as $party)
                                                    <option @if($party->id == @$item->party ) selected @endif value="{{$party->id}}">{{$party->globalName->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="regions">{{__("{$moduleName}::e.regions")}}</label>
                                        </div>
                                        <input name="regions" id="regions" value="{{@$item->regions}}">

                                    </div>

                                    <div class="order-products-block" id="homeProductsBlock">
                                        @if(@$item->products)
                                            <?php $products = \Modules\Products\Models\ProductsItemsId::whereIn('id', json_decode($item->products))->get()?>
                                                @foreach($products as $product)
                                                <div class="order-products-block home-products-block">
                                                    <table id="order-products-table">
                                                        <tbody>
                                                        <tr data-order-product-id="{{$product->id}}" class="">
                                                            <td colspan="2" style="width: 95%">
                                                                <div class="product-info direction-column">
                                                                    <div class="product-name-img">
                                                                        <div class="product-id">
                                                                            <span class="tooltip" data-tippy="" data-original-title="Product ID">#{{$product->id}}</span>
                                                                        </div>
                                                                        <input type="hidden" hidden name="producstsIds[]" value="{{$product->id}}">
                                                                        <div class="product-img">
                                                                            <a href="{{asset(@$product->file->file->file)}}" data-fancybox="thumbnail-{{$product->id}}">
                                                                                <img src="{{asset(@$product->file->file->file)}}">
                                                                            </a>
                                                                        </div>
                                                                        <div class="product-name">
                                                                            <a href="{{customUrl(['products', @$product->slug])}}" target="_blank" class="view-product-on-site"></a>
                                                                            <a href="{{customUrl(['admin', 'products', 'edit', @$product->id, LANG_ID])}}" target="_blank">{{$product->globalname->name}}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </td>
                                                            <td>
{{--                                                                <div class="actions">--}}
{{--                                                                    <div class="destroy-home-product" data-added="true" data-url="{{customUrl(['admin', 'sushi-party', 'productdestroy'])}}"></div>--}}
{{--                                                                </div>--}}
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>


                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="message">{{__("{$moduleName}::e.message")}}</label>
                                        </div>
                                        <div class="field-wrap">
                                            <textarea name="message"
                                                      id="message">{{@$item->message}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-block">
        <div class="actions-block">
            <button class="button blue submit-form-btn no-margin"
                    data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}
            </button>
        </div>
    </div>
</form>
