@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'trash', 'edit'], 'item' => @$item, 'langId' => @$langId])

        <div class="form-block orders-form">

            @include("{$path}.form", ['item' => @$item, 'ourSushiParty' =>$ourSushiParty])
        </div>
    </div>

@stop

@include('admin.footer')