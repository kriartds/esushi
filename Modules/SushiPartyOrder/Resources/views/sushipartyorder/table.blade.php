@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="align-center">{{__("{$moduleName}::e.party_id")}}</th>
            <th class="align-center">{{__("{$moduleName}::e.name")}}</th>
            <th class="left"> Phone Number</th>
            <th class="align-center">{{__("{$moduleName}::e.number_of_people")}}</th>
            <th class="align-center">{{__("{$moduleName}::e.status")}}</th>
            <th class="align-center">{{__("{$moduleName}::e.date")}}</th>
            @if($permissions->delete)
                <th class="checkbox-all align-center">
                    <div>Check all</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}" @if(!$item->seen) class="new-order" @endif>
                <td class="id">
                    <div class="row-order-status fast"></div>
                    @if($permissions->edit)
                        <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id])}}">{{$item->id}}</a>
                    @else
                        <p>{{@$item->id}}</p>
                    @endif
                </td>
                <td class="center">
                    <p>{{@$item->name}}</p>
                </td>
                <td class="left">
                    <p>{{@$item->phone}}</p>
                </td>
                <td class="center">
                    <div>{{ucfirst($item->number_of_people)}}</div>
                </td>

                <td class="order-status">
                    <div class="{{$item->status}}">{{ucfirst($item->status)}}</div>
                </td>

                <td class="align-center">{{strtotime($item->time) ? date('d-m-Y H:i', strtotime($item->time)) : '-'}}</td>
                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}">
                        <label for="{{$item->id}}">
                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
