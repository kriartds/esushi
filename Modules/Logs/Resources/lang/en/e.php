<?php

return [

    'delete_table' => 'Delete',
    'reestablish_table' => 'Reestablish',
    'id_table' => 'ID',
    'list_is_empty' => 'List is empty!',
    'from_date' => 'From date',
    'to_date' => 'To date',
    'filter' => 'Filter',
    'reset' => 'Reset',
    'date' => 'Date',
    'email' => 'Email',

    'ip' => 'Client IP',
    'component' => 'Component',
    'components' => 'Components',
    'user_name' => 'User name',
    'user_group' => 'User group',
    'user_agent' => 'User agent',
    'event' => 'Event',
    'events' => 'Events',
    'view' => 'View',
    'message' => 'Message',

    'create' => 'Create',
    'edit' => 'Edit',
    'deleted-to-trash' => 'Deleted to trash',
    'deleted-from-trash' => 'Deleted from trash',
    'deleted' => 'Deleted',
    'restored-from-trash' => 'Restored from trash',
    'activate' => 'Activate',
    'inactivate' => 'Inactivate',
    'auth' => 'Auth',
];