@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th>{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.message")}}</th>
            <th class="left">{{__("{$moduleName}::e.ip")}}</th>
            <th>{{__("{$moduleName}::e.events")}}</th>
            <th>{{__("{$moduleName}::e.date")}}</th>
            <th></th>
            @if($permissions->delete)
                <th class="checkbox-all" >
                    <div>Select All</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>
                <td class="big left td-link">
                    <a href="{{adminUrl([$currComponent->slug, 'view', $item->id])}}">{!! @$item->message !!}</a>
                </td>
                <td class="left">{{$item->ip}}</td>
                <td class="small-icon">
                    @if(!is_null(@$item->eventIcon))
                        <img src="{{$item->eventIcon}}"
                             title="{{@$item ? __("{$moduleName}::e.{$item->events}") : '-'}}" alt="{{@$item->events}}"
                             class="tooltip">
                    @else
                        -
                    @endif
                </td>
                <td class="medium">{{$item->created_at}}</td>
                <td class="empty-td"></td>
                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}">
                        <label for="{{$item->id}}">

                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
