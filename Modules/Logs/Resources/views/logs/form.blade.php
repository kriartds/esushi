<form>
    <div class="background gray">
        <div class="medium-flex">
            <div class="field-row">
                <div class="label-wrap">
                    <label for="message" class="gray-bold">{{__("{$moduleName}::e.message")}}</label>
                </div>
                <div class="field-wrap white-bg bigInput">
                    {!! @$item->message !!}
                </div>
            </div>
            <div class="flex">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="ip" class="gray-bold">{{__("{$moduleName}::e.ip")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input id="ip" value="{{@$item->ip}}" readonly>
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="component" class="gray-bold">{{__("{$moduleName}::e.component")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input id="component" value="{{@$item->component->globalName->name}}" readonly>
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="user" class="gray-bold">{{__("{$moduleName}::e.user_name")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input id="user" value="{{@$item->user->name}}" readonly>
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="user-group" class="gray-bold">{{__("{$moduleName}::e.user_group")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input id="user-group" value="{{@$item->user->group->name}}" readonly>
                    </div>
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="user-agent" class="gray-bold">{{__("{$moduleName}::e.user_agent")}}</label>
                </div>
                <div class="field-wrap ">
                    <input id="user-agent" class="bigInput" value="{{@$item->user_agent}}" readonly>
                </div>
            </div>
            <div class="flex">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="event" class="gray-bold">{{__("{$moduleName}::e.event")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input id="event" value="{{@$item->events ? __("{$moduleName}::e.{$item->events}") : '-'}}"
                               readonly>
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="date" class="gray-bold">{{__("{$moduleName}::e.date")}}</label>
                    </div>
                    <div class="field-wrap ">
                        <input id="date" value="{{@$item->created_at}}" readonly>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

