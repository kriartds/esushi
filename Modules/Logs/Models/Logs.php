<?php

namespace Modules\Logs\Models;

use App\Models\Enums;
use Illuminate\Database\Eloquent\Model;

class Logs extends Model
{
    use Enums;

    protected $table = 'logs';

    protected $fillable = [
        'component_id',
        'admin_user_id',
        'item_id',
        'ip',
        'user_agent',
        'message',
        'events'
    ];

    protected $enumEvents = [
        'create',
        'edit',
        'deleted-to-trash',
        'deleted-from-trash',
        'deleted',
        'restored-from-trash',
        'activate',
        'inactivate',
        'auth',
        'show_on_main'
    ];


    public function setMessageAttribute($value)
    {
        $this->attributes['message'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

    public function getLogMessageAttribute()
    {
        return preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $this->message);
    }

    public function component()
    {
        return $this->hasOne('App\Models\ComponentsId', 'id', 'component_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\AdminUsers', 'id', 'admin_user_id');
    }

    public function getEventIconAttribute()
    {
        $file = "admin-assets/img/logs-icons/{$this->events}.svg";

        $response = asset("img/no-image.png");

        if (file_exists($file))
            $response = asset($file);


        return $response;

    }
}
