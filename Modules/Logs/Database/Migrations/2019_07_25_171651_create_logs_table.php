<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('component_id')->nullable();
            $table->unsignedInteger('admin_user_id')->nullable();
            $table->unsignedInteger('item_id')->nullable();
            $table->enum('events', ['create', 'edit', 'deleted-to-trash', 'deleted-from-trash', 'deleted', 'restored-from-trash', 'activate', 'inactivate', 'auth'])->nullable();
            $table->ipAddress('ip')->nullable();
            $table->string('user_agent', 1024)->nullable();
            $table->text('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
