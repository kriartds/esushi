<?php

namespace Modules\ShippingZones\Http\Controllers;


use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\Models\Languages;
use Modules\ShippingZones\Models\ShippingMethodsId;
use Modules\ShippingZones\Models\ShippingZones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ShippingZonesMethodsController extends DefaultController
{
    private $moduleName;
    private $path;
    private $shippingZoneId;

    public function __construct($shippingZoneId)
    {
        $this->moduleName = 'shippingzones';
        $this->path = "{$this->moduleName}::shipping-zones.methods";
        $this->shippingZoneId = $shippingZoneId;
    }

    public function index()
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = ShippingMethodsId::where('shipping_zone_id', $this->shippingZoneId)
            ->orderBy('position')
            ->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];
        return view("{$this->path}.list", $response);

    }

    public function create()
    {
        $shippingZone = ShippingZones::findOrFail($this->shippingZoneId);

        $shippingMethodsId = new ShippingMethodsId();
        $shippingMethodsTypesEnums = $shippingMethodsId->getEnum('method_type');
        $shippingMethodsFreeShippingRequiresEnums = $shippingMethodsId->getEnum('free_shipping_requires');

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'currComponent' => $this->currComponent(),
            'shippingZone' => $shippingZone,
            'shippingMethodsTypesEnums' => $shippingMethodsTypesEnums,
            'shippingMethodsFreeShippingRequiresEnums' => $shippingMethodsFreeShippingRequiresEnums
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id, $langId)
    {

        $item = ShippingMethodsId::where('shipping_zone_id', $this->shippingZoneId)->findOrFail($id);

        $shippingMethodsTypesEnums = $item->getEnum('method_type');
        $shippingMethodsFreeShippingRequiresEnums = $item->getEnum('free_shipping_requires');

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'langId' => $langId,
            'shippingMethodsTypesEnums' => $shippingMethodsTypesEnums,
            'shippingMethodsFreeShippingRequiresEnums' => $shippingMethodsFreeShippingRequiresEnums
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function save(Request $request, $id, $langId)
    {

        $shippingZone = ShippingZones::find($this->shippingZoneId);

        if (!$shippingZone)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => "Shipping zone not exist.",
                    'type' => 'error'
                ]
            ]);

        $currComponent = parent::currComponent();
        $methodType = $request->get('method_type', 'flat');
        $freeShippingRequire = $request->get('free_shipping_requires', 'no_action');

        $rulesMsg = [];

        $rules = [
            'name' => 'required',
            'method_type' => 'required|in:flat,free'
        ];

        if ($methodType == 'free') {
            $rules = array_merge($rules, [
                'free_shipping_requires' => 'required|in:no_action,free_coupon,min_order_amount'
            ]);

            if ($freeShippingRequire != 'no_action' && $freeShippingRequire != 'free_coupon')
                $rules = array_merge($rules, [
                    'min_order_amount' => 'required|numeric|min:0'
                ]);
        } else
            $rules = array_merge($rules, [
                'amount' => 'required|numeric|min:0'
            ]);

        $item = Validator::make($request->all(), $rules, $rulesMsg);

        if ($item->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $item->messages(),
                    'type' => 'error'
                ],
            ]);

        if (is_null($id) && is_null($langId)) {
            $lang = Languages::where('active', 1)
                ->find($request->get('lang'));

            if (is_null($lang))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Lang not exist!'],
                        'type' => 'warning'
                    ]
                ]);

            $langId = $lang->id;
        }

        $item = ShippingMethodsId::where('shipping_zone_id', $shippingZone->id)->find($id);

        if (is_null($item)) {
            $item = new ShippingMethodsId();

            $nextPosition = Helpers::getNextItemPosition($item);
            $item->position = $nextPosition;
            $item->active = 1;
        }

        $item->shipping_zone_id = $shippingZone->id;
        $item->method_type = $methodType;
        $item->free_shipping_requires = $freeShippingRequire;
        $item->amount = $request->get('amount', null);
        $item->min_order_amount = $request->get('min_order_amount', null);

        $item->save();

        $item->itemByLang()->updateOrCreate([
            'shipping_method_id' => $item->id,
            'lang_id' => $langId
        ], [
            'name' => $request->get('name', null)
        ]);

        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug]) . "?shipping_zone={$shippingZone->id}"
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->globalName->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId]) . "?shipping_zone={$shippingZone->id}"
            ]);
    }

   public function actionsCheckbox(Request $request)
    {
        $shippingMethods = ShippingMethodsId::find($this->shippingZoneId);

        if (!$shippingMethods)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => "Shipping method not exist.",
                    'type' => 'error'
                ]
            ]);

        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');


        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                ShippingMethodsId::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
            case 'delete-from-trash':
                $items = ShippingMethodsId::whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->items()->delete();
                        $item->delete();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted');
                    }
                }
                break;
            default:
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function activateItem(Request $request)
    {

        $currComponent = parent::currComponent();
        $item = ShippingMethodsId::where('shipping_zone_id', $this->shippingZoneId)->findOrFail($request->get('id', null));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function updatePosition(Request $request)
    {

        $shippingZone = ShippingZones::find($this->shippingZoneId);

        if (!$shippingZone)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => "Shipping zone not exist.",
                    'type' => 'error'
                ]
            ]);

        $order = $request->get('order');

        if (!is_array($order))
            return response()->json([
                'status' => false
            ]);

        $itemsPositions = [];
        foreach ($order as $id) {
            $item = ShippingMethodsId::where('shipping_zone_id', $this->shippingZoneId)->find($id);
            $itemsPositions[] = @$item->position;
        }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($order as $key => $id) {
            ShippingMethodsId::where('id', $id)->update(['position' => $itemsPositions[$key]]);
        }

        return response()->json([
            'status' => true
        ]);
    }
}