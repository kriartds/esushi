<?php

namespace Modules\ShippingZones\Http\Controllers;


use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
//use Modules\CountriesRegion\Models\Countries;
//use Modules\CountriesRegion\Models\Regions;
use App\IikoApi\Models\IikoCities;
use App\IikoApi\Models\IikoProducts;
use Illuminate\Validation\Rule;
use Modules\ShippingZones\Models\ShippingZones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
//use Modules\ShippingZones\Models\ShippingZonesCountries;
//use Modules\ShippingZones\Models\ShippingZonesRegions;

class ShippingZonesController extends DefaultController
{

    private $moduleName;
    private $path;
    private $shippingMethods;
    private $shippingZoneId;

    public function __construct()
    {

        $this->moduleName = 'shippingzones';
        $this->path = "{$this->moduleName}::shipping-zones";
        $this->shippingZoneId = request()->get('shipping_zone', null);
        //$this->shippingMethods = new ShippingZonesMethodsController($this->shippingZoneId);

    }

    public function index()
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        if ($this->shippingZoneId)
            return $this->shippingMethods->index();

        $items = ShippingZones::orderBy('is_default', 'desc')
                                ->orderBy('position')
                                ->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.list", $response);

    }

    public function create()
    {

        if ($this->shippingZoneId)
            return $this->shippingMethods->create();

        $iiko_cities = IikoCities::all();
        $iiko_services = IikoProducts::where('type', 'Service')->get();

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'currComponent' => $this->currComponent(),
            'iiko_cities' => $iiko_cities,
            'iiko_services' => $iiko_services,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id, $langId)
    {
        $item = ShippingZones::findOrFail($id);
        $iiko_cities = IikoCities::all();

        $iiko_services = IikoProducts::where('name', 'LIKE', '%livrare%')->get();

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'langId' => $langId,
            'iiko_cities' => $iiko_cities,
            'iiko_services' => $iiko_services,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        if ($this->shippingZoneId)
            abort(404);

        $items = ShippingZones::onlyTrashed()
            ->with(['countries', 'shippingRegions.region', 'shippingMethods', 'shippingMethods.globalName'])
            ->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(Request $request, $id, $langId)
    {
        $rulesMsg = [];
        $currComponent = parent::currComponent();

        $item = ShippingZones::find($id);
        $isDefault = @$item->is_default ?: 0;

        $rules = [
                'name' => 'required',
                'shipping_amount' => 'required',
            ];

        $validator = Validator::make($request->all(), $rules, $rulesMsg);

        if ($validator->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->messages(),
                    'type' => 'error'
                ],
            ]);

        //if (!$isDefault) {
            if (is_null($item)) {
                $item = new ShippingZones();

                $nextPosition = Helpers::getNextItemPosition($item);
                $item->position = $nextPosition;
                $item->active = 1;
            }

            $item->name = $request->get('name', null);
            $item->is_default = $isDefault;
            $item->free_shipping_amount = $request->get('free_shipping_amount', null);
            $item->shipping_amount = $request->get('shipping_amount', null);
            $item->iiko_id = $request->get('iiko_id', null);
            $item->iiko_service_id = $request->get('iiko_service_id', null);

            $item->save();
        //}

        //Create relation shipping zones ~ countries

        helpers()->logActions($currComponent, $item, @$item->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId])
            ]);
    }

    public function actionsCheckbox(Request $request)
    {
        if ($this->shippingZoneId)
            return $this->shippingMethods->actionsCheckbox($request);

        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                ShippingZones::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = ShippingZones::where('is_default', 0)->whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, @$item->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = ShippingZones::where('is_default', 0)->onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->shippingCountries()->delete();
                        $item->shippingRegions()->delete();
                        $item->shippingMethods->each(function ($q) {
                            $q->items()->delete();
                        });
                        $item->shippingMethods()->delete();
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, @$item->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = ShippingZones::where('is_default', 0)->onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions($currComponent, $item, @$item->name, 'restored-from-trash');
                    }
                }
                break;
            default:
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function activateItem(Request $request)
    {

        if ($this->shippingZoneId)
            return $this->shippingMethods->activateItem($request);

        $currComponent = parent::currComponent();
        $item = ShippingZones::findOrFail($request->get('id'));

        if ($item->is_default)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => 'You can not activate or inactivate default shipping zone',
                    'type' => 'warning',
                ]
            ]);

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->name]);
            helpers()->logActions($currComponent, $item, @$item->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->name]);
            helpers()->logActions($currComponent, $item, @$item->name, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function activateIsDefault(Request $request)
    {
        $currComponent = parent::currComponent();
        $item = ShippingZones::findOrFail($request->get('id'));

//        if ($item->is_default)
//            return response()->json([
//                'status' => false,
//                'msg' => [
//                    'e' => 'You can not activate or inactivate default shipping zone',
//                    'type' => 'warning',
//                ]
//            ]);

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->name]);
            helpers()->logActions($currComponent, $item, @$item->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->name]);
            helpers()->logActions($currComponent, $item, @$item->name, 'activate');
        }

        $item->update(['is_default' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function updatePosition(Request $request)
    {

        if ($this->shippingZoneId)
            return $this->shippingMethods->updatePosition($request);

        $order = $request->get('order');

        if (!is_array($order))
            return response()->json([
                'status' => false
            ]);

        $itemsPositions = [];
        foreach ($order as $id) {
            $item = ShippingZones::find($id);
            $itemsPositions[] = @$item->position;
        }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($order as $key => $id) {
            ShippingZones::where('id', $id)->update(['position' => $itemsPositions[$key]]);
        }

        return response()->json([
            'status' => true
        ]);
    }



}