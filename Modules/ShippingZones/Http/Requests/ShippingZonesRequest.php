<?php

namespace Modules\ShippingZones\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use Modules\ShippingZones\Models\ShippingZones;

class ShippingZonesRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $countriesIds = request()->get('countries', []);
        $regionsIds = request()->get('regions', []);

        $item = ShippingZones::find($this->id);
        $isDefault = @$item->is_default ?: 0;

        $countriesRules = Rule::exists('countries', 'id')->where(function ($q) use ($countriesIds) {
            $q->whereIn('id', $countriesIds);
            $q->where('active', 1);
        });

        $regionRules = Rule::exists('regions', 'id')->where(function ($q) use ($countriesIds, $regionsIds) {
            $q->whereIn('id', $regionsIds);
            $q->whereIn('country_id', $countriesIds);
            $q->where('active', 1);
        });

        if ($isDefault) {
            $rules = [
                'countries' => [
                    'nullable',
                    $countriesRules,
                ],
                'regions' => [
                    'nullable',
                    $regionRules,
                ]
            ];
        } else
            $rules = [
                'name' => 'required',
                'countries' => [
                    'required',
                    $countriesRules,
                ],
                'regions' => [
                    'nullable',
                    $regionRules,
                ]
            ];
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        $data = [
            'status' => false,
            'validator' => true,
            'msg' => [
                'e' => $validator->messages(),
                'type' => 'error'
            ],
        ];

        throw new HttpResponseException(response()->json($data));
    }
}
