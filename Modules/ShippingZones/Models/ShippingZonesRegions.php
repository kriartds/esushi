<?php

namespace Modules\ShippingZones\Models;

use Modules\CountriesRegion\Models\Countries;
use Modules\CountriesRegion\Models\Regions;
use Illuminate\Database\Eloquent\Model;

class ShippingZonesRegions extends Model
{

    protected $table = 'shipping_zones_regions';

    protected $fillable = [
        'shipping_zones_country_id',
        'region_id'
    ];

    public function region()
    {
        return $this->hasOne(Regions::class, 'id', 'region_id');
    }

    public function shippingZoneCountry()
    {
        return $this->hasOne(ShippingZonesCountries::class, 'id', 'shipping_zones_country_id');
    }

}
