<?php

namespace Modules\ShippingZones\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingMethods extends Model
{

    protected $table = 'shipping_methods';

    protected $fillable = [
        'shipping_method_id',
        'lang_id',
        'name',
    ];

}
