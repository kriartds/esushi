<?php

namespace Modules\ShippingZones\Models;

use App\Models\Enums;
use Illuminate\Database\Eloquent\Model;

class ShippingMethodsId extends Model
{
    use Enums;

    public static $globalLangId;

    protected $table = 'shipping_methods_id';

    protected $fillable = [
        'shipping_zone_id',
        'method_type',
        'free_shipping_requires',
        'amount',
        'min_order_amount',
        'active',
        'position',
    ];

    protected $enumMethodTypes = [
        'flat',
        'free',
    ];

    protected $enumFreeShippingRequires = [
        'no_action',
        'free_coupon',
        'min_order_amount'
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$globalLangId = request()->segment(6, null);

    }

    public function scopeAvailableMethods($q, $useFreeShippingCoupon = false)
    {
        return $q->where(function ($q) use ($useFreeShippingCoupon) {
            $q->where(function ($q) {
                $q->where('method_type', 'flat');
            });
            $q->orWhere(function ($q) use ($useFreeShippingCoupon) {
                $q->where('method_type', 'free');
                $q->where(function ($q) use ($useFreeShippingCoupon) {
                    $q->where(function ($q) {
                        $q->where('free_shipping_requires', 'min_order_amount');
                        $q->where('min_order_amount', '>', 0);
                        $q->whereNotNull('min_order_amount');
                    });
                    $q->orWhere('free_shipping_requires', ($useFreeShippingCoupon ? '=' : '!='), 'free_coupon');
                    $q->orWhere('free_shipping_requires', 'no_action');
                });
            });
        })->where('active', 1)->orderBy('position');
    }

    public function globalName()
    {
        return $this->hasOne(ShippingMethods::class, 'shipping_method_id', 'id')->whereIn('shipping_methods.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(shipping_methods.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne(ShippingMethods::class, 'shipping_method_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany(ShippingMethods::class, 'shipping_method_id', 'id');
    }

    public function shippingZone()
    {
        return $this->hasOne(ShippingZones::class, 'id', 'shipping_zone_id');
    }
}
