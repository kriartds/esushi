<?php

namespace Modules\ShippingZones\Models;

use Modules\CountriesRegion\Models\Countries;
use Illuminate\Database\Eloquent\Model;

class ShippingZonesMethods extends Model
{

    protected $table = 'shipping_zones_methods';

    protected $fillable = [
        'shipping_zone_id',
        'shipping_method_id'
    ];

    public function shippingZone()
    {
        return $this->hasOne(ShippingZones::class, 'shipping_zone_id', 'id');
    }

    public function shippingMethodId()
    {
        return $this->hasOne(ShippingMethodsId::class, 'shipping_method_id', 'id');
    }

}
