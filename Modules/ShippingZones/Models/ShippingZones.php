<?php

namespace Modules\ShippingZones\Models;

use App\IikoApi\Models\IikoStreets;
use Modules\CountriesRegion\Models\Countries;
use Modules\CountriesRegion\Models\Regions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingZones extends Model
{

    use SoftDeletes;

    protected $table = 'shipping_zones';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'active',
        'position',
        'is_default',
        'iiko_id',
        'iiko_service_id',
    ];

    public function getStreets(){
        return $this->hasMany(IikoStreets::class, 'shipping_zone_id', 'id');
    }
}
