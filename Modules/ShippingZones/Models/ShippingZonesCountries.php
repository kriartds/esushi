<?php

namespace Modules\ShippingZones\Models;

use Modules\CountriesRegion\Models\Countries;
use Modules\CountriesRegion\Models\Regions;
use Illuminate\Database\Eloquent\Model;

class ShippingZonesCountries extends Model
{

    protected $table = 'shipping_zones_countries';

    protected $fillable = [
        'shipping_zone_id',
        'country_id'
    ];

    public function shippingZone()
    {
        return $this->hasOne(ShippingZones::class, 'shipping_zone_id', 'id');
    }

    public function country()
    {
        return $this->hasOne(Countries::class, 'country_id', 'id');
    }

    public function shippingRegions()
    {
        return $this->hasMany(ShippingZonesRegions::class, 'shipping_zones_country_id', 'id');
    }

    public function regions()
    {
        return $this->hasManyThrough(Regions::class, ShippingZonesRegions::class, 'shipping_zones_country_id', 'id', 'id', 'region_id');
    }

    public function getRegionsIdsAttribute()
    {
        return $this->shippingRegions->pluck('region_id')->toArray();
    }

}
