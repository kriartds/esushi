<?php

namespace Modules\ShippingZones\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\ShippingZones\Models\ShippingZones;

class ShippingZonesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ShippingZones::firstOrCreate([
            'name' => 'Default shipping zones',
            'is_default' => 1
        ], [
            'position' => 1,
            'active' => 1
        ]);
    }
}
