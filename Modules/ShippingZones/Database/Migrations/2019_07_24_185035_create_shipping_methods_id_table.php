<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingMethodsIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_methods_id', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shipping_zone_id')->nullable();
            $table->enum('method_type', ['flat', 'free'])->default('flat')->nullable();
            $table->enum('free_shipping_requires', ['no_action', 'free_coupon', 'min_order_amount'])->default('no_action')->nullable();
            $table->decimal('amount', 10)->nullable();
            $table->decimal('min_order_amount', 10)->nullable();
            $table->tinyInteger('active')->default(1);
            $table->integer('position')->default(0);
            $table->timestamps();

            $table->foreign('shipping_zone_id')->references('id')->on('shipping_zones')->onDelete('cascade')->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_methods_id');
    }
}
