<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingZonesCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_zones_countries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shipping_zone_id')->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->timestamps();

            $table->foreign('shipping_zone_id')->references('id')->on('shipping_zones')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_zones_countries');
    }
}
