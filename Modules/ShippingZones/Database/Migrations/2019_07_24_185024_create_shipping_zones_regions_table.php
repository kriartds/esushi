<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingZonesRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_zones_regions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shipping_zones_country_id')->nullable();
            $table->unsignedInteger('region_id')->nullable();
            $table->timestamps();

            $table->foreign('shipping_zones_country_id', 'shipping_zones_country_id_foreign')->references('id')->on('shipping_zones_countries')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_zones_regions');
    }
}
