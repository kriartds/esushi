<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shipping_method_id');
            $table->unsignedInteger('lang_id')->nullable();
            $table->string('name')->nullable();
            $table->timestamps();

            $table->foreign('shipping_method_id')->references('id')->on('shipping_methods_id')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('set null')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_methods');
    }
}
