@if(!$items->isEmpty())
    <table class="table"
           data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition']) . "?shipping_zone=" . request()->get('shipping_zone', null)}}">
        <thead>
        <tr>
            <th>{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.method_title")}}</th>
            <th class="left">{{__("{$moduleName}::e.method_types")}}</th>
            <th class="left">{{__("{$moduleName}::e.free_shipping_requires")}}</th>
            <th></th>
            @if($permissions->active)
                <th>{{__("{$moduleName}::e.active_table")}}</th>
            @endif
            <th>{{__("{$moduleName}::e.position_table")}}</th>

            @if($permissions->delete)
                <th class="checkbox-all align-center" data-event="to-trash">
                    <div>Select All</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>

                <td class="medium title left"><a
                            href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, DEF_LANG_ID]) . "?shipping_zone=" . request()->get('shipping_zone', null)}}">{{@$item->globalName->name}}</a>
                </td>
                <td class="medium left">{{__("{$moduleName}::e.method_type_{$item->method_type}")}}</td>
                <td class="medium left">{{$item->method_type == 'free' ? __("{$moduleName}::e.free_shipping_require_{$item->free_shipping_requires}") : '-'}}</td>
                <td class="empty-td"></td>
                @if($permissions->active)
                    <td class="status">
                        <span class="activate-item active" data-active="1" data-item-id="1"></span>
                    </td>
                @endif
                <td class="position">
                    <img src="{{asset('admin-assets/img/small-icons/position.svg')}}" alt="">
                </td>
                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}"

                               data-url="{{url(LANG, ['admin', $currComponent->slug, 'destroy']) . "?shipping_zone=" . request()->get('shipping_zone', null)}}">
                        <label for="{{$item->id}}">

                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
