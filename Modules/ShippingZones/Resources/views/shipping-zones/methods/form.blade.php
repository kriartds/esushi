<form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form"
      enctype="multipart/form-data">
    <input type="hidden" name="shipping_zone" value="{{request()->get('shipping_zone', null)}}">
    <div class="background">
        <div class="smallContainer">

                <div class="field-row language">
                    <div class="label-wrap">
                        <label for="lang">{{__("{$moduleName}::e.lang")}}*</label>
                    </div>
                    <div class="field-wrap">
                        @if(!$langList->isEmpty())
                            <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                                @foreach($langList as $lang)
                                    <option value="{{$lang->id}}" {{ (is_null(@$langId) && $lang->id == LANG_ID ? 'selected' : $lang->id == @$langId ) ? 'selected' : ''}}>
                                        {{$lang->name ?: ''}}
                                    </option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>

            <div class="field-row">
                <div class="label-wrap">
                    <label for="shipping_zone_title">{{__("{$moduleName}::e.shipping_zone_title")}}</label>
                </div>
                <div class="field-wrap">
                    <input id="shipping_zone_title" value="{{@$item->shippingZone->name ?: @$shippingZone->name}}"
                           disabled>
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="name">{{__("{$moduleName}::e.method_title")}}*</label>
                </div>
                <div class="field-wrap">
                    <input id="name" name="name" value="{{@$item->itemByLang->name}}">
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="shipping_method_type">{{__("{$moduleName}::e.method_types")}}*</label>
                </div>
                <div class="field-wrap">
                    <select autocomplete="off" name="method_type" id="shipping_method_type" class="select2 no-search">
                        @if($shippingMethodsTypesEnums)
                            @foreach($shippingMethodsTypesEnums as $methodsType)
                                <option value="{{$methodsType}}" {{@$item->method_type == $methodsType ? 'selected' : ''}}>
                                    {{__("{$moduleName}::e.method_type_{$methodsType}")}}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div id="flat-method-block"
                 class="{{@$item->method_type && $item->method_type != 'flat' ? 'hidden' : ''}}">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="amount">{{__("{$moduleName}::e.amount")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input id="amount" name="amount" value="{{@$item->amount ?: 0}}">
                    </div>
                </div>
            </div>
            <div id="free-method-block"
                 class="{{!@$item->method_type || @$item->method_type != 'free' ? 'hidden' : ''}} smallFlex">
                <div class="field-row smallField">
                    <div class="label-wrap">
                        <label for="free_shipping_requires">Shipping methods*</label>
                    </div>
                    <div class="field-wrap">
                        <select autocomplete="off" name="free_shipping_requires" id="free_shipping_requires"
                                class="select2 no-search">
                            @if($shippingMethodsFreeShippingRequiresEnums)
                                @foreach($shippingMethodsFreeShippingRequiresEnums as $freeShippingRequire)
                                    <option value="{{$freeShippingRequire}}" {{@$item->free_shipping_requires == $freeShippingRequire ? 'selected' : ''}}>
                                        {{__("{$moduleName}::e.free_shipping_require_{$freeShippingRequire}")}}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="field-row {{!@$item->free_shipping_requires || @$item->free_shipping_requires == 'free_coupon' || @$item->free_shipping_requires == 'no_action' ? 'hidden' : ''}}"
                     id="min-order-amount-block">
                    <div class="label-wrap">
                        <label for="min_order_amount">{{__("{$moduleName}::e.min_order_amount")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input id="min_order_amount" name="min_order_amount" value="{{@$item->min_order_amount}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-left padding">
        <button class="btn medium submit-form-btn"
                data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
    </div>
</form>

