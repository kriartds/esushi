@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">

        {!! helpers()->getAdminBreadcrumbs($currComponent, new \Modules\ShippingZones\Models\ShippingZones(), @request()->get('shipping_zone')) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', !is_null(@$item) ? 'edit' : ''], 'item' => @$item, 'langId' => @$langId])

        <div class="form-block">
            @include("{$path}.form", ['item' => @$item])
        </div>
    </div>

@stop

@include('admin.footer')

