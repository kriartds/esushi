@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent, new \Modules\ShippingZones\Models\ShippingZones(),@request()->get('parent')) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'trash'], 'item' => null, 'currentPage' => 'trash'])

        <div class="table-block">
            @if(!$items->isEmpty())
                <table class="table trash">
                    <thead>
                    <tr>
                        <th>{{__("{$moduleName}::e.id_table")}}</th>
                        <th class="left">{{__("{$moduleName}::e.shipping_zone_title")}}</th>
                        <th class="left">{{__("{$moduleName}::e.countries")}}</th>
                        <th class="left">{{__("{$moduleName}::e.regions")}}</th>
                        <th class="left">{{__("{$moduleName}::e.method_types")}}</th>
                    @if($permissions->delete)
                            <th class="checkbox-all" >
                                <div>Select All</div>
                            </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td class="id">
                               <p>{{$item->id}}</p>
                            </td>
                            <td class="medium left prod-name">
                                <p>{{$item->name}}</p>
                            </td>
                            <td class="medium left prod-name" >
                                @if($item->countries->isNotEmpty())
                                    @foreach($item->countries->take(4) as $key => $country)
                                        <span>{{$country->name}}{{$item->countries->count() > $key + 1 ? ', ' : ''}}{{$item->countries->count() > 4 && $key == 3 ? '...' : ''}}</span>
                                    @endforeach
                                @else
                                    {{__("{$moduleName}::e.all_countries")}}
                                @endif
                            </td>
                            <td class="medium left prod-name">
                                @if($item->shippingRegions->isNotEmpty())
                                    @foreach($item->shippingRegions->take(4) as $key => $shippingRegion)
                                        <span>{{@$shippingRegion->region->name}}{{$item->shippingRegions->count() > $key + 1 ? ', ' : ''}}{{$item->shippingRegions->count() > 4 && $key == 3 ? '...' : ''}}</span>
                                    @endforeach
                                @else
                                    {{__("{$moduleName}::e.all_regions")}}
                                @endif
                            </td>
                            <td class="medium left prod-name">
                                @if($item->shippingMethods->isNotEmpty())
                                    @foreach($item->shippingMethods->take(4) as $key => $shippingMethod)
                                        <span>{{$shippingMethod->globalName->name}}{{$item->shippingMethods->count() > $key + 1 ? ', ' : ''}}{{$item->shippingMethods->count() > 4 && $key == 3 ? '...' : ''}}</span>
                                    @endforeach
                                @else
                                    -
                                @endif
                            </td>
                            @if($permissions->delete)
                                <td class="checkbox-items">
                                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                           name="checkbox_items[{{$item->id}}]"
                                           value="{{$item->id}}"

                                           >
                                    <label for="{{$item->id}}">
{{--                                        <span>Select</span>--}}
                                    </label>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')