<form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form"
      enctype="multipart/form-data">
    <div class="background">
        <div class="smallContainer">
            <div class="field-row">
                <div class="label-wrap">
                    <label for="name">{{__("{$moduleName}::e.shipping_zone_title")}}{{!@$item->is_default ? '*' : ''}}</label>
                </div>
                <div class="field-wrap">
                    <input name='name' id="name" value="{{@$item->name}}">
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="free_shipping_amount">{{__("{$moduleName}::e.free_shipping_amount")}}</label>
                </div>
                <div class="field-wrap">
                    <input name='free_shipping_amount' id="free_shipping_amount"  value="{{@$item->free_shipping_amount}}">
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="shipping_amount">{{__("{$moduleName}::e.shipping_amount")}}{{!@$item->is_default ? '*' : ''}}</label>
                </div>
                <div class="field-wrap">
                    <input name='shipping_amount' id="shipping_amount" value="{{@$item->shipping_amount}}">
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="iiko_id">{{__("{$moduleName}::e.iiko_cities")}}</label>
                </div>
                <div class="field-wrap">
                    <select name="iiko_id" id="iiko_id" class="select2 no-search">
                        <option></option>
                        @foreach($iiko_cities as $iiko_city)
                            <option value="{{$iiko_city->id}}" @if($iiko_city->id == @$item->iiko_id) selected @endif>{{$iiko_city->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="iiko_service_id">{{__("{$moduleName}::e.iiko_delivery")}}</label>
                </div>
                <div class="field-wrap">
                    <select name="iiko_service_id" id="iiko_service_id" class="select2 no-search">
                        <option></option>
                        @foreach($iiko_services as $iiko_service)
                            <option value="{{$iiko_service->iiko_id}}" @if($iiko_service->iiko_id == @$item->iiko_service_id) selected @endif>
                                {{$iiko_service->name}} {{$iiko_service->type}} {{$iiko_service->price}}MDL
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="m-left">
        <button class="btn medium  submit-form-btn"
                data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
    </div>
</form>

