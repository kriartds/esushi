@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th>{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">Title</th>
            <th class="left">{{__("{$moduleName}::e.is_default")}}</th>
            <th class="left">{{__("{$moduleName}::e.free_shipping_amount")}}</th>
            <th class="left">{{__("{$moduleName}::e.shipping_amount")}}</th>
            <th>{{__("{$moduleName}::e.position_table")}}</th>
            @if($permissions->active)
                <th>{{__("{$moduleName}::e.active_table")}}</th>
            @endif
            @if($permissions->delete)
                <th class="checkbox-all align-center">
                    <div>Check all</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}" class="{{$item->is_default ? 'isDefault' : ''}}">
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>
                <td class="medium prod-name left">
                    <a href="{{customUrl(['admin', $currComponent->slug,'edit',$item->id])}}">{{$item->name}}</a>
                </td>
                <td class="status">
                    <span class="activate-item {{$item->is_default ? 'active' : ''}}"
                              data-active="{{$item->is_default}}" data-item-id="{{$item->id}}"
                              data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateIsDefault'])}}">
                    </span>
                </td>
                <td class="medium left prod-name"> {{$item->free_shipping_amount}} </td>
                <td class="medium center"> {{$item->shipping_amount}} </td>
                <td class="position">
                    <img src="{{asset('admin-assets/img/small-icons/position.svg')}}" alt="">
                </td>

                @if($permissions->active)
                    <td class="status">
                        <span class="activate-item {{$item->active ? 'active' : ''}}"
                              data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                              data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>
                    </td>
                @endif

                @if($permissions->delete)
                    <td class="checkbox-items">
                        @if (!$item->is_default)
                            <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                   name="checkbox_items[{{$item->id}}]"
                                   value="{{$item->id}}">
                            <label for="{{$item->id}}"></label>

                        @else
                            <svg class="tooltip" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                 viewBox="0 0 20 20" data-tippy="" data-original-title="This item is set default!">
                                <g id="Group_360" data-name="Group 360" transform="translate(-1400 -1236)">
                                    <circle id="Ellipse_22" data-name="Ellipse 22" cx="10" cy="10" r="10"
                                            transform="translate(1400 1236)" fill="#9f9f9f"></circle>
                                    <path id="Path_79" data-name="Path 79"
                                          d="M2.769-3.056,3-9.953H.9l.232,6.9Zm-.82,1.08a1.091,1.091,0,0,0-.8.3,1,1,0,0,0-.3.749.992.992,0,0,0,.3.745,1.1,1.1,0,0,0,.8.294,1.107,1.107,0,0,0,.8-.294.992.992,0,0,0,.3-.745,1,1,0,0,0-.3-.752A1.107,1.107,0,0,0,1.948-1.976Z"
                                          transform="translate(1408 1251)" fill="#fff"></path>
                                </g>
                            </svg>
                        @endif
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
