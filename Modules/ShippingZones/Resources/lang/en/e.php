<?php

return [

    'element_is_active' => 'The element :name is active!',
    'element_is_inactive' => 'The element :name is inactive!',

    'shipping_zone_title' => 'Shipping zone title',
    'method_title' => 'Title',
    'edit_table' => 'Edit',
    'position_table' => 'Position',
    'active_table' => 'Status ',
    'delete_table' => 'Delete',
    'reestablish_table' => 'Reestablish',
    'id_table' => 'ID',
    'list_is_empty' => 'List is empty!',
    'save_it' => 'Save',
    'lang' => 'Lang',

    'countries' => 'Countries',
    'regions' => 'Regions',
    'empty_countries' => 'Leave empty if you want to apply current shipping zone to all countries!',
    'empty_regions' => 'Leave empty if you want to apply current shipping zone to all regions from selected countries!',
    'cant_delete_default_shipping_zone' => "You can\'t delete default shipping zone!",
    'all_countries' => 'All countries',
    'all_regions' => 'All regions',
    'method_types' => 'Shipping methods',
    'method_type_flat' => 'Flat rate',
    'method_type_free' => 'Free shipping',
    'free_shipping_require_no_action' => 'No action',
    'free_shipping_require_free_coupon' => 'A valid free shipping coupon',
    'free_shipping_require_min_order_amount' => 'A minimum order amount',
    'amount' => 'Amount',
    'free_shipping_requires' => 'Free shipping requires',
    'min_order_amount' => 'Min order amount',
    'free_shipping_amount' => 'Free shipping from',
    'shipping_amount' => 'Shipping amount',
    'is_default' => 'Is Default',
];