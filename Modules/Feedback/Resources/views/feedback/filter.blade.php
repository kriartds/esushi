@if(isset($filterParams))
    <div class="right-col">
        <button type="button" class="button gray filter-btn">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12">
                <g>
                    <g>
                        <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2"
                              d="M1 1h14"></path>
                    </g>
                    <g>
                        <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2"
                              d="M4 6h8"></path>
                    </g>
                    <g>
                        <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2"
                              d="M6 11h4"></path>
                    </g>
                </g>
            </svg>
            Filters
            <span class="count">{{$items->total()}}</span>
        </button>
    </div>

    <div class="hideForm filter form-block ">
        <form method="post" action="{{url(LANG, ['admin', $currComponent->slug, 'filter'])}}"
              class="filter-form"
              id="filter-form">
            <div class="flex">
                <div class="field-row-container">
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="name">{{__("{$moduleName}::e.name")}}</label>
                        </div>
                        <div class="field-wrap">
                            <input name="name" id="name"
                                   value="{{ !empty($filterParams) && array_key_exists('name', $filterParams) ? $filterParams['name'] : '' }}">
                        </div>
                    </div>
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="email">{{__("{$moduleName}::e.email")}}</label>
                        </div>
                        <div class="field-wrap">
                            <input name="email" id="email"
                                   value="{{ !empty($filterParams) && array_key_exists('email', $filterParams) ? $filterParams['email'] : '' }}">
                        </div>
                    </div>
                </div>
                <div class="field-row-container">
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="company">{{__("{$moduleName}::e.company")}}</label>
                        </div>
                        <div class="field-wrap">
                            <input name="company" id="company"
                                   value="{{ !empty($filterParams) && array_key_exists('company', $filterParams) ? $filterParams['company'] : '' }}">
                        </div>
                    </div>
                </div>
                <div class="field-row-container row">
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="start_date">{{__("{$moduleName}::e.from_date")}}</label>
                        </div>
                        <div class="field-wrap">
                            <input autocomplete="off" name="start_date" id="start_date" class="datetimepicker"
                                   value="{{ !empty($filterParams) && array_key_exists('start_date', $filterParams) ? date('d-m-Y', strtotime($filterParams['start_date'])) : '' }}">
                        </div>
                    </div>
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="end_date">{{__("{$moduleName}::e.to_date")}}</label>
                        </div>
                        <div class="field-wrap">
                            <input autocomplete="off" name="end_date" id="end_date" class="datetimepicker"
                                   value="{{ !empty($filterParams) && array_key_exists('end_date', $filterParams) ? date('d-m-Y', strtotime($filterParams['end_date'])) : '' }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="field-btn">
                <button class="btn btn-inline half submit-form-btn boldUpp" data-form-id="filter-form"
                        data-form-event="submit-form">{{__("{$moduleName}::e.filter")}}
                </button>
                <button class="btn btn-inline half submit-form-btn borderButton boldUpp" data-form-id="filter-form"
                        data-form-event="refresh-form">{{__("{$moduleName}::e.reset")}}
                </button>
            </div>
        </form>
    </div>
@endif