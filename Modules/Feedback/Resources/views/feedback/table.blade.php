@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th>{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.name")}}</th>
            <th class="left">Email/phone</th>
            <th class="left">Comment</th>
            <th>{{__("{$moduleName}::e.date")}}</th>
            @if($permissions->delete)
                <th class="checkbox-all" >
                    <div>Select All</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>
                <td class="left prod-name">
                    <p>
                        <a href="{{adminUrl([$currComponent->slug, 'view', $item->id])}}">{{$item->name ?: ''}}</a>
                    </p>
                </td>
                <td class="left prod-name">
                    <p>{{$item->phone ?: '-'}}</p>
                    <p>{{$item->email ?: '-'}}</p>
                </td>
                <td class="left prod-name">
                    <p>{{$item->message}}</p>
                </td>
                <td class="medium">{{!is_null($item->created_at) ? date('d-m-Y', strtotime($item->created_at)) : '-'}}</td>
                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}">
                        <label for="{{$item->id}}">
                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
