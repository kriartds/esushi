@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list','view', 'trash'], 'item' => null,'actionButton'=>true, 'filter' => ['path' => $path, 'filterParams' => $filterParams, 'items' => $items ]])

        <div class="table-block">
            @include("{$path}.table", ['items' => $items, 'componentSlug' => $currComponent->slug])
        </div>

    </div>

@stop

@include('admin.footer')