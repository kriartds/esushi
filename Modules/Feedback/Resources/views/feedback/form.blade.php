<div class="viewAllFeedback">

    <form>
        <div class="field-row">
            <div class="label-wrap">
                <label for="name">{{__("{$moduleName}::e.name")}}</label>
            </div>
            <div class="field-wrap">
                <input id="name" disabled value="{{@$item->name}}">
            </div>
        </div>

        <div class="field-row">
        </div>

        <div class="field-row">
            <div class="label-wrap">
                <label for="phone">{{__("{$moduleName}::e.phone")}}</label>
            </div>
            <div class="field-wrap">
                <input id="phone" disabled value="{{@$item->phone}}">
            </div>
        </div>
        <div class="field-row">
            <div class="label-wrap">
                <label for="email">{{__("{$moduleName}::e.email")}}</label>
            </div>
            <div class="field-wrap">
                <input id="email" disabled value="{{@$item->email}}">
            </div>
        </div>
        <div class="field-row textarea">
            <div class="label-wrap">
                <label for="message">{{__("{$moduleName}::e.comment")}}</label>
            </div>
            <div class="field-wrap">
                <textarea id="message" disabled>{{@$item->message}}</textarea>
            </div>
        </div>
        <div class="field-row right">
            <div class="label-wrap">
                <label for="date">{{__("{$moduleName}::e.date")}}</label>
            </div>
            <div class="field-wrap">
                <input id="date"
                       value="{{!is_null(@$item->created_at) ? date('d-m-Y', strtotime($item->created_at)) : date('d-m-Y')}}"
                       disabled>
            </div>
        </div>
        <div class="field-row rightNone">
            <div class="label-wrap">
                <label for="ip">{{__("{$moduleName}::e.ip")}}</label>
            </div>
            <div class="field-wrap ">
                <input id="ip" value="{{!is_null(@$item->ip) ? $item->ip : '-'}}" disabled>
            </div>
        </div>
    </form>
</div>
