@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}

        @include('admin.templates.pageTopButtons', ['action' => ['list','view', 'trash'], 'item' => null, 'currentPage' => 'trash'])

        <div class="table-block">
            @if(!$items->isEmpty())
                <table class="table trash">
                    <thead>
                    <tr>
                        <th>{{__("{$moduleName}::e.id_table")}}</th>
                        <th class="left">{{__("{$moduleName}::e.name")}}</th>
                        <th class="left">Phone Number / Email</th>
                        <th class="left">Comment</th>
                        <th>Date</th>
                        @if($permissions->delete)
                            <th class="checkbox-all" >
                                <div>Select All</div>
                            </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td class="id">
                                <p>{{$item->id}}</p>
                            </td>
                            <td class="left">{{$item->name}}</td>
                            <td class="prod-name left">
                                <p>{{$item->phone}}</p>
                                <p>{{$item->email}}</p>
                            </td>
                            <td class="left prod-name medium">
                                <p>{{$item->message}}</p>
                            </td>

                            <td class="medium">{{!is_null($item->created_at) ? date('d-m-Y', strtotime($item->created_at)) : '-'}}</td>
                            @if($permissions->delete)
                                <td class="checkbox-items">
                                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                           name="checkbox_items[{{$item->id}}]"
                                           value="{{$item->id}}" >
                                    <label for="{{$item->id}}">
                                    </label>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')