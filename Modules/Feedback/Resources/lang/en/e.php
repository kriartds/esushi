<?php

return [

    'edit_table' => 'Edit',
    'delete_table' => 'Delete',
    'reestablish_table' => 'Reestablish',
    'id_table' => 'ID',
    'list_is_empty' => 'List is empty!',
    'from_date' => 'From date',
    'to_date' => 'To date',
    'filter' => 'Filter',
    'reset' => 'Reset',

    'name' => "Name",
    'company' => 'Company',
    'phone' => 'Phone',
    'email' => 'Email',
    'comment' => 'Comment',
    'date' => 'Date',
    'ip' => 'Client IP',

];