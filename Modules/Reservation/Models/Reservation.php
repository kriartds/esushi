<?php

namespace Modules\Reservation\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model
{
    use SoftDeletes;

    protected $table = 'reservation';

    protected $fillable = [
        'name',
        'phone',
        'number_of_people',
        'time',
        'restaurant',
        'message',
        'status',
    ];
}
