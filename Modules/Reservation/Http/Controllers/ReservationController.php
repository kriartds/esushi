<?php

namespace Modules\Reservation\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Reservation\Models\Reservation;
use Modules\Restaurants\Models\RestaurantsId;


class ReservationController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'reservation';
        $this->path = "{$this->moduleName}::reservation";
    }

    public function index(Request $request)
    {
        $ourRestaurants = RestaurantsId::with('globalName')->where('active',1)->get();
        $response = $this->filter($request);

        $response['path'] = $this->path;
        $response['ourRestaurants'] = $ourRestaurants;

        return view("{$this->path}.list", $response);

    }

    public function filter(Request $request, $onlyOrders = false)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $filterParams = array_filter($request->except('page'));

        if (!$request->ajax()) {
            $newFiltersParams = [];

            if (!empty($filterParams) && count($filterParams) > 0) {

                foreach ($filterParams as $key => $one_filter_elem) {
                    $newFiltersParams[$key] = $one_filter_elem;
                    if(strpos($one_filter_elem, '[') !== false || strpos($one_filter_elem, ']') !== false) {
                        $newFiltersParams[$key] = explode(',', substr($filterParams[$key], 1, -1));
                    }
                }
            }

            $filterParams = $newFiltersParams;
        }

        $pushUrl = '';

        if (!empty($filterParams)) {
            foreach ($filterParams as $key => $one_filter_el) {

                if (is_array($one_filter_el)) {
                    $pushUrlArr = '';
                    foreach ($one_filter_el as $k => $filter_el) {
                        $pushUrlArr .= $filter_el . ',';
                    }

                    $pushUrl .= $key . '=[' . strip_tags(substr($pushUrlArr, 0, -1)) . ']&';
                } else {
                    $pushUrl .= $key . '=' . strip_tags(str_replace('[]', '', $one_filter_el)) . '&';
                }
            }

            $pushUrl = '?' . substr($pushUrl, 0, -1);
        }

        $items = Reservation::where(function ($q) use ($filterParams, $request) {
            if (array_key_exists('status', $filterParams) && is_array($filterParams['status'])){
                $q->where(function ($q) use ($filterParams) {
                    $q->whereIn('status', $filterParams['status']);
                });
            }
            if (array_key_exists('name', $filterParams) && !is_array($filterParams['name']))
                $q->where('name','like', "%{$filterParams['name']}%");

            if (array_key_exists('phone', $filterParams) && !is_array($filterParams['phone']))
                $q->where('phone','like', "%{$filterParams['phone']}%");

            if (array_key_exists('reservation_id', $filterParams) && !is_array($filterParams['reservation_id']))
                $q->where('id', $filterParams['reservation_id']);

            if (array_key_exists('client_email', $filterParams) && !is_array($filterParams['client_email']))
                $q->where('email', $filterParams['client_email']);


            if (array_key_exists('start_date', $filterParams) && !array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']))
                $q->whereDate('time', '>=', date('Y-m-d', strtotime($filterParams['start_date'])));
            elseif (!array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['end_date']))
                $q->whereDate('time', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));
            elseif (array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']) && !is_array($filterParams['end_date']))
                $q->whereDate('time', '>=', date('Y-m-d', strtotime($filterParams['start_date'])))->whereDate('time', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));

        })
            ->orderBy('id', 'desc')
            ->paginate($perPage);

        $items->setPath(url(LANG, ['admin', parent::currComponent()->slug]) . $pushUrl);

        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
            }

            return response()->json($response);
        }

        return $response;

    }

    public function create()
    {

        $ourRestaurants = RestaurantsId::with('globalName')->where('active',1)->get();
        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'ourRestaurants' => $ourRestaurants,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id)
    {
        $item = Reservation::findOrFail($id);

        if ($item->status && !$item->seen)
            $item->update(['seen' => 1]);

        $unreadNotification = auth()->guard('admin')->user()->unreadNotifications->where('data.order_id', $item->id)->first();
        $ourRestaurants = RestaurantsId::with('globalName')->where('active',1)->get();
        if ($unreadNotification)
            $unreadNotification->markAsRead();

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'ourRestaurants' => $ourRestaurants,
        ];


        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = Reservation::onlyTrashed()->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(Request $request, $id)
    {
        $rules = [
            'status' => 'required|in:confirmed,unconfirmed,canceled',
            'time' => 'required|date|date_format:d-m-Y H:i',
            'name' => 'required',
            'phone' => 'required',
            'restaurant' => 'required',
            'message' => 'nullable|max:1024',
            'number_of_people' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->errors(),
                    'type' => 'error'
                ],
            ]);
        }

        if ($id){
            $reservation = Reservation::findOrFail($id);
        }else{
            $reservation = new Reservation();
        }

        $reservation->name = $request->input('name');
        $reservation->phone = $request->input('phone');
        $reservation->restaurant = $request->input('restaurant');
        $reservation->message = $request->input('message');
        $reservation->number_of_people = $request->input('number_of_people');
        $reservation->status = $request->input('status');
        $reservation->time = carbon($request->input('time'));
        $reservation->save();

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':

                Reservation::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = Reservation::whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = Reservation::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = Reservation::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                    }
                }
                break;
            default:

                break;
        }
        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function destroy(Request $request)
    {

        $deletedItemsId = substr($request->get('id'), 1, -1);

        if (empty($deletedItemsId))
            return response()->json([
                'status' => false
            ]);

        if ($request->get('event') != 'to-trash' && $request->get('event') != 'from-trash' && $request->get('event') != 'restore')
            return response()->json([
                'status' => false
            ]);

        $currComponent = parent::currComponent();
        $deletedItemsIds = explode(',', $deletedItemsId);

        if ($request->get('event') == 'to-trash') {
            $items = Reservation::whereIn('id', $deletedItemsIds)->get();
        } else {
            $items = Reservation::onlyTrashed()->whereIn('id', $deletedItemsIds)->get();
        }

        $cartMessage = $responseMsg = '';

        if (!$items->isEmpty()) {
            foreach ($items as $item) {

                $cartMessage .= $item->id . ', ';

                if ($request->get('event') == 'to-trash' && !$item->trashed()) {
                    $item->delete();
                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' added to trash' : '';
                    helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                } elseif ($request->get('event') == 'from-trash') {
                    $item->forceDelete();
                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' remove from trash' : '';
                    helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                } elseif ($request->get('event') == 'restore') {
                    $item->restore();
                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' restored from trash' : '';
                    helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                }

            }

            return response()->json([
                'status' => true,
                'cart_messages' => $responseMsg,
                'items' => $deletedItemsIds
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }

}
