<?php

return [

    'element_is_active' => 'The element :name is active!',
    'element_is_inactive' => 'The element :name is inactive!',
    'element_is_hidden' => 'The element :name is hidden!',
    'element_is_visible' => 'The element :name is visible!',

    'title_table' => 'Title',
    'edit_table' => 'Edit',
    'active_table' => 'Status ',
    'delete_table' => 'Delete',
    'reestablish_table' => 'Reestablish',
    'id_table' => 'ID',
    'list_is_empty' => 'List is empty!',
    'save_it' => 'Save',

    'name' => "Name",
    'message' => 'Message',
    'restaurant' => 'Restaurant',
    'comment' => 'Comment',
    'date' => 'Date',
    'number_of_people' => 'Number',
    'status' => 'Status',
    'phone' => 'Phone',
    'reservation_id' => 'Reservation ID',
    'reservation' => 'Reservation',
    'confirmed' => 'Confirmed',
    'unconfirmed' => 'Unconfirmed',
    'canceled' => 'Canceled',
    'from_date' => 'From date',
    'to_date' => 'To date',

];