@if(isset($filterParams))
    <div class="right-col">
        <button type="button" class="button gray filter-btn">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12"><g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M1 1h14"></path></g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M4 6h8"></path></g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M6 11h4"></path></g></g></svg>
            Filters
            <span class="count">{{isset($items) && ((!empty($filterParams) || empty($filterParams)) && $items->total() > 0) ? $items->total() : ""}}</span>
        </button>
    </div>
    
    <div class="filter form-block">
        <form method="post" action="{{url(LANG, ['admin', $currComponent->slug, 'filter'])}}"
              class="filter-form"
              id="filter-form">
            <div class="field-row-wrap">
                <div class="col middle">
                    <div class="with-bg">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="reservation_id">{{__("{$moduleName}::e.reservation_id")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="reservation_id" id="reservation_id"
                                    value="{{ !empty($filterParams) && array_key_exists('reservation_id', $filterParams) ? $filterParams['reservation_id'] : '' }}">
                            </div>
                        </div>
                    </div>

                    <div class="with-bg">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="phone">{{__("{$moduleName}::e.phone")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="phone" id="phone"
                                       value="{{ !empty($filterParams) && array_key_exists('phone', $filterParams) ? $filterParams['phone'] : '' }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col middle">
                    <div class="with-bg">

                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="status">{{__("{$moduleName}::e.status")}}</label>
                            </div>
                            <div class="field-wrap">
                                <select autocomplete="off" name="status[]" id="status"
                                        class="select2" multiple>
                                    <option value="confirmed" {{!empty($filterParams) && array_key_exists('status', $filterParams) && in_array('confirmed', $filterParams['status']) ? 'selected' : ''}}>
                                        {{__("{$moduleName}::e.confirmed")}}
                                    </option>
                                    <option value="unconfirmed" {{!empty($filterParams) && array_key_exists('status', $filterParams) && in_array('unconfirmed', $filterParams['status']) ? 'selected' : ''}}>
                                        {{__("{$moduleName}::e.unconfirmed")}}
                                    </option>
                                    <option value="canceled" {{!empty($filterParams) && array_key_exists('status', $filterParams) && in_array('canceled', $filterParams['status']) ? 'selected' : ''}}>
                                        {{__("{$moduleName}::e.canceled")}}
                                    </option>

                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="with-bg">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="name">{{__("{$moduleName}::e.name")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="name" id="name"
                                       value="{{ !empty($filterParams) && array_key_exists('name', $filterParams) ? $filterParams['name'] : '' }}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col middle date">
                    <div class="with-bg flexClass">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="start_date">{{__("{$moduleName}::e.from_date")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="start_date" id="start_date" class="datetimepicker"
                                    value="{{ !empty($filterParams) && array_key_exists('start_date', $filterParams) ? date('d-m-Y', strtotime($filterParams['start_date'])) : '' }}">
                            </div>
                        </div>
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="end_date">{{__("{$moduleName}::e.to_date")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="end_date" id="end_date" class="datetimepicker"
                                    value="{{ !empty($filterParams) && array_key_exists('end_date', $filterParams) ? date('d-m-Y', strtotime($filterParams['end_date'])) : '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="field-btn">
                        <button class="half submit-form-btn button blue" data-form-id="filter-form"
                                data-form-event="submit-form">{{__('e.filter')}}
                        </button>
                        <button class=" half submit-form-btn button light-blue" data-form-id="filter-form"
                                data-form-event="refresh-form">{{__('e.reset')}}
                        </button>
                    </div>
                </div>
            </div>
            
   
        </form>
    </div>
@endif