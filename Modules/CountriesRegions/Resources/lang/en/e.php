<?php

return [

    'element_is_active' => 'The element :name is active!',
    'element_is_inactive' => 'The element :name is inactive!',

    'title_table' => 'Title',
    'edit_table' => 'Edit',
    'active_table' => 'Status ',
    'delete_table' => 'Delete',
    'id_table' => 'ID',
    'list_is_empty' => 'List is empty!',
    'save_it' => 'Save',

    'iso' => 'ISO',
    'iso3' => 'ISO3',
    'code' => 'Code',
    'country' => 'Country',
    'latitude' => 'Latitude',
    'longitude' => 'Longitude',

];