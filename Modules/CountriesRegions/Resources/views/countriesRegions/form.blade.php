<form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form"
      enctype="multipart/form-data">
    <div class="background">
        <div class="top-container-small">
            @if($country)

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="parent">{{__("{$moduleName}::e.country")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input type="hidden" name="country_id" value="{{@$country->id}}">
                        <input type="text" value="{{@$country->name}}" disabled>
                    </div>
                </div>
            @endif
            <div class="field-row">
                <div class="label-wrap">
                    <label for="name">{{__("{$moduleName}::e.title_table")}}*</label>
                </div>
                <div class="field-wrap">
                    <input name="name" id="name" value="{{@$item->name}}">
                </div>
            </div>
            @if(!$country)
                <div class="flex">

                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="iso">{{__("{$moduleName}::e.iso")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <input name="iso" id="iso" value="{{@$item->iso}}">
                        </div>
                    </div>


                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="iso3">{{__("{$moduleName}::e.iso3")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <input name="iso3" id="iso3" value="{{@$item->iso3}}">
                        </div>
                    </div>
                </div>

                <div class="inline-field-wrap flex-1">
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="code">{{__("{$moduleName}::e.code")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <input name="code" id="code" value="{{@$item->code}}">
                        </div>
                    </div>
                </div>


            @else
                <div class="field-row">
                    <div class="field-wrap field-wrap-children">
                        <div class="inline-field-wrap flex-1">
                            <div class="field-row">
                                <div class="label-wrap">
                                    <label for="latitude">{{__("{$moduleName}::e.latitude")}}</label>
                                </div>
                                <div class="field-wrap">
                                    <input name="latitude" id="latitude" value="{{@$item->full_info->lat}}">
                                </div>
                            </div>
                        </div>
                        <div class="inline-field-wrap flex-1">
                            <div class="field-row">
                                <div class="label-wrap">
                                    <label for="longitude">{{__("{$moduleName}::e.longitude")}}</label>
                                </div>
                                <div class="field-wrap">
                                    <input name="longitude" id="longitude" value="{{@$item->full_info->lng}}">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </div>
    <div class="m-left padding">
        <button class="btn  medium submit-form-btn"
                data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
    </div>
</form>

