@if(isset($filterParams))
    <div class="right-col">
        <button type="button" class="button gray filter-btn">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12">
                <g>
                    <g>
                        <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2"
                              d="M1 1h14"></path>
                    </g>
                    <g>
                        <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2"
                              d="M4 6h8"></path>
                    </g>
                    <g>
                        <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2"
                              d="M6 11h4"></path>
                    </g>
                </g>
            </svg>
            Filters
            <span class="count">1</span>
        </button>
    </div>
    <div class="formForCountres">
        <div class="filter form-block">
            <form method="post"
                  action="{{url(LANG, ['admin', $currComponent->slug, 'filter']) . ($countryId ? "?country_id={$countryId}" : "")}}"
                  class="filter-form"
                  id="filter-form">
                <div class="field-row-wrap hiddenWrap countries-reg">
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="name">{{__("{$moduleName}::e.title_table")}}</label>
                        </div>
                        <div class="field-wrap">
                            <input autocomplete="off" name="name" id="name"
                                   value="{{ !empty($filterParams) && array_key_exists('name', $filterParams) ? $filterParams['name'] : '' }}">
                        </div>
                    </div>

                        @if($isCountriesList)
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="iso">{{__("{$moduleName}::e.iso")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="iso" id="iso"
                                       value="{{ !empty($filterParams) && array_key_exists('iso', $filterParams) ? $filterParams['iso'] : '' }}">
                            </div>
                    </div>
                        @endif

                </div>
                <div class="field-btn ">
                    <button class="btn btn-inline half submit-form-btn mt" data-form-id="filter-form"
                            data-form-event="submit-form">{{__('e.filter')}}
                    </button>
                    <button class="btn btn-inline half borderButton  submit-form-btn mt" data-form-id="filter-form"
                            data-form-event="refresh-form">{{__('e.reset')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endif