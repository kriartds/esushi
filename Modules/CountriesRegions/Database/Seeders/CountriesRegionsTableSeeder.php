<?php

namespace Modules\CountriesRegions\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\CountriesRegion\Models\Countries;
use Modules\CountriesRegion\Models\Regions;

class CountriesRegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countriesFile = storage_path('app/seeds/allCountries.json');
        $regionsFile = storage_path('app/seeds/allRegions.json');
        if (file_exists($countriesFile)) {
            $countries = json_decode(file_get_contents($countriesFile), true) ?: [];

            $regions = collect();
            if (file_exists($regionsFile))
                $regions = collect(json_decode(file_get_contents($regionsFile), true) ?: []);

            if (!empty($countries)) {
                $existCountriesCount = Countries::count();
                $existRegionsCount = Regions::count();

                if (count($countries) != $existCountriesCount || count($regions) != $existRegionsCount) {
                    foreach ($countries as $country) {
                        $countryItem = Countries::firstOrCreate([
                            'iso' => @$country['alpha2Code'],
                            'iso3' => @$country['alpha3Code'],
                            'code' => @$country['numericCode'],
                        ], [
                            'name' => @$country['name'],
                            'full_info' => $country,
                            'active' => 1
                        ]);

                        $countryRegions = $regions->where('country', $countryItem->iso);
                        if ($countryRegions->isNotEmpty())
                            foreach ($countryRegions as $region) {
                                Regions::updateOrCreate([
                                    'country_id' => $countryItem->id,
                                    'full_info' => $region,
                                ], [
                                    'name' => @$region['name'],
                                    'active' => 1
                                ]);
                            }
                    }
                }
            }
        }
    }
}
