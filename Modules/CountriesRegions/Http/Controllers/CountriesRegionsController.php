<?php

namespace Modules\CountriesRegions\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\CountriesRegion\Models\Countries;
use Modules\CountriesRegion\Models\Regions;

class CountriesRegionsController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'countriesregions';
        $this->path = "{$this->moduleName}::countriesRegions";
    }

    public function index(Request $request)
    {

        $response = $this->filter($request);

        $response['path'] = $this->path;

        return view("{$this->path}.list", $response);
    }

    public function filter(Request $request)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $filterTableList = filterTableList($request, $request->except('page'));
        $filterParams = $filterTableList->filterParams;
        $pushUrl = $filterTableList->pushUrl;

        $countryId = $request->get('country_id');

        if ($countryId)
            $items = Regions::where('country_id', $countryId)
                ->where(function ($q) use ($filterParams, $request) {
                    if (array_key_exists('name', $filterParams) && !is_array($filterParams['name']))
                        $q->where('name', 'LIKE', "%{$filterParams['name']}%");
                })->with('country')->paginate($perPage);
        else
            $items = Countries::where(function ($q) use ($filterParams, $request) {
                if (array_key_exists('name', $filterParams) && !is_array($filterParams['name']))
                    $q->where('name', 'LIKE', "%{$filterParams['name']}%");

                if (array_key_exists('iso', $filterParams) && !is_array($filterParams['iso']))
                    $q->where('iso', $filterParams['iso']);

            })->withCount('regions')->paginate($perPage);

        $items->setPath(url(LANG, ['admin', parent::currComponent()->slug]) . $pushUrl);

        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'moduleName' => $this->moduleName,
            'isCountriesList' => is_null($countryId),
            'countryId' => $countryId,
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
            }

            return response()->json($response);
        }

        return $response;

    }

    public function create(Request $request)
    {

        $countryId = $request->get('country_id');

        $country = null;
        if ($countryId)
            $country = Countries::findOrFail($countryId);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'country' => $country
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit(Request $request, $id)
    {

        $countryId = $request->get('country_id');

        $country = null;
        if ($countryId) {
            $item = Regions::findOrFail($id);
            $country = Countries::findOrFail($countryId);
        } else
            $item = Countries::findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'country' => $country
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function save(Request $request, $id, $langId)
    {

        $countryId = $request->get('country_id');
        $isCountry = is_null($countryId);

        $rules = [
            'name' => [
                'required',
                Rule::unique($isCountry ? 'countries' : 'regions')->where(function ($q) use ($id, $countryId) {
                    $q->where('id', '!=', $id);

                    if ($countryId)
                        $q->where('country_id', $countryId);
                }),

            ]];

        if ($isCountry)
            $rules = array_merge($rules, [
                'iso' => "required|min:2|max:2|unique:countries,iso,{$id}",
                'iso3' => "required|min:3|max:3|unique:countries,iso3,{$id}",
                'code' => "required|numeric|unique:countries,code,{$id}",
            ]);
        else
            $rules = array_merge($rules, [
                'latitude' => 'nullable|numeric',
                'longitude' => 'nullable|numeric',
            ]);

        $item = Validator::make($request->all(), $rules);

        if ($item->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $item->messages(),
                    'type' => 'error'
                ],
            ]);

        $country = null;
        if (!$isCountry) {
            $country = Countries::find($countryId);

            if (!$country)
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Country not exist!'],
                        'type' => 'warning'
                    ]
                ]);
        }

        $currComponent = $this->currComponent();

        if ($isCountry) {
            $item = Countries::find($id);

            if (is_null($item)) {
                $item = new Countries();

                $item->active = 1;
            }

            $item->iso = strtoupper($request->get('iso', null));
            $item->iso3 = strtoupper($request->get('iso3', null));
            $item->code = $request->get('code', null);
            $item->full_info = null;
        } else {
            $item = Regions::find($id);

            if (is_null($item)) {
                $item = new Regions();

                $item->active = 1;
            }

            $item->country_id = $country->id;
            $item->full_info = [
                'country' => $country->iso,
                'name' => $request->get('name', null),
                'lat' => $request->get('latitude', null),
                'lng' => $request->get('longitude', null)
            ];
        }

        $item->name = $request->get('name', null);
        $item->save();

        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug]) . (!is_null(@$country->id) ? '?country_id=' . @$country->id : '')
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id]) . (!is_null(@$country->id) ? '?country_id=' . @$country->id : '')
            ]);
    }

    public function destroy(Request $request)
    {

        $countryId = $request->get('country_id');
        $isCountry = is_null($countryId);
        $deletedItemsId = substr($request->get('id'), 1, -1);

        if (empty($deletedItemsId))
            return response()->json([
                'status' => false
            ]);

        $deletedItemsIds = explode(',', $deletedItemsId);
        $instance = $isCountry ? new Countries() : new Regions();

        $items = $instance->whereIn('id', $deletedItemsIds)->get();

        $cartMessage = $responseMsg = '';

        if (!$items->isEmpty()) {
            foreach ($items as $item) {

                if (!is_null($item->globalName))
                    $cartMessage .= $item->name . ', ';

                $item->delete();
                $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' deleted' : '';
                helpers()->logActions($this->currComponent(), $item, $item->name, 'deleted');
            }

            return response()->json([
                'status' => true,
                'cart_messages' => $responseMsg,
                'items' => $deletedItemsIds
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }


    public function actionsCheckbox(Request $request)
    {
        $countryId = $request->get('country_id');
        $isCountry = is_null($countryId);
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        $instance = $isCountry ? new Countries() : new Regions();
        $items = $instance->whereIn('id', $ItemsId)->get();


        switch ($request->get('event')) {
            case 'status_check':
                $instance->whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
            case 'delete-from-trash':
            case 'restore-from-trash':
            default:
                foreach ($items as $item) {
                    $item->delete();
                    helpers()->logActions($this->currComponent(), $item, @$item->name, 'deleted');
                }
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function activateItem(Request $request)
    {

        $countryId = $request->get('country_id');
        $isCountry = is_null($countryId);
        $instance = $isCountry ? new Countries() : new Regions();

        $item = $instance->findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->name]);
            helpers()->logActions(parent::currComponent(), $item, @$item->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->name]);
            helpers()->logActions(parent::currComponent(), $item, @$item->name, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

}
