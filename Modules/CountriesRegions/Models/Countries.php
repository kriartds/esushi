<?php

namespace Modules\CountriesRegion\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\ShippingZones\Models\ShippingZones;
use Modules\ShippingZones\Models\ShippingZonesCountries;
use Modules\ShippingZones\Models\ShippingZonesRegions;
use Modules\Stores\Models\StoresId;


class Countries extends Model
{

    protected $table = 'countries';

    protected $fillable = [
        'iso',
        'name',
        'iso3',
        'code',
        'full_info',
        'active'
    ];

    protected $casts = [
        'full_info' => 'object'
    ];

    public function scopeCountriesWithShipping($q, $shippingMethodId = null)
    {
        return $q->whereHas('regions', function ($q) {
            $q->where('active', 1);
        })
            ->whereHas('shippingZones', function ($q) use ($shippingMethodId) {
                $q->where('active', 1);
                $q->whereHas('shippingMethods', function ($q) use ($shippingMethodId) {
                    $q->where('active', 1);
                    if ($shippingMethodId)
                        $q->where('id', $shippingMethodId);
                });
            });
    }

    public function scopeCountriesWithStores($q)
    {
        return $q->where('active', 1)
            ->whereHas('regionsWithStores');
    }

    public function regions()
    {
        return $this->hasMany(Regions::class, 'country_id', 'id');
    }

    public function shippingZones()
    {
        return $this->hasManyThrough(ShippingZones::class, ShippingZonesCountries::class, 'country_id', 'id', 'id', 'shipping_zone_id');
    }

    public function shippingZonesRegions()
    {
        return $this->hasManyThrough(ShippingZonesRegions::class, ShippingZonesCountries::class, 'country_id', 'shipping_zones_country_id', 'id', 'id');
    }

    public function stores()
    {
        return $this->hasMany(StoresId::class, 'country_id', 'id');
    }

    public function regionsWithStores()
    {

        return $this->regions()
            ->where('active', 1)
            ->whereHas('stores', function ($q) {
                $q->availableStoreForPickup();
            })->orderBy('name');
    }

    public function getShippingZonesRegionsIdsAttribute()
    {
        return $this->shippingZonesRegions->pluck('region_id')->toArray();
    }

}
