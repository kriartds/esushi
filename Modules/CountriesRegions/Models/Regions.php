<?php

namespace Modules\CountriesRegion\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\ShippingZones\Models\ShippingZonesRegions;
use Modules\Stores\Models\StoresId;

class Regions extends Model
{
    protected $table = 'regions';

    protected $fillable = [
        'country_id',
        'name',
        'full_info',
        'active'
    ];

    protected $casts = [
        'full_info' => 'object'
    ];

    public function country()
    {
        return $this->hasOne(Countries::class, 'id', 'country_id');
    }

    public function shippingZonesRegions()
    {
        return $this->hasMany(ShippingZonesRegions::class, 'region_id', 'id');
    }

    public function stores()
    {
        return $this->hasMany(StoresId::class, 'region_id', 'id');
    }

}
