<?php

return [

    'element_is_active' => 'The element :name is active!',
    'element_is_inactive' => 'The element :name is inactive!',

    'title_table' => 'Title',
    'slug_table' => 'Slug',
    'edit_table' => 'Edit',
    'active_table' => 'Status ',
    'delete_table' => 'Delete',
    'id_table' => 'ID',
    'list_is_empty' => 'List is empty!',
    'save_it' => 'Save',

    'code' => 'Code',
    'symbol' => 'Symbol',
    'symbol_position' => 'Symbol position',
    'show_on_left' => 'Symbol position on left',
    'left' => 'Left',
    'right' => 'Right',
    'default_currency' => 'Default currency',

];