@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th>{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.title_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.code")}}</th>
            <th class="left">{{__("{$moduleName}::e.symbol")}}</th>
            <th class="left">{{__("{$moduleName}::e.symbol_position")}}</th>

            @if($permissions->active)
                <th>{{__("{$moduleName}::e.default_currency")}}</th>
                <th>{{__("{$moduleName}::e.active_table")}}</th>
            @endif

            <th class="checkbox-all">
                <div>Select All</div>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>
                <td class="left title">
                    <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id])}}">{{@$item->name}}</a>
                </td>
                <td class="left">{{@$item->code}}</td>
                <td class="left">{!! @$item->symbol ?: '-' !!}</td>
                <td class="left">{{@$item->show_on_left ? __("{$moduleName}::e.left") : __("{$moduleName}::e.right")}}</td>
                @if($permissions->active)


                    <td class="smallItem">
                        <div class="div-center">
                            <label class="radio ">
                                <input name="name" type="radio" @if($item->is_default) checked @endif>
                                <span class="check default-item" data-active="{{$item->active}}" data-item-id="{{$item->id}}" data-url="{{url(LANG, ['admin', $currComponent->slug, 'changeDefaultCurrency'])}}"></span>
                            </label>
                        </div>
                    </td>

                    <td class="status">
                        <span class="activate-item {{$item->active ? 'active' : ''}}"
                              data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                              data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>
                    </td>


                @endif

                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}">
                        <label for="{{$item->id}}">
                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
