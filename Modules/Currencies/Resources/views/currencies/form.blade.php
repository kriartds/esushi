<form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form" enctype="multipart/form-data">
    <div class="background">
        <div class="mediumContainer">
            <div class="flex">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__("{$moduleName}::e.title_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$item->name}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="slug">{{__("{$moduleName}::e.slug_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="slug" id="slug" value="{{@$item->slug}}">
                    </div>
                </div>
            </div>
            <div class="field-row small">
                <div class="label-wrap">
                    <label for="code">{{__("{$moduleName}::e.code")}}*</label>
                </div>
                <div class="field-wrap">
                    <input name="code" id="code" value="{{@$item->code}}">
                </div>
            </div>
            <div class="flex">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="symbol">{{__("{$moduleName}::e.symbol")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="symbol" id="symbol" value="{!! @$item->symbol !!}">
                    </div>
                </div>
                {{--            <div class="field-row">--}}
                {{--                <div class="label-wrap">--}}
                {{--                    <label for="show_on_left">{{__("{$moduleName}::e.show_on_left")}}</label>--}}
                {{--                </div>--}}
                {{--                <div class="field-wrap">--}}
                {{--                    <input type="checkbox" name="show_on_left"--}}
                {{--                           id="show_on_left" {{@$item->show_on_left ? 'checked' : ''}}>--}}
                {{--                    <label for="show_on_left"></label>--}}
                {{--                </div>--}}
                {{--            </div>--}}
                <div class="field-row inline  white-bg">
                    <div class="label-wrap">
                        <label for="show_on_left">{{__("{$moduleName}::e.show_on_left")}}</label>
                    </div>
                    <div class="checkbox-switcher">
                        <input type="checkbox" name="show_on_left"
                               id="show_on_left" {{@$item->show_on_left ? 'checked' : ''}}>
                        <label for="show_on_left"></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-left padding">
        <button class="btn medium submit-form-btn"
                data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
    </div>
</form>

