<?php

namespace Modules\Currencies\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Modules\Currencies\Models\Currencies;

class CurrenciesController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'currencies';
        $this->path = "{$this->moduleName}::currencies";
    }

    public function index(Request $request)
    {

        $response = $this->filter($request);

        $response['path'] = $this->path;

        return view("{$this->path}.list", $response);
    }

    public function filter(Request $request)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $filterTableList = filterTableList($request, $request->except('page'));
        $filterParams = $filterTableList->filterParams;
        $pushUrl = $filterTableList->pushUrl;

        $items = Currencies::where(function ($q) use ($filterParams, $request) {
            if (array_key_exists('name', $filterParams) && !is_array($filterParams['name']))
                $q->where('name', 'LIKE', "%{$filterParams['name']}%");

            if (array_key_exists('code', $filterParams) && !is_array($filterParams['code']))
                $q->where('code', $filterParams['code']);
        })->paginate($perPage);

        $items->setPath(url(LANG, ['admin', parent::currComponent()->slug]) . $pushUrl);

        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'moduleName' => $this->moduleName
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
            }

            return response()->json($response);
        }

        return $response;

    }

    public function create(Request $request)
    {
        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit(Request $request, $id)
    {

        $item = Currencies::findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function save(Request $request, $id, $langId)
    {
        $currComponent = $this->currComponent();


        $item = Currencies::find($id);

        if (is_null($item)) {
            $item = new Currencies();

            $item->active = 1;
        }

        $item->slug = $request->get('slug', null);
        $item->name = strtoupper($request->get('name', null));
        $item->code = $request->get('code', null);
        $item->symbol = $request->get('symbol', null);
        $item->show_on_left = $request->get('show_on_left', null) == 'on' ? 1 : 0;

        $item->save();

        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id])
            ]);
    }

    public function destroy(Request $request)
    {

        $deletedItemsId = substr($request->get('id'), 1, -1);

        if (empty($deletedItemsId))
            return response()->json([
                'status' => false
            ]);

        $deletedItemsIds = explode(',', $deletedItemsId);
        $items = Currencies::whereIn('id', $deletedItemsIds)->get();

        $cartMessage = $responseMsg = '';

        if (!$items->isEmpty()) {
            foreach ($items as $item) {

                if (!is_null($item->globalName))
                    $cartMessage .= $item->name . ', ';

                $item->delete();
                $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' deleted' : '';
                helpers()->logActions($this->currComponent(), $item, $item->name, 'deleted');
            }

            return response()->json([
                'status' => true,
                'cart_messages' => $responseMsg,
                'items' => $deletedItemsIds
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }

    public function activateItem(Request $request)
    {



        $item = Currencies::findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->name]);
            helpers()->logActions(parent::currComponent(), $item, @$item->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->name]);
            helpers()->logActions(parent::currComponent(), $item, @$item->name, 'activate');
        }

        if ($request->get('action') == 'default') {
            Currencies::query()->update(['is_default' => 0]);
            $item->update(['is_default' => 1]);
            Cookie::queue(Cookie::forget('defaultCurrency'));
            request()->session()->forget('defaultCurrency');
        } else{
            $item->update(['active' => $status]);
        }


        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                Currencies::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            default:
                $items = Currencies::whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted');
                    }
                }
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function changeDefaultCurrency(Request $request)
    {
        $currency = Currencies::find($request->get('id'));

        if ($currency && $currency->active) {
            Currencies::where('is_default', 1)->update(['is_default' => 0]);
            $changed = Currencies::find($request->get('id'))->update(['is_default' => 1]);
        } else {
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => 'You can\'t set as default a currency that isn\'t acctive',
                    'type' => 'error'
                ],
            ]);
        }
        return response()->json([
            'status' => true,
            'msg' => [
                'e' => __("e.successful_edited", ['name' => @$changed->name]),
                'type' => 'success'
            ],
        ]);
    }

}
