<?php

namespace Modules\Currencies\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class SaveCurrenciesRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => "required|unique:currencies,name,{$this->id}",
            'slug' => "required|unique:currencies,slug,{$this->id}",
            'code' => "required|numeric|unique:currencies,code,{$this->id}",
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function failedValidation(Validator $validator)
    {
        $data = [
            'status' => false,
            'validator' => true,
            'msg' => [
                'e' => $validator->messages(),
                'type' => 'error'
            ],
        ];

        throw new HttpResponseException(response()->json($data));
    }
}
