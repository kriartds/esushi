<?php

namespace Modules\Currencies\Database;

use Illuminate\Database\Seeder;
use Modules\Currencies\Models\Currencies;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currenciesFile = storage_path('app/seeds/currencies.json');
        if (file_exists($currenciesFile)) {
            $currencies = json_decode(file_get_contents($currenciesFile), true) ?: [];

            if (!empty($currencies))
                foreach ($currencies as $currency) {
                    Currencies::firstOrCreate([
                        'slug' => @$currency['slug'],
                        'code' => @$currency['code'],
                    ], [
                        'name' => @$currency['name'],
                        'symbol' => @$currency['symbol'],
                        'is_default' => @$currency['is_default'],
                        'show_on_left' => @$currency['show_on_left'],
                        'active' => 1
                    ]);
                }

        }
    }
}
