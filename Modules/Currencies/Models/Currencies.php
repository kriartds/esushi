<?php

namespace Modules\Currencies\Models;

use Illuminate\Database\Eloquent\Model;

class Currencies extends Model
{

    protected $table = 'currencies';

    protected $fillable = [
        'slug',
        'name',
        'code',
        'symbol',
        'is_default',
        'show_on_left',
        'active'
    ];

    public function currenciesByDate()
    {
        return $this->hasMany(CurrenciesByDate::class, 'currency_id', 'id');
    }
}
