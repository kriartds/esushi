<?php

namespace Modules\Currencies\Models;

use Illuminate\Database\Eloquent\Model;

class CurrenciesByDate extends Model
{

    protected $table = 'currencies_by_date';

    protected $fillable = [
        'currency_id',
        'value',
        'date'
    ];

    public function currency()
    {
        return $this->hasOne(Currencies::class, 'id', 'currency_id');
    }
}
