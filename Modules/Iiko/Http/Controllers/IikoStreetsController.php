<?php

namespace Modules\Iiko\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\IikoApi\IikoClient;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\IikoApi\Models\IikoCities;
use App\IikoApi\Models\IikoProducts;
use App\IikoApi\Models\IikoRegions;
use App\IikoApi\Models\IikoStreets;
use App\IikoApi\Models\LogIiko;
use Modules\Iiko\Models\IikoOrganization;
use Modules\Iiko\Models\IikoTerminal;
use Modules\Orders\Models\Cart;
use Modules\ShippingZones\Models\ShippingZones;
use Modules\SiteUsers\Models\Users;

class IikoStreetsController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'iiko';
        $this->path = "{$this->moduleName}::iiko.streets";
    }

    public function index(Request $request)
    {
        $response = $this->filter($request);

        $response['iiko_terminals'] = IikoTerminal::where('active', 1)->with(['organization'])->get();
        $response['shipping_zones'] = ShippingZones::all();
        $response['path'] = $this->path;

        return view("{$this->path}.list", $response);

    }

    public function filter(Request $request, $onlyOrders = false)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $filterParams = array_filter($request->except('page'));

        if (!$request->ajax()) {
            $newFiltersParams = [];

            if (!empty($filterParams) && count($filterParams) > 0) {

                foreach ($filterParams as $key => $one_filter_elem) {
                    $newFiltersParams[$key] = $one_filter_elem;
                    if(strpos($one_filter_elem, '[') !== false || strpos($one_filter_elem, ']') !== false) {
                        $newFiltersParams[$key] = explode(',', substr($filterParams[$key], 1, -1));
                    }
                }
            }

            $filterParams = $newFiltersParams;
        }

        $pushUrl = '';

        if (!empty($filterParams)) {
            foreach ($filterParams as $key => $one_filter_el) {

                if (is_array($one_filter_el)) {
                    $pushUrlArr = '';
                    foreach ($one_filter_el as $k => $filter_el) {
                        $pushUrlArr .= $filter_el . ',';
                    }

                    $pushUrl .= $key . '=[' . strip_tags(substr($pushUrlArr, 0, -1)) . ']&';
                } else {
                    $pushUrl .= $key . '=' . strip_tags(str_replace('[]', '', $one_filter_el)) . '&';
                }
            }

            $pushUrl = '?' . substr($pushUrl, 0, -1);
        }

        $items = IikoStreets::where(function ($q) use ($filterParams, $request) {

        })//->orderBy('order', 'ASC')
          //->orderBy('type', 'ASC')
          ->paginate($perPage)
        ;

        $items->setPath(url(LANG, ['admin', parent::currComponent()->slug]) . $pushUrl);

        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
            }

            return response()->json($response);
        }

        return $response;
    }

    public function changeTerminal(Request $request, $streetid)
    {
        if($request->ajax())
        {
            $iiko_terminal = IikoTerminal::find($request->get('item_id'));

            if(!is_null($iiko_terminal))
            {
                $iiko_street = IikoStreets::find($streetid);

                if($iiko_street){
                    $iiko_street->terminal_id = $iiko_terminal->id;
                    $iiko_street->organization_id = $iiko_terminal->organization_id;
                    $iiko_street->save();

                    return response()->json([
                        'msg' => ['type' => 'success', 'e' => 'Data successfuly saved']
                    ]);
                }else{
                    return response()->json([
                        'msg' => ['type' => 'error', 'e' => 'Street not found']
                    ]);
                }
            }else{
                return response()->json([
                    'msg' => ['type' => 'error', 'e' => 'Terminal not selected']
                ]);
            }
        }
    }

    public function changeZone(Request $request, $streetid)
    {
        if($request->ajax())
        {
            $shipping_zone = ShippingZones::find($request->get('item_id'));

            if(!is_null($shipping_zone))
            {
                $iiko_street = IikoStreets::find($streetid);

                if($iiko_street){
                    $iiko_street->shipping_zone_id = $shipping_zone->id;
                    $iiko_street->save();

                    return response()->json([
                        'msg' => ['type' => 'success', 'e' => 'Data successfuly saved']
                    ]);
                }else{
                    return response()->json([
                        'msg' => ['type' => 'error', 'e' => 'Street not found']
                    ]);
                }
            }else{
                return response()->json([
                    'msg' => ['type' => 'error', 'e' => 'Terminal not selected']
                ]);
            }
        }
    }
}
