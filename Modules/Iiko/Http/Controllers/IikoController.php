<?php

namespace Modules\Iiko\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\IikoApi\IikoClient;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\IikoApi\Models\IikoCities;
use App\IikoApi\Models\IikoProducts;
use App\IikoApi\Models\IikoRegions;
use App\IikoApi\Models\IikoStreets;
use App\IikoApi\Models\LogIiko;
use Modules\Iiko\Models\IikoOrganization;
use Modules\Iiko\Models\IikoTerminal;
use Modules\Orders\Models\Cart;
use Modules\ShippingZones\Models\ShippingZones;
use Modules\SiteUsers\Models\Users;

class IikoController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'iiko';
        $this->path = "{$this->moduleName}::iiko";
    }

    public function index(Request $request)
    {
        return redirect(adminUrl(['iiko', 'restaurants']));

        $response = $this->filter($request);

        $response['path'] = $this->path;

        return view("{$this->path}.list", $response);

    }

    public function filter(Request $request, $onlyOrders = false)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $filterParams = array_filter($request->except('page'));

        if (!$request->ajax()) {
            $newFiltersParams = [];

            if (!empty($filterParams) && count($filterParams) > 0) {

                foreach ($filterParams as $key => $one_filter_elem) {
                    $newFiltersParams[$key] = $one_filter_elem;
                    if(strpos($one_filter_elem, '[') !== false || strpos($one_filter_elem, ']') !== false) {
                        $newFiltersParams[$key] = explode(',', substr($filterParams[$key], 1, -1));
                    }
                }
            }

            $filterParams = $newFiltersParams;
        }

        $pushUrl = '';

        if (!empty($filterParams)) {
            foreach ($filterParams as $key => $one_filter_el) {

                if (is_array($one_filter_el)) {
                    $pushUrlArr = '';
                    foreach ($one_filter_el as $k => $filter_el) {
                        $pushUrlArr .= $filter_el . ',';
                    }

                    $pushUrl .= $key . '=[' . strip_tags(substr($pushUrlArr, 0, -1)) . ']&';
                } else {
                    $pushUrl .= $key . '=' . strip_tags(str_replace('[]', '', $one_filter_el)) . '&';
                }
            }

            $pushUrl = '?' . substr($pushUrl, 0, -1);
        }

        $items = IikoProducts::where(function ($q) use ($filterParams, $request) {

        })->orderBy('id', 'desc')
          ->paginate($perPage);

        $items->setPath(url(LANG, ['admin', parent::currComponent()->slug]) . $pushUrl);

        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
            }

            return response()->json($response);
        }

        return $response;
    }

    public function restaurants(Request $request)
    {
        $response['items'] = [];
        $iikoClient = new IikoClient();
        $resp = $iikoClient->getOrganizations();
        if(isset($resp['organizations']) && !empty($resp['organizations']))
        {
            $response['items'] = $resp['organizations'];
        }

        $response['iikoClient'] = $iikoClient;
        $response['path'] = $this->path;
        $response['moduleName'] = $this->moduleName;

        return view("{$this->path}.restaurants.list", $response);
    }

    public function getRegions(Request $request, $organizationId)
    {
        $response['items'] = [];
        $iikoClient = new IikoClient();
        $resp = $iikoClient->getRegions($organizationId);
        //dd($resp);
        if(isset($resp['regions']) && !empty($resp['regions']))
        {
            $response['regions'] = $resp['regions'];
        }

        $response['path'] = $this->path;
        $response['moduleName'] = $this->moduleName;

        return view("{$this->path}.regions.list", $response);
    }

    public function getCities(Request $request, $organizationId)
    {
        $response['items'] = [];
        $iikoClient = new IikoClient();
        $resp = $iikoClient->getCities($organizationId);
        //dd($resp);
        if(isset($resp['cities']) && !empty($resp['cities']))
        {
            $response['cities'] = $resp['cities'];
        }

        $response['path'] = $this->path;
        $response['moduleName'] = $this->moduleName;

        return view("{$this->path}.cities.list", $response);
    }

    public function getStreets(Request $request, $organizationId)
    {
        $response['items'] = [];
        $iikoClient = new IikoClient();
        $resp = $iikoClient->getStreetsByCity($request->city, $organizationId);

        if(isset($resp['streets']) && !empty($resp['streets']))
        {
            $response['streets'] = $resp['streets'];
        }

        $response['path'] = $this->path;
        $response['moduleName'] = $this->moduleName;

        return view("{$this->path}.streets.list", $response);
    }

    public function sync(Request $request)
    {
        if($request->ajax())
        {
            $iikoClient = new IikoClient();
            $iiko_resp = $iikoClient->getOrganizations();
            if(isset($iiko_resp['organizations']) && !empty($iiko_resp['organizations']))
            {
                $organizationIds = [];
                foreach ($iiko_resp['organizations'] as $organization)
                {
                    $temp_resp = IikoOrganization::find($organization['id']);
                    if(is_null($temp_resp)) $temp_resp = new IikoOrganization();
                    $temp_resp->id = $organization['id'];
                    $temp_resp->name = $organization['name'];
                    $temp_resp->address = $organization['restaurantAddress'];
                    $temp_resp->save();
                    $temp_resp = null;

                    $organizationIds[] = $organization['id'];
                }

                $iiko_resp = $iikoClient->getTerminals($organizationIds);
                if(isset($iiko_resp['terminalGroups']) && !empty($iiko_resp['terminalGroups'])){
                    foreach ($iiko_resp['terminalGroups'] as $terminalGroup){
                        foreach ($terminalGroup['items'] as $terminal) {
                            $temp_resp = IikoTerminal::find($terminal['id']);
                            if (is_null($temp_resp)) $temp_resp = new IikoTerminal();
                            $temp_resp->id = $terminal['id'];
                            $temp_resp->organization_id = $terminal['organizationId'];
                            $temp_resp->name = $terminal['name'];
                            $temp_resp->save();
                            $temp_resp = null;
                        }
                    }
                }

                $iiko_resp = $iikoClient->getRegions($organizationIds);
                if(isset($iiko_resp['regions']) && !empty($iiko_resp['regions'])){
                    foreach ($iiko_resp['regions'] as $item){
                        foreach ($item['items'] as $subitem) {
                            $temp_resp = IikoRegions::find($subitem['id']);
                            if (is_null($temp_resp)) $temp_resp = new IikoRegions();
                            $temp_resp->id = $subitem['id'];
                            $temp_resp->organization_id = $item['organizationId'];
                            $temp_resp->name = $subitem['name'];
                            $temp_resp->save();
                            $temp_resp = null;
                        }
                    }
                }

                $iiko_resp = $iikoClient->getCities($organizationIds);
                if(isset($iiko_resp['cities']) && !empty($iiko_resp['cities'])){
                    foreach ($iiko_resp['cities'] as $item){
                        foreach ($item['items'] as $subitem) {
                            $temp_resp = IikoCities::find($subitem['id']);
                            if (is_null($temp_resp)) $temp_resp = new IikoCities();
                            $temp_resp->id = $subitem['id'];
                            $temp_resp->organization_id = $item['organizationId'];
                            $temp_resp->name = $subitem['name'];
                            $temp_resp->save();
                            $temp_resp = null;

                            $local_organization = IikoOrganization::find($item['organizationId']);
                            $shippingZones = ShippingZones::all();
                            $iiko_resp = $iikoClient->getStreetsByCity($subitem['id'], $item['organizationId']);
                            if(isset($iiko_resp['streets']) && !empty($iiko_resp['streets'])){
                                foreach ($iiko_resp['streets'] as $itemst){
                                    $temp_resp = IikoStreets::find($itemst['id']);
                                    if (is_null($temp_resp)) $temp_resp = new IikoStreets();
                                    $temp_resp->id = $itemst['id'];
                                    $temp_resp->city_id = $subitem['id'];
                                    $temp_resp->name = $itemst['name'];
                                    //$temp_resp->text .= $item['organizationId'].'|';
                                    if(!empty($local_organization) && strpos($itemst['name'], $local_organization->name) !== false){
                                        $temp_resp->organization_id = $item['organizationId'];
                                        $temp_resp->terminal_id = $local_organization->terminals[0]->id;
                                    }

                                    foreach ($shippingZones as $zone){
                                        if(strpos($itemst['name'], $zone->name) !== false){
                                            $temp_resp->shipping_zone_id = $zone->id;
                                            break;
                                        }
                                    }

                                    $temp_resp->save();
                                    $temp_resp = null;
                                }
                            }

                        }
                    }
                }

//                $iiko_resp = $iikoClient->getNomenclature($organizationIds);
//                if(isset($iiko_resp['regions']) && !empty($iiko_resp['regions'])){
//                    foreach ($iiko_resp['regions'] as $item){
//                        foreach ($item['items'] as $subitem) {
//                            $temp_resp = IikoRegions::find($subitem['id']);
//                            if (is_null($temp_resp)) $temp_resp = new IikoRegions();
//                            $temp_resp->id = $subitem['id'];
//                            $temp_resp->organization_id = $item['organizationId'];
//                            $temp_resp->name = $subitem['name'];
//                            $temp_resp->save();
//                            $temp_resp = null;
//                        }
//                    }
//                }
            }

            return response()->json([
                'msg' => ['type' => 'success', 'e' => 'Data successfuly synchronized']
            ]);
        }
    }

    public function getOrderTypes(Request $request, $organizationId)
    {
        $response['items'] = [];
        $iikoClient = new IikoClient();
        $resp = $iikoClient->getOrderTypes($organizationId);
        //dd($resp);
        if(isset($resp['orderTypes']) && !empty($resp['orderTypes']))
        {
            $response['orderTypes'] = $resp['orderTypes'];
        }

        //$response['path'] = $this->path;
        //$response['moduleName'] = $this->moduleName;

        //return view("{$this->path}.regions.list", $response);
    }

    public function getPaymentTypes(Request $request, $organizationId)
    {
        $response['items'] = [];
        $iikoClient = new IikoClient();
        $resp = $iikoClient->getPaymentTypes($organizationId);
        dd($resp);
        if(isset($resp['paymentTypes']) && !empty($resp['paymentTypes']))
        {
            $response['paymentTypes'] = $resp['paymentTypes'];
        }

        //$response['path'] = $this->path;
        //$response['moduleName'] = $this->moduleName;

        //return view("{$this->path}.regions.list", $response);
    }

}
