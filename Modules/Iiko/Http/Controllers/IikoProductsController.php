<?php

namespace Modules\Iiko\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\IikoApi\IikoClient;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\IikoApi\Models\IikoCities;
use App\IikoApi\Models\IikoProducts;
use App\IikoApi\Models\IikoRegions;
use App\IikoApi\Models\IikoStreets;
use App\IikoApi\Models\LogIiko;
use Modules\Iiko\Models\IikoGroups;
use Modules\Iiko\Models\IikoOrganization;
use Modules\Iiko\Models\IikoProductCategories;
use Modules\Iiko\Models\IikoTerminal;
use Modules\Orders\Models\Cart;
use Modules\SiteUsers\Models\Users;

class IikoProductsController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'iiko';
        $this->path = "{$this->moduleName}::iiko.products";
    }

    public function index(Request $request)
    {
        $response = $this->filter($request);

        $response['path'] = $this->path;

        return view("{$this->path}.list", $response);

    }

    public function filter(Request $request, $onlyOrders = false)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $filterParams = array_filter($request->except('page'));

        if (!$request->ajax()) {
            $newFiltersParams = [];

            if (!empty($filterParams) && count($filterParams) > 0) {

                foreach ($filterParams as $key => $one_filter_elem) {
                    $newFiltersParams[$key] = $one_filter_elem;
                    if(strpos($one_filter_elem, '[') !== false || strpos($one_filter_elem, ']') !== false) {
                        $newFiltersParams[$key] = explode(',', substr($filterParams[$key], 1, -1));
                    }
                }
            }

            $filterParams = $newFiltersParams;
        }

        $pushUrl = '';

        if (!empty($filterParams)) {
            foreach ($filterParams as $key => $one_filter_el) {

                if (is_array($one_filter_el)) {
                    $pushUrlArr = '';
                    foreach ($one_filter_el as $k => $filter_el) {
                        $pushUrlArr .= $filter_el . ',';
                    }

                    $pushUrl .= $key . '=[' . strip_tags(substr($pushUrlArr, 0, -1)) . ']&';
                } else {
                    $pushUrl .= $key . '=' . strip_tags(str_replace('[]', '', $one_filter_el)) . '&';
                }
            }

            $pushUrl = '?' . substr($pushUrl, 0, -1);
        }

        $items = IikoProducts::where(function ($q) use ($filterParams, $request) {

        })//->orderBy('order', 'ASC')
          ->orderBy('type', 'ASC')
          ->paginate(1000)
        ;

        $items->setPath(url(LANG, ['admin', parent::currComponent()->slug]) . $pushUrl);

        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
            }

            return response()->json($response);
        }

        return $response;
    }

    public function syncProducts(Request $request)
    {
        if($request->ajax())
        {
            //f5ec5c0b-c0fc-448f-a195-50ce261175b1
            //$organization = IikoOrganization::find('f5ec5c0b-c0fc-448f-a195-50ce261175b1');
            $organizations = IikoOrganization::all();
            foreach ($organizations as $k=>$organization)
                if(!is_null($organization->id))
                {
                    $iikoClient = new IikoClient();
                    $iiko_resp = $iikoClient->getNomenclature($organization->id);

                    if(isset($iiko_resp['products']) && !empty($iiko_resp['products']))
                    {
                        foreach ($iiko_resp['products'] as $product)
                        {
                            $temp_resp = IikoProducts::where('iiko_id', $product['id'])->first();
                            if(is_null($temp_resp)) $temp_resp = new IikoProducts();
                            $temp_resp->iiko_id = $product['id'];
                            $temp_resp->organization_id = $organization->id;
                            $temp_resp->code = $product['code'];
                            $temp_resp->name = $product['name'];
                            $temp_resp->product_category_id = $product['productCategoryId'];
                            $temp_resp->group_id = $product['groupId'];
                            $temp_resp->group_modifiers = $product['groupModifiers'];
                            $temp_resp->modifiers = $product['modifiers'];
                            $temp_resp->price = $product['sizePrices'][0]['price']['currentPrice'];
                            $temp_resp->type = $product['type'];
                            $temp_resp->parent_group = $product['parentGroup'];
                            $temp_resp->order = $product['order'];
                            $temp_resp->weight = $product['weight'];
                            $temp_resp->order_item_type = $product['orderItemType'];
                            $temp_resp->size_prices = $product['sizePrices'];
                            $temp_resp->save();
                            $temp_resp = null;
                        }
                    }

                    if(isset($iiko_resp['groups']) && !empty($iiko_resp['groups']))
                    {
                        foreach ($iiko_resp['groups'] as $group)
                        {
                            $temp_resp = IikoGroups::where('iiko_id', $group['id'])->first();
                            if(is_null($temp_resp)) $temp_resp = new IikoGroups();
                            $temp_resp->iiko_id = $group['id'];
                            $temp_resp->name = $group['name'];
                            $temp_resp->imageLinks = json_encode($group['imageLinks']);
                            $temp_resp->isIncludedInMenu = $group['isIncludedInMenu'];
                            $temp_resp->isGroupModifier = $group['isGroupModifier'];
                            $temp_resp->description = $group['description'];
                            $temp_resp->save();
                            $temp_resp = null;
                        }
                    }

                    if(isset($iiko_resp['productCategories']) && !empty($iiko_resp['productCategories']))
                    {
                        foreach ($iiko_resp['productCategories'] as $productCategory)
                        {
                            $temp_resp = IikoProductCategories::where('iiko_id', $productCategory['id'])->first();
                            if(is_null($temp_resp)) $temp_resp = new IikoProductCategories();
                            $temp_resp->iiko_id = $productCategory['id'];
                            $temp_resp->name = $productCategory['name'];
                            $temp_resp->save();
                            $temp_resp = null;
                        }
                    }

                }

            return response()->json([
                'msg' => ['type' => 'success', 'e' => 'Data successfuly synchronized']
            ]);
        }
    }
}
