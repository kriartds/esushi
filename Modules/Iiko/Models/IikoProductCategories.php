<?php

namespace Modules\Iiko\Models;

use App\IikoApi\Models\IikoProducts;
use Illuminate\Database\Eloquent\Model;

class IikoProductCategories extends Model
{
    protected $fillable = [];
    protected $guarded = [];

    public function products(){
        return $this->hasMany(IikoProducts::class, 'product_category_id', 'id');
    }
}
