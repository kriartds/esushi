<?php

namespace Modules\Iiko\Models;

use Illuminate\Database\Eloquent\Model;

class IikoTerminal extends Model
{
    protected $fillable = [];
    protected $guarded = [];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function organization(){
        return $this->hasOne(IikoOrganization::class, 'id', 'organization_id');
    }
}
