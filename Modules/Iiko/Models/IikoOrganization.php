<?php

namespace Modules\Iiko\Models;

use Illuminate\Database\Eloquent\Model;

class IikoOrganization extends Model
{
    protected $fillable = [];
    protected $guarded = [];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function terminals()
    {
        return $this->hasMany(IikoTerminal::class, 'organization_id', 'id');
    }
}
