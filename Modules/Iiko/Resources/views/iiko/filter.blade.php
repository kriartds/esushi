@if(isset($filterParams))
    <div class="right-col">
        <button type="button" class="button gray filter-btn">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12"><g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M1 1h14"></path></g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M4 6h8"></path></g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M6 11h4"></path></g></g></svg>
            Filters
            <span class="count">{{isset($items) && ((!empty($filterParams) || empty($filterParams)) && $items->total() > 0) ? $items->total() : ""}}</span>
        </button>
    </div>
    
    <div class="filter form-block">
        <form method="post" action="{{url(LANG, ['admin', $currComponent->slug, 'filter'])}}" class="filter-form" id="filter-form">
            <div class="field-row-wrap">
                <div class="col middle">
                    <div class="with-bg">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="order_id">{{__("{$moduleName}::e.order_id")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="order_id" id="order_id"
                                    value="{{ !empty($filterParams) && array_key_exists('order_id', $filterParams) ? $filterParams['order_id'] : '' }}">
                            </div>
                        </div>
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="client_email">{{__("{$moduleName}::e.email")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="client_email" id="client_email"
                                    value="{{ !empty($filterParams) && array_key_exists('client_email', $filterParams) ? $filterParams['client_email'] : '' }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endif