@if(!empty($items))
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="">OrganizationId</th>
            <th class="left"> Name </th>
            <th class="left"> Address </th>
            <td></td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr>
                <td class="left" style="width: 300px;"> {{@$item['id']}} </td>
                <td class="left"> {{ @$item['name'] }}</td>
                <td class="left"> {{ @$item['restaurantAddress'] }}</td>
                <td class="left"><a href="{{adminUrl(['iiko', 'getOrderTypes', $item['id']])}}">OrderTypes</a></td>
                <td class="left"><a href="{{adminUrl(['iiko', 'getPaymentTypes', $item['id']])}}">PaymentTypes</a></td>
            </tr>
            @php
                $resp = $iikoClient->getTerminals($item['id']);
            @endphp
            @if(isset($resp['terminalGroups']) && !empty($resp['terminalGroups']))
                <tr>
                    <td colspan="3" class="left" style="padding-left: 50px;">
                        Terminals
                        <table>
                        @foreach($resp['terminalGroups'] as $titem)
                            @foreach($titem['items'] as $item2)
                                <tr>
                                    <td class="left" style="width: 300px;"> {{@$item2['id']}} </td>
                                    <td class="left"> {{ @$item2['name'] }}</td>
                                    <td class="left"> {{ @$item2['address'] }}</td>
                                </tr>
                            @endforeach
                        @endforeach
                        </table>
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
