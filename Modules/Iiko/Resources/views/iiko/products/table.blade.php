@if(!empty($items))
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th></th>
            <th class="left">IikoId</th>
            <th class="left"> Name </th>
            <td class="left">Type</td>
            <td class="left">Price</td>
            <td class="left">Group</td>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $k=>$item)
            <tr>
                <td style="width: 20px;">{{$k}}</td>
                <td class="left" style="width: 300px;"> {{@$item['iiko_id']}} </td>
                <td class="left" style="width: 240px;"> {{ @$item['name'] }}</td>
                <td class="left" style="width: 100px;"> {{ @$item['type'] }}</td>
                <td class="left"> {{ @$item['price'] }}</td>
                <td class="left"> {{ @$item->group->name }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
