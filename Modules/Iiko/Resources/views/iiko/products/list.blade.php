@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['action' => [], 'item' => null, 'actionButton'=>false ])
        <div class="table-block">
            <a href="{{adminUrl([$currComponent->slug, 'syncProducts'])}}" class="btn sync_iiko" style="margin: 10px;width: 160px;display: inline-block;">Syncronize Products</a>
            @include("{$path}.table", ['items' => $items, 'componentSlug' => $currComponent->slug])
        </div>
    </div>
@stop

@include('admin.footer')