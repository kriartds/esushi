@if(!empty($cities))
    <h3>Cities</h3>
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="">id</th>
            <th class="left"> name </th>
            <th class="left"> externalRevision </th>
            <th class="left">  </th>
            <th class="left">  </th>
            <th class="left"> isDeleted </th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($cities as $item)
            <tr><td colspan="4">OrganizationId: {{$item['organizationId']}}</td></tr>
            @foreach($item['items'] as $item2)
                <tr>
                    <td class="left" style="width: 300px;"> {{@$item2['id']}} </td>
                    <td class="left"> {{ @$item2['name'] }}</td>
                    <td class="left"> {{ @$item2['externalRevision'] }}</td>
                    <td class="left"> {{ @$item2['classifierId'] }}</td>
                    <td class="left"> {{ @$item2['additionalInfo'] }}</td>
                    <td class="left"> {{ $item2['isDeleted']? 'true':'false' }}</td>
                    <td class="left"><a href="{{adminUrl(['iiko', 'getStreets', $item['organizationId']])}}?city={{$item2['id']}}">Streets</a> </td>
                </tr>
            @endforeach
        @endforeach
        </tbody>
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
