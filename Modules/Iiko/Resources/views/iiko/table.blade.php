@if(!empty($items))
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="align-center">{{__("{$moduleName}::e.order_id")}}</th>
            <th class="left"> User</th>
            <th class="left"> Phone Number / {{__("{$moduleName}::e.email")}}</th>
            <th class="align-center">{{__("{$moduleName}::e.amount")}} / Payment method</th>
            <th class="align-center">{{__("{$moduleName}::e.order_type")}}</th>
            <th class="align-center">Delivery method</th>
            <th class="align-center">{{__("{$moduleName}::e.status")}}</th>
            <th class="align-center">{{__("{$moduleName}::e.date")}}</th>

            @if($permissions->delete)
                <th class="checkbox-all align-center">
                    <div>Check all</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}" @if(!$item->seen) class="new-order" @endif>
                <td class="id ">
                    <div class="row-order-status fast"></div>
                    @if($permissions->edit)
                        <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id])}}">{{$item->order_id}}</a>
                    @else
                        <p>{{@$item->order_id}}</p>
                    @endif
                </td>
                <td class="title left @if(!@$item->getUser->id) not-logged-user @endif"> @if(@$item->getUser->id) <a
                            href="{{adminUrl(['site-users','edit',$item->getUser->id])}}"> {{( (@$item->details->name) ? @$item->details->name : @$item->getUser->name) ?:'-'}}</a> @else {{((@$item->details->name) ? @$item->details->name : @$item->getUser->name) ?:'-'}}  @endif
                </td>
                <td class="left">
                    <p>{{@$item->phone}}</p>
                    <p>{{@$item->email}}</p>
                </td>
                <td class="align-center">{!! !is_null($item->total) ? formatPrice($item->total, @$item->order_currency->default, false) : '-' !!}
                    / {{$item->payment_method}}</td>

                <td class="order-type">
                    <div class="tooltip {{!$item->user_id && $item->is_draft ? 'draft' : (!$item->user_id ? 'admin' : (@$item->is_fast_buy ? 'fast' : 'simple'))}}"
                         title="{{ucfirst(!$item->user_id && $item->is_draft ? __("{$moduleName}::e.draft_order") : (!$item->user_id ? __("{$moduleName}::e.admin_order") : (@$item->is_fast_buy ? __("{$moduleName}::e.fast_order") : __("{$moduleName}::e.simple_order"))))}}"></div>
                </td> <!-- Class names for this div: draft, admin, fast, simple  -->

               @if (@$item->pickup_details)
                    <td class="align-center">Pickup</td>
                @else
                    <td class="align-center">{{@$item->shipping_details->name}}</td>
               @endif

                <td class="order-status">
                    <div class="{{$item->status}}">{{ucfirst($item->status)}}</div>
                    <!-- Class names for this div: checkout, cancelled, processing, approved, finished, failed -->
                </td>

                <td class="align-center">{{strtotime($item->order_date) ? date('d-m-Y H:i', strtotime($item->order_date)) : '-'}}</td>

                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}">
                        <label for="{{$item->id}}">
                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
