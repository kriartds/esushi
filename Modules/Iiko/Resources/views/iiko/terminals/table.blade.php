@if(!empty($terminalGroups))
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="">id</th>
            <th class="left"> name </th>
            <th class="left"> address </th>
            <th class="left">  </th>
        </tr>
        </thead>
        <tbody>
        @foreach($terminalGroups as $item)
            <tr><td colspan="4">OrganizationId: {{$item['organizationId']}}</td></tr>
            @foreach($item['items'] as $item2)
                <tr>
                    <td class="left" style="width: 300px;"> {{@$item2['id']}} </td>
                    <td class="left"> {{ @$item2['name'] }}</td>
                    <td class="left"> {{ @$item2['address'] }}</td>
                    <td class="left"> <a href="{{adminUrl(['iiko', 'terminals', $item2['id']])}}" target="_blank"> Show </a></td>
                </tr>
            @endforeach
        @endforeach
        </tbody>
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
