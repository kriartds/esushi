@if(!empty($items))
    <h3>Streets</h3>
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="">id</th>
            <th class="left" style="width: 300px;"> name </th>
            <th class="left"> Terminal </th>
            <th class="left"> ShippingZone </th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr>
                <td class="left" style="width: 300px;"> {{@$item['id']}} </td>
                <td class="left"> {{ @$item['name'] }}</td>
                <td class="left">
                    @if($iiko_terminals->isNotEmpty())
                        <select class="select2_ no-search select_terminal" data-href="{{adminUrl(['iiko-streets', 'changeTerminal', $item['id']])}}">
                            <option></option>
                            @foreach($iiko_terminals as $terminal)
                                <option value="{{$terminal->id}}" @if($terminal->id == $item->terminal_id) selected @endif>
                                    {{$terminal->name}} | {{$terminal->organization->name}}
                                </option>
                            @endforeach
                        </select>
                    @endif
                </td>
                <td class="left">
                    @if($shipping_zones->isNotEmpty())
                        <select class="select2_ no-search select_zone" data-href="{{adminUrl(['iiko-streets', 'changeZone', $item['id']])}}">
                            <option></option>
                            @foreach($shipping_zones as $shipping_zone)
                                <option value="{{$shipping_zone->id}}" @if($shipping_zone->id == $item->shipping_zone_id) selected @endif>{{$shipping_zone->name}}</option>
                            @endforeach
                        </select>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="2">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
