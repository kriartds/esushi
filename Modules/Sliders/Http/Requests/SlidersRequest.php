<?php

namespace Modules\Sliders\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class SlidersRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lang' => 'required',
            'name' => 'required',
            'slug' => "required|unique:sliders_id,slug,{$this->id}",
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        $data = [
            'status' => false,
            'validator' => true,
            'msg' => [
                'e' => $validator->messages(),
                'type' => 'error'
            ],
        ];

        throw new HttpResponseException(response()->json($data));
    }
}
