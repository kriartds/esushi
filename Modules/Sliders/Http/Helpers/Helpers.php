<?php

namespace Modules\Sliders\Http\Helpers;

use Modules\Sliders\Models\SlidersId;

class Helpers
{
    public static function getSliderBySlug($slug)
    {

        $item = SlidersId::where('active', 1)
            ->whereHas('parent', function ($q) use ($slug) {
                $q->where('slug', $slug);
            })
            ->orderBy('position')
            ->with(['globalName', 'file.file'])
            ->get();

        return $item;
    }
}