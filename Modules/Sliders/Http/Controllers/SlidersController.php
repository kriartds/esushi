<?php

namespace Modules\Sliders\Http\Controllers;


use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\Models\Languages;
use Modules\Sliders\Http\Requests\SlidersRequest;
use Modules\Sliders\Models\SlidersId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SlidersController extends DefaultController
{

    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'sliders';
        $this->path = "{$this->moduleName}::sliders";
    }

    public function index(Request $request)
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = SlidersId::where(function ($q) use ($request) {
            if ($request->get('parent'))
                $q->where('p_id', $request->get('parent'));
            else
                $q->whereNull('p_id');
        })
            ->orderBy('position')
            ->with(['globalName', 'items'])
            ->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.list", $response);

    }

    public function create(Request $request)
    {

        $parent = SlidersId::where('active', 1)
            ->whereNull('p_id')
            ->find($request->get('parent'));

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'parent' => $parent
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit(Request $request, $id, $langId)
    {

        $item = SlidersId::findOrFail($id);

        $parent = SlidersId::where('active', 1)
            ->whereNull('p_id')
            ->find($request->get('parent'));

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'langId' => $langId,
            'parent' => $parent
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = SlidersId::onlyTrashed()->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(SlidersRequest $request, $id, $langId)
    {

        if (is_null($id) && is_null($langId)) {
            $lang = Languages::where('active', 1)
                ->find($request->get('lang'));

            if (is_null($lang))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Lang not exist!'],
                        'type' => 'warning'
                    ]
                ]);

            $langId = $lang->id;
        }

        $parent = SlidersId::where('active', 1)
            ->whereNull('p_id')
            ->find($request->get('parent'));

        if (is_null($parent) && $request->get('parent'))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Parent not exist!'],
                    'type' => 'warning'
                ]
            ]);

        $item = SlidersId::find($id);

        if (is_null($item)) {
            $item = new SlidersId();

            $nextPosition = Helpers::getNextItemPosition($item);
            $item->position = $nextPosition;
            $item->active = 1;
        }


        $item->slug = $request->get('slug', null);
        $item->p_id = @$parent->id;

        $item->save();

        $item->itemByLang()->updateOrCreate([
            'slider_id' => $item->id,
            'lang_id' => $langId
        ], [
            'name' => $request->get('name', null),
            'description' => $request->get('description', null),
            'link' => $request->get('link', null)
        ]);

        Helpers::updateValidateDynamicField($request->all(), parent::currComponent(), $id, $langId);
        helpers()->uploadFiles($request->get('files'), $item->id, parent::currComponent()->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', parent::currComponent()->slug]) . (!is_null(@$parent->id) ? '?parent=' . @$parent->id : '')
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->globalName->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', parent::currComponent()->slug, 'edit', $item->id, $langId]) . (!is_null(@$parent->id) ? '?parent=' . @$parent->id : '')
            ]);
    }

    public function actionsCheckbox(Request $request)
    {

        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                SlidersId::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = SlidersId::whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = SlidersId::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->files()->delete();
                        $item->items()->delete();
                        $item->forceDelete();
                        helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = SlidersId::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'restored-from-trash');
                        $item->restore();
                    }
                }
                break;
            default:
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function updatePosition(Request $request)
    {

        $order = $request->get('order');

        if (!is_array($order))
            return response()->json([
                'status' => false
            ]);

        $itemsPositions = [];
        foreach ($order as $id) {
            $item = SlidersId::find($id);
            $itemsPositions[] = @$item->position;
        }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($order as $key => $id) {
            SlidersId::where('id', $id)->update(['position' => $itemsPositions[$key]]);
        }

        return response()->json([
            'status' => true
        ]);
    }

    public function activateItem(Request $request)
    {

        $item = SlidersId::findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->globalName->name]);
            helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->globalName->name]);
            helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

}