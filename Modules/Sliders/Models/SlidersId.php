<?php

namespace Modules\Sliders\Models;

use App\Http\Controllers\Admin\PagesController;
use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SlidersId extends Model
{

    use SoftDeletes;
    use FilesRelation;

    protected $table = 'sliders_id';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'slug',
        'p_id',
        'active',
        'position',
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'sliders';
        self::$globalLangId = request()->segment(6, null);

    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function children()
    {
        return $this->hasMany('Modules\Sliders\Models\SlidersId', 'p_id', 'id');
    }

    public function parent()
    {
        return $this->hasMany(SlidersId::class, 'id', 'p_id');
    }

    public function parents()
    {
        return $this->hasMany('Modules\Sliders\Models\SlidersId', 'id', 'p_id');
    }

    public function recursiveChildren()
    {
        return $this->children()->with(['recursiveChildren', 'recursiveChildren.globalName', 'globalName'])->orderBy('position');
    }

    public function recursiveParents()
    {
        return $this->parents()->with(['recursiveParents', 'recursiveParents.globalName', 'globalName'])->orderBy('position');
    }

    public function globalName()
    {
        return $this->hasOne(Sliders::class, 'slider_id', 'id')->whereIn('sliders.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(sliders.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('Modules\Sliders\Models\Sliders', 'slider_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('Modules\Sliders\Models\Sliders', 'slider_id', 'id');
    }

}
