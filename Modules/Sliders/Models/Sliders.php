<?php

namespace Modules\Sliders\Models;

use Illuminate\Database\Eloquent\Model;

class Sliders extends Model
{
    protected $table = 'sliders';

    protected $fillable = [
        'slider_id',
        'lang_id',
        'name',
        'description',
        'link'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

}
