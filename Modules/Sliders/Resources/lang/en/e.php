<?php

return [

    'element_is_active' => 'The element :name is active!',
    'element_is_inactive' => 'The element :name is inactive!',

    'slug_table' => 'Slug',
    'title_table' => 'Title',
    'edit_table' => 'Edit',
    'active_table' => 'Status ',
    'position_table' => 'Position',
    'delete_table' => 'Delete',
    'reestablish_table' => 'Reestablish',
    'id_table' => 'ID',
    'list_is_empty' => 'List is empty!',
    'save_it' => 'Save',
    'lang' => 'Lang',
    'description' => 'Description',

    'parent' => 'Parent',
    'link' => 'Link',

];