<div class="form-menu slider-create">
    <form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
          id="{{!is_null($item) ? 'edit' : 'create'}}-form"
          enctype="multipart/form-data">
        <div class="create-menus">

            <div class="select-language">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="lang">{{__("{$moduleName}::e.lang")}}*</label>
                    </div>
                    <div class="field-wrap">
                        @if(!$langList->isEmpty())
                            <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                                @foreach($langList as $lang)
                                    <option value="{{$lang->id}}" {{ (is_null(@$langId) && $lang->id == LANG_ID ? 'selected' : $lang->id == @$langId ) ? 'selected' : ''}}>{{$lang->name ?: ''}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>
            </div>
            <div class="title-slug create-custom-slider">
                @if(request()->get('parent'))

                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="parent">{{__("{$moduleName}::e.parent")}}</label>
                        </div>
                        <div class="field-wrap">
                            <input type="hidden" name="parent" value="{{@$parent->id}}">
                            <input type="text" value="{{@$parent->globalName->name}}" disabled>
                        </div>
                    </div>
                @endif

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__("{$moduleName}::e.title_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$item->itemByLang->name}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="slug">{{__("{$moduleName}::e.slug_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="slug" id="slug" value="{{@$item->slug}}">
                    </div>
                </div>
                @if(!is_null(request()->get('parent')))
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="description">{{__("{$moduleName}::e.description")}}</label>
                        </div>
                        <div class="field-wrap">
            <textarea name="description" id="description"
                      data-type="ckeditor">{!! @$item->itemByLang->description !!}</textarea>
                        </div>
                    </div>
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="link">{{__("{$moduleName}::e.link")}}</label>
                        </div>
                        <div class="field-wrap">
                            <input name="link" id="link" value="{{@$item->itemByLang->link}}">
                        </div>
                    </div>
                    <div class="field-row">
                        @include('admin.templates.uploadFile', [
                            'item' => @$item,
                            'options' => [
                                'data-component-id' => $currComponent->id
                            ]
                        ])
                    </div>

                @endif
            </div>
        </div>
        <div class="button-div">
            <button class="btn full submit-form-btn"
                    data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
        </div>
    </form>

</div>