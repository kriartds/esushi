<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Modules\\Sliders\\Database\\Seeders\\SlidersDatabaseSeeder' => $baseDir . '/Database/Seeders/SlidersDatabaseSeeder.php',
    'Modules\\Sliders\\Http\\Controllers\\SlidersController' => $baseDir . '/Http/Controllers/SlidersController.php',
    'Modules\\Sliders\\Http\\Helpers\\Helpers' => $baseDir . '/Http/Helpers/Helpers.php',
    'Modules\\Sliders\\Http\\Requests\\SlidersRequest' => $baseDir . '/Http/Requests/SlidersRequest.php',
    'Modules\\Sliders\\Models\\Sliders' => $baseDir . '/Models/Sliders.php',
    'Modules\\Sliders\\Models\\SlidersId' => $baseDir . '/Models/SlidersId.php',
    'Modules\\Sliders\\Providers\\RouteServiceProvider' => $baseDir . '/Providers/RouteServiceProvider.php',
    'Modules\\Sliders\\Providers\\SlidersServiceProvider' => $baseDir . '/Providers/SlidersServiceProvider.php',
);
