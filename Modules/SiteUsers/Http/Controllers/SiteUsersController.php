<?php

namespace Modules\SiteUsers\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\File;
use Modules\CountriesRegion\Models\Countries;
use Modules\CountriesRegion\Models\Regions;
use Modules\SiteUsers\Http\Requests\SiteUsersRequest;
use Modules\SiteUsers\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\Products\Models\ProductsItemsId;

class SiteUsersController extends DefaultController
{

    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'siteusers';
        $this->path = "{$this->moduleName}::users";
    }

    public function index()
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', $this->globalSettings());

        $items = Users::paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.list", $response);

    }

    public function create()
    {

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id)
    {

        $item = Users::findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function save(Request $request, $id)
    {
        $currComponent = parent::currComponent();

        $countryId = $request->get('country', null);
        $regionId = $request->get('region', null);
        $password = $request->get('password', null);
        $repeatPassword = $request->get('repeat_password', null);
        $changePassword = $request->get('change_password', null) == 'on';

        $country = Countries::where('active', 1)->find($countryId);

        $regionRules = Rule::exists('regions', 'id')->where(function ($q) use ($request, $countryId, $regionId) {
            $q->where('id', $regionId);
            $q->where('country_id', $countryId);
            $q->where('active', 1);
        });

        $rules = [
            'name' => 'required|max:50',
            'email' => "required|email|max:40|unique:users,email,{$id}",
            'phone' => 'required|regex:/^((\+)(\d+\s?)?(\(\d+\)\s?)?)?(\d+\s?\-?)+(\d)?$/|max:20',
            'country' => 'required',
            'region' => [
                'required',
                $regionRules
            ],
            'city' => 'required|max:100',
            'address' => 'required|max:100',
            'zip_code' => "required",
        ];


        if ($request->get('change_password') == 'on' || is_null($id))
            $rules = array_merge($rules, [
                'password' => 'required|min:4',
                'repeat_password' => 'required|same:password',
            ]);

        $validator = Validator::make($request->all(), $rules);

        $validator->after(function ($validator) use ($request, $country, $countryId) {
            if (is_null($country) && !is_null($countryId))
                $validator->errors()->add('country', 'The selected country is invalid.');
        });

        if ($validator->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->errors(),
                    'type' => 'error'
                ],
            ]);

        $item = Users::find($id);

        if (is_null($item)) {
            $item = new Users();

            $item->active = 1;
        }


        $item->name = $request->get('name', null);
        $item->email = $request->get('email', null);
        $item->phone = $request->get('phone', null);
        $item->country_id = $countryId;
        $item->region_id = $regionId;
        $item->city = $request->get('city', null);
        $item->address = $request->get('address', null);
        $item->zip_code = $request->get('zip_code', null);
        $item->avatar = null;
        $item->for_logout = $changePassword && !is_null($id) ? 1 : 0;
        $item->provider = null;
        $item->provider_id = null;
        $item->accept_terms = 1;

        if ($changePassword || is_null($id))
            $item->password = bcrypt($request->get('password', null));

        $item->save();

        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item {$item->email} was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id])
            ]);
    }

    public function destroy(Request $request)
    {

        $deletedItemsId = substr($request->get('id'), 1, -1);

        if (empty($deletedItemsId))
            return response()->json([
                'status' => false
            ]);

        if ($request->get('event') != 'to-trash' && $request->get('event') != 'from-trash' && $request->get('event') != 'restore')
            return response()->json([
                'status' => false
            ]);

        $currComponent = parent::currComponent();
        $deletedItemsIds = explode(',', $deletedItemsId);

        $items = Users::whereIn('id', $deletedItemsIds)->with('productsReviews')->get();

        $cartMessage = $responseMsg = '';

        if (!$items->isEmpty()) {
            $usersIds = $items->pluck('id')->toArray();
            $products = ProductsItemsId::whereHas('reviews', function ($q) use ($usersIds) {
                $q->whereIn('user_id', $usersIds);
            })->get();

            foreach ($items as $item) {

                $cartMessage .= $item->email . ', ';

                if ($item->avatar && file_exists($item->avatar))
                    File::delete($item->avatar);

                $item->productsReviews()->forceDelete();
                $item->delete();
                $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' was deleted' : '';
                helpers()->logActions($currComponent, $item, $item->name, 'deleted');

            }

//                Update popularity rating for products

            if ($products->isNotEmpty()) {

                foreach ($products as $product) {
                    $productAvgRating = null;
                    $productReviews = $product->reviews()->get();

                    if ($productReviews->isNotEmpty())
                        $productAvgRating = round($productReviews->avg('rating'), 2);

                    $product->update([
                        'popularity_rating' => $productAvgRating
                    ]);
                }
            }

//                Update popularity rating for products

            return response()->json([
                'status' => true,
                'cart_messages' => $responseMsg,
                'items' => $deletedItemsIds
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                Users::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
            case 'delete-from-trash':
            case 'restore-from-trash':

            default:
                $items = Users::whereIn('id', $ItemsId)->with('productsReviews')->get();

                $cartMessage = $responseMsg = '';

                if (!$items->isEmpty()) {
                    $usersIds = $items->pluck('id')->toArray();
                    $products = ProductsItemsId::whereHas('reviews', function ($q) use ($usersIds) {
                        $q->whereIn('user_id', $usersIds);
                    })->get();

                    foreach ($items as $item) {
                        $cartMessage .= $item->email . ', ';
                        if ($item->avatar && file_exists($item->avatar))
                            File::delete($item->avatar);
                        $item->productsReviews()->forceDelete();
                        $item->delete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted');
                    }
//                Update popularity rating for products
                    if ($products->isNotEmpty()) {
                        foreach ($products as $product) {
                            $productAvgRating = null;
                            $productReviews = $product->reviews()->get();

                            if ($productReviews->isNotEmpty())
                                $productAvgRating = round($productReviews->avg('rating'), 2);

                            $product->update([
                                'popularity_rating' => $productAvgRating
                            ]);
                        }
                    }
//                Update popularity rating for products
                }
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function activateItem(Request $request)
    {
        $currComponent = parent::currComponent();
        $item = Users::findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->email]);
            helpers()->logActions($currComponent, $item, @$item->id, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->email]);
            helpers()->logActions($currComponent, $item, @$item->id, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function getCountries(Request $request)
    {

        if (!$request->ajax())
            abort(404);

        $items = Countries::where('active', 1)
            ->where(function ($q) use ($request) {
                $q->where('name', 'LIKE', "%{$request->get('q')}%");
                $q->orWhere('iso', 'LIKE', "%{$request->get('q')}%");
            })
            ->whereHas('regions', function ($q) {
                $q->where('active', 1);
            })
            ->orderBy('name')
            ->paginate(10);

        $response = selectAjaxSearchItems($request, $items, 'name', null);

        return $response;
    }

    public function getRegions(Request $request)
    {

        if (!$request->ajax())
            abort(404);

        $countriesId = json_decode($request->get('countryId', null));

        $items = Regions::where('active', 1)
            ->where('country_id', $countriesId)
            ->where('name', 'LIKE', "%{$request->get('q')}%")
            ->whereHas('country', function ($q) {
                $q->where('active', 1);
            })
            ->orderBy('name')
            ->paginate(10);

        $response = selectAjaxSearchItems($request, $items, 'name', null);

        return $response;
    }

//    Methods for menu
    public function widgetsCountItems()
    {
        return Users::count();
    }
//    Methods for menu

}
