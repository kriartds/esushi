<?php

namespace Modules\SiteUsers\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\CountriesRegion\Models\Countries;

class SiteUsersRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $countryId = request()->get('country', null);
        $regionId = request()->get('region', null);
        $password = request()->get('password', null);
        $repeatPassword = request()->get('repeat_password', null);
        $changePassword = request()->get('change_password', null) == 'on';

        $country = Countries::where('active', 1)->find($countryId);

        $regionRules = Rule::exists('regions', 'id')->where(function ($q) use ($request, $countryId, $regionId) {
            $q->where('id', $regionId);
            $q->where('country_id', $countryId);
            $q->where('active', 1);
        });

        $rules = [
            'name' => 'required|max:50',
            'email' => "required|email|max:40|unique:users,email,{$this->id}",
            'phone' => 'required|regex:/^((\+)(\d+\s?)?(\(\d+\)\s?)?)?(\d+\s?\-?)+(\d)?$/|max:20',
            'country' => 'required',
            'region' => [
                'required',
                $regionRules
            ],
            'city' => 'required|max:100',
            'address' => 'required|max:100',
            'zip_code' => "required",
        ];

        if (!is_null($country))
            $rules = array_merge($rules, [
                'zip_code' => "required|postal_code:{$country->iso}",
            ]);


        if ($changePassword || is_null($this->id) || ($password && !$repeatPassword) || (!$password && $repeatPassword))
            $rules = array_merge($rules, [
                'password' => 'required|min:4',
                'repeat_password' => 'required|same:password',
            ]);

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $data = [
            'status' => false,
            'validator' => true,
            'msg' => [
                'e' => $validator->messages(),
                'type' => 'error'
            ],
        ];

        throw new HttpResponseException(response()->json($data));
    }
}
