<?php

return [

    'element_is_active' => 'The element :name is active!',
    'element_is_inactive' => 'The element :name is inactive!',

    'title_table' => 'Title',
    'edit_table' => 'Edit',
    'active_table' => 'Status ',
    'delete_table' => 'Delete',
    'id_table' => 'ID',
    'list_is_empty' => 'List is empty!',
    'save_it' => 'Save',

    'name' => "Name",
    'email' => 'Email',
    'phone' => 'Phone',
    'country' => 'Country',
    'region' => 'Region',
    'city' => 'City',
    'address' => 'Address',
    'zip_code' => 'Zip code',
    'chose_country' => 'Chose country',
    'chose_region' => 'Chose region',
    'change_password' => 'Change password',
    'password' => 'Password',
    'repeat_password' => 'Repeat password',
    'provider' => 'Provider',
    'date' => 'Date',
    'avatar' => 'Avatar',
];