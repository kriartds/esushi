@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th>{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">Users</th>
            <th class="left">Phone Number / Email</th>
            <th class="left">Address</th>
            <th>{{__("{$moduleName}::e.provider")}}</th>
            <th>Status</th>
            <th>{{__("{$moduleName}::e.date")}}</th>
            <th class="checkbox-all align-center" >
                <div>Check all</div>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>
                <td class="medium left">
                    <a href="{{adminUrl([$currComponent->slug, 'edit', $item->id])}}" class="color-blue">{{$item->name}}</a>
                </td>

                <td class="left"><p>{{$item->phone ?: '-'}}</p> <p>{{$item->email ?: '-'}}</p> </td>
                <td class="left medium">
                    <div class="limit">
                        <span class="limit-character">{{@$item->country->name?@$item->country->name:''}} {{@$item->region->name?','.@$item->region->name:''}} {{$item->city?$item->city:''}} {{$item->address?$item->address:''}} {{$item->zip_code?$item->zip_code:''}} </span>
                    </div>
                </td>
                <td>{{$item->provider ? ucfirst($item->provider) : '-'}}</td>
                @if(!@$isTrash)
                    @if($permissions->active)
                        <td class="status">
                            <span class="activate-item {{$item->active ? 'active' : ''}}"
                                  data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                                  data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>

                        </td>
                    @endif
                @endif


{{--                @if($permissions->edit)--}}
{{--                    <td class="td-link">--}}
{{--                        <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id])}}">{{__("{$moduleName}::e.edit_table")}}</a>--}}
{{--                    </td>--}}
{{--                @endif--}}
{{--                @if($permissions->active)--}}
{{--                    <td>--}}
{{--                        <span class="activate-item {{$item->active ? 'active' : ''}}"--}}
{{--                              data-active="{{$item->active}}" data-item-id="{{$item->id}}"--}}
{{--                              data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>--}}
{{--                    </td>--}}
{{--                @endif--}}
                <td >{{strtotime($item->created_at) ? date('d-m-Y', strtotime($item->created_at)) : '-'}}</td>
                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}"

                               >
                        <label for="{{$item->id}}">
{{--                            <span>Select</span>--}}
                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
