<div class="platform-users">
    <form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
          id="{{!is_null($item) ? 'edit' : 'create'}}-form"
          enctype="multipart/form-data">
        <div class="background">
            <div class="containerForInput">
                <div class="flex">
                    @if(file_exists(@$item->avatar))
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="avatar">{{__("{$moduleName}::e.avatar")}}</label>
                            </div>
                            <div class="field-wrap">
                                <div class="avatar-block">
                                    <a href="{{asset(@$item->avatar)}}"
                                       data-fancybox="thumbnail">
                                        <img src="{{asset(@$item->avatar)}}"
                                             alt="{{str_slug(@$item->avatar->name)}}"
                                             title="{{@$item->avatar->name}}" class="tooltip">
                                    </a>
                                </div>
                            </div>
                        </div>

                    @endif
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="name">{{__("{$moduleName}::e.name")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <input name="name" id="name" value="{{@$item->name}}">
                        </div>
                    </div>

                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="email">{{__("{$moduleName}::e.email")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <input name="email" id="email" value="{{@$item->email}}">
                        </div>
                    </div>

                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="phone">{{__("{$moduleName}::e.phone")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <input name="phone" id="phone" value="{{@$item->phone}}">
                        </div>
                    </div>
                </div>

                <div class="flex">
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="country">{{__("{$moduleName}::e.country")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <select autocomplete="off" name="country" id="country"
                                    class="select2 ajax countries-list"
                                    data-url="{{customUrl(['admin', $currComponent->slug, 'getCountries'])}}">
                                @if(@$item->country)
                                    <option value="{{$item->country_id}}"
                                            selected>{{@$item->country->name}}</option>
                                @else
                                    <option value="">{{__("{$moduleName}::e.chose_country")}}</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="region">{{__("{$moduleName}::e.region")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <select class="select2 ajax regions-list" name="region" id="region"
                                    data-url="{{customUrl(['admin', $currComponent->slug,'getRegions']) . (@$item->country_id ? '?countryId=' . @$item->country_id : '')}}">
                                @if(@$item->region)
                                    <option value="{{@$item->region_id}}" selected>{{@$item->region->name}}</option>
                                @else
                                    <option value="">{{__("{$moduleName}::e.chose_region")}}</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="city">{{__("{$moduleName}::e.city")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <input name="city" id="city" value="{{@$item->city}}">
                        </div>
                    </div>

                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="address">{{__("{$moduleName}::e.address")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <input name="address" id="address" value="{{@$item->address}}">
                        </div>
                    </div>
                </div>

                <div class="field-row zip">
                    <div class="label-wrap">
                        <label for="zip_code">{{__("{$moduleName}::e.zip_code")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="zip_code" id="zip_code" value="{{@$item->zip_code}}">
                    </div>
                </div>


                <div class="field-row inline  white-bg">
                    <div class="label-wrap">
                        <label for="change-site-users-pass">{{__("{$moduleName}::e.change_password")}}</label>
                    </div>

                    <div class="checkbox-switcher">
                        <input type="checkbox" name="change_password" id="change-site-users-pass"  data-show-hidden-block-id="site-users-pass-block" class="showHideBlocks" >
                        <label for="change-site-users-pass"></label>
                    </div>
                </div>

                <div  id="site-users-pass-block" class="flex column hidden">
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="password">{{__("{$moduleName}::e.password")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <input type="password" name="password" id="password">
                        </div>
                    </div>

                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="repeat_password">{{__("{$moduleName}::e.repeat_password")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <input type="password" name="repeat_password" id="repeat_password">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <button class="btn submit-form-btn"
                data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
    </form>
</div>
