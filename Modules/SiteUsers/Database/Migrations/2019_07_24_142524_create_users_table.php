<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email');
            $table->string('password');
            $table->string('phone', 64)->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->unsignedInteger('region_id')->nullable();
            $table->string('city')->nullable();
            $table->string('address', 1024)->nullable();
            $table->string('zip_code', 10)->nullable();
            $table->string('avatar', 1024)->nullable();
            $table->rememberToken();
            $table->tinyInteger('active')->default(0);
            $table->tinyInteger('for_logout')->default(0);
//            $table->string('provider', 64)->nullable();
//            $table->string('provider_id')->nullable();
            $table->tinyInteger('accept_terms')->default(0);
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null')->onUpdate('no action');
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('set null')->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
