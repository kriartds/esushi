<?php

namespace Modules\SiteUsers\Models;

use Illuminate\Database\Eloquent\Model;

class UsersPasswordReset extends Model
{
    protected $table = 'user_password_reset';

    protected $hidden = [
        'reset_token', 'email'
    ];

    protected $fillable = [
        'reset_token',
        'email',
    ];

}
