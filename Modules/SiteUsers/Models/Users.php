<?php

namespace Modules\SiteUsers\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\CountriesRegion\Models\Countries;
use Modules\CountriesRegion\Models\Regions;
use Modules\Orders\Models\Cart;
use Modules\Products\Models\ProductsItemsReviews;
use Modules\Subscribers\Models\Subscribers;

class Users extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'is_check_phone',
        'city',
        'address',
        'avatar',
        'remember_token',
        'active',
        'for_logout',
        'accept_terms',
        'user_ladder',
        'user_apartment',
        'user_floor',
        'user_inter_phone',
        'user_region',
    ];

    protected $hidden = [
        'password', 'remember_token',  'for_logout'
    ];

    public function orders()
    {
        return $this->hasMany(Cart::class, 'auth_user_id', 'id');
    }

    public function subscribe()
    {
        return $this->hasOne(Subscribers::class, 'email', 'email');
    }

    public function productsReviews()
    {
        return $this->hasMany(ProductsItemsReviews::class, 'user_id', 'id');
    }

    public function providers()
    {
        return $this->hasMany(UsersProviders::class, 'user_id', 'id');
    }

    public function getOrdersMoneySpentAttribute()
    {

        return $this->orders()->notIsInProgress()
            ->get()
            ->sum(function ($item) {
                return $item->total;
            });
    }
}
