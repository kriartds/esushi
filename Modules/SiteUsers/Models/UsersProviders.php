<?php

namespace Modules\SiteUsers\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UsersProviders extends Model
{
    use Notifiable;

    protected $table = 'users_providers';

    protected $fillable = [
        'user_id',
        'provider',
        'provider_id',
    ];

    public function user()
    {
        return $this->hasOne(Users::class, 'id', 'user_id');
    }

}
