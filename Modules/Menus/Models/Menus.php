<?php

namespace Modules\Menus\Models;

use App\Http\Controllers\Admin\PagesController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menus extends Model
{

    protected $table = 'menus';

    protected $fillable = [
        'menu_id',
        'lang_id',
        'name',
        'url'
    ];

    public function setNameAttribute($value)
    {

        $this->attributes['name'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

}
