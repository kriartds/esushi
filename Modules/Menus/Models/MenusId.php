<?php

namespace Modules\Menus\Models;

use App\Http\Helpers\Helpers;
use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;

class MenusId extends Model
{

    use FilesRelation;

    protected $table = 'menus_id';

    protected $fillable = [
        'menus_type_id',
        'p_id',
        'component_id',
        'item_id',
        'position',
        'is_direct_update',
        'active',
        'classes'
    ];

    public static $globalLangId;

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        self::$component = 'menu-items';
        self::$globalLangId = !is_null(request()->get('langId')) ? (int)request()->get('langId') : LANG_ID;
    }

    public function children()
    {
        return $this->hasMany('Modules\Menus\Models\MenusId', 'p_id', 'id')->orderBy('position');
    }

    public function recursiveChildren()
    {
        return $this->children()->with('recursiveChildren')->orderBy('position');
    }

    public function globalName()
    {
        return $this->hasOne(Menus::class, 'menu_id', 'id')->whereIn('menus.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(menus.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('Modules\Menus\Models\Menus', 'menu_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('Modules\Menus\Models\Menus', 'menu_id', 'id');
    }

    public function type()
    {
        return $this->hasOne('Modules\Menus\Models\MenusTypes', 'id', 'menus_type_id');
    }

    public function componentId()
    {
        return $this->hasOne('App\Models\ComponentsId', 'id', 'component_id');
    }

    public function componentByLang()
    {
        return $this->hasOne('App\Models\Components', 'component_id', 'component_id')->where('lang_id', self::$globalLangId);
    }

    public function getCurrNameUrlAttribute()
    {
        $currName = $currUrl = null;
        $item = $this;

        $itemGlobalName = $item->globalName;

        if (!is_null($itemGlobalName) && !is_null(@$itemGlobalName->name)) {
            $currName = @$itemGlobalName->name;

            if (substr(@$itemGlobalName->url, 0, 7) == "http://" || substr(@$itemGlobalName->url, 0, 8)) {
                $currUrl = @$itemGlobalName->url;
            } else {
                $currUrl = customUrl(@$itemGlobalName->url);
            }
        }

        if (!is_null($item->component_id)) {
            $component = $item->componentId;

            if (!is_null($component) && $component->for_menu) {

                if (is_null($item->item_id)) {
                    if (is_null($currName))
                        $currName = @$component->globalName->name;
                    $currUrl = customUrl($component->slug);
                }
            }
        }

        return (object)[
            'currName' => $currName,
            'currUrl' => $currUrl
        ];
    }
}
