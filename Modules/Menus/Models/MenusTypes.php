<?php

namespace Modules\Menus\Models;

use App\Http\Controllers\Admin\PagesController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenusTypes extends Model
{

    use SoftDeletes;

    protected $table = 'menus_types';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'slug',
        'active',
    ];

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

    public function children()
    {
        return $this->hasMany('Modules\Menus\Models\MenusId', 'menus_type_id', 'id');
    }

    public function getHasChildrenAttribute()
    {
        return $this->children->isNotEmpty();
    }

}
