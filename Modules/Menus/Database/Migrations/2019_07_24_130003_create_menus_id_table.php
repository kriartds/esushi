<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus_id', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('menus_type_id');
            $table->unsignedInteger('p_id')->nullable();
            $table->unsignedInteger('component_id')->nullable();
            $table->unsignedInteger('item_id')->nullable();
            $table->string('classes')->nullable();
            $table->integer('position')->default(0);
            $table->tinyInteger('is_direct_update')->default(0);
            $table->tinyInteger('active')->default(1);
            $table->timestamps();

            $table->foreign('menus_type_id')->references('id')->on('menus_types')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('component_id')->references('id')->on('components_id')->onDelete('cascade')->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus_id');
    }
}
