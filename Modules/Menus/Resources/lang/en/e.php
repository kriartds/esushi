<?php

return [

    'element_is_active' => 'The element :name is active!',
    'element_is_inactive' => 'The element :name is inactive!',

    'slug_table' => 'Slug',
    'title_table' => 'Title',
    'edit_table' => 'Edit',
    'active_table' => 'Status ',
    'delete_table' => 'Delete',
    'reestablish_table' => 'Reestablish',
    'id_table' => 'ID',
    'list_is_empty' => 'List is empty!',
    'save_it' => 'Save',
    'lang' => 'Lang',
    'components' => 'Components',
    'pages' => 'Pages',

    'menus_types' => 'Menu types',
    'menu_structure' => 'Menu Structure',
    'add_items_from_left_column' => 'Add menu items from the column on the left.',
    'custom_links' => 'Custom links',
    'name' => 'Name',
    'url' => 'Url',
    'class_names' => 'Class names',
    'item_has_children' => 'This item has children!',

];