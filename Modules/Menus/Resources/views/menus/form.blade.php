<div class="form-menu">
    <form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
          id="{{!is_null($item) ? 'edit' : 'create'}}-form"
          class="menu-constructor-form"
          enctype="multipart/form-data">
        <div class="create-menus">
            <div class="title-slug">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__("{$moduleName}::e.title_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$item->name}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="slug">{{__("{$moduleName}::e.slug_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="slug" id="slug" value="{{@$item->slug}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="button-div">
            <button class="btn full submit-form-btn" data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
        </div>
    </form>
</div>
