@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container menu-container">
        <?php
            $component = \App\Models\ComponentsId::where('slug', 'menus')->first();
        ?>
        {!! helpers()->getAdminBreadcrumbs($component, new \Modules\Menus\Models\MenusTypes(), request()->get('parent') ) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list'], 'langId' => @$langId])

        <div class="form-block-wrap with-menu-sidebar">

            <div class="menu-sidebar">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="custom-links-name">{{__("{$moduleName}::e.custom_links")}}</label>
                    </div>
                    <div class="field-wrap">
                        <div class="inputs">
                            <input type="text" class="custom-links" name="custom-links[name]" id="custom-links-name"
                                   placeholder="Name">
                            <input type="text" class="custom-links" name="custom-links[url]" id="custom-links-url"
                                   placeholder="Url">
                        </div>
                        <span class="add-to-menu-block tooltip" title="Add to menu"
                              data-url="{{url(LANG, ['admin', $currComponent->slug, 'addMenuItems'])}}"></span>
                    </div>
                </div>
                @if(!empty($menuComponents))
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="menu-components">{{__("{$moduleName}::e.components")}}</label>
                        </div>
                        <div class="field-wrap">
                            <select autocomplete="off" name="menu_components[]" id="menu-components" class="select2"
                                    multiple>
                                @foreach($menuComponents as $menuComponent)
                                    @if($menuComponent->slug != 'pages')
                                        <option value="{{$menuComponent->id}}">{{@$menuComponent->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <span class="add-to-menu-block tooltip" title="Add to menu"
                                  data-url="{{url(LANG, ['admin', $currComponent->slug, 'addMenuItems'])}}"></span>
                        </div>
                    </div>
                    @foreach($menuComponents as $key => $menuComponent)
                        @if(!is_null($menuComponent->elements) && !$menuComponent->elements->isEmpty())
                            <div class="field-row">
                                <div class="label-wrap">
                                    <label for="{{$menuComponent->slug . '-' . $menuComponent->id}}"
                                           data-category-id="{{$menuComponent->id}}">{{$menuComponent->name}}</label>
                                </div>
                                <div class="field-wrap">
                                    <select autocomplete="off" name="{{$menuComponent->slug}}[]"
                                            id="{{$menuComponent->slug . '-' . $menuComponent->id}}" class="select2"
                                            multiple>
                                        @foreach($menuComponent->elements as $element)
                                            <option value="{{$element->id}}">{{@$element->globalName->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="add-to-menu-block tooltip" title="Add to menu"
                                          data-url="{{url(LANG, ['admin', $currComponent->slug, 'addMenuItems'])}}"></span>
                                </div>
                            </div>
                        @endif
                        @if(!is_null($menuComponent->categories) && !$menuComponent->categories->isEmpty())
                            <div class="field-row">
                                <div class="label-wrap">
                                    <label for="{{$menuComponent->slug . '-category-' . $menuComponent->id}}"
                                           data-category-id="{{$menuComponent->id}}">{{$menuComponent->name}} -
                                        categories</label>
                                </div>
                                <div class="field-wrap">
                                    <select autocomplete="off" name="{{$menuComponent->slug . '_category'}}[]"
                                            id="{{$menuComponent->slug . '-category-' . $menuComponent->id}}"
                                            class="select2" multiple>
                                        {!! getParentChildrenInSelect($menuComponent->categories) !!}
                                    </select>
                                    <span class="add-to-menu-block tooltip" title="Add to menu"
                                          data-url="{{url(LANG, ['admin', $currComponent->slug, 'addMenuItems'])}}"></span>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif

            </div>

            <div class="form-block with-menu-sidebar">
                @include("{$path}.form")
            </div>
        </div>
        <div class="m-left">
            <button class="btn left medium submit-form-btn"
                    data-form-id="create-edit-form">{{__("{$moduleName}::e.save_it")}}</button>
        </div>
    </div>

@stop

@include('admin.footer')

