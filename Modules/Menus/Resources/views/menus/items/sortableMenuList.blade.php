@if(isset($item))
    <li class="sortable-menu-item" data-id="{{$item->item_id}}" data-category-id="{{$item->component_id}}" data-name="{{$item->is_direct_update || is_null($item->component_id) ? @$item->itemByLang->name : ''}}"
        data-url="{{@$item->itemByLang->url}}" data-menu-id="{{$item->id}}" data-menu-classes="{{@$item->classes}}" data-menu-file>
        @include("${path}.sortableMenuItem")
@endif