<form method="POST"
      action="{{url(LANG, ['admin', $currComponent->slug, 'save']) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}"
      id="create-edit-form"
      class="menu-constructor-form"
      enctype="multipart/form-data">
    <div class="field-row">
        <div class="label-wrap">
            <label for="lang">{{__("{$moduleName}::e.lang")}}*</label>
        </div>
        <div class="field-wrap">
            @if(!$langList->isEmpty())
                <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                    @foreach($langList as $lang)
                        <option value="{{$lang->id}}" {{!is_null(request()->get('langId')) ? ($lang->id == request()->get('langId') ? 'selected' : '') : ($lang->id == LANG_ID ? 'selected' : '')}}>{{$lang->name ?: ''}}</option>
                    @endforeach
                </select>
            @endif
        </div>
    </div>
    <div class="field-row">
        <div class="label-wrap">
            <label for="menu-type">{{__("{$moduleName}::e.menus_types")}}*</label>
        </div>
        <div class="field-wrap">
            @if(!$menuTypes->isEmpty())
                <select autocomplete="off" name="menu_type" id="menu-type" class="select2 no-search">
                    @foreach($menuTypes as $menuType)
                        <option value="{{$menuType->id}}" {{$menuType->id == request()->get('parent') || $menuType->id == $firstMenuType->id ? 'selected' : ''}}>{{$menuType->name ?: ''}}</option>
                    @endforeach
                </select>
            @endif
        </div>
    </div>
    <div class="title">{{__("{$moduleName}::e.menu_structure")}}</div>
    <div class="empty-menu-list {{!$items->isEmpty() ? 'hidden' : ''}}">{{__("{$moduleName}::e.add_items_from_left_column")}}</div>
    <div class="menu-constructor">
        <div class="sortable-menu" id="menu-constructor">
            <ul class="sortable-menu-list">
                {!! getSortableMenu($path, $moduleName, $items) !!}
            </ul>
        </div>
    </div>
    {{--    <button class="btn left medium submit-form-btn" data-form-id="create-edit-form">{{__("{$moduleName}::e.save_it")}}</button>--}}
</form>
