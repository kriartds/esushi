@if(isset($item))
    @if(!@$isNewItems)
        <div class="sortable-menu-handle {{!$item->active ? 'inactive' : ''}}">
            <span class="item-header">

{{--                <span class="item-title">{{!is_null($item->currentName) ? $item->currentName : @helpers()->getComponentItem($item->componentId, $item->item_id)->item->name}}</span>--}}
                <span class="item-title">{{getMenuCurrName($item)}}</span>
                <span class="item-type">{{!is_null(@$item->componentId->globalName->name) ? $item->componentId->globalName->name : __("{$moduleName}::e.custom_links")}}</span>
                <span class="edit-item"></span>
                <span class="remove-item" data-url="{{url(LANG, ['admin', $currComponent->slug, 'destroyMenuItem'])}}">Remove</span>
            </span>
        </div>
        <div class="item-settings hidden">
            <div class="field-row">
                <div class="label-wrap">
                    <label for="{{!is_null(@$item->componentId->slug) ? @$item->componentId->slug : 'custom-link'}}-{{$item->id}}">{{__("{$moduleName}::e.name")}}{{is_null(@$item->componentId) ? '*' : ''}}</label>
                </div>
                <div class="field-wrap">
                    @if(!is_null(@$item->componentId))
                        <input type="hidden" name="menus[simple][id][]" value="{{@$item->item_id}}">
                    @endif
                    <input type="text" name="menus[{{!is_null(@$item->componentId) ? 'simple' : 'custom'}}][name][]"
                           id="{{!is_null(@$item->componentId->slug) ? @$item->componentId->slug : 'custom-link'}}-{{$item->id}}"
                           value="{{$item->is_direct_update || is_null($item->component_id) ? @$item->itemByLang->name : ''}}"
                           class="menu-fields-constructor" data-field="name">
                </div>
            </div>
            @if(is_null(@$item->componentId))
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="custom-link-{{$item->id}}">{{__("{$moduleName}::e.url")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input type="hidden" name="menus[custom][id][]" value="{{@$item->item_id}}">
                        <input type="text" name="menus[custom][url][]" id="custom-link-{{$item->id}}"
                               value="{{@$item->itemByLang->url}}"
                               class="menu-fields-constructor" data-field="url">
                    </div>
                </div>
            @endif
            <div class="field-row">
                <div class="label-wrap">
                    <label for="{{!is_null(@$item->componentId->slug) ? @$item->componentId->slug : 'custom-link'}}-{{$item->id}}-class-names">{{__("{$moduleName}::e.class_names")}}</label>
                </div>
                <div class="field-wrap">
                    <input type="text" name="menus[{{!is_null(@$item->componentId) ? 'simple' : 'custom'}}][classes][]"
                           id="{{!is_null(@$item->componentId->slug) ? @$item->componentId->slug : 'custom-link'}}-{{$item->id}}-class-names"
                           value="{{@$item->classes}}" class="menu-fields-constructor" data-field="menu-classes">
                </div>
            </div>
            <div class="field-row">
                @include('admin.templates.uploadFile', [
                    'item' => @$item,
                    'options' => [
                        'data-component-id' => $currComponent->id,
                        'data-menu-field' => 'menu-file',
                        'data-single-upload' => 'true'
                    ]
                ])
            </div>
        </div>
    @else
        <li class="sortable-menu-item" data-id="{{!@$isMenuComponent ? @$item['value'] : ''}}" data-category-id="{{@$isMenuComponent ? @$item['value'] : @$categoryId}}"
            data-name="{{@$item['url'] ? @$item['name'] : ''}}"
            data-url="{{@$item['url'] ? @$item['url'] : ''}}" data-menu-id data-menu-classes data-menu-file>
            <div class="sortable-menu-handle">
            <span class="item-header">
                <span class="item-title">{{@$item['name']}}</span>
                <span class="item-type">{{@$item['category']}}</span>
                <span class="edit-item"></span>
                <span class="remove-item"></span>
            </span>
            </div>
            <div class="item-settings hidden">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name-{{@$item['value'] ? @$item['value'] : (@$iteration * (-1))}}-{{str_slug(@$item['category'])}}">{{__("{$moduleName}::e.name")}}{{@$item['url'] ? '*' : ''}}</label>
                    </div>
                    <div class="field-wrap">
                        @if(!@$item['url'])
                            <input type="hidden" name="menus[simple][id][]"
                                   value="{{@$item['value'] ? @$item['value'] : (@$iteration * (-1))}}">
                        @endif
                        <input type="text" name="menus[{{!@$item['url'] ? 'simple' : 'custom'}}][name][]"
                               id="name-{{@$item['value'] ? @$item['value'] : (@$iteration * (-1))}}-{{str_slug(@$item['category'])}}"
                               value="{{@$item['url'] ? @$item['name'] : ''}}"
                               class="menu-fields-constructor" data-field="name">
                    </div>
                </div>
                @if(@$item['url'])
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="custom-link-{{@$item['value'] ? @$item['value'] : (@$iteration * (-1))}}">{{__("{$moduleName}::e.url")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <input type="hidden" name="menus[custom][id][]"
                                   value="{{@$item['value'] ? @$item['value'] : (@$iteration * (-1))}}">
                            <input type="text" name="menus[custom][url][]"
                                   id="custom-link-{{@$item['value'] ? @$item['value'] : (@$iteration * (-1))}}"
                                   value="{{@$item['url'] ? @$item['url'] : (@$iteration * (-1))}}"
                                   class="menu-fields-constructor" data-field="url">
                        </div>
                    </div>
                @endif
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="class-names-{{@$item['value'] ? @$item['value'] : (@$iteration * (-1))}}-{{str_slug(@$item['category'])}}">{{__("{$moduleName}::e.class_names")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input type="text" name="menus[{{!@$item['url'] ? 'simple' : 'custom'}}][classes][]"
                               id="class-names-{{@$item['value'] ? @$item['value'] : (@$iteration * (-1))}}-{{str_slug(@$item['category'])}}"
                               value="" class="menu-fields-constructor" data-field="menu-classes">
                    </div>
                </div>
                <div class="field-row">
                    @include('admin.templates.uploadFile', [
                        'item' => null,
                        'options' => [
                            'data-component-id' => $currComponent->id,
                            'data-menu-field' => 'menu-file',
                            'data-single-upload' => 'true'
                        ]
                    ])
                </div>
            </div>
        </li>
    @endif
@endif