@if(!$items->isEmpty())
    <table class="table menus-informations"
           data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="id-center">{{__("{$moduleName}::e.id_table")}}</th>
            <th class="width-row">{{__("{$moduleName}::e.title_table")}}</th>
            <th>Slug</th>
            <th>Items in menu</th>
            <th>Status</th>
            <th class="checkbox-all align-center">
                <div>Check all</div>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="p-center">
                    <p>{{$item->id}}</p>
                </td>
                <td class="title width-row">
                    <a @if($permissions->edit)href="{{url(LANG, ['admin', 'menus','edit',$item->id])}} @endif">{{@$item->name}}</a>
                </td>
                <td class="width-row">
                    <span>{{$item->slug}}</span>
                </td>
                <td class="width-row-item">
                    <a href="{{url(LANG, ['admin', 'menu-items']) . "?parent={$item->id}"}}"
                       class="color">{{@$item->children->count()}} items</a>
                </td>

                <td class="status">
                            <span class="activate-item {{$item->active ? 'active' : ''}}"
                                  data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                                  data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>

                </td>
                <td class="checkbox-items">
                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                           name="checkbox_items[{{$item->id}}]" value="{{$item->id}}"
                           data-event="{{@$isTrash ? 'from' : 'to'}}-trash">
                    <label for="{{$item->id}}">

                    </label>
                </td>
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
