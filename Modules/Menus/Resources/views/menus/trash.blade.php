@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent, new \Modules\Menus\Models\Menus(),@request()->get('parent')) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'trash'],'currentPage' => 'trash', 'item' => null])

        <div class="table-block">
            @if(!$items->isEmpty())
                <table class="table menus-informations trash-menus">
                    <thead>
                    <tr>
                        <th class="id-center">{{__("{$moduleName}::e.id_table")}}</th>
                        <th class="width-row">{{__("{$moduleName}::e.title_table")}}</th>
                        <th>Slug</th>
                        <th class="checkbox-all align-center" >
                            <div>Check all</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}" >
                            <td class="p-center">
                                <p>{{$item->id}}</p>
                            </td>
                            <td class="width-row">{{@$item->name}}</td>
                            <td class="width-row">
                                <span>{{$item->slug}}</span>
                            </td>
                                <td class="checkbox-items">
                                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                           name="checkbox_items[{{$item->id}}]"
                                           value="{{$item->id}}">
                                    <label for="{{$item->id}}">
                                    </label>
                                </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')