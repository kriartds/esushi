<?php

namespace Modules\Menus\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class MenusItemsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];


        if (is_null(request()->get('menus')) || empty(request()->get('menus')))
            throw new HttpResponseException(response()->json([
                'status' => false,
                'msg' => [
                    'e' => 'The menu can have minim one item!',
                    'type' => 'error'
                ],
            ]));

        if (array_key_exists('custom', request()->get('menus')) && array_key_exists('url', request()->get('menus')['custom']) && !is_null(request()->get('menus')['custom']) && !empty(request()->get('menus')['custom'])) {
            foreach (request()->get('menus')['custom']['name'] as $key => $val) {
                $rules['menus.custom.name.' . $key] = 'required';
            }

            foreach (request()->get('menus')['custom']['url'] as $key => $val) {
                $rules['menus.custom.url.' . $key] = 'required';
            }
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        $msg = [];
        if (array_key_exists('custom', request()->get('menus')) && array_key_exists('url', request()->get('menus')['custom']) && !is_null(request()->get('menus')['custom']) && !empty(request()->get('menus')['custom'])) {
            foreach (request()->get('menus')['custom']['name'] as $key => $val) {
                $msg['menus.custom.name.' . $key . '.required'] = 'Menu name is required';
            }

            foreach (request()->get('menus')['custom']['url'] as $key => $val) {
                $msg['menus.custom.url.' . $key . '.required'] = 'Menu url is required';
            }
        }
        return $msg;
    }

    protected function failedValidation(Validator $validator)
    {
        $data = [
            'status' => false,
            'validator' => true,
            'msg' => [
                'e' => $validator->messages(),
                'type' => 'error'
            ],
        ];

        throw new HttpResponseException(response()->json($data));
    }
}
