<?php

namespace Modules\Menus\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use Modules\Menus\Http\Requests\MenusRequest;
use Modules\Menus\Models\MenusTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MenusController extends DefaultController
{

    private $moduleName;
    private $path;

    public function __construct()
    {


        $this->moduleName = 'menus';
        $this->path = "{$this->moduleName}::menus";
    }

    public function index()
    {

        $items = MenusTypes::get();

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.list", $response);

    }

    public function create()
    {

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id, $langId)
    {

        $item = MenusTypes::findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'langId' => $langId,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = MenusTypes::onlyTrashed()->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(MenusRequest $request, $id, $langId)
    {
        $item = MenusTypes::find($id);

        if (is_null($item)) {
            $item = new MenusTypes();

            $item->active = 1;
        }

        $item->name = $request->get('name', null);
        $item->slug = $request->get('slug', null);

        $item->save();

        helpers()->logActions(parent::currComponent(), $item, @$item->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', parent::currComponent()->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', parent::currComponent()->slug, 'edit', $item->id, $langId])
            ]);
    }

    public function destroy(Request $request)
    {

        $deletedItemsId = substr($request->get('id'), 1, -1);

        if (empty($deletedItemsId))
            return response()->json([
                'status' => false
            ]);

        if ($request->get('event') != 'to-trash' && $request->get('event') != 'from-trash' && $request->get('event') != 'restore')
            return response()->json([
                'status' => false
            ]);

        $deletedItemsIds = explode(',', $deletedItemsId);

        if ($request->get('event') == 'to-trash') {
            $items = MenusTypes::whereIn('id', $deletedItemsIds)->get();
        } else {
            $items = MenusTypes::onlyTrashed()->whereIn('id', $deletedItemsIds)->get();
        }

        $cartMessage = $responseMsg = '';


        return response()->json([
            'status' => false
        ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                MenusTypes::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = MenusTypes::whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        if ($item->hasChildren)
                            return response()->json([
                                'status' => false
                            ]);
                        $item->delete();
                        helpers()->logActions(parent::currComponent(), $item, $item->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = MenusTypes::onlyTrashed()->whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->children()->delete();
                        $item->forceDelete();
                        helpers()->logActions(parent::currComponent(), $item, $item->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = MenusTypes::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions(parent::currComponent(), $item, $item->name, 'restored-from-trash');
                    }
                }
                break;
            default:
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }


    public function activateItem(Request $request)
    {

        $item = MenusTypes::findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->name]);
            helpers()->logActions(parent::currComponent(), $item, @$item->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->name]);
            helpers()->logActions(parent::currComponent(), $item, @$item->name, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

}