<?php

namespace Modules\Menus\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Models\Languages;
use Modules\Menus\Http\Helpers\Helpers;
use Modules\Menus\Http\Requests\MenusItemsRequest;
use Modules\Menus\Models\MenusId;
use Modules\Menus\Models\MenusTypes;
use Modules\Pages\Models\PagesId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MenuItemsController extends DefaultController
{

    private $moduleName;
    private $path;

    public function __construct()
    {
       
        $this->moduleName = 'menus';
        $this->path = "{$this->moduleName}::menus.items";
    }

    public function index(Request $request)
    {

        $menuTypes = MenusTypes::where('active', 1)->get();

        if (!is_null($request->get('parent')))
            $menuType = $menuTypes->find($request->get('parent'));
        else
            $menuType = $menuTypes->first();

        $items = collect();

        if (!is_null($menuType)) {

            $items = MenusId::where('menus_type_id', $menuType->id)
                ->whereNull('p_id')
                ->with(['globalName', 'files', 'recursiveChildren', 'recursiveChildren.globalName', 'itemByLang', 'recursiveChildren.itemByLang', 'componentId', 'componentId.globalName', 'componentByLang', 'recursiveChildren.componentId', 'recursiveChildren.componentId.globalName', 'recursiveChildren.componentByLang', 'children' => function ($q) use ($menuType) {
                    $q->where('menus_type_id', $menuType->id);
                }])
                ->orderBy('position')
                ->get();
        }

        $menuComponents = helpers()->getComponentsWithItems();

//        $pages = PagesId::where('active', 1)->with(['globalName'])->get();

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'menuTypes' => $menuTypes,
            'firstMenuType' => $menuType,
//            'pages' => $pages,
            'menuComponents' => $menuComponents,
        ];

        return view("{$this->path}.createEdit", $response);

    }

    public function save(MenusItemsRequest $request)
    {

        $currComponent = parent::currComponent();
        $menuItems = json_decode($request->get('menuTree'));
        $lang = Languages::where('active', 1)
            ->find($request->get('lang'));

        if (is_null($lang))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Lang not exist!'],
                    'type' => 'warning'
                ]
            ]);

        $menuType = MenusTypes::where('active', 1)->find($request->get('menu_type'));

        if (is_null($menuType))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Menu type not exist!'],
                    'type' => 'warning'
                ]
            ]);

        $createRecursiveMenu = Helpers::createRecursiveMenuItems($request, $currComponent, $menuItems, $menuType, $lang);

        if (!$createRecursiveMenu || !is_array($createRecursiveMenu))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => "Menu doesn't updated!",
                    'type' => 'warning'
                ],
            ]);

        $menuItemsArr = objectToArray($createRecursiveMenu);

        array_walk_recursive($menuItemsArr, function ($value, $key) use (&$menuItemsIds) {
            if ($key == 'menuId')
                $menuItemsIds[] = $value;
        });

        Helpers::updateMenuItemsPosition($menuItemsIds);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => "Menu was successful updated!",
                'type' => 'success'
            ],
            'redirect' => url(LANG, ['admin', $currComponent->slug]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')
        ]);
    }

    public function destroyMenuItem(Request $request)
    {

        $deletedItemsId = $request->get('id');

        $item = MenusId::with(['items', 'recursiveChildren'])->find($deletedItemsId);

        if (is_null($item))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => 'Item not exist',
                    'type' => 'warning'
                ]
            ]);

        $item->items()->delete();
        recursiveChildrenDestroy($item);
        $item->delete();

        helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'deleted');

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => "Item was removed",
                'type' => 'info'
            ]
        ]);
    }

    public function addMenuItems(Request $request)
    {
        $items = $request->get('items', []);

        if (empty($items))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => 'Select minim one item',
                    'type' => 'warning'
                ]
            ]);

        $view = '';

        $isMenuComponent = $request->get('isMenuComponent') === 'true';

        foreach ($items as $key => $item) {
            try {
                $response = [
                    'item' => $item,
                    'isNewItems' => true,
                    'iteration' => $key,
                    'isMenuComponent' => $isMenuComponent,
                    'categoryId' => $request->get('categoryId', null),
                    'moduleName' => $this->moduleName,
                ];
                $view .= view("{$this->path}.sortableMenuItem", $response)->render();
            } catch (\Throwable $e) {
                $view .= '';
            }
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => (count($items) == 1 ? "Item " : "Items ") . "was added",
                'type' => 'info'
            ],
            'view' => $view
        ]);
    }

}