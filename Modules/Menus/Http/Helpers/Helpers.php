<?php

namespace Modules\Menus\Http\Helpers;


use App\Models\ComponentsId;
use Modules\Menus\Models\Menus;
use Modules\Menus\Models\MenusId;

class Helpers
{

    public static function createRecursiveMenuItems($request, $currComponent, $items, $menuType, $lang, $p_id = null, $hasChildren = false)
    {

        $createdItems = $items;

        if (is_null($items) || empty($items))
            return false;

        foreach ($items as $key => $menuItem) {

            $menuId = @$menuItem->menuId;

            $item = MenusId::find($menuId);

            if (is_null($item)) {
                $item = new MenusId();

                $nextPosition = helpers()->getNextItemPosition($item);
                $item->position = $nextPosition;
                $item->active = 1;
            }

            $itemName = $itemUrl = null;
            $isDirectMenuUpdate = !is_null($menuItem->name) && !empty($menuItem->name);

            $item->menus_type_id = $menuType->id;
            $item->p_id = $hasChildren ? $p_id : null;
            $item->component_id = $menuItem->categoryId ?: null;
            $item->item_id = $menuItem->id > 0 ? $menuItem->id : null;
            $item->classes = $menuItem->menuClasses ? $menuItem->menuClasses : null;
            $item->is_direct_update = $isDirectMenuUpdate ? 1 : 0;

            $item->save();

            if ($menuItem->categoryId && $menuItem->id) {
                $component = ComponentsId::where('active', 1)->find($menuItem->categoryId);
                $componentItem = helpers()->getComponentItem($component, $item->item_id);
                if (!$isDirectMenuUpdate) {
                    $componentItems = @$componentItem->item->allInfo->items;
                    if (!is_null($componentItems))
                        $itemName = @$componentItems->where('lang_id', $lang->id)->first()->name;

                    if (is_null($itemName))
                        $itemName = @$componentItem->item->name;
                }

                $itemUrl = @$componentItem->clearUrl;

            }

            Menus::updateOrCreate([
                'menu_id' => $item->id,
                'lang_id' => $lang->id
            ], [
                'name' => !is_null($menuItem->name) && !empty($menuItem->name) ? $menuItem->name : $itemName,
                'url' => !is_null($menuItem->url) && !empty($menuItem->url) ? $menuItem->url : $itemUrl
            ]);

            $createdItems[$key]->menuId = $item->id;

            if ($menuItem->menuFile) {
                helpers()->uploadFiles([$menuItem->menuFile], $item->id, $currComponent->id, 'edit');
            }
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'edit');

            self::createRecursiveMenuItems($request, $currComponent, @$menuItem->children, $menuType, $lang, $item->id, !is_null(@$menuItem->children));

        }

        return $createdItems;
    }

    public static function updateMenuItemsPosition($items)
    {

        if (!is_array($items))
            return false;

        $items = array_filter($items);

        $itemsPositions = [];
        foreach ($items as $id) {
            $item = MenusId::find($id);
            $itemsPositions[] = @$item->position;
        }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($items as $key => $id) {
            if (array_key_exists($key, $itemsPositions))
                MenusId::where('id', $id)->update(['position' => $itemsPositions[$key]]);
        }

        return true;
    }

    public static function updateMenuItems($currComponent, $item, $langId = null, $action = 'save', $destroyEvent = null)
    {
        if (is_null($currComponent) || is_null($item))
            return false;

        $menuIds = MenusId::where('component_id', $currComponent->id)
            ->where('item_id', $item->id)
            ->with('items')
            ->get();

        if ($menuIds->isEmpty())
            return false;

        if ($action != 'save' && $action != 'activate' && $action != 'destroy')
            return false;

        foreach ($menuIds as $menuId) {

            if ($action == 'save' && !is_null($langId)) {
                if ($menuId->is_direct_update)
                    $menuId->items()->where('lang_id', $langId)->update([
                        'url' => ($currComponent->controller !== 'PagesController' ? "{$currComponent->slug}/" : '') . @$item->slug,
                    ]);
                else {
                    $menuId->items()
                        ->updateOrCreate([
                            'lang_id' => $langId
                        ], [
                            'name' => @$item->itemByLang->name,
                            'url' => ($currComponent->controller !== 'PagesController' ? "{$currComponent->slug}/" : '') . @$item->slug,
                        ]);
                }
            } elseif ($action == 'activate') {
                $menuId->update([
                    'active' => $item->active
                ]);
            } elseif ($action == 'destroy' && !is_null($destroyEvent)) {
                if ($destroyEvent != 'restore')
                    $menuId->update([
                        'active' => 0
                    ]);
                else
                    $menuId->update([
                        'active' => 1
                    ]);
            }
        }

        return true;
    }

    public static function getMenuRecursive($typeId, $levels = 1, $isRecursion = false, $items = [], $currLevel = 1, $allItems = [])
    {
        $response = $parents = $children = collect();

        if (!$isRecursion) {
            $allItems = MenusId::whereHas('type', function ($q) use ($typeId) {
                $q->where('active', 1);
                $q->where('id', $typeId);
            })
                ->where('active', 1)
                ->orderBy('position')
                ->with(['globalName', 'componentId', 'componentId.globalName', 'file.file'])
                ->get();

            $items = $allItems->where('p_id', null);
        }

        if (!empty($items) && $items->isNotEmpty())
            foreach ($items as $item) {
                $currName = @$item->currNameUrl->currName;
                $currUrl = @$item->currNameUrl->currUrl;
                if (!is_null($currName))
                    $pushCollection = (object)[
                        'id' => $item->id,
                        'currentName' => $currName,
                        'currentUrl' => filter_var($currUrl, FILTER_VALIDATE_URL) ?: customUrl($currUrl),
                        'currentClasses' => @$item->classes,
                        'currentIcon' => @$item->firstFile->small && strpos(@$item->firstFile->small, 'no-image') === false ? @$item->firstFile->small : (strpos(@$item->firstFile->original, 'no-image') === false ? @$item->firstFile->original : null),
                        'children' => collect()
                    ];
                else
                    $pushCollection = [];


                $newItems = $allItems->where('p_id', $item->id);
                if ($newItems->isNotEmpty()) {

                    $children = self::getMenuRecursive($typeId, $levels, true, $newItems, $currLevel + 1, $allItems);

                    if ($children->isNotEmpty() && !is_array($pushCollection))
                        $pushCollection->children = $children;

                }

                $response->push($pushCollection);

            }

        return $response;
    }

}