<?php

namespace Modules\Orders\Http\Helpers;

use App\Models\AdminUsers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Modules\Orders\Models\Cart;
use Modules\Orders\Models\CartProducts;
use Modules\Orders\Notifications\OrdersNotification;
use Modules\Products\Http\Helpers\Helpers as ProductsHelpers;
use Modules\Products\Models\ProductsAttributesId;
use Nwidart\Modules\Facades\Module;

class Helpers
{
    public static function getProductCartAttributes($product, $attributes)
    {
        $response = collect();

        if ($attributes->isEmpty())
            return $response;

        if (!is_null(@$product->attributes_ids)) {
            foreach ($product->attributes_ids as $p_id => $attributesId) {
                $parent = @$attributes->find($p_id);
                $child = @$attributes->find($attributesId);

                if ($parent && $child) {
                    $response->put($p_id, collect([
                        'parent' => $parent,
                        'child' => collect(['item' => $child])
                    ]));

                    if ($parent->type->slug == 'color' || $parent->type->slug == 'image')
                        $response[$p_id]['child']['value'] = $parent->type->slug == 'color' ? collect($child->colors) : getItemFile($child, 'small');
                }
            }
        }

        return $response->sortBy('parent.position');
    }

    public static function getVariationAttributes($product, $variation, $attributesIds)
    {

        if (is_null($variation))
            return false;

        $response = collect();

        $variationAttributes = $variation->variations()
            ->with(['parentAttribute', 'parentAttribute.type', 'parentAttribute.items', 'parentAttribute.items.lang', 'childAttribute', 'childAttribute.type', 'childAttribute.items', 'childAttribute.items.lang'])
            ->get();

        $attributes = ProductsHelpers::getProductSelectedAttributes($product, true);

        if ($variationAttributes->isNotEmpty())
            foreach ($variationAttributes as $variationAttribute) {
                $fullAttrInfo = @$attributes->where('parent.id', $variationAttribute->products_attribute_parent_id)->first();
                if (is_null($fullAttrInfo))
                    continue;

                $currAttrInfoChildren = @$fullAttrInfo['children']->whereIn('id', $attributesIds)->first();
                $currAttrInfoParent = @$fullAttrInfo['parent'];

                if (is_null($variationAttribute->products_attribute_child_id)) {
                    $response->push([
                        'parent' => $currAttrInfoParent,
                        'child' => $currAttrInfoChildren
                    ]);
                } else {
                    $response->push([
                        'parent' => $variationAttribute->parentAttribute,
                        'child' => $variationAttribute->childAttribute
                    ]);
                }
            }


        return $response;
    }

    public static function displayProductAttributes($product)
    {
        $response = '';

        if (!is_null(@$product->attributes_ids)) {

            $attributes = ProductsAttributesId::where('active', 1)->orderBy('position')->with('globalName')->get();

            foreach ($product->attributes_ids as $p_id => $attributesId) {
                $response .= "<div class='product-attributes-list-block'>";
                $parent = @$attributes->find($p_id)->globalName->name;
                $child = @$attributes->find($attributesId)->globalName->name;

                if ($parent && $child) {
                    $response .= "<div class='product-attribute-item'>";
                    $response .= "<span class='title'>{$parent}</span>";
                    $response .= "<span class='description'> {$child}</span>";
                    $response .= "</div>";
                }

                $response .= "</div>";
            }
        }

        return $response;
    }

    public static function updateProductsQtyOnCheckout($cartProducts, $increment = false)
    {

        if ($cartProducts->isNotEmpty())
            foreach ($cartProducts as $cartProduct) {
                self::updateProductQtyOnCheckout($cartProduct, $increment);
            }

        return true;
    }

    public static function updateProductQtyOnCheckout($cartProduct, $increment = false, int $customQty = null)
    {
        $product = @$cartProduct->product;

        if (!is_null($product)) {
            if (@$product->isVariationProduct) {
                $variationsDetails = $cartProduct->variation;

                if (!is_null($variationsDetails) && is_numeric($variationsDetails->finStock)) {
                    if ($increment)
                        $newStock = $variationsDetails->finStock + (!is_null($customQty) ? $customQty : $cartProduct->quantity);
                    else
                        $newStock = $cartProduct->quantity >= 0 ? $variationsDetails->finStock - (!is_null($customQty) ? $customQty : $cartProduct->quantity) : 0;

                    $variationsDetails->update([
                        'stock_quantity' => $newStock
                    ]);

                }
            } elseif (is_numeric($product->finStock)) {
                if ($increment)
                    $newStock = $product->finStock + (!is_null($customQty) ? $customQty : $cartProduct->quantity);
                else
                    $newStock = $cartProduct->quantity >= 0 ? $product->finStock - (!is_null($customQty) ? $customQty : $cartProduct->quantity) : 0;

                $product->update([
                    'stock_quantity' => $newStock
                ]);

            }
        }

        return true;
    }

    public static function updateProductsPriceOnCheckout($cart, $cartProducts = null)
    {

        $totalAmount = $totalCount = 0;

        if ((is_null($cart) && is_null($cartProducts)) || (!is_null($cart) && is_null(@$cart->cartProducts)))
            return false;

        if (is_null($cartProducts))
            $cartProducts = $cart->cartProducts;

        if ($cartProducts->isNotEmpty())
            foreach ($cartProducts as $cartProduct) {
                $product = $cartProduct->product;
                $cartProductAmount = @$cartProduct->finPrice->minPrice ? @$cartProduct->finPrice->minPrice : @$cartProduct->finPrice->maxPrice;
                $variationAttributesOptions = self::formatAttributesForStoreCartProduct($product, $cartProduct->variation, array_values($cartProduct->attributes_ids));
                $cartAttributes = array_filter($variationAttributesOptions->cart_attributes);

                $cartProductInfo = [
                    'id' => $product->id,
                    'name' => $product->globalName ? $product->globalName->name : '',
                    'sku' => $product->isVariationProduct ? $cartProduct->variation->sku : $product->sku,
                    'slug' => $product->slug,
                    'firstFile' => $cartProduct->customFile,
                    'allFiles' => $cartProduct->customFiles,
                    'finPrice' => $cartProduct->finPrice
                ];

                CartProducts::where('id', $cartProduct->id)
                    ->where('cart_id', $cart->id)
                    ->update([
                        'amount' => $cartProductAmount,
                        'info' => json_encode($cartProductInfo, true),
                        'attributes' => json_encode($cartAttributes, true),
                    ]);

                $totalAmount += $cartProductAmount * $cartProduct->quantity;
                $totalCount += $cartProduct->quantity;
            }

        $cart->update([
            'amount' => $totalAmount,
            'count_items' => $totalCount,
        ]);

        return true;
    }

    public static function getCartProducts($cart = null, $limitProducts = 3, $getCartAttributes = false, $getVariationsAttributesOptions = false)
    {
        $userId = request()->cookie('cart', null);

        //if (is_null($cart) && !is_null($userId))
        //    $cart = Cart::where('user_id', $userId)->first();
        if (is_null($cart))
            $cart = Cart::personalOrCommon()->first();
        //dd($cart);
        $response = (object)[
            'products' => collect(),
            'count' => 0,
            'total' => 0,
            'outOfStockProductsIds' => collect()
        ];

        if (!is_null($cart)) {
            $cartProducts = $cart->cartProducts()
                ->with(['product', 'product.file.file', 'product.globalName', 'product.productItemsAttributes', 'product.productItemsAttributes.options', 'product.variationsDetails', 'variation', 'variation.file.file'])
                ->orderBy('created_at', 'desc')
                ->get();



            $attributes = $getCartAttributes ? ProductsAttributesId::where('active', 1)->orderBy('position')->with(['globalName', 'type', 'file.file'])->get() : collect();

            if ($cartProducts->isNotEmpty())
                foreach ($cartProducts as $cartProduct) {
                    $product = @$cartProduct->product;
                    $isVariationProduct = @$product->isVariationProduct;
                    $finPrice = $product && (!$isVariationProduct || ($isVariationProduct && @$cartProduct->variation)) ? $cartProduct->finPrice : $cartProduct->amount;
                    $cartAttributesIdsChanged = false;

                    if ($getVariationsAttributesOptions && $isVariationProduct) {
                        if (!in_array($cartProduct->attributes_ids, arrayCartesian($cartProduct->productSelectedVariationAttrIds)))
                            $cartAttributesIdsChanged = true;
                    }

                    $inStockProduct = !$cartAttributesIdsChanged ? (is_numeric($cartProduct->finStock) && $cartProduct->quantity > $cartProduct->finStock || !is_numeric($cartProduct->finStock) ? $cartProduct->finStock : true) : false;

                    //$cartProduct->totalPrice = is_object($finPrice) ? (@$finPrice->minPrice ? @$finPrice->minPrice : @$finPrice->maxPrice) : $finPrice;
                    $cartProduct->totalPrice = $cartProduct->amount;
                    $cartProduct->cartAttributes = $getCartAttributes ? self::getProductCartAttributes($cartProduct, $attributes) : collect();
                    $cartProduct->isInStockCheckedQty = $inStockProduct;

                    if (is_numeric($inStockProduct) || !$inStockProduct)
                        $response->outOfStockProductsIds->push([
                            'id' => $cartProduct->id,
                            'qty' => $cartProduct->isInStockCheckedQty,
                            'qtyMsg' => is_numeric($cartProduct->isInStockCheckedQty) ? "{$cartProduct->isInStockCheckedQty} products available" : ''
                        ]);

                    $response->total += $cartProduct->totalPrice * $cartProduct->quantity;
                    $response->count += $cartProduct->quantity;
                }

            $response->cartCountProducts = $cartProducts->count();

            if ($limitProducts === -1)
                $response->products = $cartProducts;
            else
                $response->products = $cartProducts->take(config('cms.cart.countProducts', $limitProducts));

        }

        return $response;
    }

    public static function orderIdGenerator($itemId, $orderId = null)
    {

        if (!@$itemId)
            return false;

        $padLength = 2;

        if (@$orderId && $itemId == substr($orderId, $padLength, strlen($itemId)))
            $nextSerial = $orderId;
        else
            $nextSerial = rand(11, 99) . $itemId . rand(111, 999);

        return $nextSerial;
    }

    public static function formatAttributesForStoreCartProduct($item, $variationsDetails, $attributesIds)
    {

        $variationAttributes = self::getVariationAttributes($item, $variationsDetails, $attributesIds);

        $options = $optionsIds = [];

        $response = (object)[
            'cart_attributes' => $options,
            'cart_attributes_ids' => $optionsIds
        ];


        if (@$variationAttributes && $variationAttributes->isNotEmpty()) {
            foreach ($variationAttributes as $variationAttribute) {
                $parent = @$variationAttribute['parent'];
                $child = @$variationAttribute['child'];

                if ($parent && $child) {
                    $parentItems = $childItems = [];

                    if ($parent->items->isNotEmpty()){
                        foreach ($parent->items as $parentItem) {
                            $parentItemLang = $parentItem->lang;
                            if ($parentItemLang)
                                $parentItems[$parentItemLang->slug] = array_filter([
                                    'name' => $parentItem->name,
                                    'description' => $parentItem->description
                                ]);
                        }
                    }


                    if ($child->items->isNotEmpty()){
                        foreach ($child->items as $childItem) {
                            $childItemLang = $childItem->lang;

                            if ($childItemLang)
                                $childItems[$childItemLang->slug] = array_filter([
                                    'name' => $childItem->name,
                                    'description' => $childItem->description
                                ]);
                        }
                    }else{
                        if (!is_null($child->range_value)){
                            $childItems = array_filter([
                                'name' => $child->range_value,
                            ]);
                        }
                    }

                    $parentItems = array_filter($parentItems);
                    $childItems = array_filter($childItems);

                    $options[] = array_filter([
                        'parent' => [
                            'id' => $parent->id,
                            'slug' => $parent->slug,
                            'attribute_type' => @$parent->type->slug,
                            'field_type' => $parent->field_type,
                            'position' => $parent->position,
                            'info' => $parentItems,
                        ],
                        'child' => [
                            'id' => $child->id,
                            'slug' => $child->slug,
                            'color' => $child->color ? $child->colors : null,
                            'img' => @parseURL(getItemFile($child, 'small'))->path,
                            'position' => $parent->position,
                            'info' => $childItems,
                        ]
                    ]);

                    $optionsIds[$parent->id] = $child->id;
                }
            }

            $response = (object)[
                'cart_attributes' => $options,
                'cart_attributes_ids' => $optionsIds
            ];
        }

        return $response;
    }

    public static function updateProductsSalesRating($cartProducts)
    {

        if ($cartProducts->isEmpty())
            return false;

        foreach ($cartProducts as $cartProduct) {
            $product = $cartProduct->product;

            if ($product) {
                $product->increment('popularity_sales', $cartProduct->quantity);

                $productAvgRating = null;
                if ($product->reviews->isNotEmpty())
                    $productAvgRating = round($product->reviews->avg('rating'), 2);

                $product->update([
                    'popularity_rating' => $productAvgRating
                ]);
            }

        }

        return true;
    }

    public static function updateAuthUserData($authUser, $cart)
    {
        if (!auth()->check() || !$authUser)
            return false;

        $data = [];

        if (!$authUser->name && @$cart->details->name)
            $data['name'] = $cart->details->name;

        if (!$authUser->phone && @$cart->details->phone)
            $data['phone'] = $cart->details->phone;

        if (!$authUser->country_id && @$cart->country_id)
            $data['country_id'] = $cart->country_id;

        if (!$authUser->region_id && @$cart->region_id)
            $data['region_id'] = $cart->region_id;

        if (!$authUser->city && @$cart->city)
            $data['city'] = $cart->city;

        if (!$authUser->address && @$cart->details->address)
            $data['address'] = $cart->details->address;

        if (!$authUser->zip_code && @$cart->details->zip_code)
            $data['zip_code'] = $cart->details->zip_code;

        if (!empty($data))
            $authUser->update($data);

        return true;
    }

    public static function getAuthUserOrdersProducts($authUser, $perPage = 15, $getVariationsAttributesOptions = false, $paginate = false)
    {
        $limitProducts = 4;

        $allOrders = collect();
        $ordersBuilder = $authUser->orders()
            ->whereNotNull('status')
            ->with(['cartProducts', 'cartProducts.product', 'cartProducts.product.file.file', 'cartProducts.product.globalName', 'cartProducts.product.productItemsAttributes', 'cartProducts.product.productItemsAttributes.options', 'cartProducts.product.variationsDetails', 'cartProducts.variation', 'cartProducts.variation.file.file'])
            ->orderBy('order_date', 'desc');

        if ($paginate) {
            $orders = $ordersBuilder->paginate($perPage);
            $ordersCount = $orders->count();
        } else {
            $ordersCount = $ordersBuilder->count();
            $orders = $ordersBuilder->limit($perPage)->get();
        }

        if ($orders->isNotEmpty()) {

            foreach ($orders as $order) {
                $allCartProducts = (object)[
                    'products' => collect(),
                    'count' => 0,
                    'total' => 0,
                    'outOfStockProductsIds' => collect()
                ];

                $cartProducts = $order->cartProducts;

                if ($cartProducts->isNotEmpty())
                    foreach ($cartProducts as $cartProduct) {
                        $cartAttributesIdsChanged = false;

                        if ($getVariationsAttributesOptions && @$cartProduct->product->isVariationProduct) {
                            if (!in_array($cartProduct->attributes_ids, arrayCartesian($cartProduct->productSelectedVariationAttrIds)))
                                $cartAttributesIdsChanged = true;
                        }

                        $inStockProduct = !$cartAttributesIdsChanged ? (is_numeric($cartProduct->finStock) && $cartProduct->quantity > $cartProduct->finStock || !is_numeric($cartProduct->finStock) ? $cartProduct->finStock : true) : false;

                        $cartProduct->totalPrice = $cartProduct->amount;
                        $cartProduct->isInStockCheckedQty = $inStockProduct;

                        if (is_numeric($inStockProduct) || !$inStockProduct)
                            $allCartProducts->outOfStockProductsIds->push([
                                'id' => $cartProduct->id,
                                'qty' => $cartProduct->isInStockCheckedQty,
                                'qtyMsg' => is_numeric($cartProduct->isInStockCheckedQty) ? "{$cartProduct->isInStockCheckedQty} products available" : ''
                            ]);

                        $allCartProducts->total += $cartProduct->totalPrice * $cartProduct->quantity;
                        $allCartProducts->count += $cartProduct->quantity;
                    }

                $allCartProducts->cartCountProducts = $cartProducts->count();
                $allCartProducts->products = $cartProducts;

                if ($allCartProducts->cartCountProducts > $limitProducts)
                    $allOrders->push([
                        'order' => $order,
                        'allCartProducts' => $allCartProducts,
                        'cartProducts' => $allCartProducts->products->take($limitProducts),
                        'remainCartProductsCount' => $allCartProducts->cartCountProducts - $limitProducts
                    ]);
                else
                    $allOrders->push([
                        'order' => $order,
                        'allCartProducts' => $allCartProducts,
                        'cartProducts' => $allCartProducts->products,
                        'remainCartProductsCount' => 0
                    ]);
            }

        }

        return (object)[
            'ordersCount' => $ordersCount,
            'orders' => $allOrders,
            'currentPage' => $paginate ? $orders->currentPage() : 1,
            'lastPage' => $paginate ? $orders->lastPage() : 1,
            'total' => $paginate ? $orders->total() : 1,
        ];
    }

    public static function getShippingCountries($request, $displayId = false)
    {

        if (!$request->ajax())
            abort(404);

        $defaultShippingZone = ShippingZonesHelpers::getDefaultShippingZone();

//        dd($defaultShippingZone);
        $defaultShippingZoneCountries = @$defaultShippingZone->countries;
        if ($defaultShippingZoneCountries && $defaultShippingZoneCountries->isNotEmpty())
            $itemsBuild = $defaultShippingZone->countries();
        elseif ($defaultShippingZone && $defaultShippingZoneCountries->isEmpty())
            $itemsBuild = new Countries();
        else
            $itemsBuild = Countries::countriesWithShipping();

        $items = $itemsBuild->where('active', 1)
            ->where(function ($q) use ($request) {
                $q->where('name', 'LIKE', "%{$request->get('q')}%");
                $q->orWhere('iso', 'LIKE', "%{$request->get('q')}%");
            })->orderBy('name')->paginate(10);

        return selectAjaxSearchItems($request, $items, 'name', null, $displayId);
    }

    public static function getShippingRegions($request, $displayId = false)
    {
        if (!$request->ajax())
            abort(404);

        $perPage = 10;
        $currPage = (int)$request->get('page', 1);
        $countryId = $request->get('countryId', null);
        $items = collect([
            'total' => 0,
            'items' => collect()
        ]);

        $country = Countries::where('active', 1)->find($countryId);

        $shippingZoneBuild = $country->shippingZones()->where('active', 1)
            ->whereHas('shippingMethods', function ($q) {
                $q->where('active', 1);
            })
            ->with(['shippingRegions'])
            ->orderBy('position');

        if (!is_null($country)) {
            $shippingZoneWithoutRegionBuild = clone $shippingZoneBuild;
            $shippingZoneWithoutRegion = $shippingZoneWithoutRegionBuild->whereDoesntHave('shippingRegions', function ($q) use ($countryId) {
                $q->whereHas('shippingZoneCountry', function ($q) use ($countryId) {
                    $q->where('country_id', $countryId);
                });
            })->first();

            if ($shippingZoneWithoutRegion) {

                $items = Regions::where('active', 1)
                    ->where('country_id', $countryId)
                    ->where('name', 'LIKE', "%{$request->get('q')}%")
                    ->whereHas('country', function ($q) {
                        $q->where('active', 1);
                    })
                    ->orderBy('name')
                    ->paginate($perPage);
            } else {
                $shippingZone = $shippingZoneBuild->whereHas('shippingRegions', function ($q) use ($countryId) {
                    $q->whereHas('shippingZoneCountry', function ($q) use ($countryId) {
                        $q->where('country_id', $countryId);
                    });
                })->with(['shippingRegions' => function ($q) use ($countryId) {
                    $q->whereHas('shippingZoneCountry', function ($q) use ($countryId) {
                        $q->where('country_id', $countryId);
                    });
                }])->first();

                $regions = collect();

                if ($shippingZone && $shippingZone->shippingRegions->isNotEmpty()) {
                    foreach ($shippingZone->shippingRegions as $shippingRegion) {
                        $region = $shippingRegion->region;
                        if ($region)
                            $regions->push($region);
                    }

                    $items = $regions->filter(function ($item) use ($request) {
                        return stristr(strtolower(Str::ascii($item->name)), strtolower(Str::ascii($request->get('q', '')))) && $item->active;
                    })->sortBy('name')->unique('id');

                    $items = collect([
                        'total' => $items->count(),
                        'items' => $items->forPage($currPage, $perPage)->values()
                    ]);
                }

                if (!$shippingZone) {
                    $items = $country->regions()->where(function ($q) use ($request) {
                        $q->where('name', 'LIKE', "%{$request->get('q')}%");
                    })->orderBy('name')->paginate(10);
                }
            }
        }

        return selectAjaxSearchItems($request, $items, 'name', null, $displayId);
    }

    public static function sendNotificationToAdminPanel($cart)
    {
        if (!$cart)
            return false;

        $users = AdminUsers::whereHas('group.permissions', function ($q) {
            $requireModules = ['orders', 'notifications'];
            $q->leftJoin('components_id', 'components_id.id', '=', 'admin_users_permissions.component_id');
            $q->where('for_edit', 1);
            $q->whereIn('components_id.module', $requireModules);
            $q->havingRaw('count(*) = ' . count($requireModules));
        })->get();

        try {
            $details = [
                'title' => "New order #{$cart->order_id}",
                'msg' => "You have new order #{$cart->order_id}.",
                'linkText' => 'View order',
                'url' => urldecode(customUrl(['admin', 'orders', 'edit', $cart->id])),
                'order_id' => $cart->id
            ];
            Notification::send($users, new OrdersNotification($details));

        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}