<?php

namespace Modules\Orders\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class OrdersRequest extends FormRequest
{

//    private $orderProducts;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'status' => 'required|in:checkout,processing,cancelled,approved,finished,failed',
            'order_date' => 'required|date|date_format:d-m-Y H:i',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'payment_method' => 'required|in:cash,card',
            'message' => 'nullable|max:1024',
        ];
        $isShippingMethod = request()->get('manipulate_shipping_pickup_block') == 'shipping_block';
        $isPickupMethod = request()->get('manipulate_shipping_pickup_block') == 'pickup_block';
        $shippingCountryId = request()->get('shipping_country', null);
        $shippingRegionId = request()->get('shipping_region', null);
        $pickupCountryId = request()->get('pickup_country', null);
        $pickupRegionId = request()->get('pickup_region', null);
        $pickupAddressId = request()->get('pickup_address', null);


        if ($isShippingMethod) {
            $shippingCountriesRules = Rule::exists('countries', 'id')->where(function ($q) use ($shippingCountryId) {
                $q->where('id', $shippingCountryId);
                $q->where('active', 1);
            });

            $shippingRegionRules = Rule::exists('regions', 'id')->where(function ($q) use ($shippingCountryId, $shippingRegionId) {
                $q->where('id', $shippingRegionId);
                $q->where('country_id', $shippingCountryId);
                $q->where('active', 1);
            });

            $rules = array_merge($rules, [
                'shipping_country' => [
                    'required',
                    $shippingCountriesRules,
                ],
                'shipping_region' => [
                    'required',
                    $shippingRegionRules,
                ],
                'shipping_city' => 'required',
                'zip_code' => 'nullable',
                'shipping_address' => 'required',
                'shipping_amount' => 'nullable|numeric',

            ]);
        } elseif ($isPickupMethod) {
            $pickupCountriesRules = Rule::exists('stores_id', 'country_id')->where(function ($q) use ($pickupCountryId) {
                $q->where('country_id', $pickupCountryId);
                $q->where('active', 1);
                $q->where('for_pickup', 1);
            });

            $pickupRegionRules = Rule::exists('stores_id', 'region_id')->where(function ($q) use ($pickupCountryId, $pickupRegionId) {
                $q->where('country_id', $pickupCountryId);
                $q->where('region_id', $pickupRegionId);
                $q->where('active', 1);
                $q->where('for_pickup', 1);
            });

            $pickupAddressRules = Rule::exists('stores_id', 'id')->where(function ($q) use ($pickupCountryId, $pickupRegionId, $pickupAddressId) {
                $q->where('id', $pickupAddressId);
                $q->where('country_id', $pickupCountryId);
                $q->where('region_id', $pickupRegionId);
                $q->where('active', 1);
                $q->where('for_pickup', 1);
            });

            $rules = array_merge($rules, [
                'pickup_country' => [
                    'required',
                    $pickupCountriesRules,
                ],
                'pickup_region' => [
                    'required',
                    $pickupRegionRules,
                ],
                'pickup_address' => [
                    'required',
                    $pickupAddressRules,
                ],

            ]);
        }
//
//        $this->orderProducts = request()->get('order_products', []);
//
//        if (@$this->orderProducts['count']) {
//            foreach ($this->orderProducts['count'] as $key => $val) {
//                $rules["order_products.count.{$key}"] = 'required|numeric|min:1';
//            }
//        }
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        $data = [
            'status' => false,
            'validator' => true,
            'msg' => [
                'e' => $validator->messages(),
                'type' => 'error'
            ],
        ];

        throw new HttpResponseException(response()->json($data));
    }

//    public function messages()
//    {
//        $msg = [];
//        if (@$this->orderProducts['count']) {
//            foreach ($this->orderProducts['count'] as $key => $val) {
//                $msg["order_products.count.{$key}.required"] = 'The count is required.';
//                $msg["order_products.count.{$key}.numeric"] = 'The count must be a number.';
//                $msg["order_products.count.{$key}.min"] = 'The count must be at least :min';
//            }
//        }
//        return $msg;
//    }
}
