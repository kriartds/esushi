<?php

namespace Modules\Orders\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\IikoApi\IikoClient;
use App\IikoApi\Models\LogIiko;
use App\MaibApi\MaibClient;
use App\Models\Languages;
use App\Models\LogsPayment;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\Coupons\Http\Helpers\Helpers as CouponsHelpers;
use Modules\Orders\Models\Cart;
use Modules\Orders\Models\CartProducts;
use Illuminate\Http\Request;
use Modules\Orders\Http\Helpers\Helpers as OrdersHelpers;
use Modules\Products\Models\ProductsItemsId;
use Modules\Products\Models\ProductsItemsVariationsDetailsId;
use Modules\Restaurants\Models\RestaurantsId;
use Modules\SiteUsers\Models\Users;

class OrdersController extends DefaultController
{

    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'orders';
        $this->path = "{$this->moduleName}::orders";
    }

    public function index(Request $request)
    {

        $response = $this->filter($request);

        $response['path'] = $this->path;

        return view("{$this->path}.list", $response);

    }

    public function filter(Request $request, $onlyOrders = false)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $filterParams = array_filter($request->except('page'));

        if (!$request->ajax()) {
            $newFiltersParams = [];

            if (!empty($filterParams) && count($filterParams) > 0) {

                foreach ($filterParams as $key => $one_filter_elem) {
                    $newFiltersParams[$key] = $one_filter_elem;
                    if (strpos($one_filter_elem, '[') !== false || strpos($one_filter_elem, ']') !== false) {
                        $newFiltersParams[$key] = explode(',', substr($filterParams[$key], 1, -1));
                    }
                }
            }

            $filterParams = $newFiltersParams;
        }

        $pushUrl = '';

        if (!empty($filterParams)) {
            foreach ($filterParams as $key => $one_filter_el) {

                if (is_array($one_filter_el)) {
                    $pushUrlArr = '';
                    foreach ($one_filter_el as $k => $filter_el) {
                        $pushUrlArr .= $filter_el . ',';
                    }

                    $pushUrl .= $key . '=[' . strip_tags(substr($pushUrlArr, 0, -1)) . ']&';
                } else {
                    $pushUrl .= $key . '=' . strip_tags(str_replace('[]', '', $one_filter_el)) . '&';
                }
            }

            $pushUrl = '?' . substr($pushUrl, 0, -1);
        }

        $items = Cart::where(function ($q) use ($filterParams, $request) {

            if (array_key_exists('payment_method', $filterParams) && is_array($filterParams['payment_method']))
                $q->where('payment_method', $filterParams['payment_method']);
            elseif (array_key_exists('status', $filterParams) && is_array($filterParams['status']) && !in_array('progress', $filterParams['status']))
                $q->whereNotNull('payment_method');

            if (array_key_exists('status', $filterParams) && is_array($filterParams['status']))
                $q->where(function ($q) use ($filterParams) {
                    $q->whereIn('status', $filterParams['status']);

                    if (in_array('progress', $filterParams['status']))
                        $q->orWhereNull('status');
                });
            else {
                $q->where(function ($q) {
                    $q->whereNotNull('status');
                    $q->orWhere('is_draft', 1);
                });
            }

            if (array_key_exists('order_id', $filterParams) && !is_array($filterParams['order_id']))
                $q->where('order_id', $filterParams['order_id']);

            if (array_key_exists('client_email', $filterParams) && !is_array($filterParams['client_email']))
                $q->where('email', $filterParams['client_email']);

            if (array_key_exists('users_ids', $filterParams) && is_array($filterParams['users_ids']))
                $q->whereIn('auth_user_id', $filterParams['users_ids']);

            if (array_key_exists('order_type', $filterParams) && is_array($filterParams['order_type']))
                $q->where(function ($q) use ($filterParams) {
                    if (in_array('simple', $filterParams['order_type'])) {
                        $q->whereNotNull('user_id');
                        $q->where('is_draft', 0);
                        $q->where('is_fast_buy', 0);
                    }

                    if (in_array('fast', $filterParams['order_type'])) {
                        $q->where('is_fast_buy', 1);
                        $q->whereNotNull('user_id');
                    }

                    if (in_array('admin', $filterParams['order_type']))
                        $q->whereNull('user_id');

                    if (in_array('draft', $filterParams['order_type'])) {
                        $q->whereNull('user_id');
                        $q->where('is_draft', 1);
                    }

                    if (in_array('new', $filterParams['order_type'])) {
                        $q->where('seen', 0);
                        $q->whereNotNull('user_id');
                    }
                });

            if (array_key_exists('start_date', $filterParams) && !array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']))
                $q->whereDate('order_date', '>=', date('Y-m-d', strtotime($filterParams['start_date'])));
            elseif (!array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['end_date']))
                $q->whereDate('order_date', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));
            elseif (array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']) && !is_array($filterParams['end_date']))
                $q->whereDate('order_date', '>=', date('Y-m-d', strtotime($filterParams['start_date'])))->whereDate('order_date', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));

        })
            ->orderBy('order_date', 'desc')
            ->orderBy('is_fast_buy')
            ->paginate($perPage);

        $items->setPath(url(LANG, ['admin', parent::currComponent()->slug]) . $pushUrl);

        $siteUsers = Users::whereHas('orders')->get();

        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'moduleName' => $this->moduleName,
            'siteUsers' => $siteUsers,
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
            }

            return response()->json($response);
        }

        return $response;

    }

    public function create()
    {

//        $getPickupCountriesRegionsStores = OrdersHelpers::getPickupCountriesRegionsStores();

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
//            'storesCountries' => $getPickupCountriesRegionsStores->storesCountries,
//            'storesRegions' => $getPickupCountriesRegionsStores->storesRegions,
//            'pickupStores' => $getPickupCountriesRegionsStores->pickupStores,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id)
    {


        $item = Cart::findOrFail($id);

        if ($item->status && !$item->seen)
            $item->update(['seen' => 1]);

        $unreadNotification = auth()->guard('admin')->user()->unreadNotifications->where('data.order_id', $item->id)->first();
        if ($unreadNotification)
            $unreadNotification->markAsRead();

//        $cartDiscount = @$item->discount;
        $cartDiscountAmount = 0;
//        $validateCouponErrorMsg = '';
        $allCartProducts = OrdersHelpers::getCartProducts($item, -1);

        $cartSubtotal = @$allCartProducts->total ?: 0;
        $cartProducts = @$allCartProducts->products ?: collect();
        $cartShippingAmount = @$item->shipping_amount >= 0 ? $item->shipping_amount : 0;

        if ($item->discount_id && $item->discount_details) {
            if (!$item->discount_details->allow_free_shipping) {
                $cartDiscountAmount = $item->discount_amount ? $item->discount_amount : 0;
            } else
                $cartShippingAmount = 0;
        }

        $cartSubtotalWithCoupon = $cartSubtotal - $cartDiscountAmount;
        $cartSubtotalWithCoupon = $cartSubtotalWithCoupon >= 0 ? $cartSubtotalWithCoupon : 0;
        $cartTotal = $cartSubtotalWithCoupon + $cartShippingAmount;

        //$getPickupCountriesRegionsStores = OrdersHelpers::getPickupCountriesRegionsStores();

        $iiko_logs = LogIiko::where('cart_id', $item->id)->get();
        //dd($iiko_logs);
        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
//            'storesCountries' => $getPickupCountriesRegionsStores->storesCountries,
//            'storesRegions' => $getPickupCountriesRegionsStores->storesRegions,
//            'pickupStores' => $getPickupCountriesRegionsStores->pickupStores,
            'cartSubtotal' => $cartSubtotal,
            'cartTotal' => $cartTotal,
            'shippingAmount' => $cartShippingAmount,
            'cartDiscountAmount' => $cartDiscountAmount,
            'cartSubtotalWithCoupon' => $cartSubtotalWithCoupon,
            'cartProducts' => $cartProducts,
            'orderCurrency' => @$item->order_currency,
            'useShipping' => @$item->shippingMethod || @$item->shipping_details,
            //'usePickup' => @$item->pickupStore || @$item->pickup_details,
            'enableEditing' => @$item->status != 'failed',
            //'validateCouponErrorMsg' => $validateCouponErrorMsg,
            'iiko_logs' => $iiko_logs,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = Cart::onlyTrashed()->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(Request $request, $id)
    {

        $isShippingMethod = $request->get('manipulate_shipping_pickup_block') == 'shipping_block';
        $isPickupMethod = $request->get('manipulate_shipping_pickup_block') == 'pickup_block';
        $shippingCountryId = $request->get('shipping_country', null);
        $shippingRegionId = $request->get('shipping_region', null);
        $pickupCountryId = $request->get('pickup_country', null);
        $pickupRegionId = $request->get('pickup_region', null);
        $pickupAddressId = $request->get('pickup_address', null);

        $rules = [
            'status' => 'required|in:checkout,processing,cancelled,approved,finished,failed',
            'order_date' => 'required|date|date_format:d-m-Y H:i',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'payment_method' => 'required|in:cash,card',
            'message' => 'nullable|max:1024',
        ];

        if ($isShippingMethod) {
//            $shippingCountriesRules = Rule::exists('countries', 'id')->where(function ($q) use ($shippingCountryId) {
//                $q->where('id', $shippingCountryId);
//                $q->where('active', 1);
//            });
//
//            $shippingRegionRules = Rule::exists('regions', 'id')->where(function ($q) use ($request, $shippingCountryId, $shippingRegionId) {
//                $q->where('id', $shippingRegionId);
//                $q->where('country_id', $shippingCountryId);
//                $q->where('active', 1);
//            });

            $rules = array_merge($rules, [
//                'shipping_country' => [
//                    'required',
//                    $shippingCountriesRules,
//                ],
//                'shipping_region' => [
//                    'required',
//                    $shippingRegionRules,
//                ],
                'shipping_city' => 'required',
                'zip_code' => 'nullable',
                'shipping_address' => 'required',
                'shipping_amount' => 'nullable|numeric',

            ]);
        } elseif ($isPickupMethod) {
            $pickupCountriesRules = Rule::exists('stores_id', 'country_id')->where(function ($q) use ($pickupCountryId) {
                $q->where('country_id', $pickupCountryId);
                $q->where('active', 1);
                $q->where('for_pickup', 1);
            });

            $pickupRegionRules = Rule::exists('stores_id', 'region_id')->where(function ($q) use ($request, $pickupCountryId, $pickupRegionId) {
                $q->where('country_id', $pickupCountryId);
                $q->where('region_id', $pickupRegionId);
                $q->where('active', 1);
                $q->where('for_pickup', 1);
            });

            $pickupAddressRules = Rule::exists('stores_id', 'id')->where(function ($q) use ($request, $pickupCountryId, $pickupRegionId, $pickupAddressId) {
                $q->where('id', $pickupAddressId);
                $q->where('country_id', $pickupCountryId);
                $q->where('region_id', $pickupRegionId);
                $q->where('active', 1);
                $q->where('for_pickup', 1);
            });

            $rules = array_merge($rules, [
                'pickup_country' => [
                    'required',
                    $pickupCountriesRules,
                ],
                'pickup_region' => [
                    'required',
                    $pickupRegionRules,
                ],
                'pickup_address' => [
                    'required',
                    $pickupAddressRules,
                ],

            ]);
        }

        $orderProducts = $request->get('order_products', []);

        $rulesMsg = [];

        if (@$orderProducts['count']) {
            foreach ($orderProducts['count'] as $key => $val) {

                $rules["order_products.count.{$key}"] = 'required|numeric|min:1';

                $rulesMsg["order_products.count.{$key}.required"] = 'The count is required.';
                $rulesMsg["order_products.count.{$key}.numeric"] = 'The count must be a number.';
                $rulesMsg["order_products.count.{$key}.min"] = 'The count must be at least :min';

            }
        }

        $validator = Validator::make($request->all(), $rules, $rulesMsg);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validator->errors(),
                    'type' => 'error'
                ],
            ]);
        }

        $saveUpdateOrder = $this->saveUpdateOrder($request, $id);

        return response()->json($saveUpdateOrder);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':

                Cart::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = Cart::whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = Cart::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->cartProducts()->delete();
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = Cart::onlyTrashed()->whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                    }
                }
                break;
            default:

                break;
        }
        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function destroy(Request $request)
    {

        $deletedItemsId = substr($request->get('id'), 1, -1);

        if (empty($deletedItemsId))
            return response()->json([
                'status' => false
            ]);

        if ($request->get('event') != 'to-trash' && $request->get('event') != 'from-trash' && $request->get('event') != 'restore')
            return response()->json([
                'status' => false
            ]);

        $currComponent = parent::currComponent();
        $deletedItemsIds = explode(',', $deletedItemsId);

        if ($request->get('event') == 'to-trash') {
            $items = Cart::whereIn('id', $deletedItemsIds)->get();
        } else {
            $items = Cart::onlyTrashed()->whereIn('id', $deletedItemsIds)->get();
        }

        $cartMessage = $responseMsg = '';

        if (!$items->isEmpty()) {
            foreach ($items as $item) {

                $cartMessage .= $item->id . ', ';

                if ($request->get('event') == 'to-trash' && !$item->trashed()) {
                    $item->delete();
                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' added to trash' : '';
                    helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                } elseif ($request->get('event') == 'from-trash') {
                    $item->cartProducts()->delete();
                    $item->forceDelete();
                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' remove from trash' : '';
                    helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                } elseif ($request->get('event') == 'restore') {
                    $item->restore();
                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' restored from trash' : '';
                    helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                }

            }

            return response()->json([
                'status' => true,
                'cart_messages' => $responseMsg,
                'items' => $deletedItemsIds
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }

    public function getCountries(Request $request)
    {

        if (!$request->ajax())
            abort(404);

        $items = Countries::where('active', 1)
            ->where(function ($q) use ($request) {
                $q->where('name', 'LIKE', "%{$request->get('q')}%");
                $q->orWhere('iso', 'LIKE', "%{$request->get('q')}%");
            })
            ->whereHas('regions', function ($q) {
                $q->where('active', 1);
            })
            ->orderBy('name')
            ->paginate(10);

        $response = selectAjaxSearchItems($request, $items, 'name', null);

        return $response;
    }

    public function getRegions(Request $request)
    {

        if (!$request->ajax())
            abort(404);

        $countriesId = json_decode($request->get('countryId', null));

        $items = Regions::where('active', 1)
            ->where('country_id', $countriesId)
            ->where('name', 'LIKE', "%{$request->get('q')}%")
            ->whereHas('country', function ($q) {
                $q->where('active', 1);
            })
            ->orderBy('name')
            ->paginate(10);

        $response = selectAjaxSearchItems($request, $items, 'name', null);

        return $response;
    }

    public function getPickupRegionsStores(Request $request)
    {
        if (!$request->ajax())
            abort(404);

        $countryId = $request->get('countryId', null);
        $regionId = $request->get('regionId', null);

        $country = Countries::where('active', 1)
            ->whereHas('regionsWithStores')
            ->find($countryId);

        if (!$country)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => 'The selected country is invalid.',
                    'type' => 'warning'
                ]
            ]);

        $region = null;
        if ($regionId)
            $region = Regions::where('active', 1)
                ->where('country_id', $country->id)
                ->whereHas('stores', function ($q) {
                    $q->availableStoreForPickup();
                })->find($regionId);


//        $getPickupCountriesRegionsStores = OrdersHelpers::getPickupCountriesRegionsStores($countryId, @$region->id, true);

        try {
            $pickupShopsView = view("{$this->path}.templates.pickupStoresBlock", [
//                'storesCountries' => $getPickupCountriesRegionsStores->storesCountries,
//                'storesRegions' => $getPickupCountriesRegionsStores->storesRegions,
//                'pickupStores' => $getPickupCountriesRegionsStores->pickupStores,
                'currCountry' => $country,
                'currRegion' => $region,
                'moduleName' => $this->moduleName
            ])->render();
        } catch (\Throwable $e) {
            $pickupShopsView = '';
        }

        return response()->json([
            'status' => true,
            'view' => $pickupShopsView
        ]);
    }

    public function findProducts(Request $request)
    {
        if (!$request->ajax())
            abort(404);

        $excludeProductsIds = [];
        $cartId = $request->get('cartId', null);

        if ($cartId) {
            $cart = Cart::where('id', $request->get('cartId', null))
                ->where(function ($q) {
                    $q->whereNull('user_id');
                    $q->orWhere('is_draft', 1);
                })->first();

            if (!is_null($cart)) {
                $excludeProductsIds = $cart->cartProducts()
                    ->whereNull('products_variation_item_id')
                    ->get(['products_item_id'])
                    ->pluck('products_item_id')
                    ->toArray();
            }
        }

        $items = ProductsItemsId::where('active', 1)
            ->whereNotIn('id', $excludeProductsIds)
            ->where(function ($q) use ($request) {
                $q->where('id', $request->get('q'));
                $q->orWhere('slug', $request->get('q'));
                $q->orWhere('sku', $request->get('q'));
                $q->orWhereHas('globalName', function ($q) use ($request) {
                    $q->where('name', 'LIKE', "%{$request->get('q')}%");
                });
                $q->orWhereHas('variationsDetails', function ($q) use ($request) {
                    $q->where('id', $request->get('q'));
                    $q->orWhere('sku', $request->get('q'));
                });
            })
            ->with('globalName')
            ->orderBy('position')
            ->paginate(10);

        $response = selectAjaxSearchItems($request, $items);

        return $response;
    }

    public function getProductContent(Request $request)
    {
        $productId = $request->get('productId', null);
        $count = $request->get('productsCount', 0);
        $item = ProductsItemsId::where('active', 1)->find($productId);

        if (is_null($item))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => 'Product not exist.',
                    'type' => 'error'
                ]
            ]);

        $response = defineProductVariations($item);

        $response = array_merge($response, [
            'defaultCurrency' => getDefaultCurrency(),
            'increment' => $count + 1
        ]);

        try {
            $view = view("{$this->path}.templates.variationsList", $response)->render();
        } catch (\Throwable $e) {
            $view = '';
        }

        return response()->json([
            'status' => true,
            'view' => $view,
            'productId' => $item->id,
            'increment' => $count + 1
        ]);
    }

    public function addOrderProducts(Request $request, $id)
    {

        $saveUpdateOrder = $this->saveUpdateOrder($request, $id);

        return response()->json($saveUpdateOrder);
    }

    public function updateOrderProducts(Request $request)
    {

        $qty = $request->get('qty', 1);

        $defaultCurrency = getDefaultCurrency();

        $product = CartProducts::whereHas('cart', function ($q) {
            $q->where('status', 'checkout');
        })->find($request->get('productId', null));

        if (is_null($product))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => 'Product not exist.',
                    'type' => 'error'
                ]
            ]);

        $productStock = $product->finStock;

        if (!$productStock || (is_numeric($productStock) && $qty > $productStock))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => is_numeric($productStock) && $qty > $productStock ? 'Please, select quantity lower or equal than ' . $productStock : 'Product is out of stock',
                    'type' => 'warning'
                ]
            ]);

        $oldQty = $product->quantity;

        $product->update([
            'quantity' => $qty
        ]);

        $newQty = $product->quantity - $oldQty;

        OrdersHelpers::updateProductQtyOnCheckout($product, false, $newQty);

        $cart = $product->cart;

        $allCartProducts = OrdersHelpers::getCartProducts($cart, -1);
        $cartSubtotal = @$allCartProducts->total ?: 0;
        $cartShippingAmount = @$cart->shipping_amount >= 0 ? $cart->shipping_amount : 0;
        $cartDiscount = @$cart->discount;
        $cartDiscountAmount = 0;

        if ($cartDiscount) {
            $validateCoupon = CouponsHelpers::validateCoupon(@$cart->discount->code, $defaultCurrency, $cartSubtotal);

            if (!$validateCoupon->allow_free_shipping) {
                if ($validateCoupon->type == 'fixed')
                    $cartDiscountAmount = $validateCoupon->amount;
                else
                    $cartDiscountAmount = $cartSubtotal * $validateCoupon->amount / 100;
            } else
                $cartShippingAmount = 0;
        }

        $cartSubtotalWithCoupon = $cartSubtotal - $cartDiscountAmount;
        $cartSubtotalWithCoupon = $cartSubtotalWithCoupon >= 0 ? $cartSubtotalWithCoupon : 0;
        $cartTotal = $cartSubtotalWithCoupon + $cartShippingAmount;

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => 'Product quantity was successful updated.',
                'type' => 'info'
            ],
            'cartSubtotal' => $cartSubtotal,
            'cartSubtotalStr' => formatPrice($cartSubtotal, $defaultCurrency),
            'cartTotal' => $cartTotal,
            'cartTotalStr' => formatPrice($cartTotal, $defaultCurrency),
            'shippingAmount' => $cartShippingAmount,
            'shippingAmountStr' => formatPrice($cartShippingAmount, $defaultCurrency),
            'cartDiscountAmount' => $cartDiscount ? $cartDiscountAmount : 0,
            'cartDiscountAmountStr' => $cartDiscount ? formatPrice($cartDiscountAmount, $defaultCurrency) : 0,
            'cartSubtotalWithCoupon' => $cartDiscount ? $cartSubtotalWithCoupon : 0,
            'cartSubtotalWithCouponStr' => $cartDiscount ? formatPrice($cartSubtotalWithCoupon, $defaultCurrency) : 0,
            'productSubtotal' => formatPrice($product->amount * $product->quantity, $defaultCurrency)
        ]);
    }

    private function saveUpdateOrder($request, $id)
    {
        $cartProductsData = $errorMessages = [];
        $isDraftOrder = $request->get('isDraft', 0) == 1;
        $existOrderProductsIds = [];
        $currComponent = $this->currComponent();
        $defaultCurrency = getDefaultCurrency();
        $isShippingMethod = $request->get('manipulate_shipping_pickup_block', 'shipping_block') == 'shipping_block';
        $isPickupMethod = !$isShippingMethod;
        $shippingCountryId = $request->get('shipping_country', null);
        $shippingRegionId = $request->get('shipping_region', null);
        $pickupCountryId = $request->get('pickup_country', null);
        $pickupRegionId = $request->get('pickup_region', null);
        $pickupAddressId = $request->get('pickup_address', null);
        $pickupStore = null;
        $freshAddedCartProducts = collect();

        $orderProductsArr = array_filter($request->get('order_products', []));
        $orderProductsIds = @$orderProductsArr['id'] ?: [];


        if ($isDraftOrder) {
            $orderProductsArr = array_filter($request->get('order_products', []));
            $orderProductsIds = @$orderProductsArr['id'] ?: [];


            $attributesIds = @$orderProductsArr['attributes'] ? array_filter(array_map(function ($key, $value) {
                if (strpos($key, 'attributes') !== false)
                    return $value;
            }, array_keys($orderProductsArr['attributes']), $orderProductsArr['attributes'])) : [];


            if (!$orderProductsIds)
                return [
                    'status' => false,
                    'msg' => [
                        'e' => ['Add minim one product.'],
                        'type' => 'warning'
                    ]
                ];


            if (!empty($orderProductsIds)) {
                $products = ProductsItemsId::whereIn('id', $orderProductsIds)
                    ->where('active', 1)
                    ->get();


                if ($products->isNotEmpty())
                    foreach ($products as $product) {


                        if ($product->isVariationProduct) {
                            $orderProductsVariationsIds = array_unique(@$orderProductsArr['variation_id'][$product->id] ?: []);

//                            dd(@$orderProductsArr);

                            $variationsDetails = ProductsItemsVariationsDetailsId::where('products_item_id', $product->id)
                                ->whereIn('id', $orderProductsVariationsIds)
                                ->get();

                            if ($variationsDetails->isNotEmpty()) {
                                foreach ($variationsDetails as $variationsDetail) {

                                    $variationIteration = array_search($variationsDetail->id, $orderProductsVariationsIds);
                                    $countItems = @$orderProductsArr['count'][$product->id][$variationIteration] ?: 1;
                                    $currVariationAttributesIds = array_filter(array_map(function ($key, $value) use ($product, $variationIteration) {
                                        if (@$value[$product->id][$variationIteration])
                                            return $value[$product->id][$variationIteration];
                                    }, array_keys($attributesIds), $attributesIds));

                                    $finallyProductQtyStock = $variationsDetail->finStock;

                                    if ((!is_numeric($finallyProductQtyStock) && $finallyProductQtyStock) || (is_numeric($finallyProductQtyStock) && $countItems <= $finallyProductQtyStock)) {
                                        $price = $variationsDetail->sale_price ? $variationsDetail->sale_price : $variationsDetail->price;
                                        $variationAttributesOptions = OrdersHelpers::formatAttributesForStoreCartProduct($product, $variationsDetail, $currVariationAttributesIds);
                                        $cartAttributes = array_filter($variationAttributesOptions->cart_attributes);
                                        $cartAttributesIds = array_filter($variationAttributesOptions->cart_attributes_ids);

                                        $cartProductInfo = [
                                            'name' => $product->globalName ? $product->globalName->name : '',
                                            'sku' => $variationsDetail->sku,
                                            'slug' => $product->slug,
                                            'firstFile' => getItemFile($variationsDetail->file, 'small'),
                                            'allFiles' => getItemFile($variationsDetail->file, 'small', true, 'allFiles'),
                                            'finPrice' => [
                                                'minPrice' => @$variationsDetail->sale_price ? (float)$variationsDetail->sale_price : null,
                                                'maxPrice' => @$variationsDetail->price ? (float)$variationsDetail->price : null
                                            ]
                                        ];


                                        $cartProductsData[] = [
                                            'products_item_id' => $product->id,
                                            'products_variation_item_id' => $variationsDetail->id,
                                            'product_name' => @$product->globalName->name,
                                            'quantity' => $countItems,
                                            'amount' => $price,
                                            'attributes' => $cartAttributes,
                                            'attributes_ids' => $cartAttributesIds,
                                            'info' => json_encode($cartProductInfo, true)
                                        ];
                                    } else
                                        $errorMessages["order_products[count][{$product->id}][{$variationIteration}]"] = is_numeric($finallyProductQtyStock) && $countItems > $finallyProductQtyStock ? 'Please, select quantity lower or equal than ' . $finallyProductQtyStock : 'Product is out of stock';
                                }
                            }

                        } else {
                            $countItems = @$orderProductsArr['count'][$product->id] ?: 1;
                            $finallyProductQtyStock = $product->finStock;

                            if ((!is_numeric($finallyProductQtyStock) && $finallyProductQtyStock) || (is_numeric($finallyProductQtyStock) && $countItems <= $finallyProductQtyStock)) {
                                $price = $product->sale_price ? $product->sale_price : $product->price;

                                $cartProductInfo = [
                                    'name' => $product->globalName ? $product->globalName->name : '',
                                    'sku' => $product->sku,
                                    'slug' => $product->slug,
                                    'firstFile' => getItemFile($product->file, 'small'),
                                    'allFiles' => getItemFile($product->file, 'small', true, 'allFiles'),
                                    'finPrice' => [
                                        'minPrice' => @$product->sale_price ? (float)$product->sale_price : null,
                                        'maxPrice' => @$product->price ? (float)$product->price : null
                                    ]
                                ];

                                $cartProductsData[] = [
                                    'products_item_id' => $product->id,
                                    'products_variation_item_id' => null,
                                    'product_name' => @$product->globalName->name,
                                    'quantity' => $countItems,
                                    'amount' => $price,
                                    'attributes' => [],
                                    'attributes_ids' => [],
                                    'info' => json_encode($cartProductInfo, true)
                                ];
                            } else
                                $errorMessages["order_products[count][{$product->id}]"] = is_numeric($finallyProductQtyStock) && $countItems > $finallyProductQtyStock ? 'Please, select quantity lower or equal than ' . $finallyProductQtyStock : 'Product is out of stock';
                        }
                    }
            }

            if (!empty($errorMessages))
                return [
                    'status' => false,
                    'msg' => [
                        'e' => $errorMessages,
                        'type' => 'warning'
                    ],
                    'orderValidator' => true
                ];

            if (empty($cartProductsData))
                return [
                    'status' => false,
                    'msg' => [
                        'e' => ['Add minim one product.'],
                        'type' => 'warning'
                    ]
                ];
        }

        $item = Cart::where('id', $id)
            ->where(function ($q) {
                $q->where(function ($q) {
                    $q->whereNull('user_id');
                    $q->orWhere('is_draft', 1);
                });
                $q->orWhere(function ($q) {
                    $q->whereNotNull('user_id');
                    $q->where('is_draft', 0);
                });
            })->first();

        $enableEditing = @$item && @$item->status != 'failed' || !@$item;


        if (is_null($item)) {
            if ($isDraftOrder)
                $item = new Cart();
            else
                return [
                    'status' => false,
                    'msg' => [
                        'e' => ["Add minim one product."],
                        'type' => 'warning'
                    ]
                ];
        }

        if (@$id && (!@$item->is_draft || @$item->user_id)) {
            if (is_null($item->status))
                return [
                    'status' => false,
                    'msg' => [
                        'e' => ["Item can't be update, until it is in progress"],
                        'type' => 'warning'
                    ]
                ];

            if ($item->status == 'cancelled')
                return [
                    'status' => false,
                    'msg' => [
                        'e' => ["Item can't be update, it was cancelled"],
                        'type' => 'warning'
                    ]
                ];
        }

//        if ($isPickupMethod) {
//            $pickupStore = StoresId::where('for_pickup', 1)
//                ->where('country_id', $pickupCountryId)
//                ->where('region_id', $pickupRegionId)
//                ->find($pickupAddressId);
//        }

        $changeDraftStatus = @$item->is_draft && !$isDraftOrder;
        $changeOrderStatus = $item->status != $request->get('status', null);

        $item->status = $request->get('status', null);
        $item->online_payment_name = null;
        $item->shipping_method_id = null;
        $item->payment_method = $request->get('payment_method', 'cash');
        $item->email = $request->get('email', null);
        $item->phone = $request->get('phone', null);
        $item->bank_id_transaction = null;
        $item->order_currency = $defaultCurrency;
        $item->order_lang = LANG;
        $item->details = [
            'name' => $request->get('name', null),
            'address' => $request->get('shipping_address', null),
            'zip_code' => $request->get('zip_code', null),
            'comment' => $request->get('message', null),
        ];

        if ($enableEditing) {
            $item->country_id = $isShippingMethod ? $shippingCountryId : null;
            $item->region_id = $isShippingMethod ? $shippingRegionId : null;
            $item->city = $isShippingMethod ? $request->get('shipping_city', null) : null;
            $item->shipping_pickup_address_id = $isPickupMethod ? $pickupAddressId : null;
            $item->shipping_amount = $isShippingMethod ? ($request->get('shipping_amount', null) > 0 ? $request->get('shipping_amount', null) : null) : null;
            $item->shipping_details = $isShippingMethod ? [
                'method_type' => $request->get('shipping_method', null),
                'name' => $request->get('shipping_method', null) ? __("{$this->moduleName}::e.shipping_method_{$request->get('shipping_method', null)}") : null
            ] : null;
            $item->pickup_details = $isPickupMethod ? [
                'country' => @$pickupStore->country->name,
                'region' => @$pickupStore->region->name,
                'name' => @$pickupStore->globalName->name,
                'address' => @$pickupStore->globalName->address,
                'fullCountry' => @$pickupStore->country,
                'fullRegion' => @$pickupStore->region,
                'fullPickupStore' => @$pickupStore
            ] : null;
        }
        if ($item->status != 'finished')
            $item->order_date = strtotime($request->get('order_date', null)) ? carbon($request->get('order_date')) : now();

        $item->user_ip = $request->ip();
        $item->is_fast_buy = 0;
        $item->seen = 1;
        $item->is_draft = $isDraftOrder ? 1 : 0;
        $item->save();

        if (!$item->order_currency) {
            $item->order_currency = $defaultCurrency;
            $item->save();
        }

        if ($isDraftOrder) {
            $item->order_id = OrdersHelpers::orderIdGenerator($item->id, $item->order_id);
            $item->save();

            foreach ($cartProductsData as $data) {
                $existProduct = CartProducts::where('cart_id', $item->id)
                    ->where('products_item_id', $data['products_item_id'])
                    ->where('products_variation_item_id', $data['products_variation_item_id'])
                    ->first();

                $data['cart_id'] = $item->id;

                if (is_null($existProduct)) {
                    $cartProduct = CartProducts::create($data);
                    $freshAddedCartProducts->push($cartProduct);
                } else
                    $existOrderProductsIds[] = $existProduct->id;
            }

            OrdersHelpers::updateProductsQtyOnCheckout($freshAddedCartProducts);
        }

        $allCartProducts = OrdersHelpers::getCartProducts($item, -1);

        if (!$isDraftOrder && $changeDraftStatus && $allCartProducts->outOfStockProductsIds->isNotEmpty())
            return [
                'status' => false,
                'scrollToProducts' => true,
                'outOfStockProducts' => $allCartProducts->outOfStockProductsIds,
                'msg' => [
                    'e' => ['Some products are no longer in stock. Delete them from the order to continue.'],
                    'type' => 'warning'
                ],
            ];

        $cartCount = @$allCartProducts->count ?: 0;
        $cartSubtotal = @$allCartProducts->total ?: 0;
        $cartProducts = @$allCartProducts->products ?: collect();
        $cartShippingAmount = @$item->shipping_amount >= 0 ? $item->shipping_amount : 0;

        $cartDiscountAmount = 0;

        $validateCoupon = CouponsHelpers::validateCoupon(@$item->discount->code, getDefaultCurrency(), $cartSubtotal);
        if (!is_array($validateCoupon) && !@$validateCoupon->allow_free_shipping) {
            if ($validateCoupon->type == 'fixed')
                $cartDiscountAmount = $validateCoupon->amount;
            else
                $cartDiscountAmount = $cartSubtotal * $validateCoupon->amount / 100;
        } elseif (!is_array($validateCoupon) && @$validateCoupon->allow_free_shipping)
            $cartShippingAmount = 0;

        $cartSubtotalWithCoupon = $cartDiscountAmount > 0 ? $cartSubtotal - $cartDiscountAmount : 0;
        $cartTotal = ($cartSubtotalWithCoupon > 0 ? $cartSubtotalWithCoupon : $cartSubtotal) + $cartShippingAmount;

        try {
            $view = view("{$this->path}.templates.orderProductsTableBody", [
                'cartProducts' => $cartProducts,
                'orderCurrency' => $defaultCurrency
            ])->render();
        } catch (\Throwable $e) {
            $view = '';
        }

        if (!$isDraftOrder) {
            if ($changeDraftStatus || @$item->is_draft)
                OrdersHelpers::updateProductsPriceOnCheckout($item, $cartProducts);


            if ($changeDraftStatus && $item->status != 'cancelled' && $item->discount)
                CouponsHelpers::updateCouponOnCheckout($item->discount, !is_array($validateCoupon) && @$validateCoupon->allow_free_shipping ? 'free_coupon' : null);

//        Update products quantity

            if ($item->status == 'cancelled') {
                OrdersHelpers::updateProductsQtyOnCheckout($cartProducts, true);
                CouponsHelpers::updateCouponOnCheckout(@$item->discount, !is_array($validateCoupon) && @$validateCoupon->allow_free_shipping ? 'free_coupon' : null, false);
            }

            if ($item->status == 'finished')
                OrdersHelpers::updateProductsSalesRating($cartProducts);

//        Update products quantity

            if ($changeOrderStatus) {
                $sendMailData = [
                    'item' => $item,
                    'cartProducts' => $cartProducts,
                    'useShipping' => @$item->shippingMethod || @$item->shipping_details,
                    'usePickup' => @$item->pickupStore || @$item->pickup_details,
                    'cartSubtotal' => $cartSubtotal,
                    'cartDiscountAmount' => $cartDiscountAmount,
                    'cartSubtotalWithCoupon' => $cartSubtotalWithCoupon,
                    'shippingAmount' => $cartShippingAmount,
                    'cartTotal' => $cartTotal,
                ];

                try {
                    helpers()->sendMail($item->email, @$item->details->name, $sendMailData, 'adminOrderStatus', parent::globalSettings(), 'Change order status on ' . url('/'));
                } catch (\Exception $e) {
                }
            }
        }

        helpers()->logActions($currComponent, $item, @$item->globalName->name, $isDraftOrder && $item->wasRecentlyCreated ? 'create' : 'edit');

        return [
            'status' => true,
            'msg' => [
                'e' => $isDraftOrder ? 'Products successful added. Your order save as draft.' : 'Order was successful edited.',
                'type' => $isDraftOrder ? 'info' : 'success'
            ],
            'view' => $view,
            'isDraft' => $isDraftOrder,
            'cartCount' => $cartCount,
            'cartSubtotal' => $cartSubtotal,
            'cartSubtotalStr' => formatPrice($cartSubtotal, $defaultCurrency),
            'cartTotal' => $cartTotal,
            'cartTotalStr' => formatPrice($cartTotal, $defaultCurrency),
            'shippingAmount' => $cartShippingAmount,
            'shippingAmountStr' => formatPrice($cartShippingAmount, $defaultCurrency),
            'cartDiscountAmount' => $cartDiscountAmount,
            'cartDiscountAmountStr' => formatPrice($cartDiscountAmount, $defaultCurrency),
            'cartSubtotalWithCoupon' => $cartSubtotalWithCoupon,
            'cartSubtotalWithCouponStr' => formatPrice($cartSubtotalWithCoupon, $defaultCurrency),
            'orderId' => $item->id,
            'generatedOrderId' => $item->order_id,
            'editUrl' => !$id ? customUrl(['admin', $currComponent->slug, 'edit', $item->id]) : '',
            'existProductsIds' => $existOrderProductsIds,
            'existProductsMsg' => [
                'e' => 'Focused products already added to order. You can change quantity or delete them.',
                'type' => 'info'
            ],
            'redirect' => !$isDraftOrder ? url(LANG, ['admin', $currComponent->slug, 'edit', $item->id]) : '',
            'freeShippingStr' => !is_array(@$validateCoupon) && @$validateCoupon->allow_free_shipping ? __("{$this->moduleName}::e.free_shipping_coupon") : ''
        ];
    }

    public function checkCoupon(Request $request, $id)
    {
        $couponCode = $request->get('couponCode', null);
        $defaultCurrency = getDefaultCurrency();

        $item = Cart::where('id', (int)$id)
            ->where(function ($q) {
//                $q->whereNull('user_id');
//                $q->orWhere('is_draft', 1);
            })->first();

        if (is_null($item) || (!is_null($item) && @$item->cartProducts->isEmpty()))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Add minim one product.'],
                    'type' => 'warning'
                ]
            ]);

        $allCartProducts = OrdersHelpers::getCartProducts($item, -1);
        $cartSubtotal = @$allCartProducts->total ?: 0;
        $cartShippingAmount = @$item->shipping_amount >= 0 ? $item->shipping_amount : 0;

        $validateCoupon = CouponsHelpers::validateCoupon($couponCode, getDefaultCurrency(), $cartSubtotal);

        if (is_array($validateCoupon))
            return response()->json([
                'status' => false,
                'msg' => $validateCoupon
            ]);

        $cartDiscountAmount = 0;

        if (!$validateCoupon->allow_free_shipping) {
            if ($validateCoupon->type == 'fixed')
                $cartDiscountAmount = $validateCoupon->amount;
            else
                $cartDiscountAmount = $cartSubtotal * $validateCoupon->amount / 100;
        } else
            $cartShippingAmount = 0;

        $item->discount_id = $validateCoupon->id;
//        $item->shipping_amount = $cartShippingAmount > 0 ? $cartShippingAmount : null;
        $item->discount_amount = $cartDiscountAmount > 0 ? $cartDiscountAmount : null;
        $item->discount_details = [
            'code' => $validateCoupon->code,
            'type' => $validateCoupon->type,
            'amount' => $validateCoupon->amount,
            'allow_free_shipping' => $validateCoupon->allow_free_shipping,
            'expiry_date' => $validateCoupon->expiry_date,
            'min_subtotal' => $validateCoupon->min_subtotal,
            'max_subtotal' => $validateCoupon->max_subtotal
        ];
        $item->save();

        $cartSubtotalWithCoupon = $cartSubtotal - $cartDiscountAmount;
        $cartSubtotalWithCoupon = $cartSubtotalWithCoupon >= 0 ? $cartSubtotalWithCoupon : 0;
        $cartTotal = $cartSubtotalWithCoupon + $cartShippingAmount;

        $view = '';

        try {
            $view = view("{$this->path}.templates.couponsBlock", ['item' => $item])->render();
        } catch (\Throwable $e) {
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => 'Coupon was successful applied.',
                'type' => 'info'
            ],
            'cartSubtotal' => $cartSubtotal,
            'cartSubtotalStr' => formatPrice($cartSubtotal, $defaultCurrency),
            'cartTotal' => $cartTotal,
            'cartTotalStr' => formatPrice($cartTotal, $defaultCurrency),
            'shippingAmount' => $cartShippingAmount,
            'shippingAmountStr' => formatPrice($cartShippingAmount, $defaultCurrency),
            'cartDiscountAmount' => $cartDiscountAmount,
            'cartDiscountAmountStr' => formatPrice($cartDiscountAmount, $defaultCurrency),
            'cartSubtotalWithCoupon' => $cartSubtotalWithCoupon,
            'cartSubtotalWithCouponStr' => formatPrice($cartSubtotalWithCoupon, $defaultCurrency),
            'view' => $view,
            'appliedCoupons' => $couponCode,
            'freeShippingStr' => @$validateCoupon->allow_free_shipping ? __("{$this->moduleName}::e.free_shipping_coupon") : ''
        ]);
    }

    public function deleteCoupon(Request $request, $id)
    {
        $couponId = $request->get('couponId', null);
        $defaultCurrency = getDefaultCurrency();

        $item = Cart::where('id', $id)
            ->where('discount_id', $couponId)
            ->where(function ($q) {
                $q->whereNull('user_id');
                $q->orWhere('is_draft', 1);
            })->first();

        if (is_null($item))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Order not exist.'],
                    'type' => 'warning'
                ]
            ]);

        $item->discount_id = null;
        $item->discount_details = null;
        $item->discount_amount = null;
        $item->save();

        $allCartProducts = OrdersHelpers::getCartProducts($item, -1);
        $cartSubtotal = @$allCartProducts->total ?: 0;
        $cartSubtotalWithCoupon = 0;
        $cartShippingAmount = @$item->shipping_amount >= 0 ? $item->shipping_amount : 0;
        $cartTotal = $cartSubtotal + $cartShippingAmount;

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => 'Coupon was successful removed.',
                'type' => 'info'
            ],
            'cartSubtotal' => $cartSubtotal,
            'cartSubtotalStr' => formatPrice($cartSubtotal, $defaultCurrency),
            'cartTotal' => $cartTotal,
            'cartTotalStr' => formatPrice($cartTotal, $defaultCurrency),
            'shippingAmount' => $item->shipping_amount,
            'shippingAmountStr' => formatPrice($item->shipping_amount, $defaultCurrency),
            'cartDiscountAmount' => 0,
            'cartDiscountAmountStr' => formatPrice(0, $defaultCurrency),
            'cartSubtotalWithCoupon' => $cartSubtotalWithCoupon,
            'cartSubtotalWithCouponStr' => formatPrice($cartSubtotalWithCoupon, $defaultCurrency),
        ]);
    }

    public function destroyOrderProducts(Request $request, $id)
    {
        $defaultCurrency = getDefaultCurrency();

        $product = CartProducts::whereHas('cart', function ($q) {
            $q->where('status', 'checkout');
        })->find($request->get('productId', null));

        if (is_null($product))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => 'Product not exist.',
                    'type' => 'error'
                ]
            ]);

        OrdersHelpers::updateProductQtyOnCheckout($product, true);

        $cart = $product->cart;
        $product->delete();

        $allCartProducts = OrdersHelpers::getCartProducts($cart, -1);
        $cartSubtotal = @$allCartProducts->total ?: 0;
        $cartShippingAmount = @$cart->shipping_amount >= 0 ? $cart->shipping_amount : 0;
        $cartDiscount = @$cart->discount;
        $cartDiscountAmount = 0;

        if ($cartDiscount) {
            $validateCoupon = CouponsHelpers::validateCoupon(@$cart->discount->code, $defaultCurrency, $cartSubtotal);

            if (!$validateCoupon->allow_free_shipping) {
                if ($validateCoupon->type == 'fixed')
                    $cartDiscountAmount = $validateCoupon->amount;
                else
                    $cartDiscountAmount = $cartSubtotal * $validateCoupon->amount / 100;
            } else
                $cartShippingAmount = 0;
        }

        $cartSubtotalWithCoupon = $cartSubtotal - $cartDiscountAmount;
        $cartSubtotalWithCoupon = $cartSubtotalWithCoupon >= 0 ? $cartSubtotalWithCoupon : 0;
        $cartTotal = $cartSubtotalWithCoupon + $cartShippingAmount;

        $cartProductsCount = $allCartProducts->products->count();

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => 'Product was successful deleted from order.',
                'type' => 'info'
            ],
            'count' => $cartProductsCount,
            'cartSubtotal' => $cartSubtotal,
            'cartSubtotalStr' => formatPrice($cartSubtotal, $defaultCurrency),
            'cartTotal' => $cartTotal,
            'cartTotalStr' => formatPrice($cartTotal, $defaultCurrency),
            'shippingAmount' => $cartShippingAmount,
            'shippingAmountStr' => formatPrice($cartShippingAmount, $defaultCurrency),
            'cartDiscountAmount' => $cartDiscountAmount,
            'cartDiscountAmountStr' => formatPrice($cartDiscountAmount, $defaultCurrency),
            'cartSubtotalWithCoupon' => $cartSubtotalWithCoupon,
            'cartSubtotalWithCouponStr' => formatPrice($cartSubtotalWithCoupon, $defaultCurrency),
        ]);
    }

    public function widgetsCountItems()
    {
        return Cart::whereNotNull('status')->count();
    }

    public function countItems()
    {
        return Cart::whereNotNull('status')
            ->where('seen', 0)->count();
    }

    public function exportList(Request $request)
    {
        if ($request->method() != 'POST' && !$request->ajax())
            abort(404);

        $exportsDir = storage_path('app/exports');
        $fileName = 'order-list_' . time() . '.csv';
        $filePath = $exportsDir . '/' . $fileName;

        if (!file_exists($exportsDir))
            File::makeDirectory($exportsDir, 0775, true, true);

        $filterParams = array_filter($request->except('page'));


        $handler = fopen($filePath, 'w+');
        $fileSize = filesize($filePath);
        $headerArray = $this->generateCsvHeaderContent();

        if ($fileSize == 0)
            fputcsv($handler, $headerArray);

        Cart::where(function ($q) use ($filterParams, $request) {
            if (array_key_exists('payment_method', $filterParams) && is_array($filterParams['payment_method']))
                $q->where('payment_method', $filterParams['payment_method']);
            elseif (array_key_exists('status', $filterParams) && is_array($filterParams['status']) && !in_array('progress', $filterParams['status']))
                $q->whereNotNull('payment_method');

            if (array_key_exists('status', $filterParams) && is_array($filterParams['status']))
                $q->where(function ($q) use ($filterParams) {
                    $q->whereIn('status', $filterParams['status']);

                    if (in_array('progress', $filterParams['status']))
                        $q->orWhereNull('status');
                });
            else {
                $q->where(function ($q) {
                    $q->whereNotNull('status');
                    $q->orWhere('is_draft', 1);
                });
            }

            if (array_key_exists('order_id', $filterParams) && !is_array($filterParams['order_id']))
                $q->where('order_id', $filterParams['order_id']);

            if (array_key_exists('client_email', $filterParams) && !is_array($filterParams['client_email']))
                $q->where('email', $filterParams['client_email']);

            if (array_key_exists('users_ids', $filterParams) && is_array($filterParams['users_ids']))
                $q->whereIn('auth_user_id', $filterParams['users_ids']);

            if (array_key_exists('order_type', $filterParams) && is_array($filterParams['order_type']))
                $q->where(function ($q) use ($filterParams) {
                    if (in_array('simple', $filterParams['order_type'])) {
                        $q->whereNotNull('user_id');
                        $q->where('is_draft', 0);
                        $q->where('is_fast_buy', 0);
                    }

                    if (in_array('fast', $filterParams['order_type'])) {
                        $q->where('is_fast_buy', 1);
                        $q->whereNotNull('user_id');
                    }

                    if (in_array('admin', $filterParams['order_type']))
                        $q->whereNull('user_id');

                    if (in_array('draft', $filterParams['order_type'])) {
                        $q->whereNull('user_id');
                        $q->where('is_draft', 1);
                    }

                    if (in_array('new', $filterParams['order_type'])) {
                        $q->where('seen', 0);
                        $q->whereNotNull('user_id');
                    }
                });

            if (array_key_exists('start_date', $filterParams) && !array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']))
                $q->whereDate('order_date', '>=', date('Y-m-d', strtotime($filterParams['start_date'])));
            elseif (!array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['end_date']))
                $q->whereDate('order_date', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));
            elseif (array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']) && !is_array($filterParams['end_date']))
                $q->whereDate('order_date', '>=', date('Y-m-d', strtotime($filterParams['start_date'])))->whereDate('order_date', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));

        })
            ->orderBy('order_date', 'desc')
            ->orderBy('is_fast_buy')
            ->chunk(200, function ($orders) use ($handler) {
                foreach ($orders as $order) {
                    foreach ($order->cartProducts as $key => $product) {
                        if (!$key) {
                            $content = [
                                $order->order_id,
                                @$order->details->name,
                                $order->phone,
                                $order->email,
                                @$order->details->address,
                                $order->amount,
                                $order->delivery_amount,
                                $order->amount + $order->delivery_amount,

                                $order->payment_method,
                                (!$order->user_id && $order->is_draft) ? 'draft' : (!$order->user_id ? 'admin' : (@$order->is_fast_buy ? 'fast' : 'simple')),
                                (@$order->pickup_details) ? 'Pickup' : @$order->shipping_details->name,
                                $order->status,
                                (strtotime($order->order_date)) ? date('d-m-Y H:i', strtotime($order->order_date)) : '-',


                                $product->info->name,
                                $product->quantity,
                                $product->amount,
                            ];
                            fputcsv($handler, $content);
                        } else {
                            $content = [
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                @$product->info->name,
                                $product->quantity,
                                $product->amount,
                            ];
                            fputcsv($handler, $content);
                        }
                    }
                }
            });
        fclose($handler);

        return response()->json([
            'status' => true,
            'redirect' => adminUrl([parent::currComponent()->slug, 'downloadOrdersList']) . '?file=' . $fileName,
            'msg' => [
                'e' => 'Data is being exported to csv file.',
                'type' => 'info',
            ],

        ]);
    }

    public function downloadOrdersList(Request $request)
    {
        $fileName = $request->get('file', null);

        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];

        $exportsDir = storage_path('app/exports');
        $file = "{$exportsDir}/{$fileName}";
        $newFileName = "order-list_" . date('d-m-Y') . ".csv";


        if (!$fileName || !file_exists($file))
            abort(404);

        $authUser = auth()->guard('admin')->user();
        $authUser->update([
            'has_exported_products' => 0
        ]);

        return response()->download($file, $newFileName, $headers)->deleteFileAfterSend(true);
    }

    private function generateCsvHeaderContent()
    {
        $response = [
            'OrderId',
            'User name',
            'User phone',
            'User email',
            'User address',
            'Products Price',
            'Delivery Price',
            'Total Price',
            'Payment method',
            'Order type',
            'Delivery method',
            'Status',
            'Date',
            'Product Name',
            'Product Quantity',
            'Product Price',
        ];

        return $response;
    }

    public function refundMoney(Request $request, $cartid)
    {
        $cart = Cart::find($cartid);

        if ($cart->status == 'checkout')
        {
            if (floatval($request->refundAmount) > 0 && floatVal($request->refundAmount) <= ($cart->amount + $cart->shipping_amount)) {

                $client = new MaibClient();

                $response = $client->getTransactionResult($cart->bank_id_transaction, $_SERVER['SERVER_ADDR']);

                LogsPayment::log('getTransactionResult on refund', $cart->id, 'trans_id: ' . $cart->bank_id_transaction, $response);

                if (isset($response['RESULT']) && $response['RESULT'] === "OK") {
                    //do refund
                    $response = $client->revertTransaction($cart->bank_id_transaction, $request->refundAmount);

                    LogsPayment::log('refund_money', $cart->id, 'trans_id: ' . $cart->bank_id_transaction, $response);

                    if (isset($response['RESULT']) && $response['RESULT'] === "OK") {
                        $cart->update([
                            //'status' => 'cancelled',
                            'return_amount' => $cart->return_amount + $request->refundAmount
                        ]);

                        return response()->json([
                            'status' => true,
                            'msg' => [
                                'e' => ['Success refunded'],
                                'type' => 'info'
                            ]
                        ]);
                    }

                } else {
                    return response()->json([
                        'status' => false,
                        'msg' => [
                            'e' => ['Plata nu a fost finalizata si nu puteti face retur'],
                            'type' => 'warning'
                        ]
                    ]);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Incorect refund amount'],
                        'type' => 'warning'
                    ]
                ]);
            }
        } else {
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Statutul comenzii trebuie sa fie Checkout pentru a face retur'],
                    'type' => 'warning'
                ]
            ]);
        }

    }

    public function finishTransaction(Request $request, $cartid)
    {
        $cart = Cart::find($cartid);

        if ($cart->status == 'processing')
        {
            $client = new MaibClient();

            $response = $client->getTransactionResult($cart->bank_id_transaction, $_SERVER['SERVER_ADDR']);
            LogsPayment::log('getTransactionResult on finish', $cart->id, 'trans_id: ' . $cart->bank_id_transaction, $response);

            if (isset($response['RESULT']) && $response['RESULT'] === "OK")
            {
                $string_cart_desc = 'sushi - Comanda ' . $cart->order_id;
                $response = $client->makeDMSTrans($cart->bank_id_transaction,  numberFormat($cart->amount + $cart->shipping_amount), 498, $_SERVER['REMOTE_ADDR'],htmlspecialchars_decode($string_cart_desc), LANG);         

                if (isset($response['RESULT']) && $response['RESULT'] === "OK") {

                    $cart->update([
                        'payment_bank_process' => 'T',
                        'payment_bank_status' => $response['RESULT'],
                    ]);

                    return response()->json([
                        'status' => true,
                        'msg' => [
                            'e' => ['Trazacția a fost finalizată'],
                            'type' => 'info'
                        ]
                    ]);
                }

            }else{
                $cart->update([
                    'payment_bank_process' => 'C',
                    'payment_bank_status' => $response['RESULT'],
                ]);

                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Trazacția nu a fost finalizată'],
                        'type' => 'warning'
                    ]
                ]);
            }            
        } else {
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Statutul comenzii trebuie sa fie în Processing pentru a finisa tranzacția'],
                    'type' => 'warning'
                ]
            ]);
        }

    }

    public function checkPayment(Request $request, $cartid)
    {
        $item = Cart::where('id', $cartid)->firstOrFail();

        if (!empty($item->bank_id_transaction)) {
            //$start_date = new \DateTime($item->order_date);
            //$since_start = $start_date->diff(new \DateTime());
            //if($since_start->d == 0 && $since_start->h == 0 && $since_start->i < 40)
            //{
            $client = new MaibClient();

            //TEST close BusinessDay
            //$resp = $client->closeDay();
            //dd($resp);

            $response = $client->getTransactionResult($item->bank_id_transaction, $request->ip());

            if (isset($response['RESULT']) && $response['RESULT'] === "OK")
            {
                $respMsg = 'CheckPayment response: ' . json_encode($response);
                if ($item->status == 'processing') {
                    $string_cart_desc = $item->store_id == 1 ? 'DMG' : 'FD';
                    $string_cart_desc .= ' - Comanda ' . $item->order_id;
                    $respMakeDMSTrans = $client->makeDMSTrans($item->bank_id_transaction, numberFormat($item->amount + $item->shipping_amount), 498, $_SERVER['REMOTE_ADDR'], htmlspecialchars_decode($string_cart_desc), LANG);
                    if (isset($respMakeDMSTrans['RESULT']) && $respMakeDMSTrans['RESULT'] == 'OK') {
                        $item->update([
                            'status' => 'checkout',
                            'payment_bank_process' => 'T',
                            'payment_bank_status' => $respMakeDMSTrans['RESULT'],
                        ]);
                    }else{
                        $item->update([
                            'payment_bank_process' => 'T',
                            'payment_bank_status' => $response['RESULT'],
                        ]);
                    }

                    $respMsg .= 'MakeTransaction response: ' . json_encode($respMakeDMSTrans);


//                    try {
//                          TODO: send to iiko and email to client
//                        //helpers()->sendMail($cart->email, $cart->details->name, ['cart' => $cartProductsAndTotals], 'checkoutForm', parent::globalSettings(), __('front.new_order_on'));
//                    } catch (\Exception $e) {
//                    }
                }


                //$cartProductsAndTotals = OrdersHelpers::getCartProductsAndTotals($item);

//                    if($item->auth_user_id > 0)
//                    {
//                        $user = Users::find($item->auth_user_id);
//
//                        if($cartProductsAndTotals->cashback > 0) $user->increment('cashback_ballance', $cartProductsAndTotals->cashback);
//
//                        if($item->discount_amount > 0) $user->decrement('cashback_ballance', $item->discount_amount);
//                    }

                return response()->json([
                    'status' => true,
                    'msg' => [
                        'e' => $respMsg,
                        'type' => 'info'
                    ],
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => 'Bank response: ' . json_encode($response),
                        'type' => 'info'
                    ],
                ]);
            }

        }else{
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Trans id not found'],
                    'type' => 'warning'
                ]
            ]);
        }
    }

    public function checkOrderInIiko(Request $request, $cart_id)
    {
        $cart = Cart::find($cart_id);

        $restaurant = RestaurantsId::where('id', $cart->restaurant_id)->first();

        $iikoClient = new IikoClient($restaurant);
        $resp = $iikoClient->checkOrder(null, $cart);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => json_encode($resp),
                'type' => 'info'
            ],
        ]);
    }

    public function sendOrderToIiko(Request $request, $cart_id)
    {
        $cart = Cart::find($cart_id);
        $restaurant = RestaurantsId::where('id', $cart->restaurant_id)->first();

        $iikoClient = new IikoClient($restaurant);
        $order = $iikoClient->createOrder($cart);

        if(isset($order['orderInfo']) && !empty($order['orderInfo']))
        {
            $cart->update([
                'iiko_id' => $order['orderInfo']['id'],
                'iiko_status' => $order['orderInfo']['creationStatus']
            ]);

            //if(isset($order['orderInfo']['id']) && !empty($order['orderInfo']['id']))
                //$iikoClient->closeOrder($cart);

        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => json_encode($order),
                'type' => 'info'
            ],
        ]);
    }
}