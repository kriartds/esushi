@if(isset($filterParams))
    <div class="right-col">
        <button type="button" class="button gray filter-btn">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12"><g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M1 1h14"></path></g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M4 6h8"></path></g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M6 11h4"></path></g></g></svg>
            Filters
            <span class="count">{{isset($items) && ((!empty($filterParams) || empty($filterParams)) && $items->total() > 0) ? $items->total() : ""}}</span>
        </button>
    </div>
    
    <div class="filter form-block">
        <form method="post" action="{{url(LANG, ['admin', $currComponent->slug, 'filter'])}}"
              class="filter-form"
              id="filter-form">
            <div class="field-row-wrap">
                <div class="col middle">
                    <div class="with-bg">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="order_id">{{__("{$moduleName}::e.order_id")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="order_id" id="order_id"
                                    value="{{ !empty($filterParams) && array_key_exists('order_id', $filterParams) ? $filterParams['order_id'] : '' }}">
                            </div>
                        </div>
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="client_email">{{__("{$moduleName}::e.email")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="client_email" id="client_email"
                                    value="{{ !empty($filterParams) && array_key_exists('client_email', $filterParams) ? $filterParams['client_email'] : '' }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col middle">
                    <div class="with-bg">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="payment_method">{{__("{$moduleName}::e.payment_method")}}</label>
                            </div>
                            <div class="field-wrap">
                                <select autocomplete="off" name="payment_method[]" id="payment_method"
                                        class="select2" multiple>
                                    <option value="cash" {{!empty($filterParams) && array_key_exists('payment_method', $filterParams) && in_array('cash', $filterParams['payment_method']) ? 'selected' : ''}}>
                                        Cash
                                    </option>
                                    <option value="card" {{!empty($filterParams) && array_key_exists('payment_method', $filterParams) && in_array('card', $filterParams['payment_method']) ? 'selected' : ''}}>
                                        Card
                                    </option>
                                    <option value="online" {{!empty($filterParams) && array_key_exists('payment_method', $filterParams) && in_array('online', $filterParams['payment_method']) ? 'selected' : ''}}>
                                        Online
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="status">{{__("{$moduleName}::e.status")}}</label>
                            </div>
                            <div class="field-wrap">
                                <select autocomplete="off" name="status[]" id="status"
                                        class="select2" multiple>
                                    <option value="progress" {{!empty($filterParams) && array_key_exists('status', $filterParams) && in_array('progress', $filterParams['status']) ? 'selected' : ''}}>
                                        {{__("{$moduleName}::e.in_progress")}}
                                    </option>
                                    <option value="checkout" {{!empty($filterParams) && array_key_exists('status', $filterParams) && in_array('checkout', $filterParams['status']) ? 'selected' : ''}}>
                                        {{__("{$moduleName}::e.checkout")}}
                                    </option>
                                    <option value="cancelled" {{!empty($filterParams) && array_key_exists('status', $filterParams) && in_array('cancelled', $filterParams['status']) ? 'selected' : ''}}>
                                        {{__("{$moduleName}::e.cancelled")}}
                                    </option>
                                    <option value="approved" {{!empty($filterParams) && array_key_exists('status', $filterParams) && in_array('approved', $filterParams['status']) ? 'selected' : ''}}>
                                        {{__("{$moduleName}::e.approved")}}
                                    </option>
                                    <option value="finished" {{!empty($filterParams) && array_key_exists('status', $filterParams) && in_array('finished', $filterParams['status']) ? 'selected' : ''}}>
                                        {{__("{$moduleName}::e.finished")}}
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col small">
                    <div class="with-bg">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="order_type">{{__("{$moduleName}::e.order_type")}}</label>
                            </div>
                            <div class="field-wrap">
                                <select autocomplete="off" name="order_type[]" id="order_type"
                                        class="select2" multiple>
                                    <option value="simple" {{!empty($filterParams) && array_key_exists('order_type', $filterParams) && in_array('simple', $filterParams['order_type']) ? 'selected' : ''}}>
                                        Simple order
                                    </option>
                                    <option value="fast" {{!empty($filterParams) && array_key_exists('order_type', $filterParams) && in_array('fast', $filterParams['order_type']) ? 'selected' : ''}}>
                                        Fast order
                                    </option>
                                    <option value="admin" {{!empty($filterParams) && array_key_exists('order_type', $filterParams) && in_array('admin', $filterParams['order_type']) ? 'selected' : ''}}>
                                        Admin order
                                    </option>
                                    <option value="draft" {{!empty($filterParams) && array_key_exists('order_type', $filterParams) && in_array('draft', $filterParams['order_type']) ? 'selected' : ''}}>
                                        Draft order
                                    </option>
                                    <option value="new" {{!empty($filterParams) && array_key_exists('order_type', $filterParams) && in_array('new', $filterParams['order_type']) ? 'selected' : ''}}>
                                        New order
                                    </option>
                                </select>
                            </div>
                        </div>
                        @if($siteUsers->isNotEmpty())
                            <div class="field-row">
                                <div class="label-wrap">
                                    <label for="users_ids">{{__("{$moduleName}::e.users")}}</label>
                                </div>
                                <div class="field-wrap">
                                    <select autocomplete="off" name="users_ids[]" id="users_ids" class="select2 no-search"
                                            multiple>
                                        @foreach($siteUsers as $user)
                                            <option value="{{$user->id}}" {{ !empty($filterParams) && array_key_exists('users_ids', $filterParams) && in_array($user->id, $filterParams['users_ids']) ? 'selected' : '' }}>
                                                {{$user->name ?: $user->email}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col middle date">
                    <div class="with-bg flexClass">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="start_date">{{__("{$moduleName}::e.from_date")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="start_date" id="start_date" class="datetimepicker"
                                    value="{{ !empty($filterParams) && array_key_exists('start_date', $filterParams) ? date('d-m-Y', strtotime($filterParams['start_date'])) : '' }}">
                            </div>
                        </div>
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="end_date">{{__("{$moduleName}::e.to_date")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="end_date" id="end_date" class="datetimepicker"
                                    value="{{ !empty($filterParams) && array_key_exists('end_date', $filterParams) ? date('d-m-Y', strtotime($filterParams['end_date'])) : '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="field-btn">
                        <button class="half submit-form-btn button blue" data-form-id="filter-form"
                                data-form-event="submit-form">{{__('e.filter')}}
                        </button>
                        <button class=" half submit-form-btn button light-blue" data-form-id="filter-form"
                                data-form-event="refresh-form">{{__('e.reset')}}
                        </button>
                    </div>
                </div>
            </div>


        </form>
    </div>
@endif