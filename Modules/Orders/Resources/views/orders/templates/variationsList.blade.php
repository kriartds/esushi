@if(@$product && @$product->finPrice)
    <div class="chose-product-variation-block" data-product-id="{{$product->id}}" data-increment="{{$increment}}">
        <input type="hidden" name="order_products[id][]" autocomplete="off" value="{{$product->id}}">
        @if(@$product->isVariationProduct)

            <input type="hidden" name="order_products[variation_id][{{$product->id}}][{{$increment}}]"
                   autocomplete="off">
            <input type="hidden" name="variations[{{$product->id}}]" value="{{@$allProductVariations}}"
                   autocomplete="off">
        @endif
        <div class="left-side">
            <div class="product-info">
                <div class="product-id">
                    <span title="Product ID">#{{$product->id}}</span>
                    <span class="withoutId" title="Variation ID"
                          id="variation-id-block-{{$product->id}}--{{$increment}}"></span>
                </div>
                <div class="product-name-img">
                    <div class="product-img">
                        <img src="{{getItemFile($product, 'small')}}"
                             alt="{{$product->slug}}" title="{{@$product->globalName->name}}">
                    </div>
                    <div class="product-name-wrap">

                        <div class="product-name">
                            <span>{{@$product->globalName->name}}</span>
                        </div>

                        <div class="row">
                            <div class="product-price-sale-price-wrap">
                                <div class="product-price-block {{@$product->isVariationProduct ? 'variation-price-block' : ''}}"
                                    id="price-product-block-{{$product->id}}--{{$increment}}">
                                    {!! getProductPrices(@$product, @$defaultCurrency) !!}
                                </div>
                                <div class="product-sale-percentage"
                                    id="product-sale-percentage-{{$product->id}}--{{$increment}}">
                                    @if($product->productPromotion && !$product->isVariationProduct)
                                        <span>(-{{@$product->productPromotion['percentage']}}%)</span>
                                    @endif
                                </div>
                            </div>

                            @if(!$product->isVariationProduct)
                                @if(@$product->globalStock)
                                    <div class="product-stock-block stock inStock info-product-stock"
                                        id="info-product-stock-{{$product->id}}--{{$increment}}">
                                        <span>
                                            @if(is_numeric($product->globalStock) && $displayStock && !$product->isVariationProduct)
                                                {{$product->globalStock}} in stock
                                            @else
                                                In stock
                                            @endif
                                        </span>
                                    </div>
                                @else
                                    <div class="product-stock-block stock outOfStock info-product-stock"
                                        id="info-product-stock-{{$product->id}}--{{$increment}}">
                                        <span>Out of stock</span>
                                    </div>
                                @endif
                            @else
                                <div class="product-stock-block stock info-product-stock"
                                    id="info-product-stock-{{$product->id}}--{{$increment}}">
                                    <span></span>
                                </div>
                            @endif

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="right-side">
            <div class="count-items-block {{@$product->isVariationProduct || !$product->globalStock ? 'hidden' : ''}}"
                 id="count-items-block-{{$product->id}}--{{$increment}}">
                @if(@$product->isVariationProduct)
                    <div class="qty">
                        <input type="text" class="update-product-quantity"
                               id="count-items-{{$product->id}}--{{$increment}}"
                               data-id="{{@$product->id}}"
                               name="order_products[count][{{$product->id}}][{{$increment}}]"
                               autocomplete="off"
                               value="1">
                    </div>
                @else
                    <div class="qty">
                        <input type="text" class="update-product-quantity"
                               id="count-items-{{$product->id}}"
                               name="order_products[count][{{$product->id}}]"
                               autocomplete="off"
                               placeholder="1"
                               data-id="{{@$product->id}}"
                               value="1" {{@$product->finStock && is_numeric($product->finStock) ? "data-max-value={$product->finStock}" : ''}}>
                    </div>
                @endif
            </div>
            <div class="remove-order-product"></div>
        </div>

        @if(@$product->isVariationProduct)
            @if(@$attributes && $attributes->isNotEmpty())
                <div class="product-attributes">
                    @foreach($attributes as $attribute)
                        @php
                            $parent = $attribute['parent'];
                            $children = $attribute['children'];
                        @endphp
                        <div class="attribute-item {{@$parent->type->slug == 'color' ? 'color-fields' : (@$parent->type->slug == 'image' ? 'image-fields' : (@$parent->type->slug == 'radio' ? 'radio-fields' : ''))}}">
                            <div class="title">{{@$parent->globalName->name}}</div>
                            <div class="attribute-item-container" data-item-slug="{{$parent->slug}}" data-item-type="{{@$parent->type->slug}}">
                                @switch(@$parent->type->slug)
{{--                                    @case('color')--}}
{{--                                    @if($children->isNotEmpty())--}}
{{--                                        @foreach($children as $child)--}}
{{--                                            @if(!empty($child->colors))--}}
{{--                                                <div class="attribute-item-wrap">--}}
{{--                                                    <input type="radio"--}}
{{--                                                            name="order_products[attributes][attributes-{{$parent->slug}}][{{$product->id}}][{{$increment}}]"--}}
{{--                                                            autocomplete="off"--}}
{{--                                                            id="attribute-{{$child->slug}}-{{$product->id}}--{{$increment}}"--}}
{{--                                                            value="{{$child->id}}">--}}
{{--                                                    <label for="attribute-{{$child->slug}}-{{$product->id}}--{{$increment}}"--}}
{{--                                                            title="{{@$child->globalName->name}}">--}}
{{--                                                        @foreach($child->colors as $key => $color)--}}
{{--                                                            @if(count($child->colors) > 4)--}}
{{--                                                                <span class="color-attribute"--}}
{{--                                                                        style="background: {{$color}}; width: calc(100% / {{count($child->colors)}});"></span>--}}
{{--                                                            @elseif(count($child->colors) % 2 == 0)--}}
{{--                                                                <span class="color-attribute"--}}
{{--                                                                        style="background: {{$color}}; width: calc(100% / {{count($child->colors) / 2}});"></span>--}}
{{--                                                            @elseif(count($child->colors) == 3)--}}
{{--                                                                <span class="color-attribute"--}}
{{--                                                                        style="background: {{$color}}; width: calc(100% / {{$key < 2 ? 2 : 1}});"></span>--}}
{{--                                                            @else--}}
{{--                                                                <span class="color-attribute"--}}
{{--                                                                        style="background: {{$color}}; width: calc(100% / {{$key + 1}});"></span>--}}
{{--                                                            @endif--}}
{{--                                                        @endforeach--}}
{{--                                                    </label>--}}
{{--                                                </div>--}}
{{--                                            @endif--}}
{{--                                        @endforeach--}}
{{--                                    @endif--}}
{{--                                    @break--}}
                                    @case('image')
                                    @if($children->isNotEmpty())
                                        @foreach($children as $child)
                                            <div class="attribute-item-wrap">
                                                <input type="radio"
                                                        name="order_products[attributes][attributes-{{$parent->slug}}][{{$product->id}}][{{$increment}}]"
                                                        autocomplete="off"
                                                        id="attribute-{{$child->slug}}-{{$product->id}}--{{$increment}}"
                                                        value="{{$child->id}}">
                                                <label for="attribute-{{$child->slug}}-{{$product->id}}--{{$increment}}"
                                                        title="{{@$child->globalName->name}}">
                                                    <img src="{{getItemFile($child, 'small')}}"
                                                            alt="{{$child->slug}}"
                                                            title="{{@$child->globalName->name}}">
                                                </label>
                                            </div>
                                        @endforeach
                                    @endif
                                    @break
{{--                                    @case('radio')--}}
{{--                                    @if($children->isNotEmpty())--}}
{{--                                        @foreach($children as $child)--}}
{{--                                            <div class="attribute-item-wrap">--}}
{{--                                                <input type="radio"--}}
{{--                                                        name="order_products[attributes][attributes-{{$parent->slug}}][{{$product->id}}][{{$increment}}]"--}}
{{--                                                        autocomplete="off"--}}
{{--                                                        id="attribute-{{$child->slug}}-{{$product->id}}--{{$increment}}"--}}
{{--                                                        value="{{$child->id}}">--}}
{{--                                                <label for="attribute-{{$child->slug}}-{{$product->id}}--{{$increment}}"--}}
{{--                                                        title="{{@$child->globalName->name}}">--}}
{{--                                                    <span>{{@$child->globalName->name}}</span>--}}
{{--                                                </label>--}}
{{--                                            </div>--}}
{{--                                        @endforeach--}}
{{--                                    @endif--}}
{{--                                    @break--}}
                                    @default
                                    <div class="attribute-item-wrap">
                                        <select class="select select2 no-search"
                                                name="order_products[attributes][attributes-{{$parent->slug}}][{{$product->id}}][{{$increment}}]"
                                                id="attributes-{{$parent->slug}}-{{$product->id}}--{{$increment}}"
                                                autocomplete="off">
                                            <option value="">{{__('front.selectAttribute')}}</option>
                                            @if($children->isNotEmpty())
                                                @foreach($children as $child)
                                                    <option value="{{$child->id}}">{{@$parent->field_type == 'range' ? @$child->range_value : @$child->globalName->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                @endswitch
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        @endif
    </div>
@endif