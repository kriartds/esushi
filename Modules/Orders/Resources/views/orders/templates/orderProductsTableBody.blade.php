@if(@$cartProducts && $cartProducts->isNotEmpty())
    @foreach($cartProducts as $cartProduct)
        @php
            $product = @$cartProduct->product;
        @endphp

        <tr data-order-product-id="{{$cartProduct->id}}"
            class="{{!is_numeric($cartProduct->isInStockCheckedQty) && !$cartProduct->isInStockCheckedQty && (!@$item || @$item->is_draft || @$item->is_fast_buy || !@$item->user_id) && ($product && (!@$product->isVariationProduct || (@$product->isVariationProduct && @$cartProduct->variation))) ? 'outOfStock' : ''}}">
            <td colspan="2">
                <div class="product-info direction-column">
                    <div class="product-name-img">
                    <div class="product-id">
                                <span class="tooltip" title="Product ID">#{{@$cartProduct->products_item_id ?: @$cartProduct->info->id}}</span>
                    </div>

                        <div class="product-img">
                            @if(@$product)
                                <a href="{{$cartProduct->getCustomFileAttribute('original')}}"
                                   data-fancybox="thumbnail-{{$cartProduct->id}}">
                                    <img src="{{$cartProduct->customFile}}"
                                         alt="{{@$product->slug ?: @$cartProduct->info->slug}}"
                                         title="{{@$product->globalName->name ?: @$cartProduct->info->name}}">
                                </a>
                            @else
                                <a href="{{@$cartProduct->info->firstFile}}"
                                   data-fancybox="thumbnail-{{$cartProduct->id}}">
                                    <img src="{{@$cartProduct->info->firstFile}}"
                                         alt="{{@$cartProduct->info->slug}}"
                                         title="{{@$cartProduct->info->name}}">
                                </a>
                            @endif
                        </div>
                        <div class="product-name">
                            @if(@$product)
                                <a href="{{customUrl(['products', (@$product->slug ?: @$cartProduct->info->slug), (@$cartProduct->products_variation_item_id ? $cartProduct->products_variation_item_id : "")])}}"
                                   target="_blank" class="view-product-on-site"></a>
                                <a href="{{customUrl(['admin', 'products', 'edit', @$product->id, DEF_LANG_ID])}}"
                                   target="_blank">{{@$product->globalName->name ?: @$cartProduct->info->name}}</a>
                            @else
                                <a>{{@$cartProduct->info->name}}</a>
                            @endif
                        </div>
                    </div>
                    @if(!empty($cartProduct->attributes))
                        <div class="product-attributes">
                            @foreach($cartProduct->attributes as $attribute)
                                @php
                                    $parent = $attribute->parent;
                                    $child = $attribute->child;


                                    $parentName = @collect($parent->info)[LANG] ? @$parent->info->{LANG}->name : @collect($parent->info)->first()->name;
                                    $childName = (@collect($child->info)[LANG] ? @$child->info->{LANG}->name : @collect($child->info)->first()->name) ? @collect($child->info)->first()->name : @collect($child->info)->first();
                                @endphp
                                @if($parent && $child)
                                    <div class="attribute-item">
                                        <div class="title">{{$parentName}}:</div>
                                        <div class="description attribute-item-value">
                                            @if($parent->attribute_type == 'color' && !is_null($child->color) && is_array($child->color))
                                                <span>{{$childName}}</span>
                                            @elseif($parent->attribute_type == 'image')
                                                <div class="imageContainer">
                                                    <img src="{{$child->img}}"
                                                         alt="{{@$child->slug}}"
                                                         title="{{$childName}}"
                                                         class="tooltip">
                                                </div>
                                            @else
                                                <span>{{$childName}}</span>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @endif
                </div>
            </td>
            <td class="price">{!! !is_null(@$cartProduct->amount) ? formatPrice($cartProduct->amount, $orderCurrency) : '-' !!}</td>
            <td class="product-quantity {{is_numeric($cartProduct->isInStockCheckedQty) && (!@$item || @$item && @$enableEditing) ? 'decreasedStock' : ''}}">
                <!-- <span title="{{is_numeric($cartProduct->isInStockCheckedQty) && (!@$item || @$item->is_draft || @$item->is_fast_buy) ? "{$cartProduct->isInStockCheckedQty} products available" : ''}}">{{@$cartProduct->quantity ?: '-'}}</span> -->
                <div class="change-product-stock">
                    <input type="text" class="update-product-quantity" data-url="{{customUrl(['admin', $currComponent->slug, 'updateOrderProducts'])}}" autocomplete="off"
                           data-id="{{$cartProduct->id}}" value="{{@$cartProduct->quantity ?: '-'}}" {{@$cartProduct->finStock && is_numeric($cartProduct->finStock) ? "data-max-value={$cartProduct->finStock}" : ''}}>
                </div>
            </td>
            <td class="order-products-subtotal">{!! !is_null(@$cartProduct->amount) && !is_null(@$cartProduct->quantity) ? formatPrice($cartProduct->amount * $cartProduct->quantity, $orderCurrency) : '-' !!}</td>
            <td>
                @if(!@$item || @$item && @$enableEditing)
                    <div class="actions">
                        <div class="edit-product-order"></div>
                        <div class="destroy-product-order"
                             data-url="{{customUrl(['admin', $currComponent->slug, 'destroyOrderProducts'])}}"></div>
                    </div>
                @endif
            </td>
        </tr>
    @endforeach
@endif