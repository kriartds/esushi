@if(@$item->discount_id)
    <div class="coupon-wrap {{@$validateCouponErrorMsg ? 'error' : ''}}">
        <div class="title">
            <span>Coupon</span>
        </div>
        <div class="coupons-items" id="coupons-list">
            <div class="coupon-item"
                 title="{{@$validateCouponErrorMsg && @$validateCouponErrorMsg['e'] ? @$validateCouponErrorMsg['e'] : ''}}">
                <span>{{@$item->discount_details->code}}</span>
                <div class="remove-coupon" data-id="{{@$item->discount_id}}" data-url="{{customUrl(['admin', $currComponent->slug, 'deleteCoupon', @$item->id])}}"></div>
            </div>
        </div>
    </div>
@endif