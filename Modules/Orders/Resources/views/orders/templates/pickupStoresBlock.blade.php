@if(@$storesCountries)
    <div class="row">

        <div class="col extra-large">
            <div class="field-row">
                <div class="label-wrap">
                    <label for="pickup_country">{{__("{$moduleName}::e.store_country")}}</label>
                </div>
                <div class="field-wrap">
                    <select class="select select2" name="pickup_country"
                            id="pickup_country">
{{--                        @foreach($storesCountries as $country)--}}
{{--                            <option value="{{$country->id}}" {{@$item->pickup_details->fullCountry->id == $country->id ? 'selected' : (@$currCountry->id == $country->id ? 'selected' : '')}}>{{$country->name}}</option>--}}
{{--                        @endforeach--}}
                    </select>
                </div>
            </div>
        </div>

        <div class="col extra-large">
            <div class="field-row">
                <div class="label-wrap">
                    <label for="pickup_region">{{__("{$moduleName}::e.store_region")}}</label>
                </div>
                <div class="field-wrap">
                    <select class="select select2" name="pickup_region" id="pickup_region">
{{--                        @if($storesRegions->isNotEmpty())--}}
{{--                            @foreach($storesRegions as $region)--}}
{{--                                <option value="{{$region->id}}" {{@$item->pickup_details->fullRegion->id == $region->id ? 'selected' : (@$currRegion->id == $region->id ? 'selected' : '')}}>{{$region->name}}</option>--}}
{{--                            @endforeach--}}
{{--                        @else--}}
{{--                            <option value="">Select country</option>--}}
{{--                        @endif--}}
                    </select>
                </div>
            </div>
        </div>

    </div>
                
    <div class="field-row">
        <div class="label-wrap">
            <label for="pickup_address">{{__("{$moduleName}::e.store_address")}}</label>
        </div>
        <div class="field-wrap">
            <select class="select select2 no-search" name="pickup_address"
                    id="pickup_address">
                @if($pickupStores->isNotEmpty())
                    @foreach($pickupStores as $store)
                        <option value="{{$store->id}}" {{@$item->pickup_details->fullPickupStore->id == $store->id ? 'selected' : ''}}>{{@$store->globalName->name}}
                            | {{@$store->globalName->address}}</option>
                    @endforeach
                @else
                    <option value="">Select region</option>
                @endif
            </select>
        </div>
    </div>
   
@endif