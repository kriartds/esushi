<form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form" enctype="multipart/form-data" class="orders-form">
    <div class="row">
        <div class="col">
            <div class="order-description-block">
                @if(@$item)
                    <div class="orders-header">
                        <div class="row">
                            <div class="title">
                                <span>{{__("{$moduleName}::e.order")}} #</span><span
                                        id="generated-order-id">{{@$item->order_id}}</span>
                            </div>
                            <div class="open-close-fields-order">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     width="16" height="16" viewBox="0 0 16 16">
                                    <defs>
                                        <clipPath id="clip-path">
                                            <rect id="Rectangle_628" data-name="Rectangle 628" width="16" height="16"
                                                  transform="translate(400 766)" fill="#fff" stroke="#707070"
                                                  stroke-width="1"/>
                                        </clipPath>
                                    </defs>
                                    <g id="Mask_Group_35" data-name="Mask Group 35" transform="translate(-400 -766)"
                                       clip-path="url(#clip-path)">
                                        <g id="edit" transform="translate(399.283 765.438)">
                                            <path id="Path_2225" data-name="Path 2225"
                                                  d="M7.889,2.869H2.869A1.434,1.434,0,0,0,1.434,4.3v10.04a1.434,1.434,0,0,0,1.434,1.434h10.04a1.434,1.434,0,0,0,1.434-1.434V9.323"
                                                  fill="none" stroke="#ff5e47" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-width="1.434"/>
                                            <path id="Path_2226" data-name="Path 2226"
                                                  d="M13.267,1.793a1.521,1.521,0,0,1,2.151,2.151L8.606,10.757l-2.869.717.717-2.869Z"
                                                  fill="none" stroke="#ff5e47" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-width="1.434"/>
                                        </g>
                                    </g>
                                </svg>
                                Edit
                            </div>
                        </div>

                        <div class="info">
                            <div class="info-item">
                                <span class="bold">{{__("{$moduleName}::e.order_type")}}:</span>
                                {{ucfirst(!$item->user_id && $item->is_draft ? __("{$moduleName}::e.draft_order") : (!$item->user_id ? __("{$moduleName}::e.admin_order") : (@$item->is_fast_buy ? __("{$moduleName}::e.fast_order") : __("{$moduleName}::e.simple_order"))))}}
                            </div>
                            @if(@$item->ip)
                                <div class="info-item">
                                    <span class="bold">{{__("{$moduleName}::e.ip")}}:</span> {{@$item->ip}}
                                </div>
                            @endif
                            @if(@$item->bank_id_transaction)
                                <div class="info-item">
                                    <span class="bold">{{__("{$moduleName}::e.bank_id_transaction")}}:</span>
                                    #{{@$item->bank_id_transaction}}
                                </div>
                                @if(!is_null(@$item->bank_id_transaction))
                                    <div class="info-item">
                                        <span class="bold">Payment status:</span>
                                        @if(@$item->payment_bank_status == 'OK' && @$item->status == 'checkout')
                                            Success
                                        @else
                                            In process OR Failed
                                        @endif
                                    </div>
                                @endif
                            @endif
                            <div class="info-item">
                                <span class="bold">{{__("{$moduleName}::e.payment_method")}}:</span>
                                {{@$item->payment_method == 'card' ? 'Card online' : @$item->payment_method}}
                            </div>
                            @if(@$item->online_payment_name)
                                <div class="info-item">
                                    <span class="bold">{{__("{$moduleName}::e.online_payment_name")}}:</span> {{strtoupper(@$item->online_payment_name)}}
                                </div>
                            @endif

                        </div>
                    </div>
                @endif
                    @if(@$item->payment_method == 'card')
                        <div class="field-row order-info-row">
                            <div class="field-wrap field-wrap-children">
                                <div class="container">
                                    <div class="order-title">Payments</div>
                                    <div class="row">
                                        @if(@$item->status == 'checkout')
                                            <div class="">
                                                <input type="text" name="refund_amount" id="refund_amount" value="" placeholder="Enter refund amount">
                                                <button type="button" class="action-btn-ajax button white-blue-gray bold full" id="refund-money-btn" data-url="{{customUrl(['admin', $currComponent->slug, 'refundMoney', @$item->id])}}">
                                                    Refund money
                                                </button>
                                            </div>
                                        @elseif (@$item->status == 'processing') 
                                            <div class="">
                                                <button type="button" class="action-btn-ajax button white-blue-gray bold full" id="finish-transaction-btn" data-url="{{customUrl(['admin', $currComponent->slug, 'finishTransaction', @$item->id])}}">
                                                    Finish transaction
                                                </button>
                                            </div>
                                        @endif
                                        <div class="">
                                            <button type="button" class="action-btn-ajax button white-blue-gray bold full" id="check-payment-btn"
                                                    data-url="{{customUrl(['admin', $currComponent->slug, 'checkPayment', @$item->id])}}">
                                                Check payment
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 5px;">
                                        <div class="info-item">
                                            <span class="bold">Refunded amount: </span> {{formatPrice(@$item->return_amount, @$orderCurrency)}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                <div class="field-row order-info-row">
                    <div class="field-wrap field-wrap-children">
                        <div class="container">
                            <div class="order-title">{{__("{$moduleName}::e.general")}}</div>

                            <div class="row">
                                <div class="col extra-large">
                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="status">{{__("{$moduleName}::e.status")}}</label>
                                        </div>
                                        <div class="field-wrap">
                                            <select name="status" id="status" class="select2 no-search">
                                                <option value="checkout" {{@$item->status == 'checkout' ? 'selected' : ''}}>
                                                    {{__("{$moduleName}::e.checkout")}}
                                                </option>
                                                <option value="processing" {{@$item->status == 'processing' ? 'selected' : ''}}>
                                                    {{__("{$moduleName}::e.processing")}}
                                                </option>
                                                <option value="cancelled" {{@$item->status == 'cancelled' ? 'selected' : ''}}>
                                                    {{__("{$moduleName}::e.cancelled")}}
                                                </option>
                                                <option value="approved" {{@$item->status == 'approved' ? 'selected' : ''}}>
                                                    {{__("{$moduleName}::e.approved")}}
                                                </option>
                                                <option value="finished" {{@$item->status == 'finished' ? 'selected' : ''}}>
                                                    {{__("{$moduleName}::e.finished")}}
                                                </option>
                                                <option value="failed" {{@$item->status == 'failed' ? 'selected' : ''}}>
                                                    {{__("{$moduleName}::e.failed")}}
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col extra-large">
                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="date">{{__("{$moduleName}::e.date")}}</label>
                                        </div>
                                        <div class="field-wrap">
                                            <input id="date" class="datetimepicker time" name="order_date"
                                                   value="{{strtotime(@$item->order_date) ? date('d-m-Y H:i', strtotime($item->order_date)) : date('d-m-Y H:i')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hidden-content">
                                <div class="row">
                                    <div class="col extra-large">
                                        <div class="order-title">
                                            <span>{{__("{$moduleName}::e.client_info")}}</span>
                                        </div>
                                        <div id="short-client-info-block"
                                             class="short-client-info-block {{!@$item ? 'hidden' : ''}}">
                                            @if(@$item->details->name)
                                                <div class="info-block">
                                                    <span class="name bold">{{__("{$moduleName}::e.name")}}: </span>
                                                    <span class="value">{{@$item->details->name}}</span>
                                                </div>
                                            @endif
                                            @if(@$item->email)
                                                <div class="info-block">
                                                    <span class="name bold">{{__("{$moduleName}::e.email")}}: </span>
                                                    <span class="value">{{@$item->email}}</span>
                                                </div>
                                            @endif
                                            @if(@$item->phone)
                                                <div class="info-block">
                                                    <span class="name bold">{{__("{$moduleName}::e.phone")}} :</span>
                                                    <span class="value">{{@$item->phone}}</span>
                                                </div>
                                            @endif
                                            @if(@$item->details->comment)
                                                <div class="info-block">
                                                    <span class="name bold">{{__("{$moduleName}::e.message")}}: </span>
                                                    <span class="value">{{@$item->details->comment}}</span>
                                                </div>
                                            @endif
                                            @if(@$item->details->number_of_persons)
                                                <div class="info-block">
                                                    <span class="name bold">Number of persons: </span>
                                                    <span class="value">{{@$item->details->number_of_persons}}</span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col extra-large">
                                        @if(@$item)

                                            <div class="order-title {{ @$item->hasShippingInfo || @$item->hasPickupInfo ? '' : 'hidden' }}">
                                                <span>{{@$useShipping || @$item->is_fast_buy ? __("{$moduleName}::e.shipping_info") : __("{$moduleName}::e.pickup_info")}}</span>
                                            </div>

                                            @if(@$useShipping && @$item->hasShippingInfo)
                                                <div id="short-client-shipping-info-block"
                                                     class="short-client-shipping-info-block {{!@$item ? 'hidden' : ''}}">
{{--                                                    <div class="info-block">--}}
{{--                                                        <span class="name bold">{{__("{$moduleName}::e.country")}}: </span>--}}
{{--                                                        <span class="value">{{@$item->country->name}}</span>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="info-block">--}}
{{--                                                        <span class="name bold">{{__("{$moduleName}::e.region")}}: </span>--}}
{{--                                                        <span class="value">{{@$item->region->name}}</span>--}}
{{--                                                    </div>--}}
                                                    <div class="info-block">
                                                        <span class="name bold">{{__("{$moduleName}::e.city")}} :</span>
                                                        <span class="value">{{@$item->city}}</span>
                                                    </div>
{{--                                                    <div class="info-block">--}}
{{--                                                        <span class="name bold">{{__("{$moduleName}::e.zip_code")}} :</span>--}}
{{--                                                        <span class="value">{{@$item->details->zip_code}}</span>--}}
{{--                                                    </div>--}}
                                                    @if(@$item->shipping_details->section)
                                                        <div class="info-block">
                                                            <span class="name bold">Sector :</span>
                                                            <span class="value">{{@$item->shipping_details->section}}</span>
                                                        </div>
                                                    @endif
                                                    <div class="info-block">
                                                        <span class="name bold">{{__("{$moduleName}::e.address")}} :</span>
                                                        <span class="value">{{@$item->shipping_details->address}}</span>
                                                    </div>
                                                    @if(@$item->shipping_details->house)
                                                        <div class="info-block">
                                                            <span class="name bold">House :</span>
                                                            <span class="value">{{@$item->shipping_details->house}}</span>
                                                        </div>
                                                    @endif
                                                    @if(@$item->shipping_details->scale)
                                                        <div class="info-block">
                                                            <span class="name bold">Scale :</span>
                                                            <span class="value">{{@$item->shipping_details->scale}}</span>
                                                        </div>
                                                    @endif
                                                    @if(@$item->shipping_details->apartment)
                                                        <div class="info-block">
                                                            <span class="name bold">Apartment :</span>
                                                            <span class="value">{{@$item->shipping_details->apartment}}</span>
                                                        </div>
                                                    @endif
                                                    @if(@$item->shipping_details->floor)
                                                        <div class="info-block">
                                                            <span class="name bold">Floor :</span>
                                                            <span class="value">{{@$item->shipping_details->floor}}</span>
                                                        </div>
                                                    @endif
                                                    @if(@$item->shipping_details->intercom)
                                                        <div class="info-block">
                                                            <span class="name bold">Intercom :</span>
                                                            <span class="value">{{@$item->shipping_details->intercom}}</span>
                                                        </div>
                                                    @endif
                                                    @if(@$item->shipping_details->ground_house)
                                                        <div class="info-block">
                                                            <span class="name bold">Ground house :</span>
                                                            <span class="value">{{@$item->shipping_details->ground_house ? 'yes':'no'}}</span>
                                                        </div>
                                                    @endif
                                                    @if(@$item->shipping_details->name)
                                                        <div class="info-block">
                                                            <span class="name bold">{{__("{$moduleName}::e.shipping_method")}} :</span>
                                                            <span class="value">{{@$item->shipping_details->name}}</span>
                                                        </div>
                                                    @endif
                                                </div>

                                            @elseif(@$item->hasPickupInfo)
                                                <div id="short-client-shipping-info-block"
                                                     class="short-client-shipping-info-block {{!@$item ? 'hidden' : ''}}">

                                                    <!--div class="info-block">
                                                        <span class="name bold">{{__("{$moduleName}::e.store_country")}} :</span>
                                                        <span class="value">{{@$item->pickup_details->country}}</span>
                                                    </div-->
                                                    <!--div class="info-block">
                                                        <span class="name bold">{{__("{$moduleName}::e.store_region")}} :</span>
                                                        <span class="value">{{@$item->pickup_details->region}}</span>
                                                    </div-->
                                                    <div class="info-block">
                                                        <span class="name bold">{{__("{$moduleName}::e.store_address")}}: </span>
                                                        <span class="value">{{@$item->pickupStore->globalName->address ?: @$item->pickup_details->address}}</span>
                                                    </div>
                                                    <div class="info-block">
                                                        <span class="name bold">{{__("{$moduleName}::e.store_name")}}: </span>
                                                        <span class="value">{{@$item->pickupStore->globalName->name ?: @$item->pickup_details->name}}</span>
                                                    </div>

{{--                                                    @if(@$item->shipping_details->name)--}}
{{--                                                        <div class="info-block">--}}
{{--                                                            <span class="name bold">{{__("{$moduleName}::e.shipping_method")}} :</span>--}}
{{--                                                            <span class="value">{{@$item->shipping_details->name}}</span>--}}
{{--                                                        </div>--}}
{{--                                                    @endif--}}
                                                </div>
                                                <div id="short-client-pickup-info-block" class="{{!@$item ? 'hidden' : ''}}">

                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div id="edit-client-info-block"
                                     class="{{@$item && @$item->hasClientInfo ? 'hidden' : ''}}">

                                    <div class="row">
                                        <div class="col extra-large ">
                                            <div class="field-row">
                                                <div class="label-wrap">
                                                    <label for="name">{{__("{$moduleName}::e.name")}}</label>
                                                </div>
                                                <div class="field-wrap">
                                                    <input id="name" name="name" value="{{@$item->details->name}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col extra-large">
                                            <div class="field-row">
                                                <div class="label-wrap">
                                                    <label for="email">{{__("{$moduleName}::e.email")}}</label>
                                                </div>
                                                <div class="field-wrap">
                                                    <input name="email" id="email" value="{{@$item->email}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col extra-large">
                                            <div class="field-row">
                                                <div class="label-wrap">
                                                    <label for="phone">{{__("{$moduleName}::e.phone")}}</label>
                                                </div>
                                                <div class="field-wrap">
                                                    <input name="phone" id="phone" value="{{@$item->phone}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col extra-large">
                                            <div class="field-row">
                                                <div class="label-wrap">
                                                    <label for="payment_method">{{__("{$moduleName}::e.payment_method")}}</label>
                                                </div>
                                                <div class="field-wrap">
                                                    <select name="payment_method" id="payment_method"
                                                            class="select2 no-search">
                                                        <option value="cash" {{@$item->payment_method == 'cash' ? 'selected' : ''}}>
                                                            Cash
                                                        </option>
                                                        <option value="card" {{@$item->payment_method == 'card' ? 'selected' : ''}}>
                                                            Card online
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="message">{{__("{$moduleName}::e.message")}}</label>
                                        </div>
                                        <div class="field-wrap">
                                            <textarea name="message"
                                                      id="message">{{@$item->details->comment}}</textarea>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <div class="tabs-container hidden-content">

                            @if(!@$item || @$item && @$enableEditing)
                                <div class="tabs-block no-margin {{@$item && (@$item->hasShippingInfo || @$item->hasPickupInfo) ? 'hidden' : ''}}"
                                     id="pickup-shipping-edit-block">

                                    <div class="left-side">
                                        <div class="tab-item {{!@$item || (@$useShipping && @$item->hasShippingInfo) || @$item->is_fast_buy ? 'active' : ''}}"
                                             data-target="#shipping-info-block">
                                            <label>
                                                {{__("{$moduleName}::e.shipping_info")}}
                                                <input type="radio" name="manipulate_shipping_pickup_block"
                                                       value="shipping_block" {{!@$item || (@$useShipping && @$item->hasShippingInfo) || @$item->is_fast_buy ? 'checked' : ''}}>
                                            </label>
                                        </div>

                                        <div class="tab-item {{@$item && @$usePickup && $item->hasPickupInfo ? 'active' : ''}}"
                                             data-target="#pickup-info-block">
                                            <label>
                                                {{__("{$moduleName}::e.pickup_info")}}
                                                <input type="radio" name="manipulate_shipping_pickup_block"
                                                       value="pickup_block" {{@$item && @$usePickup && $item->hasPickupInfo ? 'checked' : ''}}>
                                            </label>
                                        </div>

                                    </div>

                                    <div class="right-side">
                                        <div id="shipping-info-block"
                                             class="tab-target with-padding {{!@$item || (@$useShipping && @$item->hasShippingInfo) || @$item->is_fast_buy ? '' : 'hidden'}} tab-target">
                                            <div class="row">
                                                <div class="col extra-large">
                                                    <div class="field-row">
                                                        <div class="label-wrap">
                                                            <label for="shipping-country">{{__("{$moduleName}::e.country")}}</label>
                                                        </div>
                                                        <div class="field-wrap">
                                                            <select autocomplete="off" name="shipping_country"
                                                                    id="shipping-country"
                                                                    class="select2 ajax countries-list"
                                                                    data-url="{{customUrl(['admin', $currComponent->slug, 'getCountries'])}}">
                                                                @if(@$item->country)
                                                                    <option value="{{@$item->country_id}}"
                                                                            selected>{{"#{$item->country_id} | {$item->country->name}"}}</option>
                                                                @else
                                                                    <option value="">Chose country</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col extra-large">
                                                    <div class="field-row">
                                                        <div class="label-wrap">
                                                            <label for="shipping-regions">{{__("{$moduleName}::e.region")}}</label>
                                                        </div>
                                                        <div class="field-wrap">
                                                            <select autocomplete="off" name="shipping_region"
                                                                    id="shipping-regions"
                                                                    class="select2 ajax regions-list"
                                                                    data-url="{{customUrl(['admin', $currComponent->slug, 'getRegions']) . (@$item->country ? "?countryId={$item->country_id}" : '')}}">
                                                                @if(@$item->region)
                                                                    <option value="{{$item->region_id}}"
                                                                            selected>{{"#{$item->region_id} | {$item->region->name}"}}</option>
                                                                @endif
                                                                <option value="">Chose region</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col extra-large">
                                                    <div class="field-row">
                                                        <div class="label-wrap">
                                                            <label for="shipping-city">{{__("{$moduleName}::e.city")}}</label>
                                                        </div>
                                                        <div class="field-wrap">
                                                            <input autocomplete="off" name="shipping_city"
                                                                   id="shipping-city"
                                                                   value="{{@$item->city}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col extra-large">
                                                    <div class="field-row">
                                                        <div class="label-wrap">
                                                            <label for="zip-code">{{__("{$moduleName}::e.zip_code")}}</label>
                                                        </div>
                                                        <div class="field-wrap">
                                                            <input autocomplete="off" name="zip_code" id="zip-code"
                                                                   value="{{@$item->details->zip_code}}">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="field-row">
                                                <div class="label-wrap">
                                                    <label for="shipping-address">{{__("{$moduleName}::e.address")}}</label>
                                                </div>
                                                <div class="field-wrap">
                                                    <input autocomplete="off" name="shipping_address"
                                                           id="shipping-address"
                                                           value="{{@$item->details->address}}">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col extra-large">
                                                    <div class="field-row">
                                                        <div class="label-wrap">
                                                            <label for="shipping-method">{{__("{$moduleName}::e.shipping_method")}}</label>
                                                        </div>
                                                        <div class="field-wrap">
                                                            <select name="shipping_method" id="shipping-method"
                                                                    class="select2 no-search">
                                                                <option value="">-</option>
                                                                <option value="flat" {{@$item->shipping_details->method_type == 'flat' ? 'selected' : ''}}>
                                                                    Flat rate
                                                                </option>
                                                                <option value="free" {{@$item->shipping_details->method_type == 'free' ? 'selected' : ''}}>
                                                                    Free shipping
                                                                </option>
                                                                <option value="pickup" {{@$item->shipping_details->method_type == 'pickup' ? 'selected' : ''}}>
                                                                    Local pickup
                                                                </option>
                                                                <option value="other" {{@$item->shipping_details->method_type == 'other' ? 'selected' : ''}}>
                                                                    Other
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col extra-large">
                                                    <div class="field-row">
                                                        <div class="label-wrap">
                                                            <label for="shipping_amount">{{__("{$moduleName}::e.shipping_amount")}}</label>
                                                        </div>
                                                        <div class="field-wrap">
                                                            <input autocomplete="off" name="shipping_amount"
                                                                   id="shipping_amount"
                                                                   value="{{@$item->shipping_amount}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="pickup-info-block"
                                             class="tab-target with-padding  {{@$item && @$usePickup && $item->hasPickupInfo ? '' : 'hidden'}}">
                                            <div id="edit-client-pickup-block"
                                                 data-url="{{customUrl(['admin', $currComponent->slug, 'getPickupRegionsStores'])}}">
{{--                                                @include("{$path}.templates.pickupStoresBlock", [--}}
{{--                                                    'storesCountries' => $storesCountries,--}}
{{--                                                    'storesRegions' => $storesRegions,--}}
{{--                                                    'pickupStores' => $pickupStores,--}}
{{--                                                ])--}}
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col small">

            <div class="add-product-container">
                <button type="button" class="button white-blue-gray bold full getModal" id="add-products-btn"
                        data-modal="#modal-add-order-products">Add Products
                </button>
            </div>

            <div class="applied-coupons-container @if(!@$item->discount_details) hidden @endif">
                <span>Applied coupons:</span>
                <div class="applied-coupons">{{@$item->discount_details->code}}</div>
            </div>

            <div class="totals-block" id="totals-block">
                <div class="info-block">
                    <span class="name">{{__("{$moduleName}::e.amount")}}: </span>
                    <span class="value bold subtotalBlock">{!! @$cartSubtotal ? formatPrice(@$cartSubtotal, @$orderCurrency->default, false) : '-' !!}</span>
                </div>
                @if(@$orderCurrency->code != @$orderCurrency->default->code)
                    <div class="info-block">
                        <span class="name">{{__("{$moduleName}::e.converted_amount")}}: </span>
                        <span class="value bold">{!! @$cartSubtotal ? formatPrice(@$cartSubtotal, @$orderCurrency) : '-' !!}</span>
                    </div>
                @endif
                <div class="info-block">
                    <span class="name">{{__("{$moduleName}::e.discount_amount")}}: </span>
                    <span class="value bold discountBlock {{@$item->discount_details->allow_free_shipping ? 'free-shipping' : ''}}">{!! @$item->discount_details ? ($item->discount_details->allow_free_shipping ? __("{$moduleName}::e.free_shipping_coupon") : (@$cartDiscountAmount ? formatPrice(@$cartDiscountAmount, @$orderCurrency) : '-')) : '-' !!}</span>
                </div>
                <div class="info-block">
                    <span class="name">{{__("{$moduleName}::e.shipping_amount")}}: </span>
                    <span class="value bold shippingBlock">{!! @$shippingAmount ? formatPrice(@$shippingAmount ?: 0, @$orderCurrency) : '-' !!}</span>
                </div>
                <div class="info-block total">
                    <span class="name bold blue">{{__("{$moduleName}::e.total_amount")}}: </span>
                    <span class="value bold totalBlock">{!! @$cartTotal ? formatPrice(@$cartTotal, @$orderCurrency->default, false) : '-' !!}</span>
                </div>
            </div>

        </div>
    </div>

    @if($iiko_logs->isNotEmpty())
        <div class="row">
            <div class="iiko_block">
                <div class="title">Iiko details</div>
                <div class="info-block">
                    <span class="name">Restaurant: </span>
                    <span class="value bold">{{@$item->restaurant->globalName->name}}</span>,
                    <span class="value bold">{{@$item->restaurant->globalName->address}}</span>
                </div>
                <div style="margin: 10px 0px;">
                    @if(isset($item->iiko_id) && !empty($item->iiko_id))
                        <button type="button" id="check_order_iiko" class="button blue action-btn-ajax" data-url="{{customUrl(['admin', $currComponent->slug, 'checkOrderInIiko', @$item->id])}}">
                            Check order
                        </button>
                    @else
                        <button type="button" id="send_order_iiko" class="button blue action-btn-ajax" data-url="{{customUrl(['admin', $currComponent->slug, 'sendOrderToIiko', @$item->id])}}">
                            Send order to iiko
                        </button>
                    @endif
                </div>
                <div class="info-block">
                    <span class="name">IikoId: </span>
                    <span class="value bold">{{@$item->iiko_id}}</span>
                </div>
                <div class="info-block">
                    <span class="name">Iiko Status: </span>
                    <span class="value bold">{{@$item->iiko_status}}</span>
                </div>
                <div class="info-block">
                    <span class="name">Iiko history: </span>
                    <span class="value bold"><a href="javascript:void(0)" onclick="$('#iiko_history').toggle('slow');">Show/Hide</a></span>
                </div>

                <div id="iiko_history">
                    <table>
                        <thead>
                        <tr>
                            <th style="width: 100px;">Action</th>
                            <th>Text</th>
                            <th style="min-width: 100px;">Details</th>
                            <th>Date/Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($iiko_logs as $iiko_log)
                            <tr>
                                <td>{{$iiko_log->action}}</td>
                                <td>{{$iiko_log->text}}</td>
                                <td>
                                    <a href="javascript:void(0)" style="color: #0A67A6;" onclick="$(this).next().toggle('slow');">Show/Hide</a>
                                    <div style="display: none;">
                                        @php  dump($iiko_log->details); @endphp
{{--                                        {{json_encode($iiko_log->details)}}--}}
{{--                                        @if(isset($iiko_log->details->body))--}}
{{--                                        {{json_encode(json_decode($iiko_log->details->body), JSON_PRETTY_PRINT)}}--}}
{{--                                        @endif--}}
                                    </div>

                                </td>
                                <td>{{$iiko_log->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif

    <div class="order-products-block {{!@$cartProducts || ( @$cartProducts && $cartProducts->isEmpty()) ? 'hidden' : ''}}">
        <table id="order-products-table">
            <thead>
            <tr>
                <th colspan="2">Item</th>
                <th>{{__("{$moduleName}::e.product_amount")}}</th>
                <th>{{__("{$moduleName}::e.product_quantity")}}</th>
                <th>Total</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                @include("{$path}.templates.orderProductsTableBody")
            </tbody>
        </table>
    </div>
    <div class="footer-block {{!@$item || @$item && @$enableEditing ? '' : 'not-editable'}}">
        <div class="actions-block">
            <button class="button blue submit-form-btn no-margin"
                    data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}
            </button>
{{--            <div class="coupon-fields-wrap">--}}
{{--                <input type="text" name="coupon_code" placeholder="Enter coupon code" id="coupon-code"--}}
{{--                       autocomplete="off">--}}
{{--                <button type="button" id="apply-coupon-code" class="button white-blue-blue bold"--}}
{{--                        data-url="{{customUrl(['admin', $currComponent->slug, 'checkCoupon', @$item->id])}}">Apply--}}
{{--                    coupon--}}
{{--                </button>--}}
{{--            </div>--}}
        </div>
    </div>
</form>

<div style="display: none;">
    <div class="modal modal-add-order-products" id="modal-add-order-products">
        <div class="modal-top">
            <div class="modal-title">Add products</div>
            <div class="modal-close arcticmodal-close"></div>
        </div>
        <div class="modal-center">
            <div class="products-items">
                <div class="add-products-header">
                    <select name="chose_order_products" id="chose-order-products" class="select2 ajax"
                            data-url="{{customUrl(['admin', $currComponent->slug, 'findProducts']) . (@$item ? "?cartId={$item->id}" : '')}}"
                            data-product-content-url="{{customUrl(['admin', $currComponent->slug, 'getProductContent'])}}">
                        <option value="">Chose product</option>
                    </select>
                </div>
                <form method="post"
                      class="add-products-content" id="add-order-products-form">
                    <input type="hidden" name="products_ids">
                    <div class="empty-orders-product-list">Chose products</div>
                </form>
            </div>
            <div class="add-one-more-product">
                <div class="icon"></div>
                ADD ANOTHER PRODUCT
            </div>
        </div>
        <div class="modal-bottom">
            <button type="button" id="add-order-products" class="button white-blue-gray bold" disabled
                    data-url="{{customUrl(['admin', $currComponent->slug, 'addOrderProducts', @$item->id])}}">Add
                products
            </button>
        </div>
    </div>
</div>

