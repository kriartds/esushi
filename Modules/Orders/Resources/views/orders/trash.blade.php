@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'trash'], 'item' => null, 'currentPage'=> 'trash'])
        <div class="table-block trash">
            @if(!$items->isEmpty())
                <table class="table">
                    <thead>
                    <tr>
                        <th class="align-center">{{__("{$moduleName}::e.order_id")}}</th>
                        <th class="left"> User</th>
                        <th class="left">Phone Number / {{__("{$moduleName}::e.email")}}</th>
                        <th class="align-center">{{__("{$moduleName}::e.amount")}} / Payment method</th>
                        <th class="align-center">{{__("{$moduleName}::e.order_type")}}</th>
                        <th class="align-center">Delivery method</th>
                        <th class="align-center">{{__("{$moduleName}::e.status")}}</th>
                        <th class="align-center">{{__("{$moduleName}::e.date")}}</th>
                        @if($permissions->delete)
                            <th class="checkbox-all align-center" >
                                <div>Check all</div>
                            </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td class="id">
                                <p>{{$item->order_id}}</p>
                            </td>
                            <td class="title left"><a class="trash" href="">{{@$item->details->name ?: '-'}}</a></td>
                            <td class="left">
                                <p>{{@$item->phone}}</p>
                                <p>{{@$item->email}}</p>
                            </td>
                            <td class="align-center">{!! !is_null($item->total) ? formatPrice($item->total, @$item->order_currency->default, false) : '-' !!} / Paypal</td>
                            <td class="order-type">
                                <div class="tooltip {{!$item->user_id && $item->is_draft ? 'draft' : (!$item->user_id ? 'admin' : (@$item->is_fast_buy ? 'fast' : 'simple'))}}" title="{{ucfirst(!$item->user_id && $item->is_draft ? __("{$moduleName}::e.draft_order") : (!$item->user_id ? __("{$moduleName}::e.admin_order") : (@$item->is_fast_buy ? __("{$moduleName}::e.fast_order") : __("{$moduleName}::e.simple_order"))))}}"></div>
                            </td> <!-- Class names for this div: draft, admin, fast, simple  -->

                            <td class="align-center">{{@$item->shipping_details->name}}</td>
                            <td class="order-status">
                                <div class="trash {{$item->status}}">{{ucfirst($item->status)}}</div> <!-- Class names for this div: checkout, cancelled, processing, approved, finished, failed -->
                            </td>
                            <td class="align-center">{{strtotime($item->order_date) ? date('d-m-Y H:i', strtotime($item->order_date)) : '-'}}</td>

                            @if($permissions->delete)
                                <td class="checkbox-items">
                                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                           name="checkbox_items[{{$item->id}}]"
                                           value="{{$item->id}}">
                                    <label for="{{$item->id}}"></label>
                                </td>
                            @endif

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')