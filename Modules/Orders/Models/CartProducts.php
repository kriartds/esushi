<?php

namespace Modules\Orders\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Products\Models\ProductsItemsId;
use Modules\Products\Models\ProductsItemsVariationsDetailsId;
use Modules\Products\Models\ProductsToppingsId;


class CartProducts extends Model
{

    protected $table = 'cart_products';

    protected $fillable = [
        'cart_id',
        'products_item_id',
        'products_variation_item_id',
        'quantity',
        'amount',
        'info',
        'attributes',
        'attributes_ids',
        'toppings_ids',
        'user_id',
    ];

    protected $casts = [
        'info' => 'object',
        'attributes' => 'object',
        'attributes_ids' => 'array',
        'toppings_ids' => 'array',
    ];

    protected $hidden = [
        'customFile',
        'customFiles'
    ];

    public function cart()
    {
        return $this->hasOne(Cart::class, 'id', 'cart_id');
    }

    public function product()
    {
        return $this->hasOne(ProductsItemsId::class, 'id', 'products_item_id');
    }

    public function variation()
    {
        return $this->hasOne(ProductsItemsVariationsDetailsId::class, 'id', 'products_variation_item_id');
    }

    public function getProductSelectedVariationAttrIdsAttribute()
    {
        $response = [];
        $product = $this->product;
        $variation = $this->variation;

        if (is_null($product) || is_null($variation))
            return $response;

        $variationsChildrenIds = $variation->variationsChildrenIds;

        if (!empty($variationsChildrenIds))
            foreach ($variationsChildrenIds as $p_id => $variationsChildrenId) {
                if (is_null($variationsChildrenId)) {
                    $pAttribute = $product->productItemsAttributes->where('products_parent_attribute_id', $p_id)->first();
                    if ($pAttribute->options->isNotEmpty())
                        foreach ($pAttribute->options as $option) {
                            $response[$p_id][] = $option->products_attribute_id;
                        }
                } else {
                    $response[$p_id][] = $variationsChildrenId;
                }
            }

        return $response;

    }

    public function getCustomFileAttribute($size)
    {
        $product = $this->product;
        $variation = $this->variation;

        if (is_null($product))
            return null;

        $size = $size ?: 'medium';
        $item = @$product->isVariationProduct && !is_null($variation) && $variation->file ? $variation : $product;

        $response = getItemFile($item, $size);

        return $response;
    }

    public function getCustomFilesAttribute($size)
    {
        $product = $this->product;
        $variation = $this->variation;

        if (is_null($product))
            return null;

        $size = $size ?: 'small';
        $item = @$product->isVariationProduct && !is_null($variation) && $variation->files->isNotEmpty() ? $variation : $product;
        $response = getItemFile($item, $size, true, 'allFiles');

        return $response;
    }

    public function getFinPriceAttribute()
    {
        $product = $this->product;

        if (is_null($product))
            return null;

        $response = [
            'minPrice' => @$product->sale_price ? (float)$product->sale_price : null,
            'maxPrice' => @$product->price ? (float)$product->price : null
        ];

        if (@$product->isVariationProduct && !is_null($this->products_variation_item_id)) {
            $variation = $this->variation;

            if (!is_null($variation))
                $response = [
                    'minPrice' => @$variation->sale_price ? (float)$variation->sale_price : null,
                    'maxPrice' => @$variation->price ? (float)$variation->price : null
                ];
        }

        return (object)$response;
    }

    public function getFinStockAttribute()
    {
        $product = $this->product;
        $variation = $this->variation;

        if (!$product || @$product->isVariationProductuct && !$variation)
            return false;

        if ($product->isVariationProduct) {
            $response = @$variation->finStock;
        } else {
            $response = @$product->finStock;
        }

        return $response;
    }

    public function getToppingsAttribute()
    {
        $response = ['toppings' => null, 'toppingsName' => ''];
        if(!empty($this->toppings_ids))
        {
            $toppings = ProductsToppingsId::whereIn('id', $this->toppings_ids)->get();
            $toppings_str = '';
            foreach ($toppings as $key=>$topping)
            {
                $toppings_str .= $topping->globalName->name;
                if($toppings->keys()->last() != $key)
                {
                    $toppings_str .= ', ';
                }
            }

            $response['toppings'] = $toppings;
            $response['toppingsName'] = $toppings_str;
        }

        return $response;
    }

}
