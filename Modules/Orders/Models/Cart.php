<?php

namespace Modules\Orders\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Coupons\Models\Coupons;
use Modules\Restaurants\Models\RestaurantsId;
use Modules\SiteUsers\Models\Users;


class Cart extends Model
{
    use SoftDeletes;
    use FilesRelation;

    protected $table = 'cart';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id',
        'auth_user_id',
        'order_id',
        'status',
        'online_payment_name',
        'shipping_method_id',
        'discount_id',
        'payment_method',
        'amount',
        'shipping_amount',
        'shipping_amount',
        'discount_amount',
        'count_items',
        'email',
        'phone',
        'country_id',
        'region_id',
        'city',
        'bank_id_transaction',
        'shipping_pickup_address_id',
        'order_currency',
        'order_lang',
        'details',
        'shipping_details',
        'pickup_details',
        'discount_details',
        'order_date',
        'user_ip',
        'is_fast_buy',
        'seen',
        'is_draft',
        'common_id',
        'common_user_root',
        'common_created_at',
        'restaurant_id',
        'iiko_id',
        'payment_bank_process',
        'payment_bank_status',
        'return_amount',
    ];

    protected $casts = [
        'details' => 'object',
        'order_currency' => 'object',
        'shipping_details' => 'object',
        'pickup_details' => 'object',
        'discount_details' => 'object',
    ];

    public function scopeIsInProgress($q)
    {
        return $q->whereNull('status')
            ->orWhere('is_draft', 1);
    }

    public function scopeNotIsInProgress($q)
    {
        return $q->whereNotNull('status')
            ->where('is_draft', 0);
    }

    public function scopePersonalOrCommon($q)
    {
        if($common_id=session('common_cart_id')){
            if(self::where('common_id', $common_id)->first()){
                return $q->where('common_id', $common_id);
            }
            session(['common_cart_id' => null]);
        }

        return $q->where('user_id', request()->cookie('cart', null));
    }

    public function cartProducts()
    {
        return $this->hasMany(CartProducts::class, 'cart_id', 'id');
    }

//    public function shippingMethod()
//    {
//        return $this->hasOne(ShippingMethodsId::class, 'id', 'shipping_method_id');
//    }

    public function pickupStore()
    {
        return $this->hasOne(RestaurantsId::class, 'id', 'shipping_pickup_address_id');
    }

    public function restaurant()
    {
        return $this->hasOne(RestaurantsId::class, 'id', 'restaurant_id');
    }

    public function discount()
    {
        return $this->hasOne(Coupons::class, 'id', 'discount_id');
    }

    public function getCountAttribute()
    {
        return $this->cartProducts->sum('quantity');
    }

    public function getTotalAttribute()
    {

        $cartShippingAmount = $this->shipping_amount;
        $cartDiscountAmount = 0;

        if(!is_null(@$this->discount_details)) {
            if (!@$this->discount_details->allow_free_shipping) {
                if (@$this->discount_details->type == 'fixed')
                    $cartDiscountAmount = @$this->discount_details->amount >= 0 ? $this->discount_details->amount : 0;
                else
                    $cartDiscountAmount = $this->amount * (@$this->discount_details->amount >= 0 ? $this->discount_details->amount : 0) / 100;
            } else
                $cartShippingAmount = 0;
        }

        $response = $this->amount - $cartDiscountAmount + $cartShippingAmount;
        $response = $response >= 0 ? $response : 0;

        return $response;
    }

    public function getHasClientInfoAttribute()
    {
        return ((@$this->is_draft || @$this->is_fast_buy) && (@$this->details->name || @$this->email || @$this->phone)) || (!@$this->is_draft && !@$this->is_fast_buy);
    }

    public function getHasShippingInfoAttribute()
    {

//        return ((@$this->is_draft || @$this->is_fast_buy) && (@$this->country_id || @$this->region_id || @$this->city || @$this->shipping_details)) || (!@$this->is_draft && !@$this->is_fast_buy);
        return @$this->shipping_method_id || @$this->country_id || @$this->region_id || @$this->city || @$this->shipping_details;
    }

    public function getHasPickupInfoAttribute()
    {
//        return ((@$this->is_draft || @$this->is_fast_buy) && (@$this->shipping_pickup_address_id || @$this->pickup_details)) || (!@$this->is_draft && !@$this->is_fast_buy);
        return @$this->shipping_pickup_address_id || @$this->pickup_details;
    }

    public function getUser(){
        return $this->hasOne(Users::class, 'id', 'auth_user_id');
    }

}
