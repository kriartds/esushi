<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cart_id');
            $table->unsignedInteger('products_item_id')->nullable();
            $table->unsignedInteger('products_variation_item_id')->nullable();
            $table->integer('quantity')->nullable();
            $table->decimal('amount', 10)->nullable();
            $table->text('info')->nullable();
            $table->text('attributes')->nullable();
            $table->text('attributes_ids')->nullable();
            $table->timestamps();

            $table->foreign('cart_id')->references('id')->on('cart')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('products_item_id')->references('id')->on('products_items_id')->onDelete('set null')->onUpdate('no action');
            $table->foreign('products_variation_item_id', 'products_variation_item_id_foreign')->references('id')->on('products_items_variations_details_id')->onDelete('set null')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_products');
    }
}
