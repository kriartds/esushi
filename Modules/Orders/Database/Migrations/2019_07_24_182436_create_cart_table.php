<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id', 1024)->nullable();
            $table->unsignedInteger('auth_user_id')->nullable()->index();
            $table->bigInteger('order_id')->nullable();
            $table->unsignedInteger('shipping_method_id')->nullable()->index();
            $table->unsignedInteger('discount_id')->nullable()->index();
            $table->enum('status', ['checkout', 'processing', 'cancelled', 'approved', 'finished', 'failed'])->default('checkout')->nullable();
            $table->string('online_payment_name')->nullable();
            $table->enum('payment_method', ['cash', 'card', 'online'])->default('cash')->nullable();
            $table->decimal('amount', 10)->nullable();
            $table->decimal('shipping_amount', 10)->nullable();
            $table->decimal('discount_amount', 10)->nullable();
            $table->integer('count_items')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->unsignedInteger('country_id')->nullable()->index();
            $table->unsignedInteger('region_id')->nullable()->index();
            $table->string('city')->nullable();
            $table->string('bank_id_transaction')->nullable();
            $table->unsignedInteger('shipping_pickup_address_id')->nullable()->index();
            $table->text('order_currency')->nullable();
            $table->string('order_lang', 64)->nullable();
            $table->text('details')->nullable();
            $table->text('shipping_details')->nullable();
            $table->text('pickup_details')->nullable();
            $table->text('discount_details')->nullable();
            $table->timestamp('order_date')->nullable();
            $table->ipAddress('user_ip')->nullable();
            $table->tinyInteger('is_fast_buy')->default(0);
            $table->tinyInteger('seen')->default(0);
            $table->tinyInteger('is_draft')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
