<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->enum('type', ['fixed', 'percentage'])->default('percentage')->nullable();
            $table->decimal('amount')->nullable();
            $table->tinyInteger('allow_free_shipping')->default(0);
            $table->timestamp('start_date')->nullable();
            $table->timestamp('expiry_date')->nullable();
            $table->decimal('min_subtotal')->nullable();
            $table->decimal('max_subtotal')->nullable();
            $table->unsignedInteger('limit_usage')->nullable();
            $table->unsignedInteger('uses')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
