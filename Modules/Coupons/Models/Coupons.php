<?php

namespace Modules\Coupons\Models;

use App\Models\Enums;
use Illuminate\Database\Eloquent\Model;

class Coupons extends Model
{

    use Enums;

    protected $table = 'coupons';

    protected $fillable = [
        'code',
        'type',
        'amount',
        'allow_free_shipping',
        'start_date',
        'expiry_date',
        'min_subtotal',
        'max_subtotal',
        'limit_usage',
        'uses',
        'active',
    ];

    protected $enumTypes = [
        'fixed',
        'percentage',
    ];

}
