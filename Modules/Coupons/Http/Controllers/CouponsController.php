<?php

namespace Modules\Coupons\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use Modules\Coupons\Http\Helpers\Helpers as CouponsHelpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Coupons\Http\Requests\SaveCouponsRequest;
use Modules\Coupons\Models\Coupons;

class CouponsController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'coupons';
        $this->path = "{$this->moduleName}::coupons";
    }

    public function index()
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = Coupons::orderBy('id', 'desc')->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.list", $response);

    }

    public function create()
    {

        $coupons = new Coupons();
        $couponsTypesEnums = $coupons->getEnum('type');

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'couponsTypesEnums' => $couponsTypesEnums,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id)
    {

        $item = Coupons::findOrFail($id);
        $couponsTypesEnums = $item->getEnum('type');

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'couponsTypesEnums' => $couponsTypesEnums,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function save(SaveCouponsRequest $request, $id)
    {

        $currComponent = parent::currComponent();

        $couponType = $request->get('type', 'percentage');
        $startDate = $request->get('start_date', null);
        $expireDate = $request->get('expiry_date', null);
        $allowFreeShipping = $request->get('allow_free_shipping', null) == 'on' ? 1 : 0;
        $generateMultipleCoupons = $request->get('multiple_coupons', 0);
        $generateRandomMultipleCoupons = $request->get('random_multiple_coupons', null) == 'on' ? 1 : 0;
        $allCoupons = Coupons::get();

        $item = Coupons::find($id);

        if (is_null($item)) {
            $item = new Coupons();

            $item->active = 1;
        }

        $item->type = $couponType;
        $item->amount = $request->get('amount', null);
        $item->allow_free_shipping = $allowFreeShipping;
        $item->start_date = $startDate ? carbon($startDate) : null;
        $item->expiry_date = $expireDate ? carbon($expireDate) : null;
        $item->min_subtotal = $request->get('min_subtotal', null);
        $item->max_subtotal = $request->get('max_subtotal', null);
        $item->limit_usage = $request->get('limit_usage', 1);
        $item->uses = 0;

        if (!$generateMultipleCoupons) {
            $item->code = $request->get('coupon_code', null);
            $item->save();
        } elseif (is_null($id)) {
            $createData = [
                'type' => $couponType,
                'amount' => $request->get('amount', null),
                'allow_free_shipping' => $allowFreeShipping,
                'start_date' => $startDate ? carbon($startDate) : null,
                'expiry_date' => $expireDate ? carbon($expireDate) : null,
                'min_subtotal' => $request->get('min_subtotal', null),
                'max_subtotal' => $request->get('max_subtotal', null),
                'limit_usage' => $request->get('limit_usage', 1),
                'uses' => 0,
                'active' => 1,
            ];

            $prefix = $request->get('coupon_prefix', null);
            $suffix = $request->get('coupon_suffix', null);
            $generateCouponsOptions = $generateRandomMultipleCoupons ? [] : [
                'prefix' => $prefix,
                'suffix' => $suffix
            ];

            $countGeneratedCoupons = $request->get('count_of_coupons', 0);

            for ($i = 0; $i < $countGeneratedCoupons; $i++) {
                $couponCode = CouponsHelpers::generateCouponCodes($allCoupons, $generateCouponsOptions);

                if ($couponCode) {
                    $createData = array_merge($createData, [
                        'code' => $couponCode
                    ]);

                    Coupons::create($createData);
                }
            }
        }

        helpers()->logActions($currComponent, $item, @$item->code, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => ($generateMultipleCoupons ? 'Items' : 'Item') . " was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id])
            ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                Coupons::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            default:
                $items = Coupons::whereIn('id', $ItemsId)->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted');
                    }
                }
                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function activateItem(Request $request)
    {

        $currComponent = parent::currComponent();
        $item = Coupons::findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->id]);
            helpers()->logActions($currComponent, $item, @$item->id, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->id]);
            helpers()->logActions($currComponent, $item, @$item->id, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

}
