<?php

namespace Modules\Coupons\Http\Helpers;

use Modules\Coupons\Models\Coupons;

class Helpers
{
    public static function generateCouponCodes($allCoupons, $lengths = [])
    {

        $defaultLengths = [
            'prefixLng' => 4,
            'prefix' => '',
            'numbers' => 4,
            'suffixLng' => 4,
            'suffix' => ''
        ];


        if (!empty(array_filter($lengths)))
            $lengths = (object)array_merge($defaultLengths, $lengths);
        else
            $lengths = (object)$defaultLengths;


        $prefix = $lengths->prefix;
        $suffix = $lengths->suffix;

        $newCode = (!is_null($prefix) && !$prefix ? str_random($lengths->prefixLng) : $prefix) . (!is_null($prefix) ? '-' : '') . rand(str_repeat(0, $lengths->numbers), str_repeat(9, $lengths->numbers)) . (!is_null($suffix) ? '-' : '') . (!is_null($suffix) && !$suffix ? str_random($lengths->prefixLng) : $suffix);

        $existCode = $allCoupons->where('code', $newCode)->first();

        if ($existCode)
            return false;

        return $newCode;
    }

    public static function validateCoupon($couponCode, $currCurrency, $cartSubtotal = null, $validateOnCreate = true)
    {

        $coupon = Coupons::where('code', $couponCode)
            ->where('active', 1)
            ->first();

        if (!$coupon)
            return [
                'e' => 'Coupon doesn’t exist',
                'type' => 'error'
            ];

        if ($coupon->limit_usage && $coupon->limit_usage == $coupon->uses)
            return [
                'e' => 'The coupon has reached its limits.',
                'type' => 'warning'
            ];

        if (($coupon->start_date && carbon($coupon->start_date)->gt(now())) || ($coupon->expiry_date && carbon($coupon->expiry_date)->lte(now())))
            return [
                'e' => $coupon->expiry_date && carbon($coupon->expiry_date)->lte(now()) && !$validateOnCreate ? 'The coupon is expired.' : 'The coupon is not valid.',
                'type' => 'warning'
            ];

        if ($cartSubtotal) {
            if ($coupon->min_subtotal && $coupon->min_subtotal > $cartSubtotal)
                return [
                    'e' => 'The minimum spend for this coupon is ' . formatPrice($coupon->min_subtotal, $currCurrency),
                    'type' => 'warning'
                ];

            if ($coupon->max_subtotal && $coupon->max_subtotal < $cartSubtotal)
                return [
                    'e' => 'The maximum spend for this coupon is ' . formatPrice($coupon->max_subtotal, $currCurrency),
                    'type' => 'warning'
                ];
        }

        return $coupon;

    }

    public static function updateCouponOnCheckout($coupon, $shippingMethodFreeRequires, $increment = true)
    {

        if (is_null($coupon) || $coupon && is_array($coupon))
            return false;

        if($coupon->limit_usage && $coupon->uses == $coupon->limit_usage)
            return false;

        if (($coupon->uses == 0 && !$increment) || ($coupon->allow_free_shipping && $shippingMethodFreeRequires != 'free_coupon'))
            return false;

        if ($increment) {
            $coupon->increment('uses');
        } else
            $coupon->decrement('uses');

        return true;
    }
}