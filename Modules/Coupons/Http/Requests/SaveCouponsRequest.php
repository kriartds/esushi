<?php

namespace Modules\Coupons\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class SaveCouponsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $couponType = request()->get('type', 'percentage');
        $startDate = request()->get('start_date', null);
        $expireDate = request()->get('expiry_date', null);
        $allowFreeShipping = request()->get('allow_free_shipping', null) == 'on' ? 1 : 0;
        $generateMultipleCoupons = request()->get('multiple_coupons', 0);

        $rules = [
            'coupon_code' => "required|unique:coupons,code,{$this->id}|regex:/^[a-zA-Z0-9_.-]*$/",
            'type' => 'required|in:fixed,percentage',
            'amount' => 'required|numeric|min:0',
            'limit_usage' => 'nullable|numeric|min:1',
            'min_subtotal' => 'nullable|min:0|numeric',
            'max_subtotal' => 'nullable|min:0|lower_than_field:min_subtotal|numeric',
        ];

        if ($couponType == 'percentage')
            $rules = array_merge($rules, [
                'amount' => 'required|numeric|between:0,100',
            ]);

        if ($startDate && $expireDate || $startDate && !$expireDate)
            $rules = array_merge($rules, [
                'start_date' => 'required|date_format:d-m-Y|before_or_equal:expiry_date',
                'expiry_date' => 'required|date_format:d-m-Y|after_or_equal:start_date',
            ]);
        elseif ($expireDate)
            $rules = array_merge($rules, [
                'start_date' => 'nullable|date_format:d-m-Y|before_or_equal:expiry_date',
                'expiry_date' => 'required|date_format:d-m-Y',
            ]);


        if ($allowFreeShipping)
            $rules = array_merge($rules, [
                'amount' => 'nullable|numeric|min:0',
            ]);

        if ($generateMultipleCoupons) {
            $rules = array_merge($rules, [
                'coupon_code' => 'nullable',
                'count_of_coupons' => 'required|numeric|min:1|max:1000',
            ]);
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function failedValidation(Validator $validator)
    {
        $data = [
            'status' => false,
            'validator' => true,
            'msg' => [
                'e' => $validator->messages(),
                'type' => 'error'
            ],
        ];

        throw new HttpResponseException(response()->json($data));
    }
}
