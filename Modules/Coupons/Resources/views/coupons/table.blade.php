@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="align-center">{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.code")}}</th>
            <th class="align-center">{{__("{$moduleName}::e.amount")}}</th>
            <th class="align-center">{{__("{$moduleName}::e.usage_limit")}}</th>
            <th class="align-center">Min. cart subtotal / Max. cart subtotal</th>
            <th class="align-center">{{__("{$moduleName}::e.from_date")}} / {{__("{$moduleName}::e.to_date")}}</th>
            <th class="align-center">Allowed Free Shipping</th>
            @if($permissions->active)
                <th class="align-center">{{__("{$moduleName}::e.active_table")}}</th>
            @endif
            @if($permissions->delete)
                <th class="checkbox-all align-center">
                    <div>Select All</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>

                <td class="title left">
                    @if($permissions->edit)
                        <a @if($permissions->edit) href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id])}}" @endif>{{$item->code}}</a>
                    @else
                        <p>{{$item->code}}</p>
                    @endif
                </td>

                <td class="amount align-center">
                    @if($item->type == 'percentage')
                        <p class="percentage">{{$item->amount}} %</p>
                    @elseif($item->type == 'fixed')
                        <p class="currency">{!! formatPrice(@$item->amount, $defaultCurrency)!!}</p>
                    @endif
                </td>

                <td class="align-center">{{$item->uses}} / {!! $item->limit_usage ?: "&#8734;" !!}</td>
                <td class="align-center">{{(int)$item->min_subtotal}} / {{(int)$item->max_subtotal}}</td>
                <td class="start-exp-day align-center">
                    <span class="start-day">{{strtotime($item->start_date) ? date('d-m-Y', strtotime($item->start_date)) : '-'}} </span>
                    /
                    <span class="exp-day">{{strtotime($item->expiry_date) ? date('d-m-Y', strtotime($item->expiry_date)) : '-'}}</span>
                </td>
                <td class="shipping @if($item->allow_free_shipping) active @endif">
                    <svg xmlns="http://www.w3.org/2000/svg" width="28" height="20" viewBox="0 0 28 20">
                        <g>
                            <g>
                                <path fill=""
                                      d="M21.25 18.125a1.872 1.872 0 0 1-1.875-1.875c0-1.038.838-1.875 1.875-1.875 1.038 0 1.875.838 1.875 1.875a1.872 1.872 0 0 1-1.875 1.875zm1.875-11.25L25.575 10H20V6.875zM6.25 18.125a1.872 1.872 0 0 1-1.875-1.875c0-1.038.838-1.875 1.875-1.875 1.038 0 1.875.838 1.875 1.875a1.872 1.872 0 0 1-1.875 1.875zM23.75 5H20V0H2.5A2.507 2.507 0 0 0 0 2.5v13.75h2.5A3.745 3.745 0 0 0 6.25 20 3.745 3.745 0 0 0 10 16.25h7.5A3.745 3.745 0 0 0 21.25 20 3.745 3.745 0 0 0 25 16.25h2.5V10z"/>
                            </g>
                        </g>
                    </svg>
                </td>
                @if($permissions->active)
                    <td class="status">
                        <span class="activate-item {{$item->active ? 'active' : ''}}"
                              data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                              data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>
                    </td>
                @endif
                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}">
                        <label for="{{$item->id}}">
                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
