<form class="coupons-form" method="POST"
      action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form"
      enctype="multipart/form-data">

     <div class="tabs-block coupons" id="coupons-block">
        <div class="left-side">
            <div class="tab-item active">
                <input type="radio" name="multiple_coupons" value="0" hidden>
                <a href="#single-coupon-tab">Single Coupon</a>
            </div>

            <div class="tab-item">
                <input type="radio" name="multiple_coupons" value="1" hidden>
                <a href="#multiple-coupon-tab">Multiple Coupons</a>
            </div>
        </div>
        <div class="right-side">

            <div class="tab-target" id="single-coupon-tab">
                <div class="field-row width-single">
                    <div class="label-wrap">
                        <label for="coupon_code">
                            {{__("{$moduleName}::e.code")}}*
                        </label>
                    </div>
                    <div class="field-wrap">
                        <input name="coupon_code" id="name" value="{{@$item->code}}">
                    </div>
                </div>
            </div>

            <div class="tab-target" id="multiple-coupon-tab">
                <div id="multiple-coupons-block" class="">

                    <div class="field-row no-padding">
                        <div class="field-wrap field-wrap-children">

                            <div class="inline-field-wrap flex-1">
                                <div class="field-row">
                                    <div class="label-wrap">
                                        <label for="count_of_coupons">{{__("{$moduleName}::e.count_of_coupons")}}
                                            *</label>
                                    </div>
                                    <div class="field-wrap">
                                        <input id="count_of_coupons" name="count_of_coupons">
                                    </div>
                                </div>
                            </div>

                            <div class="flex-1">
                                <div class="field-row switcher-container ">
                                        <span class="info-msg tooltip"
                                              title="({{__("{$moduleName}::e.random_multiple_coupons_info")}})">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                                 viewBox="0 0 20 20">
                                                <g id="Group_360" data-name="Group 360"
                                                   transform="translate(-1400 -1236)">
                                                    <circle id="Ellipse_22" data-name="Ellipse 22" cx="10" cy="10"
                                                            r="10" transform="translate(1400 1236)" fill="#9f9f9f"/>
                                                    <path id="Path_79" data-name="Path 79"
                                                          d="M2.769-3.056,3-9.953H.9l.232,6.9Zm-.82,1.08a1.091,1.091,0,0,0-.8.3,1,1,0,0,0-.3.749.992.992,0,0,0,.3.745,1.1,1.1,0,0,0,.8.294,1.107,1.107,0,0,0,.8-.294.992.992,0,0,0,.3-.745,1,1,0,0,0-.3-.752A1.107,1.107,0,0,0,1.948-1.976Z"
                                                          transform="translate(1408 1251)" fill="#fff"/>
                                                </g>
                                            </svg>
                                        </span>
                                    <div class="label-wrap">
                                        <label for="random_multiple_coupons">
                                            {{__("{$moduleName}::e.random_multiple_coupons")}}
                                        </label>
                                    </div>
                                    <div class="checkbox-switcher">
                                        <input type="checkbox" id="random_multiple_coupons"
                                               name="random_multiple_coupons"
                                               checked>
                                        <label for="random_multiple_coupons"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="custom-multiple-coupons-block" class="hidden">
                        <div class="field-row no-padding">
                            <div class="field-wrap field-wrap-children align-bottom">
                                <div class="inline-field-wrap flex-1">
                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="coupon_prefix">
                                                {{__("{$moduleName}::e.coupon_prefix")}}
                                            </label>
                                        </div>
                                        <div class="field-wrap">
                                            <span class="info-msg tooltip"
                                                  title="({{__("{$moduleName}::e.coupon_prefix_info")}})">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                                     viewBox="0 0 20 20">
                                                    <g id="Group_360" data-name="Group 360"
                                                       transform="translate(-1400 -1236)">
                                                        <circle id="Ellipse_22" data-name="Ellipse 22" cx="10" cy="10"
                                                                r="10" transform="translate(1400 1236)" fill="#9f9f9f"/>
                                                        <path id="Path_79" data-name="Path 79"
                                                              d="M2.769-3.056,3-9.953H.9l.232,6.9Zm-.82,1.08a1.091,1.091,0,0,0-.8.3,1,1,0,0,0-.3.749.992.992,0,0,0,.3.745,1.1,1.1,0,0,0,.8.294,1.107,1.107,0,0,0,.8-.294.992.992,0,0,0,.3-.745,1,1,0,0,0-.3-.752A1.107,1.107,0,0,0,1.948-1.976Z"
                                                              transform="translate(1408 1251)" fill="#fff"/>
                                                    </g>
                                                </svg>
                                            </span>
                                            <input id="coupon_prefix" name="coupon_prefix">
                                        </div>
                                    </div>
                                </div>
                                <div class="inline-field-wrap flex-1">
                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="coupon_suffix">
                                                {{__("{$moduleName}::e.coupon_suffix")}}
                                                <span class="info-msg"></span>
                                            </label>
                                        </div>
                                        <div class="field-wrap">
                                            <span class="info-msg tooltip"
                                                  title="({{__("{$moduleName}::e.coupon_suffix_info")}})">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                                     viewBox="0 0 20 20">
                                                    <g id="Group_360" data-name="Group 360"
                                                       transform="translate(-1400 -1236)">
                                                        <circle id="Ellipse_22" data-name="Ellipse 22" cx="10" cy="10"
                                                                r="10" transform="translate(1400 1236)" fill="#9f9f9f"/>
                                                        <path id="Path_79" data-name="Path 79"
                                                              d="M2.769-3.056,3-9.953H.9l.232,6.9Zm-.82,1.08a1.091,1.091,0,0,0-.8.3,1,1,0,0,0-.3.749.992.992,0,0,0,.3.745,1.1,1.1,0,0,0,.8.294,1.107,1.107,0,0,0,.8-.294.992.992,0,0,0,.3-.745,1,1,0,0,0-.3-.752A1.107,1.107,0,0,0,1.948-1.976Z"
                                                              transform="translate(1408 1251)" fill="#fff"/>
                                                    </g>
                                                </svg>
                                            </span>
                                            <input id="coupon_suffix" name="coupon_suffix">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="field-row no-padding">
                <div class="title">
                    <span>{{__("{$moduleName}::e.general")}}</span>
                </div>
            </div>

            <div class="field-row no-padding">
                <div class="field-wrap field-wrap-children">
                    <div class="inline-field-wrap input-select">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="amount">{{__("{$moduleName}::e.amount")}}*</label>
                            </div>

                            <div class="field-wrap">
                                <input id="amount" name="amount" value="{{@$item->amount}}" placeholder="0">
                                <select name="type" id="type" class="select2 no-search">
                                    @if(!empty($couponsTypesEnums))
                                        @foreach($couponsTypesEnums as $type)
                                            @if (@$type == 'fixed')
                                                <?php $name_type = $defaultCurrency->name ?>
                                            @else
                                                <?php $name_type = '%' ?>
                                            @endif

                                            <option value="{{$type}}" {{@$item->type == $type ? 'selected' : ''}}>{{$name_type}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="field-row no-padding">
                <div class="field-wrap field-wrap-children">

                    <div class="inline-field-wrap flex-1">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="start_date">{{__("{$moduleName}::e.start_date")}}</label>
                            </div>
                            <div class="field-wrap date">
                                <input name="start_date" id="start_date" class="datetimepicker" autocomplete="off"
                                       value="{{strtotime(@$item->start_date) ? date('d-m-Y', strtotime($item->start_date)) : ''}}">
                            </div>
                        </div>
                    </div>

                    <div class="inline-field-wrap flex-1">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="expiry_date">{{__("{$moduleName}::e.expiry_date")}}</label>
                            </div>
                            <div class="field-wrap date">
                                <input name="expiry_date" id="expiry_date" class="datetimepicker" autocomplete="off"
                                       value="{{strtotime(@$item->expiry_date) ? date('d-m-Y', strtotime($item->expiry_date)) : ''}}">
                            </div>
                        </div>
                    </div>

                    <div class="flex-1">
                        <div class="field-row switcher-container">
                            <div class="label-wrap">
                                <label for="allow_free_shipping">{{__("{$moduleName}::e.allow_free_shipping")}}</label>
                            </div>
                            <div class="checkbox-switcher">
                                <input type="checkbox" id="allow_free_shipping"
                                       name="allow_free_shipping" {{@$item->allow_free_shipping ? 'checked' : ''}}>
                                <label for="allow_free_shipping"></label>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="field-row no-padding">
                <div class="title">
                    <span>{{__("{$moduleName}::e.usage_restrictions_limits")}}</span>
                </div>
            </div>

            <div class="field-row">
                <div class="field-wrap field-wrap-children">
                    <div class="inline-field-wrap flex-1">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="limit_usage">{{__("{$moduleName}::e.limit_usage")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input id="limit_usage" name="limit_usage" value="{{@$item->limit_usage}}"
                                       placeholder="{{__("{$moduleName}::e.unlimited")}}">
                            </div>
                        </div>
                    </div>
                    <div class="inline-field-wrap flex-1">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="min_subtotal">{{__("{$moduleName}::e.min_subtotal")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input id="min_subtotal" name="min_subtotal" value="{{@$item->min_subtotal}}"
                                       placeholder="{{__("{$moduleName}::e.no_min")}}">
                            </div>
                        </div>
                    </div>
                    <div class="inline-field-wrap flex-1">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="max_subtotal">{{__("{$moduleName}::e.max_subtotal")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input name="max_subtotal" id="max_subtotal" value="{{@$item->max_subtotal}}"
                                       placeholder="{{__("{$moduleName}::e.no_max")}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <button class="button blue submit-form-btn"
            data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
</form>

