<?php

namespace Modules\Reports\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Modules\Coupons\Models\Coupons;
use Modules\Orders\Models\Cart;
use Modules\Orders\Models\CartProducts;
use Modules\Products\Models\ProductsCategoriesId;
use Modules\Products\Models\ProductsItemsId;
use Modules\SiteUsers\Models\Users;
use Modules\Products\Http\Helpers\Helpers as ProductsHelpers;

class ReportsController extends DefaultController
{


    private $moduleName;
    private $path;
    private $perPage;
    private $defaultCurrency;

    public function __construct()
    {
        $this->moduleName = 'reports';
        $this->path = "{$this->moduleName}::reports";
        $this->perPage = helpers()->getSettingsField('cms_items_per_page', $this->globalSettings());
        $this->defaultCurrency = getDefaultCurrency();
    }


    public function index(Request $request)
    {

        $ordersPage = $request->get('orders_page');
        $function = $ordersPage ? camel_case($ordersPage) : null;

        $range = $request->get('range', 'week');
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');
        $periodTo = now();
        $periodFrom = $periodTo->copy()->subWeek();

        switch ($range) {
            case 'year':
                $periodFrom = carbon()->startOfYear();
                break;
            case 'last_month':
                $periodFrom = $periodTo->copy()->subMonth()->startOfMonth();
                $periodTo = $periodTo->copy()->subMonth()->endOfMonth();
                break;
            case 'month':
                $periodFrom = $periodTo->copy()->startOfMonth();
                break;
            case 'custom':
                if ($startDate) {
                    if (!$endDate)
                        $periodFrom = carbon($startDate);
                    elseif ($endDate && carbon($startDate)->lte(carbon($endDate))) {
                        $periodFrom = carbon($startDate);
                        $periodTo = carbon($endDate);
                    } else
                        $periodFrom = $periodTo->copy()->subWeek();
                }
                break;
            default:
                $periodFrom = $periodTo->copy()->subWeek();
        }

        $periodInstance = CarbonPeriod::between($periodFrom->format('Y-m-d'), $periodTo->format('Y-m-d'));
        $countMonths = $range == 'year' || $range == 'custom' ? $periodFrom->diffInMonths($periodTo) : 0;
        $period = $countMonths > 1 ? $periodInstance->month() : $periodInstance->days();

        $labels = collect();
        foreach ($period as $key => $date) {
            $currDate = $date->copy();
            if ($ordersPage == 'sales_by_category' && $period->count() == $key + 1) {
                if ($countMonths > 1)
                    $currDate = $currDate->endOfMonth();
                else
                    $currDate = $currDate->endOfDay();
            }

            $labels->push([
                'fullDate' => $date,
                'month' => (int)$date->format('n'),
                'day' => (int)$date->format('j'),
                'timestamp' => $date->timestamp,
                'labelTimestamp' => $currDate->timestamp
            ]);
        }

        if ($function) {
            try {
                return $this->$function($request, $range, $startDate, $endDate, $labels, $countMonths);
            } catch (\Exception $e) {
                abort(404);
            }
        }


        return $this->salesByDate($request, $range, $startDate, $endDate, $labels, $countMonths);
    }

    public function salesByDate($request, $range, $startDate, $endDate, $labels, $countMonths)
    {

        $items = Cart::selectRaw('*, MONTH(order_date) month, DAY(order_date) day')
            ->notIsInProgress()
            ->where('status', '!=', 'cancelled')
            ->where(function ($q) use ($range, $startDate, $endDate) {
                switch ($range) {
                    case 'year':
                        $q->whereDate('order_date', '>=', carbon()->startOfYear()->format('Y-m-d'));
                        break;
                    case 'last_month':
                        $q->whereDate('order_date', '>=', now()->subMonth()->startOfMonth()->format('Y-m-d'));
                        $q->whereDate('order_date', '<=', now()->subMonth()->endOfMonth()->format('Y-m-d'));
                        break;
                    case 'month':
                        $q->whereDate('order_date', '>=', now()->startOfMonth()->format('Y-m-d'));
                        break;
                    case 'custom':
                        if ($startDate) {
                            if (!$endDate)
                                $q->whereDate('order_date', '>=', carbon($startDate)->format('Y-m-d'));
                            elseif ($endDate && carbon($startDate)->lte(carbon($endDate)))
                                $q->whereDate('order_date', '>=', carbon($startDate)->format('Y-m-d'))->whereDate('order_date', '<=', carbon($endDate)->format('Y-m-d'));
                            else
                                $q->whereDate('order_date', '>=', now()->subWeek()->format('Y-m-d'));
                        } else
                            $q->whereDate('order_date', '>=', now()->subWeek()->format('Y-m-d'));
                        break;
                    default:
                        $q->whereDate('order_date', '>=', now()->subWeek()->format('Y-m-d'));

                }
            })->orderBy('order_date')
            ->get()
            ->groupBy(function ($item) use ($countMonths) {
                return $countMonths > 1 ? $item->month : $item->day;
            });

        $ordersCount = $productsCount = $shippingAmount = $discountAmount = $totalAmount = 0;
        $points = $grossOrders = $netOrders = $orders = $products = $shipping = $coupons = [];

        if ($items->isNotEmpty()) {
            foreach ($labels as $label) {
                $currLabel = $label[$countMonths > 1 ? 'month' : 'day'];
                if (in_array($currLabel, $items->keys()->toArray())) {
                    $groupedItems = $items[$currLabel];
                    $countItemsSum = $groupedItems->sum('count_items');
                    $shippingAmountSum = $groupedItems->sum('shipping_amount');
                    $discountAmountSum = $groupedItems->sum('discount_amount');
                    $totalAmountSum = $groupedItems->sum('amount');
                    $ordersCountPartial = $groupedItems->count();

                    $ordersCount += $ordersCountPartial;
                    $productsCount += $countItemsSum;
                    $shippingAmount += $shippingAmountSum;
                    $discountAmount += $discountAmountSum;
                    $totalAmount += $totalAmountSum;

                    $points[] = [
                        'date' => $label['timestamp'],
                        'grossOrdersAmount' => numberFormat($totalAmountSum - ($discountAmountSum ? $discountAmountSum : 0) + ($shippingAmountSum ? $shippingAmountSum : 0)),
                        'netOrders' => numberFormat($totalAmountSum) ?: 0,
                        'orders' => $groupedItems->count(),
                        'products' => $countItemsSum,
                        'shipping' => $shippingAmountSum ?: 0,
                        'coupons' => $discountAmountSum ?: 0,
                    ];
                } else {
                    $points[] = [
                        'date' => $label['timestamp'],
                        'grossOrdersAmount' => 0,
                        'netOrders' => 0,
                        'orders' => 0,
                        'products' => 0,
                        'shipping' => 0,
                        'coupons' => 0,
                    ];
                }
            }
        }

        if ($request->get('action') == 'export') {
            $csvHeader = [
                'Date',
                'Products purchased',
                'Orders placed',
                'Shipping amount',
                'Discount amount',
                'Net order amount',
                'Gross order amount'
            ];

            $csvContent = [];

            foreach ($points as $point) {
                array_push($csvContent, [
                    @$point['date'] ? carbon()->createFromTimestamp($point['date'])->format('d-m-Y') : '-',
                    numberFormat(@$point['products'] ?: 0, 2, '.', '', true),
                    numberFormat(@$point['orders'] ?: 0, 2, '.', '', true),
                    numberFormat(@$point['shipping'] ?: 0, 2, '.', '', true),
                    numberFormat(@$point['coupons'] ?: 0, 2, '.', '', true),
                    numberFormat(@$point['netOrders'] ?: 0, 2, '.', '', true),
                    numberFormat(@$point['grossOrdersAmount'] ?: 0, 2, '.', '', true),

                ]);
            }

            $exportFile = $this->generateCsvFile(str_slug($range, '-'), $csvHeader, $csvContent);

            if ($exportFile)
                return response()->download($exportFile->fileLocation, $exportFile->downloadFileName)->deleteFileAfterSend();
        }

        $reports = [
            'labels' => $labels->pluck('timestamp')->toArray(),
            'labelsFormat' => $countMonths > 1 ? 'month' : 'day',
            'productsCount' => $productsCount,
            'points' => $points,
            'grossOrdersPoints' => $grossOrders,
            'netOrdersPoints' => $netOrders,
            'ordersPoints' => $orders,
            'productsPoints' => $products,
            'shippingPoints' => $shipping,
            'couponsPoints' => $coupons,
            'currency' => $this->defaultCurrency ? (@$this->defaultCurrency->symbol ? html_entity_decode($this->defaultCurrency->symbol) : $this->defaultCurrency->name) : '',
            'currPage' => $request->get('orders_page', 'sales_by_date') ?: 'sales_by_date',
        ];

        $response = [
            'path' => "{$this->path}",
            'currPath' => "{$this->path}.orders",
            'moduleName' => $this->moduleName,
            'currPageKey' => 'orders_page',
            'currPage' => $request->get('orders_page'),
            'currRange' => $range,
            'reports' => $reports,
            'defaultCurrency' => $this->defaultCurrency,
            'ordersCount' => $ordersCount,
            'productsCount' => $productsCount,
            'shippingAmount' => $shippingAmount,
            'discountAmount' => $discountAmount,
            'grossTotalAmount' => $totalAmount - $discountAmount + $shippingAmount,
            'netTotalAmount' => $totalAmount,
        ];

        return view("{$this->path}.orders.saleByDate", $response);
    }

    public function salesByProduct($request, $range, $startDate, $endDate, $labels, $countMonths)
    {

        $productsIds = parseArrayQueryParams($request->all(), 'products_ids');

        $itemsBuild = CartProducts::join('cart', 'cart.id', '=', 'cart_products.cart_id')
            ->where(function ($q) {
                $q->where('cart.status', '!=', 'cancelled');
                $q->whereNotNull('cart.status');
                $q->where('cart.is_draft', 0);
            })
            ->where(function ($q) use ($range, $startDate, $endDate) {
                switch ($range) {
                    case 'year':
                        $q->whereDate('cart.order_date', '>=', carbon()->startOfYear()->format('Y-m-d'));
                        break;
                    case 'last_month':
                        $q->whereDate('cart.order_date', '>=', now()->subMonth()->startOfMonth()->format('Y-m-d'));
                        $q->whereDate('cart.order_date', '<=', now()->subMonth()->endOfMonth()->format('Y-m-d'));
                        break;
                    case 'month':
                        $q->whereDate('cart.order_date', '>=', now()->startOfMonth()->format('Y-m-d'));
                        break;
                    case 'custom':
                        if ($startDate) {
                            if (!$endDate)
                                $q->whereDate('cart.order_date', '>=', carbon($startDate)->format('Y-m-d'));
                            elseif ($endDate && carbon($startDate)->lte(carbon($endDate)))
                                $q->whereDate('cart.order_date', '>=', carbon($startDate)->format('Y-m-d'))->whereDate('cart.order_date', '<=', carbon($endDate)->format('Y-m-d'));
                            else
                                $q->whereDate('cart.order_date', '>=', now()->subWeek()->format('Y-m-d'));
                        } else
                            $q->whereDate('cart.order_date', '>=', now()->subWeek()->format('Y-m-d'));
                        break;
                    default:
                        $q->whereDate('cart.order_date', '>=', now()->subWeek()->format('Y-m-d'));

                }
            });

        $topSellersProductsBuild = clone $itemsBuild;

        $items = $itemsBuild->selectRaw('cart_products.*, MONTH(cart.order_date) month, DAY(cart.order_date) day')
            ->whereIn('cart_products.products_item_id', $productsIds)
            ->orderBy('cart.order_date')
            ->get()
            ->groupBy(function ($item) use ($countMonths) {
                return $countMonths > 1 ? $item->month : $item->day;
            });

        $productsCount = $productsAmount = 0;
        $points = [];

        foreach ($labels as $label) {
            $currLabel = $label[$countMonths > 1 ? 'month' : 'day'];
            if ($items->isNotEmpty() && in_array($currLabel, $items->keys()->toArray())) {
                $groupedItems = $items[$currLabel];
                $productsCountSum = $groupedItems->sum('quantity');
                $productsAmountSum = $groupedItems->sum(function ($item) {
                    return $item->amount * $item->quantity;
                });

                $productsCount += $productsCountSum;
                $productsAmount += $productsAmountSum;

                $points[] = [
                    'date' => $label['timestamp'],
                    'productsCount' => $productsCountSum,
                    'productsAmount' => numberFormat($productsAmountSum) ?: 0,
                ];
            } else {
                $points[] = [
                    'date' => $label['timestamp'],
                    'productsCount' => 0,
                    'productsAmount' => 0
                ];
            }
        }

        if ($request->get('action') == 'export') {
            $csvHeader = [
                'Date',
                'Number of items sold',
                'Sales amount'
            ];

            $csvContent = [];

            foreach ($points as $point) {
                array_push($csvContent, [
                    @$point['date'] ? carbon()->createFromTimestamp($point['date'])->format('d-m-Y') : '-',
                    $point['productsCount'] ?: 0,
                    numberFormat(@$point['productsAmount'] ?: 0, 2, '.', '', true),

                ]);
            }

            $exportFile = $this->generateCsvFile(str_slug($range, '-'), $csvHeader, $csvContent);

            if ($exportFile)
                return response()->download($exportFile->fileLocation, $exportFile->downloadFileName)->deleteFileAfterSend();
        }

        $reports = [
            'labels' => $labels->pluck('timestamp')->toArray(),
            'labelsFormat' => $countMonths > 1 ? 'month' : 'day',
            'points' => $points,
            'currency' => $this->defaultCurrency ? (@$this->defaultCurrency->symbol ? html_entity_decode($this->defaultCurrency->symbol) : $this->defaultCurrency->name) : '',
            'currPage' => $request->get('orders_page'),
            'productsCount' => $productsCount,
        ];

        $selectedProducts = ProductsItemsId::whereIn('id', $productsIds)
            ->with('globalName')
            ->get();

        $topSellersProducts = $topSellersProductsBuild->selectRaw('cart_products.*, SUM(quantity) as products_qty')->orderBy('products_qty', 'desc')
            ->with('product.globalName')
            ->limit(12)
            ->groupBy('products_item_id')
            ->get();

        $response = [
            'path' => "{$this->path}",
            'currPath' => "{$this->path}.orders",
            'moduleName' => $this->moduleName,
            'currPageKey' => 'orders_page',
            'currPage' => $request->get('orders_page'),
            'currRange' => $range,
            'reports' => $reports,
            'defaultCurrency' => $this->defaultCurrency,
            'productsCount' => $productsCount,
            'productsAmount' => $productsAmount,
            'existItems' => $items->isNotEmpty() || !empty($productsIds),
            'selectedProducts' => $selectedProducts,
            'topSellersProducts' => $topSellersProducts,
        ];

        return view("{$this->path}.orders.saleByProduct", $response);
    }

    public function getProducts(Request $request)
    {

        if (!$request->ajax())
            abort(404);

        $items = ProductsItemsId::where(function ($q) use ($request) {
            $q->where('id', "{$request->get('q')}");
            $q->orWhereHas('globalName', function ($q) use ($request) {
                $q->where('name', 'LIKE', "%{$request->get('q')}%");
            });
        })
            ->with('globalName')
            ->orderBy('position')
            ->paginate(10);

        $response = selectAjaxSearchItems($request, $items);

        return $response;
    }

    public function salesByCategory($request, $range, $startDate, $endDate, $labels, $countMonths)
    {
        $categoriesIds = parseArrayQueryParams($request->all(), 'categories_ids');
        $chartColours = ['#3498db', '#34495e', '#1abc9c', '#2ecc71', '#f1c40f', '#e67e22', '#e74c3c', '#2980b9', '#8e44ad', '#2c3e50', '#16a085', '#27ae60', '#f39c12', '#d35400', '#c0392b'];

        $items = CartProducts::selectRaw('cart_products.*, products_items_categories.products_category_id as products_category_id, cart.order_date, MONTH(cart.order_date) month, DAY(cart.order_date) day')
            ->join('cart', 'cart.id', '=', 'cart_products.cart_id')
            ->join('products_items_categories', 'products_items_categories.products_item_id', '=', 'cart_products.products_item_id')
            ->whereIn('products_items_categories.products_category_id', $categoriesIds)
            ->where(function ($q) {
                $q->where('cart.status', '!=', 'cancelled');
                $q->whereNotNull('cart.status');
                $q->where('cart.is_draft', 0);
            })
            ->where(function ($q) use ($range, $startDate, $endDate) {
                switch ($range) {
                    case 'year':
                        $q->whereDate('cart.order_date', '>=', carbon()->startOfYear()->format('Y-m-d'));
                        break;
                    case 'last_month':
                        $q->whereDate('cart.order_date', '>=', now()->subMonth()->startOfMonth()->format('Y-m-d'));
                        $q->whereDate('cart.order_date', '<=', now()->subMonth()->endOfMonth()->format('Y-m-d'));
                        break;
                    case 'month':
                        $q->whereDate('cart.order_date', '>=', now()->startOfMonth()->format('Y-m-d'));
                        break;
                    case 'custom':
                        if ($startDate) {
                            if (!$endDate)
                                $q->whereDate('cart.order_date', '>=', carbon($startDate)->format('Y-m-d'));
                            elseif ($endDate && carbon($startDate)->lte(carbon($endDate)))
                                $q->whereDate('cart.order_date', '>=', carbon($startDate)->format('Y-m-d'))->whereDate('cart.order_date', '<=', carbon($endDate)->format('Y-m-d'));
                            else
                                $q->whereDate('cart.order_date', '>=', now()->subWeek()->format('Y-m-d'));
                        } else
                            $q->whereDate('cart.order_date', '>=', now()->subWeek()->format('Y-m-d'));
                        break;
                    default:
                        $q->whereDate('cart.order_date', '>=', now()->subWeek()->format('Y-m-d'));

                }
            })->orderBy('cart.order_date')
            ->get()
            ->groupBy(function ($item) use ($countMonths) {
                return $countMonths > 1 ? $item->month : $item->day;
            });

        $points = [];
        $exportPoints = collect();
        $categoriesArr = collect();
        $historyCategories = collect();
        $allProductsCategories = ProductsCategoriesId::with(['globalName', 'recursiveChildren'])->get();

        foreach ($labels as $label) {
            $currLabel = $label[$countMonths > 1 ? 'month' : 'day'];

            if ($items->isNotEmpty() && in_array($currLabel, $items->keys()->toArray())) {
                $groupedItemsByCategory = $items[$currLabel]->sortBy('id')->groupBy('products_category_id');
                $iteration = 0;

                foreach ($groupedItemsByCategory as $categoryId => $itemPerCategory) {
                    $productsAmountSum = $itemPerCategory->sum(function ($item) {
                        return $item->amount * $item->quantity;
                    });

                    $currCategory = $allProductsCategories->where('id', $categoryId)->first();

                    $currCategoryDataArr = [
                        'id' => $categoryId,
                        'name' => @$currCategory->globalName->name,
                        'amount' => $productsAmountSum,
                        'amountStr' => html_entity_decode(formatPrice($productsAmountSum, $this->defaultCurrency)),
                        'color' => @$chartColours[$iteration] ?: $chartColours[0],
                        'date' => $label['timestamp']
                    ];

                    $points[] = $currCategoryDataArr;
                    $exportPoints->push($currCategoryDataArr);
                    $categoriesArr->push($currCategoryDataArr);

                    $iteration++;
                }
            } else
                $exportPoints->push([
                    'id' => 0,
                    'name' => '',
                    'amount' => 0,
                    'date' => $label['timestamp']
                ]);
        }

        if ($request->get('action') == 'export') {
            $csvHeader = [
                'Date',
            ];

            $csvContent = [];

            foreach (collect($points)->groupBy('date') as $date => $groupedPoints) {
                foreach ($groupedPoints as $groupedPoint) {
                    if (!in_array(@$groupedPoint['name'], $csvHeader))
                        $csvHeader[$groupedPoint['id']] = @$groupedPoint['name'] ?: 'No name';
                }
            }

            foreach ($exportPoints->groupBy('date') as $date => $groupedPoints) {
                $tempExportPointsArr = [
                    carbon()->createFromTimestamp($date)->format('d-m-Y')
                ];
                foreach ($csvHeader as $categoryId => $headerItem) {
                    if ($categoryId != 0) {
                        $existPoint = $groupedPoints->where('id', $categoryId)->first();
                        $tempExportPointsArr[] = numberFormat(@$existPoint['amount'] ?: 0, 2, '.', '', true);
                    }

                }

                array_push($csvContent, $tempExportPointsArr);
            }

            $exportFile = $this->generateCsvFile(str_slug($range, '-'), $csvHeader, $csvContent);

            if ($exportFile)
                return response()->download($exportFile->fileLocation, $exportFile->downloadFileName)->deleteFileAfterSend();
        }

        if ($categoriesArr->isNotEmpty()) {
            foreach ($categoriesArr->groupBy('id') as $categoriesArrItem) {
                $firstItem = @$categoriesArrItem->first();
                $historyCategoriesAmountSum = $categoriesArrItem->sum('amount');

                $historyCategories->push([
                    'id' => $categoriesArrItem->first()['id'],
                    'name' => @$firstItem['name'],
                    'color' => @$firstItem['color'],
                    'amount' => $historyCategoriesAmountSum,
                ]);
            }
        }

        $reports = [
            'labels' => $labels->pluck('labelTimestamp')->toArray(),
            'labelsFormat' => $countMonths > 1 ? 'month' : 'day',
            'points' => $points,
            'currency' => $this->defaultCurrency ? (@$this->defaultCurrency->symbol ? html_entity_decode($this->defaultCurrency->symbol) : $this->defaultCurrency->name) : '',
            'currPage' => $request->get('orders_page'),
        ];

        $allParentsCategories = $allProductsCategories->where('p_id', null);

        $response = [
            'path' => "{$this->path}",
            'currPath' => "{$this->path}.orders",
            'moduleName' => $this->moduleName,
            'currPageKey' => 'orders_page',
            'currPage' => $request->get('orders_page'),
            'currRange' => $range,
            'reports' => $reports,
            'defaultCurrency' => $this->defaultCurrency,
            'existItems' => $items->isNotEmpty() || !empty($categoriesIds),
            'allParentsCategories' => $allParentsCategories,
            'categoriesIds' => $categoriesIds,
            'historyCategories' => $historyCategories
        ];

        return view("{$this->path}.orders.saleByCategory", $response);
    }

    public function getCategories(Request $request)
    {

        if (!$request->ajax())
            abort(404);

        $items = ProductsCategoriesId::where(function ($q) use ($request) {
            $q->where('id', "{$request->get('q')}");
            $q->orWhereHas('globalName', function ($q) use ($request) {
                $q->where('name', 'LIKE', "%{$request->get('q')}%");
            });
        })
            ->with('globalName')
            ->orderBy('position')
            ->paginate(10);

        $response = selectAjaxSearchItems($request, $items);

        return $response;
    }

    public function couponsByDate($request, $range, $startDate, $endDate, $labels, $countMonths)
    {

        $couponsIds = parseArrayQueryParams($request->all(), 'coupons_ids');

        $itemsBuild = CartProducts::join('cart', 'cart.id', '=', 'cart_products.cart_id')
            ->join('coupons', 'coupons.id', '=', 'cart.discount_id')
            ->where(function ($q) {
                $q->where('cart.status', '!=', 'cancelled');
                $q->whereNotNull('cart.status');
                $q->where('cart.is_draft', 0);
            })
            ->where(function ($q) use ($range, $startDate, $endDate) {
                switch ($range) {
                    case 'year':
                        $q->whereDate('cart.order_date', '>=', carbon()->startOfYear()->format('Y-m-d'));
                        break;
                    case 'last_month':
                        $q->whereDate('cart.order_date', '>=', now()->subMonth()->startOfMonth()->format('Y-m-d'));
                        $q->whereDate('cart.order_date', '<=', now()->subMonth()->endOfMonth()->format('Y-m-d'));
                        break;
                    case 'month':
                        $q->whereDate('cart.order_date', '>=', now()->startOfMonth()->format('Y-m-d'));
                        break;
                    case 'custom':
                        if ($startDate) {
                            if (!$endDate)
                                $q->whereDate('cart.order_date', '>=', carbon($startDate)->format('Y-m-d'));
                            elseif ($endDate && carbon($startDate)->lte(carbon($endDate)))
                                $q->whereDate('cart.order_date', '>=', carbon($startDate)->format('Y-m-d'))->whereDate('cart.order_date', '<=', carbon($endDate)->format('Y-m-d'));
                            else
                                $q->whereDate('cart.order_date', '>=', now()->subWeek()->format('Y-m-d'));
                        } else
                            $q->whereDate('cart.order_date', '>=', now()->subWeek()->format('Y-m-d'));
                        break;
                    default:
                        $q->whereDate('cart.order_date', '>=', now()->subWeek()->format('Y-m-d'));

                }
            });

        $topSellersCouponsBuild = clone $itemsBuild;

        $items = $itemsBuild->selectRaw('cart_products.*, cart.discount_amount, MONTH(cart.order_date) month, DAY(cart.order_date) day')
            ->where(function ($q) use ($couponsIds) {
                if (!empty($couponsIds))
                    $q->whereIn('cart.discount_id', $couponsIds);
            })
            ->orderBy('cart.order_date')
            ->get()
            ->groupBy(function ($item) use ($countMonths) {
                return $countMonths > 1 ? $item->month : $item->day;
            });

        $couponsCount = $couponsAmount = 0;
        $points = [];

        foreach ($labels as $label) {
            $currLabel = $label[$countMonths > 1 ? 'month' : 'day'];
            if ($items->isNotEmpty() && in_array($currLabel, $items->keys()->toArray())) {
                $groupedItems = $items[$currLabel];
                $couponsCountSum = $groupedItems->count();
                $couponsAmountSum = $groupedItems->sum('discount_amount');

                $couponsCount += $couponsCountSum;
                $couponsAmount += $couponsAmountSum;

                $points[] = [
                    'date' => $label['timestamp'],
                    'couponsCount' => $couponsCountSum,
                    'couponsAmount' => numberFormat($couponsAmountSum) ?: 0,
                ];
            } else {
                $points[] = [
                    'date' => $label['timestamp'],
                    'couponsCount' => 0,
                    'couponsAmount' => 0
                ];
            }
        }

        if ($request->get('action') == 'export') {
            $csvHeader = [
                'Date',
                'Number of coupons used',
                'Discount amount'
            ];

            $csvContent = [];

            foreach ($points as $point) {
                array_push($csvContent, [
                    @$point['date'] ? carbon()->createFromTimestamp($point['date'])->format('d-m-Y') : '-',
                    $point['couponsCount'] ?: 0,
                    numberFormat(@$point['couponsAmount'] ?: 0, 2, '.', '', true),

                ]);
            }

            $exportFile = $this->generateCsvFile(str_slug($range, '-'), $csvHeader, $csvContent);

            if ($exportFile)
                return response()->download($exportFile->fileLocation, $exportFile->downloadFileName)->deleteFileAfterSend();
        }

        $reports = [
            'labels' => $labels->pluck('timestamp')->toArray(),
            'labelsFormat' => $countMonths > 1 ? 'month' : 'day',
            'points' => $points,
            'currency' => $this->defaultCurrency ? (@$this->defaultCurrency->symbol ? html_entity_decode($this->defaultCurrency->symbol) : $this->defaultCurrency->name) : '',
            'currPage' => $request->get('orders_page'),
            'couponsCount' => $couponsCount,
        ];

        $selectedCoupons = Coupons::whereIn('id', $couponsIds)->get();

        $topSellersCoupons = $topSellersCouponsBuild->selectRaw('cart_products.*, count(*) as coupons_count, coupons.code as coupon_code, cart.discount_id as coupon_id')->orderBy('coupons_count', 'desc')
            ->limit(12)
            ->groupBy('discount_id')
            ->get();

        $response = [
            'path' => "{$this->path}",
            'currPath' => "{$this->path}.orders",
            'moduleName' => $this->moduleName,
            'currPageKey' => 'orders_page',
            'currPage' => $request->get('orders_page'),
            'currRange' => $range,
            'reports' => $reports,
            'defaultCurrency' => $this->defaultCurrency,
            'couponsCount' => $couponsCount,
            'couponsAmount' => $couponsAmount,
            'selectedCoupons' => $selectedCoupons,
            'topSellersCoupons' => $topSellersCoupons,
        ];

        return view("{$this->path}.orders.couponsByDate", $response);
    }

    public function getCoupons(Request $request)
    {

        if (!$request->ajax())
            abort(404);

        $items = Coupons::where(function ($q) use ($request) {
            $q->where('id', "{$request->get('q')}");
            $q->orWhere('code', "{$request->get('q')}");
        })->paginate(10);

        $response = selectAjaxSearchItems($request, $items, 'code', null);

        return $response;
    }

    public function customers(Request $request)
    {

        $customersPage = $request->get('customers_page');
        $function = $customersPage ? camel_case($customersPage) : null;

        $range = $request->get('range', 'week');
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');
        $periodTo = now();
        $periodFrom = $periodTo->copy()->subWeek();

        switch ($range) {
            case 'year':
                $periodFrom = carbon()->startOfYear();
                break;
            case 'last_month':
                $periodFrom = $periodTo->copy()->subMonth()->startOfMonth();
                $periodTo = $periodTo->copy()->subMonth()->endOfMonth();
                break;
            case 'month':
                $periodFrom = $periodTo->copy()->startOfMonth();
                break;
            case 'custom':
                if ($startDate) {
                    if (!$endDate)
                        $periodFrom = carbon($startDate);
                    elseif ($endDate && carbon($startDate)->lte(carbon($endDate))) {
                        $periodFrom = carbon($startDate);
                        $periodTo = carbon($endDate);
                    } else
                        $periodFrom = $periodTo->copy()->subWeek();
                }
                break;
            default:
                $periodFrom = $periodTo->copy()->subWeek();
        }

        $periodInstance = CarbonPeriod::between($periodFrom->format('Y-m-d'), $periodTo->format('Y-m-d'));
        $countMonths = $range == 'year' || $range == 'custom' ? $periodFrom->diffInMonths($periodTo) : 0;
        $period = $countMonths > 1 ? $periodInstance->month() : $periodInstance->days();

        $labels = collect();
        foreach ($period as $key => $date) {
            $currDate = $date->copy();
            if ($period->count() == $key + 1) {
                if ($countMonths > 1)
                    $currDate = $currDate->endOfMonth();
                else
                    $currDate = $currDate->endOfDay();
            }

            $labels->push([
                'fullDate' => $date,
                'month' => (int)$date->format('n'),
                'day' => (int)$date->format('j'),
                'timestamp' => $date->timestamp,
                'labelTimestamp' => $currDate->timestamp
            ]);
        }

        if ($function) {
            try {
                return $this->$function($request, $range, $startDate, $endDate, $labels, $countMonths);
            } catch (\Exception $e) {
                dd($e->getMessage());
                //abort(404);
            }
        }

        return $this->customersVsGuests($request, $range, $startDate, $endDate, $labels, $countMonths);
    }

    public function customersVsGuests($request, $range, $startDate, $endDate, $labels, $countMonths)
    {

        $items = CartProducts::selectRaw('cart_products.*, cart.auth_user_id, MONTH(cart.order_date) month, DAY(cart.order_date) day')
            ->join('cart', 'cart.id', '=', 'cart_products.cart_id')
            ->where(function ($q) {
                $q->where('cart.status', '!=', 'cancelled');
                $q->whereNotNull('cart.status');
                $q->where('cart.is_draft', 0);
            })
            ->where(function ($q) use ($range, $startDate, $endDate) {
                switch ($range) {
                    case 'year':
                        $q->whereDate('cart.order_date', '>=', carbon()->startOfYear()->format('Y-m-d'));
                        break;
                    case 'last_month':
                        $q->whereDate('cart.order_date', '>=', now()->subMonth()->startOfMonth()->format('Y-m-d'));
                        $q->whereDate('cart.order_date', '<=', now()->subMonth()->endOfMonth()->format('Y-m-d'));
                        break;
                    case 'month':
                        $q->whereDate('cart.order_date', '>=', now()->startOfMonth()->format('Y-m-d'));
                        break;
                    case 'custom':
                        if ($startDate) {
                            if (!$endDate)
                                $q->whereDate('cart.order_date', '>=', carbon($startDate)->format('Y-m-d'));
                            elseif ($endDate && carbon($startDate)->lte(carbon($endDate)))
                                $q->whereDate('cart.order_date', '>=', carbon($startDate)->format('Y-m-d'))->whereDate('cart.order_date', '<=', carbon($endDate)->format('Y-m-d'));
                            else
                                $q->whereDate('cart.order_date', '>=', now()->subWeek()->format('Y-m-d'));
                        } else
                            $q->whereDate('cart.order_date', '>=', now()->subWeek()->format('Y-m-d'));
                        break;
                    default:
                        $q->whereDate('cart.order_date', '>=', now()->subWeek()->format('Y-m-d'));

                }
            })->orderBy('cart.order_date')
            ->get()
            ->groupBy(function ($item) use ($countMonths) {
                return $countMonths > 1 ? $item->month : $item->day;
            });

        $authorisedUsersAmount = $unauthorisedUsersAmount = $authorisedUsersCount = $unauthorisedUsersCount = 0;
        $points = [];

        foreach ($labels as $label) {
            $currLabel = $label[$countMonths > 1 ? 'month' : 'day'];
            if ($items->isNotEmpty() && in_array($currLabel, $items->keys()->toArray())) {
                $groupedItems = $items[$currLabel];
                $authorisedItems = $groupedItems->where('auth_user_id', '!=', null);
                $unauthorisedItems = $groupedItems->where('auth_user_id', null);

                $authorisedUsersAmountSum = $authorisedItems->sum(function ($item) {
                    return $item->amount * $item->quantity;
                });
                $unauthorisedUsersAmountSum = $unauthorisedItems->sum(function ($item) {
                    return $item->amount * $item->quantity;
                });

                $authorisedUsersCountSum = $authorisedItems->count();
                $unauthorisedUsersCountSum = $unauthorisedItems->count();

                $authorisedUsersAmount += $authorisedUsersAmountSum;
                $unauthorisedUsersAmount += $unauthorisedUsersAmountSum;
                $authorisedUsersCount += $authorisedUsersCountSum;
                $unauthorisedUsersCount += $unauthorisedUsersCountSum;

                $points[] = [
                    'date' => $label['timestamp'],
                    'authorisedUsersAmount' => numberFormat($authorisedUsersAmount) ?: 0,
                    'unauthorisedUsersAmount' => numberFormat($unauthorisedUsersAmount) ?: 0,
                    'authorisedUsersCount' => $authorisedUsersCount,
                    'unauthorisedUsersCount' => $unauthorisedUsersCount,
                ];
            } else {
                $points[] = [
                    'date' => $label['timestamp'],
                    'authorisedUsersAmount' => 0,
                    'unauthorisedUsersAmount' => 0,
                    'authorisedUsersCount' => 0,
                    'unauthorisedUsersCount' => 0
                ];
            }
        }

        if ($request->get('action') == 'export') {
            $csvHeader = [
                'Date',
                'Authorised users amount',
                'Authorised users orders count',
                'Unauthorised users amount',
                'Unauthorised users orders count'
            ];

            $csvContent = [];

            foreach ($points as $point) {
                array_push($csvContent, [
                    @$point['date'] ? carbon()->createFromTimestamp($point['date'])->format('d-m-Y') : '-',
                    numberFormat(@$point['authorisedUsersAmount'] ?: 0, 2, '.', '', true),
                    $point['authorisedUsersCount'] ?: 0,
                    numberFormat(@$point['unauthorisedUsersAmount'] ?: 0, 2, '.', '', true),
                    $point['unauthorisedUsersCount'] ?: 0,
                ]);
            }

            $exportFile = $this->generateCsvFile(str_slug($range, '-'), $csvHeader, $csvContent);

            if ($exportFile)
                return response()->download($exportFile->fileLocation, $exportFile->downloadFileName)->deleteFileAfterSend();
        }

        $reports = [
            'labels' => $labels->pluck('labelTimestamp')->toArray(),
            'labelsFormat' => $countMonths > 1 ? 'month' : 'day',
            'points' => $points,
            'currency' => $this->defaultCurrency ? (@$this->defaultCurrency->symbol ? html_entity_decode($this->defaultCurrency->symbol) : $this->defaultCurrency->name) : '',
            'currPage' => 'customers_vs_guests',
            'count' => $authorisedUsersCount > $unauthorisedUsersCount ? $authorisedUsersCount : $unauthorisedUsersCount
        ];


        $response = [
            'path' => "{$this->path}",
            'currPath' => "{$this->path}.customers",
            'reportsPage' => $request->segment(4),
            'moduleName' => $this->moduleName,
            'currPageKey' => 'customers_page',
            'currPage' => $request->get('customers_page'),
            'currRange' => $range,
            'reports' => $reports,
            'defaultCurrency' => $this->defaultCurrency,
            'authorisedUsersAmount' => $authorisedUsersAmount,
            'unauthorisedUsersAmount' => $unauthorisedUsersAmount,
            'authorisedUsersCount' => $authorisedUsersCount,
            'unauthorisedUsersCount' => $unauthorisedUsersCount,
        ];

        return view("{$this->path}.customers.customersVsGuests", $response);
    }

    public function customersList($request)
    {

        $response = $this->customersListFilter($request);

        $response = array_merge($response, [
            'path' => "{$this->path}",
            'reportsPage' => $request->segment(4),
            'currPageKey' => 'customers_page',
            'currPage' => $request->get('customers_page'),
        ]);

        return view("{$this->path}.customers.customersList", $response);
    }

    public function customersListFilter(Request $request)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', $this->globalSettings());

        $filterTableList = filterTableList($request, $request->except('page'));
        $filterParams = $filterTableList->filterParams;
        $pushUrl = $filterTableList->pushUrl;

        $items = Users::where(function ($q) use ($filterParams, $request) {
            if (array_key_exists('name', $filterParams) && !is_array($filterParams['name']))
                $q->where('name', 'like', "%{$filterParams['name']}%");

            if (array_key_exists('email', $filterParams) && !is_array($filterParams['email']))
                $q->where('email', $filterParams['email']);

        })->with(['orders' => function ($q) {
            $q->notIsInProgress();
            $q->orderBy('order_date', 'desc');
        }])->orderBy('created_at', 'desc')
            ->paginate($perPage);

        if ($request->ajax())
            $items->setPath(url(LANG, ['admin', $this->currComponent()->slug, 'customers']) . $pushUrl);

        $currPath = "{$this->path}.customers";

        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'moduleName' => $this->moduleName,
            'defaultCurrency' => $this->defaultCurrency,
            'currPath' => $currPath,
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$currPath}.table", $response)->render();
            } catch (\Throwable $e) {
            }

            return response()->json($response);
        }

        return $response;
    }

    public function stock(Request $request)
    {
        $stockPage = $request->get('stock_page');
        $function = $stockPage ? camel_case($stockPage) : null;
        $perPage = Helpers::getSettingsField('cms_items_per_page', $this->globalSettings());

        if ($function) {
            try {
                return $this->$function($request, $perPage);
            } catch (\Exception $e) {
                abort(404);
            }
        }

        return $this->lowInStock($request, $perPage);
    }

    public function lowInStock($request, $perPage)
    {

        $defaultStockUnits = config('cms.admin.lowStock', 3);

        $items = ProductsItemsId::select('products_items_id.*', 'products_items_variations_details_id.id as variation_id', 'products_items_variations_details_id.stock_quantity as variation_stock_quantity')
            ->leftJoin('products_items_variations_details_id', 'products_items_variations_details_id.products_item_id', '=', 'products_items_id.id')
            ->where(function ($q) use ($defaultStockUnits) {
                $q->where(function ($q) use ($defaultStockUnits) {
                    $q->where('item_type_id', 1);
                    $q->where('products_items_id.use_stock', 1);
                    $q->where('products_items_id.stock_quantity', '<=', $defaultStockUnits);
                    $q->where('products_items_id.stock_quantity', '>', 0);
                });
                $q->orWhere(function ($q) use ($defaultStockUnits) {
                    $q->where('item_type_id', 2);
                    $q->where('products_items_id.use_stock', 0);
                    $q->where('products_items_variations_details_id.use_stock', 1);
                    $q->where('products_items_variations_details_id.stock_quantity', '<=', $defaultStockUnits);
                    $q->where('products_items_variations_details_id.stock_quantity', '>', 0);
                });
            })
            ->with(['globalName', 'productItemsAttributes.attribute.globalName', 'productItemsAttributes.options.attribute.globalName', 'variationsDetails.variations'])
            ->paginate($perPage);

        if ($items->isNotEmpty())
            $items->each(function ($item) {
                if ($item->isVariationProduct) {
                    $attributes = ProductsHelpers::getProductSelectedAttributes($item, true, false, false);
                    $variation = ProductsHelpers::getProductDefaultVariation($item, $attributes, $item->variation_id, $item->variationsDetails);
                    $defaultVariationsAttrNamesArr = @$variation->defaultVariationsAttrNames ?: collect();
                    $defaultVariationsAttrNamesStr = '';
                    $customAttrNamesStr = '';
                    $iteration = 0;
                    if ($defaultVariationsAttrNamesArr->isNotEmpty())
                        foreach ($defaultVariationsAttrNamesArr as $key => $attr) {
                            $iteration++;
                            $defaultVariationsAttrNamesStr .= "<span class='bold'>{$attr['parent']}</span>: {$attr['child']}" . ($defaultVariationsAttrNamesArr->count() > $iteration ? ', ' : '');
                            $customAttrNamesStr .= $attr['child'] . ($defaultVariationsAttrNamesArr->count() > $iteration ? ', ' : '');
                        }

                    $item->variationAttributesNames = $defaultVariationsAttrNamesStr;
                    $item->customAttrNamesStr = $customAttrNamesStr;
                } else {
                    $item->variationAttributesNames = '';
                    $item->customAttrNamesStr = '';
                }
            });

        $response = [
            'path' => "{$this->path}",
            'reportsPage' => $request->segment(4),
            'currPageKey' => 'stock_page',
            'currPage' => $request->get('stock_page'),
            'moduleName' => $this->moduleName,
            'defaultCurrency' => $this->defaultCurrency,
            'currPath' => "{$this->path}.stock",
            'items' => $items,
        ];

        return view("{$this->path}.stock.lowInStockList", $response);
    }

    public function outOfStock($request, $perPage)
    {

        $items = ProductsItemsId::select('products_items_id.*', 'products_items_variations_details_id.id as variation_id', 'products_items_variations_details_id.stock_quantity as variation_stock_quantity')
            ->leftJoin('products_items_variations_details_id', 'products_items_variations_details_id.products_item_id', '=', 'products_items_id.id')
            ->where(function ($q) {
                $q->where(function ($q) {
                    $q->where('item_type_id', 1);
                    $q->where(function ($q) {
                        $q->where('products_items_id.use_stock', 1);
                        $q->where('products_items_id.stock_quantity', 0);
                    });
                    $q->orWhere(function ($q) {
                        $q->where('products_items_id.use_stock', 0);
                        $q->where('products_items_id.status', 'out_of_stock');
                    });
                });
                $q->orWhere(function ($q) {
                    $q->where('item_type_id', 2);
                    $q->where(function ($q) {
                        $q->where('products_items_id.use_stock', 1);
                        $q->where('products_items_id.stock_quantity', 0);
                    });
                    $q->orWhere(function ($q) {
                        $q->where('products_items_variations_details_id.use_stock', 1);
                        $q->where('products_items_variations_details_id.stock_quantity', 0);
                    });
                    $q->orWhere(function ($q) {
                        $q->where('products_items_variations_details_id.use_stock', 0);
                        $q->where('products_items_variations_details_id.status', 'out_of_stock');
                    });
                });
            })
            ->with(['globalName', 'productItemsAttributes.attribute.globalName', 'productItemsAttributes.options.attribute.globalName', 'variationsDetails.variations'])
            ->paginate($perPage);

        if ($items->isNotEmpty())
            $items->each(function ($item) {
                if ($item->isVariationProduct) {
                    $attributes = ProductsHelpers::getProductSelectedAttributes($item, true, false, false);
                    $variation = ProductsHelpers::getProductDefaultVariation($item, $attributes, $item->variation_id, $item->variationsDetails);
                    $defaultVariationsAttrNamesArr = @$variation->defaultVariationsAttrNames ?: collect();
                    $defaultVariationsAttrNamesStr = '';
                    $customAttrNamesStr = '';
                    $iteration = 0;
                    if ($defaultVariationsAttrNamesArr->isNotEmpty())
                        foreach ($defaultVariationsAttrNamesArr as $key => $attr) {
                            $iteration++;
                            $defaultVariationsAttrNamesStr .= "<span class='bold'>{$attr['parent']}</span>: {$attr['child']}" . ($defaultVariationsAttrNamesArr->count() > $iteration ? ', ' : '');
                            $customAttrNamesStr .= $attr['child'] . ($defaultVariationsAttrNamesArr->count() > $iteration ? ', ' : '');
                        }

                    $item->variationAttributesNames = $defaultVariationsAttrNamesStr;
                    $item->customAttrNamesStr = $customAttrNamesStr;
                } else {
                    $item->variationAttributesNames = '';
                    $item->customAttrNamesStr = '';
                }
            });

        $response = [
            'path' => "{$this->path}",
            'reportsPage' => $request->segment(4),
            'currPageKey' => 'stock_page',
            'currPage' => $request->get('stock_page'),
            'moduleName' => $this->moduleName,
            'defaultCurrency' => $this->defaultCurrency,
            'currPath' => "{$this->path}.stock",
            'items' => $items,
        ];

        return view("{$this->path}.stock.outOfStockList", $response);
    }

    private function generateCsvFile(string $range, array $header, array $content)
    {
        $exportsDir = storage_path('app/exports');
        $fileLocation = $exportsDir . "/report-{$range}-" . time() . ".csv";

        if (!file_exists($exportsDir))
            File::makeDirectory($exportsDir, 0775, true, true);

        $handler = fopen($fileLocation, 'w+');

        if (filesize($fileLocation) == 0)
            fputcsv($handler, $header);

        if (!empty($content))
            foreach ($content as $item)
                fputcsv($handler, $item);

        fclose($handler);

        $downloadFileName = "report-{$range}-" . now()->format('d-m-Y') . ".csv";

        $response = [
            'fileLocation' => $fileLocation,
            'downloadFileName' => $downloadFileName
        ];
        return (object)$response;
    }
}
