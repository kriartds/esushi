<div class="reports-links-block">
    <div class="item">
        <a href="{{customUrl(['admin', $currComponent->slug, @$reportsPage])}}?stock_page=low_in_stock" {{$currPage == 'low_in_stock' || !$currPage ? 'class=active' : ''}}>{{__("{$moduleName}::e.low_stock")}}</a>
    </div>
    <div class="item">
        <a href="{{customUrl(['admin', $currComponent->slug, @$reportsPage])}}?stock_page=out_of_stock" {{$currPage == 'out_of_stock' ? 'class=active' : ''}}>{{__("{$moduleName}::e.out_of_stock")}}</a>
    </div>
</div>