@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">

        @include('admin.templates.pageTopButtons', ['reportsAction' => ['orders', 'customers', 'stock']])
        @include("{$currPath}.linksBlock")

        <div class="table-block">
            @if(!$items->isEmpty())
                <table class="table">
                    <thead>
                    <tr>
                        <th>{{__("{$moduleName}::e.id_table")}}</th>
                        <th class="small">{{__("{$moduleName}::e.variation_id")}}</th>
                        <th class="medium">{{__("{$moduleName}::e.product")}}</th>
                        <th class="medium">{{__("{$moduleName}::e.parent")}}</th>
                        <th>{{__("{$moduleName}::e.units_stock")}}</th>
                        <th>{{__("{$moduleName}::e.stock_status")}}</th>
                        <th class="small">{{__("{$moduleName}::e.actions")}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td>{{$item->id}}</td>
                            <td class="small">{{$item->variation_id ?: '-'}}</td>
                            <td class="medium">
                                @if(@$item->isVariationProduct)
                                    <div><span class="bold">{{@$item->globalName->name}}</span> - {{$item->customAttrNamesStr}}</div>
                                    <div>{!! $item->variationAttributesNames !!}</div>
                                @else
                                    <span class="bold">{{@$item->globalName->name}}</span>
                                @endif
                            </td>
                            <td class="medium">{{@$item->isVariationProduct ? @$item->globalName->name : '-'}}</td>
                            <td>{{@$item->isVariationProduct ? $item->variation_stock_quantity : $item->stock_quantity}}</td>
                            <td>{{__("{$moduleName}::e.in_stock")}}</td>
                            <td class="small">
                                <div class="actions">
                                    @if($permissions->edit)
                                        <a href="{{url(LANG, ['admin', 'products', 'edit', $item->id, DEF_LANG_ID])}}"
                                           class="edit-link action-item-reports tooltip"
                                           title="{{__("{$moduleName}::e.edit_table")}}"
                                           target="_blank"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                                                <defs>
                                                    <clipPath id="clip-path">
                                                        <rect id="Rectangle_628" data-name="Rectangle 628" width="16" height="16" transform="translate(400 766)" fill="#fff" stroke="#707070" stroke-width="1"></rect>
                                                    </clipPath>
                                                </defs>
                                                <g id="Mask_Group_35" data-name="Mask Group 35" transform="translate(-400 -766)" clip-path="url(#clip-path)">
                                                    <g id="edit" transform="translate(399.283 765.438)">
                                                        <path id="Path_2225" data-name="Path 2225" d="M7.889,2.869H2.869A1.434,1.434,0,0,0,1.434,4.3v10.04a1.434,1.434,0,0,0,1.434,1.434h10.04a1.434,1.434,0,0,0,1.434-1.434V9.323" fill="none" stroke="#ff5e47" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.434"></path>
                                                        <path id="Path_2226" data-name="Path 2226" d="M13.267,1.793a1.521,1.521,0,0,1,2.151,2.151L8.606,10.757l-2.869.717.717-2.869Z" fill="none" stroke="#ff5e47" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.434"></path>
                                                    </g>
                                                </g>
                                            </svg></a>
                                    @endif
                                    <a {{@$item->active ? "href=" . url(LANG, ['products', $item->slug, $item->isVariationProduct ? $item->variation_id : '']) : ''}}
                                       class="view-link action-item-reports  tooltip {{!@$item->active ? 'disabled' : ''}}"
                                       title="{{__("{$moduleName}::e.view_table")}}"
                                       target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="33" height="19" viewBox="0 0 33 19"><g><g><g><path fill="#34ca76" d="M16.2 17c-5.3 0-10.3-2.7-13.8-7.5C4.3 6.9 6.6 5 9.2 3.7c-.6 1.1-1 2.5-1 3.9 0 4.4 3.6 8 8 8s8-3.6 8-8c0-1.4-.4-2.7-1-3.9C25.8 5 28.1 6.9 30 9.5c-3.5 4.8-8.5 7.5-13.8 7.5zM14.8 4c0 .4-.3.8-.8.8-1.2 0-2.1.9-2.1 2.1 0 .4-.3.8-.8.8-.4 0-.8-.3-.8-.8 0-2 1.6-3.6 3.6-3.6.6 0 .9.3.9.7zM32 8.9c-2.2-3.3-5.2-5.9-8.6-7.4-4.6-2-9.8-2-14.4 0C5.6 3 2.6 5.6.4 8.9l-.4.6.4.6C4.2 15.7 10 19 16.2 19c6.2 0 12-3.3 15.8-8.9l.4-.6z"></path></g></g></g></svg>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
                        <tfoot>
                        <tr>
                            <td colspan="10">
                                @include('admin.templates.pagination', ['pagination' => $items])
                            </td>
                        </tr>
                        </tfoot>
                    @endif
                </table>
            @else
                <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
            @endif

        </div>

    </div>

@stop

@include('admin.footer')