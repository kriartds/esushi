@if(!$items->isEmpty())
    <table class="table">
        <thead>
        <tr>
            <th>{{__("{$moduleName}::e.id_table")}}</th>
            <th class="medium">{{__("{$moduleName}::e.name")}}</th>
            <th>{{__("{$moduleName}::e.email")}}</th>
{{--            <th class="medium location">{{__("{$moduleName}::e.location")}}</th>--}}
            <th >{{__("{$moduleName}::e.orders")}}</th>
            <th >{{__("{$moduleName}::e.money_spent")}}</th>
            <th>{{__("{$moduleName}::e.last_order")}}</th>
            <th class="checkbox-all align-center" >
                <div>Check all</div>
            </th>
            <th class="small">{{__("{$moduleName}::e.actions")}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            @php
                $orders = $item->orders;
                $lastOrders = $orders->isNotEmpty() ? $orders->first() : null;
            @endphp
            <tr id="{{$item->id}}">
                <td>{{$item->id}}</td>
                <td class="medium">{{@$item->name}}</td>
                <td>{{@$item->email}}</td>
{{--                <td class="medium location">{{@$item->country->name ?: ''}}{{@$item->region ? ", {$item->region->name}" : ''}}</td>--}}
                <td class="addColor">{{@$orders->count()}}</td>
                <td class="addFonts">{!! formatPrice(@$item->ordersMoneySpent, $defaultCurrency) !!}</td>
                <td class="lastOrder">
                    @if($lastOrders)
                        <a href="{{customUrl(['admin', 'orders', 'edit', $lastOrders->id])}}"
                           target="_blank">#{{$lastOrders->id}}</a>
                        @if($lastOrders->order_date)
                            <span>{{carbon($lastOrders->order_date)->format('d-m-Y')}}</span>
                        @endif
                    @else
                        -
                    @endif
                </td>
                <td class="small">
                    <div class="actions">
                        @if($permissions->edit)
                            <a href="{{url(LANG, ['admin', 'site-users', 'edit', $item->id])}}"
                               class="edit-link action-item tooltip" title="{{__("{$moduleName}::e.edit_table")}}"
                               target="_blank"></a>
                        @endif
                        <a {{@$orders->count() ? "href=" . url(LANG, ['admin', 'orders']) . "?users_ids=[{$item->id}]" : ''}}
                           class="view-link action-item tooltip {{!@$orders->count() ? 'disabled' : ''}}"
                           title="{{__("{$moduleName}::e.view_table")}}"
                           target="_blank"></a>
                    </div>
                </td>
                @if($permissions->delete)
                    {{--TODO: de verificat daca lucreaza corect --}}
                        <td class="checkbox-items">
                            <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                   name="checkbox_items[{{$item->id}}]"
                                   value="{{$item->id}}"

                                   data-url="{{adminUrl([$currComponent->slug, 'destroy'])}}">
                            <label for="{{$item->id}}">
                                <span>{{__("e.select")}}</span>
                            </label>
                        </td>
                    @else
                        <td class="medium">{{__("e.role_has_users")}}</td>
                    @endif

            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
