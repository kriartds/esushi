<div class="reports-links-block">
    <div class="item">
        <a href="{{customUrl(['admin', $currComponent->slug, @$reportsPage])}}?customers_page=customers_vs_guests" {{$currPage == 'customers_vs_guests' || !$currPage ? 'class=active' : ''}}>{{__("{$moduleName}::e.customers_vs_guests")}}</a>
    </div>
    <div class="item">
        <a href="{{customUrl(['admin', $currComponent->slug, @$reportsPage])}}?customers_page=customers_list" {{$currPage == 'customers_list' ? 'class=active' : ''}}>{{__("{$moduleName}::e.customers_list")}}</a>
    </div>
</div>