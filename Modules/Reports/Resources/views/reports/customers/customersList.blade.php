@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">

        @include('admin.templates.pageTopButtons', ['reportsAction' => ['orders', 'customers', 'stock'], 'filter' => ['path' => $path, 'filterParams' => $filterParams, 'items' => $items]])
        @include("{$currPath}.linksBlock")

        <div class="table-block">
            @include("{$currPath}.table", ['items' => $items, 'componentSlug' => $currComponent->slug])
        </div>

    </div>

@stop

@include('admin.footer')