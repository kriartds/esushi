@if(isset($filterParams))
    <div class="filterBlock">
        <div class="button-filter-gray">
            <button type="button" class="button gray filter-btn">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12">
                    <g>
                        <g>
                            <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50"
                                  stroke-width="2"
                                  d="M1 1h14"></path>
                        </g>
                        <g>
                            <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50"
                                  stroke-width="2"
                                  d="M4 6h8"></path>
                        </g>
                        <g>
                            <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50"
                                  stroke-width="2"
                                  d="M6 11h4"></path>
                        </g>
                    </g>
                </svg>
                Filters
                <span class="count">{{isset($items) && ((!empty($filterParams) || empty($filterParams)) && $items->total() > 0) ? $items->total() : ""}}</span>
            </button>
        </div>
        <div class="filter form-block">
            <form method="post"
                  action="{{url(LANG, ['admin', $currComponent->slug, 'customersListFilter']) . "?customers_page={$currPage}"}}"
                  class="filter-form"
                  id="filter-form">
                <div class="field-row-wrap">
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="name">{{__("{$moduleName}::e.name")}}</label>
                        </div>
                        <div class="field-wrap">
                            <input name="name" id="name"
                                   value="{{ !empty($filterParams) && array_key_exists('name', $filterParams) ? $filterParams['name'] : '' }}">
                        </div>
                    </div>
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="email">{{__("{$moduleName}::e.email")}}</label>
                        </div>
                        <div class="field-wrap">
                            <input name="email" id="email"
                                   value="{{ !empty($filterParams) && array_key_exists('email', $filterParams) ? $filterParams['email'] : '' }}">
                        </div>
                    </div>
                </div>
                <div class="field-btn">
                    <button class="btn btn-inline half submit-form-btn" data-form-id="filter-form"
                            data-form-event="submit-form">{{__("{$moduleName}::e.filter")}}
                    </button>
                    <button class="btn btn-inline half submit-form-btn light-blue" data-form-id="filter-form"
                            data-form-event="refresh-form">{{__("{$moduleName}::e.reset")}}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endif