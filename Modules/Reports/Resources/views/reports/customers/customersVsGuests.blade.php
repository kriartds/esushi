@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">

        @include('admin.templates.pageTopButtons', ['reportsAction' => ['orders', 'customers', 'stock']])
        @include("{$currPath}.linksBlock")
        @include("{$path}.filter", ['query' => queryToString(['range', 'start_date', 'end_date'])])

        <div class="table-block">
            <div class="reports-chart-block">
                <div class="reports-sidebar-wrap">
                    <div id="reports-legend" class="legend-block">
                        <div class="item authorisedUsersAmount" data-series="0" data-default-color="#3498db">
                            <div class="value">{!! formatPrice($authorisedUsersAmount, $defaultCurrency) !!}</div>
                            <div class="title">{{__("{$moduleName}::e.authorised_users_amount")}}</div>
                        </div>
                        <div class="item authorisedUsersCount" data-series="2" data-default-color="#b1d4ea">
                            <div class="value">{{$authorisedUsersCount}}</div>
                            <div class="title">{{__("{$moduleName}::e.authorised_users_orders_count")}}</div>
                        </div>
                        <div class="item unauthorisedUsersAmount" data-series="1" data-default-color="#5cc488">
                            <div class="value">{!! formatPrice($unauthorisedUsersAmount, $defaultCurrency) !!}</div>
                            <div class="title">{{__("{$moduleName}::e.unauthorised_users_amount")}}</div>
                        </div>
                        <div class="item unauthorisedUsersCount" data-series="3" data-default-color="#dbe1e3">
                            <div class="value">{{$unauthorisedUsersCount}}</div>
                            <div class="title">{{__("{$moduleName}::e.unauthorised_users_orders_count")}}</div>
                        </div>
                    </div>
                </div>
                <div class="chart" data-chart-values="{{json_encode($reports)}}">
                    <canvas id="reports-orders-chart"></canvas>
                </div>
            </div>
        </div>

    </div>

@stop

@include('admin.footer')