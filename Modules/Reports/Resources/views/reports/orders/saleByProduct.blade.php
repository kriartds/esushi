@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">

        @include('admin.templates.pageTopButtons', ['reportsAction' => ['orders', 'customers', 'stock']])
        @include("{$currPath}.linksBlock")
        @include("{$path}.filter", ['query' => queryToString(['range', 'start_date', 'end_date'])])

        <div class="table-block">
            <div class="reports-chart-block">
                <div class="reports-sidebar-wrap">
                    @if($existItems)
                        <div id="reports-legend" class="legend-block">
                            <div class="item productsAmount" data-series="0" data-default-color="#3498db">
                                <div class="value">{!! formatPrice($productsAmount, $defaultCurrency) !!}</div>
                                <div class="title">{{__("{$moduleName}::e.sales_selected_items")}}</div>
                            </div>
                            <div class="item productsCount" data-series="1" data-default-color="#dbe1e3">
                                <div class="value">{{$productsCount}}</div>
                                <div class="title">{{__("{$moduleName}::e.purchases_selected_items")}}</div>
                            </div>
                        </div>
                    @endif
                    <div class="reports-sidebar">
                        @if(@$selectedProducts && $selectedProducts->isNotEmpty())
                            <div class="reports-results">
                                <div class="title">{{__("{$moduleName}::e.showing_reports_for")}}</div>
                                <div class="reports-results-wrap">
                                    @foreach($selectedProducts as $key => $product)
                                        <span class="item">{{@$product->globalName->name}}(#{{$product->id}}) {{$selectedProducts->count() > $key + 1 ? ', ' : ''}}</span>
                                    @endforeach
                                    <div class="reset-reports-result">
                                        <a href="{{customUrl(['admin', $currComponent->slug]) . "?orders_page={$currPage}" . ($currRange ? "&range={$currRange}" : "")}}">{{__("{$moduleName}::e.reset")}}</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="reports-actions-sidebar">
                            <div class="item-wrap">
                                <div class="title">{{__("{$moduleName}::e.product_search")}}</div>
                                <div class="hidden-content">
                                    <form action="{{customUrl(['admin', $currComponent->slug])}}" method="get">
                                        <input type="hidden" name="orders_page" value="{{$currPage}}">
                                        <input type="hidden" name="range" value="{{$currRange}}">
                                        <div class="field-row">
                                            <div class="label-wrap">
                                                <label for="reports-products">{{__("{$moduleName}::e.search_product")}}</label>
                                            </div>
                                            <div class="field-wrap">
                                                <select autocomplete="off" name="products_ids[]"
                                                        id="reports-products"
                                                        class="select2 ajax"
                                                        data-url="{{customUrl(['admin', $currComponent->slug, 'getProducts'])}}"
                                                        multiple>
                                                </select>
                                            </div>
                                        </div>
                                        <button class="btn btn-inline">{{__("{$moduleName}::e.show")}}</button>
                                    </form>
                                </div>
                            </div>
                            @if($topSellersProducts->isNotEmpty())
                                <div class="item-wrap">
                                    <div class="title">{{__("{$moduleName}::e.top_sellers")}}</div>
                                    <div class="hidden-content">
                                        @foreach($topSellersProducts as $product)
                                            <div class="top-sellers-item">
                                                <div class="qty">{{$product->products_qty}}</div>
                                                <div class="name">
                                                    <a href="{{customUrl(['admin', $currComponent->slug]) . (($query = queryToString(['products_ids'])) ? "?{$query}&products_ids=[{$product->products_item_id}]" : "?products_ids=[{$product->products_item_id}]")}}">{{@$product->product->globalName->name}}</a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                @if($existItems)
                    <div class="chart" data-chart-values="{{json_encode($reports)}}">
                        <canvas id="reports-orders-chart"></canvas>
                    </div>
                @else
                    <div class="empty-chart">
                        {{__("{$moduleName}::e.choose_product_for_stats")}}
                    </div>
                @endif
            </div>
        </div>

    </div>

@stop

@include('admin.footer')