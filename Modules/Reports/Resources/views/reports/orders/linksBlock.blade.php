<div class="reports-links-block">
    <div class="item">
        <a href="{{customUrl(['admin', $currComponent->slug])}}?orders_page=sales_by_date" {{$currPage == 'sales_by_date' || !$currPage ? 'class=active' : ''}}>{{__("{$moduleName}::e.sales_date")}}</a>
    </div>
    <div class="item">
        <a href="{{customUrl(['admin', $currComponent->slug])}}?orders_page=sales_by_product" {{$currPage == 'sales_by_product' ? 'class=active' : ''}}>{{__("{$moduleName}::e.sales_product")}}</a>
    </div>
    <div class="item">
        <a href="{{customUrl(['admin', $currComponent->slug])}}?orders_page=sales_by_category" {{$currPage == 'sales_by_category' ? 'class=active' : ''}}>{{__("{$moduleName}::e.sales_category")}}</a>
    </div>
    <div class="item">
        <a href="{{customUrl(['admin', $currComponent->slug])}}?orders_page=coupons_by_date" {{$currPage == 'coupons_by_date' ? 'class=active' : ''}}>{{__("{$moduleName}::e.coupons_date")}}</a>
    </div>
</div>