@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">

        @include('admin.templates.pageTopButtons', ['reportsAction' => ['orders', 'customers', 'stock']])
        @include("{$currPath}.linksBlock")
        @include("{$path}.filter", ['query' => queryToString(['range', 'start_date', 'end_date'])])

        <div class="table-block">
            <div class="reports-chart-block">
                <div class="reports-sidebar-wrap">
                    <div id="reports-legend" class="legend-block">
                        <div class="item couponsAmount" data-series="0" data-default-color="#3498db">
                            <div class="value">{!! formatPrice($couponsAmount, $defaultCurrency) !!}</div>
                            <div class="title">{{__("{$moduleName}::e.discounts_in_total")}}</div>
                        </div>
                        <div class="item couponsCount" data-series="1" data-default-color="#dbe1e3">
                            <div class="value">{{$couponsCount}}</div>
                            <div class="title">{{__("{$moduleName}::e.coupons_used_in_total")}}</div>
                        </div>
                    </div>
                    <div class="reports-sidebar">
                        @if(@$selectedCoupons && $selectedCoupons->isNotEmpty())
                            <div class="reports-results">
                                <div class="title">{{__("{$moduleName}::e.showing_reports_for")}}</div>
                                <div class="reports-results-wrap">
                                    @foreach($selectedCoupons as $key => $coupon)
                                        <span class="item">{{@$coupon->code}}(#{{$coupon->id}}) {{$selectedCoupons->count() > $key + 1 ? ', ' : ''}}</span>
                                    @endforeach
                                    <div class="reset-reports-result">
                                        <a href="{{customUrl(['admin', $currComponent->slug]) . "?orders_page={$currPage}" . ($currRange ? "&range={$currRange}" : "")}}">{{__("{$moduleName}::e.reset")}}</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="reports-actions-sidebar">
                            <div class="item-wrap">
                                <div class="title">{{__("{$moduleName}::e.coupon_search")}}</div>
                                <div class="hidden-content">
                                    <form action="{{customUrl(['admin', $currComponent->slug])}}" method="get">
                                        <input type="hidden" name="orders_page" value="{{$currPage}}">
                                        <input type="hidden" name="range" value="{{$currRange}}">
                                        <div class="field-row">
                                            <div class="label-wrap">
                                                <label for="reports-coupons">{{__("{$moduleName}::e.search_for_coupon")}}</label>
                                            </div>
                                            <div class="field-wrap">
                                                <select autocomplete="off" name="coupons_ids[]"
                                                        id="reports-coupons"
                                                        class="select2 ajax"
                                                        data-url="{{customUrl(['admin', $currComponent->slug, 'getCoupons'])}}"
                                                        multiple>
                                                </select>
                                            </div>
                                        </div>
                                        <button class="btn btn-inline">{{__("{$moduleName}::e.show")}}</button>
                                    </form>
                                </div>
                            </div>
                            @if($topSellersCoupons->isNotEmpty())
                                <div class="item-wrap">
                                    <div class="title">{{__("{$moduleName}::e.top_sellers")}}</div>
                                    <div class="hidden-content">
                                        @foreach($topSellersCoupons as $coupon)
                                            <div class="top-sellers-item">
                                                <div class="qty">{{$coupon->coupons_count}}</div>
                                                <div class="name">
                                                    <a href="{{customUrl(['admin', $currComponent->slug]) . (($query = queryToString(['coupons_ids'])) ? "?{$query}&coupons_ids=[{$coupon->coupon_id}]" : "?coupons_ids=[{$coupon->coupon_id}]")}}">{{@$coupon->coupon_code}}</a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="chart" data-chart-values="{{json_encode($reports)}}">
                    <canvas id="reports-orders-chart"></canvas>
                </div>
            </div>
        </div>

    </div>

@stop

@include('admin.footer')