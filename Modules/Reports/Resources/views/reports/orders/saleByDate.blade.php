@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['reportsAction' => ['orders', 'customers', 'stock']])
        @include("{$currPath}.linksBlock")
        @include("{$path}.filter", ['query' => queryToString(['range', 'start_date', 'end_date'])])

        <div class="table-block">
            <div class="reports-chart-block">
                <div class="reports-sidebar-wrap">
                    <div id="reports-legend" class="legend-block">
                        <div class="item grossOrderAmount" data-series="0" data-default-color="#b1d4ea">
                            <div class="value">{!! formatPrice($grossTotalAmount, $defaultCurrency) !!}</div>
                            <div class="title">{{__("{$moduleName}::e.gross_order_amount")}}</div>
                        </div>
                        <div class="item netOrderAmount" data-series="1" data-default-color="#3498db">
                            <div class="value">{!! formatPrice($netTotalAmount, $defaultCurrency) !!}</div>
                            <div class="title">{{__("{$moduleName}::e.net_order_amount")}}</div>
                        </div>
                        <div class="item ordersPlaced" data-series="5" data-default-color="#dbe1e3">
                            <div class="value">{{$ordersCount}}</div>
                            <div class="title">{{__("{$moduleName}::e.orders_placed")}}</div>
                        </div>
                        <div class="item productsPurchased" data-series="4" data-default-color="#E67F46">
                            <div class="value">{{$productsCount}}</div>
                            <div class="title">{{__("{$moduleName}::e.products_purchased")}}</div>
                        </div>
                        <div class="item shippingAmount" data-series="2" data-default-color="#5cc488">
                            <div class="value">{!! formatPrice($shippingAmount, $defaultCurrency) !!}</div>
                            <div class="title">{{__("{$moduleName}::e.shipping_amount")}}</div>
                        </div>
                        <div class="item discountAmount" data-series="3" data-default-color="#f1c40f">
                            <div class="value">{!! formatPrice($discountAmount, $defaultCurrency) !!}</div>
                            <div class="title">{{__("{$moduleName}::e.discount_amount")}}</div>
                        </div>
                    </div>
                </div>
                <div class="chart" data-chart-values="{{json_encode($reports)}}">
                    <canvas id="reports-orders-chart"></canvas>
                </div>
            </div>
        </div>

    </div>

@stop

@include('admin.footer')