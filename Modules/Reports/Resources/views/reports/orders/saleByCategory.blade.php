@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">

        @include('admin.templates.pageTopButtons', ['reportsAction' => ['orders', 'customers', 'stock']])
        @include("{$currPath}.linksBlock")
        @include("{$path}.filter", ['query' => queryToString(['range', 'start_date', 'end_date'])])

        <div class="table-block">
            <div class="reports-chart-block">
                <div class="reports-sidebar-wrap">
                    @if($historyCategories->isNotEmpty())
                        <div id="reports-legend" class="legend-block">
                            @foreach($historyCategories as $key => $category)
                                <div class="item productsAmount" data-series="{{$category['id']}}"
                                     data-default-color="{{@$category['color']}}" data-page="{{$currPage}}"
                                     style="border-right: 5px solid {{@$category['color']}}">
                                    <div class="value">{!! formatPrice(@$category['amount'], $defaultCurrency) !!}</div>
                                    <div class="title">{{__("{$moduleName}::e.sales_in")}} <i>{{$category['name']}}</i>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    <div class="reports-sidebar">
                        <div class="reports-actions-sidebar no-action">
                            <div class="item-wrap">
                                <div class="title">Categories</div>
                                <form action="{{customUrl(['admin', $currComponent->slug])}}" method="get">
                                    <input type="hidden" name="orders_page" value="{{$currPage}}">
                                    <input type="hidden" name="range" value="{{$currRange}}">
                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="reports-categories">{{__("{$moduleName}::e.search_for_categories")}}</label>
                                        </div>
                                        <div class="field-wrap">
                                            <select autocomplete="off" name="categories_ids[]"
                                                    id="reports-categories"
                                                    class="select2" multiple>
                                                @if(@$allParentsCategories && $allParentsCategories->isNotEmpty())
                                                    {!! getParentChildrenInSelect($allParentsCategories, $categoriesIds) !!}
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <button class="btn btn-inline">{{__("{$moduleName}::e.show")}}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @if($existItems)
                    <div class="chart" data-chart-values="{{json_encode($reports)}}">
                        <canvas id="reports-orders-chart"></canvas>
                    </div>
                @else
                    <div class="empty-chart">
                        {{__("{$moduleName}::e.choose_category_for_stats")}}
                    </div>
                @endif
            </div>
        </div>

    </div>

@stop

@include('admin.footer')