<div class="reports-filter">
    <div class="items">

        <select class="select2" name="state" id="reportsInterval">
            <option {{$currRange == 'week' || !$currRange || $currRange == 'custom' && !request()->get('start_date') ? 'selected' : ''}} data-href="{{customUrl(['admin', $currComponent->slug, @$reportsPage]) . (@$query ? "?{$query}&range=week" : '?range=week')}}">Last 7 days</option>
            <option {{$currRange == 'month' ? 'selected' : ''}} data-href="{{customUrl(['admin', $currComponent->slug, @$reportsPage]) . (@$query ? "?{$query}&range=month" : '?range=month')}}">Current month</option>
            <option {{$currRange == 'last_month' ? 'selected' : ''}} data-href="{{customUrl(['admin', $currComponent->slug, @$reportsPage]) . (@$query ? "?{$query}&range=last_month" : '?range=last_month')}}">Last month</option>
            <option {{$currRange == 'year' ? 'selected' : ''}} data-href="{{customUrl(['admin', $currComponent->slug, @$reportsPage]) . (@$query ? "?{$query}&range=year" : '?range=year')}}" >Year</option>
        </select>

{{--        <div class="item">--}}
{{--            <a href="{{customUrl(['admin', $currComponent->slug, @$reportsPage]) . (@$query ? "?{$query}&range=year" : '?range=year')}}" {{$currRange == 'year' ? 'class=active' : ''}}>{{__("{$moduleName}::e.year")}}</a>--}}
{{--        </div>--}}
{{--        <div class="item">--}}
{{--            <a href="{{customUrl(['admin', $currComponent->slug, @$reportsPage]) . (@$query ? "?{$query}&range=last_month" : '?range=last_month')}}" {{$currRange == 'last_month' ? 'class=active' : ''}}>{{__("{$moduleName}::e.last_month")}}</a>--}}
{{--        </div>--}}
{{--        <div class="item">--}}
{{--            <a href="{{customUrl(['admin', $currComponent->slug, @$reportsPage]) . (@$query ? "?{$query}&range=month" : '?range=month')}}" {{$currRange == 'month' ? 'class=active' : ''}}>{{__("{$moduleName}::e.current_month")}}</a>--}}
{{--        </div>--}}
{{--        <div class="item">--}}
{{--            <a href="{{customUrl(['admin', $currComponent->slug, @$reportsPage]) . (@$query ? "?{$query}&range=week" : '?range=week')}}" {{$currRange == 'week' || !$currRange || $currRange == 'custom' && !request()->get('start_date') ? 'class=active' : ''}}>{{__("{$moduleName}::e.last_7_days")}}</a>--}}
{{--        </div>--}}

        <div class="item">
            <form action="{{customUrl(['admin', $currComponent->slug, @$reportsPage])}}"
                  method="get">
                @if(@$currPageKey && @$currPage)
                    <input type="hidden" name="{{$currPageKey}}" value="{{$currPage}}">
                @endif
                <input type="hidden" name="range" value="custom">
                @if(request()->get('products_ids') && $currPage == 'sales_by_product')
                    <input type="hidden" name="products_ids"
                           value="{{is_array(request()->get('products_ids')) ? '[' . implode(',', request()->get('products_ids')) . ']' : request()->get('products_ids')}}">
                @endif
                @if(request()->get('categories_ids') && $currPage == 'sales_by_category')
                    <input type="hidden" name="categories_ids"
                           value="{{is_array(request()->get('categories_ids')) ? '[' . implode(',', request()->get('categories_ids')) . ']' : request()->get('categories_ids')}}">
                @endif
                <div class="title {{$currRange == 'custom' && request()->get('start_date') ? 'active' : ''}}">
                    {{__("{$moduleName}::e.custom")}}
                </div>
                <div class="field-row">
                    <div class="field-wrap">
                        <input name="start_date" id="start_date" class="datetimepicker" placeholder="From date"
                               autocomplete="off" value="{{request()->get('start_date')}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="field-wrap">
                        <input name="end_date" id="end_date" class="datetimepicker" placeholder="To date"
                               autocomplete="off" value="{{request()->get('end_date')}}">
                    </div>
                </div>
                <button class="btn btn-inline half">{{__("{$moduleName}::e.filter")}}</button>
            </form>
        </div>
    </div>
    <div class="export">
        <div class="item">
            <a href="{{customUrl(['admin', $currComponent->slug, @$reportsPage]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() . '&action=export' : '?action=export')}}">{{__("{$moduleName}::e.export_csv")}}</a>
        </div>
    </div>
</div>