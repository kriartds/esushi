<?php

namespace Modules\Subscribers\Exports;

use Modules\Subscribers\Models\Subscribers;
use Maatwebsite\Excel\Concerns\FromCollection;

class SubscribersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Subscribers::get(['email']);
    }
}
