<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Modules\\Subscribers\\Database\\Seeders\\SubscribersDatabaseSeeder' => $baseDir . '/Database/Seeders/SubscribersDatabaseSeeder.php',
    'Modules\\Subscribers\\Exports\\SubscribersExport' => $baseDir . '/Exports/SubscribersExport.php',
    'Modules\\Subscribers\\Http\\Controllers\\SubscribersController' => $baseDir . '/Http/Controllers/SubscribersController.php',
    'Modules\\Subscribers\\Http\\Controllers\\SubscribersEmailsController' => $baseDir . '/Http/Controllers/SubscribersEmailsController.php',
    'Modules\\Subscribers\\Http\\Requests\\SubscribesRequest' => $baseDir . '/Http/Requests/SubscribesRequest.php',
    'Modules\\Subscribers\\Models\\Subscribers' => $baseDir . '/Models/Subscribers.php',
    'Modules\\Subscribers\\Models\\SubscribersEmails' => $baseDir . '/Models/SubscribersEmails.php',
    'Modules\\Subscribers\\Providers\\RouteServiceProvider' => $baseDir . '/Providers/RouteServiceProvider.php',
    'Modules\\Subscribers\\Providers\\SubscribersServiceProvider' => $baseDir . '/Providers/SubscribersServiceProvider.php',
);
