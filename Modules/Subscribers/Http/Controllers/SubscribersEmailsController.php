<?php

namespace Modules\Subscribers\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use Modules\Subscribers\Http\Requests\SubscribesRequest;
use Modules\Subscribers\Models\Subscribers;
use Modules\Subscribers\Models\SubscribersEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubscribersEmailsController extends DefaultController
{

    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'subscribers';
        $this->path = "{$this->moduleName}::subscribers.emails";
    }

    public function index(Request $request)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = SubscribersEmails::paginate($perPage);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'items' => $items,
        ];

        return view("{$this->path}.list", $response);

    }

    public function create()
    {

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id, $langId)
    {
        $item = SubscribersEmails::findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'langId' => $langId
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function sendEmailTemplate(Request $request, $id)
    {
        $item = SubscribersEmails::findOrFail($id);

        $emailSubscribers = Subscribers::where(function ($q) use ($item) {
            if ($item->for_news_subscribers)
                $q->where('news', 1);

            if ($item->for_new_products_subscribers)
                $q->where('new_products', 1);
        })->get();

        foreach ($emailSubscribers as $subscriber) {
            try {
                helpers()->sendMail($subscriber->email, $subscriber->email, ['subscriber' => $subscriber, 'item' => $item], 'subscribeTemplate', parent::globalSettings(), 'You have new offer from ' . env('APP_URL'));
            } catch (\Exception $e) {
            }
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => "Email was successful send!",
                'type' => 'success'
            ]
        ]);
    }

    public function save(SubscribesRequest $request, $id, $langId)
    {

        $currComponent = parent::currComponent();
        $item = SubscribersEmails::find($id);

        if (is_null($item)) {
            $item = new SubscribersEmails();
        }
        $item->name = $request->get('name', null);
        $item->description = $request->get('description', null);
        $item->for_news_subscribers = $request->get('for_news_subscribers', null) == 'on' ? 1 : 0;
        $item->for_new_products_subscribers = $request->get('for_new_products_subscribers', null) == 'on' ? 1 : 0;

        $item->save();

        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId])
            ]);
    }
    public function actionsCheckbox(Request $request)
    {
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        $items = SubscribersEmails::whereIn('id', $ItemsId)->get();

        if (!$items->isEmpty()) {
            foreach ($items as $item) {
                $item->delete();
                helpers()->logActions(parent::currComponent(), $item, $item->name, 'deleted');
            }
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

}