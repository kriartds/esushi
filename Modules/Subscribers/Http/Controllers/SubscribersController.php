<?php

namespace Modules\Subscribers\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Models\Languages;
use Modules\Subscribers\Exports\SubscribersExport;
use App\Http\Helpers\Helpers;
use Modules\Subscribers\Models\Subscribers;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SubscribersController extends DefaultController
{

    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'subscribers';
        $this->path = "{$this->moduleName}::subscribers";
    }

    public function index(Request $request)
    {

        $response = $this->filter($request);

        $response['path'] = $this->path;

        return view("{$this->path}.list", $response);

    }

    public function filter(Request $request)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $filterParams = array_filter($request->except('page'));

        if (!$request->ajax()) {
            $newFiltersParams = [];

            if (!empty($filterParams) && count($filterParams) > 0) {

                foreach ($filterParams as $key => $one_filter_elem) {
                    $newFiltersParams[$key] = $one_filter_elem;
                    if(strpos($one_filter_elem, '[') !== false || strpos($one_filter_elem, ']') !== false) {
                        $newFiltersParams[$key] = explode(',', substr($filterParams[$key], 1, -1));
                    }
                }
            }

            $filterParams = $newFiltersParams;
        }

        $pushUrl = '';

        if (!empty($filterParams)) {
            foreach ($filterParams as $key => $one_filter_el) {

                if (is_array($one_filter_el)) {
                    $pushUrlArr = '';
                    foreach ($one_filter_el as $k => $filter_el) {
                        $pushUrlArr .= $filter_el . ',';
                    }

                    $pushUrl .= $key . '=[' . strip_tags(substr($pushUrlArr, 0, -1)) . ']&';
                } else {
                    $pushUrl .= $key . '=' . strip_tags(str_replace('[]', '', $one_filter_el)) . '&';
                }
            }

            $pushUrl = '?' . substr($pushUrl, 0, -1);
        }

        $items = Subscribers::where(function ($q) use ($filterParams, $request) {

            if (array_key_exists('email', $filterParams) && !is_array($filterParams['email']))
                $q->where('email', 'LIKE', "%{$filterParams['email']}%");

            if (array_key_exists('start_date', $filterParams) && !array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']))
                $q->whereDate('created_at', '>=', date('Y-m-d', strtotime($filterParams['start_date'])));
            elseif (!array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['end_date']))
                $q->whereDate('created_at', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));
            elseif (array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']) && !is_array($filterParams['end_date']))
                $q->whereDate('created_at', '>=', date('Y-m-d', strtotime($filterParams['start_date'])))->whereDate('created_at', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));
        })
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);

        $items->setPath(url(LANG, ['admin', parent::currComponent()->slug]) . $pushUrl);

        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
                abort(503);
            }

            return response()->json($response);
        }

        return $response;

    }

    public function destroy(Request $request)
    {

        $deletedItemsId = substr($request->get('id'), 1, -1);

        if (empty($deletedItemsId))
            return response()->json([
                'status' => false
            ]);

        if ($request->get('event') != 'from-trash')
            return response()->json([
                'status' => false
            ]);

        $deletedItemsIds = explode(',', $deletedItemsId);

        $items = Subscribers::whereIn('id', $deletedItemsIds)->get();

        $cartMessage = $responseMsg = '';

        if (!$items->isEmpty()) {
            foreach ($items as $item) {

                $cartMessage .= $item->name . ', ';

                $item->delete();
                $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' deleted' : '';
                helpers()->logActions(parent::currComponent(), $item, $item->name, 'deleted');

            }

            return response()->json([
                'status' => true,
                'cart_messages' => $responseMsg,
                'items' => $deletedItemsIds
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        $items = Subscribers::whereIn('id', $ItemsId)->get();

        if (!$items->isEmpty()) {
            foreach ($items as $item) {
                $item->delete();
                helpers()->logActions(parent::currComponent(), $item, $item->name, 'deleted');
            }
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function exportList(Request $request)
    {
        if ($request->method() != 'POST' && !$request->ajax())
            abort(404);

        $exportsDir = storage_path('app/exports');
        $fileName = 'subscribers_' . time() . '.csv';
        $filePath = $exportsDir . '/' . $fileName;

        if (!file_exists($exportsDir))
            File::makeDirectory($exportsDir, 0775, true, true);

        $filterTableList = filterTableList($request, $request->except('page'), true);
        $filterParams = $filterTableList->filterParams;

        $handler = fopen($filePath, 'w+');
        $fileSize = filesize($filePath);
        $headerArray = ['email'];

        if ($fileSize == 0)
            fputcsv($handler, $headerArray);
        Subscribers::where(function ($q) use ($filterParams, $request) {
            if (array_key_exists('email', $filterParams) && !is_array($filterParams['email']))
                $q->where('email', 'LIKE', "%{$filterParams['email']}%");

            if (array_key_exists('start_date', $filterParams) && !array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']))
                $q->whereDate('created_at', '>=', date('Y-m-d', strtotime($filterParams['start_date'])));
            elseif (!array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['end_date']))
                $q->whereDate('created_at', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));
            elseif (array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']) && !is_array($filterParams['end_date']))
                $q->whereDate('created_at', '>=', date('Y-m-d', strtotime($filterParams['start_date'])))->whereDate('created_at', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));
        })->chunk(200, function ($subscribers) use ($handler) {
            foreach ($subscribers as $subscriber) {
                if ($subscriber->email) {
                    fputcsv($handler, [$subscriber->email]);
                }
            }
        });
        fclose($handler);

        return response()->json([
            'status' => true,
            'redirect' => adminUrl([parent::currComponent()->slug, 'downloadSubscribers']) . '?file=' . $fileName,
            'msg' => [
                'e' => 'Data is being exported to csv file.',
                'type' => 'info',
            ],

        ]);
    }
    public function downloadSubscribers(Request $request)
    {
        $fileName = $request->get('file', null);

        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];

        $exportsDir = storage_path('app/exports');
        $file = "{$exportsDir}/{$fileName}";
        $newFileName = "subscribers_" . date('d-m-Y') . ".csv";


        if (!$fileName || !file_exists($file))
            abort(404);

        $authUser = auth()->guard('admin')->user();
        $authUser->update([
            'has_exported_products' => 0
        ]);

        return response()->download($file, $newFileName, $headers)->deleteFileAfterSend(true);
    }
    public function widgetsCountItems()
    {
        return Subscribers::count();
    }

}