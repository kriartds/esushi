<?php

namespace Modules\Subscribers\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;

class SubscribersEmails extends Model
{
    use FilesRelation;

    protected $table = 'subscribers_emails';

    protected $fillable = [
        'name',
        'description',
        'for_news_subscribers',
        'for_new_products_subscribers'
    ];

}
