<?php

namespace Modules\Subscribers\Models;

use Illuminate\Database\Eloquent\Model;

class Subscribers extends Model
{

    protected $table = 'subscribers';

    protected $fillable = [
        'email',
        'ip',
        'unsubscribe_token',
        'news',
        'new_products',
    ];

}
