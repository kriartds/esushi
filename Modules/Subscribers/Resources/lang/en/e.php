<?php

return [

    'edit_table' => 'Edit',
    'delete_table' => 'Delete',
    'id_table' => 'ID',
    'list_is_empty' => 'List is empty!',
    'save_it' => 'Save',

    'from_date' => 'From date',
    'to_date' => 'To date',
    'filter' => 'Filter',
    'reset' => 'Reset',
    'email' => 'Email',
    'date' => 'Date',
    'export_csv_file' => 'Export CSV file',
    'template_name' => 'Template name',
    'email_content' => 'Email Content',
    'send' => 'Send',
    'ip' => 'Client IP',
    'item_cant_be_delete' => "Item can't be delete!",
    'for_news_subscribers' => "For news subscribers",
    'for_new_products_subscribers' => "For new products subscribers",

];