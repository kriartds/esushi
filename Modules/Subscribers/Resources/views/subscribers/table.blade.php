@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th>{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.email")}}</th>
            <th class="left">Subscribed to</th>
            <th>{{__("{$moduleName}::e.ip")}}</th>
            <th>{{__("{$moduleName}::e.date")}}</th>
            <th></th>
            <th class="checkbox-all align-center">
                <div>Check all</div>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>
                <td class="left">{{$item->email ?: ''}}</td>
                <td class="left">
                @if($item->news)
                    News
                @elseif($item->new_products)
                    Stock
                @endif
                </td>
                <td>{{$item->ip ?: ''}}</td>
                <td>
                    {{!is_null($item->created_at) ? date('d-m-Y', strtotime($item->created_at)) : '-'}}
                </td>
                <td class="empty-td"></td>
                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}">
                        <label for="{{$item->id}}">
                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
