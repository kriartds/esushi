@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list'],'actionButton' =>true,  'item' => null, 'filter' => ['path' => $path, 'filterParams' => $filterParams, 'items' => $items]])

        <div class="table-block">
            <div class="export-data">
                <a href="{{url(LANG, ['admin', $currComponent->slug, 'exportSubscribers'])}}" title="{{__("{$moduleName}::e.export_csv_file")}}"></a>
            </div>
            @include("{$path}.table", ['items' => $items, 'componentSlug' => $currComponent->slug])
        </div>

    </div>

@stop

@include('admin.footer')