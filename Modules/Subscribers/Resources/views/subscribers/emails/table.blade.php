@if(!$items->isEmpty())
    <table class="table">
        <thead>
        <tr>
            <th>{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.template_name")}}</th>
            <th>{{__("{$moduleName}::e.for_news_subscribers")}}</th>
            <th>{{__("{$moduleName}::e.for_new_products_subscribers")}}</th>
            <th>{{__("{$moduleName}::e.date")}}</th>
            <th>{{__("{$moduleName}::e.send")}}</th>
            @if($permissions->delete)
                <th class="checkbox-all" >
                    <div>Select All</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>
                <td class="left medium categories">
                    <a href="{{adminUrl([$currComponent->slug, 'edit', $item->id])}}">{{@$item->name}}</a>
                </td>
                <td>{{@$item->for_news_subscribers ? 'Yes' : 'No'}}</td>
                <td>{{@$item->for_new_products_subscribers ? 'Yes' : 'No'}}</td>
                <td class="medium">{{$item->created_at}}</td>
                <td class="send-email-to-subscribers">
                <button class="btn getModal"  data-modal="#modal-email" >{{__("{$moduleName}::e.send")}}</button>
                    <form method="POST"
                          action="{{url(LANG, ['admin', $currComponent->slug, 'sendEmailTemplate', @$item->id ])}}"
                          id="{{"sendEmailTemplate-form".$item->id}}">
                    </form>
                </td>

                @if($permissions->delete)
                    <td class="checkbox-items">
                        @if($item->shipping_zone_default == 0)
                            <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                   name="checkbox_items[{{$item->id}}]"
                                   value="{{$item->id}}">
                            <label for="{{$item->id}}">
                            </label>
                        @else
                            {{__("{$moduleName}::e.item_cant_be_delete")}}
                        @endif
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
<div class="arcticmodal-container" style="display: none">
    <table class="arcticmodal-container_i">
        <tbody>
        <tr>
            <td class="arcticmodal-container_i2">
                <div class="modal bigModal" id="modal-email">
                    <div class="modal-close arcticmodal-close">

                    </div>
                    <div class="containerModal">
                        <h4>Are you sure you want to send this email?</h4>
                        <div class="text">
                            <p> The e-mail will be sent to all the subscribers <span class="boldText">NEWS</span> or
                                <span class="boldText">STOCK</span>.</p>
                        </div>
                        <div class="warningMessage">
                            <p>The action is irreversible!</p>
                        </div>

                        <button class="btn medium boldUpp submit-form-btn arcticmodal-close"
                                data-form-id={{"sendEmailTemplate-form".$item->id}}>Send
                        </button>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>