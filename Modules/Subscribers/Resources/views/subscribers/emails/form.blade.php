<form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form"
      enctype="multipart/form-data">
    <div class="background mb">
        <div class="field-row smallInput">
            <div class="label-wrap">
                <label for="name">{{__("{$moduleName}::e.template_name")}}</label>
            </div>
            <div class="field-wrap">
                <input name="name" id="name" value="{{@$item->name}}">
            </div>
        </div>
        <div class="field-row">
            <div class="label-wrap">
                <label for="description">{{__("{$moduleName}::e.email_content")}}</label>
            </div>
            <div class="field-wrap">
            <textarea name="description" id="description" data-type="ckeditor">

                @if(!is_null(@$item->description))
                    {!! @$item->description !!}
                @else
                    <table style="border: 1px solid #AFB5B9;border-collapse: collapse;font-family: 'Raleway','Arial Black', sans-serif;width: 600px;">
                        <thead>
                        <tr style="background-color: #E1E6E9">
                            <th colspan="4" style="padding: 10px; text-align: center">
                                <a href="{{url(LANG)}}" target="_blank">
                                    <img src="{{asset('assets/img/logo.png')}}" alt="{{env('APP_NAME')}}"
                                         title="{{env('APP_NAME')}}">
                                </a>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="4">
                                <h1 style="color: #000000;text-align: center;line-height: 70px;font-size: 30px;">{{env('APP_NAME')}} Email Template Heading</h1>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 200px;"><img style="height: 200px;width: 200px;object-fit: contain"
                                                           src="{{asset('admin-assets/img/no-image.png')}}"
                                                           alt="product image"></td>
                            <td style="padding:5px;width: 150px;"><span
                                        style="display: block;padding: 15px; text-transform: capitalize;font-size: 20px;">{{env('APP_NAME')}} Product Name 1</span></td>
                            <td style="width: 100px;text-align: center">
                                <span style="display:inline-block;padding:5px;text-decoration: line-through">90$</span><span
                                        style="display:inline-block;padding:5px;">30$</span>
                            </td>
                            <td style="width: 150px;"><a href="#"
                                                         style="display: block;text-align:right;padding-right: 15px; color: #0f74a8;font-size: 15px;text-decoration: underline">View more</a></td>
                        </tr>
                        <tr>
                            <td style="width: 200px;"><img style="height: 200px;width: 200px;object-fit: contain"
                                                           src="{{asset('admin-assets/img/no-image.png')}}"
                                                           alt="product image"></td>
                            <td style="padding:5px;width: 150px;"><span
                                        style="display: block;padding: 15px; text-transform: capitalize;font-size: 20px;">{{env('APP_NAME')}} Product Name 1</span></td>
                            <td style="width: 100px;text-align: center">
                                <span style="display:inline-block;padding:5px;text-decoration: line-through">90$</span><span
                                        style="display:inline-block;padding:5px;">30$</span>
                            </td>
                            <td style="width: 150px;"><a href="#"
                                                         style="display: block;text-align:right;padding-right: 15px; color: #0f74a8;font-size: 15px;text-decoration: underline">View more</a></td>
                        </tr>
                        <tr>
                            <td style="width: 200px;"><img style="height: 200px;width: 200px;object-fit: contain"
                                                           src="{{asset('admin-assets/img/no-image.png')}}"
                                                           alt="product image"></td>
                            <td style="padding:5px;width: 150px;"><span
                                        style="display: block;padding: 15px; text-transform: capitalize;font-size: 20px;">{{env('APP_NAME')}} Product Name 1</span></td>
                            <td style="width: 100px;text-align: center">
                                <span style="display:inline-block;padding:5px;text-decoration: line-through">90$</span><span
                                        style="display:inline-block;padding:5px;">30$</span>
                            </td>
                            <td style="width: 150px;"><a href="#"
                                                         style="display: block;text-align:right;padding-right: 15px; color: #0f74a8;font-size: 15px;text-decoration: underline">View more</a></td>
                        </tr>
                        </tbody>
                        <tfoot style="background-color: #E1E6E9;">
                        <tr>
                            <td colspan="4">
                                <a style="padding:5px;margin-bottom:2px;text-transform: uppercase; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none"
                                   href="tel:{{clearPhone(@helpers()->getSettingsField('md_phone', $allSettings))}}"
                                   target="_blank"><span>{{helpers()->getSettingsField('md_phone', $allSettings)}}</span></a>
                                <a style="padding:5px;margin-bottom:2px;text-transform: uppercase; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none"
                                   href="mailto:{{helpers()->getSettingsField('email', $allSettings)}}" target="_blank"><span>{{helpers()->getSettingsField('email', $allSettings)}}</span></a>
                                <span style="padding:5px;margin-bottom:2px;text-transform: uppercase; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none">{{helpers()->getSettingsField('moldova_address', $allSettings, true)}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="padding:5px;">
                                <ul class="footer-socials-links" style="list-style-type: none;padding: 0">
                                    <li style="float: left;margin-right: 10px;" class="facebook"><a
                                                href="{{helpers()->getSettingsField('facebook_link', $allSettings)}}"
                                                target="_blank" style="margin-right: 10px;display: inline-block"><img
                                                    src="{{asset('assets/img/svg-icons/social_facebook.png')}}"
                                                    alt="facebook"></a></li>
                                    <li style="float: left;margin-right: 10px;" class="instagram"><a
                                                href="{{helpers()->getSettingsField('instagram_link', $allSettings)}}"
                                                target="_blank" style="margin-right: 10px;display: inline-block"><img
                                                    src="{{asset('assets/img/svg-icons/social_instagram.png')}}"
                                                    alt="instagram"></a></li>
                                    <li style="float: left;margin-right: 10px;" class="twitter"><a
                                                href="{{helpers()->getSettingsField('twitter_link', $allSettings)}}"
                                                target="_blank" style="margin-right: 10px;display: inline-block"><img
                                                    src="{{asset('assets/img/svg-icons/social_twitter.png')}}"
                                                    alt="twitter"></a></li>
                                    <li style="float: left;margin-right: 10px;" class="telegram"><a
                                                href="{{helpers()->getSettingsField('telegram_link', $allSettings)}}"
                                                target="_blank" style="margin-right: 10px;display: inline-block"><img
                                                    src="{{asset('assets/img/svg-icons/social_telegram.png')}}"
                                                    alt="telegram"></a></li>
                                </ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                @endif
            </textarea>
            </div>
        </div>

        <div class="flexEmailSubscribers">

            <div class="field-row switcher-container">
                <div class="label-wrap">
                    <label for="for_news_subscribers">{{__("{$moduleName}::e.for_news_subscribers")}}</label>
                </div>
                <div class="checkbox-switcher">
                    <input type="checkbox" name="for_news_subscribers"
                           id="for_news_subscribers" {{@$item->for_news_subscribers ? 'checked' : ''}}>
                    <label for="for_news_subscribers"></label>
                </div>
            </div>
            <div class="field-row switcher-container">
                <div class="label-wrap">
                    <label for="for_new_products_subscribers">{{__("{$moduleName}::e.for_new_products_subscribers")}}</label>
                </div>
                <div class="checkbox-switcher">
                    <input type="checkbox" name="for_new_products_subscribers"
                           id="for_new_products_subscribers" {{@$item->for_new_products_subscribers ? 'checked' : ''}}>
                    <label for="for_new_products_subscribers"></label>
                </div>
            </div>

        </div>
    </div>
    <div class="m-left">
        <button class="btn medium submit-form-btn"
                data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
    </div>
</form>