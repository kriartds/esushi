<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsItemsAttributesOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_items_attributes_options', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('products_items_attribute_id');
            $table->unsignedInteger('products_attribute_id');
            $table->timestamps();

            $table->foreign('products_items_attribute_id', 'products_items_attribute_id_foreign')->references('id')->on('products_items_attributes')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('products_attribute_id', 'products_attribute_id_foreign')->references('id')->on('products_attributes_id')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_items_attributes_options');
    }
}
