<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsItemsReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_items_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('products_item_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->text('message')->nullable();
            $table->tinyInteger('active')->default(0);
            $table->tinyInteger('seen')->default(0);
            $table->tinyInteger('rating')->default(1);
            $table->ipAddress('ip')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('products_item_id')->references('id')->on('products_items_id')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_items_reviews');
    }
}
