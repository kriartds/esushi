<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsCategoriesIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_categories_id', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('p_id')->nullable();
            $table->string('slug')->unique();
            $table->integer('position')->default(0);
            $table->smallInteger('level')->default(1);
            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('show_on_main')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_categories_id');
    }
}
