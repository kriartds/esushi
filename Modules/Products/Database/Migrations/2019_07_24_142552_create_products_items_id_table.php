<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsItemsIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_items_id', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->unsignedInteger('item_type_id')->nullable();
            $table->decimal('price', 10)->nullable()->index('price');
            $table->decimal('sale_price', 10)->nullable()->index('sale_price');
            $table->tinyInteger('use_stock')->default(0);
            $table->integer('stock_quantity')->nullable();
            $table->enum('status', ['in_stock', 'out_of_stock'])->default('in_stock')->nullable();
            $table->float('weight')->nullable();
            $table->float('length')->nullable();
            $table->float('width')->nullable();
            $table->float('height')->nullable();
            $table->string('sku')->nullable();
            $table->integer('position')->default(0);
            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('show_on_main')->default(0);
            $table->integer('popularity_sales')->nullable();
            $table->decimal('popularity_rating', 3)->nullable();
            $table->text('recommended_by_categories_ids')->nullable();
            $table->tinyInteger('incomplete_data')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('item_type_id')->references('id')->on('products_items_types')->onDelete('set null')->onUpdate('no action');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_items_id');
    }
}
