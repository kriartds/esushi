<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsItemsVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_items_variations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('products_items_variations_details_id');
            $table->unsignedInteger('products_attribute_parent_id');
            $table->unsignedInteger('products_attribute_child_id')->nullable();
            $table->timestamps();

            $table->foreign('products_items_variations_details_id', 'products_items_variations_details_id_foreign')->references('id')->on('products_items_variations_details_id')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('products_attribute_parent_id', 'products_attribute_parent_id_foreign')->references('id')->on('products_attributes_id')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('products_attribute_child_id', 'products_attribute_child_id_foreign')->references('id')->on('products_attributes_id')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_items_variations');
    }
}
