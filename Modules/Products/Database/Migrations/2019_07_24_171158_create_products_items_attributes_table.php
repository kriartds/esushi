<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsItemsAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_items_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('products_item_id');
            $table->unsignedInteger('products_parent_attribute_id');
            $table->tinyInteger('visible_on_product_page')->default(1);
            $table->tinyInteger('for_variation')->default(0);
            $table->timestamps();

            $table->foreign('products_item_id')->references('id')->on('products_items_id')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('products_parent_attribute_id')->references('id')->on('products_attributes_id')->onDelete('cascade')->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_items_attributes');
    }
}
