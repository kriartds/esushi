<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsItemsPromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_items_promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('products_item_id');
            $table->unsignedInteger('products_promotion_id');
            $table->timestamps();

            $table->foreign('products_item_id')->references('id')->on('products_items_id')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('products_promotion_id')->references('id')->on('products_promotions_id')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_items_promotions');
    }
}
