<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsAttributesIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_attributes_id', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('p_id')->nullable();
            $table->unsignedInteger('attribute_type_id')->nullable();
            $table->string('slug')->unique();
            $table->string('color')->nullable();
            $table->integer('position')->default(0);
            $table->tinyInteger('active')->default(1);
            $table->enum('field_type', ['checkbox', 'range', 'radio'])->default('checkbox')->nullable();
            $table->float('range_value')->nullable();
            $table->tinyInteger('display_in_filter')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('attribute_type_id')->references('id')->on('products_attributes_types')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_attributes_id');
    }
}
