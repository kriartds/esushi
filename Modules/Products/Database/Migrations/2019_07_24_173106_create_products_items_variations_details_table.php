<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsItemsVariationsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_items_variations_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('products_items_variations_detail_id');
            $table->unsignedInteger('lang_id')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();

            $table->foreign('products_items_variations_detail_id', 'products_items_variations_detail_id_foreign')->references('id')->on('products_items_variations_details_id')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('set null')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_items_variations_details');
    }
}
