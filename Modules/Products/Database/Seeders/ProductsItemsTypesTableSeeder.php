<?php

namespace Modules\Products\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Products\Models\ProductsItemsTypes;

class ProductsItemsTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductsItemsTypes::firstOrCreate([
            'slug' =>'simple'
        ]);

        ProductsItemsTypes::firstOrCreate([
            'slug' =>'variation'
        ]);
    }
}
