<?php

namespace Modules\Products\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Products\Models\ProductsAttributesTypes;

class ProductsAttributesTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            ['slug' => 'select'],
            ['slug' => 'color'],
            ['slug' => 'image'],
            ['slug' => 'radio'],
        ];

        foreach ($types as $type) {
            ProductsAttributesTypes::firstOrCreate($type);
        }
    }
}
