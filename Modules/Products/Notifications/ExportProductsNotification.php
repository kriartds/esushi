<?php

namespace Modules\Products\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ExportProductsNotification extends Notification
{
    use Queueable;

    private $details;
    private $globalSettings;

    /**
     * Create a new notification instance.
     *
     * @param $details
     * @param $globalSettings
     */
    public function __construct($details, $globalSettings)
    {
        $this->details = $details;
        $this->globalSettings = $globalSettings;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Export products from ' . config('app.name'))
            ->view('admin.emails.exportProducts', $this->details);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'title' => @$this->details['title'],
            'msg' => @$this->details['msg'],
            'linkText' => @$this->details['linkText'],
            'url' => @$this->details['url'],
            'exportedProductsFile' => @$this->details['exportedProductsPath']
        ];
    }
}
