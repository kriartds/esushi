<?php

namespace Modules\Products\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ImportProductsNotification extends Notification
{
    use Queueable;

    private $details;
    private $globalSettings;

    /**
     * Create a new notification instance.
     *
     * @param $details
     * @param $globalSettings
     */
    public function __construct($details, $globalSettings)
    {
        $this->details = $details;
        $this->globalSettings = $globalSettings;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (@$this->details['errorMsg'])
            $this->details['url'] = urldecode(customUrl(['admin', 'notifications', 'view', $this->id]));

        return (new MailMessage)
            ->subject('Import products from ' . config('app.name'))
            ->view('admin.emails.importProducts', $this->details);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {

        return [
            'title' => @$this->details['title'],
            'msg' => @$this->details['msg'],
            'linkText' => @$this->details['linkText'],
            'url' => @$this->details['errorMsg'] ? urldecode(customUrl(['admin', 'notifications', 'view', $this->id])) : @$this->details['url'],
            'errorMsg' => @$this->details['errorMsg']
        ];
    }
}
