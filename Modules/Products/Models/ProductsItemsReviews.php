<?php

namespace Modules\Products\Models;

use App\Models\FilesRelation;
use Modules\SiteUsers\Models\Users;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductsItemsReviews extends Model
{
    use SoftDeletes;

    protected $table = 'products_items_reviews';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'products_item_id',
        'user_id',
        'message',
        'active',
        'seen',
        'rating',
        'ip'
    ];

    protected $casts = [
        'message' => 'object'
    ];

    public function product()
    {
        return $this->hasOne(ProductsItemsId::class, 'id', 'products_item_id');
    }

    public function user()
    {
        return $this->hasOne(Users::class, 'id', 'user_id');
    }

    public function siblingsReviews()
    {
        return $this->hasMany(ProductsItemsReviews::class, 'products_item_id', 'products_item_id');
    }

    public function getReviewAddedDateAttribute()
    {
        return now()->subMinutes(now()->diffInMinutes($this->created_at))->diffForHumans();
    }

    public function getReviewRatingAttribute()
    {

        $response = collect([
            'rating' => $this->rating,
            'percentage' => $this->rating * 100 / 5
        ]);

        return $response;
    }
}
