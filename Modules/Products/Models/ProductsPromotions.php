<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsPromotions extends Model
{
    protected $table = 'products_promotions';

    protected $fillable = [
        'products_promotion_id',
        'lang_id',
        'name',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

    public function productsPromotionId()
    {
        return $this->hasOne('Modules\Products\Models\ProductsPromotionsId', 'id', 'products_promotion_id');
    }
}
