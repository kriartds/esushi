<?php

namespace Modules\Products\Models;

use App\Models\Languages;
use Illuminate\Database\Eloquent\Model;

class ProductsAttributes extends Model
{
    protected $table = 'products_attributes';

    protected $fillable = [
        'products_attribute_id',
        'lang_id',
        'name',
        'description'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

    public function lang()
    {
        return $this->hasOne(Languages::class, 'id', 'lang_id')->where('active', 1);
    }
}
