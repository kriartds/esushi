<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsToppings extends Model
{
    protected $table = 'products_toppings';

    protected $fillable = [
        'products_toppings_id',
        'lang_id',
        'name',
        'description',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }
}
