<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsItemsAttributesOptions extends Model
{
    protected $table = 'products_items_attributes_options';

    protected $fillable = [
        'products_items_attribute_id',
        'products_attribute_id'
    ];

    public function attribute()
    {
        return $this->hasOne('Modules\Products\Models\ProductsAttributesId', 'id', 'products_attribute_id')->whereNotNull('p_id');
    }

    public function productsAttr()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsAttributes', 'id', 'products_items_attribute_id');
    }

}
