<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsAttributesTypes extends Model
{

    protected $table = 'products_attributes_types';

    protected $fillable = [
        'slug'
    ];

    public function attributes()
    {
        return $this->hasMany('Modules\Products\Models\ProductsAttributesId', 'attribute_type_id', 'id');
    }

    public function getNameAttribute()
    {
        return @ucfirst($this->slug);
    }

}
