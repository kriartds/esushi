<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsItemsCategories extends Model
{
    protected $table = 'products_items_categories';

    protected $fillable = [
        'products_item_id',
        'products_category_id',
    ];

    public function category()
    {
        return $this->hasOne('Modules\Products\Models\ProductsCategoriesId', 'id', 'products_category_id');
    }

    public function product()
    {
        return $this->hasOne('Modules\Products\Models\ProductsItemsId', 'id', 'products_item_id');
    }

}
