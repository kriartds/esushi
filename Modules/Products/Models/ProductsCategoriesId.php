<?php

namespace Modules\Products\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductsCategoriesId extends Model
{
    use SoftDeletes;
    use FilesRelation;

    protected $table = 'products_categories_id';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'p_id',
        'slug',
        'position',
        'level',
        'active',
        'show_on_main'
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'products-categories';
        self::$globalLangId = request()->segment(6, null);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function parent()
    {
        return $this->belongsTo(ProductsCategoriesId::class, 'p_id', 'id');
    }

    public function parents()
    {
        return $this->hasMany('Modules\Products\Models\ProductsCategoriesId', 'id', 'p_id');
    }


    public function children()
    {
        return $this->hasMany('Modules\Products\Models\ProductsCategoriesId', 'p_id', 'id');
    }

    public function recursiveChildren()
    {
        return $this->children()->with(['recursiveChildren', 'recursiveChildren.globalName', 'globalName'])->orderBy('position');
    }

    public function recursiveParents()
    {
        return $this->parents()->with(['recursiveParents', 'recursiveParents.globalName', 'globalName'])->orderBy('position');
    }

    public function globalName()
    {
        return $this->hasOne(ProductsCategories::class, 'products_category_id', 'id')->whereIn('products_categories.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(products_categories.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('Modules\Products\Models\ProductsCategories', 'products_category_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('Modules\Products\Models\ProductsCategories', 'products_category_id', 'id');
    }

    public function products()
    {
        return $this->hasManyThrough('Modules\Products\Models\ProductsItemsId', 'Modules\Products\Models\ProductsItemsCategories', 'products_category_id', 'id', 'id', 'products_item_id');
    }

    public function productsItemsCategories()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsCategories', 'products_category_id', 'id');
    }

    public function getHasChildrenAttribute()
    {
        return $this->children->isNotEmpty();
    }

    public function getHasProductsAttribute()
    {
        return $this->products->isNotEmpty();
    }

    public function getActiveChildrenIdsAttribute()
    {
        return $this->children->where('active', 1)->get()->pluck('id')->toArray();
    }

}
