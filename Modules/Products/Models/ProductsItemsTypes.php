<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsItemsTypes extends Model
{

    protected $table = 'products_items_types';

    protected $fillable = [
        'slug'
    ];

    public function products()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsId', 'attribute_type_id', 'item_type_id');
    }

    public function getNameAttribute()
    {
        return @ucfirst($this->slug);
    }

}
