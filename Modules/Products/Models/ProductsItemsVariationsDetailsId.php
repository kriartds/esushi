<?php

namespace Modules\Products\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;

class ProductsItemsVariationsDetailsId extends Model
{

    use FilesRelation;

    public static $isVariation;

    protected $table = 'products_items_variations_details_id';

    protected $fillable = [
        'products_item_id',
        'use_stock',
        'stock_quantity',
        'price',
        'sale_price',
        'discount_percentage',
        'status',
        'weight',
        'length',
        'width',
        'height',
        'sku',
        'is_default',
        'iiko_id',
    ];

    protected $hidden = ['finStock', 'variationsChildrenIds'];


    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'products';
        self::$globalLangId = request()->segment(6, request()->get('lang_id', null));
        self::$isVariation = true;
    }

    public function globalName()
    {
        return $this->hasOne('Modules\Products\Models\ProductsItemsVariationsDetails', 'products_items_variations_detail_id', 'id')->where('lang_id', LANG_ID);
    }

    public function itemByLang()
    {
        return $this->hasOne('Modules\Products\Models\ProductsItemsVariationsDetails', 'products_items_variations_detail_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsVariationsDetails', 'products_items_variations_detail_id', 'id');
    }

    public function product()
    {
        return $this->hasOne('Modules\Products\Models\ProductsItemsId', 'id', 'products_item_id');
    }

    public function variations()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsVariations', 'products_items_variations_details_id', 'id');
    }

    public function variation()
    {
        return $this->hasOne('Modules\Products\Models\ProductsItemsVariations', 'products_items_variations_details_id', 'id');
    }

    public function cartVariations()
    {
        return $this->hasMany('Modules\Orders\Models\CartProducts', 'products_variation_item_id', 'id');
    }

    public function getVariationsChildrenIdsAttribute()
    {

        $response = [];

        $variations = $this->variations->groupBy('products_attribute_parent_id');

        if ($variations->isNotEmpty())
            foreach ($variations as $p_id => $variation) {
                if ($variation->isNotEmpty())
                    foreach ($variation as $item) {
                        $response[$p_id] = $item->products_attribute_child_id;
                    }
            }

        return $response;
    }


    public function getFinStockAttribute()
    {

        $response = false;

        if (!@$this->use_stock && @$this->status == 'in_stock')
            $response = true;
        elseif (@$this->use_stock && @$this->stock_quantity)
            $response = $this->stock_quantity;

        return $response;
    }
}
