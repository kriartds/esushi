<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsItems extends Model
{
    protected $table = 'products_items';

    protected $fillable = [
        'products_item_id',
        'lang_id',
        'name',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'weight',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

    public function productsItemId()
    {
        return $this->hasOne('Modules\Products\Models\ProductsItemsId', 'id', 'products_item_id');
    }
}
