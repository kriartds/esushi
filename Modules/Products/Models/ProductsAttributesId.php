<?php

namespace Modules\Products\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductsAttributesId extends Model
{
    use SoftDeletes;
    use FilesRelation;

    protected $table = 'products_attributes_id';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'p_id',
        'attribute_type_id',
        'slug',
        'color',
        'position',
        'active',
        'field_type',
        'range_value',
        'display_in_filter'
    ];

//    protected $hidden = [
//        'children',
//        'globalName',
//        'type'
//    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'attributes';
        self::$globalLangId = request()->segment(6, null);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function children()
    {
        return $this->hasMany('Modules\Products\Models\ProductsAttributesId', 'p_id', 'id')->orderBy('position');
    }

    public function parent()
    {
        return $this->hasOne('Modules\Products\Models\ProductsAttributesId', 'id', 'p_id');
    }

    public function globalName()
    {
        return $this->hasOne(ProductsAttributes::class, 'products_attribute_id', 'id')->whereIn('products_attributes.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(products_attributes.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('Modules\Products\Models\ProductsAttributes', 'products_attribute_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('Modules\Products\Models\ProductsAttributes', 'products_attribute_id', 'id');
    }

    public function type()
    {
        return $this->hasOne('Modules\Products\Models\ProductsAttributesTypes', 'id', 'attribute_type_id');
    }

    public function products()
    {
        return $this->hasManyThrough('Modules\Products\Models\ProductsItemsId', 'Modules\Products\Models\ProductsItemsAttributes', 'products_parent_attribute_id', 'id', 'id', 'products_item_id');
    }

    public function options()
    {
        return $this->hasManyThrough('Modules\Products\Models\ProductsItemsAttributesOptions', 'Modules\Products\Models\ProductsItemsAttributes', 'id', 'products_items_attribute_id', 'products_attribute_id', 'products_parent_attribute_id');
    }

    public function attrOptions()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsAttributesOptions', 'products_attribute_id', 'id');
    }

    public function productsAttr()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsAttributes', 'products_parent_attribute_id', 'id');
    }

    public function getHasChildrenAttribute()
    {
        return !$this->children->isEmpty();
    }

    public function getHasProductsAttribute()
    {
        return $this->attrOptions->isNotEmpty();
    }

    public function getColorsAttribute()
    {
        return array_filter(explode(',', $this->color));
    }

    public function getMinChildValueAttribute()
    {

        $response = 0;

        if ($this->children->isEmpty())
            return $response;

        $value = $this->children->min('range_value');
        $response = !is_null(@floor($value)) ? floor($value) : 0;

        return $response;
    }

    public function getMaxChildValueAttribute()
    {
        $response = 0;

        if ($this->children->isEmpty())
            return $response;

        $value = $this->children->max('range_value');
        $response = !is_null(@ceil($value)) ? ceil($value) : 1;

        return $response;
    }

}
