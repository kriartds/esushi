<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsItemsPromotions extends Model
{
    protected $table = 'products_items_promotions';

    protected $fillable = [
        'products_item_id',
        'products_promotion_id',
    ];

    public function promotion()
    {
        return $this->hasOne('Modules\Products\Models\ProductsPromotionsId', 'id', 'products_promotion_id');
    }

    public function product()
    {
        return $this->hasOne('Modules\Products\Models\ProductsItemsId', 'id', 'products_item_id');
    }




}
