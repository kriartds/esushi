<?php

namespace Modules\Products\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class ProductsItemsId extends Model
{
    use SoftDeletes;
    use FilesRelation;

    protected $table = 'products_items_id';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'slug',
        'item_type_id',
        'price',
        'sale_price',
        'use_stock',
        'stock_quantity',
        'status',
        'sku',
        'position',
        'active',
        'show_on_main',
        'popularity_sales',
        'popularity_rating',
        'recommended_by_categories_ids',
        'incomplete_data',
        'badges',
        'toppings_ids',
        'video'
    ];

    protected $casts = [
        'recommended_by_categories_ids' => 'array',
        'badges' => 'array',
        'toppings_ids' => 'array',
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'products';
        self::$globalLangId = request()->segment(6, null);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value);
    }

    public function scopeInStockProducts($q)
    {
        return $q->where(function ($q) {
            $q->where(function ($q) {
                $q->where('item_type_id', 1);
                $q->where(function ($q) {
                    $q->where(function ($q) {
                        $q->where('use_stock', 0);
                        $q->where('status', 'in_stock');
                    });
                    $q->orWhere(function ($q) {
                        $q->where('use_stock', 1);
                        $q->where('stock_quantity', '>', 0);
                    });
                });
            });
            $q->orWhere(function ($q) {
                $q->where('item_type_id', 2);
                $q->where(function ($q) {
                    $q->where(function ($q) {
                        $q->where('use_stock', 0);
                        $q->whereHas('variationsDetails', function ($q) {
                            $q->where(function ($q) {
                                $q->where('use_stock', 0);
                                $q->where('status', 'in_stock');
                            });
                            $q->orWhere(function ($q) {
                                $q->where('use_stock', 1);
                                $q->where('stock_quantity', '>', 0);
                            });
                        });
                    });
                    $q->orWhere(function ($q) {
                        $q->where('use_stock', 1);
                        $q->where('stock_quantity', '>', 0);
                    });
                });

            });
        });
    }

    public function scopeOutOfStockProducts($q)
    {
        return $q->where(function ($q) {
            $q->where(function ($q) {
                $q->where('item_type_id', 1);
                $q->where(function ($q) {
                    $q->where(function ($q) {
                        $q->where('use_stock', 0);
                        $q->where('status', 'out_of_stock');
                    });
                    $q->orWhere(function ($q) {
                        $q->where('use_stock', 1);
                        $q->where('stock_quantity', 0);
                    });
                });
            });
            $q->orWhere(function ($q) {
                $q->where('item_type_id', 2);
                $q->where(function ($q) {
                    $q->where(function ($q) {
                        $q->where('use_stock', 0);
                        $q->whereHas('variationsDetails', function ($q) {
                            $q->where(function ($q) {
                                $q->where('use_stock', 0);
                                $q->where('status', 'out_of_stock');
                            });
                            $q->orWhere(function ($q) {
                                $q->where('use_stock', 1);
                                $q->where('stock_quantity', 0);
                            });
                        });
                    });
                    $q->orWhere(function ($q) {
                        $q->where('use_stock', 1);
                        $q->where('stock_quantity', 0);
                    });
                });

            });
        });
    }

    public function globalName()
    {
        return $this->hasOne(ProductsItems::class, 'products_item_id', 'id')->whereIn('products_items.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(products_items.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('Modules\Products\Models\ProductsItems', 'products_item_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItems', 'products_item_id', 'id');
    }

    public function categories()
    {
        return $this->hasManyThrough('Modules\Products\Models\ProductsCategoriesId', 'Modules\Products\Models\ProductsItemsCategories', 'products_item_id', 'id', 'id', 'products_category_id');
    }

    public function promotions()
    {
        return $this->hasManyThrough('Modules\Products\Models\ProductsPromotionsId', 'Modules\Products\Models\ProductsItemsPromotions', 'products_item_id', 'id', 'id', 'products_promotion_id');
    }

    public function productAttributes()
    {
        return $this->hasManyThrough('Modules\Products\Models\ProductsAttributesId', 'Modules\Products\Models\ProductsItemsAttributes', 'products_item_id', 'id', 'id', 'products_parent_attribute_id');
    }

    public function productItemsOptions()
    {
        return $this->hasManyThrough('Modules\Products\Models\ProductsItemsAttributesOptions', 'Modules\Products\Models\ProductsItemsAttributes', 'products_item_id', 'products_items_attribute_id', 'id', 'id');
    }

    public function productItemsAttributes()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsAttributes', 'products_item_id', 'id');
    }

    public function productCategories()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsCategories', 'products_item_id', 'id');
    }

    public function productPromotions()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsPromotions', 'products_item_id', 'id');
    }

    public function type()
    {
        return $this->hasOne('Modules\Products\Models\ProductsItemsTypes', 'id', 'item_type_id');
    }

    public function variationsDetails()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsVariationsDetailsId', 'products_item_id', 'id');
    }

    public function cartProducts()
    {
        return $this->hasMany('Modules\Orders\Models\CartProducts', 'products_item_id', 'id');
    }

    public function cartProduct()
    {
        //get current cart id
        return $this->hasOne('Modules\Orders\Models\CartProducts', 'products_item_id', 'id')->where('cart_id', '=', @getCurrentCart()->id);
    }

    public function reviews()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsReviews', 'products_item_id', 'id');
    }

    public function getAttributesParentsIdsAttribute()
    {
        return $this->productAttributes->filter()->pluck('id')->toArray();
    }

    public function getCategoriesIdsAttribute()
    {
        return $this->categories->pluck('id')->toArray();
    }

    public function getPromotionsIdsAttribute()
    {
        return $this->promotions->pluck('id')->toArray();
    }

    public function getSpecificAttrAttribute($attributeId = null)
    {
        return @$this->productItemsAttributes->where('products_parent_attribute_id', $attributeId)->first();
    }

//    public function getSelectedAttributesAttribute()
//    {
//        $optionsIds = $newOptionsIds = [];
//
//        if ($this->variationsDetails->isNotEmpty())
//            foreach ($this->variationsDetails as $productItemsOption) {
//                $optionsIds = array_merge($optionsIds, $productItemsOption->variations()
//                    ->get(['products_attribute_parent_id', 'products_attribute_child_id'])
//                    ->toArray());
//            }
//
//
//        if (!empty($optionsIds))
//            foreach ($optionsIds as $optionsId) {
//                if (is_null($optionsId['products_attribute_child_id'])) {
//                    $productsItemsAttributes = ProductsItemsAttributes::where('products_item_id', $this->id)
//                        ->where('products_parent_attribute_id', $optionsId['products_attribute_parent_id'])
//                        ->where('for_variation', 1)
//                        ->first();
//
//                    if (!is_null($productsItemsAttributes))
//                        $newOptionsIds = array_merge($newOptionsIds, $productsItemsAttributes->options->pluck('products_attribute_id')->toArray());
//                } else {
//                    $newOptionsIds[] = $optionsId['products_attribute_child_id'];
//                }
//            }
//
//        $attributes = ProductsAttributesId::where('active', 1)
//            ->whereNotNull('p_id')
//            ->whereIn('id', $newOptionsIds)
//            ->orderBy('position')
//            ->get()
//            ->groupBy('p_id');
//
//        return $attributes;
//    }

    public function getCurrentVariationAttribute($attributesIds)
    {

        if (!is_array($attributesIds))
            $attributesIds = [];

        $requiredAttrCount = $this->productItemsAttributes->where('for_variation', 1)->count();

        if (count($attributesIds) == $requiredAttrCount) {

            $variationsDetails = $this->variationsDetails()
                ->whereHas('variations', function ($q) use ($attributesIds) {
                    $q->whereIn('products_attribute_child_id', $attributesIds);
                    $q->orWhereNull('products_attribute_child_id');
                    $q->havingRaw('count(*) = ' . count($attributesIds));
                })
                ->with(['globalName'])
                ->first();

            return $variationsDetails;
        }

        return null;
    }

    public function getFinPriceAttribute()
    {

        $response = [
            'minPrice' => @$this->sale_price ? (float)$this->sale_price : null,
            'maxPrice' => @$this->price ? (float)$this->price : null
        ];

        if (@$this->isVariationProduct) {

            $minPrice = $maxPrice = null;
            $variations = $this->variationsDetails;

            if ($variations->isNotEmpty()) {

                //set prices for default variation if it exist
                foreach ($variations as $variation){
                    if($variation->is_default === 1){
                        $minPrice = $variation->sale_price ? floatval($variation->sale_price) : null;
                        $maxPrice = $variation->price ? floatval($variation->price) : null;
                    }
                }

                if(is_null($minPrice) && is_null($maxPrice))
                {
                    $variationPrice = collect([
                        'minVariationPrice' => $variations->min('price'),
                        'maxVariationPrice' => $variations->max('price'),
                        'minVariationSalePrice' => $variations->min('sale_price'),
                        'maxVariationSalePrice' => $variations->max('sale_price')
                    ]);

                    $minPrice = $variationPrice->min();
                    $maxPrice = $variationPrice->max();
                }

            }

            $response = [
                'minPrice' => @$minPrice ? (float)$minPrice : null,
                'maxPrice' => @$maxPrice ? (float)$maxPrice : null
            ];
        }

        return (object)$response;
    }


    public function getFinStockAttribute()
    {
        $response = false;

        if (!@$this->use_stock && @$this->status == 'in_stock')
            $response = true;
        elseif (@$this->use_stock && @$this->stock_quantity)
            $response = $this->stock_quantity;

        return $response;
    }

    public function getGlobalStockAttribute()
    {
        $response = false;

        if (!@$this->use_stock && @$this->status == 'in_stock')
            $response = true;
        elseif (@$this->use_stock && @$this->stock_quantity)
            $response = $this->stock_quantity;

        if (@$this->isVariationProduct) {
            $variations = $this->variationsDetails;
            if ($variations->isNotEmpty()) {
                foreach ($variations as $variation) {
                    $response = $variation->finStock;

                    if ($response)
                        break;
                }
            }
        }
        return $response;
    }

    public function getProductReviewsAttribute()
    {

        $response = collect([
            'count' => 0,
            'avg' => 0,
            'percentage' => 0
        ]);

        $reviews = $this->reviews;
        if ($reviews->isNotEmpty()) {
            $count = $reviews->count();
            $sumRating = $reviews->sum('rating');
            $ratingAvg = $sumRating / $count;

            $response = collect([
                'count' => $count,
                'avg' => round($ratingAvg, 2),
                'percentage' => $ratingAvg * 100 / 5
            ]);
        }

        return $response;
    }

    public function getProductPromotionAttribute()
    {

        $response = null;

        if (!is_null($this->sale_price) && $this->sale_price > 0) {
            $price = (float)$this->price;
            $salePrice = (float)$this->sale_price;

            $response = collect([
                'price' => $price,
                'salePrice' => $salePrice,
                'percentage' => round(($price - $salePrice) * 100 / $price)
            ]);
        }

        if (@$this->isVariationProduct) {

            $saleVariation = $this->variationsDetails
                ->where('sale_price', '!=', null)
                ->where('sale_price', '>', 0)
                ->first();

            if (!is_null($saleVariation) && @$saleVariation->price && @$saleVariation->sale_price) {
                $price = (float)$saleVariation->price;
                $salePrice = (float)$saleVariation->sale_price;

                $response = collect([
                    'price' => $price,
                    'salePrice' => $salePrice,
                    'percentage' => round(($price - $salePrice) * 100 / $price),
                    'variationId' => $saleVariation->id
                ]);
            }
        }

        return $response;
    }

    public function getIsVariationProductAttribute()
    {
        return $this->item_type_id == 2; // variation
    }

    public function getIsNewProductAttribute()
    {
        return carbon($this->created_at)->addWeeks(config('cms.product.isNew', 1))->gte(now());
    }
}
