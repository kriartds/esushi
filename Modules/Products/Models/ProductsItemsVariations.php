<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsItemsVariations extends Model
{

    protected $table = 'products_items_variations';

    protected $fillable = [
        'products_items_variations_details_id',
        'products_attribute_parent_id',
        'products_attribute_child_id',
    ];

    public function parentAttribute()
    {
        return $this->hasOne('Modules\Products\Models\ProductsAttributesId', 'id', 'products_attribute_parent_id')->whereNull('p_id');
    }

    public function childAttribute()
    {
        return $this->hasOne('Modules\Products\Models\ProductsAttributesId', 'id', 'products_attribute_child_id')->whereNotNull('p_id');
    }

    public function variationDetailId()
    {
        return $this->hasOne('Modules\Products\Models\ProductsItemsVariationsDetailsId', 'id', 'products_items_variations_details_id');
    }

}
