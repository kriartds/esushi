<?php

namespace Modules\Products\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductsPromotionsId extends Model
{
    use SoftDeletes;
    use FilesRelation;

    protected $table = 'products_promotions_id';

    protected $dates = [
        'deleted_at',
        'start_date',
        'end_date',
    ];

    protected $fillable = [
        'slug',
        'position',
        'active',
        'show_on_main',
        'start_date',
        'end_date',
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'promotions-components';
        self::$globalLangId = request()->segment(6, null);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function scopeAvailablePromotions($q)
    {
        return $q->where('active', 1)
            ->whereDate('start_date', '<=', now()->format('Y-m-d'))
            ->where(function ($q) {
                $q->whereDate('end_date', '>=', now()->format('Y-m-d'));
                $q->orWhereNull('end_date');
            });
    }

    public function globalName()
    {
        return $this->hasOne(ProductsPromotions::class, 'products_promotion_id', 'id')->whereIn('products_promotions.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(products_promotions.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function itemByLang()
    {
        return $this->hasOne('Modules\Products\Models\ProductsPromotions', 'products_promotion_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function items()
    {
        return $this->hasMany('Modules\Products\Models\ProductsPromotions', 'products_promotion_id', 'id');
    }

    public function products()
    {
        return $this->hasManyThrough('Modules\Products\Models\ProductsItemsId', 'Modules\Products\Models\ProductsItemsPromotions', 'products_promotion_id', 'id', 'id', 'products_item_id');
    }

    public function productsItemsPromotions()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsPromotions', 'products_promotion_id', 'id');
    }

}
