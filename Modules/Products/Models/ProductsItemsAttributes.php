<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsItemsAttributes extends Model
{
    protected $table = 'products_items_attributes';

    protected $fillable = [
        'products_item_id',
        'products_parent_attribute_id',
        'visible_on_product_page',
        'for_variation'
    ];

    public function attribute()
    {
        return $this->hasOne('Modules\Products\Models\ProductsAttributesId', 'id', 'products_parent_attribute_id')->whereNull('p_id');
    }

    public function product()
    {
        return $this->hasOne('Modules\Products\Models\ProductsItemsId', 'id', 'products_item_id');
    }

    public function options()
    {
        return $this->hasMany('Modules\Products\Models\ProductsItemsAttributesOptions', 'products_items_attribute_id', 'id');
    }

}
