<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsCategories extends Model
{
    protected $table = 'products_categories';

    protected $fillable = [
        'products_category_id',
        'lang_id',
        'name',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

    public function productsCategoryId()
    {
        return $this->hasOne('Modules\Products\Models\ProductsCategoriesId', 'id', 'products_category_id');
    }
}
