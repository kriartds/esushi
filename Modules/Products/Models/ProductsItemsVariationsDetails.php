<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsItemsVariationsDetails extends Model
{
    protected $table = 'products_items_variations_details';

    protected $fillable = [
        'products_items_variations_detail_id',
        'lang_id',
        'description'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['description'] = !is_null($value) ? preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $value) : null;
    }

    public function productsVariationDetailId()
    {
        return $this->hasOne('Modules\Products\Models\ProductsItemsVariationsDetailsId', 'id', 'products_items_variations_detail_id');
    }
}
