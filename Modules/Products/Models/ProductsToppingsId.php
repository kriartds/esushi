<?php

namespace Modules\Products\Models;

use App\Models\FilesRelation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductsToppingsId extends Model
{
    use SoftDeletes;
    use FilesRelation;

    protected $table = 'products_toppings_id';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'p_id',
        'slug',
        'position',
        'level',
        'active',
        'show_on_main',
        'iiko_id',
        'iiko_code',
        'iiko_group_id',
        'modifier_type'
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        self::$component = 'toppings';
        self::$globalLangId = request()->segment(6, null);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = \Illuminate\Support\Str::slug($value);
    }

    public function itemByLang()
    {
        return $this->hasOne('Modules\Products\Models\ProductsToppings', 'products_toppings_id', 'id')->where('lang_id', self::$globalLangId);
    }

    public function globalName()
    {
        return $this->hasOne(ProductsToppings::class, 'products_toppings_id', 'id')->whereIn('products_toppings.lang_id', [LANG_ID, DEF_LANG_ID])->orderByRaw("FIELD(products_toppings.lang_id, '" . LANG_ID . "', '" . DEF_LANG_ID . "' ) ASC ");
    }

    public function items()
    {
        return $this->hasMany('Modules\Products\Models\ProductsToppings', 'products_toppings_id', 'id');
    }
}
