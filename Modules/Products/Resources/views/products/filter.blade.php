@if(isset($filterParams))

    <div class="right-col">
        <button type="button" class="button gray filter-btn">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12"><g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M1 1h14"/></g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M4 6h8"/></g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M6 11h4"/></g></g></svg>
            Filters
            <span class="count">{{isset($items) && ((!empty($filterParams) || empty($filterParams)) && $items->total() > 0) ? $items->total() : ""}}</span>
        </button>
    </div>
    
    <div class="filter form-block">
        <form method="post" action="{{url(LANG, ['admin', $currComponent->slug, 'filter'])}}" class="filter-form" id="filter-form">
            
            <div class="field-row-wrap background-wh padding">

                <div class="col middle">
                    <div class="with-bg">

                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="name">{{__("{$moduleName}::e.title_table")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input name="name" id="name"
                                    value="{{ !empty($filterParams) && array_key_exists('name', $filterParams) ? $filterParams['name'] : '' }}">
                            </div>
                        </div>

                        @if($categories->isNotEmpty())
                            <div class="field-row">
                                <div class="label-wrap">
                                    <label for="category">{{__("{$moduleName}::e.categories")}}</label>
                                </div>
                                <div class="field-wrap">
                                    <select autocomplete="off" name="category[]" id="category"
                                            class="select2" multiple>
                                        {!! getParentChildrenInSelect($categories, @$filterParams['category']) !!}
                                    </select>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>

                <div class="col middle">
                    <div class="with-bg ">
                            <div class="field-row">
                                <div class="label-wrap">
                                    <label for="visible_on_home">Visible on the main page</label>
                                </div>
                                <div class="field-wrap">
                                    <select autocomplete="off" name="visible_on_home" id="visible_on_home" class="select2 no-search">
                                        <option value="">-</option>
                                            <option value="visible" {{ !empty($filterParams) && array_key_exists('visible_on_home', $filterParams) && $filterParams['visible_on_home'] == 1 ? 'selected' : '' }}>Visible</option>
                                            <option value="hidden" {{ !empty($filterParams) && array_key_exists('visible_on_home', $filterParams) && $filterParams['visible_on_home'] == 0? 'selected' : '' }}>Hidden</option>
                                    </select>
                                </div>
                            </div>
                    </div>
                </div>
                
                <div class="col small">
                    <div class="with-bg">
                        <div class="field-row">
                            <div class="field-row">
                                <div class="label-wrap">
                                    <label for="stock">{{__("{$moduleName}::e.stock")}}</label>
                                </div>
                                <div class="field-wrap">
                                    <select autocomplete="off" name="stock" id="stock" class="select2 no-search">
                                        <option value="">-</option>
                                        <option value="in_stock" {{ !empty($filterParams) && array_key_exists('stock', $filterParams) && $filterParams['stock'] == 'in_stock' ? 'selected' : '' }}>{{__("{$moduleName}::e.in_stock")}}</option>
                                        <option value="out_of_stock" {{ !empty($filterParams) && array_key_exists('stock', $filterParams) && $filterParams['stock'] == 'out_of_stock' ? 'selected' : '' }}>{{__("{$moduleName}::e.out_of_stock")}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>



                        @if($promotions->isNotEmpty())
                            <div class="field-row">
                                <div class="label-wrap">
                                    <label for="promotions">{{__("{$moduleName}::e.promotions")}}</label>
                                </div>
                                <div class="field-wrap">
                                    <select autocomplete="off" name="promotions[]" id="promotions" class="select2" multiple>
                                        @foreach($promotions as $promotion)
                                            <option value="{{$promotion->id}}" {{ !empty($filterParams) && array_key_exists('promotions', $filterParams) && is_array($filterParams['promotions']) && in_array($promotion->id, $filterParams['promotions']) ? 'selected' : '' }}>{{@$promotion->globalName->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="col middle date">
                    <div class="with-bg flex">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="start_date">{{__("{$moduleName}::e.from_date")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="start_date" id="start_date" class="datetimepicker"
                                    value="{{ !empty($filterParams) && array_key_exists('start_date', $filterParams) ? date('d-m-Y', strtotime($filterParams['start_date'])) : '' }}">
                            </div>
                        </div>

                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="end_date">{{__("{$moduleName}::e.to_date")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input autocomplete="off" name="end_date" id="end_date" class="datetimepicker"
                                    value="{{ !empty($filterParams) && array_key_exists('end_date', $filterParams) ? date('d-m-Y', strtotime($filterParams['end_date'])) : '' }}">
                            </div>
                        </div>
                    </div>

                    <div class="field-btn">
                        <button class="  half submit-form-btn button blue" data-form-id="filter-form"
                                data-form-event="submit-form">{{__("{$moduleName}::e.filter")}}
                        </button>
                        <button class=" half submit-form-btn button light-blue" data-form-id="filter-form"
                                data-form-event="refresh-form">{{__("{$moduleName}::e.reset")}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endif