@if(!$items->isEmpty())

    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="align-center">{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.title_table")}}</th>
            <th class="left">Slug</th>
            @if(request()->get('parent') && (@$parent->type->slug != 'select' && @$parent->type->slug != 'radio'))
                <th class="align-center">Options</th>
            @endif
            @if(!request()->get('parent'))
                <th>{{__("{$moduleName}::e.type")}}</th>
                <th class="align-center">{{__("{$moduleName}::e.options")}}</th>
            @endif
            @if($permissions->active)
                <th class="align-center">
                    Status
                </th>
            @endif

            @if($permissions->edit)
                <th class="align-center">Position</th>
                <th class="align-center">Language</th>
            @endif
            @if($permissions->delete)
                <th class="checkbox-all align-center" >
                    <div>Select All</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">
                    <p class="id">{{$item->id}}</p>
                </td>

                <td class="title left">
                        <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id,DEF_LANG_ID]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}">{{@$parent->field_type == 'range' ? @$item->range_value : @$item->globalName->name}}</a>
                </td>

                <td class="slug left">
                    <p>{{$item->slug}}</p>
                </td>

                @if(request()->get('parent') && (@$parent->type->slug != 'select' && @$parent->type->slug != 'radio'))
                    <td class="small1">
                        <div class="item-attribute">
                            @if(@$parent->type->slug == 'color' && !empty($item->colors))
                                <div class="colors-attribute tooltip" title="{{@$item->globalName->name}}">
                                    @foreach($item->colors as $key => $color)
                                        @if(count($item->colors) > 4)
                                            <span class="color-attribute"
                                                  style="background: {{$color}}; width: calc(100% / {{count($item->colors)}});"></span>
                                        @elseif(count($item->colors) % 2 == 0)
                                            <span class="color-attribute"
                                                  style="background: {{$color}}; width: calc(100% / {{count($item->colors) / 2}});"></span>
                                        @elseif(count($item->colors) == 3)
                                            <span class="color-attribute"
                                                  style="background: {{$color}}; width: calc(100% / {{$key < 2 ? 2 : 1}});"></span>
                                        @else
                                            <span class="color-attribute"
                                                  style="background: {{$color}}; width: calc(100% / {{$key + 1}});"></span>
                                        @endif
                                    @endforeach
                                </div>
                            @elseif(@$parent->type->slug == 'image')
                                <div class="image-attribute">
                                    <img src="{{$item->firstFile->small}}" alt="{{$item->slug}}" class="tooltip"
                                         title="{{@$item->globalName->name}}">
                                </div>
                            @endif
                        </div>
                    </td>
                @endif

                @if(!request()->get('parent'))
                    <td>{{!is_null(@$item->type->name) ? $item->type->name : '-'}}</td>
                    <td>
                        <div class="inline-item-attribute">
                            @if(!$item->children->isEmpty())
                                @foreach($item->children as $key => $child)
                                    @if(@$item->type->slug == 'select' || @$item->type->slug == 'radio')
                                        @if($item->field_type == 'range')
                                            <span> {{$child->range_value}}{{$item->children->count() > $key + 1 ? ', ' : ''}}</span>
                                        @elseif(!is_null($child->globalName))
                                            <span> {{@$child->globalName->name}}{{$item->children->count() > $key + 1 ? ', ' : ''}}</span>
                                        @endif
                                    @elseif(@$item->type->slug == 'color')
                                        <div class="colors-attribute tooltip" title="{{@$child->globalName->name}}">
                                            @foreach($child->colors as $key => $color)
                                                @if(count($child->colors) > 4)
                                                    <span class="color-attribute"
                                                          style="background: {{$color}}; width: calc(100% / {{count($child->colors)}});"></span>
                                                @elseif(count($child->colors) % 2 == 0)
                                                    <span class="color-attribute"
                                                          style="background: {{$color}}; width: calc(100% / {{count($child->colors) / 2}});"></span>
                                                @elseif(count($child->colors) == 3)
                                                    <span class="color-attribute"
                                                          style="background: {{$color}}; width: calc(100% / {{$key < 2 ? 2 : 1}});"></span>
                                                @else
                                                    <span class="color-attribute"
                                                          style="background: {{$color}}; width: calc(100% / {{$key + 1}});"></span>
                                                @endif
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="image-attribute">
                                            <img src="{{$child->firstFile->small}}" alt="{{$child->slug}}"
                                                 class="tooltip"
                                                 title="{{@$child->globalName->name}}">
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
{{--                        @if(!$item->children->isEmpty())--}}
                            <div class="see-option"><a href="{{url(LANG, ['admin', $currComponent->slug]) . "?parent={$item->id}"}}">See List</a></div>
{{--                        @endif--}}
                    </td>
                @endif
                @if($permissions->active)
                    <td class="status">
                        <span class="activate-item {{$item->active ? 'active' : ''}}"
                              data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                              data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>
                    </td>
                @endif
                <td class="position">
                    <img src="{{asset('admin-assets/img/small-icons/position.svg')}}" alt="">
                </td>
                @if($permissions->edit)
                    <td class="td-link">
                        @if(@$parent->field_type == 'range')
                            <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}">{{__("{$moduleName}::e.edit_table")}}</a>
                        @else
                            @foreach($langList as $lang)
                                <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $lang->id]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}" {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                            @endforeach
                        @endif
                    </td>
                @endif
                @if($permissions->delete)
                    <td class="checkbox-items">
                        @if($item->children->isEmpty() && $item->attrOptions->isEmpty())
                            <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                   name="checkbox_items[{{$item->id}}]"
                                   value="{{$item->id}}">
                            <label for="{{$item->id}}">
                            </label>
                        @else
                            <svg title="{{__("{$moduleName}::e.attr_is_used_or_has_children")}}" class="tooltip" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                <g id="Group_360" data-name="Group 360" transform="translate(-1400 -1236)">
                                    <circle id="Ellipse_22" data-name="Ellipse 22" cx="10" cy="10" r="10" transform="translate(1400 1236)" fill="#9f9f9f"/>
                                    <path id="Path_79" data-name="Path 79" d="M2.769-3.056,3-9.953H.9l.232,6.9Zm-.82,1.08a1.091,1.091,0,0,0-.8.3,1,1,0,0,0-.3.749.992.992,0,0,0,.3.745,1.1,1.1,0,0,0,.8.294,1.107,1.107,0,0,0,.8-.294.992.992,0,0,0,.3-.745,1,1,0,0,0-.3-.752A1.107,1.107,0,0,0,1.948-1.976Z" transform="translate(1408 1251)" fill="#fff"/>
                                </g>
                            </svg>
                        @endif
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
