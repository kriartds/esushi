@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent, new \Modules\Products\Models\ProductsAttributesId(),@request()->get('parent')) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'trash'], 'item' => null, 'currentPage'=> 'trash'])

        <div class="table-block trash">
            @if(!$items->isEmpty())
                <table class="table">
                    <thead>
                    <tr>
                        <th class="align-center">{{__("{$moduleName}::e.id_table")}}</th>
                        <th>{{__("{$moduleName}::e.title_table")}}</th>
                        <th>Slug</th>
                        <th class="align-center">Option</th>
                        @if($permissions->edit)
                            <th class="align-center">{{__("{$moduleName}::e.edit_table")}}</th>
                        @endif
                        @if($permissions->delete)
                            <th class="checkbox-all align-center" >
                                <div>Select All</div>
                            </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td class="id">
                                <p>{{$item->id}}</p>
                            </td>

                            <td class="title">
                                <p>{{@$item->parent->globalName->name}}</p>
                            </td>

                            <td class="slug">
                                <p>{{$item->slug}}</p>
                            </td>

                            <td class="title">
                                <div class="item-attribute">
                                    <div class="colors-attribute tooltip" title="{{@$item->globalName->name}}">
                                        @if(@$item->parent->slug == 'color')

                                            @foreach($item->colors as $key => $color)
                                                @if(count($item->colors) > 4)
                                                    <span class="color-attribute"
                                                          style="background: {{$color}}; width: calc(100% / {{count($item->colors)}});"></span>
                                                @elseif(count($item->colors) % 2 == 0)
                                                    <span class="color-attribute"
                                                          style="background: {{$color}}; width: calc(100% / {{count($item->colors) / 2}});"></span>
                                                @elseif(count($item->colors) == 3)
                                                    <span class="color-attribute"
                                                          style="background: {{$color}}; width: calc(100% / {{$key < 2 ? 2 : 1}});"></span>
                                                @else
                                                    <span class="color-attribute"
                                                          style="background: {{$color}}; width: calc(100% / {{$key + 1}});"></span>
                                                @endif
                                            @endforeach
                                        @else
                                            <p class="align-center">{{@$parent->field_type == 'range' ? @$item->range_value : @$item->globalName->name}}</p>
                                        @endif
                                    </div>
                                </div>
                            </td>

                            @if($permissions->edit)
                                <td class="td-link small">
                                    @foreach($langList as $lang)
                                        <a {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                                    @endforeach
                                </td>
                            @endif

                            @if($permissions->delete)
                                <td class="checkbox-items">
                                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                           name="checkbox_items[{{$item->id}}]"
                                           value="{{$item->id}}">
                                    <label for="{{$item->id}}"></label>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')