<form class="attributes" method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}" id="{{!is_null($item) ? 'edit' : 'create'}}-form"
      enctype="multipart/form-data">

    <div class="container">
        <div class="row">
            <div class="col middle">

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="lang">{{__("{$moduleName}::e.lang")}}*</label>
                    </div>
                    <div class="field-wrap">
                        @if(!$langList->isEmpty())
                            <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                                @foreach($langList as $lang)
                                    <option value="{{$lang->id}}" {{ (is_null(@$langId) && $lang->id == LANG_ID ? 'selected' : $lang->id == @$langId) ? 'selected' : ''}}>{{$lang->name ?: ''}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>

                @if(!request()->get('parent') && !$types->isEmpty())
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="attributes-types">{{__("{$moduleName}::e.type_attributes")}}*</label>
                        </div>
                        <div class="field-wrap">
                            <select autocomplete="off" name="type" id="attributes-types" class="select2 no-search">
                                @foreach($types as $type)
                                    <option value="{{$type->id}}" {{@$item->type->id == $type->id ? 'selected' : ''}}>{{$type->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif

                @if(request()->get('parent') && !is_null($parent))
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="parent">{{__("{$moduleName}::e.attribute")}}</label>
                        </div>
                        <div class="field-wrap">
                            <input type="text" value="{{@$parent->globalName->name}}" disabled>
                            <input type="hidden" name="parent" value="{{$parent->id}}">
                        </div>
                    </div>
                @endif

            </div>

            @if(!request()->get('parent'))
                <div class="col middle">
                        <div class="field-row {{(@$item->type->slug == 'select' || @$item->type->slug == 'radio' || is_null(@$item->type->slug)) ? '' : 'hidden'}}" id="attributes-fields-types">
                            <div class="label-wrap">
                                <label for="field_type">{{__("{$moduleName}::e.field_type_attributes")}}*</label>
                            </div>
                            <div class="field-wrap">
                                <select autocomplete="off" name="field_type" id="field_type" class="select2 no-search">
                                        <option value="checkbox" {{@$item->field_type == 'checkbox' ? 'selected' : ''}}>Checkbox</option>
                                        <option value="range" {{@$item->field_type == 'range' ? 'selected' : ''}}>Range</option>
                                        <option value="radio" {{@$item->field_type == 'radio' ? 'selected' : ''}}>Radio</option>
                                </select>
                            </div>
                        </div>

                        <div class="field-row switcher-container">
                            <div class="label-wrap">
                                <label for="display_in_filter">{{__("{$moduleName}::e.display_in_filter")}}</label>
                            </div>
                            <div class="checkbox-switcher">
                                <input type="checkbox" name="display_in_filter" id="display_in_filter" {{@$item->display_in_filter ? 'checked' : ''}}>
                                <label for="display_in_filter"></label>
                            </div>
                        </div>
                </div>
            @endif

            <div class="col large">

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__("{$moduleName}::e.title_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$parent->field_type == 'range' ? @$item->range_value : @$item->itemByLang->name}}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="slug">{{__("{$moduleName}::e.slug_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="slug" id="slug" value="{{@$item->slug}}">
                    </div>
                </div>

            </div>
        </div>

        <div class="field-row">
            <div class="label-wrap">
                <label for="description">{{__("{$moduleName}::e.description")}}</label>
            </div>
            <div class="field-wrap">
                <textarea name="description" id="description">{!! @$item->itemByLang->description !!}</textarea>
            </div>
        </div>

        @if(request()->get('parent'))

            @if(@$parent->type->slug == 'image')
                <div class="field-row">
                    @include('admin.templates.uploadFile', [
                        'item' => @$item,
                        'options' => [
                            'data-component-id' => $currComponent->id,
                            'data-types' => 'image'
                        ]
                    ])
                </div>
            @elseif(@$parent->type->slug == 'color')

{{--                <div class="color-attr-container">--}}

{{--                    <div class="label-wrap">--}}
{{--                        <label for="color">{{__("{$moduleName}::e.color")}}*</label>--}}
{{--                    </div>--}}

{{--                    <div class="field-row switcher-container">--}}
{{--                        <div class="label-wrap">--}}
{{--                            <label for="use_many_colors">{{__("{$moduleName}::e.use_many_colors")}}</label>--}}
{{--                        </div>--}}
{{--                        <div class="checkbox-switcher">--}}
{{--                            <input type="checkbox" name="use_many_colors" id="use_many_colors" {{@$item->color && count($item->colors) > 1 ? 'checked' : ''}}>--}}
{{--                            <label for="use_many_colors"></label>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="field-row inline">--}}
{{--                        <div class="box-for-colorpicker">--}}
{{--                            <input type="text" class="attribute-colors {{!@$item->color || count($item->colors) < 2 ? 'colorpicker' : ''}}" name="color" value="{{@$item->color}}" >--}}
{{--                        </div>--}}
{{--                        <div class="field-wrap">--}}
{{--                            <input type="text"  name="color" id="color" value="{{@$item->color}}" placeholder="{{__("{$moduleName}::e.colo_placeholder_info")}}" style="color: {{@$item->color}}">--}}
{{--                            <input type="text" class="attribute-colors {{!@$item->color || count($item->colors) < 2 ? 'colorpicker' : ''}}"--}}
{{--                                   placeholder="{{__("{$moduleName}::e.colo_placeholder_info")}}" name="color" id="color" value="{{@$item->color}}">--}}


{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="color-attr-container">
                    ​
                    <div class="label-wrap">
                        <label for="color">{{__("{$moduleName}::e.color")}}*</label>
                    </div>
                    ​
                    <div class="field-row switcher-container">
                        <div class="label-wrap">
                            <label for="use_many_colors">{{__("{$moduleName}::e.use_many_colors")}}</label>
                        </div>
                        <div class="checkbox-switcher">
                            <input type="checkbox" name="use_many_colors" id="use_many_colors" {{@$item->color && count($item->colors) > 1 ? 'checked' : ''}}>
                            <label for="use_many_colors"></label>
                        </div>
                    </div>
                    ​
                    <div class="field-row inline">
                        <div class="box-for-colorpicker"></div>
                        <div class="field-wrap">
                            <input type="text" class="attribute-colors {{!@$item->color || count($item->colors) < 2 ? 'colorpicker' : ''}}" placeholder="{{__("{$moduleName}::e.colo_placeholder_info")}}" name="color" id="color" value="{{@$item->color}}">
                        </div>
                    </div>
                </div>

            @endif

        @endif

    </div>
    <button class="button blue submit-form-btn" data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
</form>

