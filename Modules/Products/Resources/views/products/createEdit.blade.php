 @extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'import','trash', !is_null(@$item) ? 'edit' : ''], 'item' => @$item, 'langId' => @$langId, 'goToSitePage' => @$item ? customUrl(['products', @$item->slug]) : ''])

        <div class="form-block">
            @include("{$path}.form", ['item' => @$item])
        </div>
    </div>
@stop

@include('admin.footer')