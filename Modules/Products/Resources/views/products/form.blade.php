<form class="products" method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form"
      enctype="multipart/form-data">

    <div class="container">

        <div class="row">

            <div class="col middle">

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="lang">{{__("{$moduleName}::e.lang")}}*</label>
                    </div>
                    <div class="field-wrap">
                        @if(!$langList->isEmpty())
                            <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                                @foreach($langList as $lang)
                                    <option value="{{$lang->id}}" {{ (is_null(@$langId) && $lang->id == LANG_ID ? 'selected' : $lang->id == @$langId) ? 'selected' : ''}}>{{$lang->name ?: ''}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="categories">{{__("{$moduleName}::e.categories")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <select autocomplete="off" name="categories[]" id="categories" class="select2" multiple>
                            @if(!$parents->isEmpty())
                                {!! getParentChildrenInSelect($parents, @$item->categoriesIds) !!}
                            @endif
                        </select>
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="toppings">{{__("{$moduleName}::e.toppings")}}</label>
                    </div>
                    <div class="field-wrap">
                        <select autocomplete="off" name="toppings[]" id="toppings" class="select2" multiple>
                            @if($toppings->isNotEmpty())
                                @foreach($toppings as $topping)
                                    <option value="{{$topping->id}}" {{@$item->toppings_ids && in_array($topping->id,  @$item->toppings_ids) ? 'selected' : ''}}>{{@$topping->globalName->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

            </div>

            <div class="col middle">

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__("{$moduleName}::e.title_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$item->itemByLang->name}}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="weight">{{__("{$moduleName}::e.weight")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="weight" id="weight" value="{{@$item->itemByLang->weight}}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="badges">{{__("{$moduleName}::e.badges")}}</label>
                    </div>
                    <div class="field-wrap">
                        <select autocomplete="off" name="badges[]" id="badges" class="select2" multiple>
                            <option value="">-</option>
                            <option value="new" @if(!is_null(@$item->badges) && in_array('new', @$item->badges)) selected @endif>New</option>
                            <option value="sale" @if(!is_null(@$item->badges) && in_array('sale', @$item->badges)) selected @endif>Sale</option>
                            <option value="top" @if(!is_null(@$item->badges) && in_array('top', @$item->badges)) selected @endif>Top</option>
                            <option value="party" @if(!is_null(@$item->badges) && in_array('party', @$item->badges)) selected @endif>Party</option>
                            <option value="vegan" @if(!is_null(@$item->badges) && in_array('vegan', @$item->badges)) selected @endif>Vegan</option>
                            <option value="spicy" @if(!is_null(@$item->badges) && in_array('spicy', @$item->badges)) selected @endif>Spicy</option>
                        </select>
                    </div>
                </div>



            </div>

            <div class="col large">

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="slug">{{__("{$moduleName}::e.slug_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="slug" id="slug" value="{{@$item->slug}}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="promotions">{{__("{$moduleName}::e.promotions")}}</label>
                    </div>
                    <div class="field-wrap">
                        <select autocomplete="off" name="promotions[]" id="promotions" class="select2" multiple>
                            @if($promotions->isNotEmpty())
                                @foreach($promotions as $promotion)
                                    <option value="{{$promotion->id}}" {{@$item && in_array($promotion->id,  @$item->promotionsIds) ? 'selected' : ''}}>{{@$promotion->globalName->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

{{--                @if(is_null($item) || (!is_null($item) && $item->item_type_id == 1))--}}
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="iiko_id">{{__("{$moduleName}::e.iiko_product")}}</label>
                        </div>
                        <div class="field-wrap">
                            <select autocomplete="off" name="iiko_id" id="iiko_id" class="select2">
                                <option></option>
                                @if($iiko_products->isNotEmpty())
                                    @foreach($iiko_products as $iiko_product)
                                        <option value="{{$iiko_product->iiko_id}}" {{@$item && $iiko_product->iiko_id == @$item->iiko_id ? 'selected' : ''}}>{{@$iiko_product->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
{{--                @endif--}}
            </div>

        </div>

        <div class="field-row">
            <div class="label-wrap">
                <label for="description">{{__("{$moduleName}::e.description")}}</label>
            </div>
            <div class="field-wrap">
                <textarea name="description" id="description"
                        data-type="ckeditor">{!! @$item->itemByLang->description !!}</textarea>
            </div>
        </div>

        <div class="field-row">
            @include('admin.templates.uploadFile', [
                'item' => @$item,
                'options' => [
                    'data-component-id' => $currComponent->id,
                    'data-types' => 'image'
                ]
            ])
        </div>

        <div class="field-row">
            <div class="label-wrap">
                <label for="video">{{__("{$moduleName}::e.video")}}*</label>
            </div>
            <div class="field-wrap">
                <input name="video" id="video" value="{{@$item->video}}">
            </div>
        </div>

    </div>

    <div class="container middle transparent-bg">
        <div class="row">

            @if(!$types->isEmpty())
                <div class="col flex-1">
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="product_type">{{__("{$moduleName}::e.product_type")}}</label>
                        </div>
                        <div class="field-wrap">
                            <select autocomplete="off" name="product_type" id="product_type" class="select2 no-search">
                                @foreach($types as $type)
                                    <option value="{{$type->id}}"
                                            data-type="{{$type->slug}}" {{$type->id == @$item->item_type_id ? 'selected' : ''}}>{{$type->name ?: ''}}
                                        product
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

            @endif

            <div class="col flex-1">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="recommended_categories">{{__("{$moduleName}::e.recommended_categories")}}</label>
                    </div>
                    <div class="field-wrap">
                        <select autocomplete="off" name="recommended_categories[]" id="recommended_categories" class="select2"
                                multiple>
                            @if(!$parents->isEmpty())
                                {!! getParentChildrenInSelect($parents, @$item->recommended_by_categories_ids) !!}
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="col flex-1">
                <div class="field-row inline  white-bg">
                    <div class="label-wrap">
                        <label for="show_on_main">{{__("{$moduleName}::e.show_on_main")}}</label>
                    </div>
                    <div class="checkbox-switcher">
                        <input type="checkbox" name="show_on_main" id="show_on_main" {{@$item->show_on_main ? 'checked' : ''}}>
                        <label for="show_on_main"></label>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @if(!$types->isEmpty())
        <div class="tabs-block">
            <div class="left-side">
                <div class="tab-item {{is_null(@$item) ? (@$types->first()->slug != 'simple' ? 'hidden' : 'active') : (@$item->isVariationProduct ? 'hidden' : 'active')}}"
                     data-type="simple">
                    <a href="#general-block">{{__("{$moduleName}::e.product_general")}}</a>
                </div>
                <div class="tab-item {{is_null(@$item) ? (@$types->first()->slug != 'simple' ? 'hidden' : '') : (@$item->isVariationProduct ? 'hidden' : '')}}"
                     data-type="simple">
                    <a href="#inventory-block">{{__("{$moduleName}::e.inventory")}}</a>
                </div>
                <div class="tab-item {{is_null(@$item) ? (@$types->first()->slug != 'simple' ? 'active' : '') : (@$item->isVariationProduct ? 'active' : '')}}" >
                    <a href="#attributes-block">{{__("{$moduleName}::e.attributes")}}</a>
                </div>
                <div class="tab-item {{is_null(@$item) ? (@$types->first()->slug != 'variation' ? 'hidden' : '') : (!@$item->isVariationProduct ? 'hidden' : '')}}"
                     data-type="variation" data-url="{{customUrl(['admin', $currComponent->slug, 'getVariations']) . (@$langId ? "?lang_id={$langId}" : '')}}"
                     data-update-content="true" data-item-id="{{@$item->id}}">
                    <a href="#variations-block">{{__("{$moduleName}::e.product_variation")}}</a>
                </div>
            </div>
            <div class="right-side ">

                <div class="tab-target {{is_null(@$item) ? (@$types->first()->slug != 'simple' ? 'hidden' : '') : (@$item->isVariationProduct ? 'hidden' : '')}}"
                     id="general-block">
                    <div class="price-container">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="price">{{__("{$moduleName}::e.price")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input name="price" id="price" value="{{@$item->price}}">
                            </div>
                        </div>
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="sale_price">{{__("{$moduleName}::e.sale_price")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input name="sale_price" id="sale_price" value="{{@$item->sale_price}}">
                            </div>
                        </div>
                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="discount_percentage">{{__("{$moduleName}::e.discount_percentage")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input name="discount_percentage" id="discount_percentage" value="{{@$item->discount_percentage}}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-target {{is_null(@$item) ? (@$types->first()->slug != 'simple' ? 'hidden' : '') : (@$item->isVariationProduct ? 'hidden' : '')}}" id="inventory-block">
                    <div class="inventory-container">

                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="sku">{{__("{$moduleName}::e.sku")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input name="sku" id="sku" value="{{@$item->sku}}">
                            </div>
                        </div>

                        <div class="field-row switcher-container">
                            <div class="label-wrap">
                                <label for="use_stock">{{__("{$moduleName}::e.use_stock")}}</label>
                            </div>
                            <div class="checkbox-switcher">
                                <input type="checkbox" name="use_stock"
                                    id="use_stock" {{@$item->use_stock ? 'checked' : ''}}>
                                <label for="use_stock"></label>
                            </div>
                        </div>

                        <div class="field-row {{!@$item->use_stock ? 'hidden' : ''}}" id="stock-quantity-block">
                            <div class="label-wrap">
                                <label for="stock_quantity">{{__("{$moduleName}::e.stock_quantity")}}</label>
                            </div>
                            <div class="field-wrap">
                                <input name="stock_quantity" id="stock_quantity" value="{{@$item->stock_quantity}}">
                            </div>
                        </div>

                        <div class="field-row {{@$item->use_stock || (is_null(@$item) && @$types->first()->slug == 'variation') || (@$item->isVariationProduct) ? 'hidden' : ''}}"
                            id="stock-status-block">
                            <div class="label-wrap">
                                <label for="stock_status">{{__("{$moduleName}::e.stock_status")}}</label>
                            </div>
                            <div class="field-wrap">
                                <select autocomplete="off" name="stock_status" id="stock_status" class="select2 no-search">
                                    <option value="in_stock" {{@$item->status == 'in_stock' ? 'selected' : ''}}>{{__("{$moduleName}::e.in_stock")}}</option>
                                    <option value="out_of_stock" {{@$item->status == 'out_of_stock' ? 'selected' : ''}}>{{__("{$moduleName}::e.out_of_stock")}}</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="tab-target {{is_null(@$item) ? (@$types->first()->slug != 'simple' ? 'active' : '') : (@$item->isVariationProduct ? 'active' : '')}}" id="attributes-block" >
                    @if(!$attributes->isEmpty())
                        <div class="field-row">

                            <div class="label-wrap">
                                <label for="attributes">{{__("{$moduleName}::e.attributes")}}</label>
                            </div>

                            <div class="field-wrap field-wrap-children small">

                                <div class="inline-field-wrap flex-3">
                                    <select autocomplete="off" name="attributes[]" id="attributes" class="select2"
                                            multiple>
                                        @foreach($attributes as $attribute)
                                            <option value="{{$attribute->id}}" {{in_array($attribute->id, is_array(@$item->attributesParentsIds) ? @$item->attributesParentsIds : []) ? 'disabled' : ''}}>{{@$attribute->globalName->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="inline-field-wrap">
                                    <button type="button" class="button blue" id="get-attributes-btn" disabled
                                            data-url="{{url(LANG, ['admin', $currComponent->slug, 'getAttributes'])}}" data-product-type="{{@$item->item_type_id}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><defs><clipPath id="y5rza"><path fill="#fff" d="M0 9a9 9 0 1 1 18 0A9 9 0 0 1 0 9z"/></clipPath></defs><g><g><g><path fill="none" stroke="#fff" stroke-miterlimit="50" stroke-width="4" d="M0 9a9 9 0 1 1 18 0A9 9 0 0 1 0 9z" clip-path="url(&quot;#y5rza&quot;)"/></g><g><path fill="none" stroke="#fff" stroke-miterlimit="50" stroke-width="2" d="M9 5v8"/></g><g><path fill="none" stroke="#fff" stroke-miterlimit="50" stroke-width="2" d="M5 9h8"/></g></g></g></svg>
                                            {{__("{$moduleName}::e.add_attributes")}}
                                    </button>
                                </div>
                            </div>

                        </div>

                        <div class="selected-attributes-block">
                            @if(!is_null(@$selectedAttributes) && !$selectedAttributes->isEmpty())
                                @foreach($selectedAttributes as $selectedAttribute)
                                    @include("{$path}.templates.attributesItems", ['item' => $selectedAttribute, 'product' => @$item])
                                @endforeach
                            @endif
                        </div>

                        <div class="save-attributes-btn">
                            <button type="button"
                                    class="button blue"
                                    id="save-attributes-btn" {{!@$item->attributesParentsIds ? 'disabled' : ''}}
                                    id="save-attributes-btn" disabled
                                    data-url="{{url(LANG, ['admin', $currComponent->slug, 'saveAttributes', @$item->id, @$langId])}}">{{__("{$moduleName}::e.save_attributes")}}
                            </button>
                        </div>
                    @else
                        <div class="empty-list">
                            {{__("{$moduleName}::e.list_is_empty")}}
                            <a href="{{url(LANG, ['admin', 'attributes', 'create'])}}"
                               target="_blank">{{__("{$moduleName}::e.add_attributes")}}</a>
                        </div>
                    @endif
                </div>

                <div class="tab-target" id="variations-block">

                    <div class="field-row variations-header">

                        <div class="label-wrap">
                            <label for="variation_actions">{{__("{$moduleName}::e.variation_actions")}}</label>
                        </div>

                        <div class="field-wrap field-wrap-children">
                            <div class="inline-field-wrap">
                                <select autocomplete="off" name="variation_actions" id="variation_actions"
                                        class="select2 no-search">
                                    <optgroup label="General">
                                        <option value="add_variation"
                                                data-visible="true">{{__("{$moduleName}::e.add_variation")}}</option>
                                        <option value="add_variations_recursive"
                                                data-visible="true">{{__("{$moduleName}::e.create_variation_recursive")}}</option>
                                        <option value="destroy_variations">{{__("{$moduleName}::e.destroy_variations")}}</option>
                                    </optgroup>
                                    <optgroup label="Pricing">
                                        <option value="variations_regular_price"
                                                data-with-value="true">{{__("{$moduleName}::e.set_regular_prices")}}</option>
                                        <option value="variations_regular_price_increase"
                                                data-with-value="true">{{__("{$moduleName}::e.increase_regular_prices")}}</option>
                                        <option value="variations_regular_price_decrease"
                                                data-with-value="true">{{__("{$moduleName}::e.decrease_regular_prices")}}</option>
                                        <option value="variations_sale_price"
                                                data-with-value="true">{{__("{$moduleName}::e.set_sale_prices")}}</option>
                                        <option value="variations_sale_price_increase"
                                                data-with-value="true">{{__("{$moduleName}::e.increase_sale_prices")}}</option>
                                        <option value="variations_sale_price_decrease"
                                                data-with-value="true">{{__("{$moduleName}::e.decrease_sale_prices")}}</option>
                                    </optgroup>
                                    <optgroup label="Inventory">
                                        <option value="variations_use_stock">{{__("{$moduleName}::e.toggle_use_stock")}}</option>
                                        <option value="variations_stock_quantity"
                                                data-with-value="true">{{__("{$moduleName}::e.stock_quantity")}}</option>
                                        <option value="variations_in_stock">{{__("{$moduleName}::e.set_status_in_stock")}}</option>
                                        <option value="variations_out_of_stock">{{__("{$moduleName}::e.set_status_out_of_stock")}}</option>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="inline-field-wrap hidden" id="variation-action-value-block">
                                <input type="text" name="variation_action_value" id="variation-action-value"
                                       placeholder="{{__("{$moduleName}::e.variation_action_value")}}">
                            </div>
                            <div class="inline-field-wrap">
                                <button type="button" class="button blue" id="make_variation_action-btn"
                                        data-item-id="{{@$item->id}}"
                                        data-url="{{url(LANG, ['admin', $currComponent->slug, 'makeVariationAction'])}}"
                                        data-action-url="{{url(LANG, ['admin', $currComponent->slug, 'setVariationsActions'])}}"
                                        data-destroy-url="{{url(LANG, ['admin', $currComponent->slug, 'destroyVariations'])}}">
                                    {{__("{$moduleName}::e.apply")}}
                                </button>
                            </div>
                            <!-- <div class="inline-field-wrap">
                                <div class="toggle-variations-description {{@$paginationVariations && $paginationVariations->total > 0 ? '' : 'hidden'}}"></div>
                            </div> -->
                        </div>

                    </div>

                    <div class="variations-list">

                        <div class="variations-list-header">
                            <div class="default-variation-block">
                                @include("{$path}.templates.defaultVariationItems", ['defaultVariationAttributes' => @$defaultVariationAttributes])
                            </div>
                            <div class="pagination-block">
                                @if(@$paginationVariations && $paginationVariations->total > $paginationVariations->perPage)
                                    @include("{$path}.templates.paginationVariations", ['pagination' => $paginationVariations])
                                @endif
                            </div>
                        </div>

                        <div class="variations-items"></div>

                        <div class="variations-list-footer">
                            <div class="save-variations-changes-block {{@$paginationVariations && $paginationVariations->total > 0 ? 'active' : ''}}"
                                 id="save_variations_changes_block">
                                <button type="button" class="button blue" id="save_variations_changes-btn"
                                        data-item-id="{{@$item->id}}"
                                        data-url="{{url(LANG, ['admin', $currComponent->slug, 'updateVariations'])}}"
                                        disabled>
                                    {{__("{$moduleName}::e.save_changes")}}
                                </button>
                            </div>
                            <div class="pagination-block">
                                @if(@$paginationVariations && $paginationVariations->total > $paginationVariations->perPage)
                                    @include("{$path}.templates.paginationVariations", ['pagination' => $paginationVariations])
                                @endif
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    @endif

    <div class="meta-conatiner">

        <div class="field-row">
            <div class="label-wrap">
                <label for="meta_title">{{__("{$moduleName}::e.meta_title_page")}}</label>
            </div>
            <div class="field-wrap">
                <input name="meta_title" id="meta_title" value="{{@$item->itemByLang->meta_title}}">
            </div>
        </div>

        <div class="field-row">
            <div class="label-wrap">
                <label for="meta_keywords">{{__("{$moduleName}::e.meta_keywords_page")}}</label>
            </div>
            <div class="field-wrap">
                <input name="meta_keywords" id="meta_keywords" value="{{@$item->itemByLang->meta_keywords}}">
            </div>
        </div>

        <div class="field-row">
            <div class="label-wrap">
                <label for="meta_description">{{__("{$moduleName}::e.meta_description_page")}}</label>
            </div>
            <div class="field-wrap">
                <textarea name="meta_description"
                        id="meta_description">{!! @$item->itemByLang->meta_description !!}</textarea>
            </div>
        </div>

    </div>

    <button class="button blue submit-form-btn"data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">
        {{__("{$moduleName}::e.save_it")}}
    </button>
</form>

