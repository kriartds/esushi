@php
    $iteration = !is_null(@$variationsDetail->id) ? @$variationsDetail->id : @$iteration
@endphp
<div class="variation-body">

    <div class="col">
        <div class="title">{{__("{$moduleName}::e.pricing")}}</div>
        <div class="field-row">
            <div class="field-wrap field-wrap-children">
                <div class="inline-field-wrap">
                    <div class="label-wrap">
                        <label for="price_{{@$iteration}}_variation">{{__("{$moduleName}::e.price")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input class="price_variation" name="item_variations[price_variation][{{@$iteration}}]" id="price_{{@$iteration}}_variation" value="{{@$variationsDetail->price}}">
                    </div>
                </div>
                <div class="inline-field-wrap">
                    <div class="label-wrap">
                        <label for="sale_price_{{@$iteration}}_variation">{{__("{$moduleName}::e.sale_price")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input class="sale_price_variation" name="item_variations[sale_price_variation][{{@$iteration}}]" id="sale_price_{{@$iteration}}_variation" value="{{@$variationsDetail->sale_price}}">
                    </div>
                </div>
                <div class="inline-field-wrap">
                    <div class="label-wrap">
                        <label for="discount_percentage_{{@$iteration}}_variation">Sale %</label>
                    </div>
                    <div class="field-wrap">
                        <input class="discount_percentage_variation" name="item_variations[discount_percentage_variation][{{@$iteration}}]" id="discount_percentage_{{@$iteration}}_variation" value="{{@$variationsDetail->discount_percentage}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="field-row">
            <div class="field-wrap field-wrap-children">
                <div class="inline-field-wrap">
                    <div class="label-wrap">
                        <label for="iiko_{{@$iteration}}_variation">{{__("{$moduleName}::e.iiko_product")}}</label>
                    </div>
                    <div class="field-wrap">
                        <select autocomplete="off" name="item_variations[iiko_id][{{@$iteration}}]" id="iiko_{{@$iteration}}_variation" class="select2">
                            <option></option>
                            @if($iiko_products->isNotEmpty())
                                @foreach($iiko_products as $iiko_product)
                                    <option value="{{$iiko_product->iiko_id}}" {{@$variationsDetail && $iiko_product->iiko_id == @$variationsDetail->iiko_id ? 'selected' : ''}}>{{@$iiko_product->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col">
        <div class="title">{{__("{$moduleName}::e.inventory")}}</div>
        <div class="field-row">
            <div class="field-wrap field-wrap-children">

                <div class="inline-field-wrap">
                    <div class="label-wrap">
                        <label for="sku_{{@$iteration}}_variation">{{__("{$moduleName}::e.sku")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="item_variations[sku_variation][{{@$iteration}}]" id="sku_{{@$iteration}}_variation" value="{{@$variationsDetail->sku}}">
                    </div>
                </div>

                <div class="switcher-row">
                    <div class="label-wrap">
                        <label for="use_stock_{{@$iteration}}_variation">{{__("{$moduleName}::e.use_stock")}}</label>
                    </div>
                    <div class="checkbox-switcher">
                        <input type="checkbox" name="item_variations[use_stock_variation][{{@$iteration}}]" class="use_stock_variation"
                            id="use_stock_{{@$iteration}}_variation" {{@$variationsDetail->use_stock ? 'checked' : ''}}>
                        <label for="use_stock_{{@$iteration}}_variation"></label>
                    </div>
                </div>
            </div>

            <div class="field-wrap field-wrap-children">

                <div class="inline-field-wrap stock-quantity-block_variation {{!@$variationsDetail->use_stock ? 'hidden' : ''}}">
                    <div class="label-wrap">
                        <label for="stock_quantity_{{@$iteration}}_variation">{{__("{$moduleName}::e.stock_quantity")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="item_variations[stock_quantity_variation][{{@$iteration}}]" id="stock_quantity_{{@$iteration}}_variation"
                            value="{{@$variationsDetail->stock_quantity}}">
                    </div>
                </div>

                <div class="inline-field-wrap">
                    <div class="stock-status-block_variation {{@$variationsDetail->use_stock ? 'hidden' : ''}}">
                        <div class="label-wrap">
                            <label for="stock_status_{{@$iteration}}_variation">{{__("{$moduleName}::e.stock_status")}}</label>
                        </div>
                        <div class="field-wrap">
                            <select autocomplete="off" name="item_variations[stock_status_variation][{{@$iteration}}]" id="stock_status_{{@$iteration}}_variation"
                                    class="select2 no-search">
                                <option value="in_stock" {{@$variationsDetail->status == 'in_stock' ? 'selected' : ''}}>{{__("{$moduleName}::e.in_stock")}}</option>
                                <option value="out_of_stock" {{@$variationsDetail->status == 'out_of_stock' ? 'selected' : ''}}>{{__("{$moduleName}::e.out_of_stock")}}</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>
