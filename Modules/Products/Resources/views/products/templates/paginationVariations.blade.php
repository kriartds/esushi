@if(@$pagination && $pagination->lastPage > 1)
        <div class="total">{{__("{$moduleName}::e.variation_pagination_total", ['count' => $pagination->total])}}</div>
        <ul class="pagination">
            <li><span class="prev first-last-page"></span></li>
            <li>
                <select name="paginate_variations" title="Current page" data-last-page="{{$pagination->lastPage}}">
                    @for($i = 1; $i <= $pagination->lastPage; $i++)
                        <option value="{{$i}}" {{$pagination->currPage == $i ? 'selected' : ''}}>{{$i}}</option>
                    @endfor
                </select>
                <span class="total-pages">{{__("{$moduleName}::e.variation_pagination_of", ['count' => $pagination->lastPage])}}</span>
            </li>
            <li><span class="next first-last-page"></span></li>
        </ul>
@endif