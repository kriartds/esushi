@if(isset($item))
    <div class="field-row" data-item-id="{{$item->id}}">
        <div class="field-wrap field-wrap-children attr-row">

            <div class="title atribute">
                {{@$item->globalName->name}}
            </div>

            <div class="inline-field-wrap">
                @if($item->children->isNotEmpty())
                    <select autocomplete="off" name="attributes_opt[{{$item->id}}][children][]" id="attr-children-{{$item->id}}" class="select2" multiple>
                        @foreach($item->children as $child)
                            <option value="{{$child->id}}" {{in_array($child->id, is_array(@$selectedAttributesOptions) ? @$selectedAttributesOptions : []) ? 'selected' : ''}}>{{@$child->parent->field_type == 'range' ? @$child->range_value : @$child->globalName->name }}</option>
                        @endforeach
                    </select>
                @else
                    <div class="empty-list">
                        {{__("{$moduleName}::e.list_is_empty")}}
                    </div>
                @endif
            </div>

            <div class="inline-field-wrap switcher-container">
                <div class="checkbox-label">
                    <label for="on-product-page-{{$item->id}}">{{__("{$moduleName}::e.display_on_product_page")}}</label>
                    <div class="checkbox-switcher">
                        <input type="checkbox" name="attributes_opt[{{$item->id}}][on_product_page]" id="on-product-page-{{$item->id}}" {{!@$product ? 'checked' : (@$product->getSpecificAttrAttribute($item->id)->visible_on_product_page ? 'checked' : '')}}>
                        <label for="on-product-page-{{$item->id}}"></label>
                    </div>
                </div>
            </div>

            <div class="inline-field-wrap switcher-container {{@$product && !@$product->isVariationProduct || !@$product && !@$isVariationProduct ? 'hidden' : ''}}">
                <div class="checkbox-label use-for-variations-checkbox-label {{@$product && !@$product->isVariationProduct || !@$product && !@$isVariationProduct ? 'hidden' : ''}}">
                    <label for="use-variation-{{$item->id}}">{{__("{$moduleName}::e.use_for_variation")}}</label>
                    <div class="checkbox-switcher">
                        <input type="checkbox" name="attributes_opt[{{$item->id}}][use_variation]" id="use-variation-{{$item->id}}" {{@$product && @$product->getSpecificAttrAttribute($item->id)->for_variation ? 'checked' : ''}}>
                        <label for="use-variation-{{$item->id}}"></label>
                    </div>
                </div>
            </div>

            <div class="destroy-attribute"  data-item-id="{{$item->id}}" data-product-id="{{@$product->id}}" data-url="{{url(LANG, ['admin', $currComponent->slug, 'destroyAttribute'])}}">
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><g><g><g><path fill="#ea5355" d="M0 9a9 9 0 1 1 18 0A9 9 0 0 1 0 9z"/></g><g><g><path fill="none" stroke="#fff" stroke-miterlimit="50" stroke-width="2" d="M5 5l8 8"/></g><g><path fill="none" stroke="#fff" stroke-miterlimit="50" stroke-width="2" d="M13 5l-8 8"/></g></g></g></g></svg>
                Remove
            </div>
        </div>
    </div>
@endif