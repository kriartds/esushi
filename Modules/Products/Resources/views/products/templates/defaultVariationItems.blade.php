@if(@$defaultVariationAttributes && !empty($defaultVariationAttributes))
    <div class="title">Default variations</div>
    <div class="default-variation-items">
        @foreach($defaultVariationAttributes as $defaultVariationAttr)
            <div class="default-variation-item">
                <span class="parent">{{$defaultVariationAttr['parent']}}</span>
                <span>{{$defaultVariationAttr['child']}}</span>
            </div>
        @endforeach
    </div>
@endif