@if(@$variationsDetails && $variationsDetails->isNotEmpty() && @$variationAttributes && $variationAttributes->isNotEmpty())
    @foreach($variationsDetails as $currKey => $variationsDetail)
        <div class="field-row variation-item" data-variation-id="{{$variationsDetail->id}}">

            <div class="variation-header variation">

                <div class="field-wrap">
                    <div class="variation-id">#{{$variationsDetail->id}}</div>
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="default_variation_{{$variationsDetail->id}}">Set as default variation</label>
                        </div>
                        <div class="checkbox-switcher">
                            <input type="checkbox" id="default_variation_{{$variationsDetail->id}}" name="item_variations[is_default_variation][{{$variationsDetail->id}}]" class="set-default-variation" {{$variationsDetail->is_default ? 'checked' : ''}}>
                            <label for="default_variation_{{$variationsDetail->id}}" class="tooltip" title="{{__("{$moduleName}::e.set_as_default_variation")}}"></label>
                        </div>
                    </div>
                </div>

                <div class="field-wrap field-wrap-children">
                    @foreach($variationAttributes as $children)
                        @if(($parent = @$children['parent']) && $children['children']->isNotEmpty())
                            <div class="inline-field-wrap">
                                <div class="label-wrap">
                                    <label for="{{$parent->slug}}_{{$variationsDetail->id}}_variation">{{@$parent->globalName->name}}</label>
                                </div>
                                <div class="field-wrap">
                                    <select autocomplete="off"
                                            name="item_variations[attr_ids][{{$variationsDetail->id}}][{{$parent->id}}]"
                                            id="{{$parent->slug}}_{{$variationsDetail->id}}_variation"
                                            class="select2" data-parent-id="{{$parent->id}}">
                                        <option value="all">{{__("{$moduleName}::e.all_attributes")}}</option>
                                        @foreach($children['children'] as $child)
                                            <option value="{{$child->id}}" {{in_array($parent->id, array_keys($variationsDetail->variationsChildrenIds)) && in_array($child->id, $variationsDetail->variationsChildrenIds) ? 'selected' : ''}}>{{$parent->field_type == 'range' ? @$child->range_value : @$child->globalName->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

            </div>

            @include("{$path}.templates.variationsItemsForm", ['variationsDetail' => $variationsDetail])

            <span class="destroy-variation" data-item-id="{{@$productId}}" data-variation-id="{{$variationsDetail->id}}" data-url="{{url(LANG, ['admin', $currComponent->slug, 'destroyVariations'])}}">
                Remove
            </span>

        </div>
    @endforeach

@endif