@if($items->isNotEmpty())
    <table class="table {{@$isTrash ? 'trash' : ''}}" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="align-center">{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.title_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.categories")}}</th>
{{--            <th>Brand</th>--}}
            <th class="align-center">Price</th>
            <th class="align-center">{{__("{$moduleName}::e.stock")}}</th>
            @if(!@$isTrash)
                <th class="visible align-center">Visible on the homepage</th>
                @if($permissions->active)
                    <th class="align-center">{{__("{$moduleName}::e.active_table")}}</th>
                @endif
                @if($permissions->edit)
                    <th class="align-center">Languages</th>
                @endif
            @endif

            @if($permissions->delete)
                <th class="checkbox-all align-center" data-event="{{@$isTrash ? 'from' : 'to'}}-trash">
                    <div>Select All</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}" {{$item->incomplete_data ? 'class=incomplete' : ''}}>
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>
                <td class="prod-name left">
                    <a class="{{@$isTrash ? 'trash' : ''}}" @if(!@$isTrash) href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, DEF_LANG_ID]) . @$requestQueryString}}" @endif
                       title="{{@$item->globalName->name}}">
                        {{@$item->globalName->name}}
                    </a>
                </td>
                <td class="categories left ">
                    <div class="prod-list">
                        @if(!$item->categories->isEmpty())
                            @foreach($item->categories->take(2) as $key => $category)
                                @if(!is_null($category->globalName))
                                    <span>{{@$category->globalName->name}}{{$item->categories->count() > $key + 1 ? ', ' : ''}}{{$item->categories->count() > 2 && $key == 1 ? '...' : ''}}</span>
                                @endif
                            @endforeach
                        @else
                            -
                        @endif
                    </div>
                </td>
{{--                <td class="brand">--}}
{{--                    {{@$item->brand->name}}--}}
{{--                </td>--}}
                <td class="price">
                    @if(@$item->finPrice->minPrice && @$item->isVariationProduct)

                        @if(@$item->finPrice->minPrice == @$item->finPrice->maxPrice)
                            <p>{!! formatPrice(@$item->finPrice->minPrice, $defaultCurrency) !!} </p>
                        @else
                            <p>From {!! formatPrice(@$item->finPrice->minPrice, $defaultCurrency) !!} </p>
                        @endif
                    @elseif(@$item->finPrice->minPrice && !@$item->isVariationProduct)
                        <p class="last">{!! formatPrice(@$item->finPrice->minPrice, $defaultCurrency)!!} </p>
                        <p class="past">{!! formatPrice(@$item->finPrice->maxPrice, $defaultCurrency)!!} </p>
                    @else
                        <p>{!! formatPrice(@$item->finPrice->maxPrice, $defaultCurrency) !!} </p>
                    @endif
                </td>
                <td class="stock">
                    @if(is_numeric($item->globalStock))
                        <p class="count">
                            {{$item->globalStock}}
                        </p>
                    @elseif($item->globalStock)
                        <p class="in {{@$isTrash ? 'trash' : ''}}">
                            {{__("{$moduleName}::e.in_stock")}}
                        </p>
                    @else
                        <p class="out">
                            {{__("{$moduleName}::e.out_of_stock")}}
                        </p>
                    @endif
                </td>
                @if(!@$isTrash)
                    <td class="visibility-on-homepage">

                            <div class="visible show_on_main @if($item->show_on_main) active @endif"
                                 data-active="{{$item->show_on_main}}" data-item-id="{{$item->id}}"
                                 data-url="{{url(LANG, ['admin', $currComponent->slug, 'show-on-main'])}}"
                                 data-msg="Do you want to be hidden this item?"
                                 data-action="show_on_main">
                                <svg xmlns="http://www.w3.org/2000/svg" width="33" height="19" viewBox="0 0 33 19">
                                    <g>
                                        <g>
                                            <g>
                                                <path fill="#34ca76"
                                                      d="M16.2 17c-5.3 0-10.3-2.7-13.8-7.5C4.3 6.9 6.6 5 9.2 3.7c-.6 1.1-1 2.5-1 3.9 0 4.4 3.6 8 8 8s8-3.6 8-8c0-1.4-.4-2.7-1-3.9C25.8 5 28.1 6.9 30 9.5c-3.5 4.8-8.5 7.5-13.8 7.5zM14.8 4c0 .4-.3.8-.8.8-1.2 0-2.1.9-2.1 2.1 0 .4-.3.8-.8.8-.4 0-.8-.3-.8-.8 0-2 1.6-3.6 3.6-3.6.6 0 .9.3.9.7zM32 8.9c-2.2-3.3-5.2-5.9-8.6-7.4-4.6-2-9.8-2-14.4 0C5.6 3 2.6 5.6.4 8.9l-.4.6.4.6C4.2 15.7 10 19 16.2 19c6.2 0 12-3.3 15.8-8.9l.4-.6z"/>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>

                            <div class="unvisible show_on_main @if(!$item->show_on_main) active @endif"
                                 data-active="{{$item->show_on_main}}" data-item-id="{{$item->id}}"
                                 data-url="{{url(LANG, ['admin', $currComponent->slug, 'show-on-main'])}}"
                                 data-msg="Do you want to be visible this item?"
                                 data-action="show_on_main">
                                <svg xmlns="http://www.w3.org/2000/svg" width="33" height="28" viewBox="0 0 33 28">
                                    <g>
                                        <g>
                                            <g>
                                                <path fill="#9f9f9f"
                                                      d="M24.27 12.046c0-1-.2-2.1-.6-3l-5.2 10.7c3.4-1 5.8-4 5.8-7.7z"/>
                                            </g>
                                            <g>
                                                <path fill="#9f9f9f"
                                                      d="M32.1 13.432c-1.9-2.9-4.4-5.2-7.3-6.7l-.9 1.8c2.3 1.3 4.4 3.2 6.1 5.5-3.1 4.3-7.6 7-12.4 7.4l-1 2.1c6.1-.1 11.7-3.4 15.4-8.9l.4-.6z"/>
                                            </g>
                                            <g>
                                                <path fill="#9f9f9f"
                                                      d="M11.347 20.706c-.904-.298-1.908-.695-2.812-1.192A18.677 18.677 0 0 1 2.41 14.05c1.908-2.484 4.217-4.471 6.828-5.763-.602 1.193-1.004 2.484-1.004 3.875 0 1.788.603 3.477 1.607 4.769.602.795 1.406 1.49 2.31 2.086zm2.912-12.915c.301.099.602.397.602.695a.788.788 0 0 1-.803.795c-.2 0-.401 0-.602.1-.904.198-1.507.993-1.507 1.986a.788.788 0 0 1-.803.795c-.402 0-.803-.298-.803-.795.1-1.987 1.707-3.576 3.715-3.576h.201zm9.74-6.657L21.79.041c-.1-.1-.301 0-.301.1l-2.41 4.669c-1.105-.199-2.109-.298-3.213-.199-2.31 0-4.62.497-6.829 1.49-3.414 1.49-6.426 4.074-8.635 7.353L0 14.05l.402.596c2.008 2.881 4.518 5.166 7.33 6.656.904.497 1.807.895 2.812 1.193l-2.009 4.073c-.1.1 0 .298.1.298l2.31 1.093c.1.1.302 0 .302-.1L24.1 1.532c.1-.198 0-.298-.1-.397z"/>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>

                    </td>
                @endif

                @if(!@$isTrash)
                    @if($permissions->active)
                        <td class="status">
                            <span class="activate-item {{$item->active ? 'active' : ''}}"
                                  data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                                  data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>

                        </td>
                    @endif
                @endif
                @if(!@$isTrash)
                    @if($permissions->edit)
                        <td class="td-link small">
                            @foreach($langList as $lang)
                                <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $lang->id]) . @$requestQueryString}}" {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                            @endforeach
                        </td>
                    @endif
                @endif

                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]" value="{{$item->id}}"
                               data-event="{{@$isTrash ? 'from' : 'to'}}-trash">
                        <label for="{{$item->id}}">
                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
