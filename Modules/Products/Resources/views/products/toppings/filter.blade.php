@if(isset($filterParams))
    <style>
        .filter .select2-container .select2-selection {
            background: #fff;
        }
    </style>
{{--    <button type="button" class="btn btn-inline filter-btn">--}}
{{--        <span class="count">{{isset($items) && ((!empty($filterParams) || empty($filterParams)) && $items->total() > 0) ? $items->total() : ""}}</span>--}}
{{--    </button>--}}

    <div class="right-col">
        <button type="button" class="button gray filter-btn">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12"><g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M1 1h14"/></g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M4 6h8"/></g><g><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50" stroke-width="2" d="M6 11h4"/></g></g></svg>
            Filters
            <span class="count">{{isset($items) && ((!empty($filterParams) || empty($filterParams)) && $items->total() > 0) ? $items->total() : ""}}</span>
        </button>
    </div>

    <div class="filter form-block" style="display: none;">
        <form method="post" action="{{url(LANG, ['admin', $currComponent->slug, 'filter'])}}"
              class="filter-form"
              id="filter-form">
            <div class="field-row-wrap">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__("{$moduleName}::e.title_table")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{ !empty($filterParams) && array_key_exists('name', $filterParams) ? $filterParams['name'] : '' }}">
                    </div>
                </div>
{{--                <div class="field-row">--}}
{{--                    <div class="label-wrap">--}}
{{--                        <label for="modifier_type">Tip modificator</label>--}}
{{--                    </div>--}}
{{--                    <div class="field-wrap">--}}
{{--                        <select autocomplete="off" name="modifier_type" id="modifier_type" class="select2 no-search">--}}
{{--                            <option value="">Not selected</option>--}}
{{--                            <option value="1" @if(@$filterParams['modifier_type'] == 1) selected @endif> Topping 1 (toate pizza) </option>--}}
{{--                            <option value="2" @if(@$filterParams['modifier_type'] == 2) selected @endif> Topping 2 (al 2-lea gust giganta) </option>--}}
{{--                            <option value="3" @if(@$filterParams['modifier_type'] == 3) selected @endif> Gust pizza giganta </option>--}}
{{--                            <option value="4" @if(@$filterParams['modifier_type'] == 4) selected @endif> Altele </option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                @if($categories->isNotEmpty())--}}
{{--                    <div class="field-row">--}}
{{--                        <div class="label-wrap">--}}
{{--                            <label for="category">{{__("{$moduleName}::e.categories")}}</label>--}}
{{--                        </div>--}}
{{--                        <div class="field-wrap">--}}
{{--                            <select autocomplete="off" name="category[]" id="category" class="select2" multiple>--}}
{{--                                {!! getParentChildrenInSelect($categories, @$filterParams['category']) !!}--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endif--}}

{{--                @if($productsTypes->isNotEmpty())--}}
{{--                    <div class="field-row">--}}
{{--                        <div class="label-wrap">--}}
{{--                            <label for="type">{{__("{$moduleName}::e.type")}}</label>--}}
{{--                        </div>--}}
{{--                        <div class="field-wrap">--}}
{{--                            <select autocomplete="off" name="type" id="type" class="select2 no-search">--}}
{{--                                <option value="">-</option>--}}
{{--                                @foreach($productsTypes as $productsType)--}}
{{--                                    <option value="{{$productsType->id}}" {{ !empty($filterParams) && array_key_exists('type', $filterParams) && $filterParams['type'] == $productsType->id ? 'selected' : '' }}>{{ucfirst($productsType->slug)}}</option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endif--}}

{{--                <div class="field-row">--}}
{{--                    <div class="field-row">--}}
{{--                        <div class="label-wrap">--}}
{{--                            <label for="stock">{{__("{$moduleName}::e.stock")}}</label>--}}
{{--                        </div>--}}
{{--                        <div class="field-wrap">--}}
{{--                            <select autocomplete="off" name="stock" id="stock" class="select2 no-search">--}}
{{--                                <option value="">-</option>--}}
{{--                                <option value="in_stock" {{ !empty($filterParams) && array_key_exists('stock', $filterParams) && $filterParams['stock'] == 'in_stock' ? 'selected' : '' }}>{{__("{$moduleName}::e.in_stock")}}</option>--}}
{{--                                <option value="out_of_stock" {{ !empty($filterParams) && array_key_exists('stock', $filterParams) && $filterParams['stock'] == 'out_of_stock' ? 'selected' : '' }}>{{__("{$moduleName}::e.out_of_stock")}}</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                @if($brands->isNotEmpty())--}}
{{--                    <div class="field-row">--}}
{{--                        <div class="label-wrap">--}}
{{--                            <label for="brands">{{__("{$moduleName}::e.brands")}}</label>--}}
{{--                        </div>--}}
{{--                        <div class="field-wrap">--}}
{{--                            <select autocomplete="off" name="brands[]" id="brands" class="select2" multiple>--}}
{{--                                @foreach($brands as $brand)--}}
{{--                                    <option value="{{$brand->id}}" {{ !empty($filterParams) && array_key_exists('brands', $filterParams) && is_array($filterParams['brands']) && in_array($brand->id, $filterParams['brands']) ? 'selected' : '' }}>{{$brand->name}}</option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endif--}}

{{--                @if($promotions->isNotEmpty())--}}
{{--                    <div class="field-row">--}}
{{--                        <div class="label-wrap">--}}
{{--                            <label for="promotions">{{__("{$moduleName}::e.promotions")}}</label>--}}
{{--                        </div>--}}
{{--                        <div class="field-wrap">--}}
{{--                            <select autocomplete="off" name="promotions[]" id="promotions" class="select2" multiple>--}}
{{--                                @foreach($promotions as $promotion)--}}
{{--                                    <option value="{{$promotion->id}}" {{ !empty($filterParams) && array_key_exists('promotions', $filterParams) && is_array($filterParams['promotions']) && in_array($promotion->id, $filterParams['promotions']) ? 'selected' : '' }}>{{@$promotion->globalName->name}}</option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endif--}}

{{--                @if($stores->isNotEmpty())--}}
{{--                    <div class="field-row">--}}
{{--                        <div class="label-wrap">--}}
{{--                            <label for="store">{{__("{$moduleName}::e.store")}}</label>--}}
{{--                        </div>--}}
{{--                        <div class="field-wrap">--}}
{{--                            <select autocomplete="off" name="store[]" id="store" class="select2" multiple>--}}
{{--                                <option value=""></option>--}}
{{--                                <option value="-1" @if(@$filterParams['store'] == -1) selected @endif>{{__("{$moduleName}::e.in_all_stores")}}</option>--}}
{{--                                @foreach($stores as $item)--}}
{{--                                    <option value="{{$item->id}}" {{ !empty($filterParams) && array_key_exists('store', $filterParams) && is_array($filterParams['store']) && in_array($item->id, $filterParams['store']) ? 'selected' : '' }}>{{@$item->globalName->name}}</option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endif--}}

{{--                <div class="field-row">--}}
{{--                    <div class="label-wrap">--}}
{{--                        <label for="start_date">{{__("{$moduleName}::e.from_date")}}</label>--}}
{{--                    </div>--}}
{{--                    <div class="field-wrap">--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="field-row">--}}
{{--                    <div class="label-wrap">--}}
{{--                        <label for="start_date">{{__("{$moduleName}::e.from_date")}}</label>--}}
{{--                    </div>--}}
{{--                    <div class="field-wrap">--}}
{{--                        <input autocomplete="off" name="start_date" id="start_date" class="datetimepicker"--}}
{{--                               value="{{ !empty($filterParams) && array_key_exists('start_date', $filterParams) ? date('d-m-Y', strtotime($filterParams['start_date'])) : '' }}">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="field-row">--}}
{{--                    <div class="label-wrap">--}}
{{--                        <label for="end_date">{{__("{$moduleName}::e.to_date")}}</label>--}}
{{--                    </div>--}}
{{--                    <div class="field-wrap">--}}
{{--                        <input autocomplete="off" name="end_date" id="end_date" class="datetimepicker"--}}
{{--                               value="{{ !empty($filterParams) && array_key_exists('end_date', $filterParams) ? date('d-m-Y', strtotime($filterParams['end_date'])) : '' }}">--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
            <div class="field-btn">
                <button class="btn btn-inline half submit-form-btn" data-form-id="filter-form"
                        data-form-event="submit-form">{{__("{$moduleName}::e.filter")}}
                </button>
                <button class="btn btn-inline half submit-form-btn" data-form-id="filter-form"
                        data-form-event="refresh-form">{{__("{$moduleName}::e.reset")}}
                </button>
            </div>
        </form>
    </div>
@endif