<form method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form" enctype="multipart/form-data" class="products">

    <div class="container">
        <div class="row">
            <div class="col large">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="lang">{{__("{$moduleName}::e.lang")}}*</label>
                    </div>
                    <div class="field-wrap">
                        @if(!$langList->isEmpty())
                            <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                                @foreach($langList as $lang)
                                    <option value="{{$lang->id}}" {{ $lang->id == @$langId ? 'selected' : ''}}>{{$lang->name ?: ''}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="iiko_product">{{__("{$moduleName}::e.iiko_product")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <select autocomplete="off" name="iiko_id" id="iiko_id" class="select2">
                            <option value="0">{{__("{$moduleName}::e.iiko_product")}}</option>
                            @foreach($iikoProducts as $iikoproduct)
{{--                                @if($iikoproduct['type'] == 'modifier')--}}
                                    <option value="{{$iikoproduct['id']}}" @if(@$item->iiko_id == $iikoproduct['id']) selected @endif> #{{$iikoproduct['code']}} {{$iikoproduct['name']}} </option>
{{--                                @endif--}}
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col large">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__("{$moduleName}::e.title_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$item->itemByLang->name}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="slug">{{__("{$moduleName}::e.slug_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="slug" id="slug" value="{{@$item->slug}}">
                    </div>
                </div>
            </div>
            <div class="col large">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="price">{{__("{$moduleName}::e.topping_price")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="price" id="price" value="{{@$item->price}}" type="number">
                    </div>
                </div>
            </div>
        </div>

{{--        <div class="field-row">--}}
{{--            <div class="label-wrap">--}}
{{--                <label for="description">{{__("{$moduleName}::e.description")}}</label>--}}
{{--            </div>--}}
{{--            <div class="field-wrap">--}}
{{--                <textarea name="description" id="description" data-type="ckeditor">{!! @$item->itemByLang->description !!}</textarea>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>

    <button class="button blue submit-form-btn"data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">
        {{__("{$moduleName}::e.save_it")}}
    </button>
</form>

