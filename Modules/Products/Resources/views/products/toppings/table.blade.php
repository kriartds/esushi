@if(!$items->isEmpty())
    <table class="table {{@$isTrash ? 'trash' : ''}}" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="align-center">{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.title_table")}}</th>
            @if(!@$isTrash && $permissions->edit)
                <th>{{__("{$moduleName}::e.edit_table")}}</th>
            @endif
            @if(!@$isTrash && $permissions->active)
                <th>{{__("{$moduleName}::e.active_table")}}</th>
            @endif
            @if(!@$isTrash)
            <th>{{__("{$moduleName}::e.position_table")}}</th>
            @endif
            @if($permissions->delete)
                <th class="checkbox-all align-center" data-event="{{@$isTrash ? 'from' : 'to'}}-trash">
                    <div>Select All</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">{{$item->id}}</td>
                <td class="prod-name left">{{@$item->globalName->name}}</td>

                @if(!@$isTrash && $permissions->edit)
                    <td class="td-link">
                        @foreach($langList as $lang)
                            <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $lang->id]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}" {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                        @endforeach
                    </td>
                @endif
                @if(!@$isTrash && $permissions->active)
                    <td class="status">
                            <span class="activate-item {{$item->active ? 'active' : ''}}"
                                  data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                                  data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>

                    </td>
                @endif
                @if(!@$isTrash)
                    <td class="position">
                        <img src="{{asset('admin-assets/img/small-icons/position.svg')}}" alt="">
                    </td>
                @endif
                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]" value="{{$item->id}}"
                               data-event="{{@$isTrash ? 'from' : 'to'}}-trash">
                        <label for="{{$item->id}}">
                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
