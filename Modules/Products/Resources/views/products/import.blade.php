@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')
}
@section('container')

    {!! helpers()->getAdminBreadcrumbs($currComponent, '') !!}
    <div class="container">
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'import','trash'], 'item' => null])

        <div class="import-table">
            <table>
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>A</th>
                        <th>B</th>
                        <th>C</th>
                        <th>D</th>
                        <th>E</th>
                        <th>F</th>
                        <th>G</th>
                        <th>H</th>
                        <th>I</th>
                        <th>J</th>
                        <th>K</th>
                        <th>L</th>
                        <th>M</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="count">1</td>
                        <td>ID</td>
                        <td>Product type(s,v)</td>
                        <td>Product SKU</td>
                        <td>Name_en</td>
                        <td>Name_ro</td>
                        <td>Categories</td>
                        <td>Brand</td>
                        <td>Price</td>
                        <td>Sale price</td>
                        <td>Use stock(1/10)</td>
                        <td>Stock status(i,o)</td>
                        <td>Stock qty</td>
                        <td>Files</td>
                    </tr>
                    <tr>
                        <td class="count" title="2">2</td>
                        <td title="7">7</td>
                        <td title="v">v</td>
                        <td title="1470000000000">1470000000000</td>
                        <td title="Wooden table for bedroom with black legs">Wooden table for bedroom with black legs</td>
                        <td title="Masă din lemn pentru dormitor cu picioare negr">Masă din lemn pentru dormitor cu picioare negr</td>
                        <td title="1,2,3,4,5,6,7,8,9">1,2,3,4,5,6,7,8,9</td>
                        <td title="Apple">Apple</td>
                        <td title="12056.65">12056.65</td>
                        <td title="19.32">19.32</td>
                        <td title="0">0</td>
                        <td title="i">i</td>
                        <td title="92">92</td>
                        <td title="https://telegram.com/files/12.pdf">https://telegram.com/files/12.pdf</td>
                    </tr>
                    
                </tbody>
            </table>
        </div>

        <div class="rules-for-import">
            <h3>Reguli generale:</h3>
            <p>&nbsp;</p>
            <p>- Toate coloanele trebuie să fie prezente în document, în ordinea indicată mai jos. Chiar dacă o coloană nu se completează, ea se lasă goală însă se păstrează spaţiul pentru ea. Excepţie este doar coloana <b>Translation</b>, apariţia şi completarea căreia este descrisă mai jos.</p>
            <p>&nbsp;</p>
            <p>- Obligatoriu se completează următoarele coloane:</p>
            <ul>
                <li><b>Denumire</b></li>
                <li><b>Produse cu variaţii sau simple</b> ( În cazul în care coloana se completează cu v, obligatoriu se completează una dintre coloanele Mărimile produsului sau Culoare cu cel puţin 1 atribut ) </li>
                <li><b>Preţ</b></li>
                <li><b>Slug-ul categoriei</b></li>
            </ul>
            <p>&nbsp;</p>
            <p>- Documentul se importează doar în formatul .csv </p>


            <p>&nbsp;</p>
            <p><b>Denumire</b>  - denumirea produsului, ex. <span>Carmen 10 Den Descriere</span> </p>
            <p>&nbsp;</p>
            <p><b>Descrierea</b>   - descrierea produsului, ex. <span>Ciorapi de damă ultrasubţiri, transparenţi, parfumaţi. Conţinutul Lycra asigură mulare perfectă, iar Multifibra face ciorapii moi şi gingaşi.</span> </p>
            <p>&nbsp;</p>
            <p><b>Specificaţii</b>  - specificaţiile produsului, ex. <span>Dantela siliconata;Aspect semi-mat, catifelat;Vīrfuri transparente;</span> </p>
            <p>&nbsp;</p>
            <p><b>Conţinut</b> - din ce este compus produsul, fiecare punct trebuie urmat de ";", inclusiv ultimul punct, ex. <span>PA Multifibra 80%;Lycra Invista 20%;</span> </p>
            <p>&nbsp;</p>
            <p><b>Denumirea imaginii</b> - imaginea atribuită produsului. !!!Nu trebuie să se repete, ex.  <span>carmen10den.png</span> </p>
            <p>&nbsp;</p>
            <p><b>Stoc</b> - informaţie referitoare la stoc, ex. 9000 Preţ - preţul produsului. !!!Pentru a indica şi zecimalele, trebuie folosit "." ex. <span>255.75</span></p>
            <p>&nbsp;</p>
            <p><b>Preţ </b> - preţul produsului. !!!Pentru a indica şi zecimalele, trebuie folosit "." <span>ex. 255.75</span> </p>
            <p>&nbsp;</p>
            <p><b>Preţ la reducere</b> - preţul produsului la reducere. !!!Pentru a indica şi zecimalele, trebuie folosit "." ex. <span>223.45</span> </p>
            <p>&nbsp;</p>
            <p><b>Produse cu variaţii sau simple</b> -  produsele cu variaţii sunt cele care au o diversitate în culori şi/sau mărimi, celelalte sunt simple. Exemplu de produse variate sunt ciorapii - pot fi alese diferite măsuri sau culori; exemplu de produs simplu este o faţă de masă, care are o mărime şi o culoare stabilită, iar utilizatorului nu i se cere alegerea unor parametri adiţionali. Pentru produsele simple, caseta se completează cu "s", pentru cele cu variaţii cu "v". Ex. <span>v</span> </p>
            <p>&nbsp;</p>
            <p><b>Slug-ul categoriei </b> - se completează cu slug-urile categoriilor cum au fost înregistrată în panela de administrare, în cazul în care sunt selectate mai multe categorii, se separă prin ";" (ultima categorie nu trebuie să fie urmată de ";"), fără spaţii între ele. Slug-ul trebuie să conţină doar litere mici, latine (fără diacritice şi nu în limba rusă), în loc de spaţii trebuie folosit minus "-". La completarea acestei casete, trebuie să se includă toată ierarhia. Ierarhia şi slug-ul corect al unei categorii poate fi aflat prin următoarele modalităţi: 
                <ul>
                    <li> Din <a href="">panela de administrare</a> - în drept cu categoria dorită se află slug-ul acestei categorii </li>
                    <li>Din documentul unde se indică <a href="">ierarhia categoriilor prin intermediul slug-urilor</a> - <a href="">https://ponti.cherry.agency/en/export-categories</a> </li>
                    <li> Din <a href="">link-ul</a> paginii categoriei dorite </li>
                </ul>
                Slug-ul categoriei nu se schimbă în dependenţă de limba în care se adaugă produsele, ci este acelaşi slug pentru toate traducerile. În cazul scrierii incorecte a slug-ului sau scrierea unui slug care nu este încă înregistrat, produsului nu-i va fi atribuit această categorie. Ex. <span> ponti-home;bucataria;saculete-pentru-pastrare;terasa-and-bbq;saculete-pentru-pastrare-terasa</span>
            </p>
            <p>&nbsp;</p>
            <p><b>Meta name </b> - este folosit pentru SEO şi poate fi completat fie prin indicarea numelui produsului şi a câtorva caracteristici adiţionale, fie caseta poate fi păstrată goală - în ultimul caz, în bază automat acest câmp va fi completat cu numele produsului, ex. <span>Carmen 10 den - colanţi de damă Ponti pentru sezonul cald</span> </p>
            <p>&nbsp;</p>
            <p><b>Meta keywords</b> - constituie cuvintele cheie care descriu produsul. Câmpul dat la fel este folosit pentru SEO şi contribuie la eficientizarea căutării produselor, ex. <span>ciorapi, ponti, ciorapi lycra, ciorapi parfumaţi, ciorapi subţiri, ciorapi femei</span> </p>
            <p>&nbsp;</p>
            <p><b>Meta description</b> - la fel ca şi Meta Name, poate fi completat cu o descriere mai atractivă a produsului sau poate fi lăsat gol - în ultimul caz în bază automat acest câmp va fi completat cu descrierea produsului, ex. Ciorapi de damă ultrasubţiri, transparenţi, parfumaţi. Conţinutul Lycra asigura mulare perfectă, iar Multifibra face ciorapii moi şi gingaşi.</p>
            <p>&nbsp;</p>
            <p><b>ID-urile promoţiilor</b> - este completat în cazul în care produsul se află la promoţii, ex. <span>4;23;27</span> </p>
            <p>&nbsp;</p>
            <p><b> Greutatea produsului</b> - acă se cunoaşte, se completează în kg, ex. <span>0.342</span> </p>
            <p>&nbsp;</p>
            <p><b>Translations</b> - această coloană este opţională şi se adaugă doar atunci cînd se încarcă traducerile pentru produsele deja existente în limba română. Modul de completare a tabelului în acest caz este următorul:
                <ol>
                    <li>Se asigură că există aceste produse în limba română.</li>
                    <li>Coloana Denumirea este completată cu denumirea produsului în limba română, ex. <span>Decorațiuni lavanda</span> </li>
                    <li>Între coloanele Greutatea produsului şi Mărimile produsului se adaugă o coloană cu titlul Translation</li>
                    <li>Denumirea în limba actuală a produsului se completează în caseta Translation , ex. <span>Lavender Decorations</span> </li>
                    <li> Toate celelalte casete se completează în mod obişnuit, în limba necesară (în afară de slug-uri - ele rămân identice pentru toate limbile)</li>
                </ol>
            </p>
            <p>&nbsp;</p>
            <p><b>Mărimile produsului</b> - este completat cu slug-ul atributului, ex. <span>marime</span>;, după care urmează slug-urile mărimilor produsului. Slug-urile mărimilor trebuie indicate exact cum sunt în lista de mărimi - https://ponti.cherry.agency/ro/admin/attributes?parent=27 - doar în latină (fără diacritice şi nu în limba rusă), cu litere mici, în loc de spaţii trebuie folosit minus "-", se separă prin ";" (ultimul atribut nu trebuie să fie urmat de ";"), fără spaţii între ele. În cazul scrierii incorecte a slug-ului sau scrierea unui slug care nu este încă înregistrat, produsului nu-i va fi atribuit această mărime. Ex. <span>marime;120x140;140x140;170x140;140x190;marimea-poate-devia</span> </p>
            <p>&nbsp;</p>
            <p><b>Culoare</b> - este completat cu slug-ul atributului, ex. <span>culoare</span>;, după care urmează slug-urile culorilor produsului. Slug-urile culorilor trebuie indicate exact cum sunt în lista de culori - https://ponti.cherry.agency/ro/admin/attributes?parent=208 - doar în latină (fără diacritice şi nu în limba rusă), cu litere mici, în loc de spaţii trebuie folosit minus "-", se separă prin ";" (ultimul atribut nu trebuie să fie urmat de ";"), fără spaţii între ele. În cazul scrierii incorecte a slug-ului sau scrierea unui slug care nu este încă înregistrat, produsului nu-i va fi atribuit această culoare. Ex. <span>culoare;monocrom;cu-dantela;cu-imprimeu;rosu;visiniu;negru</span> </p>
        </div>

        <div class="import-footer">
            <div class="left-col">
                <form method="get" action="{{adminUrl([$currComponent->slug, 'importProducts'])}}" id="importForm" enctype="multipart/form-data">
                    <input type="file" accept=".csv, text/csv" name="file" id="import">
                </form>
                <label class="button blue" for="import">UPLOAD PRODUCTS FILE</label>
            </div>

            <div class="right-col">
                <svg xmlns="http://www.w3.org/2000/svg" width="33.086" height="29" viewBox="0 0 33.086 29">
                    <g id="Icon_feather-alert-triangle" data-name="Icon feather-alert-triangle" transform="translate(-0.787 -2.846)">
                        <path id="Path_76" data-name="Path 76" d="M14.873,5.729,2.713,26.037a2.873,2.873,0,0,0,2.455,4.309h24.32a2.873,2.873,0,0,0,2.455-4.309L19.783,5.729a2.871,2.871,0,0,0-4.91,0Z" transform="translate(0 0)" fill="none" stroke="#ea5355" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                        <path id="Path_77" data-name="Path 77" d="M18,13.5v6" transform="translate(-0.672 -0.499)" fill="none" stroke="#ea5355" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                        <path id="Path_78" data-name="Path 78" d="M18,25.5h0" transform="translate(-0.672 -0.899)" fill="none" stroke="#ea5355" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                    </g>
                </svg>
                <p>Attention! The action is irreversible! <br> If the uploaded file have some errors and it influences negatively the current content on the website - the initial content can’t be returned.</p>
            </div>

            <div class="input-file-name">
                <p title=""></p>
                <div class="delete-file">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><g><g><g><path fill="#ea5355" d="M0 9a9 9 0 1 1 18 0A9 9 0 0 1 0 9z"></path></g><g><g><path fill="none" stroke="#fff" stroke-miterlimit="50" stroke-width="2" d="M5 5l8 8"></path></g><g><path fill="none" stroke="#fff" stroke-miterlimit="50" stroke-width="2" d="M13 5l-8 8"></path></g></g></g></g></svg>
                    Remove
                </div>

                <div class="upload-file">
                    <svg id="Layer" enable-background="new 0 0 64 64" width="20" height="20" viewBox="0 0 64 64" width="512" xmlns="http://www.w3.org/2000/svg"><path d="m34.718 51.81c-1.769.254-3.667.254-5.436 0-1.095-.145-2.106.604-2.262 1.698s.604 2.106 1.698 2.263c1.071.152 2.175.229 3.282.229s2.211-.077 3.282-.229c1.093-.156 1.854-1.169 1.698-2.263-.156-1.093-1.168-1.842-2.262-1.698z"/><path d="m21.09 53.381c.981.5 2.188.113 2.691-.871.503-.983.113-2.188-.871-2.691-6.729-3.44-10.91-10.268-10.91-17.819 0-11.028 8.972-20 20-20s20 8.972 20 20c0 7.551-4.181 14.379-10.91 17.818-.984.503-1.374 1.708-.871 2.691.354.692 1.055 1.09 1.782 1.09.307 0 .617-.07.909-.219 8.074-4.127 13.09-12.319 13.09-21.38 0-13.233-10.766-24-24-24s-24 10.767-24 24c0 9.061 5.016 17.253 13.09 21.381z"/><path d="m40 28c.512 0 1.024-.195 1.414-.586.781-.781.781-2.047 0-2.828l-8-8c-.78-.781-2.048-.781-2.828 0l-8 8c-.781.781-.781 2.047 0 2.828.78.781 2.048.781 2.828 0l4.586-4.586v23.172c0 1.104.896 2 2 2s2-.896 2-2v-23.172l4.586 4.586c.39.391.902.586 1.414.586z"/></svg>
                    Upload
                </div>
            </div>

        </div>
    </div>
@stop
@include('admin.footer')