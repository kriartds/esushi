<form class="promotions-form" method="POST" action="{{url(LANG, ['admin', $currComponent->slug, 'save', @$item->id, @$langId ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form"
      enctype="multipart/form-data">
    <div class="container">
        <div class="row">
            <div class="col middle">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="lang">{{__("{$moduleName}::e.lang")}}*</label>
                    </div>
                    <div class="field-wrap">
                        @if(!$langList->isEmpty())
                            <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                                @foreach($langList as $lang)
                                    <option value="{{$lang->id}}" {{ (is_null(@$langId) && $lang->id == LANG_ID ? 'selected' : $lang->id == @$langId ) ? 'selected' : ''}}>{{$lang->name ?: ''}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col middle">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="start_date">{{__("{$moduleName}::e.start_promo_date")}}</label>
                    </div>
                    <div class="field-wrap date">
                        <input name="start_date" id="start_date" class="datetimepicker"
                            value="{{strtotime(@$item->start_date) ? date('d-m-Y', strtotime($item->start_date)) : ''}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="end_date">{{__("{$moduleName}::e.end_promo_date")}}</label>
                    </div>
                    <div class="field-wrap date">
                        <input name="end_date" id="end_date" class="datetimepicker"
                            value="{{strtotime(@$item->end_date) ? date('d-m-Y', strtotime($item->end_date)) : ''}}">
                    </div>
                </div>
            </div>
            <div class="col middle">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__("{$moduleName}::e.title_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$item->itemByLang->name}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="slug">{{__("{$moduleName}::e.slug_table")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="slug" id="slug" value="{{@$item->slug}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="field-row">
            <div class="label-wrap">
                <label for="description">{{__("{$moduleName}::e.description")}}</label>
            </div>
            <div class="field-wrap">
                <textarea name="description" id="description"
                        data-type="ckeditor">{!! @$item->itemByLang->description !!}</textarea>
            </div>
        </div>
        <div class="field-row">
            @include('admin.templates.uploadFile', [
                'item' => @$item,
                'options' => [
                    'data-component-id' => $currComponent->id,
                    'data-types' => 'image'
                ]
            ])
        </div>
    </div>

    <div class="meta-conatiner">
        <div class="field-row">
            <div class="label-wrap">
                <label for="meta_title">{{__("{$moduleName}::e.meta_title_page")}}</label>
            </div>
            <div class="field-wrap">
                <input name="meta_title" id="meta_title" value="{{@$item->itemByLang->meta_title}}">
            </div>
        </div>
        <div class="field-row">
            <div class="label-wrap">
                <label for="meta_keywords">{{__("{$moduleName}::e.meta_keywords_page")}}</label>
            </div>
            <div class="field-wrap">
                <input name="meta_keywords" id="meta_keywords" value="{{@$item->itemByLang->meta_keywords}}">
            </div>
        </div>
        <div class="field-row">
            <div class="label-wrap">
                <label for="meta_description">{{__("{$moduleName}::e.meta_description_page")}}</label>
            </div>
            <div class="field-wrap">
                <textarea name="meta_description"
                        id="meta_description">{!! @$item->itemByLang->meta_description !!}</textarea>
            </div>
        </div>
    </div>

    <button class="button blue submit-form-btn"
            data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__("{$moduleName}::e.save_it")}}</button>
</form>

