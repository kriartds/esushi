@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent, new \Modules\Products\Models\ProductsPromotionsId(),@request()->get('parent')) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'trash'], 'item' => null, 'currentPage' => 'trash'])

        <div class="table-block">
            @if(!$items->isEmpty())
                <table class="table trash">
                    <thead>
                    <tr>
                        <th class="align-center">{{__("{$moduleName}::e.id_table")}}</th>
                        <th class="left">{{__("{$moduleName}::e.title_table")}}</th>
                        <th class="left">Slug</th>
                        <th class="align-center">Start Date / End Date</th>
                        @if($permissions->edit)
                            <th class="align-center">{{__("{$moduleName}::e.edit_table")}}</th>
                        @endif
                        @if($permissions->delete)
                            <th class="checkbox-all align-center">
                                <div>Select All</div>
                            </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td class="id">
                                <p>{{$item->id}}</p>
                            </td>
                            <td class="title left">
                                <p>{{@$item->globalName->name}}</p>
                            </td>
                            <td class="title left">
                                <p>{{@$item->slug}} </p>
                            </td>
                            <td class="start-exp-day align-center ">
                                <span class="start-day trash">{{@$item->start_date->format('d-m-Y')}}</span>
                                /
                                <span class="exp-day trash">{{@$item->end_date ? @$item->end_date->format('d-m-Y') : '-'}}</span>
                            </td>
                            <td class="td-link ">
                                @foreach($langList as $lang)
                                    <a class="trash" {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                                @endforeach
                            </td>
                            @if($permissions->delete)
                                <td class="checkbox-items">
                                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                           name="checkbox_items[{{$item->id}}]"
                                           value="{{$item->id}}">
                                    <label for="{{$item->id}}">
                                    </label>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')