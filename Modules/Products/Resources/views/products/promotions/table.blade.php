@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="align-center">{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.title_table")}}</th>
            <th class="left">Slug</th>
            <th class="left">Promotion’s Products </th>
            <th class="align-center">Start Date / End Date</th>
            @if($permissions->active)
                <th class="align-center">{{__("{$moduleName}::e.active_table")}}</th>
            @endif
            @if($permissions->edit)
                <th class="align-center">{{__("{$moduleName}::e.edit_table")}}</th>
            @endif
            @if($permissions->delete)
                <th class="checkbox-all align-center" >
                    <div>Select All</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>
                <td class="title left">
                    <p> <a @if($permissions->edit) href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, DEF_LANG_ID]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}" @endif>{{@$item->globalName->name}}</a></p>
                </td>
                <td class="title left">
                    <p>{{@$item->slug}} </p>
                </td>

                <td class="categories left">
                    @if($item->products->count())
                        <a href="{{adminUrl(['products']).'?promotions=['.$item->id.']'}}">{{$item->products->count()}} Products</a>
                    @else
                        -
                    @endif
                </td>
                <td class="start-exp-day align-center">
                    <span class="start-day">{{@$item->start_date->format('d-m-Y')}}</span>
                    /
                    <span class="exp-day">{{@$item->end_date ? @$item->end_date->format('d-m-Y') : '-'}}</span>
                </td>

                @if($permissions->active)
                    <td class="status">
                        <span class="activate-item {{$item->active ? 'active' : ''}}"
                              data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                              data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>
                    </td>
                @endif

                @if($permissions->edit)
                    <td class="td-link">
                        @foreach($langList as $lang)
                            <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $lang->id]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}" {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                        @endforeach
                    </td>
                @endif

                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}"

                               >
                        <label for="{{$item->id}}">
                            <!-- <span>Select</span> -->
                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
