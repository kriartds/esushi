@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}


        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create','import', 'trash'], 'item' => null, 'currentPage' => 'trash'])

        <div class="table-block">
            @include("{$path}.table", ['items' => $items, 'componentSlug' => $currComponent->slug, 'isTrash' => true])
        </div>
    </div>
@stop

@include('admin.footer')