@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'trash'], 'item' => null,  'currentPage'=> 'trash'])

        <div class="table-block">
            @if(!$items->isEmpty())
                <table class="table">
                    <thead>
                    <tr>
                        <th class="align-center">{{__("{$moduleName}::e.id_table")}}</th>
                        <th>User</th>
                        <th>{{__("{$moduleName}::e.product")}}</th>
                        <th class="align-center">Rating</th>
                        <th class="align-center">Date</th>
                        @if($permissions->delete)
                            <th class="checkbox-all align-center" >
                                <div>Select All</div>
                            </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td class="id">
                                <p>{{$item->id}}</p>    
                            </td>
                            <td>
                                <p>{{@$item->user->name ?: '-'}}</p>
                                <p>{{@$item->user->email ?: '-'}}</p>
                            </td>
                            <td class="title">
                                <p>{{@$item->product->globalName->name ?: '-'}}</p>
                            </td>
                            <td class="rating">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12"><g><g><path fill="#ffaf54" d="M5.356.4L3.892 3.37l-3.277.477a.718.718 0 0 0-.397 1.225l2.37 2.31-.56 3.264a.717.717 0 0 0 1.04.755L6 9.861l2.932 1.54a.718.718 0 0 0 1.04-.755l-.56-3.264 2.37-2.31a.718.718 0 0 0-.397-1.225L8.108 3.37 6.644.4a.718.718 0 0 0-1.288 0z"/></g></g></svg>
                                {{$item->rating}}
                            </td>
                            <td class="align-center">{{strtotime($item->created_at) ? date('d-m-Y', strtotime($item->created_at)) : '-'}}</td>
                            @if($permissions->delete)
                                <td class="checkbox-items">
                                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                           name="checkbox_items[{{$item->id}}]"
                                           value="{{$item->id}}">
                                    <label for="{{$item->id}}">

                                    </label>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')