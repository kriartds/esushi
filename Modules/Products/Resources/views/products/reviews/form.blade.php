<form class="review-view" id="{{!is_null($item) ? 'edit' : 'create'}}-form">

    <div class="container">
        <div class="row">
            <div class="col large">
                <div class="field-row">
                    <div class="label-wrap">
                        <label>{{__("{$moduleName}::e.product")}}</label>
                    </div>
                    <div class="field-wrap inline">
                        <span class="id">{{@$item->products_item_id}}</span>
                        <a href="{{customUrl(['admin', 'products', 'edit', @$item->products_item_id, DEF_LANG_ID])}}"
                        target="_blank">{{@$item->product->globalName->name}}</a>
                    </div>
                </div>
            </div>
            <div class="col large">
                <div class="field-row">
                    <div class="label-wrap">
                        <label>{{__("{$moduleName}::e.user")}}</label>
                    </div>
                    <div class="field-wrap">
                        @if(@$item->user->email)
                            <a href="sendmail:{{@$item->user->email}}">
                                {{@$item->user->email}}
                            </a>
                        @endif
                        <a href="{{customUrl(['admin', 'site-users', 'edit', @$item->user_id])}}"
                        target="_blank">
                            {{@$item->user->name}}
                        </a>
                    </div>
                </div>
            </div>
            <div class="col small">
                <div class="field-row">
                    <div class="label-wrap align-center">
                        <label for="rating">{{__("{$moduleName}::e.rating")}}</label>
                    </div>
                    <div class="field-wrap inline rating">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12"><g><g><path fill="#ffaf54" d="M5.356.4L3.892 3.37l-3.277.477a.718.718 0 0 0-.397 1.225l2.37 2.31-.56 3.264a.717.717 0 0 0 1.04.755L6 9.861l2.932 1.54a.718.718 0 0 0 1.04-.755l-.56-3.264 2.37-2.31a.718.718 0 0 0-.397-1.225L8.108 3.37 6.644.4a.718.718 0 0 0-1.288 0z"/></g></g></svg>
                        <p>{{@$item->rating}}</p>
                    </div>
                </div>
            </div>
            <div class="col middle">
                <div class="field-row">
                    <div class="label-wrap align-center">
                        <label for="date">{{__("{$moduleName}::e.date")}}</label>
                    </div>
                    <div class="field-wrap inline">
                        <p>{{strtotime($item->created_at) ? date('d-m-Y', strtotime($item->created_at)) : date('d-m-Y')}}</p>
                    </div>
                </div>
            </div>
            <div class="col middle">
                <div class="field-row">
                    <div class="label-wrap align-center">
                        <label for="ip">{{__("{$moduleName}::e.ip")}}</label>
                    </div>
                    <div class="field-wrap inline">
                        <p>{{!is_null(@$item->ip) ? $item->ip : request()->ip()}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="comment-details">
        @if(config('cms.product.reviews.addAdvantages', true))
            <div class="field-row">
                <div class="label-wrap">
                    <label for="advantage">{{__("{$moduleName}::e.advantage")}}</label>
                </div>
                <div class="field-wrap">
                    <p>{{@$item->message->advantage}}</p>
                </div>
            </div>
        @endif
        @if(config('cms.product.reviews.addDisadvantages', true))
            <div class="field-row">
                <div class="label-wrap">
                    <label for="disadvantage">{{__("{$moduleName}::e.disadvantage")}}</label>
                </div>
                <div class="field-wrap">

                    <p>{{@$item->message->disadvantage}}</p>
                </div>
            </div>
        @endif
        <div class="field-row">
            <div class="label-wrap">
                <label for="message">{{__("{$moduleName}::e.comment_table")}}</label>
            </div>
            <div class="field-wrap">
                <p>{{@$item->message->msg}}</p>
            </div>
        </div>
    </div>


</form>

