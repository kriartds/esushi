@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent, new \Modules\Products\Models\ProductsItemsReviews(),@request()->get('parent')) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'trash'], 'item' => null, 'filter' => ['path' => $path, 'filterParams' => $filterParams, 'items' => $items], 'actionButton'=>true])

        <div class="table-block">
            @include("{$path}.table", ['items' => $items, 'componentSlug' => $currComponent->slug])
        </div>

    </div>

@stop

@include('admin.footer')