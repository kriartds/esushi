@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent, new \Modules\Products\Models\ProductsItemsReviews(),@request()->get('parent')) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'trash', !is_null(@$item) ? 'view' : ''], 'item' => @$item, 'langId' => @$langId])

        <div class="form-block">
            @include("{$path}.form", ['item' => @$item])
        </div>
    </div>

@stop

@include('admin.footer')