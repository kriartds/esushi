@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="align-center">{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">User</th>
            <th class="left">{{__("{$moduleName}::e.product")}}</th>
            <th class="left">Comment</th>
            <th class="align-center">{{__("{$moduleName}::e.rating")}}</th>
            <th class="align-center">{{__("{$moduleName}::e.date")}}</th>
            @if($permissions->active)
                <th class="align-center">{{__("{$moduleName}::e.active_table")}}</th>
            @endif
            @if($permissions->delete)
                <th class="checkbox-all align-center" >
                    <div>Select All</div>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td class="id">
                    <p>{{$item->id}}</p>
                </td>
                <td class="title left">
                    @if($permissions->view)
                        <a href="{{url(LANG, ['admin', $currComponent->slug, 'view', $item->id])}}">{{@$item->user->name ?: '-'}}</a>
                        <a href="{{url(LANG, ['admin', $currComponent->slug, 'view', $item->id])}}">{{@$item->user->email ?: '-'}}</a>
                    @else
                        <p>{{@$item->user->name ?: '-'}}</p>
                        <p>{{@$item->user->email ?: '-'}}</p>
                    @endif
                </td>


                <td class="title left">
                    <a href="{{customUrl(['products',@$item->product()->first()->slug])}}">{{@$item->product->globalName->name ?: '-'}}</a>
                </td>

                <td class="title left">
                    <p>{{str_limit(@$item->message->advantage, '110')}}</p>
                    <p>{{str_limit(@$item->message->disadvantage, '110')}}</p>
                    <p>{{str_limit(@$item->message->msg, '110')}}</p>
                </td>
                <td class="rating">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12"><g><g><path fill="#ffaf54" d="M5.356.4L3.892 3.37l-3.277.477a.718.718 0 0 0-.397 1.225l2.37 2.31-.56 3.264a.717.717 0 0 0 1.04.755L6 9.861l2.932 1.54a.718.718 0 0 0 1.04-.755l-.56-3.264 2.37-2.31a.718.718 0 0 0-.397-1.225L8.108 3.37 6.644.4a.718.718 0 0 0-1.288 0z"/></g></g></svg>
                    {{$item->rating}}
                </td>
                <td class="align-center">{{strtotime($item->created_at) ? date('d-m-Y', strtotime($item->created_at)) : '-'}}</td>
                @if($permissions->active)
                    <td class="status"> 
                        <span class="activate-item {{$item->active ? 'active' : ''}}"
                              data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                              data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>
                    </td>
                @endif
                @if($permissions->delete)
                    <td class="checkbox-items">
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="checkbox_items[{{$item->id}}]"
                               value="{{$item->id}}"

                               >
                        <label for="{{$item->id}}">
                            <!-- <span>Select</span> -->
                        </label>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
@endif
