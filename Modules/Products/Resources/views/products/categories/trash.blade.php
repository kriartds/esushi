@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent, @$parent) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'trash'], 'item' => null, 'currentPage'=> 'trash'])

        <div class="table-block">
            @if(!$items->isEmpty())
                <table class="table">
                    <thead>
                        <tr>
                            <th class="align-center">{{__("{$moduleName}::e.id_table")}}</th>
                            <th>{{__("{$moduleName}::e.title_table")}}</th>
                            <th>Slug</th>
                            <th class="align-center">Languages</th>
                            @if($permissions->delete)
                                <th class="checkbox-all align-center" >
                                    <div>Select All</div>
                                </th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td class="id">
                                <p>{{$item->id}}</p>
                            </td>
                            <td class="prod-name">
                                <p>{{@$item->globalName->name}}</p>
                            </td>
                            <td class="slug">
                                <p>{{$item->slug}}</p>
                            </td>
                            @if($permissions->edit)
                                <td class="td-link small">
                                    @foreach($langList as $lang)
                                        <a  {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                                    @endforeach
                                </td>
                            @endif
                            @if($permissions->delete)
                                <td class="checkbox-items">
                                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                           name="checkbox_items[{{$item->id}}]"
                                           value="{{$item->id}}">
                                    <label for="{{$item->id}}">
                                    </label>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__("{$moduleName}::e.list_is_empty")}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')