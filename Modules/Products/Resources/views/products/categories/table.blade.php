@if(!$items->isEmpty())
    <table class="table" data-position-url="{{url(LANG, ['admin', $currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th class="align-center">{{__("{$moduleName}::e.id_table")}}</th>
            <th class="left">{{__("{$moduleName}::e.title_table")}}</th>
            <th class="left">Slug</th>
            <th class="left">
                Category's Products
            </th>
            <th>Children's Categories</th>
            @if($permissions->active)
                <th class="align-center">
                <!-- {{__("{$moduleName}::e.active_table")}} -->
                    Status
                </th>
            @endif
            <th>{{__("{$moduleName}::e.position_table")}}</th>
            @if($permissions->edit)
                <th class="align-center">
                    Languages
                </th>
            @endif
            @if($permissions->delete)
                <th class="checkbox-all align-center">
                    <div>Select All</div>
                </th>
            @endif
        </tr>
        </thead>

        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">

                <td class="id">
                    <p>{{$item->id}}</p>
                </td>

                <td class="prod-name left" {{$item->children->isNotEmpty() ? 'class=td-link' : ''}}>
                    <p>
                        <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, DEF_LANG_ID]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}">{{@$item->globalName->name}}</a>
                    </p>
                </td>

                <td class="slug left">
                    <p>{{$item->slug}}</p>
                </td>
                <td class="left categories" {{$item->products_count > 0 ? 'class=categories' : ''}}>

                        @if($item->products_count > 0)

                            <a href="{{url(LANG, ['admin', 'products']) . "?category=[{$item->id}]"}}"> {{$item->products_count}} Products</a>
                        @else
                            -
                        @endif



                </td>
                <td>
                    @if($item->children->isNotEmpty())
                        <p>
                            <?php $childrens = $item->children->take(2) ?>
                            @foreach($childrens as $key => $children)
                                @if(count($childrens) > 1 && $key < 1)
                                    {{@$children->globalName->name}},
                                @else
                                    {{@$children->globalName->name}}
                                @endif
                            @endforeach
                        </p>
                        <div class="see-option"><a href="{{url(LANG, ['admin', $currComponent->slug]) . "?parent={$item->id}"}}">See List</a></div>
                     @else
                    -
                    @endif

                </td>
                @if($permissions->active)
                    <td class="status"> 
                        <span class="activate-item {{$item->active ? 'active' : ''}}"
                              data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                              data-url="{{url(LANG, ['admin', $currComponent->slug, 'activateItem'])}}"></span>
                    </td>
                @endif

                @if($permissions->edit)
                    <td class="position">
                        <img src="{{asset('admin-assets/img/small-icons/position.svg')}}" alt="">
                    </td>

                    <td class="td-link">
                        @foreach($langList as $lang)
                            <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $lang->id]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}" {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                        @endforeach
                    </td>
                @endif


                @if($permissions->delete)
                    <td class="checkbox-items">
                        @if($item->children->isEmpty() && $item->products_count == 0)
                            <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                   name="checkbox_items[{{$item->id}}]"
                                   value="{{$item->id}}">
                            <label for="{{$item->id}}"></label>
                        @else
                            <svg title="{{__("{$moduleName}::e.item_has_children")}}" class="tooltip"
                                 xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                <g id="Group_360" data-name="Group 360" transform="translate(-1400 -1236)">
                                    <circle id="Ellipse_22" data-name="Ellipse 22" cx="10" cy="10" r="10"
                                            transform="translate(1400 1236)" fill="#9f9f9f"/>
                                    <path id="Path_79" data-name="Path 79"
                                          d="M2.769-3.056,3-9.953H.9l.232,6.9Zm-.82,1.08a1.091,1.091,0,0,0-.8.3,1,1,0,0,0-.3.749.992.992,0,0,0,.3.745,1.1,1.1,0,0,0,.8.294,1.107,1.107,0,0,0,.8-.294.992.992,0,0,0,.3-.745,1,1,0,0,0-.3-.752A1.107,1.107,0,0,0,1.948-1.976Z"
                                          transform="translate(1408 1251)" fill="#fff"/>
                                </g>
                            </svg>
                        @endif
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">
        <p>No categories found. Press the button below to create your first category.</p>

        @if($permissions->create)
            <div class="item">
                <a class="button blue" href="{{adminUrl([$currComponent->slug, 'create']) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}">CREATE CATEGORY</a>
            </div>
        @endif
    </div>
@endif

