<?php

namespace Modules\Products\Jobs;

use App\Models\Languages;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Products\Http\Controllers\ProductsController;
use Modules\Products\Http\Helpers\Helpers as ProductsHelpers;
use Modules\Products\Models\ProductsItemsId;
use Modules\Products\Notifications\ExportProductsNotification;

class ExportProductsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $attributes;
    public $path;
    public $user;
    private $globalSettings;
    private $filterParams;

    /**
     * Create a new job instance.
     * @param $path
     * @param $attributes
     * @param $user
     * @param $globalSettings
     * @param $filterParams
     */
    public function __construct($path, $attributes, $user, $globalSettings, $filterParams)
    {
        $this->path = $path;
        $this->attributes = $attributes;
        $this->user = $user;
        $this->globalSettings = $globalSettings;
        $this->filterParams = $filterParams;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $filePath = $this->path;

        $handler = fopen($filePath, 'w+');
        $fileSize = filesize($filePath);

        $headerArray = $this->generateCsvHeaderContent();

        if ($fileSize == 0)
            fputcsv($handler, $headerArray);

        $productsController = new ProductsController();

        ProductsItemsId::with(['items', 'variationsDetails', 'variationsDetails.variations', 'variationsDetails.items', 'productCategories', 'brand', 'files.file', 'productItemsAttributes', 'productItemsAttributes.attribute', 'productItemsAttributes.attribute.globalName', 'productItemsAttributes.attribute.type', 'productItemsAttributes.options', 'productItemsAttributes.options.attribute', 'productItemsAttributes.options.attribute.globalName'])
            ->where(function ($q) use ($productsController) {
                $productsController->filterQuery($q, $this->filterParams);
            })->orderBy('position')
            ->chunk(200, function ($products) use ($handler) {
                foreach ($products as $product) {
                    $attributes = ProductsHelpers::getProductSelectedAttributes($product, false, false, false);
                    $productData = exportProductsData($product, $attributes);
                    if (@$productData->item) {

                        $content = $this->generateCsvHeaderContent($productData, null, $attributes);

                        fputcsv($handler, $content);

                        if (@$productData->item->isVariationProduct && @$productData->variations->isNotEmpty()) {
                            foreach ($productData->variations->sortByDesc('item.id') as $variation) {

                                $variationContent = $this->generateCsvHeaderContent($productData, $variation, $attributes);

                                fputcsv($handler, $variationContent);
                            }
                        }
                    }
                }
            });

        fclose($handler);

        try {
            $details = [
                'title' => 'Export products from CSV',
                'msg' => 'Your csv file was successful generated. Access this link or check your email.',
                'emailMsg' => 'Export products stock to CSV',
                'linkText' => 'Download file',
                'url' => urldecode(customUrl(['admin', 'products', 'downloadProductsStock']) . '?file=' . basename($filePath)),
                'exportedProductsPath' => basename($filePath)
            ];
            $this->user->notify(new ExportProductsNotification($details, $this->globalSettings));

            $this->user->update([
                'has_exported_products' => 1
            ]);
        } catch (\Exception $e) {}
    }

    /**
     * The job failed to process.
     *
     * @param \Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {

        $this->user->update([
            'has_exported_products' => 0
        ]);

        try {
            $details = [
                'title' => 'Export products from CSV',
                'msg' => 'Your csv file generation was failed. Try again.',
                'emailMsg' => 'Your csv file generation was failed. Try again.',
                'linkText' => 'Go to products list',
                'url' => urldecode(customUrl(['admin', 'products'])),
            ];
            $this->user->notify(new ExportProductsNotification($details, $this->globalSettings));
        } catch (\Exception $e) {}
    }


}
