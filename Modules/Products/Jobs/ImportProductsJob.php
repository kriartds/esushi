<?php

namespace Modules\Products\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\File;
use Modules\Products\Http\Helpers\Helpers as ProductsHelpers;
use Modules\Products\Models\ProductsCategoriesId;
use Modules\Products\Models\ProductsItems;
use Modules\Products\Models\ProductsItemsAttributes;
use Modules\Products\Models\ProductsItemsAttributesOptions;
use Modules\Products\Models\ProductsItemsCategories;
use Modules\Products\Models\ProductsItemsId;
use Modules\Products\Models\ProductsItemsVariations;
use Modules\Products\Models\ProductsItemsVariationsDetails;
use Modules\Products\Models\ProductsItemsVariationsDetailsId;
use Modules\Products\Notifications\ImportProductsNotification;

class ImportProductsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $allAttributes;
    public $langList;
    public $filePath;
    public $fileContent;
    public $user;
    private $headerFile;
    private $globalSettings;
    private $currComponent;

    /**
     * Create a new job instance.
     * @param $filePath
     * @param $fileContent
     * @param $langList
     * @param $allAttributes
     * @param $headerFile
     * @param $user
     * @param $globalSettings
     * @param $currComponent
     */
    public function __construct($filePath, $fileContent, $langList, $allAttributes, $headerFile, $user, $globalSettings, $currComponent)
    {
        $this->filePath = $filePath;
        $this->fileContent = $fileContent;
        $this->langList = $langList;
        $this->allAttributes = $allAttributes;
        $this->headerFile = $headerFile;
        $this->user = $user;
        $this->globalSettings = $globalSettings;
        $this->currComponent = $currComponent;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $errorMsg = [];

        $contentFile = array_slice($this->fileContent, 1);
        $partiallyContentFile = collect(array_map('str_getcsv', $contentFile))
            ->filter(function ($item) {
                return $item[0];
            })
            ->groupBy(function ($item) {
                return $item[0];
            })->chunk(1000);

        $iteration = 1;

        if ($partiallyContentFile->isNotEmpty()) {
            foreach ($partiallyContentFile as $productsDetails) {
                foreach ($productsDetails as $productId => $products) {
                    $iteration++;
                    $mainProduct = @$products->first();
                    $productVariations = $products->slice(1);
                    $product = array_combine($this->headerFile, $mainProduct);
                    $isVariationProduct = @$product['product_typesv'] == 'v';

                    if ($isVariationProduct && $productVariations->isEmpty()) {
                        $errorMsg[$iteration][] = "Product doesn't have variations.";
                        continue;
                    }
                    $categoriesIds = explode(',', @$product['categories'] ?: '');

                    $price = $salePrice = null;
                    if (!$isVariationProduct) {
                        $price = (float)$product['price'] ?: null;
                        $salePrice = (float)$product['sale_price'] ?: null;

                        if ($salePrice > $price)
                            $salePrice = null;
                    }

                    $productNames = filterArrayKeys($product, 'name_');
                    $productDescriptions = filterArrayKeys($product, 'description_');
                    $defaultNameKey = "name_" . DEF_LANG;
                    $slug = @$productNames[$defaultNameKey] ? str_slug($productNames[$defaultNameKey]) : str_slug(str_random());
                    $sku = @$product['product_sku'] ?: -1;

                    $productAttributes = collect();
                    $productAttributesForVariation = [];
                    $productAttributesArr = explode(';', @$product['attributes']);
                    if (!empty($productAttributesArr))
                        foreach ($productAttributesArr as $productAttribute) {
                            $parentChildrenAttr = explode(':', $productAttribute);
                            $parentSlug = @$parentChildrenAttr[0] ?: null;
                            $childrenSlug = explode(',', @str_replace(' ', '', @$parentChildrenAttr[1]) ?: '');

                            if (is_null($parentSlug)) {
                                $errorMsg[$iteration][] = "Product attributes category is empty.";
                                continue;
                            }

                            $parentAttr = $this->allAttributes->where('parent.slug', $parentSlug)->first();

                            if (is_null($parentAttr)) {
                                $errorMsg[$iteration][] = "Product attributes category, `{$parentSlug}`, not exist.";
                                continue;
                            }

                            $childrenAttr = $parentAttr['children']->whereIn('slug', $childrenSlug);

                            if ($childrenAttr->isEmpty() || ($childrenAttr->isNotEmpty() && $childrenAttr->count() != count($childrenSlug))) {
                                $errorMsg[$iteration][] = "Product attributes items, from category `{$parentSlug}`, not exists.";
                                continue;
                            }

                            $productAttributes->push([
                                'parent' => @$parentAttr['parent'],
                                'children' => $childrenAttr
                            ]);
                        }

                    $item = ProductsItemsId::find($productId);
                    $isNewProduct = is_null($item);


                    if (is_null($item)) {
                        $item = new ProductsItemsId();

                        if (!is_null($item->where('slug', $slug)->first())) // exist slug
                            $slug = null;

                        if (!is_null($item->where('sku', $sku)->first())) // exist sku
                            $sku = null;
                    }

                    $incompleteData = empty($productNames) || is_null($slug) || (is_null($sku) && !$isVariationProduct) || (is_null($price) && !$isVariationProduct) || empty($categoriesIds) || ($productAttributes->isEmpty() && $isVariationProduct) ? 1 : 0;


                    /** Incomplete data errors */

                    if (empty($productNames))
                        $errorMsg[$iteration][] = "Product doesn't have name.";

                    if (is_null($slug))
                        $errorMsg[$iteration][] = "Product doesn't have slug or the slug has already been taken.";

                    if (is_null($sku) && !$isVariationProduct)
                        $errorMsg[$iteration][] = "Product doesn't have sku or the sku has already been taken.";

                    if (is_null($price) && !$isVariationProduct)
                        $errorMsg[$iteration][] = "Product doesn't have price.";

                    if (empty($categoriesIds))
                        $errorMsg[$iteration][] = "Product doesn't have category.";

                    if ($productAttributes->isEmpty() && $isVariationProduct)
                        $errorMsg[$iteration][] = "Product doesn't have attributes.";

                    /** Incomplete data errors */

                    if ($isNewProduct) {
                        $item->slug = $slug;
                        $item->position = time() . $iteration;
                        $item->active = !$incompleteData;
                        $item->show_on_main = 0;
                        $item->popularity_sales = null;
                        $item->popularity_rating = null;
                        $item->recommended_by_categories_ids = null;
                    }

                    $item->item_type_id = $isVariationProduct ? 2 : 1;
                    $item->brand_id = @$brand->id ?: null;
                    $item->price = $price;
                    $item->sale_price = $salePrice;
                    $item->use_stock = @$product['use_stock10'] == 1 ? 1 : 0;

                    if ((@$product['use_stock10'] == 1 && @$product['stock_qty'] != '-') || $isNewProduct)
                        $item->stock_quantity = @$product['use_stock10'] == 1 ? (@$product['stock_qty'] >= 0 ? $product['stock_qty'] : 0) : null;

                    $item->status = @strtolower($product['stock_statusio']) == 'i' ? 'in_stock' : 'out_of_stock';
                    $item->weight = (float)@$product['weight'] ?: null;
                    $item->length = (float)@$product['length'] ?: null;
                    $item->width = (float)@$product['width'] ?: null;
                    $item->height = (float)@$product['height'] ?: null;
                    $item->sku = @$sku != -1 ? $sku : null;
                    $item->incomplete_data = $incompleteData;
                    $item->save();

                    foreach ($this->langList as $lang) {
                        ProductsItems::updateOrCreate([
                            'products_item_id' => $item->id,
                            'lang_id' => $lang->id
                        ], [
                            'name' => @$productNames["name_{$lang->slug}"],
                            'description' => @$productDescriptions["description_{$lang->slug}"] && @$productDescriptions["description_{$lang->slug}"] != '-' ? @$productDescriptions["description_{$lang->slug}"] : null,
                        ]);
                    }

                    /** Save product files */

                    $importProductsFilesResponse = ProductsHelpers::importProductsFiles($item->id, @$product['files'], $this->currComponent->id, $iteration);
                    if (!$importProductsFilesResponse->status && $importProductsFilesResponse->errorMsg)
                        $errorMsg[$iteration][] = $importProductsFilesResponse->errorMsg;

                    /** Save product files */


                    /** Create relation products ~ categories */

                    $productCategories = ProductsCategoriesId::whereIn('id', $categoriesIds)->get();

                    ProductsItemsCategories::where('products_item_id', $item->id)->whereNotIn('id', $categoriesIds)->delete();
                    if ($productCategories->isNotEmpty())
                        foreach ($productCategories as $category) {
                            ProductsItemsCategories::firstOrCreate([
                                'products_item_id' => $item->id,
                                'products_category_id' => $category->id
                            ]);
                        }

                    /** Create relation products ~ categories */

                    /** Create relation products ~ attributes */

                    if ($productAttributes->isNotEmpty()) {
                        $parentAttributesIds = $productAttributes->pluck('parent.id')->toArray();

                        $existItemsAttributes = $item->productItemsAttributes()->whereIn('products_parent_attribute_id', $parentAttributesIds)->get();

                        if ($existItemsAttributes->isNotEmpty())
                            foreach ($existItemsAttributes as $existItemsAttribute) {
                                $childrenAttributesIds = $productAttributes->pluck('children')->flatten()->pluck('id')->unique()->toArray();

                                $existItemsAttribute->options()->whereNotIn('products_attribute_id', $childrenAttributesIds)->delete();
                            }

                        $item->productItemsAttributes()->whereNotIn('products_parent_attribute_id', $parentAttributesIds)->delete();

                        foreach ($productAttributes as $attribute) {
                            $parent = @$attribute['parent'];
                            $children = @$attribute['children'];

                            if (!$parent || !$children || ($children && $children->isEmpty()))
                                continue;

                            $itemAttribute = ProductsItemsAttributes::firstOrCreate([
                                'products_item_id' => $item->id,
                                'products_parent_attribute_id' => $parent->id
                            ], [
                                'visible_on_product_page' => 1,
                                'for_variation' => $isVariationProduct ? 1 : 0
                            ]);

                            $itemAttributeOptionsArr = [];
                            foreach ($children as $child) {
                                $itemAttributeOptions = ProductsItemsAttributesOptions::firstOrCreate([
                                    'products_items_attribute_id' => $itemAttribute->id,
                                    'products_attribute_id' => $child->id
                                ]);

                                if ($itemAttribute->for_variation)
                                    $itemAttributeOptionsArr[] = $itemAttributeOptions->products_attribute_id;
                            }

                            if ($itemAttribute->for_variation && !empty($itemAttributeOptionsArr))
                                $productAttributesForVariation[$itemAttribute->products_parent_attribute_id] = $itemAttributeOptionsArr;
                        }
                    }

                    /** Create relation products ~ attributes */

                    /** Create - update variations */

                    foreach ($productVariations as $productVariation) {
                        $iteration++;
                        $variation = array_combine($this->headerFile, $productVariation);
                        $allVariationAttributesCombination = arrayCartesian($productAttributesForVariation);

                        $variationAttributes = collect();
                        $variationAttributesIds = [];
                        $variationAttributesArr = explode(';', @$variation['variation_attributes']);
                        if (!empty($variationAttributesArr))
                            foreach ($variationAttributesArr as $variationAttribute) {
                                $parentChildrenAttr = explode(':', $variationAttribute);
                                $parentSlug = @str_replace(' ', '', @$parentChildrenAttr[0]) ?: null;
                                $childrenSlug = @str_replace(' ', '', @$parentChildrenAttr[1]) ?: null;

                                if (is_null($parentSlug)) {
                                    $errorMsg[$iteration][] = "Product variation attributes category is empty.";
                                    continue;
                                }

                                $parentAttr = $this->allAttributes->where('parent.slug', $parentSlug)->first();

                                if (is_null($parentAttr)) {
                                    $errorMsg[$iteration][] = "Product variation attributes category, `{$parentSlug}`, not exist.";
                                    continue;
                                }

                                if (is_null($childrenSlug)) {
                                    $errorMsg[$iteration][] = "Product variation attributes item is empty.";
                                    continue;
                                }

                                $childrenAttr = $parentAttr['children']->where('slug', $childrenSlug)->first();

                                if (is_null($childrenAttr) && $childrenSlug != 'all' && $childrenSlug != '-') {
                                    $errorMsg[$iteration][] = "Product variation attributes item, `{$childrenSlug}`, not exist.";
                                    continue;
                                }

                                $variationAttributes->push([
                                    'parent' => @$parentAttr['parent'],
                                    'child' => $childrenAttr
                                ]);

                                $variationAttributesIds[@$parentAttr['parent']->id] = $childrenSlug == 'all' || $childrenSlug == '-' ? 'all' : @$childrenAttr->id;
                            }

                        $isValidVariationAttr = false;
                        foreach ($allVariationAttributesCombination as $combination)
                            if (in_array('all', $variationAttributesIds)) {
                                $isValidVariationParentAttr = [];
                                foreach ($variationAttributesIds as $p_id => $filterVariationAttributesId)
                                    $isValidVariationParentAttr[] = array_key_exists($p_id, $combination) && !is_null($filterVariationAttributesId);

                                if (!in_array(false, $isValidVariationParentAttr) && count($variationAttributesIds) == count($combination)) {
                                    $isValidVariationAttr = true;
                                    break;
                                }
                            } else if ($variationAttributesIds == $combination) {
                                $isValidVariationAttr = true;
                                break;
                            }

                        if (!$isValidVariationAttr) {
                            $errorMsg[$iteration][] = "Product variation attributes combination not valid.";
                            continue;
                        }

                        $variationId = @$variation['variation_id'];
                        $variationItem = ProductsItemsVariationsDetailsId::where('products_item_id', $item->id)->find($variationId);
                        $isNewVariation = is_null($variationItem);
                        $variationSku = @$variation['variation_sku'] ?: -1;

                        if ($isNewVariation) {
                            $variationItem = new ProductsItemsVariationsDetailsId();

                            if (!is_null($variationItem->where('sku', $variationSku)->first())) // exist sku
                                $variationSku = null;
                        }

                        $variationPrice = @(float)$variation['variation_price'] ?: null;
                        $variationSalePrice = @(float)$variation['variation_sale_price'] ?: null;

                        if ($variationSalePrice > $variationPrice)
                            $variationSalePrice = null;

                        $variationDescriptions = filterArrayKeys($variation, 'variation_description_');

                        $incompleteVariationData = is_null($variationSku) || is_null($variationPrice) || empty($variationAttributesIds) ? 1 : 0;
                        if ($incompleteVariationData == 1 || ($incompleteVariationData == 0 && $item->incomplete_data == 0)) {
                            $item->update([
                                'incomplete_data' => $incompleteVariationData,
                                'active' => !$incompleteVariationData
                            ]);
                        }

                        /** Incomplete variations data errors */

                        if (is_null($variationSku))
                            $errorMsg[$iteration][] = "Product variation doesn't have sku or the sku has already been taken.";

                        if (is_null($variationPrice))
                            $errorMsg[$iteration][] = "Product variation doesn't have price.";

                        if (empty($variationAttributesIds))
                            $errorMsg[$iteration][] = "Product variation doesn't have attributes.";

                        /** Incomplete variations data errors */

                        if ($isNewVariation) {
                            $variationItem->products_item_id = $item->id;
                            $variationItem->is_default = 0;
                        }

                        $variationItem->price = $variationPrice;
                        $variationItem->sale_price = $variationSalePrice;
                        $variationItem->use_stock = @$variation['variation_use_stock10'] == 1 ? 1 : 0;

                        if ((@$variation['use_stock10'] == 1 && @$variation['stock_qty'] != '-') || $isNewVariation)
                            $variationItem->stock_quantity = @$variation['variation_use_stock10'] == 1 ? (@$variation['variation_stock_qty'] >= 0 ? $variation['variation_stock_qty'] : 0) : null;

                        $variationItem->status = @strtolower($variation['variation_stock_statusio']) == 'i' ? 'in_stock' : 'out_of_stock';
                        $variationItem->weight = (float)@$variation['variation_weight'] ?: null;
                        $variationItem->length = (float)@$variation['variation_length'] ?: null;
                        $variationItem->width = (float)@$variation['variation_width'] ?: null;
                        $variationItem->height = (float)@$variation['variation_height'] ?: null;
                        $variationItem->sku = @$variationSku != -1 ? $variationSku : null;
                        $variationItem->save();

                        foreach ($this->langList as $lang) {
                            ProductsItemsVariationsDetails::updateOrCreate([
                                'products_items_variations_detail_id' => $variationItem->id,
                                'lang_id' => $lang->id
                            ], [
                                'description' => @$variationDescriptions["variation_description_{$lang->slug}"] && @$variationDescriptions["variation_description_{$lang->slug}"] != '-' ? @$variationDescriptions["variation_description_{$lang->slug}"] : null,
                            ]);
                        }

                        /** Save variation files */

                        $importVariationsFilesResponse = ProductsHelpers::importProductsFiles($item->id, @$variation['variation_files'], $this->currComponent->id, $iteration, $variationItem->id);
                        if (!$importVariationsFilesResponse->status && $importVariationsFilesResponse->errorMsg)
                            $errorMsg[$iteration][] = $importVariationsFilesResponse->errorMsg;

                        /** Save variation files */

                        /** Create relation products ~ variations */

                        if (!empty($variationAttributesIds)) {
                            foreach ($variationAttributesIds as $parentAttrId => $childAttrId) {
                                ProductsItemsVariations::updateOrCreate([
                                    'products_items_variations_details_id' => $variationItem->id,
                                    'products_attribute_parent_id' => $parentAttrId,
                                ], [
                                    'products_attribute_child_id' => $childAttrId != 'all' && is_int($childAttrId) ? $childAttrId : null,
                                ]);
                            }
                        }

                        /** Create relation products ~ variations */

                    }

                    /** Create - update variations */

                    if ($item->incomplete_data)
                        $errorMsg[$iteration][] = "Product with ID `{$item->id}` is incomplete.";
                }
            }
        } else
            $errorMsg['rest'][] = 'Your file has a wrong format.';

        $tempErrorMsg = [];
        if (!empty($errorMsg)) {
            foreach ($errorMsg as $key => $error) {
                $tempErrorMsg[$key] = array_flatten($error);
            }
            $errorMsg = $tempErrorMsg;
        }

        if (file_exists($this->filePath))
            File::delete($this->filePath);

        try {
            $details = [
                'title' => 'Import products from CSV',
                'msg' => !empty($errorMsg) ? 'Your csv file was successful imported but you have some errors. Access the link or check your email.' : 'Your csv file was successful imported. Access this link or check your email.',
                'emailMsg' => 'Import products stock from CSV file was finished',
                'linkText' => !empty($errorMsg) ? 'View complete notification' : 'Go to products list',
                'url' => urldecode(customUrl(['admin', 'products'])),
                'errorMsg' => $errorMsg
            ];
            $this->user->notify(new ImportProductsNotification($details, $this->globalSettings));

            $this->user->update([
                'has_imported_products' => 0
            ]);
        } catch (\Exception $e) {
        }
    }

    /**
     * The job failed to process.
     *
     * @param \Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {

        $this->user->update([
            'has_exported_products' => 0
        ]);

        try {
            $details = [
                'title' => 'Import products from CSV',
                'msg' => 'Your csv file import was failed. Try again.',
                'emailMsg' => 'Your csv file import was failed. Try again.',
                'linkText' => 'Go to products list',
                'url' => urldecode(customUrl(['admin', 'products'])),
            ];
            $this->user->notify(new ImportProductsNotification($details, $this->globalSettings));
        } catch (\Exception $e) {
        }
    }
}
