<?php

namespace Modules\Products\Repositories;

use App\Repositories\BaseRepository;
use Modules\Products\Models\ProductsItemsId as Model;

class ProductsItemsIdRepository extends BaseRepository
{

    protected $model;

    public function __construct()
    {
        parent::__construct(new Model());
    }
}