<?php

namespace Modules\Products\Repositories;

use Modules\Products\Models\ProductsAttributesId as Model;
use App\Repositories\BaseRepository;

class ProductsAttributesIdRepository extends BaseRepository
{

    protected $model;

    public function __construct()
    {
        parent::__construct(new Model());
    }
}