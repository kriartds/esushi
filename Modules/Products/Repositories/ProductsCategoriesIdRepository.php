<?php

namespace Modules\Products\Repositories;

use Modules\Products\Models\ProductsCategoriesId as Model;
use App\Repositories\BaseRepository;

class ProductsCategoriesIdRepository extends BaseRepository
{

    protected $model;

    public function __construct()
    {
        parent::__construct(new Model());
    }
}