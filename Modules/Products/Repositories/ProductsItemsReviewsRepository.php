<?php

namespace Modules\Products\Repositories;

use App\Repositories\BaseRepository;
use Modules\Products\Models\ProductsItemsReviews as Model;

class ProductsItemsReviewsRepository extends BaseRepository
{

    protected $model;

    public function __construct()
    {
        parent::__construct(new Model());
    }

}