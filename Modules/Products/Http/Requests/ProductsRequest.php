<?php

namespace Modules\Products\Http\Requests;

use App\Http\Helpers\Helpers;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    protected $msg = [];
    public function rules()
    {


        $rules = [
            'lang' => 'required',
            'product_type' => 'required',
            'name' => 'required',
            'slug' => "required|unique:products_items_id,slug,{$this->id}",
            'sku' => "nullable|unique:products_items_id,sku,{$this->id}",
            'price' => 'nullable|lower_than_field:sale_price|numeric',
            'sale_price' => 'nullable|numeric',
        ];

        $currComponent = Helpers::currComponent();
        $dynamicRules = Helpers::updateValidateDynamicField(request()->all(), $currComponent, $this->id, $this->langId, 'validate');

        if (!empty($dynamicRules)) {
            $rules = array_merge($rules, $dynamicRules);
            foreach ($dynamicRules as $key => $dynamicRule) {
                $this->msg["{$key}.{$dynamicRule}"] = 'The field is required';
            }
        }

        if (request()->get('item_variations')) {
            if ((@count(request()->get('item_variations')['attr_ids']) < 1 || is_null(@count(request()->get('item_variations')['price_variation']))) && @count(request()->get('item_variations')['price_variation']) != @count(request()->get('item_variations')['sale_price_variation']))
                throw new HttpResponseException(response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Add minim one variation!'],
                        'type' => 'warning'
                    ]]));

            if (@request()->get('item_variations')['price_variation'])
                foreach (request()->get('item_variations')['price_variation'] as $key => $val) {

                    $rules["item_variations.price_variation.{$key}"] = 'nullable|lower_than_field:sale_price|numeric';
                    $rules["item_variations.sale_price_variation.{$key}"] = 'nullable|numeric';

                    $this->msg["item_variations.price_variation.{$key}.numeric"] = 'The price must be a number.';
                    $this->msg["item_variations.sale_price_variation.{$key}.numeric"] = 'The price must be a number.';

                    if ($val < request()->get('item_variations')['sale_price_variation'][$key])
                        $variationPriceErr[] = [
                            'rule' => 'item_variations.price_variation.' . $key . '.' . 0,
                            'msg' => 'The price may not be lower than sale price'
                        ];

                }
        }
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return $this->msg;
    }

    protected function failedValidation(Validator $validator)
    {
        $data = [
            'status' => false,
            'validator' => true,
            'msg' => [
                'e' => $validator->messages(),
                'type' => 'error'
            ],
        ];

        throw new HttpResponseException(response()->json($data));
    }
}
