<?php

namespace Modules\Products\Http\Requests;

use App\Http\Helpers\Helpers;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductsPromotionsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    private $msg = [];
    public function rules()
    {
        $rules = [
            'lang' => 'required',
            'name' => 'required',
            'slug' => "required|unique:products_promotions_id,slug,{$this->id}",
            'start_date' => "required|date|after_or_equal:" . now()->format('d-m-Y'),
            'end_date' => 'nullable|date|after_or_equal:start_date',
        ];

        $currComponent = Helpers::currComponent();
        $dynamicRules = Helpers::updateValidateDynamicField(request()->all(), $currComponent, $this->id, $this->langId, 'validate');

        if (!empty($dynamicRules)) {
            $rules = array_merge($rules, $dynamicRules);
            foreach ($dynamicRules as $key => $dynamicRule) {
                $this->msg["{$key}.{$dynamicRule}"] = 'The field is required';
            }
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        $data = [
            'status' => false,
            'validator' => true,
            'msg' => [
                'e' => $validator->messages(),
                'type' => 'error'
            ],
        ];

        throw new HttpResponseException(response()->json($data));
    }
}
