<?php

namespace Modules\Products\Http\Helpers;

use App\Models\ComponentsId;
use App\Models\Files;
use App\Models\FilesItems;
use Modules\Products\Models\ProductsToppingsId;
use function GuzzleHttp\Psr7\mimetype_from_extension;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Modules\Products\Models\ProductsAttributesId;
use Modules\Products\Models\ProductsItemsId;
use Modules\Products\Models\ProductsItemsVariationsDetailsId;
use Modules\Products\Repositories\ProductsAttributesIdRepository;
use Modules\Products\Repositories\ProductsItemsIdRepository;
use Throwable;

class Helpers
{
    public static function makeFilter($request, $currComponent, $attributes, $filteredProductsPricesRange, $currCurrency = null, $currCategory = null)
    {

        $response = null;

        $filterTableList = filterTableList($request, $request->except('page'));
        $filterParams = $filterTableList->filterParams;

//        Use price range

        $filteredProductsPriceList = self::filteredProductsPriceList($filteredProductsPricesRange, $currCurrency);

        $productsMinPrice = $filteredProductsPriceList['productsMinPrice'];
        $productsMaxPrice = $filteredProductsPriceList['productsMaxPrice'];


        try {
            $response = [
                'currComponent' => $currComponent,
                'attributes' => $attributes,
                'filterParams' => $filterParams,
                'usePriceRange' => config('cms.catalog.makeFilter.usePriceRange', false),
                'useSubmitBtn' => config('cms.catalog.makeFilter.useSubmitBtn', false),
                'displayColorName' => config('cms.catalog.makeFilter.displayColorName', false),
                'displayImageName' => config('cms.catalog.makeFilter.displayImageName', false),
                'productsMinPrice' => $productsMinPrice,
                'productsMaxPrice' => $productsMaxPrice,
                'categoryId' => @$currCategory->id,
            ];


            if ($request->ajax())
                return view('front.components.catalog.filterListItems', $response)->render();
            else
                return view('front.components.catalog.filterList', $response)->render();
        } catch
        (Throwable $e) {
            return null;
        }
    }

    private static function filteredProductsPriceList($filteredProductsPricesRange, $currCurrency = null)
    {
//        Use price range

        $productsMinPrice = 0;
        $productsMaxPrice = 1;

        if ($filteredProductsPricesRange->isNotEmpty()) {
            $allPrices = $filteredProductsPricesRange->filter()->unique();

            if ($allPrices->isNotEmpty()) {
                $productsMinPrice = $allPrices->min();
                $productsMaxPrice = $allPrices->max();
            }
        }
//        Use price range

        return collect([
            'productsMinPrice' => convertPrice($productsMinPrice, $currCurrency),
            'productsMaxPrice' => convertPrice($productsMaxPrice, $currCurrency)
        ]);
    }

    public static function filterProducts($request, $currComponent, $perPage, $currCurrency = null, $category = null, $excepts = ['page'])
    {

        $productAttributesIdRepo = new ProductsAttributesIdRepository();
        $productsItemsIdRepo = new ProductsItemsIdRepository();

        $filterTableList = filterTableList($request, $request->except($excepts));
        $filterParams = $filterTableList->filterParams;
        $pushUrl = $filterTableList->pushUrl;

        $productsAttrIds = $selectedAttrIds = $sortFilterParams = $selectedProductsAttributes = [];
        $selectedAttributes = collect();

        foreach ($filterParams as $key => $filterParam) {

            if (strpos($key, 'filter-') !== false) {
                $attrSlug = preg_replace('/^filter-|-from$|-to$/', '', $key);

                if ($attrSlug) {
                    $attribute = $productAttributesIdRepo->first([
                        'active' => 1,
                        'whereNull' => 'p_id',
                        'slug' => $attrSlug
                    ], ['children']);

                    if (!is_null($attribute))
                        $selectedAttributes->push($attribute);
                }
            }

            if (strpos($key, 'sort-') !== false) {
                $newValue = preg_replace('/^sort-by-/', '', $filterParam);
                $sortFilterParams[$key] = $newValue;

            }
        }

        $selectedAttributes = $selectedAttributes->unique('id');

        if ($selectedAttributes->isNotEmpty()) {
            $selectedAttrIds = $selectedAttributes->pluck('id')->toArray();

            foreach ($selectedAttributes as $attribute) {

                $selectedAttr = $attribute->children()
                    ->where(function ($q) use ($attribute, $filterParams) {
                        if ($attribute->field_type == 'checkbox' || $attribute->field_type == 'radio') {
                            $filterKey = "filter-{$attribute->slug}";

                            if (@$filterParams[$filterKey] && is_array($filterParams[$filterKey]))
                                $q->whereIn('id', $filterParams[$filterKey]);
                        }

                        if ($attribute->field_type == 'range') {
                            $fromKey = "filter-{$attribute->slug}-from";
                            $toKey = "filter-{$attribute->slug}-to";

                            if (@$filterParams[$fromKey] && !@$filterParams[$toKey])
                                $q->where('range_value', '>=', (float)$filterParams[$fromKey]);
                            elseif (!@$filterParams[$fromKey] && @$filterParams[$toKey])
                                $q->where('range_value', '<=', (float)$filterParams[$toKey]);
                            elseif (@$filterParams[$fromKey] && @$filterParams[$toKey]) {
                                $q->where('range_value', '>=', (float)$filterParams[$fromKey])->where('range_value', '<=', (float)$filterParams[$toKey]);
                            }
                        }
                    })
                    ->get();

                $selectedProductsAttributes[] = [
                    'parent' => $attribute,
                    'children' => $selectedAttr
                ];
                $productsAttrIds[] = $selectedAttr->pluck('id')->toArray();
            }
        }

        $productsItemsInit = $productsItemsIdRepo->init();

        $items = $productsItemsInit;
        $onlyForPriceItems = clone $items;

        $queryOptions = [
            'filterParams' => $filterParams,
            'productsAttrIds' => $productsAttrIds,
            'categoryId' => @$category->id,
            'selectedAttrIds' => $selectedAttrIds,
            'currCurrency' => $currCurrency
        ];

        $variationsPrices = ProductsItemsVariationsDetailsId::selectRaw('MAX(price) as max_price, MIN(price) as min_price, MAX(sale_price) as max_sale_price, MIN(sale_price) as min_sale_price')
            ->whereHas('product', function ($q) use ($queryOptions) {
                self::filterQueryConditions($q, $queryOptions);
            })
            ->limit(1)
            ->first();

        $productsPrices = $onlyForPriceItems->selectRaw('MAX(price) as max_price, MIN(price) as min_price, MAX(sale_price) as max_sale_price, MIN(sale_price) as min_sale_price')
            ->where(function ($q) use ($queryOptions) {
                self::filterQueryConditions($q, $queryOptions);
            })
            ->where('item_type_id', 1)
            ->limit(1)
            ->first();

        $filteredProductsPricesRange = collect([
            'max_variations_price' => @$variationsPrices->max_price,
            'min_variations_price' => @$variationsPrices->min_price,
            'max_variations_sale_price' => @$variationsPrices->max_sale_price,
            'min_variations_sale_price' => @$variationsPrices->min_sale_price,
            'max_price' => @$productsPrices->max_price,
            'min_price' => @$productsPrices->min_price,
            'max_sale_price' => @$productsPrices->max_sale_price,
            'min_sale_price' => @$productsPrices->min_sale_price,
        ]);

//        Filtered items attributes

        $items = $items->select('products_items_id.*')
            ->leftJoin('products_items_variations_details_id', 'products_items_variations_details_id.products_item_id', '=', 'products_items_id.id')
            ->groupBy('products_items_id.id')
            ->where(function ($q) use ($queryOptions) {
                self::filterQueryConditions($q, $queryOptions);
            });

        if (!empty($filterParams)) {
            $items = $items->where(function ($q) use ($queryOptions) {
                self::filterQueryConditions($q, $queryOptions, true);
            });
        }

        $items = $items->with(['globalName', 'file.file', 'reviews', 'variationsDetails']);

        if (!empty($sortFilterParams)) {
            $defaultOrder = true;

            if (@$sortFilterParams['sort-items']) {
                $defaultOrder = false;

                if ($sortFilterParams['sort-items'] == 'popularity')
                    $items = $items->orderBy('popularity_sales', 'desc');
                elseif ($sortFilterParams['sort-items'] == 'rating')
                    $items = $items->orderBy('popularity_rating', 'desc');
                elseif ($sortFilterParams['sort-items'] == 'newest')
                    $items = $items->orderBy('created_at', 'desc');
                elseif ($sortFilterParams['sort-items'] == 'price-asc' || $sortFilterParams['sort-items'] == 'price-desc') {
                    $order = $sortFilterParams['sort-items'] == 'price-asc' ? 'ASC' : 'DESC';

                    $items = $items->selectRaw('(CASE WHEN (products_items_id.item_type_id = 2)
                                         THEN
                                            (CASE WHEN (products_items_variations_details_id.sale_price = 0 || products_items_variations_details_id.sale_price IS NULL)
                                                THEN
                                                    products_items_variations_details_id.price
                                                ELSE
                                                    products_items_variations_details_id.sale_price
                                            END)
                                        ELSE
                                            (CASE WHEN (products_items_id.sale_price = 0 || products_items_id.sale_price IS NULL)
                                                THEN
                                                    products_items_id.price
                                                ELSE
                                                    products_items_id.sale_price
                                            END)
                                        END) as min_product_price')->orderBy('min_product_price', $order);
                }
            }

            if (@$sortFilterParams['sort-sales-products'] && @$sortFilterParams['sort-sales-products'] == 'on') {
                $items = $items->where(function ($q) {
                    $q->where(function ($q) {
                        $q->whereNotNull('products_items_id.sale_price');
                        $q->where('products_items_id.sale_price', '>', 0);
                    });
                    $q->orWhere(function ($q) {
                        $q->whereNotNull('products_items_variations_details_id.sale_price');
                        $q->where('products_items_variations_details_id.sale_price', '>', 0);
                    });
                });
            }


            if ($defaultOrder)
                $items = $items->orderBy('position');

            if (@$sortFilterParams['sort-per-page'] && ($sortFilterParams['sort-per-page'] == 24 || $sortFilterParams['sort-per-page'] == 36))
                $items = $items->paginate($sortFilterParams['sort-per-page']);
            else
                $items = $items->paginate($perPage);

        } else
            $items = $items->orderBy('position')->paginate($perPage);

//        Filter attributes

        $attributes = ProductsAttributesId::where(function ($q) use ($queryOptions, $items, $selectedAttrIds) {
            if ($items->isNotEmpty()) {
                $q->whereHas('products', function ($q) use ($queryOptions) {
                    $q->leftJoin('products_items_variations_details_id', 'products_items_variations_details_id.products_item_id', '=', 'products_items_id.id');
                    self::filterQueryConditions($q, $queryOptions, true);
                });
            } else
                $q->whereIn('id', $selectedAttrIds);
            $q->whereNull('p_id');
        })
            ->where('products_attributes_id.active', 1)
            ->where('products_attributes_id.display_in_filter', 1)
            ->whereNull('products_attributes_id.p_id')
            ->with(['type', 'globalName', 'children.globalName', 'children' => function ($q) use ($queryOptions) {
                $q->whereHas('attrOptions', function ($q) use ($queryOptions) {
                    if (!is_null($queryOptions['categoryId']))
                        $q->whereHas('productsAttr.product.productCategories', function ($q) use ($queryOptions) {
                            $q->where('products_category_id', $queryOptions['categoryId']);
                        });
                });
                $q->orderBy('position');
            }])
            ->orderBy('position')
            ->get();

//        Filter attributes

        $renderedNewFilter = null;

        if ($request->ajax())
            $renderedNewFilter = self::makeFilter($request, $currComponent, $attributes, $filteredProductsPricesRange, $currCurrency, $category);

        $response = [
            'status' => true,
            'items' => $items,
            'filteredProductsPricesRange' => $filteredProductsPricesRange,
            'pushUrl' => $pushUrl,
//            'filterParams' => $filterParams,
//            'selectedFilterParams' => self::getSelectedFilterParams($selectedProductsAttributes, $filterParams),
            'filter' => $renderedNewFilter,
            'attributes' => $attributes,
        ];

        return $response;
    }

    public static function makeSortFilter($request, $currComponent, $category = null)
    {

        $response = null;

        $filterParams = array_filter($request->except('page'));

        $sortFilterParams = [];

        foreach ($filterParams as $key => $filterParam) {

            if (strpos($key, 'sort-') !== false)
                $sortFilterParams[$key] = $filterParam;
        }

        try {
            $response = [
                'useSort' => config('cms.catalog.makeSortFilter.useSort', true),
                'usePerPage' => config('cms.catalog.makeSortFilter.usePerPage', true),
                'currComponent' => $currComponent,
                'categoryId' => @$category->id,
                'sortFilterParams' => $sortFilterParams
            ];

            return view('front.components.catalog.sortList', $response)->render();
        } catch (Throwable $e) {
            return null;
        }
    }

    public static function defineProductVariations($product, $itemVariation = null, $forMakeProductVariations = true)
    {
        $displayStockConfig = config('cms.product.makeProductVariations.displayStock', false);
        $fullDescTitle = config('cms.product.fullDescTitle', true);

        $response = null;

        if (is_null($product))
            return $response;

        $currCurrency = getCurrentCurrency();
        $attributes = self::getProductSelectedAttributes($product, true);

        if ($attributes)
            $attributes = $attributes->sortBy(function ($attr) {
                return @$attr['parent']->position;
            });

        if ($attributes->isEmpty() && @$product->isVariationProduct)
            return $response;

        $allProductVariationsWithAttr = $allTempProductVariationsWithAttr = collect();
        $allProductVariations = collect();
        $availableAttrChildrenIds = [];

        $productFilesSmall = getItemFile($product, 'small', true, 'allFiles');
        $productFilesMedium = getItemFile($product, 'medium', true, 'allFiles');
        $productFilesLarge = getItemFile($product, 'large', true, 'allFiles');

        if (@$product->isVariationProduct) {
            $allProductVariations = $product->variationsDetails()->with(['variations', 'globalName', 'files.file'])->get();

            if ($allProductVariations->isNotEmpty())
                foreach ($allProductVariations as $allProductVariation) {
                    $allAttrChildrenIds = [];
                    $allAttrChildren = [];
                    $allAttrFullNames = [];
                    $allChildrenIsNull = [];

                    $allItemVariations = $allProductVariation->variations;

                    if ($allItemVariations->isNotEmpty())
                        foreach ($allItemVariations as $allItemVariation) {
                            $fullAttrInfo = @$attributes->where('parent.id', $allItemVariation->products_attribute_parent_id)->first();
                            if (is_null($fullAttrInfo))
                                continue;

                            $fullAttrInfoChildren = @$fullAttrInfo['children'];
                            $currAttrInfoParent = @$fullAttrInfo['parent'];
                            if (is_null($allItemVariation->products_attribute_child_id))
                                $currAttrInfoChildren = @$fullAttrInfoChildren->first();
                            else
                                $currAttrInfoChildren = @$fullAttrInfoChildren->where('id', $allItemVariation->products_attribute_child_id)->first();

                            $tempAllAttrChildrenIds = @$fullAttrInfoChildren->pluck('id')->toArray();

                            if (is_null($allItemVariation->products_attribute_child_id)) {
                                $allChildrenIsNull[] = true;

                                if ($tempAllAttrChildrenIds) {
                                    $allAttrChildrenIds[] = $tempAllAttrChildrenIds;

                                    foreach ($tempAllAttrChildrenIds as $k => $tempAllAttrChildrenId) {
                                        $availableAttrChildrenIds[] = $tempAllAttrChildrenId;

                                        $currFullAttrInfoChildren = $fullAttrInfoChildren->where('id', $tempAllAttrChildrenId)->first();
                                        if ($currFullAttrInfoChildren)
                                            $allAttrChildren[$currAttrInfoParent->slug][] = $currFullAttrInfoChildren->id;

                                    }
                                }
                            } else {
                                $allChildrenIsNull[] = false;
                                $allAttrChildrenIds[] = $allItemVariation->products_attribute_child_id;
                                $availableAttrChildrenIds[] = $allItemVariation->products_attribute_child_id;
                                $allAttrFullNames[] = @$currAttrInfoParent->globalName->name . "&nbsp;" . @$currAttrInfoChildren->globalName->name;

                                if ($fullAttrInfoChildren->whereIn('id', $allAttrChildrenIds)->isNotEmpty())
                                    foreach ($fullAttrInfoChildren->whereIn('id', $allAttrChildrenIds) as $fullAttrInfoChild) {
                                        $allAttrChildren[$currAttrInfoParent->slug] = $fullAttrInfoChild->id;
                                    }
                            }
                        }

                    $allAttrChildrenIds = array_flatten($allAttrChildrenIds);
                    $allAttrFullNames = array_filter($allAttrFullNames);

                    if (count(array_unique($allChildrenIsNull)) > 1 || (count(array_unique($allChildrenIsNull)) == 1 && in_array(false, array_unique($allChildrenIsNull)))) {
                        $newAllAttrChild = [];
                        $tempAllAttrChildren = $allAttrChildren;
                        if (!empty($allAttrChildren)) {
                            foreach ($allAttrChildren as $key => $allAttrChild) {
                                if (is_array($allAttrChild)) {
                                    foreach ($allAttrChild as $attrChild) {
                                        $tempAllAttrChildren[$key] = $attrChild;
                                        if (!is_array($attrChild))
                                            $newAllAttrChild[] = $tempAllAttrChildren;
                                    }
                                }
                            }
                        }

                        if (!empty($newAllAttrChild))
                            $allAttrChildren = $newAllAttrChild;
                    } else
                        $allAttrChildren = arrayCartesian($allAttrChildren);

                    $variationFilesSmall = getItemFile($allProductVariation, 'small', false, 'allFiles');
                    $variationFilesMedium = getItemFile($allProductVariation, 'medium', false, 'allFiles');
                    $variationFilesLarge = getItemFile($allProductVariation, 'large', false, 'allFiles');

                    $finProductVariationsArr = [
                        'variation' => [
                            'id' => $allProductVariation->id,
                            'slug' => $product->slug,
                            'name' => @$product->globalName->name,
                            'stock_qty' => $allProductVariation->finStock,
                            'stock' => $allProductVariation->finStock ? (is_numeric($allProductVariation->finStock) && $displayStockConfig ? __('front.in_stock_qty', ['quantity' => $allProductVariation->finStock]) : __('front.in_stock')) : __('front.out_of_stock'),
                            'price' => formatPrice($allProductVariation->price, $currCurrency),
                            'sale_price' => formatPrice($allProductVariation->sale_price, $currCurrency),
                            'cleanPrice' => convertPrice($allProductVariation->price, $currCurrency),
                            'cleanSalePrice' => convertPrice($allProductVariation->sale_price, $currCurrency),
                            'productPromotionPercentage' => $allProductVariation->price > 0 ? round(($allProductVariation->price - $allProductVariation->sale_price) * 100 / $allProductVariation->price) : 0,
                            'weight' => $allProductVariation->weight,
                            'length' => $allProductVariation->length,
                            'width' => $allProductVariation->width,
                            'height' => $allProductVariation->height,
                            'sku' => $allProductVariation->sku,
                            'description_title' => !empty($allAttrFullNames) && $fullDescTitle ? __("front.product_description_title", ['title' => @$product->globalName->name, 'attributes' => implode('&nbsp;', $allAttrFullNames)]) : null,
                            'description' => @$allProductVariation->globalName->description,
                            'files' => [],
                            'productFiles' => [
                                'small' => $productFilesSmall,
                                'medium' => $productFilesMedium,
                                'large' => $productFilesLarge
                            ]
                        ],
                        'attributes' => $allAttrChildren,
                        'attributesIds' => $allAttrChildrenIds
                    ];

                    if ($variationFilesSmall || $variationFilesMedium || $variationFilesLarge)
                        $finProductVariationsArr['variation']['files'] = [
                            'small' => $variationFilesSmall,
                            'medium' => $variationFilesMedium,
                            'large' => $variationFilesLarge
                        ];

                    $allTempProductVariationsWithAttr->put($allProductVariation->id, $finProductVariationsArr);

                }

            $allProductVariationsWithAttr = [
                'variations' => $allTempProductVariationsWithAttr,
            ];
            if (!empty($availableAttrChildrenIds)) {
                $availableAttrChildrenIds = array_unique(array_flatten($availableAttrChildrenIds));
                $filteredAttributes = collect();
                foreach ($attributes as $attribute) {
                    $tempAttr = $attribute;
                    $tempAttr['children'] = $attribute['children']->whereIn('id', $availableAttrChildrenIds);
                    $filteredAttributes->push($tempAttr);
                }

                $attributes = $filteredAttributes;
            }

        }

        $defaultVariation = self::getProductDefaultVariation($product, $attributes, $itemVariation, $allProductVariations);

        $response = [
            'product' => $product,
            'attributes' => $attributes,
            'displayStock' => $displayStockConfig,
            'buyInOneClick' => config('cms.product.makeProductVariations.buyInOneClick', true),
            'defaultVariation' => $defaultVariation,
            'allProductVariations' => json_encode($allProductVariationsWithAttr),
        ];

        if (!$forMakeProductVariations)
            $response['allProductVariationsArr'] = $allProductVariationsWithAttr;

        return $response;
    }

    public static function makeProductVariations($product, $itemVariation = null, $defaultValuesFromVariations = [], $scope=null, $options=[])
    {

        try {
            $response = !empty($defaultValuesFromVariations) ? $defaultValuesFromVariations : self::defineProductVariations($product, $itemVariation);

            $response['scope'] = $scope;

            $response['toppings'] = !empty($product->toppings_ids) ? ProductsToppingsId::where('active', 1)->whereIn('id', $product->toppings_ids)->with(['globalName'])->get() : collect();

            if(!empty($options))
                $response = array_merge($response, $options);

            return view('front.components.productPage.templates.variationsList', $response)->render();

        } catch (Throwable $e) {
            return null;
        }
    }

    public static function getProductDefaultVariation($product, $attributes, $variationId = null, $allProductVariations = null)
    {

        if (!$product->isVariationProduct)
            return null;

        if (!$allProductVariations)
            $allProductVariations = $product->variationsDetails()->with(['variations', 'globalName', 'files.file'])->get();

        if (is_null($variationId))
            $defaultVariation = $allProductVariations->where('is_default', 1)->first();
        else {
            $defaultVariation = $allProductVariations->find($variationId);
            if (is_null($defaultVariation))
                abort(404);
        }

        $allAttrNamesArr = collect();
        $allAttrChildrenIds = [];

        if (!is_null($defaultVariation)) {
            $allItemVariations = $defaultVariation->variations;

            if ($allItemVariations->isNotEmpty())
                foreach ($allItemVariations as $key => $allItemVariation) {
                    $fullAttrInfo = @$attributes->where('parent.id', $allItemVariation->products_attribute_parent_id)->first();
                    $fullAttrInfoParent = @$fullAttrInfo['parent'];
                    $fullAttrInfoChildren = @$fullAttrInfo['children'];

                    $parentName = @$fullAttrInfoParent->globalName->name;
                    $childName = null;
                    if ($fullAttrInfoChildren)
                        $childName = @$fullAttrInfoChildren->where('id', $allItemVariation->products_attribute_child_id)->first()->globalName->name;

                    if ($parentName && ($childName && $allItemVariation->products_attribute_child_id || !$childName && !$allItemVariation->products_attribute_child_id))
                        $allAttrNamesArr->push([
                            'parent' => $parentName,
                            'child' => $allItemVariation->products_attribute_child_id ? $childName : 'All attributes',
                            'position' => $fullAttrInfoParent->position
                        ]);

                    if (is_null($allItemVariation->products_attribute_child_id))
                        $allAttrChildrenIds[] = @$fullAttrInfoChildren->first()->id;
                    else
                        $allAttrChildrenIds[] = $allItemVariation->products_attribute_child_id;

                }

            $allAttrChildrenIds = array_flatten($allAttrChildrenIds);

            if ($allAttrNamesArr->isNotEmpty())
                $allAttrNamesArr = $allAttrNamesArr->sortBy('position');

            $defaultVariation->defaultVariationsAttrIds = $allAttrChildrenIds;
            $defaultVariation->defaultVariationsAttrNames = $allAttrNamesArr;
        }

        return $defaultVariation;
    }

    public static function getProductSelectedAttributes($product, $forVariation = false, $visibleOnProductPage = false, $newInstance = true)
    {

        if (is_null($product))
            return false;

        $response = collect();

        if ($newInstance)
            $productItemsAttributes = $product->productItemsAttributes()
                ->with(['attribute', 'attribute.globalName', 'attribute.type', 'options', 'options.attribute', 'options.attribute.globalName'])
                ->get();
        else
            $productItemsAttributes = $product->productItemsAttributes;

        if ($forVariation)
            $productItemsAttributes = $productItemsAttributes->where('for_variation', 1);

        if ($visibleOnProductPage)
            $productItemsAttributes = $productItemsAttributes->where('visible_on_product_page', 1);

        if ($productItemsAttributes->isNotEmpty())
            foreach ($productItemsAttributes as $productItemsAttribute) {
                $options = collect();
                if ($productItemsAttribute->options->isNotEmpty())
                    foreach ($productItemsAttribute->options as $option) {
                        $options->push($option->attribute);
                    }

                $response->push([
                    'parent' => $productItemsAttribute->attribute,
                    'children' => $options
                ]);
            }

        if ($response->isNotEmpty())
            $response = $response->sortBy('parent.position');

        return $response;
    }

    public static function getSelectedFilterParams($attributes, $filterParams)
    {

        $response = collect();

        if (!empty($attributes)) {
            foreach ($attributes as $key => $attribute) {
                $parent = @$attribute['parent'];
                $children = @$attribute['children'];

                if ($parent) {
                    $response->put($key, collect([
                        'type' => @$parent->type->slug,
                        'parentId' => @$parent->id,
                        'parent' => @$parent->globalName->name,
                        'children' => collect()
                    ]));

                    if ($children) {
                        foreach ($children as $k => $child) {

                            $response[$key]['children']->put($k, collect(['id' => @$child->id, 'name' => @$child->globalName->name]));

                            if (@$parent->type->slug == 'color')
                                $response[$key]['children'][$k]->put('value', $child->colors);
                            elseif (@$parent->type->slug == 'image')
                                $response[$key]['children'][$k]->put('value', @$child->firstFile->small);
                            else
                                $response[$key]['children'][$k]->put('value', @$child->globalName->name);
                        }
                    }
                }
            }
        }

        $response->push(collect([
            'type' => 'price',
            'parentId' => 'price-attribute',
            'children' => @$filterParams['price-from'] || @$filterParams['price-to'] ? collect([
                'from' => @$filterParams['price-from'] ? $filterParams['price-from'] : 0,
                'to' => @$filterParams['price-to'] ? $filterParams['price-to'] : 1
            ]) : collect()
        ]));

        return $response;
    }

    private static function filterQueryConditions($query, $options = [], $filterByPrice = false)
    {

        $filterParams = $options['filterParams'];
        $productsAttrIds = $options['productsAttrIds'];
        $categoryId = $options['categoryId'];
        $currCurrency = $options['currCurrency'];
//        $selectedAttrIds = $options['selectedAttrIds'];

        $query->where('products_items_id.active', 1);
        $query->where(function ($q) use ($filterParams, $productsAttrIds, $categoryId) {
            if (!empty($productsAttrIds))
                foreach ($productsAttrIds as $productsAttrId) {
                    $q->whereHas('productItemsAttributes.options', function ($q) use ($productsAttrId) {
                        $q->whereIn('products_attribute_id', $productsAttrId);
                    });
                }

            if (!is_null($categoryId))
                $q->whereHas('productCategories', function ($q) use ($categoryId) {
                    $q->where('products_category_id', $categoryId);
                });

            if (array_key_exists('categories', $filterParams) && is_array($filterParams['categories']))
                $q->whereHas('productCategories', function ($q) use ($filterParams) {
                    $q->whereIn('products_category_id', $filterParams['categories']);
//                        $q->havingRaw('count(*) = ' . count($filterParams['categories']));
                });

        });

        if ($filterByPrice) {
            $fromKey = "price-from";
            $toKey = "price-to";

            if (@floor($filterParams[$fromKey]) && !@ceil($filterParams[$toKey])) {
                $fromPrice = $currCurrency->is_default ? floor(convertPrice($filterParams[$fromKey], $currCurrency, true)) : convertPrice($filterParams[$fromKey], $currCurrency, true);
                $query->whereRaw("(CASE WHEN (products_items_id.price = 0 || products_items_id.price IS NULL)
                                    THEN (
                                    CASE WHEN (products_items_variations_details_id.sale_price = 0 || products_items_variations_details_id.sale_price IS NULL)
                                      THEN
                                        products_items_variations_details_id.price
                                    ELSE
                                      products_items_variations_details_id.sale_price
                                    END
                                    )
                                    ELSE
                                      (
                                        CASE WHEN (products_items_id.sale_price = 0 || products_items_id.sale_price IS NULL)
                                          THEN
                                            products_items_id.price
                                        ELSE
                                          products_items_id.sale_price
                                        END
                                      )
                                    END
                                    ) >= '{$fromPrice}'");

            } elseif (!@floor($filterParams[$fromKey]) && @ceil($filterParams[$toKey])) {
                $toPrice = $currCurrency->is_default ? ceil(convertPrice($filterParams[$toKey], $currCurrency, true)) : convertPrice($filterParams[$toKey], $currCurrency, true);
                $query->whereRaw("(CASE WHEN (products_items_id.price = 0 || products_items_id.price IS NULL)
                                    THEN (
                                    CASE WHEN (products_items_variations_details_id.sale_price = 0 || products_items_variations_details_id.sale_price IS NULL)
                                      THEN
                                        products_items_variations_details_id.price
                                    ELSE
                                      products_items_variations_details_id.sale_price
                                    END
                                    )
                                    ELSE
                                      (
                                        CASE WHEN (products_items_id.sale_price = 0 || products_items_id.sale_price IS NULL)
                                          THEN
                                            products_items_id.price
                                        ELSE
                                          products_items_id.sale_price
                                        END
                                      )
                                    END
                                    ) <= '{$toPrice}'");

            } elseif (@floor($filterParams[$fromKey]) && @ceil($filterParams[$toKey])) {
                $fromPrice = $currCurrency->is_default ? floor(convertPrice($filterParams[$fromKey], $currCurrency, true)) : convertPrice($filterParams[$fromKey], $currCurrency, true);
                $toPrice = $currCurrency->is_default ? ceil(convertPrice($filterParams[$toKey], $currCurrency, true)) : convertPrice($filterParams[$toKey], $currCurrency, true);
                $query->whereRaw("(CASE WHEN (products_items_id.price = 0 || products_items_id.price IS NULL)
                                    THEN (
                                    CASE WHEN (products_items_variations_details_id.sale_price = 0 || products_items_variations_details_id.sale_price IS NULL)
                                      THEN
                                        products_items_variations_details_id.price
                                    ELSE
                                      products_items_variations_details_id.sale_price
                                    END
                                    )
                                    ELSE
                                      (
                                        CASE WHEN (products_items_id.sale_price = 0 || products_items_id.sale_price IS NULL)
                                          THEN
                                            products_items_id.price
                                        ELSE
                                          products_items_id.sale_price
                                        END
                                      )
                                    END
                                    ) <= '{$toPrice}'
                                    AND
                                    (
                                    CASE WHEN (products_items_id.price = 0 || products_items_id.price IS NULL)
                                      THEN (
                                        CASE WHEN (products_items_variations_details_id.sale_price = 0 || products_items_variations_details_id.sale_price IS NULL)
                                          THEN
                                            products_items_variations_details_id.price
                                        ELSE
                                          products_items_variations_details_id.sale_price
                                        END
                                      )
                                    ELSE
                                      (
                                        CASE WHEN (products_items_id.sale_price = 0 || products_items_id.sale_price IS NULL)
                                          THEN
                                            products_items_id.price
                                        ELSE
                                          products_items_id.sale_price
                                        END
                                      )
                                    END
                                    ) >= '{$fromPrice}'");
            }
        }

        return $query;
    }

    public static function recentViewedProducts($item = null, $countItems = 12)
    {

        $cookieTime = 45000;
        $recentProducts = Cookie::get('recentProducts');

        if (!is_null($item)) {
            if (!is_null($recentProducts)) {
                Cookie::queue(Cookie::forget('recentProducts'));
                $recentProductsArr = array_slice(array_reverse(explode(',', $recentProducts)), 0, $countItems);
                array_push($recentProductsArr, $item->id);
                $recentProducts = array_unique($recentProductsArr);
                Cookie::queue('recentProducts', implode(',', $recentProducts), $cookieTime);
            } else
                Cookie::queue('recentProducts', $item->id, $cookieTime);
        }

        $recentProductsIds = Cookie::get('recentProducts') ? explode(',', Cookie::get('recentProducts')) : [];

        $productsItemsIdRepo = new ProductsItemsIdRepository();

        $products = $productsItemsIdRepo->limit([
            'active' => 1,
            'id' => ['!=', @$item->id],
            'whereIn' => ['id', $recentProductsIds]
        ], $countItems, ['globalName', 'file.file']);

        return $products;
    }

    public static function similarProducts($item, $countItems = 12)
    {
        $sameProductsQuery = ProductsItemsId::where('active', 1)
            ->where('id', '!=', $item->id)
            ->inRandomOrder()
            ->with(['file.file', 'globalName', 'variationsDetails', 'reviews'])
            ->limit($countItems);

        $sameProductsPerCategoryQuery = clone $sameProductsQuery;

        $sameProducts = $sameProductsPerCategoryQuery->whereHas('categories', function ($q) use ($item) {
            if (is_array($item->categoriesIds) && !empty($item->categoriesIds))
                $q->whereIn('products_categories_id.id', $item->categoriesIds);
        })->get();

        if ($sameProducts->isEmpty())
            $sameProducts = $sameProductsQuery->get();

        return $sameProducts;
    }

    public static function recommendedProducts($item, $countItems = 12)
    {

        if (empty($item->recommended_by_categories_ids) || !is_array($item->recommended_by_categories_ids))
            return collect();

        $products = ProductsItemsId::whereHas('categories', function ($q) use ($item) {
            $q->whereIn('products_categories_id.id', $item->recommended_by_categories_ids);
        })
            ->where('id', '!=', $item->id)
            ->where('active', 1)
            ->with(['file.file', 'globalName', 'variationsDetails', 'reviews'])
            ->inRandomOrder()
            ->limit($countItems)
            ->get();

        return $products;
    }

    public static function exportProductsData($product, $attributes)
    {
        $allVariations = collect();
        if ($product->isVariationProduct) {
            $variations = $product->variationsDetails;
            if ($variations->isNotEmpty()) {
                foreach ($variations as $variation) {
                    $allItemVariations = $variation->variations;
                    $currVariationAttributes = collect();

                    if ($allItemVariations->isNotEmpty())
                        foreach ($allItemVariations as $allItemVariation) {
                            $currAttrInfoParent = @$attributes->where('parent.id', $allItemVariation->products_attribute_parent_id)
                                ->first();

                            if (!is_null($currAttrInfoParent)) {
                                $currAttrInfoChildren = !is_null($allItemVariation->products_attribute_child_id) ? $currAttrInfoParent['children']->where('id', $allItemVariation->products_attribute_child_id)->first() : 'all';

                                $currVariationAttributes->push([
                                    'parent' => [
                                        'id' => @$currAttrInfoParent['parent']->id,
                                        'name' => @$currAttrInfoParent['parent']->globalName->name,
                                        'slug' => @$currAttrInfoParent['parent']->slug,
                                        'position' => @$currAttrInfoParent['parent']->position
                                    ],
                                    'child' => !is_null($allItemVariation->products_attribute_child_id) ? [
                                        'id' => @$currAttrInfoChildren->id,
                                        'name' => @$currAttrInfoChildren->globalName->name,
                                        'slug' => @$currAttrInfoChildren->slug
                                    ] : 'all'
                                ]);
                            }
                        }

                    $allVariations->push((object)[
                        'item' => $variation,
                        'attributes' => $currVariationAttributes
                    ]);
                }
            }
        }

        $response = (object)[
            'item' => $product,
            'variations' => $allVariations
        ];

        return $response;
    }

    public static function getAllAttributesStructured()
    {
        $attributes = ProductsAttributesId::with(['globalName', 'type', 'children', 'children.globalName'])->get();
        $allAttributes = collect();

        if ($attributes->isNotEmpty())
            foreach ($attributes as $attr) {
                $options = collect();
                if ($attr->children->isNotEmpty())
                    foreach ($attr->children as $child) {
                        $options->push($child);
                    }

                $allAttributes->push([
                    'parent' => $attr,
                    'children' => $options
                ]);
            }

        return $allAttributes;
    }

    public static function importProductsFiles($productId, $files, $componentId, $iteration, $variationId = null)
    {

        $errorMsg = [];

        $allFiles = explode(',', $files);

        foreach ($allFiles as $file) {
            if ($file == '-' || empty($file))
                continue;

            $existHost = parse_url($file, PHP_URL_HOST);

            if (!$existHost) {
                $errorMsg[] = "File url is invalid from the link `{$file}`.";
                continue;
            }

            if (getAppDomain() !== $existHost) {
                $ch = curl_init($file);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                $fileContent = curl_exec($ch);
                $info = curl_getinfo($ch);
                $retCode = $info['http_code'];
                $fileType = $info['content_type'];
                $fileSize = $info['download_content_length'];

                if (curl_errno($ch)) {
                    $errorMsg[] = "File can't be download from the link `{$file}`.";
                    continue;
                }

                curl_close($ch);

                if ($retCode !== 200 || $fileContent == false) {
                    $errorMsg[] = "File not found the link `{$file}`.";
                    continue;
                }

                if (strpos($fileType, 'image') !== false && $fileSize > 10485760) { // 10 MB
                    $errorMsg[] = "File from the link `{$file}` is too large, this may have maximum 10 MB.";
                    continue;
                }

                $extension = pathinfo($file, PATHINFO_EXTENSION);

                if (strtolower($extension) != 'jpg' && strtolower($extension) != 'jpeg' && strtolower($extension) != 'png') {
                    $errorMsg[] = "File from the link `{$file}` has wrong extension, this can have .jpg, .jpeg or .png extensions.";
                    continue;
                }


                $originalFileName = basename($file);
                $fileName = getCleanFileName($originalFileName) . '-_-' . time() . '.' . $extension;
                $destinationPath = 'files/' . date('Y-m-d') . '/images';
                $originalPath = "{$destinationPath}/{$fileName}";

                $existFile = Files::where('original_name', $originalFileName)
                    ->where('size', $fileSize)
                    ->first();

                $fileItems = new FilesItems();
                $nextPosition = helpers()->getNextItemPosition($fileItems);

                if ($existFile) {

                    FilesItems::firstOrCreate([
                        'file_id' => $existFile->id,
                        'component_id' => $componentId,
                        'item_id' => $productId,
                        'variation_id' => $variationId
                    ], [
                        'lang_id' => null,
                        'active' => $existFile->active,
                        'position' => $nextPosition
                    ]);
                } else {

                    $largePath = "{$destinationPath}/large/";
                    $mediumPath = "{$destinationPath}/medium/";
                    $smallPath = "{$destinationPath}/small/";

                    File::makeDirectory($largePath, 0775, true, true);
                    File::makeDirectory($mediumPath, 0775, true, true);
                    File::makeDirectory($smallPath, 0775, true, true);

                    $newFile = fopen($originalPath, 'w+');
                    fwrite($newFile, $fileContent);
                    fclose($newFile);

                    Image::make($originalPath)->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save($largePath . $fileName);

                    Image::make($originalPath)->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save($mediumPath . $fileName);

                    Image::make($originalPath)->resize(150, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save($smallPath . $fileName);

                    $originalImageResolution = getImageResolution($originalPath);

                    $resolution = array_filter([
                        'width' => @$originalImageResolution->width,
                        'height' => @$originalImageResolution->height
                    ]);

                    $item = Files::create([
                        'file' => $originalPath,
                        'original_name' => $originalFileName,
                        'active' => 1,
                        'type' => substr($fileType, 0, strpos($fileType, '/')),
                        'mime_type' => $fileType,
                        'size' => $fileSize,
                        'resolution' => $resolution,
                    ]);

                    $fileItems->create([
                        'file_id' => $item->id,
                        'component_id' => $componentId,
                        'item_id' => null,
                        'variation_id' => null,
                        'lang_id' => null,
                        'active' => $item->active,
                        'position' => $nextPosition

                    ]);

                    $fileItems->create([
                        'file_id' => $item->id,
                        'component_id' => $componentId,
                        'item_id' => $productId,
                        'variation_id' => $variationId,
                        'lang_id' => null,
                        'active' => $item->active,
                        'position' => $nextPosition + 1
                    ]);
                }

            } else {
                $filePath = parseURL($file)->path;
                if (!file_exists(public_path($filePath))) {
                    $errorMsg[] = "File not found the link `{$file}`.";
                    continue;
                }

                $existFile = Files::where('file', substr($filePath, 1))->first();

                if (!$existFile) {
                    $errorMsg[] = "File not found the link `{$file}`.";
                    continue;
                }

                $fileItems = new FilesItems();
                $nextPosition = helpers()->getNextItemPosition($fileItems);

                FilesItems::firstOrCreate([
                    'file_id' => $existFile->id,
                    'component_id' => $componentId,
                    'item_id' => $productId,
                    'variation_id' => $variationId
                ], [
                    'lang_id' => null,
                    'active' => $existFile->active,
                    'position' => $nextPosition
                ]);
            }
        }

        return (object)[
            'status' => empty($errorMsg),
            'errorMsg' => $errorMsg
        ];

    }



}