<?php

namespace Modules\Products\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\Models\Languages;
use Modules\Products\Http\Requests\ProductsCategoriesRequest;
use Modules\Products\Http\Requests\ProductsPromotionsRequest;
use Modules\Products\Models\ProductsCategoriesId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductsCategoriesController extends DefaultController
{

    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'products';
        $this->path = "{$this->moduleName}::products.categories";
    }

    public function index(Request $request)
    {


        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = ProductsCategoriesId::selectRaw('products_categories_id.*, COUNT(products_items_id.id) as products_count')
            ->leftJoin('products_items_categories', 'products_items_categories.products_category_id', '=', 'products_categories_id.id')
            ->leftJoin('products_items_id', 'products_items_id.id', '=', 'products_items_categories.products_item_id')
            ->where(function ($q) use ($request) {
                if ($request->get('parent'))
                    $q->where('products_categories_id.p_id', $request->get('parent'));
                else
                    $q->whereNull('products_categories_id.p_id');
            })
            ->orderBy('products_categories_id.position')
            ->groupBy('products_categories_id.id')
            ->with(['globalName', 'items',
                'children' => function ($q) {
                    $q->withTrashed();
                }
            ])
            ->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.list", $response);

    }

    public function create()
    {

        $parents = ProductsCategoriesId::where('active', 1)
            ->whereNull('p_id')
            ->with(['globalName', 'children', 'recursiveChildren', 'recursiveChildren.globalName'])
            ->get();

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'parents' => $parents
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id, $langId)
    {

        $parents = ProductsCategoriesId::where('active', 1)
            ->whereNull('p_id')
            ->with(['globalName', 'children', 'recursiveChildren', 'recursiveChildren.globalName'])
            ->get();

        $item = ProductsCategoriesId::with(['files.file', 'itemByLang'])->findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'parents' => $parents,
            'langId' => $langId
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = ProductsCategoriesId::onlyTrashed()->with('globalName')->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(ProductsCategoriesRequest $request, $id, $langId)
    {
        $currComponent = parent::currComponent();
        $parent = ProductsCategoriesId::where('active', 1)
            ->find($request->get('parent'));

        if (is_null($parent) && !is_null($request->get('parent')))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Category not exist!'],
                    'type' => 'warning'
                ]
            ]);

        if (is_null($id) && is_null($langId)) {
            $lang = Languages::where('active', 1)
                ->find($request->get('lang'));

            if (is_null($lang))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Lang not exist!'],
                        'type' => 'warning'
                    ]
                ]);

            $langId = $lang->id;
        }

        $item = ProductsCategoriesId::find($id);

        if (is_null($item)) {
            $item = new ProductsCategoriesId();

            $nextPosition = Helpers::getNextItemPosition($item);
            $item->position = $nextPosition;
            $item->active = 1;
        }

        $nextLevel = Helpers::getNextItemLevel(@$parent->id, $item);

        $item->p_id = @$parent->id;
        $item->slug = $request->get('slug', null);
        $item->level = $nextLevel;
        $item->show_on_main = $request->get('show_on_main') == 'on' ? 1 : 0;

        $item->save();

        $item->itemByLang()->updateOrCreate([
            'products_category_id' => $item->id,
            'lang_id' => $langId
        ], [
            'name' => $request->get('name', null),
            'description' => $request->get('description', null),
            'meta_title' => $request->get('meta_title', null),
            'meta_keywords' => $request->get('meta_keywords', null),
            'meta_description' => $request->get('meta_description', null)
        ]);

//        Helpers::updateValidateDynamicField($request->all(), $currComponent, $id, $langId);
        updateMenuItems($currComponent, $item, $langId);
        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug]) . (!is_null(@$parent->id) ? '?parent=' . @$parent->id : '')
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->globalName->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId]) . (!is_null(@$parent->id) ? '?parent=' . @$parent->id : '')
            ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                ProductsCategoriesId::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = ProductsCategoriesId::whereIn('id', $ItemsId)->with(['globalName',
                    'children' => function ($q) {
                        $q->withTrashed();
                    },
                    'products' => function ($q) {
                        $q->withTrashed();
                    }])->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        if ($item->children->isNotEmpty() || $item->products->isNotEmpty())
                            return response()->json([
                                'status' => false
                            ]);
                        $item->delete();

                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-to-trash');
                        updateMenuItems($currComponent, $item, null, 'destroy', $request->get('event'));
                    }
                }
                break;
            case 'delete-from-trash':
                $items = ProductsCategoriesId::onlyTrashed()->whereIn('id', $ItemsId)->with(['globalName',
                    'children' => function ($q) {
                        $q->withTrashed();
                    },
                    'products' => function ($q) {
                        $q->withTrashed();
                    }])->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->files()->delete();
                        $item->items()->delete();
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = ProductsCategoriesId::onlyTrashed()->whereIn('id', $ItemsId)->with(['globalName',
                    'children' => function ($q) {
                        $q->withTrashed();
                    },
                    'products' => function ($q) {
                        $q->withTrashed();
                    }])->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'restored-from-trash');
                    }
                }
                break;

            default:

                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function updatePosition(Request $request)
    {

        $order = $request->get('order');

        if (!is_array($order))
            return response()->json([
                'status' => false
            ]);

        $itemsPositions = [];
        foreach ($order as $id) {
            $item = ProductsCategoriesId::find($id);
            $itemsPositions[] = @$item->position;
        }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($order as $key => $id) {
            ProductsCategoriesId::where('id', $id)->update(['position' => $itemsPositions[$key]]);
        }

        return response()->json([
            'status' => true
        ]);
    }

    public function activateItem(Request $request)
    {

        $currComponent = parent::currComponent();

        $item = ProductsCategoriesId::with(['globalName'])->findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'activate');
        }

        $item->update(['active' => $status]);

        updateMenuItems($currComponent, $item, null, 'activate');

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

//    Methods for menu
    public function getCategoriesForMenu()
    {
        $items = ProductsCategoriesId::whereNull('p_id')
            ->where('active', 1)
            ->with(['globalName', 'recursiveChildren', 'recursiveChildren.globalName'])
            ->get();

        return $items;
    }

    public function getItem($id)
    {
        $item = ProductsCategoriesId::where('active', 1)
            ->with(['globalName'])
            ->find($id);

        $response = [];

        if (!is_null($item))
            $response = (object)[
                'id' => $item->id,
                'name' => !is_null(@$item->globalName->name) ? $item->globalName->name : null,
                'slug' => $item->slug,
                'allInfo' => $item,
//                'parent' => $item->parent,
//                'items' => $this->getItems()
            ];

        return $response;
    }
//    Methods for menu

}