<?php

namespace Modules\Products\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use Modules\Products\Models\ProductsItemsId;
use Modules\Products\Models\ProductsItemsReviews;
use Illuminate\Http\Request;

class ProductsReviewsController extends DefaultController
{

    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'products';
        $this->path = "{$this->moduleName}::products.reviews";
    }

    public function index(Request $request)
    {

        $response = $this->filter($request);

        $response['path'] = $this->path;

        return view("{$this->path}.list", $response);

    }

    public function filter(Request $request)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $filterParams = array_filter($request->except('page'));

        if (!$request->ajax()) {
            $newFiltersParams = [];

            if (!empty($filterParams) && count($filterParams) > 0) {

                foreach ($filterParams as $key => $one_filter_elem) {
                    $newFiltersParams[$key] = $one_filter_elem;
                    if(strpos($one_filter_elem, '[') !== false || strpos($one_filter_elem, ']') !== false) {
                        $newFiltersParams[$key] = explode(',', substr($filterParams[$key], 1, -1));
                    }
                }
            }

            $filterParams = $newFiltersParams;
        }

        $pushUrl = '';

        if (!empty($filterParams)) {
            foreach ($filterParams as $key => $one_filter_el) {

                if (is_array($one_filter_el)) {
                    $pushUrlArr = '';
                    foreach ($one_filter_el as $k => $filter_el) {
                        $pushUrlArr .= $filter_el . ',';
                    }

                    $pushUrl .= $key . '=[' . strip_tags(substr($pushUrlArr, 0, -1)) . ']&';
                } else {
                    $pushUrl .= $key . '=' . strip_tags(str_replace('[]', '', $one_filter_el)) . '&';
                }
            }

            $pushUrl = '?' . substr($pushUrl, 0, -1);
        }

        $items = ProductsItemsReviews::where(function ($q) use ($filterParams, $request) {

            if (array_key_exists('products', $filterParams) && is_array($filterParams['products']))
                $q->whereHas('product', function ($q) use ($filterParams) {
                    $q->whereIn('products_items_id.id', $filterParams['products']);
                });

            if (array_key_exists('active_inactive', $filterParams) && !is_array($filterParams['active_inactive']) && ($filterParams['active_inactive'] == 'active' || $filterParams['active_inactive'] == 'inactive'))
                $q->where(function ($q) use ($filterParams) {
                    $q->where('active', $filterParams['active_inactive'] == 'active' ? 1 : 0);
                });

            if (array_key_exists('start_date', $filterParams) && !array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']))
                $q->whereDate('created_at', '>=', date('Y-m-d', strtotime($filterParams['start_date'])));
            elseif (!array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['end_date']))
                $q->whereDate('created_at', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));
            elseif (array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']) && !is_array($filterParams['end_date']))
                $q->whereDate('created_at', '>=', date('Y-m-d', strtotime($filterParams['start_date'])))->whereDate('created_at', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));

        })
            ->with(['product', 'product.globalName', 'user'])
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);

        $items->setPath(url(LANG, ['admin', parent::currComponent()->slug]) . $pushUrl);

        $products = collect();

        if (array_key_exists('products', $filterParams) && is_array($filterParams['products']))
            $products = ProductsItemsId::where('active', 1)
                ->whereIn('id', $filterParams['products'])
                ->with('globalName')
                ->orderBy('position')
                ->get();

        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'products' => $products,
            'moduleName' => $this->moduleName,
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
                abort(503);
            }

            return response()->json($response);
        }

        return $response;

    }

    public function view($id)
    {

        $item = ProductsItemsReviews::findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = ProductsItemsReviews::with(['product', 'product.globalName'])
            ->onlyTrashed()
            ->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }


    public function activateItem(Request $request)
    {

        $item = ProductsItemsReviews::findOrFail($request->get('id'));
        $productAvgRating = null;
        if ($item->siblingsReviews->isNotEmpty())
            $productAvgRating = round($item->siblingsReviews->avg('rating'), 2);

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->id]);
            helpers()->logActions(parent::currComponent(), $item, @$item->id, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->id]);
            helpers()->logActions(parent::currComponent(), $item, @$item->id, 'activate');
        }

        $item->update(['active' => $status]);
        $item->product()->update([
            'popularity_rating' => $productAvgRating
        ]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function countItems()
    {
        return ProductsItemsReviews::where('seen', 0)->count();
    }

    public function widgetsCountItems()
    {
        return ProductsItemsReviews::count();
    }

    public function findProducts(Request $request)
    {

        $items = ProductsItemsId::where('active', 1)
            ->where(function ($q) use ($request) {
                $q->where('id', "{$request->get('q')}");
                $q->orWhereHas('globalName', function ($q) use ($request) {
                    $q->where('name', 'LIKE', "%{$request->get('q')}%");
                });
            })
            ->with('globalName')
            ->orderBy('position')
            ->paginate(10);

        $response = selectAjaxSearchItems($request, $items);

        return $response;
    }

    public function destroy(Request $request)
    {

        $deletedItemsId = substr($request->get('id'), 1, -1);

        if (empty($deletedItemsId))
            return response()->json([
                'status' => false
            ]);

        if ($request->get('event') != 'to-trash' && $request->get('event') != 'from-trash' && $request->get('event') != 'restore')
            return response()->json([
                'status' => false
            ]);

        $deletedItemsIds = explode(',', $deletedItemsId);

        if ($request->get('event') == 'to-trash') {
            $items = ProductsItemsReviews::whereIn('id', $deletedItemsIds)->get();
        } else {
            $items = ProductsItemsReviews::onlyTrashed()->whereIn('id', $deletedItemsIds)->get();
        }

        $cartMessage = $responseMsg = '';

        if (!$items->isEmpty()) {
            foreach ($items as $item) {

                $productAvgRating = null;
                if ($item->siblingsReviews->isNotEmpty())
                    $productAvgRating = round($item->siblingsReviews->avg('rating'), 2);

                $cartMessage .= $item->id . ', ';

                if ($request->get('event') == 'to-trash' && !$item->trashed()) {
                    $item->delete();
                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' added to trash' : '';
                    helpers()->logActions(parent::currComponent(), $item, $item->name, 'deleted-to-trash');
                } elseif ($request->get('event') == 'from-trash') {
                    $item->forceDelete();
                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' remove from trash' : '';
                    helpers()->logActions(parent::currComponent(), $item, $item->name, 'deleted-from-trash');
                } elseif ($request->get('event') == 'restore') {
                    $item->restore();
                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' restored from trash' : '';
                    helpers()->logActions(parent::currComponent(), $item, $item->name, 'restored-from-trash');
                }

                $item->product()->update([
                    'popularity_rating' => $productAvgRating
                ]);

            }
        }

        return response()->json([
            'status' => false
        ]);
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                ProductsItemsReviews::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = ProductsItemsReviews::whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $productAvgRating = null;
                        if ($item->siblingsReviews->isNotEmpty())
                            $productAvgRating = round($item->siblingsReviews->avg('rating'), 2);
                        $item->delete();
                        helpers()->logActions(parent::currComponent(), $item, $item->name, 'deleted-to-trash');
                        $item->product()->update([
                            'popularity_rating' => $productAvgRating
                        ]);

                    }
                }
                break;
            case 'delete-from-trash':
                $items = ProductsItemsReviews::onlyTrashed()->whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $productAvgRating = null;
                        if ($item->siblingsReviews->isNotEmpty())
                            $productAvgRating = round($item->siblingsReviews->avg('rating'), 2);
                        $item->forceDelete();
                        helpers()->logActions(parent::currComponent(), $item, $item->name, 'deleted-from-trash');
                        $item->product()->update([
                            'popularity_rating' => $productAvgRating
                        ]);

                    }
                }
                break;
            case 'restore-from-trash':
                $items = ProductsItemsReviews::onlyTrashed()->whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $productAvgRating = null;
                        if ($item->siblingsReviews->isNotEmpty())
                            $productAvgRating = round($item->siblingsReviews->avg('rating'), 2);

                        $item->restore();
                        helpers()->logActions(parent::currComponent(), $item, $item->name, 'restored-from-trash');
                    }

                    $item->product()->update([
                        'popularity_rating' => $productAvgRating
                    ]);

                }
                break;

            default:
                break;
        }

        return response()->json(['status' => true,
            'msg' => ['e' => ['Action successfully applied'],
                'type' => 'info']]);
    }

}