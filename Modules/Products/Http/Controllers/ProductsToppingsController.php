<?php

namespace Modules\Products\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\IikoApi\IikoClient;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Products\Models\ProductsToppingsId;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\Validator;
use App\Models\Languages;
use Modules\Products\Models\ProductsCategoriesId;
use Modules\Products\Models\ProductsItemsId;
use Modules\Products\Models\ProductsPromotionsId;
use Modules\Restaurants\Models\RestaurantsId;


class ProductsToppingsController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'products';
        $this->path = "{$this->moduleName}::products.toppings";
    }

    public function index(Request $request)
    {

//        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());
//
//        $items = ProductsToppingsId::orderBy('products_toppings_id.position')
//            //->groupBy('products_toppings_id.id')
//            ->with(['globalName', 'items'])
//            ->paginate($perPage);
//
//        $response = [
//            'items' => $items,
//            'path' => $this->path,
//            'moduleName' => $this->moduleName,
//            'filterParams' => []
//        ];

        $response = $this->filter($request);

        $response['path'] = $this->path;

        return view("{$this->path}.list", $response);
    }

    public function filter(Request $request)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $filterTableList = filterTableList($request, $request->except('page'));
        $filterParams = $filterTableList->filterParams;
        $pushUrl = $filterTableList->pushUrl;

        $items = ProductsToppingsId::where(function ($q) use ($filterParams) {
            //$q->where('id', 1);
            //$this->filterQuery($q, $filterParams);
            if (array_key_exists('name', $filterParams) && !is_array($filterParams['name'])) {
                $q->whereHas('globalName', function ($q) use ($filterParams) {
                    $q->where('name', 'LIKE', "%{$filterParams['name']}%");
                });
            }

            if (array_key_exists('modifier_type', $filterParams) && !is_array($filterParams['modifier_type'])) {
                $q->where('modifier_type', $filterParams['modifier_type']);
            }
        })->with(['globalName', 'items'])
            ->orderBy('position')
            ->paginate($perPage);

        $items->setPath(url(LANG, ['admin', parent::currComponent()->slug]) . $pushUrl);
        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'moduleName' => $this->moduleName,
            'requestQueryString' => $pushUrl,
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
                abort(503);
            }

            return response()->json($response);
        }

        return $response;
    }

    public function create()
    {

        $parents = ProductsToppingsId::where('active', 1)
            ->with(['globalName'])
            ->get();

        //get iiko products
        $stores = RestaurantsId::where('active', 1)->with('globalName')->get();
        $iiko = new IikoClient($stores[1]);
        $resp = $iiko->getNomenclature();
        $iikoProducts = [];
        if(isset($resp['products']) && !empty($resp['products']))
        {
//            foreach ($resp['products'] as $product){
//                if($product['type'] != 'Dish'){
//                    $iikoProducts[] = $product;
//                }
//            }
            $iikoProducts = $resp['products'];
        }

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'parents' => $parents,
            'iikoProducts' => $iikoProducts
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id, $langId)
    {

//        $parents = ProductsToppingsId::where('active', 1)
//            ->whereNull('p_id')
//            ->with(['globalName', 'children', 'recursiveChildren', 'recursiveChildren.globalName'])
//            ->get();

        $item = ProductsToppingsId::findOrFail($id);

        //get iiko products
        $stores = RestaurantsId::where('active', 1)->with('globalName')->get();

        $iiko = new IikoClient($stores[1]);
        $resp = $iiko->getNomenclature();
        $iikoProducts = [];
        if(isset($resp['products']) && !empty($resp['products']))
        {
//            foreach ($resp['products'] as $product){
//                if($product['type'] != 'Dish'){
//                    $iikoProducts[] = $product;
//                }
//            }

            $iikoProducts = $resp['products'];
        }

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            //'parents' => $parents,
            'langId' => $langId,
            'iikoProducts' => $iikoProducts
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = ProductsToppingsId::onlyTrashed()->with('globalName')->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
            'path' => $this->path,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(Request $request, $id, $langId)
    {

        $currComponent = parent::currComponent();

        $rulesMsg = [];

        $rules = [
            'lang' => 'required',
            'name' => 'required',
            'slug' => "required|unique:products_toppings_id,slug,{$id}",
            'price' => 'required',
        ];

//       Validate dynamic fields

//        $dynamicRules = Helpers::updateValidateDynamicField($request->all(), $currComponent, $id, $langId, 'validate');
//
//        if (!empty($dynamicRules)) {
//            $rules = array_merge($rules, $dynamicRules);
//            foreach ($dynamicRules as $key => $dynamicRule) {
//                $rulesMsg["{$key}.{$dynamicRule}"] = 'The field is required';
//            }
//        }


        $item = Validator::make($request->all(), $rules, $rulesMsg);

        if ($item->fails())
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $item->messages(),
                    'type' => 'error'
                ],
            ]);

//        $parent = ProductsToppingsId::where('active', 1)
//            ->find($request->get('parent'));
//
//        if (is_null($parent) && !is_null($request->get('parent')))
//            return response()->json([
//                'status' => false,
//                'msg' => [
//                    'e' => ['Category not exist!'],
//                    'type' => 'warning'
//                ]
//            ]);

        if (is_null($id) && is_null($langId)) {
            $lang = Languages::where('active', 1)
                ->find($request->get('lang'));

            if (is_null($lang))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Lang not exist!'],
                        'type' => 'warning'
                    ]
                ]);

            $langId = $lang->id;
        }

        $item = ProductsToppingsId::find($id);

        if (is_null($item)) {
            $item = new ProductsToppingsId();

            $nextPosition = Helpers::getNextItemPosition($item);
            $item->position = $nextPosition;
            $item->active = 1;
        }

        //$nextLevel = Helpers::getNextItemLevel(@$parent->id, $item);

        //$item->p_id = @$parent->id;
        $item->slug = $request->get('slug', null);
        $item->price = $request->price ?: 0;
        $item->iiko_id = $request->get('iiko_id', null);
        //$item->modifier_type = $request->get('modifier_type', null);

        $item->save();

        $item->itemByLang()->updateOrCreate([
            'products_toppings_id' => $item->id,
            'lang_id' => $langId
        ], [
            'name' => $request->get('name', null),
            'description' => $request->get('description', null),
//            'meta_title' => $request->get('meta_title', null),
//            'meta_keywords' => $request->get('meta_keywords', null),
//            'meta_description' => $request->get('meta_description', null)
        ]);

        //Helpers::updateValidateDynamicField($request->all(), $currComponent, $id, $langId);
        //updateMenuItems($currComponent, $item, $langId);
        //helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug]) . (!is_null(@$parent->id) ? '?parent=' . @$parent->id : '')
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->globalName->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId]) . (!is_null(@$parent->id) ? '?parent=' . @$parent->id : '')
            ]);
    }

//    public function actionsCheckbox(Request $request)
//    {
//        $currComponent = parent::currComponent();
//
//        $deletedItemsId = substr($request->get('id'), 1, -1);
//
//        if (empty($deletedItemsId))
//            return response()->json([
//                'status' => false
//            ]);
//
//        if ($request->get('event') != 'to-trash' && $request->get('event') != 'from-trash' && $request->get('event') != 'restore')
//            return response()->json([
//                'status' => false
//            ]);
//
//        $deletedItemsIds = explode(',', $deletedItemsId);
//
//        if ($request->get('event') == 'to-trash') {
//            $items = ProductsToppingsId::whereIn('id', $deletedItemsIds)->with(['globalName'])->get();
//        } else {
//            $items = ProductsToppingsId::onlyTrashed()->whereIn('id', $deletedItemsIds)->with(['globalName'])->get();
//        }
//
//        $cartMessage = $responseMsg = '';
//
//        if (!$items->isEmpty()) {
//            foreach ($items as $item) {
//
////                if ($item->children->isNotEmpty() || $item->products->isNotEmpty())
////                    return response()->json([
////                        'status' => false
////                    ]);
//
//                if (!is_null($item->globalName))
//                    $cartMessage .= $item->globalName->name . ', ';
//
//                if ($request->get('event') == 'to-trash' && !$item->trashed()) {
//                    $item->delete();
//                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' added to trash' : '';
//                    helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-to-trash');
//                } elseif ($request->get('event') == 'from-trash') {
//                    $item->files()->delete();
//                    $item->items()->delete();
//                    $item->forceDelete();
//                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' remove from trash' : '';
//                    helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-from-trash');
//                } elseif ($request->get('event') == 'restore') {
//                    $item->restore();
//                    $responseMsg = !empty($cartMessage) ? substr($cartMessage, 0, -2) . ' restored from trash' : '';
//                    helpers()->logActions($currComponent, $item, @$item->globalName->name, 'restored-from-trash');
//                }
//
//                //updateMenuItems($currComponent, $item, null, 'destroy', $request->get('event'));
//
//            }
//
//            return response()->json([
//                'status' => true,
//                'cart_messages' => $responseMsg,
//                'items' => $deletedItemsIds
//            ]);
//        }
//
//        return response()->json([
//            'status' => false
//        ]);
//    }
    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                ProductsToppingsId::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = ProductsToppingsId::whereIn('id', $ItemsId)->with(['globalName'])->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = ProductsToppingsId::onlyTrashed()->whereIn('id', $ItemsId)->with(['globalName'])->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->items()->delete();
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = ProductsToppingsId::onlyTrashed()->whereIn('id', $ItemsId)->with(['globalName'])->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'restored-from-trash');
                    }
                }
                break;

            default:

                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function updatePosition(Request $request)
    {

        $order = $request->get('order');

        if (!is_array($order))
            return response()->json([
                'status' => false
            ]);

        $itemsPositions = [];
        foreach ($order as $id) {
            $item = ProductsToppingsId::find($id);
            $itemsPositions[] = @$item->position;
        }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($order as $key => $id) {
            ProductsToppingsId::where('id', $id)->update(['position' => $itemsPositions[$key]]);
        }

        return response()->json([
            'status' => true
        ]);
    }

    public function activateItem(Request $request)
    {

        $currComponent = parent::currComponent();

        $item = ProductsToppingsId::with(['globalName'])->findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'activate');
        }

        $item->update(['active' => $status]);

        updateMenuItems($currComponent, $item, null, 'activate');

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

//    //    Methods for menu
//
//    public function gettoppingsForMenu()
//    {
//        $items = ProductsToppingsId::whereNull('p_id')
//            ->where('active', 1)
//            ->with(['globalName', 'recursiveChildren', 'recursiveChildren.globalName'])
//            ->get();
//
//        return $items;
//    }

    public function getItem($id)
    {
        $item = ProductsToppingsId::where('active', 1)
            ->with(['globalName'])
            ->find($id);

        $response = [];

        if (!is_null($item))
            $response = (object)[
                'id' => $item->id,
                'name' => !is_null(@$item->globalName->name) ? $item->globalName->name : null,
                'slug' => $item->slug,
                'allInfo' => $item,
//                'parent' => $item->parent,
//                'items' => $this->getItems()
            ];

        return $response;
    }

//    public function setItemToStore(Request $request, $storeid)
//    {
//
//        $product = ProductsToppingsId::find($request->id);
//
//        if (is_null($product))
//            return response()->json([
//                'status' => false
//            ]);
//
//        if($product->store_id == 0){
//            $activeForChisinau = 1;
//            $activeForIaloveni = 1;
//        }elseif($product->store_id == 1){
//            $activeForChisinau = 1;
//            $activeForIaloveni = 0;
//        }elseif($product->store_id == 2){
//            $activeForChisinau = 0;
//            $activeForIaloveni = 1;
//        }else{
//            $activeForChisinau = 0;
//            $activeForIaloveni = 0;
//        }
//
//
//        if($storeid == 1){
//            if($request->active == 1) $activeForChisinau = 0;//se doreste dezactivare
//            else $activeForChisinau = 1;//se doreste activare
//        }elseif ($storeid == 2){
//            if($request->active == 1) $activeForIaloveni = 0;//se doreste dezactivare
//            else $activeForIaloveni = 1;//se doreste activare
//        }
//
//        if(($activeForChisinau == 1 && $product->store_id == 2) || ($activeForIaloveni == 1 && $product->store_id == 1)){
//            $product->store_id = 0;
//        }elseif ($activeForChisinau == 1 && ($product->store_id == 0 || $product->store_id == -1)){
//            $product->store_id = 1;
//        }elseif ($activeForIaloveni == 1 && ($product->store_id == 0 || $product->store_id == -1)){
//            $product->store_id = 2;
//        }else{
//            $product->store_id = -1;
//        }
//
//        $product->save();
//
//        return response()->json([
//            'status' => true
//        ]);
//    }
}
