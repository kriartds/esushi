<?php

namespace Modules\Products\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\Models\Languages;
use Modules\Products\Http\Requests\ProductsAttributesRequest;
use Modules\Products\Models\ProductsAttributesId;
use Modules\Products\Models\ProductsAttributesTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductsAttributesController extends DefaultController
{

    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'products';
        $this->path = "{$this->moduleName}::products.attributes";
    }

    public function index(Request $request)
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = ProductsAttributesId::where(function ($q) use ($request) {
            if ($request->get('parent'))
                $q->where('p_id', $request->get('parent'));
            else
                $q->whereNull('p_id');
        })
            ->orderBy('position');

        if ($request->get('parent'))
            $items->with(['globalName', 'items', 'type', 'children.globalName', 'file.file', 'attrOptions', 'children' => function ($q) {
                $q->withTrashed();
            }]);
        else
            $items->with(['globalName', 'items', 'type', 'children.globalName', 'children.file.file', 'attrOptions', 'children' => function ($q) {
                $q->withTrashed();
            }]);

        $items = $items->paginate($perPage);

        $parent = null;

        if ($request->get('parent'))
            $parent = ProductsAttributesId::where('active', 1)
                ->whereNull('p_id')
                ->with(['type'])
                ->find($request->get('parent'));

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'parent' => $parent
        ];

//        dd($response);

        return view("{$this->path}.list", $response);

    }

    public function create(Request $request)
    {

        $parent = ProductsAttributesId::where('active', 1)
            ->whereNull('p_id')
            ->find($request->get('parent'));

        $types = ProductsAttributesTypes::get();

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'parent' => $parent,
            'types' => $types
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit(Request $request, $id, $langId)
    {

        $parent = ProductsAttributesId::where('active', 1)
            ->whereNull('p_id')
            ->find($request->get('parent'));

        $types = ProductsAttributesTypes::get();

        $item = ProductsAttributesId::with(['files.file', 'itemByLang'])->findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'parent' => $parent,
            'langId' => $langId,
            'types' => $types
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $items = ProductsAttributesId::onlyTrashed()->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(ProductsAttributesRequest $request, $id, $langId)
    {
        $currComponent = parent::currComponent();

        $attrType = ProductsAttributesTypes::find($request->get('type'));

        if (is_null($attrType) && !$request->get('parent'))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Attribute type not exist!'],
                    'type' => 'warning'
                ]
            ]);

        if (!$request->get('parent') && !$request->get('field_type') && $attrType->slug == 'select')
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Field type is required!'],
                    'type' => 'warning'
                ]
            ]);

        $attrTypeId = @$attrType->id;

        $parent = ProductsAttributesId::where('active', 1)
            ->whereNull('p_id')
            ->find($request->get('parent'));

        if (is_null($parent) && $request->get('parent'))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Attribute not exist!'],
                    'type' => 'warning'
                ]
            ]);

        $parentId = @$parent->id;

        if (@$parent->type->slug == 'image' && !$request->get('files'))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['File is required!'],
                    'type' => 'warning'
                ]
            ]);

        if (@$parent->type->slug == 'color' && !$request->get('color'))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Color is required!'],
                    'type' => 'warning'
                ]
            ]);

        if (is_null($id) && is_null($langId)) {
            $lang = Languages::where('active', 1)
                ->find($request->get('lang'));

            if (is_null($lang))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Lang not exist!'],
                        'type' => 'warning'
                    ]
                ]);

            $langId = $lang->id;
        }

        $item = ProductsAttributesId::find($id);

        if (is_null($item)) {
            $item = new ProductsAttributesId();

            $nextPosition = Helpers::getNextItemPosition($item);
            $item->position = $nextPosition;
            $item->active = 1;
        }

        $item->p_id = $parentId;
        $item->attribute_type_id = $attrTypeId;
        $item->slug = $request->get('slug', null);
        $item->color = $request->get('color', null);
        $item->field_type = @$attrType->slug == 'select' ? $request->get('field_type') : (!is_null(@$attrType->slug) ? 'checkbox' : null);
        $item->range_value = @$parent->field_type == 'range' ? $request->get('name', null) : null;
        $item->display_in_filter = $request->get('display_in_filter') == 'on' ? 1 : 0;

        $item->save();

        if (@$parent->field_type != 'range') {
            $item->itemByLang()->updateOrCreate([
                'products_attribute_id' => $item->id,
                'lang_id' => $langId
            ], [
                'name' => $request->get('name', null),
                'description' => $request->get('description', null)
            ]);
        }

        Helpers::updateValidateDynamicField($request->all(), $currComponent, $id, $langId);

        if (@$parent->type->slug == 'image' && $request->get('files'))
            helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');

        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug]) . (!is_null(@$parent->id) ? '?parent=' . @$parent->id : '')
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item {$item->id} was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId]) . (!is_null(@$parent->id) ? '?parent=' . @$parent->id : '')
            ]);
    }

     public function actionsCheckbox(Request $request)
    {

        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                ProductsAttributesId::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = ProductsAttributesId::whereIn('id', $ItemsId)->with(['globalName', 'children' => function ($q) {
                    $q->withTrashed();
                }])->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        if ($item->children->isNotEmpty())
                            return response()->json([
                                'status' => false
                            ]);
                        $item->delete();
                        helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = ProductsAttributesId::onlyTrashed()->whereIn('id', $ItemsId)->with(['globalName', 'children' => function ($q) {
                    $q->withTrashed();
                }])->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->files()->delete();
                        $item->items()->delete();
                        $item->forceDelete();

                        helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = ProductsAttributesId::onlyTrashed()->whereIn('id', $ItemsId)->with(['globalName', 'children' => function ($q) {
                    $q->withTrashed();
                }])->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'restored-from-trash');
                    }
                }
                break;

            default:

                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function updatePosition(Request $request)
    {

        $order = $request->get('order');

        if (!is_array($order))
            return response()->json([
                'status' => false
            ]);

        $itemsPositions = [];
        foreach ($order as $id) {
            $item = ProductsAttributesId::find($id);
            $itemsPositions[] = @$item->position;
        }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($order as $key => $id) {
            ProductsAttributesId::where('id', $id)->update(['position' => $itemsPositions[$key]]);
        }

        return response()->json([
            'status' => true
        ]);
    }

    public function activateItem(Request $request)
    {

        $item = ProductsAttributesId::findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->globalName->name]);
            helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->globalName->name]);
            helpers()->logActions(parent::currComponent(), $item, @$item->globalName->name, 'activate');
        }

        $item->update(['active' => $status]);

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

//    Required methods
    public function getItem($id)
    {
        $item = ProductsAttributesId::where('active', 1)
            ->with('globalName')
            ->find($id);

        $response = [];

        if (!is_null($item))
            $response = (object)[
                'id' => $item->id,
                'name' => !is_null(@$item->globalName->name) ? $item->globalName->name : null,
                'slug' => $item->slug,
            ];

        return $response;
    }
//    Required methods
}