<?php

namespace Modules\Products\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\IikoApi\Models\IikoProducts;
use App\Models\Languages;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\File;
use Modules\Products\Models\ProductsAttributesId;
use Modules\Products\Models\ProductsCategoriesId;
use Modules\Products\Models\ProductsItems;
use Modules\Products\Models\ProductsItemsAttributes;
use Modules\Products\Models\ProductsItemsAttributesOptions;
use Modules\Products\Models\ProductsItemsCategories;
use Modules\Products\Models\ProductsItemsId;
use Modules\Products\Models\ProductsItemsPromotions;
use Modules\Products\Models\ProductsItemsTypes;
use Modules\Products\Models\ProductsItemsVariations;
use Modules\Products\Models\ProductsItemsVariationsDetails;
use Modules\Products\Models\ProductsItemsVariationsDetailsId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Products\Models\ProductsPromotionsId;
use Modules\Products\Models\ProductsToppingsId;
use Modules\Products\Notifications\ExportProductsNotification;
use Modules\Products\Notifications\ImportProductsNotification;
use Nwidart\Modules\Facades\Module;
use Modules\Products\Http\Helpers\Helpers as ProductsHelpers;

class ProductsController extends DefaultController
{

    private $moduleName;
    private $path;
    private $maxGeneratedVariations;

    public function __construct()
    {

        $this->moduleName = 'products';
        $this->path = "{$this->moduleName}::products";
        $this->maxGeneratedVariations = 10;
    }

    public function index(Request $request)
    {

        $response = $this->filter($request);
        $response['path'] = $this->path;
        return view("{$this->path}.list", $response);
    }

    public function filter(Request $request)
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $filterTableList = filterTableList($request, $request->except('page'));
        $filterParams = $filterTableList->filterParams;
        $pushUrl = $filterTableList->pushUrl;

        $items = ProductsItemsId::where(function ($q) use ($filterParams) {
            $this->filterQuery($q, $filterParams);
        })->with(['globalName', 'items', 'categories.globalName', 'type', 'variationsDetails', 'categories'])
            ->orderBy('id', 'desc')
//            ->orderBy('position')
            ->paginate($perPage);

        $items->setPath(url(LANG, ['admin', parent::currComponent()->slug]) . $pushUrl);

        $categories = ProductsCategoriesId::where('active', 1)
            ->whereNull('p_id')
            ->with(['globalName', 'recursiveChildren'])
            ->get();


        $promotions = ProductsPromotionsId::where('active', 1)->with(['globalName'])->get();

        $productsTypes = ProductsItemsTypes::get();

        $response = [
            'status' => true,
            'count' => $items->total(),
            'pushUrl' => $pushUrl,
            'filterParams' => $filterParams,
            'items' => $items,
            'categories' => $categories,
            'promotions' => $promotions,
            'productsTypes' => $productsTypes,
            'moduleName' => $this->moduleName,
            'requestQueryString' => $pushUrl
        ];

        if ($request->ajax()) {
            try {
                $response['view'] = view("{$this->path}.table", $response)->render();
            } catch (\Throwable $e) {
                abort(503);
            }

            return response()->json($response);
        }

        return $response;

    }

    public function create()
    {

        $parents = ProductsCategoriesId::where('active', 1)
            ->whereNull('p_id')
            ->with(['globalName', 'recursiveChildren'])
            ->get();

        $types = ProductsItemsTypes::get();


        $promotions = ProductsPromotionsId::where('active', 1)->with(['globalName'])->get();
        $toppings = ProductsToppingsId::where('active', 1)->with(['globalName'])->get();

        $attributes = ProductsAttributesId::where('active', 1)
            ->whereNull('p_id')
            ->orderBy('position')
            ->with(['globalName'])
            ->get();

        $iiko_products = IikoProducts::all();

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'parents' => $parents,
            'types' => $types,
            'promotions' => $promotions,
            'attributes' => $attributes,
            'toppings' => $toppings,
            'iiko_products' => $iiko_products
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function edit($id, $langId)
    {

        $parents = ProductsCategoriesId::where('active', 1)
            ->whereNull('p_id')
            ->with(['globalName', 'recursiveChildren'])
            ->get();

        $types = ProductsItemsTypes::get();

        $promotions = ProductsPromotionsId::where('active', 1)->with(['globalName'])->get();

        $toppings = ProductsToppingsId::where('active', 1)->with(['globalName'])->get();

        $allAttributes = ProductsAttributesId::where('active', 1)
            ->orderBy('position')
            ->with(['globalName', 'children', 'children.globalName', 'children.parent'])
            ->get();

        $attributes = $allAttributes->where('p_id', null);

        $item = ProductsItemsId::with(['productAttributes', 'files.file', 'productItemsOptions', 'productItemsAttributes', 'productItemsAttributes.options'])->findOrFail($id);

        $selectedAttributes = $attributes->whereIn('id', $item->attributesParentsIds);

        $selectedAttributesOptions = $item->productItemsOptions()->whereHas('productsAttr', function ($q) use ($item) {
            $q->where('products_item_id', $item->id);
        })->get()->pluck('products_attribute_id')->toArray();

//        Variations

        $variationsDetailsCount = ProductsItemsVariationsDetailsId::where('products_item_id', $item->id)->count();
        $variationAttributes = ProductsHelpers::getProductSelectedAttributes($item, true);
        $defaultProductVariation = ProductsHelpers::getProductDefaultVariation($item, $variationAttributes);

//        Variations

        $iiko_products = IikoProducts::all();

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'parents' => $parents,
            'langId' => $langId,
            'selectedAttributes' => $selectedAttributes,
            'selectedAttributesOptions' => $selectedAttributesOptions,
            'types' => $types,
            'promotions' => $promotions,
            'attributes' => $attributes,
            'toppings' => $toppings,
            'paginationVariations' => (object)[
                'total' => $variationsDetailsCount,
                'lastPage' => (int)ceil($variationsDetailsCount / $this->maxGeneratedVariations),
                'currPage' => 1,
                'perPage' => $this->maxGeneratedVariations
            ],
            'defaultVariationAttributes' => @$defaultProductVariation->defaultVariationsAttrNames ?: [],
            'iiko_products' => $iiko_products,
        ];

        return view("{$this->path}.createEdit", $response);
    }

    public function trash()
    {
        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = ProductsItemsId::onlyTrashed()->with(['globalName', 'categories', 'categories.globalName'])->paginate($perPage);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'items' => $items,
        ];

        return view("{$this->path}.trash", $response);
    }

    public function save(Request $request, $id, $langId)
    {
        $currComponent = parent::currComponent();

        $rulesMsg = [];

        $rules = [
            'lang' => 'required',
            'product_type' => 'required',
            'name' => 'required',
            'slug' => "required|unique:products_items_id,slug,{$id}",
            'sku' => "nullable|unique:products_items_id,sku,{$id}",
            'price' => 'nullable|lower_than_field:sale_price|numeric',
            'sale_price' => 'nullable|numeric',
        ];

//        Variations

        $variationPriceErr = $arr_rules = [];

        if ($request->get('item_variations')) {
            if ((@count($request->get('item_variations')['attr_ids']) < 1 || is_null(@count($request->get('item_variations')['price_variation']))) && @count($request->get('item_variations')['price_variation']) != @count($request->get('item_variations')['sale_price_variation']))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Add minim one variation!'],
                        'type' => 'warning'
                    ]
                ]);

            if (@$request->get('item_variations')['price_variation'])
                foreach ($request->get('item_variations')['price_variation'] as $key => $val) {

                    $rules["item_variations.price_variation.{$key}"] = 'nullable|lower_than_field:sale_price|numeric';
                    $rules["item_variations.sale_price_variation.{$key}"] = 'nullable|numeric';

                    $rulesMsg["item_variations.price_variation.{$key}.numeric"] = 'The price must be a number.';
                    $rulesMsg["item_variations.sale_price_variation.{$key}.numeric"] = 'The price must be a number.';

                    if ($val < $request->get('item_variations')['sale_price_variation'][$key])
                        $variationPriceErr[] = [
                            'rule' => 'item_variations.price_variation.' . $key . '.' . 0,
                            'msg' => 'The price may not be lower than sale price'
                        ];

                }
        }

//        Variations

//       Validate dynamic fields

        $dynamicRules = Helpers::updateValidateDynamicField($request->all(), $currComponent, $id, $langId, 'validate');

        if (!empty($dynamicRules)) {
            $rules = array_merge($rules, $dynamicRules);
            foreach ($dynamicRules as $key => $dynamicRule) {
                $rulesMsg["{$key}.{$dynamicRule}"] = 'The field is required';
            }
        }

//       Validate dynamic fields

        $item = Validator::make($request->all(), $rules, $rulesMsg);

        $validatorMsg = $item->messages();
        $validatorFails = $item->fails();

        if (!empty($variationPriceErr)) {
            foreach ($variationPriceErr as $priceErr) {
                $validatorMsg->add($priceErr['rule'], $priceErr['msg']);
            }

            $validatorFails = true;
        }


        if ($validatorFails)
            return response()->json([
                'status' => false,
                'validator' => true,
                'msg' => [
                    'e' => $validatorMsg,
                    'type' => 'error'
                ],
            ]);


        if (!$request->get('categories'))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Select minim one category!'],
                    'type' => 'warning'
                ]
            ]);

        $categories = ProductsCategoriesId::where('active', 1)
            ->whereIn('id', $request->get('categories'))
            ->get();

        if ($categories->isEmpty())
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Categories not exist!'],
                    'type' => 'warning'
                ]
            ]);

        $promotions = ProductsPromotionsId::where('active', 1)
            ->whereIn('id', $request->get('promotions', []))
            ->get();

        if (is_null($id) && is_null($langId)) {
            $lang = Languages::where('active', 1)
                ->find($request->get('lang'));

            if (is_null($lang))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Lang not exist!'],
                        'type' => 'warning'
                    ]
                ]);

            $langId = $lang->id;
        }

        $productType = ProductsItemsTypes::find($request->get('product_type'));

        if (is_null($productType))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Product type not exist!'],
                    'type' => 'warning'
                ]
            ]);

        $recommendedCategoriesIds = $request->get('recommended_categories', []);

        if (!empty($recommendedCategoriesIds)) {
            $recommendedCategoriesIds = ProductsCategoriesId::where('active', 1)
                ->whereIn('id', $recommendedCategoriesIds)
                ->get()
                ->pluck('id')
                ->toArray();
        }

        $item = ProductsItemsId::find($id);

        if (is_null($item)) {
            $item = new ProductsItemsId();

            $nextPosition = Helpers::getNextItemPosition($item);
            $item->position = $nextPosition;
            $item->active = 1;
        }

        $price = $salePrice = null;

        if ($productType->slug == 'simple') {
            $price = $request->get('price', null) ? (float)$request->get('price') : null;
            $salePrice = $request->get('sale_price', null) ? (float)$request->get('sale_price') : null;
        }

        $item->slug = $request->get('slug', null);
        $item->item_type_id = $productType->id;
        $item->price = $price;
        $item->sale_price = $salePrice;
        $item->discount_percentage = $request->get('discount_percentage', null);
        $item->use_stock = $request->get('use_stock') == 'on' ? 1 : 0;
        $item->stock_quantity = $request->get('use_stock') == 'on' ? $request->get('stock_quantity', 0) : null;
        $item->status = $request->get('stock_status', null);
        $item->sku = $request->get('sku', null);
        $item->incomplete_data = 0;
        $item->show_on_main = $request->get('show_on_main') == 'on' ? 1 : 0;
        $item->recommended_by_categories_ids = $recommendedCategoriesIds;
        $item->badges = $request->get('badges', null);
        $item->toppings_ids = $request->get('toppings', null);
        $item->video = $request->get('video', null);
        $item->iiko_id = $request->get('iiko_id', null);

        $item->save();

        $item->itemByLang()->updateOrCreate([
            'products_item_id' => $item->id,
            'lang_id' => $langId
        ], [
            'name' => $request->get('name', null),
            'description' => $request->get('description', null),
            'meta_title' => $request->get('meta_title', null),
            'meta_keywords' => $request->get('meta_keywords', null),
            'meta_description' => $request->get('meta_description', null),
            'weight' => $request->get('weight', null),
        ]);

//        Create relation products ~ categories

        $categoriesData = [];
        ProductsItemsCategories::where('products_item_id', $item->id)->delete();
        foreach ($categories as $category) {
            $categoriesData[] = [
                'products_item_id' => $item->id,
                'products_category_id' => $category->id
            ];
        }
        if (!empty($categoriesData))
            ProductsItemsCategories::insert($categoriesData);
//        Create relation products ~ promotions


        $promotionsData = [];
        ProductsItemsPromotions::where('products_item_id', $item->id)->delete();
        foreach ($promotions as $promotion) {
            $promotionsData[] = [
                'products_item_id' => $item->id,
                'products_promotion_id' => $promotion->id
            ];
        }
        if (!empty($promotionsData))
            ProductsItemsPromotions::insert($promotionsData);

//        Create relation products ~ promotions

        $allAttributes = ProductsHelpers::getAllAttributesStructured();

        if (!empty($request->get('attributes_opt', []))) {

            $existItemsAttributes = $item->productItemsAttributes;
            if ($existItemsAttributes->isNotEmpty())
                foreach ($existItemsAttributes as $existItemsAttribute) {
                    $existItemsAttribute->options()->delete();
                    if (!@$request->get('attributes_opt')[$existItemsAttribute->products_parent_attribute_id]['use_variation'])
                        ProductsItemsVariations::whereHas('variationDetailId', function ($q) use ($item) {
                            $q->where('products_item_id', $item->id);
                        })->where('products_attribute_parent_id', $existItemsAttribute->products_parent_attribute_id)->delete();
                }

            $item->productItemsAttributes()->delete();

            foreach ($request->get('attributes_opt') as $attributeId => $option) {
                $currAttributes = $allAttributes->where('parent.id', $attributeId)->first();

                if (is_null($currAttributes))
                    continue;

                $attribute = @$currAttributes['parent'];
                $childrenAttr = @$currAttributes['children'];

                if (is_null($attribute) || is_null($childrenAttr))
                    continue;

                if (array_key_exists('children', $option) && !empty(array_filter($option['children'])) && array_key_exists('on_product_page', $option) && array_key_exists('use_variation', $option)) {

                    $itemAttribute = ProductsItemsAttributes::create([
                        'products_item_id' => $item->id,
                        'products_parent_attribute_id' => $attribute->id,
                        'visible_on_product_page' => $option['on_product_page'] == 'on' ? 1 : 0,
                        'for_variation' => $option['use_variation'] == 'on' ? 1 : 0
                    ]);

                    foreach ($option['children'] as $child) {
                        $childAttr = $childrenAttr->where('id', $child)->first();

                        if (is_null($childAttr))
                            continue;

                        ProductsItemsAttributesOptions::create([
                            'products_items_attribute_id' => $itemAttribute->id,
                            'products_attribute_id' => $childAttr->id
                        ]);
                    }

                }
            }

        }

//        Create relation products ~ attributes


        if (!$request->get('save_attributes_events', null) && $item->isVariationProduct) {

//        Create variations products ~ attributes

            $this->saveProductVariations($request, $item, $langId, $currComponent, $allAttributes);

//        Create variations products ~ attributes
        }


        Helpers::updateValidateDynamicField($request->all(), $currComponent, $id, $langId);

        updateMenuItems($currComponent, $item, $langId);

        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');


        if (!$request->get('save_attributes_events', null)) {
            if (is_null($id))
                return response()->json([
                    'status' => true,
                    'msg' => [
                        'e' => "Item was successful created!",
                        'type' => 'success'
                    ],
                    'redirect' => url(LANG, ['admin', $currComponent->slug])
                ]);
            else
                return response()->json([
                    'status' => true,
                    'msg' => [
                        'e' => "Item, {$item->globalName->name}, was successful edited",
                        'type' => 'success'
                    ],
                    'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId])
                ]);
        } else {
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Attributes was successful saved!",
                    'type' => 'info'
                ],
                'itemId' => @$item->id,
                'editUrl' => is_null($id) ? url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId]) : null
            ]);
        }
    }

    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':
                ProductsItemsId::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'set_percent':
                $items = ProductsItemsId::whereIn('id', $ItemsId)->get();
                $percentage = $request->get('action');

                if ($items->isNotEmpty()) {
                    foreach ($items as $item) {
                        if ($item->isVariationProduct) {
                            $item->variationsDetails->each(function ($q) use ($percentage) {
                                $q->each(function ($q) use ($percentage) {
                                    if ($q->price && $percentage) {
                                        $q->sale_price = $q->price - ($percentage / 100) * $q->price;
                                        $q->save();
                                    } else {
                                        $q->sale_price = null;
                                        $q->save();
                                    }
                                });
                            });
                        } else {
                            if ($item->price && $percentage) {
                                $item->sale_price = $item->price - ($percentage / 100) * $item->price;
                                $item->save();
                            } else {
                                $item->sale_price = null;
                                $item->save();
                            }
                        }
                    }
                }
                break;
            case 'delete-to-trash':
                $items = ProductsItemsId::whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $fileSystem = new Filesystem();
                        if (Module::has('Orders') && $fileSystem->exists(Module::getPath() . '/Orders/Models/CartProducts.php'))
                            try {
                                $item->cartProducts()->whereHas('cart', function ($q) {
                                    $q->whereNull('status');
                                })->delete();
                            } catch (\Exception $e) {
                            }
                        $item->delete();

                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = ProductsItemsId::onlyTrashed()->whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->files()->delete();

                        $item->productItemsAttributes->each(function ($q) {
                            $q->options()->delete();
                        });

                        $item->productItemsAttributes()->delete();

                        $item->variationsDetails->each(function ($q) {
                            $q->files()->delete();
                            $q->items()->delete();
                            $q->variations()->delete();
                        });

                        $item->variationsDetails()->delete();
                        $item->productCategories()->delete();
                        $item->productPromotions()->delete();
                        $item->items()->delete();
                        $item->forceDelete();
                        helpers()->logActions($currComponent, $item, $item->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = ProductsItemsId::onlyTrashed()->whereIn('id', $ItemsId)->get();

                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                        helpers()->logActions($currComponent, $item, $item->name, 'restored-from-trash');
                    }
                }
                break;

            default:

                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function updatePosition(Request $request)
    {

        $order = $request->get('order');

        if (!is_array($order))
            return response()->json([
                'status' => false
            ]);

        $itemsPositions = [];
        foreach ($order as $id) {
            $item = ProductsItemsId::find($id);
            $itemsPositions[] = @$item->position;
        }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($order as $key => $id) {
            ProductsItemsId::where('id', $id)->update(['position' => $itemsPositions[$key]]);
        }

        return response()->json([
            'status' => true
        ]);
    }

    public function activateItem(Request $request)
    {

        $currComponent = parent::currComponent();

        $item = ProductsItemsId::findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'inactivate');

//                Destroy product from cart
            $fileSystem = new Filesystem();
            if (Module::has('Orders') && $fileSystem->exists(Module::getPath() . '/Orders/Models/CartProducts.php'))
                try {
                    $item->cartProducts()->whereHas('cart', function ($q) {
                        $q->whereNull('status');
                    })->delete();
                } catch (\Exception $e) {
                }
//                Destroy product from cart
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'activate');
        }

        $item->update(['active' => $status]);

        updateMenuItems($currComponent, $item, null, 'activate');

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function showOnMain(Request $request)
    {
        $currComponent = parent::currComponent();
        $item = ProductsItemsId::findOrFail($request->get('id'));

        $item->update(['show_on_main' => !(int)$request->get('active')]);

        if ($request->get('active')) {
            $msg = __("{$this->moduleName}::e.element_is_hidden", ['name' => @$item->globalName->name]);
        } else {
            $msg = __("{$this->moduleName}::e.element_is_visible", ['name' => @$item->globalName->name]);
        }

        try {
            helpers()->logActions($currComponent, $item, @$item->globalName->name, $request->get('action'));
        } catch (\Throwable $e) {

        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    public function getAttributes(Request $request)
    {
        $productTypeId = $request->get('productType', null);
        $itemIds = $request->get('itemIds', []);

        $items = ProductsAttributesId::where('active', 1)
            ->whereNull('p_id')
            ->whereIn('id', $itemIds)
            ->orderBy('position')
            ->with(['globalName', 'children.globalName', 'children' => function ($q) {
                $q->where('active', 1);
            }])
            ->get();

        if ($items->isEmpty())
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Attributes not exist'],
                    'type' => 'warning',
                ]
            ]);

        $view = '';
        $selectedIds = [];

        foreach ($items as $item) {
            $selectedIds[] = $item->id;

            try {
                $response = [
                    'item' => $item,
                    'isVariationProduct' => $productTypeId == 2,
                    'moduleName' => $this->moduleName,
                ];
                $view .= view("{$this->path}.templates.attributesItems", $response)->render();
            } catch (\Throwable $e) {
                continue;
            }
        }

        return response()->json([
            'status' => true,
            'selectedIds' => $selectedIds,
            'view' => $view,
        ]);
    }

    public function destroyAttribute(Request $request)
    {
        $itemAttribute = ProductsItemsAttributes::where('products_parent_attribute_id', $request->get('itemId'))
            ->where('products_item_id', $request->get('productId'))
            ->first();

        if (is_null($itemAttribute))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Attribute not exist'],
                    'type' => 'warning',
                ]
            ]);


        $productsItemsVariations = ProductsItemsVariations::where('products_attribute_parent_id', $itemAttribute->products_parent_attribute_id)
            ->whereHas('variationDetailId', function ($q) use ($itemAttribute) {
                $q->where('products_item_id', $itemAttribute->products_item_id);
            })->get();

        if ($productsItemsVariations->isNotEmpty()) {
            if (!$request->get('confirmDeleteAttr', false))
                return response()->json([
                    'status' => true,
                    'confirmDeleteAttr' => true,
                    'confirmMsg' => 'This attribute is contained in variations. If you delete it, the associated variations will be deleted. Continue deleting this attribute?'
                ]);
            else
                $productsItemsVariations->each(function ($item) {
                    $item->delete();
                });
        }


        $itemAttribute->options()->delete();
        $itemAttribute->delete();

        $allItemsAttributesCount = ProductsItemsAttributes::where('products_item_id', $request->get('productId'))->count();

        if (!$allItemsAttributesCount) {
            $variations = ProductsItemsVariationsDetailsId::where('products_item_id', $request->get('productId'))->get();
            if ($variations->isNotEmpty()) {
                foreach ($variations as $variation) {

//                Destroy product from cart
                    $fileSystem = new Filesystem();
                    if (Module::has('Orders') && $fileSystem->exists(Module::getPath() . '/Orders/Models/CartProducts.php'))
                        try {
                            $variation->cartVariations()->whereHas('cart', function ($q) {
                                $q->whereNull('status');
                            })->delete();
                        } catch (\Exception $e) {
                        }
//                Destroy product from cart
//
                    $variation->delete();
                }
            }
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Attribute was deleted successful!'],
                'type' => 'info',
            ]
        ]);
    }

    public function saveProductVariations($request, $item, $langId, $currComponent, $allAttributes, $saveRecursive = false, $attributesIds = [])
    {

        if (is_null($item))
            return false;

        if (!empty($attributesIds)) {

            if ($saveRecursive) {
                foreach ($attributesIds as $attributesIdArr) {
                    $variationDetailsId = ProductsItemsVariationsDetailsId::create([
                        'products_item_id' => $item->id,
                        'use_stock' => 0,
                        'stock_quantity' => null,
                        'price' => null,
                        'sale_price' => null,
                        'discount_percentage' => null,
                        'status' => 'in_stock',
                        'weight' => null,
                        'length' => null,
                        'width' => null,
                        'height' => null,
                        'sku' => null,
                        'is_default' => 0,
                        'iiko_id' => null,
                    ]);

                    foreach ($attributesIdArr as $parentAttrId => $attributeId) {
                        ProductsItemsVariations::create([
                            'products_items_variations_details_id' => $variationDetailsId->id,
                            'products_attribute_parent_id' => $parentAttrId,
                            'products_attribute_child_id' => $attributeId,
                        ]);
                    }
                }
            } else {
                $variationDetailsId = ProductsItemsVariationsDetailsId::create([
                    'products_item_id' => $item->id,
                    'use_stock' => 0,
                    'stock_quantity' => null,
                    'price' => null,
                    'sale_price' => null,
                    'discount_percentage' => null,
                    'status' => 'in_stock',
                    'weight' => null,
                    'length' => null,
                    'width' => null,
                    'height' => null,
                    'sku' => null,
                    'is_default' => 0,
                    'iiko_id' => null,
                ]);

                foreach (array_keys($attributesIds) as $parentAttrId) {
                    ProductsItemsVariations::create([
                        'products_items_variations_details_id' => $variationDetailsId->id,
                        'products_attribute_parent_id' => $parentAttrId,
                        'products_attribute_child_id' => null,
                    ]);
                }

            }

        }
        $allExistVariationsIds = [];

        $itemVariationArr = $request->get('item_variations', []);

        if (!empty($itemVariationArr)) {
            $tempArr = $newItemVariationArr = [];
            $itemVariationAttrIds = $itemVariationArr['attr_ids'];

            if (@$itemVariationArr['is_default_variation']) {
                $item->variationsDetails()->where('is_default', 1)->update(['is_default' => 0]);
            }

            foreach ($itemVariationAttrIds as $key => $variationAttrId) {
                if (is_array($variationAttrId) && !empty($variationAttrId)) {
                    $allExistVariationsIds[] = $key;

                    foreach ($variationAttrId as $k => $oneVariationAttrId) {
                        if (in_array($k, array_keys($request->get('attributes_opt'))) && in_array($oneVariationAttrId, !empty($request->get('attributes_opt')[$k]['children']) ? $request->get('attributes_opt')[$k]['children'] : []))
                            $tempArr[$k] = $oneVariationAttrId;
                        elseif ($oneVariationAttrId == 'all')
                            $tempArr[$k] = 'all';
                    }

                    $newItemVariationArr[$key] = $tempArr;
                }
            }

            if (!empty($newItemVariationArr) && !empty($allExistVariationsIds)) {

                foreach ($newItemVariationArr as $key => $variationAttrId) {

                    if (is_array($variationAttrId)) {

                        $variationalWeight = !is_null(@$itemVariationArr['weight_variation'][$key]) ? @$itemVariationArr['weight_variation'][$key] / 1000 : null;

                        $variationDetailsId = ProductsItemsVariationsDetailsId::updateOrCreate([
                            'id' => $key > 0 ? $key : null,
                            'products_item_id' => $item->id,
                        ], [
                            'use_stock' => @$itemVariationArr['use_stock_variation'][$key] == 'on' ? 1 : 0,
                            'stock_quantity' => @$itemVariationArr['use_stock_variation'][$key] == 'on' ? @$itemVariationArr['stock_quantity_variation'][$key] : null,
                            'price' => @$itemVariationArr['price_variation'][$key] ? (float)$itemVariationArr['price_variation'][$key] : null,
                            'sale_price' => @$itemVariationArr['sale_price_variation'][$key] ? (float)$itemVariationArr['sale_price_variation'][$key] : null,
                            'discount_percentage' => @$itemVariationArr['discount_percentage_variation'][$key] ? (float)$itemVariationArr['discount_percentage_variation'][$key] : null,
                            'status' => @$itemVariationArr['stock_status_variation'][$key],
                            'weight' => $variationalWeight,
                            'length' => @$itemVariationArr['length_variation'][$key],
                            'width' => @$itemVariationArr['width_variation'][$key],
                            'height' => @$itemVariationArr['height_variation'][$key],
                            'sku' => @$itemVariationArr['sku_variation'][$key],
                            'is_default' => !is_null(@$itemVariationArr['is_default_variation'][$key]) ? 1 : 0,
                            'iiko_id' => @$itemVariationArr['iiko_id'][$key],
                        ]);

                        ProductsItemsVariationsDetails::updateOrCreate([
                            'products_items_variations_detail_id' => $variationDetailsId->id,
                            'lang_id' => $langId
                        ], [
                            'description' => @$itemVariationArr['description_variation'][$key]
                        ]);

                        $createdUpdatedVariationDetailsIds[] = $variationDetailsId->id;

                        foreach ($variationAttrId as $p_id => $attr_id) {

                            if (in_array($p_id, array_keys($request->get('attributes_opt')))) {

                                $currAttributes = $allAttributes->where('parent.id', $p_id)->first();

                                if (!is_null($currAttributes)) {

                                    $parentAttr = @$currAttributes['parent'];
                                    $childrenAttr = @$currAttributes['children'];

                                    if (!is_null($parentAttr) && !is_null($childrenAttr)) {

                                        $childAttr = $childrenAttr->where('id', $attr_id)->first();

                                        if (!is_null($childAttr) || $attr_id == 'all') {

                                            ProductsItemsVariations::updateOrCreate([
                                                'products_items_variations_details_id' => $variationDetailsId->id,
                                                'products_attribute_parent_id' => $parentAttr->id
                                            ], [
                                                'products_items_variations_details_id' => $variationDetailsId->id,
                                                'products_attribute_parent_id' => $parentAttr->id,
                                                'products_attribute_child_id' => $attr_id == 'all' ? null : $attr_id,
                                            ]);
                                        }
                                    }
                                }
                            }
                        }

                        helpers()->uploadFiles(@$itemVariationArr['variation_files'][$key], $item->id, $currComponent->id, $variationDetailsId->wasRecentlyCreated ? 'create' : 'edit', null, $variationDetailsId->id);
                    }
                }


            }
        }

        return [
            'status' => true,
            'msg' => [
                'e' => ['Variations was successful updated'],
                'type' => 'info'
            ]
        ];
    }

    public function updateVariations(Request $request)
    {

        $item = ProductsItemsId::find($request->get('itemId'));

        if (is_null($item))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Product not exist!'],
                    'type' => 'warning'
                ]
            ]);

        $lang = Languages::where('active', 1)
            ->find($request->get('lang'));

        if (is_null($lang))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Lang not exist!'],
                    'type' => 'warning'
                ]
            ]);

        $allAttributes = ProductsHelpers::getProductSelectedAttributes($item, true);

        $response = $this->saveProductVariations($request, $item, $lang->id, parent::currComponent(), $allAttributes);

        if (!is_array($response) || !$response)
            $response = [
                'status' => false,
                'msg' => [
                    'e' => ['Something was wrong, please try again!'],
                    'type' => 'warning'
                ]
            ];

        $defaultProductVariation = ProductsHelpers::getProductDefaultVariation($item, $allAttributes);

        try {
            $defaultVariationView = view("{$this->path}.templates.defaultVariationItems", ['defaultVariationAttributes' => @$defaultProductVariation->defaultVariationsAttrNames])->render();
            $response['defaultVariationView'] = $defaultVariationView;
        } catch (\Throwable $e) {
        }

        return response()->json($response);
    }

    public function setVariationsActions(Request $request)
    {

        $actionName = $request->get('action', null);
        $actionValue = $request->get('value', null);

        if (!$actionName)
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Variations action not exist!'],
                    'type' => 'warning'
                ]
            ]);

        ProductsItemsVariationsDetailsId::where('products_item_id', $request->get('itemId', null))
            ->chunk(100, function ($variations) use ($actionName, $actionValue) {
                if ($variations->isNotEmpty())
                    foreach ($variations as $variation) {
                        $updateValue = null;
                        $data = [];

                        switch ($actionName) {
                            case 'variations_regular_price':
                                if (is_numeric($actionValue) && $actionValue >= 0)
                                    $updateValue = $actionValue;

                                if ($updateValue && $variation->sale_price >= $updateValue)
                                    $updateValue = null;

                                $data = ['price' => $updateValue];
                                break;

                            case 'variations_regular_price_increase':

                                if (str_contains($actionValue, '%')) {
                                    $actionValuePercentage = str_replace('%', '', $actionValue);

                                    if ($actionValuePercentage > 0 && $actionValuePercentage <= 100 && $variation->price)
                                        $updateValue = $variation->price + ($variation->price * $actionValuePercentage / 100);

                                } elseif (is_numeric($actionValue))
                                    $updateValue = $variation->price + $actionValue;

                                if ($updateValue && $variation->sale_price >= $updateValue)
                                    $updateValue = null;

                                $data = ['price' => $updateValue];
                                break;

                            case 'variations_regular_price_decrease':
                                if (str_contains($actionValue, '%')) {
                                    $actionValuePercentage = str_replace('%', '', $actionValue);

                                    if ($actionValuePercentage > 0 && $actionValuePercentage <= 100 && $variation->price)
                                        $updateValue = $variation->price - ($variation->price * $actionValuePercentage / 100);

                                } elseif (is_numeric($actionValue))
                                    $updateValue = $variation->price - (float)$actionValue;

                                if ($updateValue < 0)
                                    $updateValue = 0;

                                if ($updateValue && $variation->sale_price >= $updateValue)
                                    $updateValue = null;

                                $data = ['price' => $updateValue];
                                break;

                            case 'variations_sale_price':
                                if (is_numeric($actionValue) && $actionValue >= 0)
                                    $updateValue = $actionValue;

                                if ($updateValue && $variation->price <= $updateValue)
                                    $updateValue = null;

                                $data = ['sale_price' => $updateValue];
                                break;

                            case 'variations_sale_price_increase':
                                if (str_contains($actionValue, '%')) {
                                    $actionValuePercentage = str_replace('%', '', $actionValue);

                                    if ($actionValuePercentage > 0 && $actionValuePercentage <= 100 && $variation->price)
                                        $updateValue = $variation->sale_price + ($variation->sale_price * $actionValuePercentage / 100);

                                } elseif (is_numeric($actionValue))
                                    $updateValue = $variation->sale_price + $actionValue;

                                if ($updateValue && $variation->price <= $updateValue)
                                    $updateValue = null;

                                $data = ['sale_price' => $updateValue];
                                break;

                            case 'variations_sale_price_decrease':
                                if (str_contains($actionValue, '%')) {
                                    $actionValuePercentage = str_replace('%', '', $actionValue);

                                    if ($actionValuePercentage > 0 && $actionValuePercentage <= 100 && $variation->price)
                                        $updateValue = $variation->sale_price - ($variation->sale_price * $actionValuePercentage / 100);

                                } elseif (is_numeric($actionValue))
                                    $updateValue = $variation->sale_price - (float)$actionValue;

                                if ($updateValue && $variation->sale_price <= $updateValue)
                                    $updateValue = null;

                                $data = ['sale_price' => $updateValue > 0 ?: null];
                                break;

                            case 'variations_use_stock':
                                $updateValue = $variation->use_stock ? 0 : 1;
                                $data = ['use_stock' => $updateValue];
                                break;

                            case 'variations_stock_quantity':
                                if (is_numeric($actionValue) && $actionValue >= 0)
                                    $updateValue = $actionValue;

                                $data = [
                                    'stock_quantity' => $updateValue,
                                    'use_stock' => 1
                                ];
                                break;

                            case 'variations_in_stock':
                                $updateValue = 'in_stock';
                                $data = [
                                    'status' => $updateValue,
                                    'use_stock' => 0,
                                    'stock_quantity' => null
                                ];
                                break;

                            case 'variations_out_of_stock':
                                $updateValue = 'out_of_stock';
                                $data = [
                                    'status' => $updateValue,
                                    'use_stock' => 0,
                                    'stock_quantity' => null
                                ];
                                break;

                            case 'variations_length':
                                if (is_numeric($actionValue) && $actionValue >= 0)
                                    $updateValue = $actionValue;

                                $data = ['length' => $updateValue];
                                break;

                            case 'variations_width':
                                if (is_numeric($actionValue) && $actionValue >= 0)
                                    $updateValue = $actionValue;

                                $data = ['width' => $updateValue];
                                break;

                            case 'variations_height':
                                if (is_numeric($actionValue) && $actionValue >= 0)
                                    $updateValue = $actionValue;

                                $data = ['height' => $updateValue];
                                break;

                            case 'variations_weight':
                                if (is_numeric($actionValue) && $actionValue >= 0)
                                    $updateValue = $actionValue;

                                $data = ['weight' => $updateValue];
                                break;
                        }

                        if (!is_null($updateValue) && $data)
                            $variation->update($data);
                    }
            });

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Variations action was successful applied'],
                'type' => 'info'
            ]
        ]);
    }

    public function getVariations(Request $request, $excludeIds = [])
    {

        $item = ProductsItemsId::find($request->get('itemId', null));

        if (is_null($item))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Product not exist!'],
                    'type' => 'warning'
                ]
            ]);

        $currPage = (int)$request->get('currPage', 1);
        $skipItems = ($currPage > 0 ? $currPage - 1 : 0) * $this->maxGeneratedVariations;

        $variationsDetailsBuilder = $item->variationsDetails()
            ->whereNotIn('id', $excludeIds);

        $countVariationsDetails = $variationsDetailsBuilder->count();

        $variationsDetails = $variationsDetailsBuilder->skip($skipItems)
            ->limit($this->maxGeneratedVariations)
            ->with(['variations', 'itemByLang', 'files.file'])
            ->orderBy('id', 'desc')
            ->get();

        $variationAttributes = ProductsHelpers::getProductSelectedAttributes($item, true);

        $pagination = (object)[
            'total' => $countVariationsDetails,
            'lastPage' => (int)ceil($countVariationsDetails / $this->maxGeneratedVariations),
            'currPage' => $currPage,
            'perPage' => $this->maxGeneratedVariations
        ];

        $defaultProductVariation = ProductsHelpers::getProductDefaultVariation($item, $variationAttributes);

        $iiko_products = IikoProducts::all();

        try {
            $data = [
                'path' => $this->path,
                'moduleName' => $this->moduleName,
                'productId' => $item->id,
                'variationsDetails' => @$variationsDetails,
                'variationAttributes' => $variationAttributes,
                'iiko_products' => $iiko_products,
            ];

            $view = view("{$this->path}.templates.variationsItems", $data)->render();

            $paginationView = view("{$this->path}.templates.paginationVariations", [
                'pagination' => $pagination,
                'moduleName' => $this->moduleName
            ])->render();

            $defaultVariationView = view("{$this->path}.templates.defaultVariationItems", ['defaultVariationAttributes' => @$defaultProductVariation->defaultVariationsAttrNames])->render();

        } catch (\Throwable $e) {
            $view = $paginationView = $defaultVariationView = '';
        }

        return [
            'status' => true,
            'view' => $view,
            'pagination' => $pagination,
            'paginationView' => $paginationView,
            'defaultVariationView' => $defaultVariationView,
        ];
    }

    public function makeVariationAction(Request $request)
    {

        $addVariationsRecursive = $request->get('action') == 'add_variations_recursive';

        $item = ProductsItemsId::find($request->get('itemId'));

        if (is_null($item))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Product not exist!'],
                    'type' => 'warning'
                ]
            ]);


        $lang = Languages::where('active', 1)
            ->find($request->get('lang'));

        if (is_null($lang))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Lang not exist!'],
                    'type' => 'warning'
                ]
            ]);

        $currComponent = parent::currComponent();

        $allAttributes = ProductsHelpers::getProductSelectedAttributes($item, true);

        if ($allAttributes->isEmpty())
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Use for variations minim one attribute!'],
                    'type' => 'warning'
                ]
            ]);

        $response = [
            'status' => true,
            'variationAttributes' => $allAttributes,
            'iteration' => $request->get('countItems') ? $request->get('countItems') - 1 : -1,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'updateMultiple' => $addVariationsRecursive,
            'productId' => $item->id
        ];

        $attributesIds = $multipleAttributesIds = $newMultipleAttributes = $existVariationsIds = $tempExistVariationsIds = [];

        if ($addVariationsRecursive) {

            foreach ($allAttributes as $children) {
                if ($children['children']->isNotEmpty())
                    foreach ($children['children'] as $child) {
                        $attributesIds[$children['parent']->id][] = $child->id;
                    }
            }

            $existVariations = ProductsItemsVariations::whereHas('variationDetailId.product', function ($q) use ($item) {
                $q->where('id', $item->id);
            })->orderBy('products_items_variations_details_id')->get()->groupBy('products_items_variations_details_id');

            if ($existVariations->isNotEmpty()) {

                $iteration = -1;
                foreach ($existVariations as $key => $existVariationsId) {
                    $checkIfExistNullChild = !is_null($existVariationsId->where('products_attribute_child_id', null)->first());
                    $tempExistVariationsIds[] = $existVariationsId->pluck('products_items_variations_details_id')->toArray();

                    if ($checkIfExistNullChild)
                        continue;

                    $iteration++;
                    foreach ($existVariationsId as $value)
                        $existVariationsIds[$iteration][$value->products_attribute_parent_id] = $value->products_attribute_child_id;

                    $existVariationsIds[$iteration] = array_reverse($existVariationsIds[$iteration], true);
                }
            }

            $multipleAttributesIds = arrayCartesian($attributesIds);

            foreach ($multipleAttributesIds as $multipleAttributesId) {
                if (in_array($multipleAttributesId, $existVariationsIds))
                    continue;

                $newMultipleAttributes[] = $multipleAttributesId;
            }

            $totalCountMultipleAttributes = count($multipleAttributesIds);
            $totalCountNewMultipleAttributes = count($newMultipleAttributes);
            $totalExistsAttributesCount = count($existVariationsIds);

            if ($totalCountMultipleAttributes > $this->maxGeneratedVariations)
                $newMultipleAttributes = array_slice($newMultipleAttributes, 0, $this->maxGeneratedVariations);

            $generatedCountNewMultipleAttributes = $totalExistsAttributesCount + count($newMultipleAttributes);

            $response['multipleAttributesIds'] = $newMultipleAttributes;
            $response['updateMultipleView'] = !empty($newMultipleAttributes);

            if ($totalCountMultipleAttributes > $this->maxGeneratedVariations && $totalCountNewMultipleAttributes > 0) {
                if ($generatedCountNewMultipleAttributes < $totalCountMultipleAttributes) {
                    $msg = __("{$this->moduleName}::e.generated_variations_count", ['count' => $generatedCountNewMultipleAttributes, 'total' => $totalCountMultipleAttributes]);

                    $response['msg'] = [
                        'e' => [$msg],
                        'type' => 'warning'
                    ];

                } else {
                    $msg = __("{$this->moduleName}::e.generated_variations_count_all", ['count' => $generatedCountNewMultipleAttributes, 'total' => $totalCountMultipleAttributes]);

                    $response['msg'] = [
                        'e' => [$msg],
                        'type' => 'info'
                    ];
                }

            } else {
                $msg = __("{$this->moduleName}::e.generated_variations_max");

                $response['msg'] = [
                    'e' => [$msg],
                    'type' => 'info'
                ];
            }

        } else {
            foreach ($allAttributes as $children) {
                if ($children['children']->isNotEmpty())
                    $attributesIds[$children['parent']->id] = 'all';
            }
        }

        $this->saveProductVariations($request, $item, $lang->id, $currComponent, $allAttributes, $addVariationsRecursive, ($addVariationsRecursive ? $newMultipleAttributes : $attributesIds));

        return response()->json($response);
    }

    public function destroyVariations(Request $request)
    {
        $item = ProductsItemsId::find($request->get('itemId'));

        if (is_null($item))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['Product not exist!'],
                    'type' => 'warning'
                ]
            ]);

        $variationId = $request->get('variationId', null);

        if ($variationId)
            $variationsDetails = $item->variationsDetails->where('id', $request->get('variationId'));
        else
            $variationsDetails = $item->variationsDetails;

        if ($variationsDetails->isNotEmpty())
            $variationsDetails->map(function ($item) {

////                Destroy product from cart
//                $fileSystem = new Filesystem();
//                if (Module::has('Orders') && $fileSystem->exists(Module::getPath() . '/Orders/Models/CartProducts.php'))
//                    try {
//                        $item->cartVariations()->whereHas('cart', function ($q) {
//                            $q->whereNull('status');
//                        })->delete();
//                    } catch (\Exception $e) {
//                    }
////                Destroy product from cart

                $item->items()->delete();
                $item->variations()->delete();
                $item->files()->delete();
                $item->delete();
            });

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Variation was successful deleted!'],
                'type' => 'info'
            ],
        ]);
    }

    public function exportList(Request $request)
    {
        if ($request->method() != 'POST' && !$request->ajax())
            abort(404);

        $exportsDir = storage_path('app/exports');
        $fileName = 'products-stock_' . time() . '.csv';
        $filePath = $exportsDir . '/' . $fileName;

        if (!file_exists($exportsDir))
            File::makeDirectory($exportsDir, 0775, true, true);

        $filterTableList = filterTableList($request, $request->except('page'), true);
        $filterParams = $filterTableList->filterParams;

        $handler = fopen($filePath, 'w+');
        $fileSize = filesize($filePath);
        $headerArray = $this->generateCsvHeaderContent();

        if ($fileSize == 0)
            fputcsv($handler, $headerArray);

        $productsController = new ProductsController();

        ProductsItemsId::with(['items', 'variationsDetails', 'variationsDetails.variations', 'variationsDetails.items', 'productCategories', 'files.file', 'productItemsAttributes', 'productItemsAttributes.attribute', 'productItemsAttributes.attribute.globalName', 'productItemsAttributes.attribute.type', 'productItemsAttributes.options', 'productItemsAttributes.options.attribute', 'productItemsAttributes.options.attribute.globalName'])
            ->where(function ($q) use ($productsController, $filterParams) {
                $productsController->filterQuery($q, $filterParams);
            })->orderBy('position')
            ->chunk(200, function ($products) use ($handler) {
                foreach ($products as $product) {
                    $attributes = ProductsHelpers::getProductSelectedAttributes($product, false, false, false);
                    $productData = exportProductsData($product, $attributes);
                    if (@$productData->item) {

                        $content = $this->generateCsvHeaderContent($productData, null, $attributes);

                        fputcsv($handler, $content);

                        if (@$productData->item->isVariationProduct && @$productData->variations->isNotEmpty()) {
                            foreach ($productData->variations->sortByDesc('item.id') as $variation) {

                                $variationContent = $this->generateCsvHeaderContent($productData, $variation, $attributes);

                                fputcsv($handler, $variationContent);
                            }
                        }
                    }
                }
            });

        fclose($handler);

        return response()->json([
            'status' => true,
            'redirect' => adminUrl([parent::currComponent()->slug, 'downloadProductsStock']) . '?file=' . $fileName,
            'msg' => [
                'e' => 'Data is being exported to csv file.',
                'type' => 'info',
            ],

        ]);
    }

    public function downloadProductsStock(Request $request)
    {
        $fileName = $request->get('file', null);

        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];

        $exportsDir = storage_path('app/exports');
        $file = "{$exportsDir}/{$fileName}";
        $newFileName = "products-stock_" . date('d-m-Y') . ".csv";


        if (!$fileName || !file_exists($file))
            abort(404);

        $authUser = auth()->guard('admin')->user();
        $authUser->update([
            'has_exported_products' => 0
        ]);

        return response()->download($file, $newFileName, $headers)->deleteFileAfterSend(true);
    }

    public function import()
    {
        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName
        ];

        return view("{$this->path}.import", $response);
    }

    public function importProducts(Request $request)
    {

        if ($request->method() != 'POST' && !$request->ajax())
            abort(404);

        $authUser = auth()->guard('admin')->user();

//        if ($authUser->has_imported_products)
//            return response()->json([
//                'status' => false,
//                'msg' => [
//                    'e' => ['Your csv file import is in process. Please, have patience, this may take some time.'],
//                    'type' => 'warning'
//                ]
//            ]);

        $file = $request->file('file');


        $extension = $file->getClientOriginalExtension();

        $fileType = $file->getClientMimeType();

        $fileSize = $file->getSize();


        if ($fileType != 'text/csv' && strtolower($extension) != 'csv')
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ["Invalid file format. You can upload only .csv files."],
                    'type' => 'warning'
                ]
            ]);

        if ($fileSize > 67108864) // 64 MB
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ["File can have max 64 MB."],
                    'type' => 'warning'
                ]
            ]);


        $importsDir = storage_path('app/imports');

        if (!file_exists($importsDir))
            File::makeDirectory($importsDir, 0775, true, true);

        $fileName = 'products-stock_' . time() . '.csv';
        $file->move($importsDir, $fileName);
        $fileLocation = "{$importsDir}/{$fileName}";


        if (!file_exists($fileLocation))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => ['File not exist.'],
                    'type' => 'error'
                ]
            ]);

        $fileContent = file($fileLocation);

        if (empty($fileContent))
            return response()->json([
                'status' => false,
                'msg' => [
                    'e' => 'Your file is empty.',
                    'type' => 'warning'
                ]
            ]);

        $langList = @view()->getShared()['langList'] ? view()->getShared()['langList']->sortByDesc('is_default') : Languages::where('active', 1)
            ->orderBy('is_default', 'desc')
            ->get();

        $allAttributes = ProductsHelpers::getAllAttributesStructured();

        $headerFile = array_map(function ($itemFile) {
            return str_slug($itemFile, '_');
        }, str_getcsv($fileContent[0]));
        $errorMsg = [];

        $contentFile = array_slice($fileContent, 1);

        $partiallyContentFile = collect(array_map('str_getcsv', $contentFile))
            ->filter(function ($item) {
                return $item[0];
            })
            ->groupBy(function ($item) {
                return $item[0];
            })->chunk(1000);

        $iteration = 1;

        if ($partiallyContentFile->isNotEmpty()) {
            foreach ($partiallyContentFile as $productsDetails) {
                foreach ($productsDetails as $productId => $products) {
                    $iteration++;
                    $mainProduct = @$products->first();
                    $productVariations = $products->slice(1);
                    $product = array_combine($headerFile, $mainProduct);
                    $isVariationProduct = @$product['product_typesv'] == 'v';

                    if ($isVariationProduct && $productVariations->isEmpty()) {
                        $errorMsg[$iteration][] = "Product doesn't have variations.";
                        continue;
                    }

                    $categoriesIds = explode(',', @$product['categories'] ?: '');

                    $price = $salePrice = null;

                    if (!$isVariationProduct) {
                        $price = (float)$product['price'] ?: null;
                        $salePrice = (float)$product['sale_price'] ?: null;

                        if ($salePrice > $price)
                            $salePrice = null;
                    }


                    $productNames = filterArrayKeys($product, 'name_');
                    $productDescriptions = filterArrayKeys($product, 'description_');

                    $defaultNameKey = "name_" . DEF_LANG;
                    $slug = @$productNames[$defaultNameKey] ? str_slug($productNames[$defaultNameKey]) : str_slug(str_random());
                    $sku = @$product['product_sku'] ?: -1;

                    $productAttributes = collect();
                    $productAttributesForVariation = [];
                    $productAttributesArr = explode(';', @$product['attributes']);

                    if (!empty($productAttributesArr)) {
                        foreach ($productAttributesArr as $productAttribute) {
                            $parentChildrenAttr = explode(':', $productAttribute);
                            $parentSlug = @$parentChildrenAttr[0] ?: null;
                            $childrenSlug = explode(',', @str_replace(' ', '', @$parentChildrenAttr[1]) ?: '');

                            if (is_null($parentSlug)) {
                                $errorMsg[$iteration][] = "Product attributes category is empty.";
                                continue;
                            }

                            $parentAttr = $allAttributes->where('parent.slug', $parentSlug)->first();

                            if (is_null($parentAttr)) {
                                $errorMsg[$iteration][] = "Product attributes category, `{$parentSlug}`, not exist.";
                                continue;
                            }

                            $childrenAttr = $parentAttr['children']->whereIn('slug', $childrenSlug);

                            if ($childrenAttr->isEmpty() || ($childrenAttr->isNotEmpty() && $childrenAttr->count() != count($childrenSlug))) {
                                $errorMsg[$iteration][] = "Product attributes items, from category `{$parentSlug}`, not exists.";
                                continue;
                            }

                            $productAttributes->push([
                                'parent' => @$parentAttr['parent'],
                                'children' => $childrenAttr
                            ]);
                        }
                    }

                    $item = ProductsItemsId::find($productId);

                    $isNewProduct = is_null($item);


                    if (is_null($item)) {
                        $item = new ProductsItemsId();

                        if (!is_null($item->where('slug', $slug)->first())) // exist slug
                            $slug = null;

                        if (!is_null($item->where('sku', $sku)->first())) // exist sku
                            $sku = null;
                    }


                    $incompleteData = empty($productNames) || is_null($slug) || (is_null($sku) && !$isVariationProduct) || (is_null($price) && !$isVariationProduct) || empty($categoriesIds) || ($productAttributes->isEmpty() && $isVariationProduct) ? 1 : 0;


                    /** Incomplete data errors */

                    if (empty($productNames))
                        $errorMsg[$iteration][] = "Product doesn't have name.";

                    if (is_null($slug))
                        $errorMsg[$iteration][] = "Product doesn't have slug or the slug has already been taken.";

                    if (is_null($sku) && !$isVariationProduct)
                        $errorMsg[$iteration][] = "Product doesn't have sku or the sku has already been taken.";

                    if (is_null($price) && !$isVariationProduct)
                        $errorMsg[$iteration][] = "Product doesn't have price.";

                    if (empty($categoriesIds))
                        $errorMsg[$iteration][] = "Product doesn't have category.";

                    if ($productAttributes->isEmpty() && $isVariationProduct)
                        $errorMsg[$iteration][] = "Product doesn't have attributes.";

                    /** Incomplete data errors */

                    if ($isNewProduct) {
                        $item->slug = $slug;
                        $item->position = time() . $iteration;
                        $item->active = !$incompleteData;
                        $item->show_on_main = 0;
                        $item->popularity_sales = null;
                        $item->popularity_rating = null;
                        $item->recommended_by_categories_ids = null;

                    }

                    $item->item_type_id = $isVariationProduct ? 2 : 1;
                    $item->price = $price;
                    $item->sale_price = $salePrice;
                    $item->use_stock = @$product['use_stock10'] == 1 ? 1 : 0;

                    if ((@$product['use_stock10'] == 1 && @$product['stock_qty'] != '-') || $isNewProduct) {
                        $item->stock_quantity = @$product['use_stock10'] == 1 ? (@$product['stock_qty'] >= 0 ? $product['stock_qty'] : 0) : null;
                    }

                    $item->status = @strtolower($product['stock_statusio']) == 'i' ? 'in_stock' : 'out_of_stock';

                    $item->sku = @$sku != -1 ? $sku : null;
                    $item->incomplete_data = $incompleteData;

                    $item->save();

                    foreach ($langList as $lang) {
                        ProductsItems::updateOrCreate([
                            'products_item_id' => $item->id,
                            'lang_id' => $lang->id
                        ], [
                            'name' => @$productNames["name_{$lang->slug}"],
                            'description' => @$productDescriptions["description_{$lang->slug}"] && @$productDescriptions["description_{$lang->slug}"] != '-' ? @$productDescriptions["description_{$lang->slug}"] : null,
                        ]);
                    }

                    /** Save product files */

                    $importProductsFilesResponse = ProductsHelpers::importProductsFiles($item->id, @$product['files'], $this->currComponent()->id, $iteration);
                    if (!$importProductsFilesResponse->status && $importProductsFilesResponse->errorMsg)
                        $errorMsg[$iteration][] = $importProductsFilesResponse->errorMsg;

                    /** Save product files */


                    /** Create relation products ~ categories */

                    $productCategories = ProductsCategoriesId::whereIn('id', $categoriesIds)->get();

                    ProductsItemsCategories::where('products_item_id', $item->id)->whereNotIn('id', $categoriesIds)->delete();
                    if ($productCategories->isNotEmpty())
                        foreach ($productCategories as $category) {
                            ProductsItemsCategories::firstOrCreate([
                                'products_item_id' => $item->id,
                                'products_category_id' => $category->id
                            ]);
                        }

                    /** Create relation products ~ categories */

                    /** Create relation products ~ attributes */

                    if ($productAttributes->isNotEmpty()) {
                        $parentAttributesIds = $productAttributes->pluck('parent.id')->toArray();

                        $existItemsAttributes = $item->productItemsAttributes()->whereIn('products_parent_attribute_id', $parentAttributesIds)->get();

                        if ($existItemsAttributes->isNotEmpty())
                            foreach ($existItemsAttributes as $existItemsAttribute) {
                                $childrenAttributesIds = $productAttributes->pluck('children')->flatten()->pluck('id')->unique()->toArray();

                                $existItemsAttribute->options()->whereNotIn('products_attribute_id', $childrenAttributesIds)->delete();
                            }

                        $item->productItemsAttributes()->whereNotIn('products_parent_attribute_id', $parentAttributesIds)->delete();

                        foreach ($productAttributes as $attribute) {
                            $parent = @$attribute['parent'];
                            $children = @$attribute['children'];

                            if (!$parent || !$children || ($children && $children->isEmpty()))
                                continue;

                            $itemAttribute = ProductsItemsAttributes::firstOrCreate([
                                'products_item_id' => $item->id,
                                'products_parent_attribute_id' => $parent->id
                            ], [
                                'visible_on_product_page' => 1,
                                'for_variation' => $isVariationProduct ? 1 : 0
                            ]);

                            $itemAttributeOptionsArr = [];
                            foreach ($children as $child) {
                                $itemAttributeOptions = ProductsItemsAttributesOptions::firstOrCreate([
                                    'products_items_attribute_id' => $itemAttribute->id,
                                    'products_attribute_id' => $child->id
                                ]);

                                if ($itemAttribute->for_variation)
                                    $itemAttributeOptionsArr[] = $itemAttributeOptions->products_attribute_id;
                            }

                            if ($itemAttribute->for_variation && !empty($itemAttributeOptionsArr))
                                $productAttributesForVariation[$itemAttribute->products_parent_attribute_id] = $itemAttributeOptionsArr;
                        }
                    }

                    /** Create relation products ~ attributes */

                    /** Create - update variations */

                    foreach ($productVariations as $productVariation) {
                        $iteration++;
                        $variation = array_combine($headerFile, $productVariation);
                        $allVariationAttributesCombination = arrayCartesian($productAttributesForVariation);

                        $variationAttributes = collect();
                        $variationAttributesIds = [];
                        $variationAttributesArr = explode(';', @$variation['variation_attributes']);
                        if (!empty($variationAttributesArr))
                            foreach ($variationAttributesArr as $variationAttribute) {
                                $parentChildrenAttr = explode(':', $variationAttribute);
                                $parentSlug = @str_replace(' ', '', @$parentChildrenAttr[0]) ?: null;
                                $childrenSlug = @str_replace(' ', '', @$parentChildrenAttr[1]) ?: null;

                                if (is_null($parentSlug)) {
                                    $errorMsg[$iteration][] = "Product variation attributes category is empty.";
                                    continue;
                                }

                                $parentAttr = $allAttributes->where('parent.slug', $parentSlug)->first();

                                if (is_null($parentAttr)) {
                                    $errorMsg[$iteration][] = "Product variation attributes category, `{$parentSlug}`, not exist.";
                                    continue;
                                }

                                if (is_null($childrenSlug)) {
                                    $errorMsg[$iteration][] = "Product variation attributes item is empty.";
                                    continue;
                                }

                                $childrenAttr = $parentAttr['children']->where('slug', $childrenSlug)->first();

                                if (is_null($childrenAttr) && $childrenSlug != 'all' && $childrenSlug != '-') {
                                    $errorMsg[$iteration][] = "Product variation attributes item, `{$childrenSlug}`, not exist.";
                                    continue;
                                }

                                $variationAttributes->push([
                                    'parent' => @$parentAttr['parent'],
                                    'child' => $childrenAttr
                                ]);

                                $variationAttributesIds[@$parentAttr['parent']->id] = $childrenSlug == 'all' || $childrenSlug == '-' ? 'all' : @$childrenAttr->id;
                            }

                        $isValidVariationAttr = false;
                        foreach ($allVariationAttributesCombination as $combination)
                            if (in_array('all', $variationAttributesIds)) {
                                $isValidVariationParentAttr = [];
                                foreach ($variationAttributesIds as $p_id => $filterVariationAttributesId)
                                    $isValidVariationParentAttr[] = array_key_exists($p_id, $combination) && !is_null($filterVariationAttributesId);

                                if (!in_array(false, $isValidVariationParentAttr) && count($variationAttributesIds) == count($combination)) {
                                    $isValidVariationAttr = true;
                                    break;
                                }
                            } else if ($variationAttributesIds == $combination) {
                                $isValidVariationAttr = true;
                                break;
                            }

                        if (!$isValidVariationAttr) {
                            $errorMsg[$iteration][] = "Product variation attributes combination not valid.";
                            continue;
                        }

                        $variationId = @$variation['variation_id'];
                        $variationItem = ProductsItemsVariationsDetailsId::where('products_item_id', $item->id)->find($variationId);
                        $isNewVariation = is_null($variationItem);
                        $variationSku = @$variation['variation_sku'] ?: -1;

                        if ($isNewVariation) {
                            $variationItem = new ProductsItemsVariationsDetailsId();

                            if (!is_null($variationItem->where('sku', $variationSku)->first())) // exist sku
                                $variationSku = null;
                        }

                        $variationPrice = @(float)$variation['variation_price'] ?: null;
                        $variationSalePrice = @(float)$variation['variation_sale_price'] ?: null;

                        if ($variationSalePrice > $variationPrice)
                            $variationSalePrice = null;

                        $variationDescriptions = filterArrayKeys($variation, 'variation_description_');

                        $incompleteVariationData = is_null($variationSku) || is_null($variationPrice) || empty($variationAttributesIds) ? 1 : 0;
                        if ($incompleteVariationData == 1 || ($incompleteVariationData == 0 && $item->incomplete_data == 0)) {
                            $item->update([
                                'incomplete_data' => $incompleteVariationData,
                                'active' => !$incompleteVariationData
                            ]);
                        }

                        /** Incomplete variations data errors */

                        if (is_null($variationSku))
                            $errorMsg[$iteration][] = "Product variation doesn't have sku or the sku has already been taken.";

                        if (is_null($variationPrice))
                            $errorMsg[$iteration][] = "Product variation doesn't have price.";

                        if (empty($variationAttributesIds))
                            $errorMsg[$iteration][] = "Product variation doesn't have attributes.";

                        /** Incomplete variations data errors */

                        if ($isNewVariation) {
                            $variationItem->products_item_id = $item->id;
                            $variationItem->is_default = 0;
                        }

                        $variationItem->price = $variationPrice;
                        $variationItem->sale_price = $variationSalePrice;
                        $variationItem->use_stock = @$variation['variation_use_stock10'] == 1 ? 1 : 0;

                        if ((@$variation['use_stock10'] == 1 && @$variation['stock_qty'] != '-') || $isNewVariation)
                            $variationItem->stock_quantity = @$variation['variation_use_stock10'] == 1 ? (@$variation['variation_stock_qty'] >= 0 ? $variation['variation_stock_qty'] : 0) : null;

                        $variationItem->status = @strtolower($variation['variation_stock_statusio']) == 'i' ? 'in_stock' : 'out_of_stock';
                        $variationItem->weight = (float)@$variation['variation_weight'] ?: null;
                        $variationItem->length = (float)@$variation['variation_length'] ?: null;
                        $variationItem->width = (float)@$variation['variation_width'] ?: null;
                        $variationItem->height = (float)@$variation['variation_height'] ?: null;
                        $variationItem->sku = @$variationSku != -1 ? $variationSku : null;
                        $variationItem->save();

                        foreach ($langList as $lang) {
                            ProductsItemsVariationsDetails::updateOrCreate([
                                'products_items_variations_detail_id' => $variationItem->id,
                                'lang_id' => $lang->id
                            ], [
                                'description' => @$variationDescriptions["variation_description_{$lang->slug}"] && @$variationDescriptions["variation_description_{$lang->slug}"] != '-' ? @$variationDescriptions["variation_description_{$lang->slug}"] : null,
                            ]);
                        }

                        /** Save variation files */
                        $importVariationsFilesResponse = ProductsHelpers::importProductsFiles($item->id, @$variation['variation_files'], $this->currComponent()->id, $iteration, $variationItem->id);
                        if (!$importVariationsFilesResponse->status && $importVariationsFilesResponse->errorMsg)
                            $errorMsg[$iteration][] = $importVariationsFilesResponse->errorMsg;
                        /** Save variation files */

                        /** Create relation products ~ variations */

                        if (!empty($variationAttributesIds)) {
                            foreach ($variationAttributesIds as $parentAttrId => $childAttrId) {
                                ProductsItemsVariations::updateOrCreate([
                                    'products_items_variations_details_id' => $variationItem->id,
                                    'products_attribute_parent_id' => $parentAttrId,
                                ], [
                                    'products_attribute_child_id' => $childAttrId != 'all' && is_int($childAttrId) ? $childAttrId : null,
                                ]);
                            }
                        }

                        /** Create relation products ~ variations */

                    }

                    /** Create - update variations */

                    if ($item->incomplete_data)
                        $errorMsg[$iteration][] = "Product with ID `{$item->id}` is incomplete.";
                }
            }
        } else {
            $errorMsg['rest'][] = 'Your file has a wrong format.';
        }


        $tempErrorMsg = [];
        if (!empty($errorMsg)) {
            foreach ($errorMsg as $key => $error) {
                $tempErrorMsg[$key] = array_flatten($error);
            }
            $errorMsg = $tempErrorMsg;
        }

        if (file_exists($fileLocation))
            File::delete($fileLocation);

        try {
            $details = [
                'title' => 'Import products from CSV',
                'msg' => !empty($errorMsg) ? 'Your csv file was successful imported but you have some errors. Access the link or check your email.' : 'Your csv file was successful imported. Access this link or check your email.',
                'emailMsg' => 'Import products stock from CSV file was finished',
                'linkText' => !empty($errorMsg) ? 'View complete notification' : 'Go to products list',
                'url' => urldecode(customUrl(['admin', 'products'])),
                'errorMsg' => $errorMsg
            ];

            $authUser->notify(new ImportProductsNotification($details, $this->globalSettings()));

//            $authUser->user->update([
//                'has_imported_products' => 0
//            ]);

        } catch (\Exception $e) {

            dd($e, 'exception');

        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => 'Data from file is being imported to database.',
                'type' => 'info'
            ]
        ]);
    }

    function filterQuery($query, $filterParams)
    {

        $query->where(function ($q) use ($filterParams) {

            if (array_key_exists('name', $filterParams) && !is_array($filterParams['name']))
                $q->whereHas('globalName', function ($q) use ($filterParams) {
                    $q->where('name', 'LIKE', "%{$filterParams['name']}%");
                });

            if (array_key_exists('category', $filterParams) && is_array($filterParams['category']))
                $q->whereHas('categories', function ($q) use ($filterParams) {
                    $q->whereIn('products_categories_id.id', $filterParams['category']);
                });

            if (array_key_exists('visible_on_home', $filterParams) && !is_array($filterParams['visible_on_home']))
                if ($filterParams['visible_on_home'] == 'visible') {
                    $q->where('show_on_main', 1);
                } elseif ($filterParams['visible_on_home'] == 'hidden') {
                    $q->where('show_on_main', 0);
                }

            if (array_key_exists('stock', $filterParams) && !is_array($filterParams['stock'])) {
                $q->where(function ($q) use ($filterParams) {
                    if ($filterParams['stock'] == 'in_stock') {
                        $q->inStockProducts();
                    } elseif ($filterParams['stock'] == 'out_of_stock') {
                        $q->outOfStockProducts();
                    }
                });
            }


            if (array_key_exists('promotions', $filterParams) && is_array($filterParams['promotions']))
                $q->whereHas('promotions', function ($q) use ($filterParams) {
                    $q->whereIn('products_promotions_id.id', $filterParams['promotions']);
                });

            if (array_key_exists('start_date', $filterParams) && !array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']))
                $q->whereDate('created_at', '>=', date('Y-m-d', strtotime($filterParams['start_date'])));
            elseif (!array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['end_date']))
                $q->whereDate('created_at', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));
            elseif (array_key_exists('start_date', $filterParams) && array_key_exists('end_date', $filterParams) && !is_array($filterParams['start_date']) && !is_array($filterParams['end_date']))
                $q->whereDate('created_at', '>=', date('Y-m-d', strtotime($filterParams['start_date'])))->whereDate('created_at', '<=', date('Y-m-d', strtotime($filterParams['end_date'])));

        });

        return $query;
    }

    public function widgetsCountItems()
    {
        return ProductsItemsId::count();
    }

// Methods for menu
    public function getItems()
    {
        $items = ProductsItemsId::where('active', 1)
            ->with('globalName')
            ->get();

        return $items;
    }
    public function getItem($id)
    {
        $item = ProductsItemsId::where('active', 1)
            ->with('globalName')
            ->find($id);

        $response = [];

        if (!is_null($item))
            $response = (object)[
                'id' => $item->id,
                'name' => !is_null(@$item->globalName->name) ? $item->globalName->name : null,
                'slug' => $item->slug,
                'allInfo' => $item,
            ];

        return $response;
    }
// Methods for menu

    private function generateCsvHeaderContent($productData = null, $variationData = null, $attributes = null)
    {
        $langList = @view()->getShared()['langList'] ? view()->getShared()['langList']->sortByDesc('is_default') : Languages::where('active', 1)
            ->orderBy('is_default', 'desc')
            ->get();

        if (is_null($productData)) {

            $response = [
                'Product ID',
                'Product type(s,v)',
                'Product SKU'
            ];

            $headerDescriptionArray = [];

            $variationHeaderArray = [
                'Variation ID',
                'Variation SKU',
                'Variation attributes',
                'Variation price',
                'Variation sale price',
                'Variation use stock(1/0)',
                'Variation stock status(i,o)',
                'Variation stock qty',
                'Variation weight',
                'Variation width',
                'Variation height',
                'Variation length',
                'Variation files',
            ];

            if ($langList->isNotEmpty())
                foreach ($langList as $lang) {
                    array_push($response, "Name_{$lang->slug}");
                    array_push($headerDescriptionArray, "Description_{$lang->slug}");
                    array_push($variationHeaderArray, "Variation description_{$lang->slug}");
                }

            $response = array_merge($response, [
                'Categories',
                'Price',
                'Sale price',
                'Use stock(1/0)',
                'Stock status(i,o)',
                'Stock qty',
                'Weight',
                'Width',
                'Height',
                'Length',
                'Files',
            ], $headerDescriptionArray, [
                'Attributes'
            ], $variationHeaderArray);

        } else {
            $product = @$productData->item;
            $productNames = $productDescriptions = $variationDescriptions = [];
            $productItems = @$product->items;
            $variation = @$variationData->item;
            $variationAttributes = @$variationData->attributes ? $variationData->attributes->sortBy('parent.position') : [];
            $variationItems = @$variation->items;

            $productCategoriesIds = @$product->productCategories->pluck('products_category_id')->toArray() ?: [];
//            $productFiles = @$product->files->pluck('file.file')->toArray() ?: [];
            $productFiles = getItemFile($product, 'original', false, 'allFiles') ?: [];
            $productAttributes = [];
            if ($attributes->isNotEmpty())
                foreach ($attributes as $key => $attribute) {
                    $parent = @$attribute['parent'];
                    $children = @$attribute['children'];

                    if ($parent && $children)
                        $productAttributes[] = $parent->slug . ': ' . implode(',', $children->pluck('slug')->toArray());
                }

            if ($langList->isNotEmpty())
                foreach ($langList as $lang) {
                    $productItem = $productItems->where('lang_id', $lang->id)->first();
                    $productNames[] = $productItem && !$variationItems ? $productItem->name : '-';
                    $productDescriptions[] = $productItem && !$variationItems ? $productItem->description : '-';

                    $variationItem = $variationItems ? $variationItems->where('lang_id', $lang->id)->first() : null;
                    $variationDescriptions[] = $variationItem ? $variationItem->description : '-';

                }

            if (is_null($variationData)) {
                $response = [
                    @$product->id,
                    @$product->isVariationProduct ? 'v' : 's',
                    @$product->sku,
                ];

                $response = array_merge($response, $productNames, [
                    !empty($productCategoriesIds) ? implode(',', array_unique($productCategoriesIds)) : '-',
                    @$product->price ?: '-',
                    @$product->sale_price ?: '-',
                    @$product->use_stock,
                    !@$product->use_stock ? (@$product->status == 'in_stock' ? 'i' : 'o') : '-',
                    @$product->use_stock ? @$product->stock_quantity : '-',
                    @$product->weight ?: '-',
                    @$product->width ?: '-',
                    @$product->height ?: '-',
                    @$product->length ?: '-',
                    !empty($productFiles) ? implode(',', $productFiles) : '-',
                ], $productDescriptions, [
                    implode(';', $productAttributes)
                ], [
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-'
                ], $variationDescriptions);

            } else {
                $attributesArr = [];

                if (!empty($variationAttributes))
                    foreach ($variationAttributes as $attribute)
                        $attributesArr[] = @$attribute['parent']['slug'] . ': ' . (@$attribute['child'] == 'all' ? 'all' : @$attribute['child']['slug']);

//                $variationFiles = @$variation->files->pluck('file.file')->toArray() ?: [];
                $variationFiles = getItemFile($variation, 'original', false, 'allFiles') ?: [];

                $response = [
                    @$product->id,
                    '-',
                    '-'
                ];

                $response = array_merge($response, $productNames, [
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                    '-',
                ], $productDescriptions, [
                    '-',
                    @$variation->id,
                    @$variation->sku,
                    !empty($attributesArr) ? implode(';', $attributesArr) : '-',
                    @$variation->price ?: '-',
                    @$variation->sale_price ?: '-',
                    @$variation->use_stock,
                    !@$variation->use_stock ? (@$variation->status == 'in_stock' ? 'i' : 'o') : '-',
                    @$variation->use_stock ? @$variation->stock_quantity : '-',
                    @$variation->weight ?: '-',
                    @$variation->width ?: '-',
                    @$variation->height ?: '-',
                    @$variation->length ?: '-',
                    !empty($variationFiles) ? implode(',', $variationFiles) : '-',
                ], $variationDescriptions);
            }
        }

        return $response;
    }
}