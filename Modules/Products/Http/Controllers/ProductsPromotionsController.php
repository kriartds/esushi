<?php

namespace Modules\Products\Http\Controllers;

use App\Http\Controllers\Admin\DefaultController;
use App\Http\Helpers\Helpers;
use App\Models\Languages;
use Modules\Products\Http\Requests\ProductsPromotionsRequest;
use Modules\Products\Models\ProductsPromotionsId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductsPromotionsController extends DefaultController
{
    private $moduleName;
    private $path;

    public function __construct()
    {
        $this->moduleName = 'products';
        $this->path = "{$this->moduleName}::products.promotions";
    }
    public function index(Request $request)
    {

        $perPage = Helpers::getSettingsField('cms_items_per_page', parent::globalSettings());

        $items = ProductsPromotionsId::orderBy('position')
            ->with(['globalName', 'items',
                'products' => function ($q) {
                    $q->withTrashed();
                }])
            ->paginate($perPage);

        $response = [
            'items' => $items,
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.list", $response);

    }
    public function create()
    {

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.createEdit", $response);
    }
    public function edit($id, $langId)
    {

        $item = ProductsPromotionsId::with(['files.file', 'itemByLang'])->findOrFail($id);

        $response = [
            'path' => $this->path,
            'moduleName' => $this->moduleName,
            'item' => $item,
            'langId' => $langId
        ];

        return view("{$this->path}.createEdit", $response);
    }
    public function trash()
    {
        $items = ProductsPromotionsId::onlyTrashed()->with('globalName')->get();

        $response = [
            'items' => $items,
            'moduleName' => $this->moduleName,
        ];

        return view("{$this->path}.trash", $response);
    }
    public function save(ProductsPromotionsRequest $request, $id, $langId)
    {
        $currComponent = Helpers::currComponent();

        if (is_null($id) && is_null($langId)) {
            $lang = Languages::where('active', 1)
                ->find($request->get('lang'));

            if (is_null($lang))
                return response()->json([
                    'status' => false,
                    'msg' => [
                        'e' => ['Lang not exist!'],
                        'type' => 'warning'
                    ]
                ]);

            $langId = $lang->id;
        }

        $item = ProductsPromotionsId::find($id);

        if (is_null($item)) {
            $item = new ProductsPromotionsId();

            $nextPosition = Helpers::getNextItemPosition($item);
            $item->position = $nextPosition;
            $item->active = 1;
        }

        $item->slug = $request->get('slug', null);
        $item->show_on_main = $request->get('show_on_main') == 'on' ? 1 : 0;
        $item->start_date = $request->get('start_date', null) ? carbon($request->get('start_date')) : null;
        $item->end_date = $request->get('end_date', null) ? carbon($request->get('end_date')) : null;

        $item->save();

        $item->itemByLang()->updateOrCreate([
            'products_promotion_id' => $item->id,
            'lang_id' => $langId
        ], [
            'name' => $request->get('name', null),
            'description' => $request->get('description', null),
            'meta_title' => $request->get('meta_title', null),
            'meta_keywords' => $request->get('meta_keywords', null),
            'meta_description' => $request->get('meta_description', null)
        ]);

        Helpers::updateValidateDynamicField($request->all(), $currComponent, $id, $langId);
        updateMenuItems($currComponent, $item, $langId);
        helpers()->uploadFiles($request->get('files'), $item->id, $currComponent->id, is_null($id) ? 'create' : 'edit');
        helpers()->logActions($currComponent, $item, @$item->globalName->name, is_null($id) ? 'create' : 'edit');

        if (is_null($id))
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item was successful created!",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug])
            ]);
        else
            return response()->json([
                'status' => true,
                'msg' => [
                    'e' => "Item, {$item->globalName->name}, was successful edited",
                    'type' => 'success'
                ],
                'redirect' => url(LANG, ['admin', $currComponent->slug, 'edit', $item->id, $langId])
            ]);
    }
    public function actionsCheckbox(Request $request)
    {
        $currComponent = parent::currComponent();
        $ItemsId = $request->get('id');

        if (empty($ItemsId))
            return response()->json([
                'status' => false
            ]);

        switch ($request->get('event')) {
            case 'status_check':

                ProductsPromotionsId::whereIn('id', $ItemsId)->update(['active' => (int)$request->get('action')]);
                break;
            case 'delete-to-trash':
                $items = ProductsPromotionsId::whereIn('id', $ItemsId)->with(['globalName'])->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->delete();
                        helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-to-trash');
                    }
                }
                break;
            case 'delete-from-trash':
                $items = ProductsPromotionsId::onlyTrashed()->whereIn('id', $ItemsId)->with(['globalName'])->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                    $item->files()->delete();
                    $item->items()->delete();
                    $item->productsItemsPromotions()->delete();
                    $item->forceDelete();
                    helpers()->logActions($currComponent, $item, @$item->globalName->name, 'deleted-from-trash');
                    }
                }
                break;
            case 'restore-from-trash':
                $items = ProductsPromotionsId::onlyTrashed()->whereIn('id', $ItemsId)->with(['globalName'])->get();
                if (!$items->isEmpty()) {
                    foreach ($items as $item) {
                        $item->restore();
                    helpers()->logActions($currComponent, $item, @$item->globalName->name, 'restored-from-trash');
                    }
                }
                break;

            default:

                break;
        }

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => ['Action successfully applied'],
                'type' => 'info'
            ]
        ]);
    }
    public function updatePosition(Request $request)
    {

        $order = $request->get('order');

        if (!is_array($order))
            return response()->json([
                'status' => false
            ]);


        $items = ProductsPromotionsId::find($order)
            ->sortBy(function ($model) use ($order) {
                return array_search($model->getKey(), $order);
            });

        $itemsPositions = [];

        if ($items->isNotEmpty())
            foreach ($items as $item) {
                $itemsPositions[] = @$item->position;
            }

        $itemsPositions = array_filter($itemsPositions);
        sort($itemsPositions);

        foreach ($order as $key => $id) {
            $items->find($id)->update(['position' => $itemsPositions[$key]]);
        }

        return response()->json([
            'status' => true
        ]);
    }
    public function activateItem(Request $request)
    {

        $currComponent = parent::currComponent();

        $item = ProductsPromotionsId::with(['globalName'])->findOrFail($request->get('id'));

        if ($request->get('active')) {
            $status = 0;
            $msg = __("{$this->moduleName}::e.element_is_inactive", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'inactivate');
        } else {
            $status = 1;
            $msg = __("{$this->moduleName}::e.element_is_active", ['name' => @$item->globalName->name]);
            helpers()->logActions($currComponent, $item, @$item->globalName->name, 'activate');
        }

        $item->update(['active' => $status]);

        updateMenuItems($currComponent, $item, null, 'activate');

        return response()->json([
            'status' => true,
            'msg' => [
                'e' => $msg,
                'type' => 'info',
            ]
        ]);
    }

    //    Methods for menu
    public function getCategoriesForMenu()
    {
        $items = ProductsPromotionsId::where('active', 1)
            ->with(['globalName'])
            ->get();

        return $items;
    }

    public function getItem($id)
    {
        $item = ProductsPromotionsId::where('active', 1)
            ->with('globalName')
            ->find($id);

        $response = [];

        if (!is_null($item))
            $response = (object)[
                'id' => $item->id,
                'name' => !is_null(@$item->globalName->name) ? $item->globalName->name : null,
                'slug' => $item->slug,
                'allInfo' => $item
            ];

        return $response;
    }

//    Methods for menu

}