// Определяем зависимости в переменных
const gulp = require('gulp'),
    gulpSequence = require('gulp-sequence'),
    gulpif = require('gulp-if'),
    debug = require('gulp-debug'),
    del = require('del'), //для clean
    watch = require('gulp-watch'),
    plumber = require('gulp-plumber'), //(Предохраняем Gulp от вылета)
    include = require('gulp-include'), //include
    sass = require('gulp-sass'),
    sassModuleImporter = require('sass-module-importer'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCss = require('gulp-clean-css'), //minifier
    sourceMaps = require('gulp-sourcemaps'),
    uglifyJs = require('gulp-uglify'),
    babel = require('gulp-babel'),
    imagemin = require('gulp-imagemin'),
    imageminJpegRecompress = require('imagemin-jpeg-recompress'),
    imageminPngquant = require('imagemin-pngquant'),
    svgSprite = require('gulp-svg-sprite'),
    svgmin = require('gulp-svgmin'),
    cheerio = require('gulp-cheerio'),
    replace = require('gulp-replace'),
    //browserSync = require('browser-sync'),
    // reload = browserSync.reload,
    // gutil = require('gulp-util'),
    spritesmith = require("gulp.spritesmith");
    // ngrok = require('ngrok');

require('dotenv').config(); // need for access to process.env

const isProd = process.env.APP_ENV === 'production';
const themes = process.env.LOAD_THEMES;

// Создаём js объект в котором прописываем все нужные нам пути
let paths = {
    admin: {
        public: {
            js: 'public/admin-assets/js/',
            ckeditorFile: 'public/admin-assets/js/ckeditor/',
            css: 'public/admin-assets/css/',
            img: 'public/admin-assets/img/',
            fonts: 'public/admin-assets/fonts/',
            favicon: 'public/admin-assets/',
            video: 'public/admin-assets/video/'
        },
        src: { //Пути откуда брать исходники
            srcFolder: 'resources/assets/admin/',
            js: 'resources/assets/admin/js/main.js',//В стилях и скриптах нам понадобятся только main файлы
            //jsFiles: ['resources/assets/admin/js/*.js', '!resources/assets/admin/js/main.js'], //для форматирования (пока не используется)
            //jsFolder: 'resources/assets/admin/js/', //для форматирования (пока не используется)
            ckeditorFolder: 'resources/assets/admin/js/ckeditor/**/*',
            styles: 'resources/assets/admin/styles/main.scss',
            stylesPartialsFolder: 'resources/assets/admin/styles/partials/', //для спрайта
            img: ['resources/assets/admin/img/**/*.*', '!resources/assets/admin/img/icons/*.*'], //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
            imgFolder: 'resources/assets/admin/img/', //для спрайта
            icons: 'resources/assets/admin/img/icons/*.*',
            svgIcons: 'resources/assets/admin/img/svg-icons/**/*.svg',
            fonts: 'resources/assets/admin/fonts/**/*.*',
            //fontsFolder: 'resources/assets/admin/fonts/',
            favicon: 'resources/assets/admin/favicon.{png,ico}',
            video: 'resources/assets/admin/video/**/*'
        },
        watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
            js: 'resources/assets/admin/js/**/*.js',
            styles: 'resources/assets/admin/styles/**/*.{css,scss}',
            img: ['resources/assets/admin/img/**/*.*', '!resources/assets/admin/img/icons/*.*'],
            icons: 'resources/assets/admin/img/icons/*.*',
            svgIcons: 'resources/assets/admin/img/svg-icons/**/*.svg',
            fonts: 'resources/assets/admin/fonts/**/*',
            favicon: 'resources/assets/admin/favicon.{png,ico}',
            video: 'resources/assets/admin/video/**/*',
        }
    },
    front: {
        public: {
            js: 'public/assets/'+themes+'/js/',
            css: 'public/assets/'+themes+'/css/',
            img: 'public/assets/img/',
            fonts: 'public/assets/'+themes+'/fonts/',
            php: 'public/assets/'+themes+'/',
            favicon: 'public/assets/'+themes+'/',
            video: 'public/assets/'+themes+'/video/'
        },
        src: { //Пути откуда брать исходники
            srcFolder: 'resources/assets/front/'+themes+'/',
            js: 'resources/assets/front/'+themes+'/js/main.js',//В стилях и скриптах нам понадобятся только main файлы
            //jsFiles: ['resources/assets/front/js/*.js', '!resources/assets/front/js/main.js'], //для форматирования (пока не используется)
            //jsFolder: 'resources/assets/front/js/', //для форматирования (пока не используется)
            styles: 'resources/assets/front/'+themes+'/styles/main.scss',
            stylesPartialsFolder: 'resources/assets/front/'+themes+'/styles/partials/', //для спрайта
            img: ['resources/assets/front/'+themes+'/img/**/*.*', '!resources/assets/front/'+themes+'/img/icons/*.*'], //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
            imgFolder: 'resources/assets/front/'+themes+'/img/', //для спрайта
            icons: 'resources/assets/front/'+themes+'/img/icons/*.*',
            svgIcons: 'resources/assets/front/'+themes+'/img/svg-icons/**/*.svg',
            fonts: 'resources/assets/front/'+themes+'/fonts/**/*.*',
            //fontsFolder: 'resources/assets/front/fonts/',
            favicon: 'resources/assets/front/'+themes+'/favicon.{png,ico}',
            video: 'resources/assets/front/'+themes+'/video/**/*'
        },
        watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
            js: 'resources/assets/front/'+themes+'/js/**/*.js',
            styles: 'resources/assets/front/'+themes+'/styles/**/*.{css,scss}',
            img: ['resources/assets/front/'+themes+'/img/**/*.*', '!resources/assets/front/img/icons/*.*'],
            icons: 'resources/assets/front/'+themes+'/img/icons/*.*',
            svgIcons: 'resources/assets/front/'+themes+'/img/svg-icons/**/*.svg',
            fonts: 'resources/assets/front/'+themes+'/fonts/**/*',
            favicon: 'resources/assets/front/'+themes+'/favicon.{png,ico}',
            video: 'resources/assets/front/'+themes+'/video/**/*',
        }
    }
};

// browserSync config
// const browserSyncConfig = {
//     server: {
//         baseDir: "./dev"
//     },
//     //tunnel: true,
//     host: 'localhost',
//     port: 2626,
//     logPrefix: "Gulp",
//     notify: false,
//     open: false
// };

// Создадим переменную с настройками плагина plumber для захвата ошибок
const plumberOptions = {
    handleError: function (err) {
        console.log(err);
        this.emit('end');
    }
};

// Создадим переменную с настройками плагина autoprefixer
const autoprefixerOptions = {
    browsers: ['last 2 versions', "ie 10", 'android 4'],
    cascade: false
};

/**************************Общие таски***************************/
// Task for browserSync
// gulp.task('browserSync', function () {
//     browserSync(browserSyncConfig);
//     // browserSync(browserSyncConfig, function (err, bs) {
//     // 	ngrok.connect({
//     // 		proto: 'http', // http|tcp|tls
//     // 		addr: bs.options.get('port'), // port or network address
//     // 	}, function (err, url) {
//     // 		gutil.log('[ngrok]', ' => ', gutil.colors.magenta.underline(url));
//     // 	});
//     // });
// });

// Task for img-sprite
gulp.task('admin-sprite', function () {
    let spriteData =
        gulp.src(paths.admin.src.icons) // путь, откуда берем картинки для спрайта
            .pipe(debug({title: 'building sprite:', showFiles: false}))
            .pipe(spritesmith({
                imgName: '../img/sprite.png',
                cssName: '_sprite.scss',
                algorithm: 'binary-tree',
                padding: 5,
                cssVarMap: function (sprite) {
                    sprite.name = 'i-' + sprite.name //имя каждой иконки будет состоять из имени файла и конструкции 'i-' в начале имени
                }
            }));
    spriteData.img.pipe(gulp.dest(paths.admin.src.imgFolder)); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest(paths.admin.src.stylesPartialsFolder)); // путь, куда сохраняем стили
});

// Task for svg-sprite
gulp.task('admin-svg-sprite', function () {
    return gulp.src(paths.admin.src.svgIcons)
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        // remove all fill, style and stroke declarations in out shapes
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[stroke]').removeAttr('stroke');
                $('[style]').removeAttr('style');
            },
            parserOptions: {xmlMode: true}
        }))
        // cheerio plugin create unnecessary string '&gt;', so replace it.
        .pipe(replace('&gt;', '>'))
        // build svg sprite
        .pipe(svgSprite({
            mode: {
                symbol: {
                    sprite: "../sprite.svg"
                }
            }
        }))
        .pipe(gulp.dest(paths.admin.src.imgFolder));
});

///////////////////////////////dev - prod///////////////////////////////

// Task for js:
gulp.task('admin-js', ['admin-ckeditor'], function () {
    return gulp.src(paths.admin.src.js) //Найдем наш main.js файл
        .pipe(debug({title: 'building js:', showFiles: false}))
        .pipe(plumber(plumberOptions))
        .pipe(gulpif(!isProd, sourceMaps.init())) //Инициализируем sourcemap
        .pipe(include()).on('error', console.log)
        .pipe(gulpif(isProd, babel({
            presets: ['es2015-script'], //es2015-script is for runing in browser, this = window, not undefined
            plugins: ["transform-object-rest-spread"]
//			plugins: ['']
        })))
        .pipe(gulpif(isProd, uglifyJs()))
        .pipe(gulpif(!isProd, sourceMaps.write('./')))
        .pipe(gulp.dest(paths.admin.public.js));
});

gulp.task('admin-ckeditor', function () {
    return gulp.src(paths.admin.src.ckeditorFolder)
        .pipe(debug({title: 'Copying ckeditor:', showFiles: false}))
        .pipe(gulp.dest(paths.admin.public.ckeditorFile))
});

// Task for styles:
gulp.task('admin-styles', function () {
    return gulp.src(paths.admin.src.styles) //Выберем наш main.scss
        .pipe(debug({title: 'building css:', showFiles: false}))
        .pipe(plumber(plumberOptions))
        .pipe(gulpif(!isProd, sourceMaps.init()))
        .pipe(include()).on('error', console.log)
        .pipe(sass({importer: sassModuleImporter()}).on('error', sass.logError))
        .pipe(gulpif(isProd, autoprefixer(autoprefixerOptions)))
        .pipe(gulpif(isProd, cleanCss({compatibility: 'ie10'})))
        .pipe(gulpif(!isProd, sourceMaps.write('./')))
        .pipe(gulp.dest(paths.admin.public.css));
    // .pipe(reload({stream: true}));
});

// Task for optimizing images
gulp.task('admin-img', function () {
    return gulp.src(paths.admin.src.img) //Выберем наши картинки
        .pipe(debug({title: 'building img:', showFiles: false}))
        .pipe(plumber(plumberOptions))
        .pipe(gulpif(isProd, gulp.dest(paths.admin.public.img))) //Копируем изображения заранее, imagemin может пропустить парочку )
        .pipe(gulpif(isProd, imagemin([
            imagemin.gifsicle({interlaced: true}),
            imageminJpegRecompress({
                progressive: true,
                max: 80,
                min: 70
            }),
            imageminPngquant({quality: [.7, .8]}),
        ])))
        .pipe(gulp.dest(paths.admin.public.img));
});

// Task for fonts
gulp.task('admin-fonts', function () {
    return gulp.src(paths.admin.src.fonts)
        .pipe(debug({title: 'Copying fonts:', showFiles: false}))
        .pipe(gulp.dest(paths.admin.public.fonts))
});

// Task for favicon
gulp.task('admin-favicon', function () {
    return gulp.src(paths.admin.src.favicon)
        .pipe(debug({title: 'Copying favicon:', showFiles: false}))
        .pipe(gulp.dest(paths.admin.public.favicon))
});

// Task for video
gulp.task('admin-video', function () {
    return gulp.src(paths.admin.src.video)
        .pipe(debug({title: 'Copying video:', showFiles: false}))
        .pipe(gulp.dest(paths.admin.public.video))
});

// Task for cleaning
gulp.task('admin-clean', function (cb, done) {
    return del([
        isProd ? `${paths.admin.public.js}*.*` : '',
        paths.admin.public.js,
        paths.admin.public.css,
        paths.admin.public.img,
        paths.admin.public.fonts,
        paths.admin.public.video,
    ]);
});

// Task for build
gulp.task('admin', gulpSequence('admin-clean', 'admin-sprite', 'admin-svg-sprite', [
    'admin-js',
    'admin-styles',
    'admin-fonts',
    'admin-favicon',
    'admin-img',
    'admin-video'
]));

// Task for watching files and run tasks
gulp.task('admin-watch', function () {
    watch(paths.admin.watch.styles, function (event, cb) {
        // gulpSequence('admin-styles');
        gulp.start('admin-styles');
        // gulpSequence('admin-styles', reload);
    });
    watch(paths.admin.watch.js, function (event, cb) {
        // gulpSequence('admin-js');
        gulp.start('admin-js');
        // gulpSequence('admin-js', reload);
    });
    watch(paths.admin.watch.fonts, function (event, cb) {
        // gulpSequence('admin-fonts');
        gulp.start('admin-fonts');
        // gulpSequence('admin-fonts', reload);
    });
    watch(paths.admin.watch.favicon, function (event, cb) {
        // gulpSequence('admin-favicon');
        gulp.start('admin-favicon');
        // gulpSequence('admin-favicon', reload);
    });
    watch(paths.admin.watch.img, function (event, cb) {
        // gulpSequence('admin-img');
        gulp.start('admin-img');
        // gulpSequence('admin-img', reload);
    });
    watch(paths.admin.watch.video, function (event, cb) {
        // gulpSequence('admin-video');
        gulp.start('admin-video');
        // gulpSequence('admin-video', reload);
    });
    watch(paths.admin.watch.icons, function (event, cb) {
        // gulpSequence('admin-sprite');
        gulp.start('admin-sprite');
        // gulpSequence('admin-sprite', reload);
    });
    watch(paths.admin.watch.svgIcons, function (event, cb) {
        // gulpSequence('admin-svg-sprite');
        gulp.start('admin-svg-sprite');
        // gulpSequence('admin-svg-sprite', reload);
    });
});

/**************************Front part***************************/

// Task for img-sprite
gulp.task('front-sprite', function () {
    let spriteData =
        gulp.src(paths.front.src.icons) // путь, откуда берем картинки для спрайта
            .pipe(debug({title: 'building sprite:', showFiles: false}))
            .pipe(spritesmith({
                imgName: '../img/sprite.png',
                cssName: '_sprite.scss',
                algorithm: 'binary-tree',
                padding: 5,
                cssVarMap: function (sprite) {
                    sprite.name = 'i-' + sprite.name //имя каждой иконки будет состоять из имени файла и конструкции 'i-' в начале имени
                }
            }));
    spriteData.img.pipe(gulp.dest(paths.front.src.imgFolder)); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest(paths.front.src.stylesPartialsFolder)); // путь, куда сохраняем стили
});

// Task for svg-sprite
gulp.task('front-svg-sprite', function () {
    return gulp.src(paths.front.src.svgIcons)
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        // remove all fill, style and stroke declarations in out shapes
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[stroke]').removeAttr('stroke');
                $('[style]').removeAttr('style');
            },
            parserOptions: {xmlMode: true}
        }))
        // cheerio plugin create unnecessary string '&gt;', so replace it.
        .pipe(replace('&gt;', '>'))
        // build svg sprite
        .pipe(svgSprite({
            mode: {
                symbol: {
                    sprite: "../sprite.svg"
                }
            }
        }))
        .pipe(gulp.dest(paths.front.src.imgFolder));
});

///////////////////////////////dev - prod///////////////////////////////

// Task for js:
gulp.task('front-js', function () {
    return gulp.src(paths.front.src.js) //Найдем наш main.js файл
        .pipe(debug({title: 'building js:', showFiles: false}))
        .pipe(plumber(plumberOptions))
        .pipe(gulpif(!isProd, sourceMaps.init()))
        .pipe(include()).on('error', console.log)
        .pipe(gulpif(isProd, babel({
            presets: ['es2015-script'], //es2015-script is for runing in browser, this = window, not undefined
            plugins: ["transform-object-rest-spread"]
//			plugins: ['']
        })))
        .pipe(gulpif(isProd, uglifyJs()))
        .pipe(gulpif(!isProd, sourceMaps.write('./')))
        .pipe(gulp.dest(paths.front.public.js)); //Выплюнем готовый файл в dev
});

// Task for styles:
gulp.task('front-styles', function () {
    return gulp.src(paths.front.src.styles) //Выберем наш main.scss
        .pipe(debug({title: 'building css:', showFiles: false}))
        .pipe(plumber(plumberOptions))
        .pipe(gulpif(!isProd, sourceMaps.init()))
        .pipe(include()).on('error', console.log)
        .pipe(sass({importer: sassModuleImporter()}).on('error', sass.logError))
        .pipe(gulpif(isProd, autoprefixer(autoprefixerOptions)))
        .pipe(gulpif(isProd, cleanCss({compatibility: 'ie10'})))
        .pipe(gulpif(!isProd, sourceMaps.write('./')))
        .pipe(gulp.dest(paths.front.public.css));
        // .pipe(reload({stream: true}));
});

// Task for optimizing images
gulp.task('front-img', function () {
    return gulp.src(paths.front.src.img)
        .pipe(debug({title: 'building img:', showFiles: false}))
        .pipe(plumber(plumberOptions))
        .pipe(gulpif(isProd, gulp.dest(paths.front.public.img))) //Копируем изображения заранее, imagemin может пропустить парочку )
        .pipe(gulpif(isProd, imagemin([
            imagemin.gifsicle({interlaced: true}),
            imageminJpegRecompress({
                progressive: true,
                max: 80,
                min: 70
            }),
            imageminPngquant({quality: [.7, .8]}),
        ])))
        .pipe(gulp.dest(paths.front.public.img));
});

// Task for fonts
gulp.task('front-fonts', function () {
    return gulp.src(paths.front.src.fonts)
        .pipe(debug({title: 'Copying fonts:', showFiles: false}))
        .pipe(gulp.dest(paths.front.public.fonts))
});

// Task for favicon
gulp.task('front-favicon', function () {
    return gulp.src(paths.front.src.favicon)
        .pipe(debug({title: 'Copying favicon:', showFiles: false}))
        .pipe(gulp.dest(paths.front.public.favicon))
});

// Task for video
gulp.task('front-video', function () {
    return gulp.src(paths.front.src.video)
        .pipe(debug({title: 'Copying video:', showFiles: false}))
        .pipe(gulp.dest(paths.front.public.video))
});

// Task for cleaning
gulp.task('front-clean', function (cb, done) {
    return del([
        paths.front.public.js,
        paths.front.public.css,
        paths.front.public.img,
        paths.front.public.fonts,
        paths.front.public.video,
    ]);
});

// Task for build
gulp.task('front', gulpSequence('front-clean', 'front-sprite', 'front-svg-sprite', [
    'front-js',
    'front-styles',
    'front-fonts',
    'front-favicon',
    'front-img',
    'front-video'
]));

// Task for watching files and run tasks
gulp.task('front-watch', function (cb, done) {
    watch(paths.front.watch.styles, function (event, cb) {
        // gulpSequence('front-styles');
        gulp.start('front-styles');
        // gulpSequence('front-styles', reload);
    });
    watch(paths.front.watch.js, function (event, cb) {
        // gulpSequence('front-js');
        gulp.start('front-js');
        // gulpSequence('front-js', reload);
    });
    watch(paths.front.watch.fonts, function (event, cb) {
        // gulpSequence('front-fonts');
        gulp.start('front-fonts');
        // gulpSequence('front-fonts', reload);
    });
    watch(paths.front.watch.favicon, function (event, cb) {
        // gulpSequence('front-favicon');
        gulp.start('front-favicon');
        // gulpSequence('front-favicon', reload);
    });
    watch(paths.front.watch.img, function (event, cb) {
        // gulpSequence('front-img');
        gulp.start('front-img');
        // gulpSequence('front-img', reload);
    });
    watch(paths.front.watch.video, function (event, cb) {
        // gulpSequence('front-video');
        gulp.start('front-video');
        // gulpSequence('front-video', reload);
    });
    watch(paths.front.watch.icons, function (event, cb) {
        // gulpSequence('front-sprite');
        gulp.start('front-sprite');
        // gulpSequence('front-sprite', reload);
    });
    watch(paths.front.watch.svgIcons, function (event, cb) {
        // gulpSequence('front-svg-sprite');
        gulp.start('front-svg-sprite');
        // gulpSequence('front-svg-sprite', reload);
    });
});

// Default task (gulp)
gulp.task('default', function (cb) {
    (isProd ? gulpSequence('admin', 'front') : gulpSequence('admin', 'front', ['admin-watch', 'front-watch']))(cb)
});