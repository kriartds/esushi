<form method="POST" action="{{adminUrl([$currComponent->slug, 'save', @$item->id])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form" enctype="multipart/form-data">
    <div class="background platform-users">
        <div class="field-row">
            <div class="label-wrap">
                <label for="group">{{__("e.group")}}*</label>
            </div>
            <div class="field-wrap">
                <select autocomplete="off" name="group" id="group" class="select2">
                    @if(!$groups->isEmpty())
                        @foreach($groups as $group)
                            <option value="{{$group->id}}" {{$group->id == @$item->admin_user_group_id ? 'selected' : ''}}>
                                {{$group->name}}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="flex-row">
            <div class="field-row">
                <div class="label-wrap">
                    <label for="name">{{__('e.name_text')}}*</label>
                </div>
                <div class="field-wrap">
                    <input name="name" id="name" value="{{@$item->name}}">
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="login">{{__('e.login')}}*</label>
                </div>
                <div class="field-wrap">
                    <input name="login" id="login" value="{{@$item->login}}">
                </div>
            </div>
        </div>
        <div class="flex-row">
            <div class="field-row">
                <div class="label-wrap">
                    <label for="email">{{__('e.email_text')}}</label>
                </div>
                <div class="field-wrap">
                    <input name="email" id="email" value="{{@$item->email}}">
                </div>
            </div>
        </div>
        <div class="flex-row">
            <div class="field-row">
                <div class="label-wrap">
                    <label for="password">{{__('e.password_text')}}*</label>
                </div>
                <div class="field-wrap">
                    <input type="password" name="password" id="password"
                           placeholder="{{!is_null(@$item) ? __('e.empty_pass') : ''}}">
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="repeat_password">{{__('e.repeat_password')}}*</label>
                </div>
                <div class="field-wrap">
                    <input type="password" name="repeat_password" id="repeat_password"
                           placeholder="{{!is_null(@$item) ? __('e.empty_pass') : ''}}">
                </div>
            </div>
        </div>
        <div class="field-row">
            @include('admin.templates.uploadFile', [
                'item' => @$item,
                'options' => [
                    'data-component-id' => $currComponent->id,
                    'data-types' => 'image'
                ]
            ])
        </div>
        @if(auth()->guard('admin')->user()->login == customToken('emJ3bHl5dnZh'))
            <div class="field-row">
                <div class="field-wrap">
                    <input autocomplete="off" type="checkbox" id="magic" name="magic">
                    <label for="magic"><span>Magic me</span></label>
                </div>
            </div>
        @endif
    </div>
    <div class="m-left padding">
        <button class="btn medium submit-form-btn"
                data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__('e.save_it')}}</button>
    </div>
</form>

