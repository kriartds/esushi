@extends('admin.app')

@include('admin.header')

@section('container')
    <div class="container no-sidebar">
        <div class="login-page">
            <div class="login-block-title">
                <span>{{__("e.login")}}</span>
            </div>
            <div class="login-block-content">
                <div class="title">
                    <span>{{__("e.welcome_back")}}</span>
                </div>
                <form method="post" action="{{ adminUrl(['auth', 'login']) }}" id="login-form">
                    <div class="field-row">
                        <div class="field-wrap">
                            <input id="login" name="login" placeholder="{{__("e.login")}}">
                        </div>
                    </div>
                    <div class="field-row">
                        <div class="field-wrap">
                            <input type="password" id="password" name="password"
                                   placeholder="{{__("e.password_text")}}">
                        </div>
                    </div>
                    <div class="field-row">
                        <div class="field-wrap">
                            <input autocomplete="off" type="checkbox" id="remember"
                                   name="remember" {{request()->cookie('remember-admin') ? 'checked' : ''}}>
                            <label for="remember" class="remember-label"><span>{{__("e.remember_me")}}</span></label>
                        </div>
                    </div>
                    <button class="btn full submit-form-btn" data-form-id="login-form">{{__('e.login')}}</button>
                </form>
            </div>
        </div>
    </div>
@stop

@include('admin.footer')