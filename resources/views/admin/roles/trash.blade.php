@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent, null, null, true) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'trash'], 'item' => null, 'currentPage' => 'trash'])

        <div class="table-block">
            @if(!$items->isEmpty())
                <table class="table trash">
                    <thead>
                    <tr>
                        <th>{{__("e.id_table")}}</th>
                        <th class="left">{{__('e.title_table')}}</th>
                        <th></th>
                        <th class="checkbox-all medium"><div>Select All</div></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td class="small">
                                <p>{{$item->id}}</p>
                            </td>
                            <td class="left medium">{{$item->name ?: ''}}</td>
                            <td class="empty-td"></td>

                            @if($permissions->delete)
                                <td class="checkbox-items">
                                    @if(auth()->user()->root || !$item->root)
                                        @if(auth()->user()->id != $item->id)
                                            <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                                   name="checkbox_items[{{$item->id}}]" value="{{$item->id}}">
                                            <label for="{{$item->id}}"></label>
                                        @else
                                            <svg class="tooltip" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                                 viewBox="0 0 20 20" data-tippy="" data-original-title="This item has children!">
                                                <g id="Group_360" data-name="Group 360" transform="translate(-1400 -1236)">
                                                    <circle id="Ellipse_22" data-name="Ellipse 22" cx="10" cy="10" r="10"
                                                            transform="translate(1400 1236)" fill="#9f9f9f"></circle>
                                                    <path id="Path_79" data-name="Path 79"
                                                          d="M2.769-3.056,3-9.953H.9l.232,6.9Zm-.82,1.08a1.091,1.091,0,0,0-.8.3,1,1,0,0,0-.3.749.992.992,0,0,0,.3.745,1.1,1.1,0,0,0,.8.294,1.107,1.107,0,0,0,.8-.294.992.992,0,0,0,.3-.745,1,1,0,0,0-.3-.752A1.107,1.107,0,0,0,1.948-1.976Z"
                                                          transform="translate(1408 1251)" fill="#fff"></path>
                                                </g>
                                            </svg>                                                @endif
                                    @else
                                        <svg class="tooltip" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                             viewBox="0 0 20 20" data-tippy="" data-original-title="This item has children!">
                                            <g id="Group_360" data-name="Group 360" transform="translate(-1400 -1236)">
                                                <circle id="Ellipse_22" data-name="Ellipse 22" cx="10" cy="10" r="10"
                                                        transform="translate(1400 1236)" fill="#9f9f9f"></circle>
                                                <path id="Path_79" data-name="Path 79"
                                                      d="M2.769-3.056,3-9.953H.9l.232,6.9Zm-.82,1.08a1.091,1.091,0,0,0-.8.3,1,1,0,0,0-.3.749.992.992,0,0,0,.3.745,1.1,1.1,0,0,0,.8.294,1.107,1.107,0,0,0,.8-.294.992.992,0,0,0,.3-.745,1,1,0,0,0-.3-.752A1.107,1.107,0,0,0,1.948-1.976Z"
                                                      transform="translate(1408 1251)" fill="#fff"></path>
                                            </g>
                                        </svg>                                            @endif
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__('e.list_is_empty')}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')