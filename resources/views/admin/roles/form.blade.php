<form method="POST" action="{{adminUrl([$currComponent->slug, 'save', @$item->id])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form" enctype="multipart/form-data">

    <div class="rights">
        <div class="background">
            <div class="flexTopCol">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__('e.title_table')}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$item->name}}">
                    </div>
                </div>

                <div class="field-row-column">
                    <span class="span">{{__("e.group_rights")}}</span>
                    <div class="activate-maintenance">

                        <div class="field-row">

                            <div class="label-wrap">
                                <label for="global-check-all-rights">{{__("e.check_all_rights")}}</label>
                            </div>
                        </div>

                        <div class="field-wrap">
                            <input type="checkbox" name="global-check-all-rights" id="global-check-all-rights"
                                   value="{{@$item->name}}">
                            <label for="global-check-all-rights">
                                <div class="slider"></div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-block">
            <table class="table">
                <tbody>
                <tr class="hoverNone">
                    <th></th>
                    {{--                    <th>{{__("e.check_all")}}</th>--}}
                    <th></th>
                    <th class="boldTh">{{__("e.create_items")}}</th>
                    <th class="boldTh">{{__("e.edit_items")}}</th>
                    <th class="boldTh">{{__("e.delete_items")}}</th>
                    <th class="boldTh">{{__("e.view_items")}}</th>
                    <th class="boldTh">{{__("e.active_items")}}</th>
                </tr>
                {!! getParentChildrenInTable($components, $rights) !!}
                </tbody>
            </table>
        </div>
    </div>
    <div class="m-left">
        <button class="btn medium submit-form-btn"
                data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__('e.save_it')}}</button>
    </div>
</form>

