<div class="container-setings">

    <form method="POST" action="{{adminUrl([$currComponent->slug, 'save'])}}" id="edit-form"
          enctype="multipart/form-data">
        <div class="general-setings background">
            <h3>General</h3>
            <div class="select-logo">
                <div class="field-row">
                    <div class="field-row">
                        @include('admin.templates.uploadFile', [
                            'label' => __("e.site_logo"),
                            'item' => @$items['files'],
                            'options' => [
                                'data-component-id' => $currComponent->id,
                                'data-types' => 'image'
                            ],
                            'settings_file' => true,
                        ])
                    </div>
                </div>
                <div class="logo-atention">Attention! This logo must...</div>
            </div>

            <div class="inputFor-home-mentenance-items" style="display: none">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="home_page">{{__("e.home_page")}}</label>
                    </div>
                    <div class="field-wrap">
                        <select name="home_page" id="home_page" class="select2">
                            <option value="">-</option>
                            @if(!$pages->isEmpty())
                                @foreach($pages as $page)
                                    <option value="{{$page->id}}" {{$page->id == @$items['home_page']->value ? 'selected' : ''}}>{{@$page->globalName->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="items-per-page">
                    <div class="label-wrap small-title">
                        <label for="cms_items_per_page">Items per page</label>
                    </div>
                    <div class="flex-container">
                        <div class="field-row">
                            <div class="label-wrap">
                                <label>In panel admin</label>
                            </div>
                            <div class="field-wrap">
                                <input name="cms_items_per_page" id="cms_items_per_page"
                                       value="{{@$items['cms_items_per_page']->value}}">
                            </div>
                        </div>

                        <div class="field-row">
                            <div class="label-wrap">
                                <label for="site_items_per_page">In client area</label>
                            </div>
                            <div class="field-wrap">
                                <input name="site_items_per_page" id="site_items_per_page"
                                       value="{{@$items['site_items_per_page']->value}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="activate-maintenance">
                    <div class="field-row">
                        <div class="label-wrap">
                            <label for="maintenance_mode">{{__("e.maintenance_mode")}}</label>
                        </div>
                    </div>

                    <div class="field-wrap">
                        <input type="checkbox" name="maintenance_mode"
                               id="maintenance_mode" {{@$items['maintenance_mode']->value ? 'checked' : ''}}>
                        <label for="maintenance_mode">
                            <div class="slider"></div>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="contacts-container background">
            <h3>Contacts</h3>
            <div class="email-phone">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="email">{{__("e.email")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="email" id="email" value="{{ @$items['email']->value }}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="email">{{__("e.order_email")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="order_email" id="order_email" value="{{ @$items['order_email']->value }}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="restaurant_reservations">{{__("e.restaurant_reservations")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="restaurant_reservations" id="restaurant_reservations" value="{{ @$items['restaurant_reservations']->value }}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="delivery_operator">{{__("e.delivery_operator")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="delivery_operator" id="delivery_operator" value="{{ @$items['delivery_operator']->value }}">
                    </div>
                </div>
            </div>
            <div class="iframe-map">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="google-iframe">{{__("e.google_map_iframe")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="google-iframe" id="google-iframe" value="{{ @$items['google-iframe']->value }}">
                    </div>
                </div>
            </div>

            <div class="field-row">
                <div class="title">
                    <span>{{__("e.socials_links")}}</span>
                </div>
            </div>
            <div class="social-links">
                <div class="field-row ">
                    <div class="label-wrap">
                        <label for="instagram_link">Instagram link</label>
                    </div>
                    <div class="field-wrap instagram">
                        <input  name="instagram_link" id="instagram_link" value="{{ @$items['instagram_link']->value }}">
                    </div>
                </div>
                <div class="field-row ">
                    <div class="label-wrap">
                        <label for="facebook_link">Facebook link</label>
                    </div>
                    <div class="field-wrap facebook">
                        <input name="facebook_link" id="facebook_link" value="{{ @$items['facebook_link']->value }}">
                    </div>
                </div>
                <div class="field-row ">
                    <div class="label-wrap">
                        <label for="twitter_link">Twitter link</label>
                    </div>
                    <div class="field-wrap twitter ">
                        <input name="twitter_link" id="twitter_link" value="{{ @$items['twitter_link']->value }}">
                    </div>
                </div>

            </div>
        </div>

        <div class="contacts-container background">
            <h3>Working hours</h3>
            <div class="field-row">
                <div class="label-wrap">
                    <label for=""></label>
                </div>
                @foreach(['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'] as $day)

                    <div class="field-wrap">
                        <label style="display: inline-block;width: 30px;">{{$day}}</label>
                        - from
                        <select name="working_hours[{{$day}}][from]">
                            @for($i=0;$i<=23;$i++)
                                <option value="{{$i}}:00" @if(@$working_hours[$day]['from'] == $i.':00') selected @endif>
                                    {{$i}}:00
                                </option>
                            @endfor
                        </select>
                        to
                        <select name="working_hours[{{$day}}][to]">
                            @for($i=0;$i<=23;$i++)
                                <option value="{{$i}}:00" @if(@$working_hours[$day]['to'] == $i.':00') selected @endif>
                                    {{$i}}:00
                                </option>
                            @endfor
                        </select>
                    </div>
                @endforeach
            </div>
        </div>
{{--        <div class="email-configuration background">--}}
{{--            <div class="field-row">--}}
{{--                <h3>{{__("e.email_configuration")}}</h3>--}}
{{--            </div>--}}
{{--            <div class="flex-mail">--}}
{{--                <div class="field-row">--}}
{{--                    <div class="label-wrap">--}}
{{--                        <label for="config_email">{{__("e.send_email_to")}}</label>--}}
{{--                    </div>--}}
{{--                    <div class="field-wrap">--}}
{{--                        <input name="config_email" id="config_email" value="{{ @$items['config_email']->value }}"--}}
{{--                               autocomplete="off">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="field-row">--}}
{{--                    <div class="label-wrap">--}}
{{--                        <label for="config_email_pass">{{__("e.email_password")}}</label>--}}
{{--                    </div>--}}
{{--                    <div class="field-wrap">--}}
{{--                        <input type="password" name="config_email_pass" id="config_email_pass"--}}
{{--                               value="{{ @$items['config_email_pass']->value }}" autocomplete="off">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="field-row">--}}
{{--                    <div class="label-wrap">--}}
{{--                        <label for="config_email_host">{{__("e.email_host")}}</label>--}}
{{--                    </div>--}}
{{--                    <div class="field-wrap">--}}
{{--                        <input name="config_email_host" id="config_email_host"--}}
{{--                               value="{{ @$items['config_email_host']->value }}">--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="field-row">--}}
{{--                    <div class="label-wrap">--}}
{{--                        <label for="config_email_port">{{__("e.email_port")}}</label>--}}
{{--                    </div>--}}
{{--                    <div class="field-wrap">--}}
{{--                        <input name="config_email_port" id="config_email_port"--}}
{{--                               value="{{ @$items['config_email_port']->value }}">--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--            <div class="buttonSetings">--}}
{{--                <button class="btn" style="display: none" id="test_connection">Test conection</button>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="buttonSetings">
            <button class="btn  submit-form-btn" data-form-id="edit-form">{{__('e.save_it')}}</button>
        </div>
    </form>
</div>