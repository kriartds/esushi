@if(@$emailMsg)
    <table style="border: 1px solid #AFB5B9; color: #2C2B2A;border-collapse: collapse;font-family: 'Raleway','Arial Black', sans-serif;width: 600px;">
        <tr style="background-color: #E1E6E9">
            <td style="padding: 10px; text-align: center" colspan="2">
                <a href="{{url(LANG)}}" target="_blank">
                    {!! getLogo($allSettings) !!}
                </a>
            </td>
        </tr>
        <tr style="background-color: #E1E6E9">
            <td style="padding: 10px;text-align: center;text-transform: uppercase" colspan="2">
                <h1 style="color: #2C2B2A;">{{__("e.import_products_on")}} {{config('app.name')}}</h1>
            </td>
        </tr>
        <tr>
            <td style="padding: 10px;" colspan="2">
                <p style="color: #2C2B2A;">{{@$emailMsg}}</p>
                <a href="{{@$url}}">{{@$linkText}}</a>
            </td>
        </tr>
    </table>
@endif