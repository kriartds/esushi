@if(isset($item))
    <table style="border: 1px solid #AFB5B9; color: #2C2B2A;border-collapse: collapse;font-family: 'Raleway','Arial Black', sans-serif;width: 600px;">
        <tr style="background-color: #E1E6E9">
            <td style="padding: 10px; text-align: center" colspan="2">
                <a href="{{customUrl()}}" target="_blank">
                    {!! getLogo($allSettings) !!}
                </a>
            </td>
        </tr>
        <tr style="background-color: #E1E6E9">
            <td style="padding: 10px;text-align: center;" colspan="2">
                <h1 style="color: #2C2B2A;font-size:26px;">{{__("e.new_feedback_on")}} <a href="{{url('/')}}">{{@getSettingsField('site_title', $allSettings, true)}}</a></h1>
            </td>
        </tr>
        <tr style="background-color: #E1E6E9">
            <td style="padding: 10px;text-align: left;" colspan="2">
                <h3 style="margin: 0;">{{__("e.client")}}</h3>
            </td>
        </tr>
        <tr style="text-align: left;">
            <td style="border: 1px solid #AFB5B9;width: 50%; padding: 7px 5px;text-align: left;">{{__("e.name")}}</td>
            <td style="border: 1px solid #AFB5B9;width: 50%;padding: 7px 5px;text-align: left;">{{@$item->name}}</td>
        </tr>
        <tr style="">
            <td style="border: 1px solid #AFB5B9;padding: 7px 5px;text-align: left;">{{__("e.phone")}}</td>
            <td style="border: 1px solid #AFB5B9;padding: 7px 5px;text-align: left;">{{@$item->phone}}</td>
        </tr>
        <tr style="">
            <td style="border: 1px solid #AFB5B9;padding: 7px 5px;text-align: left;">{{__("e.email")}}</td>
            <td style="border: 1px solid #AFB5B9;padding: 7px 5px;text-align: left;">{{@$item->email}}</td>
        </tr>
        <tr style="background-color: #E1E6E9">
            <td style="padding: 5px">{{__("e.message")}}</td>
            <td style="padding: 5px; font-size: 12px;">{{@$item->message}}</td>
        </tr>
        @if(!is_null(strtotime($item->created_at)))
            <tr style="background-color: #E1E6E9">
                <td style="padding: 5px">{{__("e.date")}}</td>
                <td style="padding: 5px;color: #B5111E">{{strtotime($item->created_at) ? date('d-m-Y H:i', strtotime($item->created_at)) : '-'}}</td>
            </tr>
        @endif
    </table>
@endif