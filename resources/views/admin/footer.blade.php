@section('footer')
    @if(auth()->guard('admin')->check())
        <div style="display: none;">
            <div class="modal modal-gallery" id="modal-gallery">
                <div class="modal-top">
                    <div class="modal-title">{{__("e.gallery")}}</div>
                    <div class="modal-close arcticmodal-close"></div>
                </div>
                <div class="modal-center">
                    <div class="gallery-dropzone">
                        <div class="progress-bar">
                            <span></span>
                        </div>
                        <div class="form-block filter-gallery-block">
                            <form action method="post" id="filter-gallery-files">
                                <div class="filter-fields-wrap">
                                    <div class="filter-fields">
                                        <select name="gallery-file-type" id="gallery-file-type"
                                                class="select2 no-search">
                                            <option value="">{{__("e.all_file_types")}}</option>
                                            <option value="image">{{__("e.images")}}</option>
                                            <option value="video">{{__("e.video")}}</option>
                                            <option value="application">{{__("e.files")}}</option>
                                            <option value="audio">{{__("e.audio")}}</option>
                                            <option value="text">{{__("e.text")}}</option>
                                        </select>
                                        <select name="gallery_component" id="gallery-component" class="select2">
                                            <option value="">{{__("e.all_components")}}</option>
                                            <option value="test">Test</option>
                                            @if(!$components->isEmpty())
                                                {!! getParentChildrenInSelect($components) !!}
                                            @endif
                                        </select>
                                    </div>
                                    <div class="filter-search">
                                        <input name="q" id="search_file" placeholder="{{__("e.search_files_by_name")}}">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <form action="{{adminUrl(['uploadFile'])}}" method="post" class="dropzone"
                              id="gallery-file-upload"
                              enctype="multipart/form-data">
                            <input type="hidden" id="all-selected-files">
                            <input type="hidden" id="all-selected-files-sizes">
                            <div class="fallback">
                                <input name="file" type="file" style="display: none;"/>
                            </div>
                        </form>
                        <div id="gallery-loader-gif"></div>
                    </div>
                    <div class="gallery-right-sidebar">
                        <div class="file-content">
                            @include('admin.templates.galleryFileContent')
                        </div>
                    </div>
                </div>
                <div class="modal-bottom">
                    <button class="btn right" id="gallery-insert-files"
                            data-component-name="{{@$currComponent->globalName->name}}" disabled></button>
                </div>
                @include('admin.templates.dropzonePreviewTemplate')
            </div>
        </div>
    @endif


    <div id="wait-sitemap" class="hidden">{{__('e.wait_sitemap_msg')}}</div>

    <!-- <div class="hidden" id="notifications-block"></div> -->

    <!-- <button class="go-top"></button> -->
    <div id="loader-gif"></div>
@stop