
<div class="dropdown" id="actionsForm" data-url="{{url(LANG, ['admin', @$currComponent->slug, 'actions-checkbox']).'?'.request()->getQueryString()}}">
    <button class="button blue" id="listActionsBtn">Actions</button>
    <div class="dropdown-menu">
        @if($currComponent->slug != 'reservation-components' && $currComponent->slug != 'feedback' && $currComponent->slug != 'subscribers' &&  $currComponent->slug != 'emails-for-subscribers' &&  $currComponent->slug != 'logs' && $currComponent->slug != 'reviews')
            <div class="dropdown-item">
                Set status to inactive / active:
                <div class="checkbox-switcher">
                    <input autocomplete="off" type="checkbox" id="status_check" name="status_check"
                           data-event="status_check">
                    <label for="status_check"></label>
                </div>
            </div>
        @endif

        @if($currComponent->slug == 'products')
            <div class="dropdown-item">
                Apply instant discount:
                <div class="checkbox-switcher">
                    <input autocomplete="off" type="checkbox" name="set_percent" id="set_percent" value="1"
                           data-event="set_percent">
                    <label for="set_percent"></label>
                </div>
                <div class="input-container">
                    <input type="text" name="percent_count" placeholder="10" value="" max="100">
                </div>
            </div>
        @endif

        <div class="dropdown-item">
            <form class="export-data">
                <?php
                    $componentsAcceptExport = [
                        'products',
                        'subscribers',
                        'orders',
                    ];
                ?>
                @if(in_array($currComponent->slug, $componentsAcceptExport))
                    <label class="export" for="uploadfile"
                           data-url="{{url(LANG, ['admin', $currComponent->slug, 'exportList']) . "{$pushUrl}"}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="23" height="20" viewBox="0 0 23 20">
                            <g>
                                <g>
                                    <g>
                                        <path fill="none" stroke="#647cfe" stroke-miterlimit="50" stroke-width="2"
                                              d="M16.766 1v0H8v18h14v0V5.989v0z"/>
                                    </g>
                                    <g>
                                        <path fill="none" stroke="#647cfe" stroke-miterlimit="50" stroke-width="2"
                                              d="M14 10H4.5"/>
                                    </g>
                                    <g>
                                        <path fill="#647cfe" d="M0 10l5-4v8z"/>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        Export CSV
                    </label>
                @endif
            </form>

            <div class="delete-btn" data-event="delete-to-trash">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="22" viewBox="0 0 20 22">
                    <g>
                        <g>
                            <g>
                                <g>
                                    <path fill="none" stroke="#ea5355" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-miterlimit="50" stroke-width="2" d="M1 4.777v0h2v0h16v0"/>
                                </g>
                                <g>
                                    <path fill="none" stroke="#ea5355" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-miterlimit="50" stroke-width="2"
                                          d="M14 4.535V2.999a2 2 0 0 0-2-2H8a2 2 0 0 0-2 2v1.536m-3 .464v14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2v-14"/>
                                </g>
                                <g>
                                    <path fill="none" stroke="#ea5355" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-miterlimit="50" stroke-width="2" d="M8 9.999v5"/>
                                </g>
                                <g>
                                    <path fill="none" stroke="#ea5355" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-miterlimit="50" stroke-width="2" d="M12 9.999v5"/>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
        </div>
    </div>
</div>