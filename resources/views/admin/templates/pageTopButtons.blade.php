@if(is_null(request()->segment(4)))
    @if (request()->has('shipping_zone'))
        <h1 class="component-title">Shipping Methods List</h1>
    @else
        <h1 class="component-title">{{@$currComponent->globalName->name}} List</h1>
    @endif
@else
    @if (request()->has('shipping_zone'))
        <h1 class="component-title">Shipping Method</h1>
    @else
        <h1 class="component-title">{{@$currComponent->globalName->name}}</h1>
    @endif
@endif
@if(@$action && !empty($action))
    <div class="page-top-buttons">
        <div class="left-col">
            @if(in_array('list', $action))
                @if($currComponent->slug == 'menu-items')
                    <div class="item">
                        <a href="{{adminUrl(['menus'])}}" {{url()->full() == adminUrl([$currComponent->slug]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '') ? 'class=active' : ''}}>{{__("e.nav_items_list")}}</a>
                    </div>
                @else

                    <div class="item">
                        <a href="{{adminUrl([$currComponent->slug]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}" {{url()->full() == adminUrl([$currComponent->slug]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '') ? 'class=active' : ''}}>{{__("e.nav_items_list")}}</a>
                    </div>
                @endif
            @endif

            @if(in_array('create', $action) && $permissions->create)
                <div class="item">
                    <a href="{{adminUrl([$currComponent->slug, 'create']) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}" {{url()->current() == adminUrl([$currComponent->slug, 'create']) ? 'class=active' : ''}}>{{__("e.nav_create_item")}}</a>
                </div>
            @endif

            @if(in_array('import', $action) && $permissions->create)
                <div class="item">
                    <a href="{{adminUrl([$currComponent->slug, 'import']) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}" {{url()->current() == adminUrl([$currComponent->slug, 'import']) ? 'class=active' : ''}}>Import</a>
                </div>
            @endif


            @if(in_array('edit', $action) && !is_null($item) && $permissions->edit)
                <div class="item">
                    <a href="{{adminUrl([$currComponent->slug, 'edit', $item->id]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}" {{url()->current() == adminUrl([$currComponent->slug, 'edit', $item->id, @$langId]) ? 'class=active' : ''}}>{{__("e.nav_edit_item")}}</a>
                </div>
            @endif
            @if(in_array('view', $action) && !is_null($item) && $permissions->view)
                <div class="item">
                    <a href="{{adminUrl([$currComponent->slug, 'view', $item->id]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}" {{url()->current() == adminUrl([$currComponent->slug, 'view', $item->id]) ? 'class=active' : ''}}>{{__("e.nav_view_item")}}</a>
                </div>
            @endif
            @if(in_array('trash', $action))
                <div class="item">
                    <a href="{{adminUrl([$currComponent->slug, 'trash']) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}" {{url()->current() == adminUrl([$currComponent->slug, 'trash']) ? 'class=active' : ''}}>{{__("e.nav_trash_item")}}</a>
                </div>
            @endif
            @if(@$goToSitePage)
                <div class="item">
                    <a href="{{$goToSitePage}}" target="_blank">{{__("e.nav_visit_item_page")}}</a>
                </div>
            @endif
        </div>

        @if(isset($filter))
            @include("{$path}.filter", ['filterParams' => @$filterParams, 'items' => @$items])
        @endif

        @if(isset($actionButton) && $actionButton)
            @include('admin.templates.actionsButton')
        @endif

        @if(isset($currentPage) && $currentPage == 'trash')
            <div class="right-col">
                <div class="dropdown" id="actionsForm"
                     data-url="{{url(LANG, ['admin', @$currComponent->slug, 'actions-checkbox'])}}">
                    <button class="button blue" id="listActionsBtn">Actions</button>
                    <div class="dropdown-menu">
                        <div class="dropdown-item">
                            <div class="restore-btn" data-event="restore-from-trash">
                                <svg xmlns="http://www.w3.org/2000/svg" width="23" height="20"
                                     viewBox="0 0 23 20">
                                    <g>
                                        <g>
                                            <path fill="#34ca76"
                                                  d="M13.143 0C7.7 0 3.286 4.478 3.286 10H0l4.26 4.322.077.156L8.762 10H5.476c0-4.3 3.428-7.778 7.667-7.778C17.38 2.222 20.81 5.7 20.81 10c0 4.3-3.429 7.778-7.667 7.778a7.551 7.551 0 0 1-5.41-2.29l-1.556 1.579C7.962 18.877 10.416 20 13.143 20 18.586 20 23 15.522 23 10S18.586 0 13.143 0z"/>
                                        </g>
                                    </g>
                                </svg>
                                Restore
                            </div>

                            <div class="delete-btn" data-event="delete-from-trash">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="22"
                                     viewBox="0 0 20 22">
                                    <g>
                                        <g>
                                            <g>
                                                <g>
                                                    <path fill="none" stroke="#ea5355"
                                                          stroke-linecap="round"
                                                          stroke-linejoin="round" stroke-miterlimit="50"
                                                          stroke-width="2" d="M1 4.777v0h2v0h16v0"/>
                                                </g>
                                                <g>
                                                    <path fill="none" stroke="#ea5355"
                                                          stroke-linecap="round"
                                                          stroke-linejoin="round" stroke-miterlimit="50"
                                                          stroke-width="2"
                                                          d="M14 4.535V2.999a2 2 0 0 0-2-2H8a2 2 0 0 0-2 2v1.536m-3 .464v14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2v-14"/>
                                                </g>
                                                <g>
                                                    <path fill="none" stroke="#ea5355"
                                                          stroke-linecap="round"
                                                          stroke-linejoin="round" stroke-miterlimit="50"
                                                          stroke-width="2" d="M8 9.999v5"/>
                                                </g>
                                                <g>
                                                    <path fill="none" stroke="#ea5355"
                                                          stroke-linecap="round"
                                                          stroke-linejoin="round" stroke-miterlimit="50"
                                                          stroke-width="2" d="M12 9.999v5"/>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endif

@if(@$reportsAction && !empty($reportsAction))
    <div class="page-top-buttons">
        @if(in_array('orders', $reportsAction) && $permissions->view)
            <div class="item">
                <a href="{{adminUrl([$currComponent->slug])}}" {{url()->current() == adminUrl([$currComponent->slug]) ? 'class=active' : ''}}>{{__("e.nav_orders")}}</a>
            </div>
        @endif
        @if(in_array('customers', $reportsAction) && $permissions->view)
            <div class="item">
                <a href="{{adminUrl([$currComponent->slug, 'customers'])}}" {{url()->current() == adminUrl([$currComponent->slug, 'customers']) ? 'class=active' : ''}}>{{__("e.nav_customers")}}</a>
            </div>
        @endif
        @if(in_array('stock', $reportsAction) && $permissions->view)
            <div class="item">
                <a href="{{adminUrl([$currComponent->slug, 'stock'])}}" {{url()->current() == adminUrl([$currComponent->slug, 'stock']) ? 'class=active' : ''}}>{{__("e.nav_stock")}}</a>
            </div>
        @endif
        @if(isset($filter))
            @include("{$currPath}.filter", ['filterParams' => @$filterParams, 'items' => @$items])
        @endif
    </div>
@endif