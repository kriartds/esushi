@php
    //'options' => [
    //            'data-types' => 'image',
    //            'data-component-id' => $currComponent->id,
    //            'data-editor' => 'description'
    //            'data-menu-field' => 'name of field' -> only for menu upload files
    //            'data-single-upload' => 'true' -> just for single upload
    //]

        if(@$options) {
            if(is_array($options))
                $options = implode(' ', array_map(function ($v, $k) {
                    if(is_int($k))
                        $response = $v;
                    else
                        $response = "{$k}={$v}";
                    return $response;
                }, $options, array_keys($options)));
        }
        else
            $options = '';
@endphp

<div class="label-wrap">
    <label for="img">{{@$label ? $label : __("e.upload_files")}}</label>
</div>
<div class="input-wrap">
    <div class='file-div'>
        @if(!@$settings_file)
            <button type="button" class="button blue open-gallery-modal getModal"
                    {{$options}} data-modal="#modal-gallery"
                    data-hidden-filed-name="{{@$fieldName ? $fieldName : 'files'}}">
                <span>{{__('e.select_files')}}</span>
            </button>
        @endif

        <div class="files-list {{!empty($item) && !$item->files->isEmpty() ? 'not-empty' : ''}}"
             data-position-url="{{adminUrl(['changeFilePosition'])}}">
            @if(!empty($item) && !$item->files->isEmpty())
                @foreach($item->files as $key => $oneFile)
                    <div class="img-block"
                         style="background-image: url('{{asset($oneFile->file->fileLocationBySize)}}');"
                         data-id="{{$oneFile->id}}" data-file-id="{{$oneFile->file->id}}">
                        <input type="hidden" name="{{@$fieldName ? $fieldName : 'files'}}[]"
                               value="{{$oneFile->file->id}}">
                        <span class="remove-upload-file" data-file-id="{{$oneFile->id}}" data-is-attachment></span>
                        <span class="active-upload-file {{$oneFile->active == 1 ? 'active' : ''}}"
                              data-active="{{$oneFile->active}}" data-file-id="{{$oneFile->id}}"
                              data-is-attachment>
                        </span>
                    </div>
                @endforeach
            @endif
        </div>

        @if(@$settings_file)
            <div class="label-wrap new-logo">
                <label for="img">Upload new logo</label>
            </div>
            <button type="button" class="button blue open-gallery-modal getModal"
                    {{$options}} data-modal="#modal-gallery"
                    data-hidden-filed-name="{{@$fieldName ? $fieldName : 'files'}}">
                <span>{{__('e.select_files')}}</span>
            </button>
        @endif
    </div>
</div>
