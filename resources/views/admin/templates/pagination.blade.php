@if ($pagination->lastPage() > 1)
    @php
        $start = $pagination->currentPage() - 3;
        $end = $pagination->currentPage() + 3;
        $last_page = $pagination->lastPage();
        if ($start < 1) $start = 1;
        if ($end >= $pagination->lastPage()) $end = $pagination->lastPage();
    @endphp

    <div class="pagination-block">
        <ul class="pagination">
            <li {{ ($pagination->currentPage() == 1) ? 'class=disabled' : '' }}>
                {!! ($pagination->currentPage() == 1) ? '<span class="prev"></span>' : '<a href="' . $pagination->url($pagination->currentPage()-1) . (queryToString('page') ? '&' . queryToString('page') : '') . '" class="prev"></a>' !!}
            </li>
            <li {{ ($pagination->currentPage() == 1) ? 'class=active' : '' }}>
                {!! ($pagination->currentPage() == 1) ? '<span>' . 1 . '</span>' : '<a href="' . $pagination->url(1) . (queryToString('page') ? '&' . queryToString('page') : '') . '">' . 1 . '</a>' !!}
            </li>
            @if($start > 1)
                <li>
                    <span>...</span>
                </li>
            @endif
            @for ($i = $start + 1; $i < $end; $i++)
                <li {{ ($pagination->currentPage() == $i) ? 'class=active' : '' }}>
                    {!! ($pagination->currentPage() == $i) ? '<span>' . $i . '</span>' : '<a href="' . $pagination->url($i) . (queryToString('page') ? '&' . queryToString('page') : '') . '">' . $i . '</a>' !!}
                </li>
            @endfor
            @if($end < $pagination->lastPage())
                <li>
                    <span>...</span>
                </li>
            @endif
            <li {{ ($pagination->currentPage() == $last_page) ? 'class=active' : '' }}>
                {!! ($pagination->currentPage() == $last_page) ? '<span>' . $last_page . '</span>' : '<a href="' . $pagination->url($last_page) . (queryToString('page') ? '&' . queryToString('page') : '') . '">' . $last_page . '</a>' !!}
            </li>
            <li {{ ($pagination->currentPage() == $pagination->lastPage()) ? 'class=disabled' : '' }}>
                {!! ($pagination->currentPage() == $pagination->lastPage()) ? '<span class="next"></span>' : '<a href="' . $pagination->url($pagination->currentPage()+1) . (queryToString('page') ? '&' . queryToString('page') : '') . '" class="next"></a>'  !!}
            </li>
        </ul>
    </div>
@endif