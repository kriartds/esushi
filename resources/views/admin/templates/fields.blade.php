@if(isset($fieldType))
    @if(@$isRepeaterField && @$repeatFieldId && !is_null(@$multipleFieldsIds) && !empty($multipleFieldsIds))
        <div class="field-row">
            <div class="title">
                <span>{{@$repeatFieldId->label_name}}</span>
            </div>
        </div>
        <div class="field-row clone-items">
            @foreach($multipleFieldsIds as $key => $fieldIds)
                @if($fieldIds->isNotEmpty())
                    <div class="field-wrap field-wrap-children clone {{$displayOption}}">
                        @foreach($fieldIds as $fieldId)
                            @php
                                $repeatFieldType = @$fieldId->fieldType->slug;
                            @endphp
                            @if($repeatFieldType == 'input')
                                <div class="inline-field-wrap flex-1">
                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id . '-' . $key}}">{{@$fieldId->label_name}}{{@$fieldId->is_required ? '*' : ''}}</label>
                                        </div>
                                        <div class="field-wrap">
                                            <input name="repeater-dynamic-field-{{$repeatFieldId->slug}}[{{$fieldId->slug}}][{{$key}}]"
                                                   id="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id . '-' . $key}}"
                                                   class="{{@$fieldId->class_name}}"
                                                   {{@$fieldId->attributes}} value="{{@$fieldId->itemByLang->value}}"
                                                   placeholder="{{@$fieldId->itemByLang->placeholder}}">
                                        </div>
                                    </div>
                                </div>
                            @elseif(($repeatFieldType == 'radio' || $repeatFieldType == 'checkbox') && !$fieldId->fieldValues->isEmpty())
                                <div class="inline-field-wrap flex-1">
                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id}}">{{@$fieldId->label_name}}{{$fieldId->is_required ? '*' : ''}}</label>
                                        </div>
                                        <div class="field-wrap radio-checkbox">
                                            @foreach($fieldId->fieldValues as $fieldValue)
                                                <input type="{{$repeatFieldType == 'checkbox' ? 'checkbox' : 'radio'}}"
                                                       name="repeater-dynamic-field-{{$repeatFieldId->slug}}[{{$fieldId->slug}}][{{$key}}]{{$repeatFieldType == 'checkbox' ? '[]' : ''}}"
                                                       id="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldValue->input_key . '-' . $key}}"
                                                       class="{{$fieldId->class_name}}"
                                                       {{@$fieldId->attributes}} {{$fieldValue->is_selected ? 'checked' : ''}} value="{{$fieldValue->input_key}}">
                                                <label for="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldValue->input_key . '-' . $key}}">{{@$fieldValue->globalName->name}}</label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @elseif($repeatFieldType == 'textarea' || $repeatFieldType == 'ckeditor')
                                <div class="inline-field-wrap flex-1">
                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id}}">{{@$fieldId->label_name}}{{$fieldId->is_required ? '*' : ''}}</label>
                                        </div>
                                        <div class="field-wrap">
                                    <textarea
                                            name="repeater-dynamic-field-{{$repeatFieldId->slug}}[{{$fieldId->slug}}][{{$key}}]"
                                            id="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id}}"
                                            {{$repeatFieldType == 'ckeditor' ? 'data-type=ckeditor' : ''}} class="{{$fieldId->class_name}}"
                                            {{@$fieldId->attributes}} placeholder="{{@$fieldId->itemByLang->placeholder}}">{{@$fieldId->itemByLang->value}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            @elseif($repeatFieldType == 'files')
                                <div class="inline-field-wrap flex-1">
                                    <div class="field-row">
                                        @include('admin.templates.uploadFile', [
                                            'label' => @$fieldId->label_name,
                                            'item' => @$fieldId,
                                            'fieldName' => "repeater-dynamic-field-files-{$repeatFieldId->slug}[{$fieldId->slug}][{$key}]",
                                            'options' => @$fieldId->attributes
                                        ])
                                    </div>
                                </div>
                            @elseif(($repeatFieldType == 'select' || $repeatFieldType == 'multiple-select') && !$fieldId->fieldValues->isEmpty())
                                <div class="inline-field-wrap flex-1">
                                    <div class="field-row">
                                        <div class="label-wrap">
                                            <label for="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id}}">{{@$fieldId->label_name}}{{$fieldId->is_required ? '*' : ''}}</label>
                                        </div>
                                        <div class="field-wrap radio-checkbox">
                                            <select name="repeater-dynamic-field-{{$repeatFieldId->slug}}[{{$fieldId->slug}}][{{$key}}]{{$repeatFieldType == 'multiple-select' ? '[]' : ''}}"
                                                    id="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id}}"
                                                    class="select2 {{$fieldId->class_name}}"
                                                    {{@$fieldId->attributes}} {{$repeatFieldType == 'multiple-select' ? 'multiple' : ''}}>
                                                @foreach($fieldId->fieldValues as $fieldValue)
                                                    <option value="{{$fieldValue->input_key}}" {{$fieldValue->is_selected ? 'selected' : ''}}>
                                                        {{@$fieldValue->globalName->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        <div title="{{__('e.destroy_field')}}" class="destroy-clone tooltip"
                             data-repeater-id="{{$repeatFieldId->id}}"
                             data-group-id="{{$key}}"
                             data-clone-type="dynamic-fields"></div>
                    </div>
                @endif
            @endforeach
            <div class="label-wrap">
                <span class="clone-item tooltip" title="{{__('e.clone_field')}}" data-clone-type="dynamic-fields"
                      data-url="{{url(LANG, ['admin', 'createDestroyRepeatField'])}}"
                      data-field-id="{{$repeatFieldId->id}}"
                      data-destroy-url="{{url(LANG, ['admin', 'createDestroyRepeatField'])}}"></span>
            </div>
        </div>
    @elseif(@$fieldId)
        @if($fieldType->slug == 'input')
            <div class="field-row">
                <div class="label-wrap">
                    <label for="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id}}">{{@$fieldId->label_name}}{{$fieldId->is_required ? '*' : ''}}</label>
                </div>
                <div class="field-wrap">
                    <input name="dynamic-field-{{$fieldId->slug}}"
                           id="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id}}"
                           class="{{$fieldId->class_name}}"
                           {{@$fieldId->attributes}} value="{{!is_null(@$fieldId->itemByLang) ? $fieldId->itemByLang->value : @$fieldId->globalName->value}}"
                           placeholder="{{@$fieldId->itemByLang->placeholder}}">
                </div>
            </div>
        @elseif(($fieldType->slug == 'radio' || $fieldType->slug == 'checkbox') && !$fieldId->fieldValues->isEmpty())
            <div class="field-row">
                <div class="label-wrap">
                    <label for="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id}}">{{@$fieldId->label_name}}{{$fieldId->is_required ? '*' : ''}}</label>
                </div>
                <div class="field-wrap radio-checkbox">
                    @foreach($fieldId->fieldValues as $fieldValue)
                        <input type="{{$fieldType->slug == 'checkbox' ? 'checkbox' : 'radio'}}"
                               name="dynamic-field-{{$fieldId->slug}}"
                               id="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldValue->input_key}}"
                               class="{{$fieldId->class_name}}"
                               {{@$fieldId->attributes}} {{$fieldValue->is_selected ? 'checked' : ''}} value="{{$fieldValue->input_key}}">
                        <label for="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldValue->input_key}}">{{@$fieldValue->globalName->name}}</label>
                    @endforeach
                </div>
            </div>
        @elseif($fieldType->slug == 'textarea' || $fieldType->slug == 'ckeditor')
            <div class="field-row">
                <div class="label-wrap">
                    <label for="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id}}">{{@$fieldId->label_name}}{{$fieldId->is_required ? '*' : ''}}</label>
                </div>
                <div class="field-wrap">
                <textarea name="dynamic-field-{{$fieldId->slug}}"
                          id="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id}}"
                          {{$fieldType->slug == 'ckeditor' ? 'data-type=ckeditor' : ''}} class="{{$fieldId->class_name}}"
                          {{@$fieldId->attributes}} placeholder="{{@$fieldId->itemByLang->placeholder}}">{{!is_null(@$fieldId->itemByLang) ? $fieldId->itemByLang->value : @$fieldId->globalName->value}}</textarea>
                </div>
            </div>
        @elseif($fieldType->slug == 'files')
            <div class="field-row">
                {{--fieldName--}}
                {{--labelName--}}
                {{--trans--}}
                @include('admin.templates.uploadFile', [
                    'label' => @$fieldId->label_name,
                    'item' => @$fieldId,
                    'fieldName' => "dynamic-field-files-{$fieldId->slug}",
                    'options' => @$fieldId->attributes
                ])
            </div>
        @elseif(($fieldType->slug == 'select' || $fieldType->slug == 'multiple-select') && !$fieldId->fieldValues->isEmpty())
            <div class="field-row">
                <div class="label-wrap">
                    <label for="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id}}">{{@$fieldId->label_name}}{{$fieldId->is_required ? '*' : ''}}</label>
                </div>
                <div class="field-wrap radio-checkbox">
                    <select name="dynamic-field-{{$fieldId->slug}}"
                            id="{{str_replace('[]', '', $fieldId->slug) . '-' . $fieldId->id}}"
                            class="select2 {{$fieldId->class_name}}"
                            {{@$fieldId->attributes}} {{$fieldType->slug == 'multiple-select' ? 'multiple' : ''}}>
                        @foreach($fieldId->fieldValues as $fieldValue)
                            <option value="{{$fieldValue->input_key}}" {{$fieldValue->is_selected ? 'selected' : ''}}>
                                {{@$fieldValue->globalName->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        @endif

    @endif
@endif
