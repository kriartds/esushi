<div id="preview-template" style="display: none;">
    <div class="dz-preview dz-file-preview">
        <span class="active-upload-file active" data-active="1"></span>
        <div class="dz-image"><img data-dz-thumbnail=""></div>

        <div class="dz-details">
            <div class="dz-size"><span data-dz-size=""></span></div>
            <div class="dz-filename"><span data-dz-name=""></span></div>
        </div>
        <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
        <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
        <div class="dz-success-mark">
            {!! svgFileToCode(asset('admin-assets/img/small-icons/dz-success.svg')) !!}
        </div>
        <div class="dz-error-mark">
            {!! svgFileToCode(asset('admin-assets/img/small-icons/dz-error.svg')) !!}
        </div>
    </div>
</div>