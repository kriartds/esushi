@if(@$items && $items->isNotEmpty())
    @foreach($items as $item)
        @if(@$item->data['msg'])
            <div class="notification">
                <div class="close" data-id="{{$item->id}}" title="{{__("e.mark_notification_read")}}"></div>
                <div class="msg">{{@$item->data['msg']}}</div>
                @if(@$item->data['url'])
                    <div class="link">
                        <a href="{{$item->data['url']}}">{{@$item->data['linkText']}}</a>
                    </div>
                @endif
            </div>
        @endif
    @endforeach
@endif