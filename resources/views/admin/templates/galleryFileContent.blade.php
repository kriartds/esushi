<input type="hidden" id="sidebarFileId" name="fileId">
<div class="file-info">
    <div class="img">
        <a href="{{asset('admin-assets/img/no-image.png')}}" data-fancybox="admin-gallery">
            <img src="{{asset('admin-assets/img/no-image.png')}}" alt="icon">
        </a>
    </div>
    <div class="info">
        <span class="file-id"></span>
        <span class="name"></span>
        <span class="date"></span>
        <span class="size"></span>
        <span class="resolution"></span>
    </div>
    <div class="url">
        <span class="copy-url tooltip" title="{{__("e.url_to_clipboard")}}"></span>
        <input value="" id="clipboard-file-url" readonly>
    </div>
</div>
<div class="edit-file">
    <div class="form-block modal-gallery-form hidden">
{{--        <form action="" method="post" id="update-file-info">--}}
{{--            <div class="field-row">--}}
{{--                <div class="label-wrap">--}}
{{--                    <label for="title">Title</label>--}}
{{--                </div>--}}
{{--                <div class="field-wrap">--}}
{{--                    <input name="title" id="title">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="field-row">--}}
{{--                <div class="label-wrap">--}}
{{--                    <label for="alt">Alt</label>--}}
{{--                </div>--}}
{{--                <div class="field-wrap">--}}
{{--                    <input name="alt" id="alt">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </form>--}}
    </div>
</div>
<div class="attachment-info hidden">
    <div class="title">{{__("e.attachment_settings")}}</div>
    <div class="form-block modal-gallery-form">
        <form action="" id="attachment-file-settings">
            <div class="field-row">
                <div class="label-wrap">
                    <label for="link-to">{{__("e.link_to")}}</label>
                </div>
                <div class="field-wrap">
                    <select name="link_to" id="link-to" class="select2 no-search">
                        <option value="" data-type="image">-</option>
                        <option value="media_file" data-type="image">{{__("e.media_file")}}</option>
                        <option value="custom_link">{{__("e.custom_link")}}</option>
                    </select>
                </div>
            </div>
            <div class="field-row hidden" id="custom-link-block">
                <div class="label-wrap">
                    <label for="custom-link">{{__("e.custom_link")}}*</label>
                </div>
                <div class="field-wrap">
                    <input type="text" name="custom_link" id="custom-link">
                </div>
            </div>
            <div class="field-row hidden" id="file-sizes-block">
                <div class="label-wrap">
                    <label for="size">{{__("e.size")}}</label>
                </div>
                <div class="field-wrap">
                    <select name="size" id="size" class="select2 no-search"></select>
                </div>
            </div>
        </form>
    </div>
</div>