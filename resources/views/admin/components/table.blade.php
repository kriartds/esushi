@if(!$items->isEmpty())
    <table class="table" data-position-url="{{adminUrl([$currComponent->slug, 'updatePosition'])}}">
        <thead>
        <tr>
            <th>{{__("e.id_table")}}</th>
            <th>{{__('e.title_table')}}</th>
            @if($permissions->edit)
                <th>{{__('e.edit_table')}}</th>
            @endif
            @if($permissions->active)
                <th>{{__('e.active_table')}}</th>
            @endif
            <th>{{__('e.position_table')}}</th>
            @if($permissions->delete)
                <th class="checkbox-all" >{{__('e.delete_table')}}</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            <tr id="{{$item->id}}">
                <td>{{$item->id}}</td>
                <td {{$item->children->isNotEmpty() ? 'class=td-link' : ''}}>
                    @if($item->children->isNotEmpty())
                        <a href="{{adminUrl([$currComponent->slug]) . "?parent={$item->id}"}}">{{@$item->globalName->name}}</a>
                    @else
                        {{@$item->globalName->name}}
                    @endif
                </td>
                @if($permissions->edit)
                    <td class="td-link">
                        @foreach($langList as $lang)
                            <a href="{{adminUrl([$currComponent->slug, 'edit', $item->id, $lang->id]) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}" {{ is_null(helpers()->getItemByLang($item, $lang->id)) ? 'class=inactive' : ''}}>{{ucfirst($lang->slug)}}</a>
                        @endforeach
                    </td>
                @endif
                @if($permissions->active)
                    <td>
                        <span class="activate-item {{$item->active ? 'active' : ''}}"
                              data-active="{{$item->active}}" data-item-id="{{$item->id}}"
                              data-url="{{adminUrl([$currComponent->slug, 'activateItem'])}}"></span>
                    </td>
                @endif
                <td class="position">
                    <img src="{{asset('admin-assets/img/small-icons/position.svg')}}" alt="">
                </td>
                @if($permissions->delete)
                    <td class="checkbox-items">
                        @if($item->children->isEmpty())
                        <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                               name="destroy_items[{{$item->id}}]"
                               value="{{$item->id}}"

                               data-url="{{adminUrl([$currComponent->slug, 'destroy'])}}">
                        <label for="{{$item->id}}">
                            <!-- <span>{{__("e.select")}}</span> -->
                        </label>
                            @else
                            <span>{{__("e.item_has_children")}}</span>
                        @endif
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
        @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())
            <tfoot>
            <tr>
                <td colspan="10">
                    @include('admin.templates.pagination', ['pagination' => $items])
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    <div class="empty-list">{{__('e.list_is_empty')}}</div>
@endif
