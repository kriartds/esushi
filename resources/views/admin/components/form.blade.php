<div class="components-container">
    <form method="POST" action="{{adminUrl([$currComponent->slug, 'save', @$item->id, @$langId ])}}"
          id="{{!is_null($item) ? 'edit' : 'create'}}-form"
          enctype="multipart/form-data">
        <div class="background">
            <div class="field-row language">
                <div class="label-wrap">
                    <label for="lang">{{__('e.lang')}}*</label>
                </div>
                <div class="field-wrap">
                    @if(!$langList->isEmpty())
                        <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                            @foreach($langList as $lang)
                                <option value="{{$lang->id}}" {{ (is_null(@$langId) && $lang->id == LANG_ID ? 'selected' : $lang->id == @$langId ) ? 'selected' : ''}}>
                                    {{$lang->name ?: ''}}
                                </option>
                            @endforeach
                        </select>
                    @endif
                </div>
            </div>
            <div class="medium-container-component">
                <div class="field-row drpdwn">
                    <div class="label-wrap">
                        <label for="parent">{{__("e.components")}}*</label>
                    </div>
                    <div class="field-wrap">
                        <select autocomplete="off" name="parent" id="parent" class="select2">
                            <option value="">{{__('e.parent_component')}}</option>
                            @if(!$parents->isEmpty())
                                {!! getParentChildrenInSelect($parents, request()->get('parent'), @$item->id) !!}
                            @endif
                        </select>
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="name">{{__('e.title_table')}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name" id="name" value="{{@$item->itemByLang->name}}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="slug">{{__('e.slug_table')}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="slug" id="slug" value="{{@$item->slug}}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="component-module">{{__("e.module")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="module" id="component-module" value="{{@$item->module}}">
                    </div>
                </div>

                <div class="field-row">
                    <div class="label-wrap">
                        <label for="controller">{{__('e.controller')}}*</label>
                    </div>
                    <div class="field-wrap">
                        <input name="controller" id="controller" value="{{@$item->controller}}">
                    </div>
                </div>


                <div class="field-row category" id="categoryComponent">
                    <div class="label-wrap">
                        <label for="name_category">{{__("e.category")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="name_category" id="name_category"
                               value="{{@$item->itemByLang->name_category}}">
                    </div>
                </div>
            </div>
            <div class="field-row-checkbox  ">
                <div class="field-row select-photo">
                    @include('admin.templates.uploadFile', [
                        'item' => @$item,
                        'options' => [
                            'data-component-id' => $currComponent->id,
                            'data-types' => 'image'
                        ]
                    ])
                </div>

                {{--                    <div class="field-row">--}}
                {{--                        <div class="label-wrap">--}}
                {{--                            <label for="for_root">{{__('e.for_root_module')}}</label>--}}
                {{--                        </div>--}}
                {{--                        <div class="field-wrap">--}}
                {{--                            <input type="checkbox" name="for_root"--}}
                {{--                                   id="for_root" {{@$item->for_root ? 'checked' : ''}}>--}}
                {{--                            <label for="for_root"></label>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}

                <div class="field-row inline  white-bg">
                    <div class="label-wrap">
                        <label for="for_root">{{__('e.for_root_module')}}</label>
                    </div>
                    <div class="checkbox-switcher">
                        <input type="checkbox" name="for_root"
                               id="for_root" {{@$item->for_root ? 'checked' : ''}}>
                        <label for="for_root"></label>
                    </div>
                </div>

                {{--                    <div class="field-row">--}}
                {{--                        <div class="label-wrap">--}}
                {{--                            <label for="for_count">{{__('e.for_count_module')}}</label>--}}
                {{--                        </div>--}}
                {{--                        <div class="field-wrap">--}}
                {{--                            <input type="checkbox" name="for_count"--}}
                {{--                                   id="for_count" {{@$item->for_count ? 'checked' : ''}}>--}}
                {{--                            <label for="for_count"></label>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                <div class="field-row inline  white-bg">
                    <div class="label-wrap">
                        <label for="for_count">{{__('e.for_count_module')}}</label>
                    </div>

                    <div class="checkbox-switcher">
                        <input type="checkbox" name="for_count"
                               id="for_count" {{@$item->for_count ? 'checked' : ''}}>
                        <label for="for_count"></label>
                    </div>
                </div>


                {{--                    <div class="field-row">--}}
                {{--                        <div class="label-wrap">--}}
                {{--                            <label for="for_sidebar">{{__('e.for_sidebar_module')}}</label>--}}
                {{--                        </div>--}}
                {{--                        <div class="field-wrap">--}}
                {{--                            <input type="checkbox" name="for_sidebar"--}}
                {{--                                   id="for_sidebar" {{@$item->for_sidebar ? 'checked' : ''}}>--}}
                {{--                            <label for="for_sidebar"></label>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}

                <div class="field-row inline  white-bg">
                    <div class="label-wrap">
                        <label for="for_sidebar">{{__('e.for_sidebar_module')}}</label>
                    </div>
                    <div class="checkbox-switcher">
                        <input type="checkbox" name="for_sidebar"
                               id="for_sidebar" {{@$item->for_sidebar ? 'checked' : ''}}>
                        <label for="for_sidebar"></label>
                    </div>
                </div>

                {{--                    <div class="field-row">--}}
                {{--                        <div class="label-wrap">--}}
                {{--                            <label for="for_menu">{{__('e.for_menu_module')}}</label>--}}
                {{--                        </div>--}}
                {{--                        <div class="field-wrap">--}}
                {{--                            <input type="checkbox" name="for_menu"--}}
                {{--                                   id="for_menu" {{@$item->for_menu ? 'checked' : ''}}>--}}
                {{--                            <label for="for_menu"></label>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}

                <div class="field-row inline  white-bg">
                    <div class="label-wrap">
                        <label for="for_menu">{{__('e.for_menu_module')}}</label>
                    </div>
                    <div class="checkbox-switcher">
                        <input type="checkbox" name="for_menu"
                               id="for_menu" {{@$item->for_menu ? 'checked' : ''}}>
                        <label for="for_menu"></label>
                    </div>
                </div>

                {{--                    <div class="field-row">--}}
                {{--                        <div class="label-wrap">--}}
                {{--                            <label for="for_statistics">{{__('e.for_statistics_module')}}</label>--}}
                {{--                        </div>--}}
                {{--                        <div class="field-wrap">--}}
                {{--                            <input type="checkbox" name="for_statistics"--}}
                {{--                                   id="for_statistics" {{@$item->for_statistics ? 'checked' : ''}}>--}}
                {{--                            <label for="for_statistics"></label>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}


                <div class="field-row inline  white-bg">
                    <div class="label-wrap">
                        <label for="for_statistics">{{__('e.for_statistics_module')}}</label>
                    </div>
                    <div class="checkbox-switcher">
                        <input type="checkbox" name="for_statistics"
                               id="for_statistics" {{@$item->for_statistics ? 'checked' : ''}}>
                        <label for="for_statistics"></label>
                    </div>
                </div>

                {{--                    <div class="field-row">--}}
                {{--                        <div class="label-wrap">--}}
                {{--                            <label for="for_sitemap">{{__('e.for_sitemap')}}</label>--}}
                {{--                        </div>--}}
                {{--                        <div class="field-wrap">--}}
                {{--                            <input type="checkbox" name="for_sitemap"--}}
                {{--                                   id="for_sitemap" {{@$item->for_sitemap ? 'checked' : ''}}>--}}
                {{--                            <label for="for_sitemap"></label>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}

                <div class="field-row inline  white-bg">
                    <div class="label-wrap">
                        <label for="for_sitemap">{{__('e.for_sitemap')}}</label>
                    </div>
                    <div class="checkbox-switcher">
                        <input type="checkbox" name="for_sitemap"
                               id="for_sitemap" {{@$item->for_sitemap ? 'checked' : ''}}>
                        <label for="for_sitemap"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-left">
            <button class="btn medium submit-form-btn"
                    data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__('e.save_it')}}</button>
        </div>
    </form>
</div>
