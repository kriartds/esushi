@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'trash'], 'item' => null])

        <div class="table-block">
            @if(!$items->isEmpty())
                <table class="table">
                    <thead>
                    <tr>
                        <th class="align-center">{{__("e.id_table")}}</th>
                        <th>{{__('e.title_table')}}</th>
                        @if($permissions->delete)
                            <th class="restore-all" data-event="restore">{{__('e.reestablish_table')}}</th>
                            <th class="checkbox-all" >{{__('e.delete_table')}}</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td class="id">
                                {{$item->id}}
                            </td>
                            <td>{{@$item->globalName->name}}</td>
                            @if($permissions->delete)
                                <td class="restore-items">
                                    <input autocomplete="off" type="checkbox" class="restore-item" id="{{$item->id}}"
                                           name="restore_items[{{$item->id}}]"
                                           value="{{$item->id}}"
                                           data-event="restore"
                                           data-url="{{adminUrl([$currComponent->slug, 'destroy'])}}">
                                    <label for="{{$item->id}}">
                                        <span>{{__("e.select")}}</span>
                                    </label>
                                </td>
                                <td class="checkbox-items">
                                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                           name="destroy_items[{{$item->id}}]"
                                           value="{{$item->id}}"

                                           data-url="{{adminUrl([$currComponent->slug, 'destroy'])}}">
                                    <label for="{{$item->id}}">
                                        <span>{{__("e.select")}}</span>
                                    </label>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__('e.list_is_empty')}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')