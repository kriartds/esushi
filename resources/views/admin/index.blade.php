@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">

        <div class="widgets">
{{--            <div class="widgets-column">--}}
{{--                <div class="widget">--}}
{{--                    <div  class="canvas-container" style="width: 400px; height: 400px;" >--}}
{{--                        <canvas style="width: 276px; height: 276px;" id="myChart"></canvas>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}
            <div class="widgets-column">

                @if($statistics->isNotEmpty())
                    <div class="widget">
                        <div class="content-block statistics">
                            @foreach($statistics as $statistic)
                                <a href="{{adminUrl([$statistic->slug])}}" class="item">
                                    <div class="leftBlock">
                                        <div class="icon">
                                            {!! svgFileToCode($statistic->firstFile->original) !!}
                                        </div>
                                        <div class="name">{{@$statistic->globalName->name}}</div>
                                    </div>
                                    <div class="count">{{$statistic->countForWidgets}}</div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                @endif
                @if($reviews->isNotEmpty())
                    <div class="widget">
                        <div class="title">{{__("e.new_reviews")}}</div>
                        <div class="content-block reviews">
                            @foreach($reviews as $review)
                                <div class="item">
                                    <a href="{{adminUrl(['reviews', 'edit', $review->id])}}">
                                    <span class="sub-title">
                                        <span class="name">{{$review->first_name}} {{$review->last_name}}: </span>
                                        <span class="message">{{subStrText($review->message, 200)}}</span>
                                    </span>
                                    </a>
                                    <span class="date">
                                        <span class="bold">{{__("e.date")}}:</span> {{date('d/m/Y', strtotime($review->created_at))}}
                                    </span>
                                </div>
                            @endforeach
                            @if($reviews->count() > 10)
                                <div class="view-all">
                                    <a href="{{adminUrl(['reviews'])}}"
                                       class="btn btn-inline">{{__("e.view_new_reviews", ['count' => $reviewsCount])}}</a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
                @if(isset($analyticsTotalVisitorsAndPageViews) && $analyticsTotalVisitorsAndPageViews->isNotEmpty())
                    <div class="widget">
                        <div class="title">{{__("e.visitors_page_views", ['from_date' => @$analyticsSubMoth->format('d.m.Y'), 'to_date' => @$analyticsCurrDate->format('d.m.Y')])}}</div>
                        <div class="content-block analytics">
                            <div class="item">
                                <div class="chart"
                                     data-chart-values="{{json_encode($analyticsTotalVisitorsAndPageViews)}}">
                                    <canvas id="visitors-chart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if(isset($analyticsUserTypes) && $analyticsUserTypes->isNotEmpty())
                    <div class="widget">
                        {{--                        <div class="title">{{__("e.user_types", ['from_date' => @$analyticsSubMoth->format('d.m.Y'), 'to_date' => @$analyticsCurrDate->format('d.m.Y')])}}</div>--}}
                        <div class="content-block table">
                            <div class="table-block">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>{{__("e.users_type")}}</th>
                                        <th>{{__("e.sessions")}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($analyticsUserTypes as $analyticsUserType)
                                        <tr>
                                            <td class="big">{{$analyticsUserType['type']}}</td>
                                            <td>{{$analyticsUserType['sessions']}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
                @if(isset($analyticsTopBrowsers) && $analyticsTopBrowsers->isNotEmpty())
                    <div class="widget">
                        <div class="title">
                            {{__("e.top_browsers", ['from_date' => @$analyticsSubYear->format('d.m.Y'), 'to_date' => @$analyticsCurrDate->format('d.m.Y')])}}
                        </div>
                        <div class="content-block table">
                            <div class="table-block">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>{{__("e.browser")}}</th>
                                        <th>{{__("e.sessions")}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($analyticsTopBrowsers as $analyticsTopBrowser)
                                        <tr>
                                            <td class="big">{{$analyticsTopBrowser['browser']}}</td>
                                            <td>{{$analyticsTopBrowser['sessions']}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="widgets-column">
                @if(!empty($ordersLabels))
                    <div class="widget">
                        <div class="title">{{__("e.orders", ['from_date' => @$startOrdersMonth->format('d.m.Y'), 'to_date' => @$endOrdersMonth->format('d.m.Y')])}}</div>
                        <div class="content-block orders">
                            <div class="item">
                                <div class="totals">
                                    <div>
                                        <span class="name">{{__("e.paid_orders")}}</span>
                                        <span class="value">{!! formatPrice(@$ordersPaidAmount, $defaultCurrency) !!}</span>
                                    </div>
                                    <div>
                                        <span class="name">{{__("e.pending_orders")}}</span>
                                        <span class="value">{!! formatPrice(@$orderWaitingPaymentAmount, $defaultCurrency) !!}</span>
                                    </div>
                                </div>
                                <div class="chart" data-chart-values="{{json_encode($ordersLabels)}}">
                                    <canvas id="orders-chart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if($productsReviews->isNotEmpty())
                    <div class="widget">
                        <div class="content-block reviews">
                            @foreach($productsReviews as $review)

                                <div class="item">
                                    <a href="{{adminUrl(['products-reviews', 'edit', $review->id])}}">
                                    <span class="sub-title">
                                        {{--                                        TODO" name--}}
                                        <span class="name">Mihai Tibirna: </span>
{{--                                        TODO" BUG REVIEW--}}
                                        <span class="message">{{subStrText(@$review->message->msg, 200)}}</span>
                                    </span>
                                    </a>
                                    <span class="date">
                                        {{date('d/m/Y', strtotime($review->created_at))}}
                                    </span>
                                </div>
                            @endforeach
                            @if($productsReviews->count() > 10)
                                <div class="view-all">
                                    <a href="{{adminUrl(['products-reviews'])}}"
                                       class="btn btn-inline">{{__("e.view_new_reviews_products", ['count' => $productsReviewsCount])}}</a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
                @if(isset($analyticsMostVisitedPages) && $analyticsMostVisitedPages->isNotEmpty())
                    <div class="widget">
                        <div class="title">{{__("e.most_visited_pages", ['from_date' => @$analyticsSubMoth->format('d.m.Y'), 'to_date' => @$analyticsCurrDate->format('d.m.Y')])}}</div>
                        <div class="content-block table">
                            <div class="table-block">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>{{__("e.analytics_url")}}</th>
                                        <th>{{__("e.page_views")}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($analyticsMostVisitedPages as $analyticsMostVisitedPage)
                                        <tr>
                                            <td class="big td-link">
                                                <a href="{{url($analyticsMostVisitedPage['url'])}}"
                                                   target="_blank">{{url($analyticsMostVisitedPage['url'])}}</a>
                                            </td>
                                            <td>{{$analyticsMostVisitedPage['pageViews']}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
                @if(isset($analyticsTopReferrers) && $analyticsTopReferrers->isNotEmpty())
                    <div class="widget">
                        <div class="title">{{__("e.top_referrers", ['from_date' => @$analyticsSubMoth->format('d.m.Y'), 'to_date' => @$analyticsCurrDate->format('d.m.Y')])}}</div>
                        <div class="content-block table">
                            <div class="table-block">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>{{__("e.analytics_url")}}</th>
                                        <th>{{__("e.page_views")}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($analyticsTopReferrers as $analyticsTopReferrer)
                                        <tr>
                                            <td class="big">{{urldecode($analyticsTopReferrer['url'])}}</td>
                                            <td>{{$analyticsTopReferrer['pageViews']}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

@stop

@include('admin.footer')