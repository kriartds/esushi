@section('sidebar')
    <div class="sidebar">
        <div class="copyrightBar">
            <div class="copyright">
                © <span class="span">2021</span>
                <span class="bold"> {{@getSettingsField('site_title', $allSettings, true)}}</span>
            </div>
            <a class="craftedBy" href="https://mediapark.md" target="_blank">Mediapark
            </a>
        </div>
        <div class="wrap">
            @if(!$sidebarComponents->isEmpty())
                <div class="sidebar-items">
                    @foreach($sidebarComponents as $component)
                        <div class="sidebar-item {{$component->hasChildren ? 'has-children' : ''}} {{request()->segment(3) && ($component->hasActiveChildren || $component->slug == request()->segment(3)) ? 'open' : ''}}">
                            <a href="{{adminUrl([$component->slug])}}"
                               {{request()->segment(3) && (request()->segment(3) == $component->slug || $component->hasActiveChildren) ? 'class=active' : ''}} id="{{$component->slug}}"
                               title="{{@$component->globalName->name ?: __('e.missing_name')}}">
                                {!! svgFileToCode($component->firstFile->original) !!}
                                <span>

                                    @if(!@$component->hasChildren)
                                        {{@$component->globalName->name ?: __('e.missing_name')}}
                                    @else
                                        {{@$component->globalName->name_category ?: __('e.missing_name')}}
                                    @endif
                                    {!! !empty($componentsCount = $component->componentsCount) ? "<sup class=count-items>{$componentsCount}</sup>" : "" !!}
                                </span>
                            </a>
                            @if($component->hasChildren)
                                <div class="hidden-sidebar-items {{!request()->segment(3) ? 'hidden' : (!$component->hasActiveChildren && $component->slug != request()->segment(3) ? 'hidden' : '')}}">
                                    <div class="sidebar-item">
                                        <a href="{{adminUrl([$component->slug])}}"
                                           {{request()->segment(3) == $component->slug ? 'class=active' : ''}} id="{{$component->slug}}-child"
                                           title="{{@$component->globalName->name ?: __('e.missing_name')}}">
                                            <span>{{@$component->globalName->name ?: __('e.missing_name')}}</span>
                                        </a>
                                    </div>
                                    @foreach($component->children as $child)
                                        <div class="sidebar-item {{$child->hasChildren ? 'has-children' : ''}} {{request()->segment(3) && ($child->hasActiveChildren || $child->slug == request()->segment(3)) ? 'open' : ''}}">
                                            <a href="{{adminUrl([$child->slug])}}"
                                               {{request()->segment(3) == $child->slug ? 'class=active' : ''}} id="{{$child->slug}}"
                                               title="{{@$child->globalName->name ?: __('e.missing_name')}}">
                                                <span>
                                                    @if(!$child->hasChildren)
                                                        {{@$child->globalName->name ?: __('e.missing_name')}}
                                                    @else
                                                        {{@$child->globalName->name_category ?: __('e.missing_name')}}
                                                    @endif
                                                </span>
                                            </a>
                                            @if($child->hasChildren)
                                                <div class="hidden-sidebar-items {{!request()->segment(3) ? 'hidden' : (!$child->hasActiveChildren && $child->slug != request()->segment(3) ? 'hidden' : '')}}">
                                                    <div class="sidebar-item">
                                                        <a href="{{adminUrl([$child->slug])}}"
                                                           {{request()->segment(3) == $child->slug ? 'class=active' : ''}} id="{{$child->slug}}-child"
                                                           title="{{@$child->globalName->name ?: __('e.missing_name')}}">
                                                            <span> {{@$child->globalName->name ?: __('e.missing_name')}}</span>
                                                        </a>
                                                    </div>

                                                    @foreach($child->children as $child2)
                                                        <div class="sidebar-item">
                                                            <a href="{{adminUrl([$child2->slug])}}"
                                                               {{request()->segment(3) == $child2->slug ? 'class=active' : ''}} id="{{$child2->slug}}"
                                                               title="{{@$child2->globalName->name ?: __('e.missing_name')}}">
                                                                <span> {{@$child2->globalName->name ?: __('e.missing_name')}} </span>
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@stop
