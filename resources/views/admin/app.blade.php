<!DOCTYPE html>
<html lang="{{LANG}}">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=1140, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{asset('favicon.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('favicon.png')}}">
    <title>{{getAdminTabName(@$currComponent) ?: env('APP_NAME')}}</title>
    <link rel="stylesheet" href="{{asset('admin-assets/css/main.css?d=') . time()}}">
</head>
<body class="preload">

<div class="content">
    @yield('header')

    <div class="wrap {{auth()->guard('admin')->check() && !is_null(request()->cookie('sidebar')) ? 'close-sidebar' : (!auth()->guard('admin')->check() ? 'no-sidebar' : '')}} {{isset($errorPage) && $errorPage ? 'error-page' : ''}}">
        @yield('container')
    </div>

    @yield('footer')
</div>

@yield('sidebar')

<script src="{{asset('admin-assets/js/ckeditor/ckeditor.js?d=') . time()}}"></script>
<script src="{{asset('admin-assets/js/main.js?d=') . time()}}"></script>

</body>
</html>