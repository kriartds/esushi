@if(isset($filterParams))
    <div class="form-filter-translation">
        <div class="right-col">
            <button type="button" class="button gray filter-btn right">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12">
                    <g>
                        <g>
                            <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50"
                                  stroke-width="2"
                                  d="M1 1h14"></path>
                        </g>
                        <g>
                            <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50"
                                  stroke-width="2"
                                  d="M4 6h8"></path>
                        </g>
                        <g>
                            <path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="50"
                                  stroke-width="2"
                                  d="M6 11h4"></path>
                        </g>
                    </g>
                </svg>
                Filters
                <span class="count">{{@$count}}</span>
            </button>
        </div>
        <div class="filter-translation">
            <div class="filter form-block">
                <form method="post" action="{{adminUrl([$currComponent->slug, 'filter'])}}" class="filter-form"
                      id="filter-form">
                    <div class="formContainer">
                        <div class="backgroundGray">
                            <div class="field-row">
                                <div class="label-wrap">
                                    <label for="title">{{__("e.title_table")}}</label>
                                </div>
                                <div class="field-wrap">
                                    <input name="title" id="title"
                                           value="{{ !empty($filterParams) && array_key_exists('title', $filterParams) ? $filterParams['title'] : '' }}">
                                </div>
                            </div>
                        </div>
                        <div class="field-btn">
                            <button class="btn btn-inline half submit-form-btn mt" data-form-id="filter-form"
                                    data-form-event="submit-form">{{__('e.filter')}}
                            </button>
                            <button class="btn btn-inline borderButton half submit-form-btn mt"
                                    data-form-id="filter-form"
                                    data-form-event="refresh-form">{{__('e.reset')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endif
