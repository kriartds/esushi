<div class="site-translation-list">
@if(!$items->isEmpty())
    <div class="tabs-block">
        <div class="tabs-body">
            @foreach($items as $key => $item)
                @if(!empty($item) && $key == 'front')
                    <div class="tab-target active" id="{{str_slug($key)}}">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="id left">
                                    <p>{{__("e.id_table")}}</p>
                                </th>
                                <th class="left"> {{__('e.title_table')}}</th>
                                <th></th>
                                @if($permissions->edit)
                                    <th>{{__('e.edit_table')}}</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($item as $value)
                                <tr>
                                    <td class="left small-id">{{$value->id}}</td>
                                    <td class=" left">{{$value->children->isEmpty() && is_string($value->value) ? subStrText(@$value->value, 200) : '-'}}</td>
                                    <td class="empty-td"></td>
                                    @if($value->children->isEmpty())
                                        @if($permissions->edit)
                                            <td class="td-link small">
                                                @foreach($langList as $lang)
                                                    <a href="{{url(LANG, ['admin', $currComponent->slug, 'edit', $key, $lang->id]) . "?item={$value->id}"}}">{{ucfirst($lang->slug)}}</a>
                                                @endforeach
                                            </td>
                                        @endif
                                    @else
                                        <td>-</td>
                                    @endif
                                </tr>
                                @if($value->children->isNotEmpty())
                                    @foreach($value->children as $child)
                                        @if(!is_array($child))
                                            <tr id="{{$child->id}}">
                                                <td><span>&nbsp;&nbsp;–&nbsp;</span>{{$child->id}}</td>
                                                <td class="medium">{{$child->children->isEmpty() ? subStrText(@$child->value, 200) : ''}}</td>
                                                @if($permissions->edit)
                                                    <td class="td-link">
                                                        @foreach($langList as $lang)
                                                            <a href="{{adminUrl([$currComponent->slug, 'edit', $key, $lang->id]) . "?parent={$value->id}&item={$child->id}"}}">{{ucfirst($lang->slug)}}</a>
                                                        @endforeach
                                                    </td>
                                                @endif
                                            </tr>
                                        @else
                                            @foreach($child as $val)
                                                @if(!is_array($val))
                                                    <tr id="{{$val->id}}">
                                                        <td><span>&nbsp;&nbsp;–&nbsp;</span>{{$val->id}}</td>
                                                        <td class="medium">{{$val->children->isEmpty() ? subStrText(@$val->value, 200) : ''}}</td>
                                                        @if($permissions->edit)
                                                            <td class="td-link">
                                                                @foreach($langList as $lang)
                                                                    <a href="{{adminUrl([$currComponent->slug, 'edit', $key, $lang->id]) . "?parent={$value->id}&item={$val->id}"}}">{{ucfirst($lang->slug)}}</a>
                                                                @endforeach
                                                            </td>
                                                        @endif
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@else
    <div class="empty-list">{{__('e.list_is_empty')}}</div>
@endif
</div>