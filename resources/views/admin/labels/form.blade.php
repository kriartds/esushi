
<form method="POST"
      action="{{adminUrl([$currComponent->slug, 'save', @$item['id'], @$langId ]) . "?item={$item['child']}" . (@$item['parent'] ? "&parent={$item['parent']}" : '')}}"
      id="edit-form">
    <div class="background">
        <div class="mediumBlock">
            <div class="language">
            <div class="field-row">
                <div class="label-wrap">
                    <label for="lang">{{__('e.lang')}}*</label>
                </div>
                <div class="field-wrap">
                    @if(!$langList->isEmpty())
                        <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                            @foreach($langList as $lang)
                                <option value="{{$lang->id}}" {{ (is_null(@$langId) && $lang->id == LANG_ID ? 'selected' : $lang->id == @$langId ) ? 'selected' : ''}}>
                                    {{$lang->name ?: ''}}
                                </option>
                            @endforeach
                        </select>
                    @endif
                </div>
            </div>
            </div>

            <div class="field-row">
                <div class="label-wrap">
                    <label for="title">{{__('e.title_table')}}* </label>
                </div>
                <div class="field-wrap">
                    <textarea name="title" id="title">{{@$item['value']}}</textarea>
                    <span class="warning-msg">({{__("e.label_info_msg")}})</span>
                </div>
            </div>
        </div>
    </div>
    <div class="m-left padding">
        <button class="btn medium submit-form-btn" data-form-id="edit-form">{{__('e.save_it')}}</button>
    </div>
</form>

