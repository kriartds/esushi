@extends('admin.app')

@include('admin.header')

@include('admin.sidebar')

@section('container')

    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list'], 'item' => 'labels'])

        <div class="form-block">
            @include("{$path}.form", ['item' => @$label])
        </div>
    </div>

@stop

@include('admin.footer')

