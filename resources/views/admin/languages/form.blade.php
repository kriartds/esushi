<form method="POST" action="{{adminUrl([$currComponent->slug, 'save', @$item->id, @$langId ])}}"
      id="{{!is_null($item) ? 'edit' : 'create'}}-form" enctype="multipart/form-data">
    <div class="background">
        <div class="top-container-small">
            <div class="field-row">
                <div class="label-wrap">
                    <label for="name">{{__('e.title_table')}}*</label>
                </div>
                <div class="field-wrap">
                    <input name="name" id="name" value="{{@$item->name}}">
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="slug">{{__('e.slug_table')}}*</label>
                </div>
                <div class="field-wrap">
                    <input name="slug" id="slug" value="{{@$item->slug}}">
                </div>
            </div>
            <div class="field-row">
                @include('admin.templates.uploadFile', [
                    'item' => @$item,
                    'options' => [
                        'data-component-id' => $currComponent->id,
                        'data-types' => 'image'
                    ]
                ])
            </div>
        </div>
    </div>
    <div class="m-left padding">
    <button class="btn medium submit-form-btn"
            data-form-id="{{!is_null($item) ? 'edit' : 'create'}}-form">{{__('e.save_it')}}</button>
    </div>
</form>

