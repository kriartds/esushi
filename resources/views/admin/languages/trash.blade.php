@extends('admin.app')

@include('admin.sidebar')

@include('admin.header')

@section('container')
    <div class="container">
        {!! helpers()->getAdminBreadcrumbs($currComponent, new \App\Models\Languages(),@request()->get('parent')) !!}
        @include('admin.templates.pageTopButtons', ['action' => ['list', 'create', 'trash'], 'item' => null, 'currentPage' => 'trash'])

        <div class="table-block">
            @if(!$items->isEmpty())
                <table class="table">
                    <thead>
                    <tr>
                        <th>{{__("e.id_table")}}</th>
                        <th class="left">{{__('e.title_table')}}</th>
                        @if($permissions->delete)
                            <th class="checkbox-all" >{{__('e.delete_table')}}</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr id="{{$item->id}}">
                            <td class="small">{{$item->id}}</td>
                            <td class="left">{{$item->name ?: ''}}</td>
                            @if($permissions->delete)
                                <td class="checkbox-items">
                                    <input autocomplete="off" type="checkbox" class="checkbox-item" id="{{$item->id}}"
                                           name="destroy_items[{{$item->id}}]"
                                           value="{{$item->id}}">
                                    <label for="{{$item->id}}">
                                    </label>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="empty-list">{{__('e.list_is_empty')}}</div>
            @endif
        </div>
    </div>
@stop

@include('admin.footer')