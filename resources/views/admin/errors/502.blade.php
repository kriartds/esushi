@extends('admin.app', ['errorPage' => true])
@section('container')
    <div class="container">
        <div>
            <div class="logo">
                <img class="img-responsive" src="{{asset('admin-assets/img/logo.svg')}}" alt="logo"/>
            </div>
            <div class="header">502</div>
            <div class="title">Bad Gateway!</div>
            <a class="go-back" href="{{url(LANG, [(request()->segment(2) == 'admin' ? 'admin' : '')])}}">Go back!</a>
        </div>
    </div>
@stop
