@extends('admin.app', ['errorPage' => true])
@section('container')
    <div class="container">
        <div>
            <div class="logo">
                <img class="img-responsive" src="{{asset('admin-assets/img/logo.svg')}}" alt="logo"/>
            </div>
            <div class="header">405</div>
            <div class="title">Method Not Allowed!</div>
            <a class="go-back" href="{{url(LANG, [(request()->segment(2) == 'admin' ? 'admin' : '')])}}">Go back!</a>
        </div>
    </div>
@stop