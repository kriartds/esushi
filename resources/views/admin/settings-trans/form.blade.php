<div class="translable-general-settings-list">
    <form method="POST"
          action="{{adminUrl([$currComponent->slug, 'save']) . (!is_null(request()->getQueryString()) ? '?' . request()->getQueryString() : '')}}"
          id="create-edit-form"
          enctype="multipart/form-data">
        <div class="background mb">
            <div class="language">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="lang">{{__('e.lang')}}*</label>
                    </div>
                    <div class="field-wrap">
                        @if(!$langList->isEmpty())
                            <select autocomplete="off" name="lang" id="lang" class="select2 no-search">
                                @foreach($langList as $lang)
                                    <option value="{{$lang->id}}" {{!is_null(request()->get('langId')) ? ($lang->id == request()->get('langId') ? 'selected' : '') : ($lang->id == LANG_ID ? 'selected' : '')}}>
                                        {{$lang->name ?: ''}}
                                    </option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>
            </div>
            <div class="medium-container">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="site_title">{{__("e.site_title")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="site_title" id="site_title" value="{{@$items['site_title']->itemByLang->value}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="address">{{__("e.company_address")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="address" id="address" value="{{ @$items['address']->itemByLang->value }}">
                    </div>
                </div>
                <div class="field-row ">
                    <div class="label-wrap">
                        <label for="short_description">{{__("e.short_description")}}</label>
                    </div>
                    <div class="field-wrap">
            <textarea name="short_description"
                      id="short_description">{{@$items['short_description']->itemByLang->value}}</textarea>
                    </div>
                </div>
            </div>
            <div class="field-row">
                <div class="label-wrap">
                    <label for="payment_delivery_desc">{{__("e.payment_delivery")}}</label>
                </div>
                <div class="field-wrap">
            <textarea name="payment_delivery_desc" data-type="ckeditor"
                      id="payment_delivery_desc">{{@$items['payment_delivery_desc']->itemByLang->value}}</textarea>
                </div>
            </div>
            <div class="field-row">
                @include('admin.templates.uploadFile', [
                    'item' => @$item,
                    'options' => [
                        'data-component-id' => $currComponent->id,
                        'data-types' => 'image'
                    ]
                ])
            </div>
            <div class="mediumBlock">
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="meta_title">{{__("e.meta_title")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="meta_title" id="meta_title" value="{{@$items['meta_title']->itemByLang->value}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="meta_keywords">{{__("e.meta_keywords")}}</label>
                    </div>
                    <div class="field-wrap">
                        <input name="meta_keywords" id="meta_keywords"
                               value="{{@$items['meta_keywords']->itemByLang->value}}">
                    </div>
                </div>
                <div class="field-row">
                    <div class="label-wrap">
                        <label for="meta_description">{{__("e.meta_description")}}</label>
                    </div>
                    <div class="field-wrap">
            <textarea name="meta_description"
                      id="meta_description">{{@$items['meta_description']->itemByLang->value}}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-left padding">
            <button class="btn medium submit-form-btn" data-form-id="create-edit-form">{{__('e.save_it')}}</button>
        </div>
    </form>
</div>