@section('footer')
    <footer class="footer">
        <div class="wrap">
            <div class="footer__inner">
                <div class="footer__col">
                    <a class="logo" href="{{customUrl()}}">
                        <img src="{{asset('assets/img/svg-icons/logo-white.svg')}}" alt="">
                    </a>
                    <p class="description">{{@getSettingsField('short_description', $allSettings, true)}} </p>

                    <div class="social-links">

                        @if(@getSettingsField('facebook_link', $allSettings))
                            <a href="{{@getSettingsField('facebook_link', $allSettings)}}" target="_blank"><img
                                        src="{{asset('assets/img/svg-icons/facebook.svg')}}" alt="facebook"></a>
                        @endif

                        @if(@getSettingsField('instagram_link', $allSettings))
                            <a href="{{@getSettingsField('instagram_link', $allSettings)}}" target="_blank"><img
                                        src="{{asset('assets/img/svg-icons/instagram.svg')}}"
                                        alt="Instagram"></a>
                        @endif
                        @if(@getSettingsField('twitter_link', $allSettings))
                            <a href="{{@getSettingsField('twitter_link', $allSettings)}}" target="_blank"><img
                                        src="{{asset('assets/img/svg-icons/twitter.svg')}}" alt="Twitter"></a>
                        @endif
                    </div>
                </div>
                <div class="footer__col">
                    <h6>{{__('front.fmenu_info')}}</h6>
                    @if($footerMenu = getMenuRecursive(2))
                        @if($footerMenu->isNotEmpty())
                            @foreach($footerMenu as $menu)
                                @if(!empty($menu))
                                    <a href="{{$menu->currentUrl}}"
                                       class="{{$menu->currentClasses}}">{{$menu->currentName}}</a>
                                @endif
                            @endforeach
                        @endif
                    @endif
                </div>
                <div class="footer__col">
                    @if(isset($ourRestaurants))
                        <h6>{{__('front.fmenu_restaurants')}}</h6>
                        @foreach($ourRestaurants as $restaurant)
                            <a href="{{customUrl(['restaurant',$restaurant->slug])}}"><b>{{@$restaurant->globalName->name}}
                                :</b> {{@$restaurant->globalName->address}}</a>
                        @endforeach
                    @endif
                </div>
                <div class="footer__col">
                    <h6>{{__('front.fmenu_contacts')}}</h6>
                    <a href="tel:{{clearPhone(@getSettingsField('delivery_operator', $allSettings))}}">
                        <b>{{__('front.delivery_operator')}}</b> {{@getSettingsField('delivery_operator', $allSettings)}}
                    </a>
                    <a href="tel:{{clearPhone(@getSettingsField('restaurant_reservations', $allSettings))}}">
                        <b>{{__('front.reservation')}}</b> {{@getSettingsField('restaurant_reservations', $allSettings)}}
                    </a>
                    <a href="mailto:{{@getSettingsField('email', $allSettings)}}">
                        <b>{{__('front.email')}}</b> {{@getSettingsField('email', $allSettings)}}
                    </a>
                </div>
            </div>
        </div>
        <div class="copyright-bar">
            <div class="wrap">
                <div class="copyright-bar__inner">
                    <div class="copyright">© 2020 - <script type="text/javascript">document.write(new Date().getFullYear());</script> Esushi | All right reserved</div>
                    <div class="logo-cherry">
                        <a href="https://cherrydigitalagency.com/en" target="_blank">
                            <svg>
                                <use xlink:href="{{asset('assets/img/sprite.svg#logo-cherry')}}"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="payments-methods">
                        <img src="{{asset('assets/img/svg-icons/mastercard.svg')}}" alt="">
                        <img src="{{asset('assets/img/svg-icons/visa.svg')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="cookiesContainer">
            <div class="closeCookie">
                <img src="{{asset('assets/img/cookies.png')}}" alt="">
            </div>
            <div class="text">
                Folosim cookie-uri pentru a vă asigura cea mai bună experiență pe website-ul nostru. Prin continuarea
                navigării, sunteți de acord cu acestea.
                <a href="" class="detailsLink">Detalii</a>
            </div>
        </div>
    </footer>
    <!--modal-success-->
{{--    <div class="arcticmodal-container">--}}
{{--        <div class="arcticmodal-container_table">--}}
{{--            <div class="arcticmodal-container_td">--}}
{{--                <div class="modal modal-success" id="modal-success">--}}
{{--                    <div class="modal-close arcticmodal-close">--}}
{{--                        <svg>--}}
{{--                            <use xlink:href="{{asset('assets/img/sprite.svg#close-modal')}}"></use>--}}
{{--                        </svg>--}}
{{--                    </div>--}}
{{--                    <div class="title">Data successful send, thank you!</div>--}}
{{--                    <div class="desc">We will contact you soon.</div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    @if(!auth()->user())
        <div class="arcticmodal-container top" style="display: none">
            <table class="arcticmodal-container_i loginModal">
                <tbody>
                <tr>
                    <td class="arcticmodal-container_i2">
                        <div class="modal modal-success" id="login">
                            <div class="modal-close arcticmodal-close">
                                <svg>
                                    <use xlink:href="{{asset('assets/img/sprite.svg#close-modal')}}"></use>
                                </svg>
                            </div>
                            <div class="padding">
                                <h1 class="h1">Logare</h1>
                                <div class="txt">logați-vă cu rețele de socializare</div>
                                <div class="socials">
                                    <a href="{{customUrl('login/google')}}" class="icon">
                                        <img src="{{asset('assets/img/google.png')}}" alt="">
                                    </a>
                                    <a href="{{customUrl('login/facebook')}}" class="icon">
                                        <img src="{{asset('assets/img/facebook.png')}}" alt="">
                                    </a>
                                </div>
                                <div class="txt">sau folosiți e-mailul și parola</div>
                                <form class="form-group loginForm" method="post" action="{{customUrl('login')}}"
                                      id="modal-login-page-form">
                                    <div class="input-wrap">
                                        <input type="text" name="login_email" placeholder="E-mail">
                                    </div>
                                    <div class="input-wrap">
                                        <input type="password" name="login_password" placeholder="Parola">
                                    </div>
                                    <div class="buttons">
                                        <button data-form-id="modal-login-page-form" class="button submit-form-btn">
                                            Loghează-te
                                        </button>
                                        <button href="#modal-register" class="button registr getModal">Înregistrare</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="arcticmodal-container top" style="display: none">
            <table class="arcticmodal-container_i loginModal">
                <tbody>
                <tr>
                    <td class="arcticmodal-container_i2">
                        <div class="modal modal-success" id="modal-register">
                            <div class="modal-close arcticmodal-close">
                                <svg>
                                    <use xlink:href="{{asset('assets/img/sprite.svg#close-modal')}}"></use>
                                </svg>
                            </div>
                            <div class="padding">
                                <h1 class="h1">Înregistrare</h1>
                                <div class="txt">logați-vă cu rețele de socializare</div>
                                <div class="socials">
                                    <a href="" class="icon">
                                        <img src="{{asset('assets/img/google.png')}}" alt="">
                                    </a>
                                    <a href="" class="icon">
                                        <img src="{{asset('assets/img/facebook.png')}}" alt="">
                                    </a>
                                </div>
                                <div class="txt">sau folosiți e-mailul și parola</div>
                                <form class="form-group" method="POST" action="{{customUrl('register')}}"
                                      id="modal-register-page-form">
                                    <div class="input-wrap">
                                        <input type="text" name="register_email" placeholder="E-mail">
                                    </div>
                                    <div class="input-wrap">
                                        <input type="password" name="register_password" placeholder="Parola">
                                    </div>
                                    <div class="input-wrap">
                                        <input type="password" name="register_repeat_password"
                                               placeholder="Repetați parola">
                                    </div>
                                    <label class="label" for="accept_register_terms">
                                        <input type="checkbox" name="accept_terms" id="accept_register_terms">
                                        <div class="check"></div>
                                        <div class="textBl">
                                            <span class="text">Sunt de acord cu</span>
                                            <a href="" class="text link">Politica de confidentialitate,</a>
                                            <span class="text">și cu</span>
                                            <a href="" class="text link">Termeni si conditii</a>
                                        </div>
                                    </label>
                                    <div class="buttons">
                                        <button data-form-id="modal-register-page-form" class="button submit-form-btn">
                                            Înregistrare
                                        </button>
                                        <button href="#login" class="button registr getModal">Logare</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!--END modal-success-->
    @endif
@stop
