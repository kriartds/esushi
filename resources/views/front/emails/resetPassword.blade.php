<table style=" color: #2C2B2A;border-collapse: collapse;font-family: 'Raleway','Arial Black', sans-serif; width: 600px; margin: auto">
        <tr style="background-image: url({{asset('assets/img/header-email.svg')}}); background-repeat: no-repeat; height: 144px">
            <td style="padding: 10px; text-align: center; " colspan="2">
                <a href="{{url(LANG)}}" target="_blank">
                    {!! getLogo($allSettings) !!}
                </a>
            </td>
        </tr>
        <tr>
            <td style="background-image: url({{asset('assets/img/reset-pswrd.svg')}}); background-repeat: no-repeat; background-position: center;  padding-top: 47px; height: 150px; display: block; margin-top: 20px">

            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="2">
                <h1 style="color: #180D0D; font-size: 48px; margin: 0; font-family: Arial; font-weight: bold">{{__("front.reset_password_on", ['name' => @getSettingsField('site_title', $allSettings, true)])}}</h1>
            </td>
        </tr>
        <tr>
            <td style=" text-align: center; padding: 10px;" colspan="2">
                <p style="color:  #180D0D; font-family: Arial; font-weight: normal; font-size: 16px; line-height: 24px">{{__("front.reset_password_email_msg", ['name' => @$item->name, 'url' => url('/')])}}</p>
                <a href="{{urldecode(@$resetPassUrl)}}"
                   style="color: #ffffff; font-size: 14px; font-family: Arial; padding: 16px 50px; background-color: #FF5E47; text-decoration: none; border-radius: 10px; margin-top: 10px; display: inline-block;">{{__("front.link_reset_pass")}}</a>
            </td>
        </tr>
    </table>

    <table bgcolor="#F9F9F8" width="600" align="center" style="margin: 48px auto 0 auto;" cellpadding="0"
           cellspacing="0">
        <tr>
            <td style="padding: 20px;">
                <table align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                    <tr>
                        @if(@getSettingsField('instagram_link', $allSettings))
                            <td style="line-height: 10px; font-size: 1px; background: #ffffff; padding: 6px 10px; border: 2px solid #EEEEEE; border-radius: 10px">
                                <a href="{{@getSettingsField('instagram_link', $allSettings)}}">
                                    <img src="{{asset('assets/img/socials/instagram.png')}}" alt="icon">
                                </a>
                            </td>
                            <td width="10"></td>
                        @endif

                        @if(@getSettingsField('facebook_link', $allSettings))
                            <td style="line-height: 10px; font-size: 1px; background: #ffffff; padding: 6px 5px; border: 2px solid #EEEEEE; border-radius: 10px">
                                <a href="{{@getSettingsField('facebook_link', $allSettings)}}">
                                    <img src="{{asset('assets/img/svg-icons/facebook.png')}}" alt="icon">
                                </a>
                            </td>
                            <td width="10"></td>
                        @endif

                        @if(@getSettingsField('twitter_link', $allSettings))
                            <td style="line-height: 10px; font-size: 1px; background: #ffffff; padding: 6px 5px; border: 2px solid #EEEEEE; border-radius: 10px">
                                <a href="{{@getSettingsField('twitter_link', $allSettings)}}">
                                    <img src="{{asset('assets/img/svg-icons/twitter.png')}}" alt="icon">
                                </a>
                            </td>
                            <td width="10"></td>
                        @endif

                        @if(@getSettingsField('telegram_link', $allSettings))
                            <td style="line-height: 10px; font-size: 1px; background: #ffffff; padding: 6px 5px; border: 2px solid #EEEEEE; border-radius: 10px">
                                <a href="{{@getSettingsField('telegram_link', $allSettings)}}">
                                    <img src="{{asset('assets/img/svg-icons/telegram.png')}}" alt="icon">
                                </a>
                            </td>
                            <td width="10"></td>
                        @endif
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style="font:10px Arial; color:#242424; line-height: 20px;">
                <a href="mailto:{{@getSettingsField('email', $allSettings)}}" target="_blank"
                   style="line-height: 20px; font: 400 10px Arial, Helvetica, sans-serifl; color:  #2e3135; text-decoration: none; display: inline-block; padding-bottom: 5px;">{{@getSettingsField('email', $allSettings)}}</a>
                <br/>

                <a href="#" target="_blank"
                   style="line-height: 20px; font: 400 10px Arial, Helvetica, sans-serif; color:  #2e3135; text-decoration: none; margin-bottom: 5px">
                    {{@getSettingsField('phone', $allSettings)}}</a>
                <br/>

                {{@getSettingsField('site_title', $allSettings, true)}}
                , {{@getSettingsField('address', $allSettings, true)}}
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" style="margin:0 auto; padding: 15px 30px;" cellpadding="0" cellspacing="0">
                    <tr>
                        @if($footerMenu = @getMenuRecursive(3))
                            @if(!empty($footerMenu))
                                @foreach($footerMenu as $menu)
                                    <td align="center">
                                        <a href="{{$menu->currentUrl}}"
                                           style="color: #B8B8B8; font:400 10px Arial; color:#b8b8b8;">{{$menu->currentName}}</a>
                                    </td>
                                    <td width="30"></td>
                                @endforeach
                            @endif
                        @endif
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style="margin:0 auto; padding: 0 30px 15px 30px; color:#b8b8b8; font: 400 10px Arial;">
                © 2020 {{@getSettingsField('site_title', $allSettings, true)}}
            </td>
        </tr>
    </table>
    <br>


