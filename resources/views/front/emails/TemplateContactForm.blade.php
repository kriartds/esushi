<table style="color: #2C2B2A; border-collapse: collapse; font-family: 'Raleway','Arial Black', sans-serif; width: 600px; margin: auto">
    <tr style="background-image: url({{asset('assets/img/header-email.svg')}}); background-repeat: no-repeat; height: 144px">
        <td style="padding: 20px; text-align: center;" colspan="2">
            <a href="{{url(LANG)}}" target="_blank">
                {!! getLogo($allSettings) !!}
            </a>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;" colspan="2">
            <h1 style="color: #180D0D; font-size: 48px; font-family: 'Arial'; padding-top: 20px; ">{{__('front.contact_form_subject')}}

            </h1>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p style="color: #180D0D; font-size: 16px; font-family: 'Arial'; text-align: left;">Name
                : {{$userInfo['name']}}</p>
            <p style="color: #180D0D; font-size: 16px; font-family: 'Arial'; text-align: left;">Phone
                : {{$userInfo['phone']}}</p>
            <p style="color: #180D0D; font-size: 16px; font-family: 'Arial'; text-align: left;">Email
                : {{$userInfo['email']}}</p>
            <p style="color: #180D0D; font-size: 16px; font-family: 'Arial'; text-align: left;">Tema
                : {{$userInfo['tema']}}</p>
            <p style="color: #180D0D; font-size: 16px; font-family: 'Arial'; text-align: left;">Message
                : {{$userInfo['message']}}</p>
        </td>
    </tr>
    <br>
</table>