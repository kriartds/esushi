@if(isset($item))
    <table style="border: 1px solid #AFB5B9; color: #2C2B2A;border-collapse: collapse;font-family: 'Raleway','Arial Black', sans-serif;width: 600px;">
        <tr style="background-color: #E1E6E9">
            <td style="padding: 10px; text-align: center" colspan="2">
                <a href="{{url(LANG)}}" target="_blank">
                    {!! getLogo($allSettings) !!}
                </a>
            </td>
        </tr>
        <tr style="background-color: #E1E6E9">
            <td style="padding: 10px;text-align: center;text-transform: uppercase" colspan="2">
                <h1 style="color: #2C2B2A;">{{__("front.success_subscribed", ['name' => @getSettingsField('site_title', $allSettings, true)])}}</h1>
            </td>
        </tr>
        <tr>
            <td style="padding: 10px;" colspan="2">
                <p style="color: #2C2B2A;">{{__("front.unsubscribe_email_msg")}}</p>
                <a href="{{urldecode(customUrl("unsubscribe/{$item->unsubscribe_token}"))}}">{{__("front.unsubscribe")}}</a>
            </td>
        </tr>
    </table>

    <br>

    @if($footerMenu = @getMenuRecursive(4))
        @if(!empty($footerMenu))
            @foreach($footerMenu as $menu)
                <a style="padding:5px 0;text-transform: uppercase; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #0A67A6;text-decoration: none" target="_blank" href="{{$menu->currentUrl}}">{{$menu->currentName}}</a>
            @endforeach
        @endif
    @endif

    <br>

    <a style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none"
       href="tel:{{clearPhone(@getSettingsField('md_phone', $allSettings))}}"
       target="_blank"><span>{{@getSettingsField('md_phone', $allSettings)}}</span></a>
    <a style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none"
       href="mailto:{{@getSettingsField('email', $allSettings)}}"
       target="_blank"><span>{{@getSettingsField('email', $allSettings)}}</span></a>
    <span style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none">{{@getSettingsField('moldova_address', $allSettings, true)}}</span>

    <ul class="footer-socials-links" style="list-style-type: none;padding: 0">
        @if(@getSettingsField('facebook_link', $allSettings))
            <li style="float: left;margin-right: 10px;" class="facebook"><a
                        href="{{@getSettingsField('facebook_link', $allSettings)}}" target="_blank">
                    <img src="{{asset('assets/img/socials/facebook.png')}}" alt="facebook"></a></li>
        @endif
        @if(@getSettingsField('instagram_link', $allSettings))
            <li style="float: left;margin-right: 10px;" class="instagram"><a
                        href="{{@getSettingsField('instagram_link', $allSettings)}}" target="_blank"><img
                            src="{{asset('assets/img/socials/instagram.png')}}" alt="instagram"></a></li>
        @endif
        @if(@getSettingsField('twitter_link', $allSettings))
            <li style="float: left;margin-right: 10px;" class="twitter"><a
                        href="{{@getSettingsField('twitter_link', $allSettings)}}" target="_blank">
                    <img src="{{asset('assets/img/socials/twitter.png')}}" alt="twitter"></a></li>
        @endif
    </ul>
@endif

