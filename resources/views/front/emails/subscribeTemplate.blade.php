@if(isset($item))
    <div style="width: 600px;">
        {!! @$item['item']->description !!}
    </div>
    <a href="{{urldecode(customUrl("unsubscribe/{$item['subscriber']->unsubscribe_token}"))}}">{{__("front.unsubscribe_clean_msg")}}</a>


    <a style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none"
       href="tel:{{clearPhone(@getSettingsField('md_phone', $allSettings))}}"
       target="_blank"><span>{{@getSettingsField('md_phone', $allSettings)}}</span></a>
    <a style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none"
       href="mailto:{{@getSettingsField('email', $allSettings)}}"
       target="_blank"><span>{{@getSettingsField('email', $allSettings)}}</span></a>
    <span style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none">{{@getSettingsField('moldova_address', $allSettings, true)}}</span>

    <ul class="footer-socials-links" style="list-style-type: none;padding: 0">
        @if(@getSettingsField('facebook_link', $allSettings))
            <li style="float: left;margin-right: 10px;" class="facebook"><a
                        href="{{@getSettingsField('facebook_link', $allSettings)}}" target="_blank">
                    <img src="{{asset('assets/img/socials/facebook.png')}}" alt="facebook"></a></li>
        @endif
        @if(@getSettingsField('instagram_link', $allSettings))
            <li style="float: left;margin-right: 10px;" class="instagram"><a
                        href="{{@getSettingsField('instagram_link', $allSettings)}}" target="_blank"><img
                            src="{{asset('assets/img/socials/instagram.png')}}" alt="instagram"></a></li>
        @endif
        @if(@getSettingsField('twitter_link', $allSettings))
            <li style="float: left;margin-right: 10px;" class="twitter"><a
                        href="{{@getSettingsField('twitter_link', $allSettings)}}" target="_blank">
                    <img src="{{asset('assets/img/socials/twitter.png')}}" alt="twitter"></a></li>
        @endif
    </ul>
@endif

