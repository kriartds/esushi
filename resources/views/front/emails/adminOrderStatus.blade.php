<table style="border: 1px solid #AFB5B9; color: #2C2B2A;border-collapse: collapse;font-family: 'Raleway','Arial Black', sans-serif;width: 640px;">
    <tr style="background-color: #E1E6E9">
        <td style="padding: 10px; text-align: center" colspan="3">
            <a href="{{url(LANG)}}" target="_blank">
                {!! getLogo($allSettings) !!}
            </a>
        </td>
    </tr>
    <tr style="background-color: #E1E6E9">
        <td style="padding: 10px;text-align: center;" colspan="3">
            <h1 style="color: #2C2B2A;">{{__("front.change_order_status")}} <a href="{{url('/')}}"
                                                                               target="_blank">{{@getSettingsField('site_title', $allSettings, true)}}</a>
            </h1>
        </td>
    </tr>
    <tr>
        <td style="padding: 10px;" colspan="3">
            <p style="color: #2C2B2A;">{{__("front.your_order")}},
                <b>#{{@$item->order_id}}</b>, {{__("front.has_status", ['name' => @$item->status])}}</p>
        </td>
    </tr>
    <tr style="background-color: #E1E6E9">
        <td style="padding: 10px;text-align: left;" colspan="3">
            <h3 style="margin: 0;">{{__("front.client")}}</h3>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td style="border: 1px solid #AFB5B9;padding: 7px 5px;text-align: left;" colspan="3">{{__("front.order_id")}}:
            <b>#{{@$item->order_id}}</b></td>
    </tr>
    <tr style="text-align: left;">
        <td style="border: 1px solid #AFB5B9;padding: 7px 5px;text-align: left;">{{__("front.name")}}
            : {{@$item->details->name}}</td>
        <td style="border: 1px solid #AFB5B9;width: 50%;padding: 7px 5px;text-align: left;" colspan="2">
            Email: {{@$item->email}}</td>
    </tr>
    <tr>
        <td style="border: 1px solid #AFB5B9;width: 50%; padding: 7px 5px;text-align: left;">
            Phone: {{@$item->phone}}</td>
        <td style="border: 1px solid #AFB5B9;padding: 7px 5px;text-align: left;"
            colspan="2">{{__("front.payment_method")}}: {{strtoupper(@$item->payment_method)}}</td>
    </tr>
    <tr style="background-color: #E1E6E9">
        <td style="padding: 10px;text-align: left;" colspan="3">
            <h3 style="margin: 0;">{{@$useShipping ? __("front.delivery_information") : __("front.pickup_information")}}</h3>
        </td>
    </tr>
    @if(@$useShipping)
        <tr style="text-align: left;">
            <td style="border: 1px solid #AFB5B9;padding: 7px 5px;text-align: left;">
                {{__("front.country")}}: {{@$item->country->name}}</td>
            <td style="border: 1px solid #AFB5B9;width: 50%;padding: 7px 5px;text-align: left;" colspan="2">
                {{__("front.region")}}: {{@$item->region->name}}</td>
        </tr>
        <tr>
            <td style="border: 1px solid #AFB5B9;width: 50%; padding: 7px 5px;text-align: left;">
                {{__("front.city")}}: {{@$item->city}}</td>
            <td style="border: 1px solid #AFB5B9;padding: 7px 5px;text-align: left;"
                colspan="2">{{__("front.zip_code")}}: {{@$item->details->zip_code}}</td>
        </tr>
        <tr>
            <td style="border: 1px solid #AFB5B9;width: 50%; padding: 7px 5px;text-align: left;">
                {{__("front.address")}}: {{@$item->details->address}}</td>
            <td style="border: 1px solid #AFB5B9;padding: 7px 5px;text-align: left;"
                colspan="2">{{__("front.shipping_method")}}: {{@$item->shipping_details->name}}</td>
        </tr>
    @elseif(@$usePickup)
        <tr style="text-align: left;">
            <td style="border: 1px solid #AFB5B9;padding: 7px 5px;text-align: left;">
                Country: {{@$item->pickup_details->country}}</td>
            <td style="border: 1px solid #AFB5B9;width: 50%;padding: 7px 5px;text-align: left;" colspan="2">
                Region: {{@$item->pickup_details->region}}</td>
        </tr>
        <tr>
            <td style="border: 1px solid #AFB5B9;width: 50%; padding: 7px 5px;text-align: left;">
                Address: {{@$item->pickupStore->globalName->address ?: @$item->pickup_details->address}}</td>
            <td style="border: 1px solid #AFB5B9;padding: 7px 5px;text-align: left;" colspan="2">
                Store: {{@$item->pickupStore->globalName->name ?: @$item->pickup_details->name}}</td>
        </tr>
    @endif
    @if(@$cartProducts && $cartProducts->isNotEmpty())
        <tr style="background-color: #E1E6E9">
            <td style="padding: 10px;text-align: left;" colspan="3">
                <h3 style="margin: 0;">Products ({{@$cartProducts->count()}})</h3>
            </td>
        </tr>
        @foreach($cartProducts as $cartProduct)
            @php
                $product = @$cartProduct->product;
            @endphp
            <tr>
                <td style="width:70%;border: 1px solid #AFB5B9;padding: 7px 5px;text-align: left;" colspan="2">
                    <div style="display: inline-block;float:left;">
                        <img style="height: 60px;margin-right: 10px;" src="{{$cartProduct->customFile}}"
                             alt="{{@$product->slug ?: @$cartProduct->info->slug}}"
                             title="{{@$product->globalName->name ?: @$cartProduct->info->name}}">
                    </div>
                    <div style="display: inline-block;vertical-align: middle;">
                        <a style="margin: 0;display: block;"
                           href="{{customUrl(['products', (@$product->slug ?: @$cartProduct->info->slug), (@$cartProduct->products_variation_item_id ? $cartProduct->products_variation_item_id : "")])}}"
                           style="color: #B5111E"
                           target="_blank">{{@$product->globalName->name ?: @$cartProduct->info->name}}</a>
                        @if(!empty($cartProduct->attributes))
                            <div style="margin-top: 10px; font-size: 14px;">
                                @foreach($cartProduct->attributes as $attribute)
                                    @php
                                        $parent = $attribute->parent;
                                        $child = $attribute->child;
                                        $parentName = @collect($parent->info)[LANG] ? @$parent->info->{LANG}->name : @collect($parent->info)->first()->name;
                                        $childName = @collect($child->info)[LANG] ? @$child->info->{LANG}->name : @collect($child->info)->first()->name;
                                    @endphp
                                    @if($parent && $child)
                                        <span style="font-weight: bold;">{{$parentName}}: </span>
                                        <span>{{$childName}}</span>
                                    @endif
                                @endforeach
                            </div>
                        @endif
                    </div>
                </td>
                <td style="width: 30%;padding: 7px 5px;text-align: right;">
                    <div style="font-weight: bold;font-size: 18px;margin-bottom: 10px;">{!! @$cartProduct->amount && @$cartProduct->quantity ? formatPrice($cartProduct->amount * $cartProduct->quantity, @$item->order_currency) : '-' !!}</div>
                    <div>
                        <span style="margin-right: 10px;">x{{@$cartProduct->quantity}}</span>
                        <span>{!! @$cartProduct->amount ? formatPrice($cartProduct->amount, @$item->order_currency) : '-' !!}</span>
                    </div>
                </td>
            </tr>
            <tr style="background-color: #E1E6E9">
                <td style="padding: 10px;text-align: left;" colspan="3"></td>
            </tr>
        @endforeach
    @endif
    @if(@$item->details->comment)
        <tr style="background-color: #E1E6E9">
            <td style="padding: 5px">{{__("front.additional_description")}}</td>
            <td style="padding: 5px; font-size: 12px;">{{@$item->details->comment}}</td>
        </tr>
    @endif
    <tr style="background-color: #E1E6E9">
        <td style="padding: 5px">{{__("front.date")}}</td>
        <td style="padding: 5px;color: #B5111E; text-align: right;"
            colspan="2">{{strtotime(@$item->order_date) ? date('d-m-Y H:i', strtotime($item->order_date)) : '-'}}</td>
    </tr>
</table>
<table style="border: 1px solid #AFB5B9; color: #2C2B2A;border-collapse: collapse;font-family: 'Raleway','Arial Black', sans-serif;width: 640px;margin-top: 10px;">
    <tr style="background-color: #E1E6E9">
        <td style="padding: 10px;text-align: left;" colspan="3">
            <h3 style="margin: 0;">{{__("front.totals")}}</h3>
        </td>
    </tr>
    <tr style="background-color: #E1E6E9;">
        <td style="padding: 5px">{{__("front.subtotal")}}</td>
        <td style="padding: 5px;color: #B5111E;text-align: right;"
            colspan="2">{!! @$cartSubtotal ? formatPrice(@$cartSubtotal, @$orderCurrency->default, false) : '-' !!}</td>
    </tr>
    <tr style="background-color: #E1E6E9;">
        <td style="padding: 5px">{{__("front.discount_amount")}}</td>
        <td style="padding: 5px;color: #B5111E;text-align: right;"
            colspan="2">{!! @$item->discount_details ? ($item->discount_details->allow_free_shipping ? __("front.free_shipping_coupon") : (@$cartSubtotalWithCoupon ? formatPrice(@$cartSubtotalWithCoupon, @$orderCurrency) : '-')) : '-' !!}</td>
    </tr>
    <tr style="background-color: #E1E6E9;">
        <td style="padding: 5px">{{__("front.delivery_amount")}}</td>
        <td style="padding: 5px;color: #B5111E;text-align: right;"
            colspan="2">{!! @$shippingAmount ? formatPrice(@$shippingAmount ?: 0, @$orderCurrency) : '-' !!}</td>
    </tr>
    <tr style="background-color: #E1E6E9;">
        <td style="padding: 5px;font-size: 18px;font-weight: bold;">{{__("front.total")}}</td>
        <td style="padding: 5px;color: #B5111E;text-align: right;font-size: 18px;font-weight: bold;"
            colspan="2">{!! @$cartTotal ? formatPrice(@$cartTotal, @$orderCurrency->default, false) : '-' !!}</td>
    </tr>
</table>

<br>

@if($footerMenu = @getMenuRecursive(4))
    @if(!empty($footerMenu))
        @foreach($footerMenu as $menu)
            <a style="padding:5px 0; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #0A67A6;text-decoration: none"
               target="_blank" href="{{$menu->currentUrl}}">{{$menu->currentName}}</a>
        @endforeach
    @endif
@endif

<br>

<a style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none"
   href="tel:{{clearPhone(@getSettingsField('md_phone', $allSettings))}}"
   target="_blank"><span>{{@getSettingsField('md_phone', $allSettings)}}</span></a>
<a style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none"
   href="mailto:{{@getSettingsField('email', $allSettings)}}"
   target="_blank"><span>{{@getSettingsField('email', $allSettings)}}</span></a>
<span style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none">{{@getSettingsField('moldova_address', $allSettings, true)}}</span>

<ul class="footer-socials-links" style="list-style-type: none;padding: 0">
    @if(@getSettingsField('facebook_link', $allSettings))
        <li style="float: left;margin-right: 10px;" class="facebook"><a
                    href="{{@getSettingsField('facebook_link', $allSettings)}}" target="_blank">
                <img src="{{asset('assets/img/socials/facebook.png')}}" alt="facebook"></a></li>
    @endif
    @if(@getSettingsField('instagram_link', $allSettings))
        <li style="float: left;margin-right: 10px;" class="instagram"><a
                    href="{{@getSettingsField('instagram_link', $allSettings)}}" target="_blank"><img
                        src="{{asset('assets/img/socials/instagram.png')}}" alt="instagram"></a></li>
    @endif
    @if(@getSettingsField('twitter_link', $allSettings))
        <li style="float: left;margin-right: 10px;" class="twitter"><a
                    href="{{@getSettingsField('twitter_link', $allSettings)}}" target="_blank">
                <img src="{{asset('assets/img/socials/twitter.png')}}" alt="twitter"></a></li>
    @endif
</ul>