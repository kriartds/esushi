@if(isset($item))
{{--    NEW SUBSCRIBE--}}
    <table style=" color: #2C2B2A;border-collapse: collapse;font-family: 'Raleway','Arial Black', sans-serif;width: 600px; margin: auto">
        <tr style="background-image: url({{asset('assets/img/header-email.svg')}}); background-repeat: no-repeat; height: 144px">
            <td style="padding: 10px; text-align: center" colspan="2">
                <a href="{{url(LANG)}}" target="_blank">
                    {!! getLogo($allSettings) !!}
                </a>
            </td>
        </tr>
        <tr>
            <td style="background-image: url({{asset('assets/img/subscribe.svg')}}); background-repeat: no-repeat; background-position: center; background-size: 180px 150px; padding-top: 47px; height: 150px;  display: block; margin-top: 20px">

            </td>
        </tr>
        <tr>
            <td style=" text-align: center;" colspan="2">
                <h1 style="color: #180D0D; font-size: 48px">{{__("front.success_subscribed", ['name' => @getSettingsField('site_title', $allSettings, true)])}}</h1>
            </td>
        </tr>
        <tr>
            <td style="padding: 10px;" colspan="2">
                <p style="color: #180D0D; font-size: 16px; font-family: Arial; font-weight: normal">{{__("front.unsubscribe_email_msg")}}</p>
                <a href="{{urldecode(customUrl("unsubscribe/{$item->unsubscribe_token}"))}}" style="color: #ffffff; font-size: 14px; font-family: Arial; padding: 16px 50px; background-color: #FF5E47; text-decoration: none; text-align: center; border-radius: 10px; margin: 10px auto 0 auto; display: block; width: 240px">{{__("front.unsubscribe")}}</a>
            </td>
        </tr>
    </table>

    <br>

    @if($footerMenu = @getMenuRecursive(4))
        @if(!empty($footerMenu))
            @foreach($footerMenu as $menu)
                <a style="padding:5px 0;text-transform: uppercase; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #0A67A6;text-decoration: none" target="_blank" href="{{$menu->currentUrl}}">{{$menu->currentName}}</a>
            @endforeach
        @endif
    @endif

    <br>

    <a style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none"
       href="tel:{{clearPhone(@getSettingsField('md_phone', $allSettings))}}"
       target="_blank"><span>{{@getSettingsField('md_phone', $allSettings)}}</span></a>
    <a style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none"
       href="mailto:{{@getSettingsField('email', $allSettings)}}"
       target="_blank"><span>{{@getSettingsField('email', $allSettings)}}</span></a>
    <span style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none">{{@getSettingsField('moldova_address', $allSettings, true)}}</span>

    <ul class="footer-socials-links" style="list-style-type: none;padding: 0">
        @if(@getSettingsField('facebook_link', $allSettings))
            <li style="float: left;margin-right: 10px;" class="facebook"><a
                        href="{{@getSettingsField('facebook_link', $allSettings)}}" target="_blank">
                    <img src="{{asset('assets/img/socials/facebook.png')}}" alt="facebook"></a></li>
        @endif
        @if(@getSettingsField('instagram_link', $allSettings))
            <li style="float: left;margin-right: 10px;" class="instagram"><a
                        href="{{@getSettingsField('instagram_link', $allSettings)}}" target="_blank"><img
                            src="{{asset('assets/img/socials/instagram.png')}}" alt="instagram"></a></li>
        @endif
        @if(@getSettingsField('twitter_link', $allSettings))
            <li style="float: left;margin-right: 10px;" class="twitter"><a
                        href="{{@getSettingsField('twitter_link', $allSettings)}}" target="_blank">
                    <img src="{{asset('assets/img/socials/twitter.png')}}" alt="twitter"></a></li>
        @endif
    </ul>
@endif



