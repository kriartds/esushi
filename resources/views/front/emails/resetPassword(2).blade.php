@if(isset($item))
    <table style=" color: #2C2B2A; border-collapse: collapse;font-family: 'Raleway','Arial Black', sans-serif; width: 600px; margin: auto">
        <tr style="background-image: url({{asset('assets/img/header-email.svg')}}); background-repeat: no-repeat; height: 144px">
            <td style="padding: 10px; text-align: center; " colspan="2">
                <a href="{{url(LANG)}}" target="_blank">
                    {!! getLogo($allSettings) !!}
                </a>
            </td>
        </tr>
        <tr>
            <td style="background-image: url({{asset('assets/img/sale.svg')}}); background-repeat: no-repeat; background-position: center;  padding-top: 47px; height: 150px; display: block; margin-top: 20px">

            </td>
        </tr>
        <tr>
            {{--            REDUCERI--}}
            <td style="text-align: center;" colspan="2">
                <h1 style="color: #180D0D; font-size: 48px; margin: 0; font-family: Arial; font-weight: bold">{{__("front.reset_password_on", ['name' => @getSettingsField('site_title', $allSettings, true)])}}</h1>
            </td>
        </tr>
        <tr>
            <td style=" text-align: center; padding: 10px;" colspan="2">
                <p style="color:  #180D0D; font-family: Arial; font-weight: normal; font-size: 16px; line-height: 24px">{{__("front.reset_password_email_msg", ['name' => @$item['item']->name, 'url' => url('/')])}}</p>
            </td>
        </tr>
        <tr>
            <td style="background-image: url({{asset('assets/img/img-sale.png')}}); background-repeat: no-repeat; background-position: center;  padding-top: 47px; height: 266px; width: 472px; display: block; margin: 20px auto 0 auto">
            </td>
        </tr>
        <tr>
            <td>
                {{--TODO: add button EXPLORE US--}}
                <a href="{{urldecode(@$item['resetPassUrl'])}}"
                   style="color: #ffffff; font-size: 14px; font-family: Arial; padding: 16px 50px; background-color: #FF5E47; text-decoration: none; text-align: center; border-radius: 10px; margin: 10px auto 0 auto; display: block; width: 240px">{{__("front.link_reset_pass")}}</a>

            </td>
        </tr>
    </table>
    {{--TODO: add footer--}}
    <table bgcolor="#F9F9F8" width="600" align="center" style="margin: 48px auto 0 auto;" cellpadding="0"
           cellspacing="0">
        <tr>
            <td style="padding: 20px;">
                <table align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="line-height: 10px; font-size: 1px; background: #ffffff; padding: 6px 10px; border: 2px solid #EEEEEE; border-radius: 10px">
                            <a href="">
                                <img src="{{asset('assets/img/svg-icons/social_facebook.svg')}}" alt="icon">
                            </a>
                        </td>

                        <td width="10"></td>
                        <td style="line-height: 10px; font-size: 1px; background: #ffffff; padding: 6px 5px; border: 2px solid #EEEEEE; border-radius: 10px">
                            <a href="">
                                <img src="{{asset('assets/img/svg-icons/social_google.svg')}}" alt="icon">
                            </a>
                        </td>

                        <td width="10"></td>
                        <td style="line-height: 10px; font-size: 1px; background: #ffffff; padding: 6px 6px; border: 2px solid #EEEEEE; border-radius: 10px">
                            <a href="">
                                <img src="{{asset('assets/img/svg-icons/social_tweeter.svg')}}" alt="icon">
                            </a>
                        </td>

                        <td width="10"></td>
                        <td style="line-height: 10px; font-size: 1px; background: #ffffff; padding: 6px 6px; border: 2px solid #EEEEEE; border-radius: 10px">
                            <a href="">
                                <img src="{{asset('assets/img/svg-icons/social_vk.svg')}}" alt="icon">
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style="font:10px Arial; color:#242424; line-height: 20px;">
                <a href="mailto:sales@ecommerce.rocks" target="_blank"
                   style="line-height: 20px; font: 400 10px Arial, Helvetica, sans-serifl; color:  #2e3135; text-decoration: none; display: inline-block; padding-bottom: 5px;">info@moonglow.md</a>
                <br/>
                <a href="tel: +37369417415" target="_blank"
                   style="line-height: 20px; font: 400 10px Arial, Helvetica, sans-serif; color:  #2e3135; text-decoration: none; margin-bottom: 5px">+373
                    78 000 000 / +373 22 00 00 00</a>
                <br/>
                ecommerce.rocks, mun. Chișinău, str. Milescu-Spătaru 78
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" style="margin:0 auto; padding: 15px 30px;" cellpadding="0" cellspacing="0">
                    <tr>

                        <td align="center">
                            <a href="#" style="color: #B8B8B8; font:400 10px Arial; color:#b8b8b8;">Despre Noi</a>
                        </td>

                        <td width="30"></td>

                        <td align="center">
                            <a href="#" style="color: #B8B8B8; font: 400 10px Arial; color:#b8b8b8;">Politica de
                                cofidentialitate</a>
                        </td>

                        <td width="30"></td>

                        <td align="center">
                            <a href="#" style="color: #B8B8B8; font:400 10px Arial; color:#b8b8b8;">Anuleaza
                                abonarea</a>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td align="center" style="margin:0 auto; padding: 0 30px 15px 30px; color:#b8b8b8; font: 400 10px Arial;">
                © 2021 ecommerce.rocks
            </td>
        </tr>
    </table>
    <br>

    @if($footerMenu = @getMenuRecursive(4))
        @if(!empty($footerMenu))
            @foreach($footerMenu as $menu)
                <a style="padding:5px 0; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #0A67A6;text-decoration: none"
                   target="_blank" href="{{$menu->currentUrl}}">{{$menu->currentName}}</a>
            @endforeach
        @endif
    @endif

    <br>


    <a style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none"
       href="tel:{{clearPhone(@getSettingsField('md_phone', $allSettings))}}"
       target="_blank"><span>{{@getSettingsField('md_phone', $allSettings)}}</span></a>
    <a style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none"
       href="mailto:{{@getSettingsField('email', $allSettings)}}"
       target="_blank"><span>{{@getSettingsField('email', $allSettings)}}</span></a>
    <span style="margin-bottom:2px; font-family: 'Raleway','Arial Black', sans-serif;font-size:15px;font-weight:300;display: block;color: #2C2B2A;text-decoration: none">{{@getSettingsField('moldova_address', $allSettings, true)}}</span>

    <ul class="footer-socials-links" style="list-style-type: none;padding: 0">
        @if(@getSettingsField('facebook_link', $allSettings))
            <li style="float: left;margin-right: 10px;" class="facebook"><a
                        href="{{@getSettingsField('facebook_link', $allSettings)}}" target="_blank">
                    <img src="{{asset('assets/img/socials/facebook.png')}}" alt="facebook"></a></li>
        @endif
        @if(@getSettingsField('instagram_link', $allSettings))
            <li style="float: left;margin-right: 10px;" class="instagram"><a
                        href="{{@getSettingsField('instagram_link', $allSettings)}}" target="_blank"><img
                            src="{{asset('assets/img/socials/instagram.png')}}" alt="instagram"></a></li>
        @endif
        @if(@getSettingsField('twitter_link', $allSettings))
            <li style="float: left;margin-right: 10px;" class="twitter"><a
                        href="{{@getSettingsField('twitter_link', $allSettings)}}" target="_blank">
                    <img src="{{asset('assets/img/socials/twitter.png')}}" alt="twitter"></a></li>
        @endif
    </ul>
@endif

