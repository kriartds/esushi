<table style="color: #2C2B2A; border-collapse: collapse; font-family: 'Raleway','Arial Black', sans-serif; width: 600px; margin: auto">
    <tr style="background-image: url({{asset('assets/img/header-email.svg')}}); background-repeat: no-repeat; height: 144px">
        <td style="padding: 20px; text-align: center;" colspan="2">
            <a href="{{url(LANG)}}" target="_blank">
                {!! getLogo($allSettings) !!}
            </a>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;" colspan="2">
            <h1 style="color: #180D0D; font-size: 48px; font-family: 'Arial'; padding-top: 20px; ">{{__("front.you_have_success_order")}}
                <a style="color: #FF5E47; font-size: 32px; display: block;" href="{{url('/')}}"
                   target="_blank">{{@getSettingsField('site_title', $allSettings, true)}}</a>
            </h1>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p style="color: #180D0D; font-size: 16px; font-family: 'Arial'; text-align: center;">{{__("front.representatives_will_contact")}}
                <a style="color: #FF5E47; text-decoration: none"
                   target="_blank">{{@getSettingsField('phone', $allSettings)}}</a>{{__("front.to_confirm_order")}}
            </p>
            <p style="color: #180D0D; font-size: 16px; font-family: 'Arial'; text-align: center;">{{__("front.write_email_on")}}
                <a style="color: #FF5E47; text-decoration: none"
                   href="mailto:{{@getSettingsField('order_email', $allSettings)}}"
                   target="_blank">{{@getSettingsField('order_email', $allSettings)}}</a>.
            </p>
        </td>
    </tr>
    <table style="border: 2px solid #EEEEEE; border-radius: 10px; padding-left: 24px; padding-right: 24px; width: 600px; margin: 24px auto 0 auto">
        <tr style="text-align: left;">
            <td style="width: 300px; padding-bottom: 13px; padding-top: 22px; text-align: left; color: #180D0D; font-size: 14px; font-family: 'Arial'; font-weight: bold;"
                colspan="1">{{__("front.order_id")}}: <b style="color: #FF5E47">#{{@$item->order_id}}</b></td>
            <td style="  text-align: right;  font-weight: bold; font-family: 'Arial'; color: #180D0D; font-size: 14px;"
                colspan="2">{{__("front.date")}} <span
                        style=" font-family: 'Arial'; text-align: right;  font-weight: bold; color: #180D0D; font-size: 14px;">{{strtotime(@$item->order_date) ? date('d-m-Y H:i', strtotime($item->order_date)) : '-'}}</span>
            </td>

        </tr>

        <tr>
            <td style=" padding-bottom: 10px; text-align: left;" colspan="3">
                <h3 style="margin: 0; color: #B4B9C6; font-family:'Arial'; font-size: 14px; font-weight: bold;">{{@$useShipping ? __("front.delivery_information") : __("front.pickup_information")}}</h3>
            </td>
        </tr>

        <tr style="text-align: left;">
            <td style=" text-align: left; color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold;">{{__("front.name")}}
                : {{@$item->details->name}}</td>
            <td style="width: 300px;  color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold; text-align: right;"
                colspan="2">
                {{__("front.email")}}: {{@$item->email}}</td>
        </tr>
        <tr>
            <td style="width: 300px; padding: 3px 0;text-align: left; color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold;">
                {{__("front.phone")}}: {{@$item->phone}}
            </td>
            <td style="padding: 3px 0; text-align: right; color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold;"
                colspan="2">{{__("front.payment_method")}}: {{strtoupper(@$item->payment_method)}}</td>
        </tr>

        @if(@$useShipping)
            <tr>
                <td style="color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold;width: 300px; padding: 3px 0;text-align: left;"> {{__("front.city")}}: {{@$item->city}}</td>
                <td style="color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold;padding: 3px 0;text-align: right;" colspan="2"> {{__("front.section")}}: {{@$item->shipping_details->section}}</td>
            </tr>
            <tr>
                <td style="color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold; width: 300px; padding: 3px 0;text-align: left;" colspan="3">
                    {{__("front.address")}}: {{@$item->shipping_details->address}}
                </td>
            </tr>
            @if(@$item->shipping_details->house)
                <tr>
                    <td style="color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold; width: 300px; padding: 3px 0;text-align: left;" colspan="3">
                        {{__("front.house")}}: {{@$item->shipping_details->house}}
                    </td>
                </tr>
            @endif
            @if(@$item->shipping_details->scale)
                <tr>
                    <td style="color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold; width: 300px; padding: 3px 0;text-align: left;" colspan="3">
                        {{__("front.scale")}}: {{@$item->shipping_details->scale}}
                    </td>
                </tr>
            @endif
            @if(@$item->shipping_details->floor)
                <tr>
                    <td style="color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold; width: 300px; padding: 3px 0;text-align: left;" colspan="3">
                        {{__("front.floor")}}: {{@$item->shipping_details->floor}}
                    </td>
                </tr>
            @endif
            @if(@$item->shipping_details->intercom)
                <tr>
                    <td style="color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold; width: 300px; padding: 3px 0;text-align: left;" colspan="3">
                        {{__("front.intercom")}}: {{@$item->shipping_details->intercom}}
                    </td>
                </tr>
            @endif
            @if(@$item->shipping_details->apartment)
                <tr>
                    <td style="color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold; width: 300px; padding: 3px 0;text-align: left;" colspan="3">
                        {{__("front.apartment")}}: {{@$item->shipping_details->apartment}}
                    </td>
                </tr>
            @endif
            @if(@$item->shipping_details->ground_house)
                <tr>
                    <td style="color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold; width: 300px; padding: 3px 0;text-align: left;" colspan="3">
                        {{__("front.ground_house")}}: {{@$item->shipping_details->ground_house ? 'yes':'no'}}
                    </td>
                </tr>
            @endif
        @elseif(@$usePickup)

            <tr style="text-align: left;">
                <td style="color: #2D2D2D; font-size: 14px; font-family: 'Arial'; font-weight: bold; padding: 3px 0;text-align: left;">
                    {{@$item->pickupStore->globalName->address ?: @$item->pickup_details->address}},
                    {{@$item->pickupStore->globalName->name ?: @$item->pickup_details->name}}
                </td>
            </tr>

        @endif
        @if(@$cartProducts && $cartProducts->isNotEmpty())
            @foreach($cartProducts as $cartProduct)
                @php
                    $product = @$cartProduct->product;
                @endphp
                <tr>
                    <td style="width: 300px;  text-align: left; padding-bottom: 10px">
                        <div style="display: inline-block; float:left;">
                            <img style="height: 64px; width:64px; margin-right: 10px; border: 2px solid #EEEEEE; border-radius: 10px;"
                                 src="{{$cartProduct->customFile}}"
                                 alt="{{@$product->slug ?: @$cartProduct->info->slug}}"
                                 title="{{@$product->globalName->name ?: @$cartProduct->info->name}}">
                        </div>
                        <div style="display: inline-block; vertical-align: middle; width: 200px;">
                            <a style="margin: 0;  color: #2E3135; font-family: 'Arial'; font-size: 14px; text-decoration: none; "
                               href="{{customUrl(['products', (@$product->slug ?: @$cartProduct->info->slug), (@$cartProduct->products_variation_item_id ? $cartProduct->products_variation_item_id : "")])}}"
                               style="color: #2E3135; font-family: 'Arial'; font-size: 14px; "
                               target="_blank">{{@$product->globalName->name ?: @$cartProduct->info->name}}</a>
                            @if(!empty($cartProduct->attributes))
                                <div style="margin-top: 10px; font-size: 14px; color: #2E3135; font-family: 'Arial';">
                                    @foreach($cartProduct->attributes as $attribute)
                                        @php
                                            $parent = $attribute->parent;
                                            $child = $attribute->child;
                                            $parentName = @collect($parent->info)[LANG] ? @$parent->info->{LANG}->name : @collect($parent->info)->first()->name;
                                            $childName = @collect($child->info)[LANG] ? @$child->info->{LANG}->name : @collect($child->info)->first()->name;
                                        @endphp
                                        @if($parent && $child)
                                            <span style="color: #878D9B; font-size: 12px; ">{{$parentName}}: </span>
                                            <span style="font-size: 12px">{{$childName}}</span>
                                        @endif
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </td>
                    <td style=" text-align: right" colspan="1" width="300px">
                        <div style="font-size: 14px; margin-bottom: 10px;  text-align: right; font-family: 'Arial'; color: #878D9B; ">{!! @$cartProduct->amount && @$cartProduct->quantity ? formatPrice($cartProduct->amount * $cartProduct->quantity, @$item->order_currency) : '-' !!}

                            <span style="margin-right: 10px; margin-left: 10px; text-align: right; font-family: 'Arial'; font-size: 12px;  color: #2E3135">x{{@$cartProduct->quantity}}</span>
                            <span style="text-align: right; font-family: 'Arial'; font-size: 14px;   color: #2E3135">{!! @$cartProduct->amount ? formatPrice($cartProduct->amount, @$item->order_currency) : '-' !!}</span>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        @if(@$item->details->number_of_simple_sticks)
            <tr style="">
                <td style="padding: 16px 0; font-family: 'Arial'; font-size: 14px; border-top: 2px solid #EEEEEE; font-weight: bold; color: #B4B9C6;">{{__("front.number_of_simple_sticks")}}</td>
                <td style="padding: 16px 0; font-family: 'Arial'; font-size: 14px; text-align: right;  border-top: 2px solid #EEEEEE;">{{@$item->details->number_of_simple_sticks}}</td>
            </tr>
        @endif
        @if(@$item->details->number_of_beginner_sticks)
            <tr style="">
                <td style="padding: 16px 0; font-family: 'Arial'; font-size: 14px; border-top: 2px solid #EEEEEE; font-weight: bold; color: #B4B9C6;">{{__("front.number_of_beginner_sticks")}}</td>
                <td style="padding: 16px 0; font-family: 'Arial'; font-size: 14px; text-align: right;  border-top: 2px solid #EEEEEE;">{{@$item->details->number_of_beginner_sticks}}</td>
            </tr>
        @endif
        @if(@$item->details->comment)
            <tr style="">
                <td style="padding: 16px 0; font-family: 'Arial'; font-size: 14px; border-top: 2px solid #EEEEEE; font-weight: bold; color: #B4B9C6;">{{__("front.additional_description")}}</td>
                <td style="padding: 16px 0; font-family: 'Arial'; font-size: 14px; text-align: right;  border-top: 2px solid #EEEEEE;">{{@$item->details->comment}}</td>
            </tr>
        @endif
        <tr>
            <td style="padding-top: 20px; text-align: left;  font-size: 14px; font-family: 'Arial'; color: #2E3135; border-top: 2px solid #EEEEEE; "
                colspan="1">{{__("front.subtotal")}}</td>
            <td style="padding-top: 20px; text-align: right;  font-size: 14px;  font-family: 'Arial'; color: #2E3135; font-weight: bold; margin-left: 10px;border-top: 2px solid #EEEEEE; "
                colspan="1">{!! @$cartSubtotal ? formatPrice(@$cartSubtotal, @$orderCurrency) : '-' !!}</td>
        </tr>
        <tr>
            <td style="padding-top: 5px; text-align: left; font-size: 14px; font-family: 'Arial'; color: #2E3135;"
                colspan="1">{{__("front.discount_amount")}}</td>
            <td style="padding-top: 5px; text-align: right; font-size: 14px; font-family: 'Arial'; color: #2E3135; font-weight: bold"
                colspan="1">{!! @$item->discount_details ? ($item->discount_details->allow_free_shipping ? __("front.free_shipping_coupon") : (@$cartSubtotalWithCoupon ? formatPrice(@$cartSubtotalWithCoupon, @$orderCurrency) : '-')) : '-' !!}</td>
        </tr>
        <tr>
            <td style="padding-top: 5px; text-align: left;   font-size: 14px; font-family: 'Arial'; color: #2E3135;"
                colspan="1">{{__("front.delivery_amount")}}</td>
            <td style="padding-top: 5px; text-align: right; font-size: 14px; font-family: 'Arial'; color: #2E3135; font-weight: bold"
                colspan="1">{!! @$shippingAmount ? formatPrice(@$shippingAmount ?: 0, @$orderCurrency) : '-' !!}</td>
        </tr>
        <tr>
            <td style="padding-top: 5px; padding-bottom: 23px; text-align: left; font-size: 14px; font-family: 'Arial'; color: #2E3135; font-weight: bold"
                colspan="1">{{__("front.total")}}</td>
            <td style="padding-top: 5px; padding-bottom: 23px; text-align: right; font-size: 14px; font-family: 'Arial'; color: #2E3135; font-weight: bold"
                colspan="1">{!! @$cartTotal ? formatPrice(@$cartTotal, @$orderCurrency) : '-' !!}</td>
        </tr>
    </table>
    <table bgcolor="#F9F9F8" width="600" align="center" style="margin: 48px auto 0 auto;" cellpadding="0"
           cellspacing="0">
        <tr>
            <td style="padding: 20px;">
                <table align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
                    <tr>
                        @if(@getSettingsField('instagram_link', $allSettings))
                            <td style="line-height: 10px; font-size: 1px; background: #ffffff; padding: 6px 10px; border: 2px solid #EEEEEE; border-radius: 10px">
                                <a href="{{@getSettingsField('instagram_link', $allSettings)}}">
                                    <img src="{{asset('assets/img/socials/instagram.png')}}" alt="icon">
                                </a>
                            </td>
                            <td width="10"></td>
                        @endif

                        @if(@getSettingsField('facebook_link', $allSettings))
                            <td style="line-height: 10px; font-size: 1px; background: #ffffff; padding: 6px 5px; border: 2px solid #EEEEEE; border-radius: 10px">
                                <a href="{{@getSettingsField('facebook_link', $allSettings)}}">
                                    <img src="{{asset('assets/img/svg-icons/facebook.png')}}" alt="icon">
                                </a>
                            </td>
                            <td width="10"></td>
                        @endif

                        @if(@getSettingsField('twitter_link', $allSettings))
                            <td style="line-height: 10px; font-size: 1px; background: #ffffff; padding: 6px 5px; border: 2px solid #EEEEEE; border-radius: 10px">
                                <a href="{{@getSettingsField('twitter_link', $allSettings)}}">
                                    <img src="{{asset('assets/img/svg-icons/twitter.png')}}" alt="icon">
                                </a>
                            </td>
                            <td width="10"></td>
                        @endif

                        @if(@getSettingsField('telegram_link', $allSettings))
                            <td style="line-height: 10px; font-size: 1px; background: #ffffff; padding: 6px 5px; border: 2px solid #EEEEEE; border-radius: 10px">
                                <a href="{{@getSettingsField('telegram_link', $allSettings)}}">
                                    <img src="{{asset('assets/img/svg-icons/telegram.png')}}" alt="icon">
                                </a>
                            </td>
                            <td width="10"></td>
                        @endif
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style="font:10px Arial; color:#242424; line-height: 20px;">
                <a href="mailto:{{@getSettingsField('email', $allSettings)}}" target="_blank"
                   style="line-height: 20px; font: 400 10px Arial, Helvetica, sans-serifl; color:  #2e3135; text-decoration: none; display: inline-block; padding-bottom: 5px;">{{@getSettingsField('email', $allSettings)}}</a>
                <br/>

                <a href="#" target="_blank"
                   style="line-height: 20px; font: 400 10px Arial, Helvetica, sans-serif; color:  #2e3135; text-decoration: none; margin-bottom: 5px">
                    {{@getSettingsField('phone', $allSettings)}}</a>
                <br/>

                {{@getSettingsField('site_title', $allSettings, true)}}, {{@getSettingsField('address', $allSettings, true)}}
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" style="margin:0 auto; padding: 15px 30px;" cellpadding="0" cellspacing="0">
                    <tr>
                        @if($footerMenu = @getMenuRecursive(3))
                            @if(!empty($footerMenu))
                                @foreach($footerMenu as $menu)
                                    <td align="center">
                                        <a href="{{$menu->currentUrl}}"
                                           style="color: #B8B8B8; font:400 10px Arial; color:#b8b8b8;">{{$menu->currentName}}</a>
                                    </td>
                                    <td width="30"></td>
                                @endforeach
                            @endif
                        @endif
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style="margin:0 auto; padding: 0 30px 15px 30px; color:#b8b8b8; font: 400 10px Arial;">
                © 2020 {{@getSettingsField('site_title', $allSettings, true)}}
            </td>
        </tr>
    </table>
</table>
<br>