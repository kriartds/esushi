@if(request()->url() == customUrl('products-categories'))
    <div class="catalog-page"></div>
@endif

<!-- <div class="hide"> -->
<div class="">
    <div class="anchorSliderSubstrate"></div>
    <div class="anchorSlider" id="anchor-slider-main">
        <div class="wrap wrap__large">
            <div class="multiple-items">
                @foreach($sectionCategories as $sectionCategory)
                    <div>
                        <div class="categoriesListItem">
                            <a href="#{{$sectionCategory->slug}}" class="categoriesListLink scrolling">
                                <div class="imgContainer">
                                    <img src="{{getItemFile($sectionCategory, 'small')}}"
                                         alt="{{@$sectionCategory->globalName->name}}">
                                </div>
                                <div class="title">{{@$sectionCategory->globalName->name}}</div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="slick-prev slick-arrow">
            <svg>
                <use xlink:href="{{asset('assets/img/sprite.svg#carret-down')}}"></use>
            </svg>
        </div>
        <div class="slick-next slick-arrow">
            <svg>
                <use xlink:href="{{asset('assets/img/sprite.svg#carret-down')}}"></use>
            </svg>
        </div>
    </div>
    <div class="closeAnchorSlider"></div>
    <div class="wrap wrap__large">
        @include('front.templates.productSection', ['sections'=>$sectionCategories])
    </div>
</div>
