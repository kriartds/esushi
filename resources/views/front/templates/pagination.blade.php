@if ($pagination->lastPage() > 1)
    @php
        $currPage = $pagination->currentPage();
        $lastPage = $pagination->lastPage();
        $start = $currPage - 3;
        $end = $currPage + 3;
        if ($start < 1) $start = 1;
        if ($end >= $pagination->lastPage()) $end = $pagination->lastPage();

        $countPerPageEnd = $currPage < $pagination->lastPage() ? $pagination->perPage() * $currPage : $pagination->total();
        $paginationType = config('cms.catalog.paginationType', 'input');
    @endphp

    @if(@$displayCount && config('cms.catalog.displayTotalCount', true))
        <div class="productCountOnPage">
            {{__('front.products_count_on_catalog', ['count' => $countPerPageEnd, 'total' => $pagination->total()])}}
        </div>
    @endif

    <div class="pagination{{$paginationType == 'classic' ? '-classic' : ''}}">
        @if($paginationType == 'input')
            @if($currPage == 1)
                <div class="page-number first-page">
                    <svg class="icon leftArrow">
                        <use xlink:href="{{asset('/assets/img/sprite.svg#left')}}"></use>
                    </svg>
                </div>
            @else
                <a href="{{$pagination->url($currPage-1)}}" class="page-number first-page">
                    <svg class="icon leftArrow">
                        <use xlink:href="{{asset('/assets/img/sprite.svg#left')}}"></use>
                    </svg>
                </a>
            @endif

            <form action="{{$pagination->url($currPage)}}" method="get" id="pagination-form"
                  data-last-page="{{$lastPage}}">
                <input type="text" value="{{$currPage ?: 1}}" name="page" placeholder="{{$currPage ?: 1}}">
            </form>
            <span>din</span>
            <div>{{$lastPage}}</div>

            @if($currPage == $lastPage)
                <div class="page-number last-page">
                    <svg class="icon rightArrow">
                        <use xlink:href="{{asset('/assets/img/sprite.svg#left')}}"></use>
                    </svg>
                </div>
            @else
                <a class="page-number last-page" href="{{$pagination->url($currPage+1)}}">
                    <svg class="icon rightArrow">
                        <use xlink:href="{{asset('/assets/img/sprite.svg#left')}}"></use>
                    </svg>
                </a>
            @endif
        @else
            @if($currPage > 1)
                <a href="{{$pagination->url($currPage - 1)}}" class="page-number">
                    <svg class="icon leftArrow">
                        <use xlink:href="{{asset('assets/img/sprite.svg#carretBlack')}}"></use>
                    </svg>
                </a>
            @endif

            @if($currPage == 1)
                <span class="page-number active">1</span>
            @else
                <a class="page-number" href="{{$pagination->url(1)}}">1</a>
            @endif

            @if($start > 1)
                <span class="page-number">...</span>
            @endif

            @for ($i = $start + 1; $i < $end; $i++)
                @if($currPage == $i)
                    <span class="page-number active">{{$i}}</span>
                @else
                    <a class="page-number" href="{{$pagination->url($i)}}">{{$i}}</a>
                @endif
            @endfor

            @if($end < $pagination->lastPage())
                <span class="page-number">...</span>
            @endif

            @if($currPage == $lastPage)
                <span class="page-number active">{{$lastPage}}</span>
            @else
                <a class="page-number" href="{{$pagination->url($lastPage)}}">{{$lastPage}}</a>
            @endif

            @if($currPage < $lastPage)
                <a class="page-number" href="{{$pagination->url($currPage + 1)}}">
                    <svg class="icon rightArrow">
                        <use xlink:href="{{asset('assets/img/sprite.svg#carretBlack')}}"></use>
                    </svg>
                </a>
            @endif
        @endif
    </div>
@endif