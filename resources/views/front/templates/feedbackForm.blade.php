<section class="feedback-form">
    <div class="wrap">
        <div class="feedback-form__inner">
            <h3>{{__('front.subscribe_to_newsletter')}}</h3>
            <p>{{__('front.subscribe_first')}}</p>
            <form action="{{customUrl('subscribe')}}" method="POST" id="subscribe-form">
                <div class="form-group">
                    <input type="email" name="email" autocomplete="off"  placeholder="Email">
                    <div class="form-group__error-msg">Error message</div>
                </div>
                <button data-form-id="subscribe-form" type="submit" class="button submit-form-btn">{{__('front.subscribe')}}</button>
            </form>
        </div>
    </div>
    <div class="imgContainer">
        <div class="img">
{{--            <img src="{{asset('assets/img/feedback-form.png')}}" alt="">--}}
{{--            <img src="{{asset('assets/img/esushi_588x454_2.png')}}" alt="">--}}
        </div>
    </div>
</section>