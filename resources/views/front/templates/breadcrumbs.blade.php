@if($breadcrumbs = helpers()->breadcrumbs(@$currComponent, @$item, @$lastCategory, @$breadcrumbsByUrl))
    @if(!empty($breadcrumbs))
        <div class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
            @foreach($breadcrumbs as $key => $breadcrumb)
                @if($key < count($breadcrumbs) - 1 || count($breadcrumbs) == 1)
                    <div class="linkContainer" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a href="{{$breadcrumb['url']}}" itemtype="https://schema.org/Thing" itemprop="item">
                            <span itemprop="name">{{$breadcrumb['name']}}</span>
                        </a>
                        <meta itemprop="position" content="{{$key + 1}}"/>
                    </div>
                @else
                    <div class="linkContainer">
                        <span>{{$breadcrumb['name']}}</span>
                    </div>
                @endif
            @endforeach
        </div>
    @endif
@endif
