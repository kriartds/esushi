
@foreach($sections as $section)
    <section class="products-list" id="{{$section->slug}}">
        <h2 class="catalog_title">{{@$section->globalName->name}}</h2>
        @if($section->products && $section->products->isNotEmpty())
            @foreach($section->products as $itemKey=>$item)
                @include('front.products.productItem')
            @endforeach
        @endif
    </section>
@endforeach

