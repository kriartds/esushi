<!DOCTYPE html>
<html lang="{{LANG}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="{{@$descriptionPage ? $descriptionPage : ''}}">
    <meta name="keywords" content="{{@$keywordsPage ? $keywordsPage : ''}}">
    <title>{{@$titlePage ? $titlePage : config('app.name')}}</title>
    @foreach($langList as $lang)
        <meta property="og:locale{{$lang->is_default == 0 ? ':alternate' : ''}}"
              content="{{$lang->slug}}_{{strtoupper($lang->slug)}}"/>
    @endforeach
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{@$titlePage && !is_null($titlePage) ? $titlePage : config('app.name')}}"/>
    <meta property="og:description" content="{{@$descriptionPage ? $descriptionPage : ''}}"/>
    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:site_name" content="{{getAppDomain()}}"/>
    <meta property="og:fb:admins" content="{{getAppDomain()}}"/>
    <meta property="og:image" content="@if(isset($item) && !is_null($item)) {{getItemFile($item, 'medium')}} @endif" />
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:description" content="{{@$descriptionPage ? $descriptionPage : ''}}"/>
    <meta name="twitter:title" content="{{@$titlePage && !is_null($titlePage) ? $titlePage : config('app.name')}}"/>

    <link rel="icon" type="image/png" href="{{asset('favicon.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('favicon.png')}}">
    @php $themes_name = env('LOAD_THEMES'); @endphp
    <!-- <link href="/css/font-awesome-animation.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.0.7/css/all.css">
    <link rel="stylesheet" href="{{config('app.env') == 'production' ? asset('assets/'.$themes_name.'/css/font-awesome-animation.css') : asset('assets/'.$themes_name.'/css/font-awesome-animation.css?d=') . time()}}">
    <link rel="stylesheet"
          href="{{config('app.env') == 'production' ? asset('assets/'.$themes_name.'/css/main.css') : asset('assets/'.$themes_name.'/css/main.css?d=') . time()}}">
    <script>
        var base_url = '{{customUrl()}}';
        var current_currency_name = '{{@$currCurrency->name}}';
    </script>

<!-- Global site tag (gtag.js) - Google Ads: 822268665 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-822268665"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-822268665');
</script>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111138068-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-111138068-1');
        gtag('config', 'AW-822268665');
    </script>
    <!-- Global site tag (gtag.js) - Google Ads: 656603437 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-656603437"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-656603437');
	gtag('config', 'AW-822268665');
    </script>
    <script>
        function gtag_report_conversion(url) {
            var callback = function () {
                if (typeof(url) != 'undefined') {
                    window.location = url;
                }
            };
            gtag('event', 'conversion', {
                'send_to': 'AW-656603437/M9sVCI_RxN0BEK3yi7kC',
                'transaction_id': '',
                'event_callback': callback
            });
            return false;
        }
    </script>

<!-- Event snippet for Achiziție site conversion page
In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
<script>
function gtag_report_conversion(url) {
  var callback = function () {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  };
  gtag('event', 'conversion', {
      'send_to': 'AW-822268665/wOlPCNji6egBEPmli4gD',
      'transaction_id': '',
      'event_callback': callback
  });
  return false;
}
</script>

<!-- <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/80a2a67ab077a540d4dfb5d60/614aeaddc09c6ecb723c2f824.js");</script> -->


<!-- Global site tag (gtag.js) - Google Ads: 822268665 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-822268665"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-822268665'); </script> 

<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>

<!-- Event snippet for contact apel telefon site conversion page --> <script> gtag('event', 'conversion', {'send_to': 'AW-822268665/eqNiCOyxucsDEPmli4gD'}); </script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '294016985460294');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=294016985460294&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->


</head>
<body>

<div class="global-wrap">
    @yield('header')

    <main class="main">
        @yield('container')
    </main>
    @yield('footer')
    
    @php 
    /**
    * Check If Store Is Open
    */
    $restaurantInfo = helpers()->restaurantsIsOpen(true);
    
    if(is_array($restaurantInfo) && !Session::has('closedInformed')) {
        $isStoreClosed = TRUE;
        $restaurantAvailableTime = $restaurantInfo['availableTime'];

        $openTime = @array_keys(@$restaurantAvailableTime)[0];
        $closeTime = @array_values(@$restaurantAvailableTime)[0];

        Session::put('closedInformed', '1');
    }
    @endphp
   
    @if(@$isStoreClosed)
        @include('front.components.modals.closedStoreModal', ['openTime' => $openTime, 'closeTime' => $closeTime])
    @endif

{{--    {% include 'cart.njk' %}--}}
</div>
<!--scripts-->
{{--<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=default,Array.prototype.contains,Array.prototype.find,Array.prototype.findIndex,Array.prototype.Array.prototype.entries,Array.prototype.includes,Object.entries,String.prototype.contains,fetch,String.prototype.repeat"></script>--}}
<script src="{{config('app.env') == 'production' ? asset('assets/'.$themes_name.'/js/main2.js') : asset('assets/'.$themes_name.'/js/main.js?d=') . time()}}"></script>


<script src="{{asset('assets/'.$themes_name.'/js/jquery.maskedinput.js')}}"></script>

<script src="{{config('app.env') == 'production' ? asset('assets/'.$themes_name.'/js/core.js') : asset('assets/'.$themes_name.'/js/core.js?_=') . time()}}"></script>
<script src="{{config('app.env') == 'production' ? asset('assets/'.$themes_name.'/js/sms.js') : asset('assets/'.$themes_name.'/js/sms.js?_=') . time()}}"></script>

<!-- Global site tag (gtag) - Google Ads: 822268665 --> <amp-analytics type="gtag" data-credentials="include"> <script type="application/json"> { "vars": { "gtag_id": "AW-822268665", "config": { "AW-822268665": { "groups": "default" } } }, "triggers": { } } </script> </amp-analytics> 

</body>
</html>
