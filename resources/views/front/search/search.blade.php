@extends('front.app')

@include('front.header')

@section('container')

    <div class="wrap wrap__large">
        @if($items->isNotEmpty())
            <section class="viewResultSearch">
                <p class="result">Rezultatul căutării</p>
                <h1>{{request()->get('q', null)}}</h1>
                <section class="products-list">
                    @foreach($items as $item)
                        @include('front.products.productItem', ['item' => $item])
                    @endforeach
                </section>
            </section>
        @else
            <section class="resultSearchContainer">
                <div class="noResultSearch">
                    <p>{{__('front.search_not_found', ['search' => request()->get('q', null)])}}</p>
                    <a href="{{customUrl('')}}" class="button">{{__('front.go_to_homepage')}}</a>
                </div>
                <div class="imgContainer">
                    <img src="{{asset('assets/img/no-result.png')}}" alt="search no result">
                </div>
            </section>
        @endif
    </div>
    @include('front.templates.feedbackForm')
@stop

@include('front.footer')