@section('header')
    <header class="header">
        <div class="wrap wrap__large">
            <div class="header__inner">
                <div class="header__open-mobile-nav">
                    <div class="line"></div>
                    <div class="line"></div>
                    <div class="line"></div>
                </div>
                <div class="header__col">
                    <a class="header__logo" href="{{customUrl()}}">
                        {!! getLogo($allSettings) !!}
                    </a>

                    <div class="btnAnchorSlider hidden">
                        {{__('front.menu')}}
                        <svg>
                            <use xlink:href="{{asset('assets/img/sprite.svg#carret-down-anhor-menu')}}"></use>
                        </svg>
                    </div>

                    <div class="nav__close-nav"></div>
                    <div class="nav__nav-container">
                        <div class="nav__top-row">
                            <div class="header__user-auth-container">
                                @if(!auth()->user())
                                    <div class="header__unauth-user getModal" data-modal="#login">
                                        <svg>
                                            <use xlink:href="{{asset('assets/img/sprite.svg#user')}}"></use>
                                        </svg>
                                    </div>
                                @else
                                    <a class="header__auth-user" href="{{customUrl('dashboard')}}">
                                        <div class="dropdown__btn">
                                            @if(is_null(auth()->user()->avatar))
                                                <svg>
                                                    <use xlink:href="{{asset('assets/img/sprite.svg#user')}}">
                                                    </use>
                                                </svg>
                                            @else
                                                <img src="{{auth()->user()->avatar}}" alt="">
                                            @endif
                                        </div>
                                    </a>
                                @endif
                            </div>

                            @include('front.components.header.nav')

                            <div class="header__lang-container">
                                <div class="dropdown__menu">
                                    <div class="dropdown-mobile-menu">
                                        @include('front.components.header.headerLanguageDropdown', ['mobile'=>true])
                                    </div>
                                    <div class="dropdown-mobile-menu currency">
                                        @include('front.components.header.headerCurrencyDropdown', ['mobile'=>true])
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nav__bottom-row">
                            <div class="social-links">
                                <a href="" target="_blank"><img src="{{asset('assets/img/svg-icons/facebook.svg')}}" alt=""></a>
                                <a href="" target="_blank"><img src="{{asset('assets/img/svg-icons/instagram.svg')}}" alt=""></a>
                                <a href="" target="_blank"><img src="{{asset('assets/img/svg-icons/twitter.svg')}}" alt=""></a>
                            </div>
                            <div class="copyright">© 2020 - Esushi | All right reserved</div>
                        </div>
                    </div>
                </div>
                <div class="header__col">
                    <div class="header__search-container">
                        <div class="header__open-search-btn">
                            <span>{{__('front.search')}}</span>
                            <svg>
                                <use xlink:href="{{asset('assets/img/sprite.svg#lens')}}"></use>
                            </svg>
                        </div>
                        <div class="header__close-search-form"></div>
                        <form autocomplete="off" method="GET" action="{{customUrl('search')}}"
                              class="header__search-form"
                              id="search-form">
                            <div class="header__search-input-container">
                                <input type="text" autocomplete="off" name="q" placeholder="Cautare">
                                <button class="button " type="submit">
                                    <svg class="lens">
                                        <use xlink:href="{{asset('assets/img/sprite.svg#lens')}}"></use>
                                    </svg>
                                    <svg viewBox="0 0 50 50">
                                        <path fill="#000"
                                              d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
                                            <animateTransform attributeType="xml" attributeName="transform"
                                                              type="rotate" from="0 25 25" to="360 25 25" dur="1s"
                                                              repeatCount="indefinite"/>
                                        </path>
                                    </svg>
                                </button>
                            </div>
                            <div class="header__search-result" id="searchResult"></div>
                        </form>
                    </div>
                    <div class="header__user-auth-container">
                        @if(!auth()->user())
                            <div class="header__unauth-user getModal" data-modal="#login">
                                <svg>
                                    <use xlink:href="{{asset('assets/img/sprite.svg#user')}}"></use>
                                </svg>
                            </div>
                        @else
                            <div class="header__auth-user">
                                <div class="dropdown">
                                    <div class="dropdown__btn getModal" data-modal="#login">
                                        @if(is_null(auth()->user()->avatar))
                                            <svg>
                                                <use xlink:href="{{asset('assets/img/sprite.svg#user')}}">
                                                </use>
                                            </svg>
                                        @else
                                            <img src="{{asset(auth()->user()->avatar)}}" alt="">
                                        @endif
                                    </div>
                                    <div class="dropdown__menu">
                                        <div class="dropdown__item"
                                             href="tel:{{clearPhone(@getSettingsField('delivery_operator', $allSettings))}}">
                                            {{@getSettingsField('delivery_operator', $allSettings)}}
                                        </div>
                                        <a class="dropdown__item" href="{{customUrl('dashboard')}}">
                                            <svg>
                                                <use xlink:href="{{asset('assets/img/sprite.svg#dashboard')}}">
                                                </use>
                                            </svg>
                                            <div>{{__("front.consumer_dash_dashboard")}}</div>
                                        </a>
                                        <a class="dropdown__item" href="{{customUrl(['dashboard','settings'])}}">
                                            <svg>
                                                <use xlink:href="{{asset('assets/img/sprite.svg#user')}}">
                                                </use>
                                            </svg>
                                            <div>{{__("front.consumer_dash_profilul")}}</div>
                                        </a>
                                        <a class="dropdown__item" href="{{customUrl(['dashboard','orders-history'])}}">
                                            <svg>
                                                <use xlink:href="{{asset('assets/img/sprite.svg#clock')}}">
                                                </use>
                                            </svg>
                                            <div>{{__("front.consumer_dash_istoria_comenzilor")}}</div>
                                        </a>
                                        <a class="dropdown__item" href="{{customUrl(['logout'])}}">
                                            <svg>
                                                <use xlink:href="{{asset('assets/img/sprite.svg#logout')}}"></use>
                                            </svg>
                                            <div>{{__("front.consumer_dash_iesire")}}</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="header__cart-container">
                        <div class="dropdown">
                            <div class="dropdown__btn ">
                                {{--                                {#                             our shoping bug#}--}}
                                {{--                                {#                            <div class="ourBagHeader">#}--}}
                                {{--                                    {#                                <div class="icon-bg">#}--}}
                                {{--                                        {#                                    <svg>#}--}}
                                {{--                                            {#                                        <use xlink:href="./img/sprite.svg#our-shopping-bag">#}--}}
                                {{--                                                {#                                            <use>#}--}}
                                {{--                                                    {#                                                <svg>#}--}}
                                {{--                                                        {#                                </div>#}--}}
                                {{--                                    {#                                <div class="bag-header">#}--}}
                                {{--                                        {#                                    <div class="header__cart-total-price">99,00 Lei</div>#}--}}
                                {{--                                        {#                                    <div class="text">Cos comun</div>#}--}}
                                {{--                                        {#                                </div>#}--}}
                                {{--                                    {#                            </div>#}--}}
                                {{--                                {#                             our shoping bug#}--}}

                                {{--                                {#                            bag by default#}--}}

                                <div class="icon">
                                    <div class="counter" id="cart-count">
                                        {{@$cartCount > 0 ? (@$cartCount < 10 ? $cartCount : '9+') : '0'}}
                                    </div>
                                    <svg>
                                        <use xlink:href="{{asset('assets/img/sprite.svg#cart')}}"></use>
                                    </svg>
                                </div>
                                <div class="header__cart-total-price"
                                     id="cart-total">{!! formatPrice($cartSubtotal, getCurrentCurrency()) !!}</div>
                            </div>
                            <div class="dropdown__menu" id="cart-preview">
                                @include('front.components.header.headerCartThumbnail')
                            </div>
                        </div>
                    </div>
                    <div class="header__lang-value-container">
                        @include('front.components.header.headerCurrencyDropdown')
                        @include('front.components.header.headerLanguageDropdown')
                    </div>
                </div>
            </div>
        </div>
    </header>
@stop
