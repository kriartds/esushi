@if($items && $items->isNotEmpty())
    @foreach($items as $item)
        @include('front.blog.blogItem')
    @endforeach
@endif