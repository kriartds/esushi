@if(@$item)
    <a class="blogCard" href="{{customUrl(['blog', $item->slug])}}">
        <img src="{{getItemFile($item, @$isMainItem ? 'large' : 'medium')}}" alt="{{$item->slug}}"
             title="{{@$item->globalName->name}}" class="lazy">
        <div class="before">

            <div class="title">{{@$item->globalName->name}}</div>
            <div class="text">{!! subStrText(strip_tags(@$item->globalName->description), 150) !!}</div>
            <div class="icon-container">
                <svg class="icon">
                    <use xlink:href="{{asset('/assets/img/sprite.svg#icon-date')}}"></use>
                </svg>
                <div class="date">{{carbon($item->publish_date)->format('d M Y')}} </div>


            </div>
        </div>
    </a>
@endif