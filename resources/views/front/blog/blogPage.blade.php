@extends('front.app')

@include('front.header')

@section('container')

    <div class="wrap">

        @include("front.templates.breadcrumbs", ['currComponent' => $currComponent, 'item' => @$item, 'lastCategory' => @$lastCategory])
        <div class="blogPageContainer">
            <h1 class="pageTitle">
                {{@$item->globalName->name}}
            </h1>
            <div class="imgConatiner">
                <img src="{{getItemFile($item, 'large')}}" alt="{{$item->slug}}" title="{{@$item->globalName->name}}">
            </div>

            {!! @$item->globalName->description !!}

        </div>
    </div>

@stop

@include('front.footer')
