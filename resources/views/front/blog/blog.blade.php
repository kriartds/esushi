@extends('front.app')

@include('front.header')

@section('container')

    <div class="wrap">
        @include("front.templates.breadcrumbs", ['currComponent' => $currComponent, 'item' => @$category])

        <h1 class="pageTitle">
            {{@$category ? @$category->globalName->name : $titlePage}}
        </h1>

        <div class="blogContainer">
                @if(!is_null(@$mainItem))
                    <div class="firstSection">
                        <div class="leftBlock">
                            @include('front.blog.blogItem', ['item' => $mainItem, 'isMainItem' => true])
                        </div>
                        <div class="rightBlock">
                            @include('front.blog.blogList', ['items' => $secondaryItems])
                        </div>
                    </div>
                    <div class="mainSection load-more-content">

                        @include('front.blog.blogList', ['items' => $items])

                    </div>

                    @if($items->total() > 9)
                        <button class="viewAll load-more-btn custom-button-2"
                                data-url="{{customUrl(['blog', 'getBlogItems'])}}"
                                data-item-id="{{@$category->id}}"
                                data-current-page="{{$items->currentPage()}}" data-last-page="{{$items->lastPage()}}">
                            {{__("front.show_more")}}
                        </button>
                    @endif
                @else
                    <div class="empty-list">{{__("front.empty_list")}}</div>
                @endif
        </div>
    </div>
@stop

@include('front.footer')