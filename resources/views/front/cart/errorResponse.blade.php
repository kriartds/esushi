@extends('front.app')

@include('front.header')

@section('container')

    <section class="checkoutPage">
        <div class="wrap">
            <div class="checkoutSection error">
                <div class="order">
                    <h1 class="h1">{{__('front.order_error_title')}}</h1>
                    <a href="{{customUrl()}}" class="button">{{__('front.go_to_homepage')}}</a>
                </div>
                <div class="imgCheck">
                    <img src="{{asset('assets/img/error-check.png')}}" alt="">
                </div>
            </div>

        </div>

        @include('front.components.subscribe.subscribeForm')

    </section>

@stop

@include('front.footer')