@if(@$shippingZone)
    @if(@$shippingZone->zone && $shippingZone->methods->isNotEmpty())
        @foreach($shippingZone->methods as $key => $shippingMethod)
            <div class="deliveryMethod">
                <input type="radio"  name="shipping_method" id="{{$shippingMethod->method_type . $shippingMethod->id}}"
                       value="{{$shippingMethod->id}}" {{$shippingMethod->isCheckedMethod ? 'checked' : ''}} {{$shippingMethod->isDisabledMethod ? 'disabled' : ''}}>
                <label for="{{$shippingMethod->method_type . $shippingMethod->id}}">
                    {{@$shippingMethod->globalName->name}}
                    @if($shippingMethod->method_type == 'flat' || ($shippingMethod->method_type == 'pickup' && $shippingMethod->amount > 0))
                        ({!! formatPrice($shippingMethod->amount, $currCurrency) !!})
                    @elseif($shippingMethod->method_type == 'free' && $shippingMethod->free_shipping_requires != 'no_action' && $shippingMethod->free_shipping_requires != 'free_coupon')
                        (from {!! formatPrice($shippingMethod->min_order_amount, $currCurrency) !!})
                    @endif
                </label>
            </div>
        @endforeach
    @endif
@endif