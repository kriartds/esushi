@if(@$shippingZone && @$shippingZone->error)
    <div class="noDeliveryMsg">
        <span>{{$shippingZone->error}}</span>
    </div>
@endif