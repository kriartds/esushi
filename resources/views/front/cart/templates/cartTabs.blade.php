
<div class="cartTabsContainer" id="cartTabsContainer">
    <div class="wrap">
        <div class="cartTabs">
            <div class="navTabs">
                <div class="navItem active" data-tab="delivery"><span>{{__("front.delivery")}}</span></div>
                <div class="navItem" data-tab="pickUp"><span>{{__("front.pickup_from_store")}}</span></div>
            </div>
            <div class="tabContent">
                <div class="smallWrap">
                    <form method="post" action="{{customUrl('checkout')}}" class="form-group checkout-form">
                        <input type="hidden" name="checkout_type_shipping" value="shipping">
                        <h6>{{__("front.personal_data")}}</h6>
                        <div class="inputContainer">
                            <input type="text" placeholder="{{__("front.name")}}" name="name" value="{{auth()->check() ? $authUser->name : ''}}" required>
                        </div>
                        <div class="inputContainer">
                            <input type="text" placeholder="{{__("front.email")}}" name="email" value="{{auth()->check() ? $authUser->email : ''}}" required>
                        </div>
                        <div class="inputContainer">
                            <input class="phone" type="text" placeholder="{{__("front.phone")}}" name="phone" value="{{auth()->check() ? $authUser->phone : ''}}" required>
                        </div>
                        <div class="inputContainerNr">
                            <div class="nr">{{__('front.number_of_simple_sticks')}}</div>
                            <div class="product__counter">
                                <div class="minus">
                                    <img src="{{asset('assets/img/svg-icons/btn-minus.svg')}}" alt="">
                                </div>
                                <input class="counter_input" type="text" value="0" name="number_of_simple_sticks" readonly>
                                <div class="plus">
                                    <img src="{{asset('assets/img/svg-icons/btn-plus.svg')}}" alt="">
                                </div>
                            </div>
                            <!-- <label class="label">
                                <input type="checkbox" name="subscribe" value="1" checked>
                                <div class="check"></div>
                                <span>{{__('front.find_out_first_about_new_products')}}</span>
                            </label> -->
                        </div>
                        <div class="inputContainerNr">
                            <div class="nr">{{__('front.number_of_beginner_sticks')}}</div>
                            <div class="product__counter">
                                <div class="minus">
                                    <img src="{{asset('assets/img/svg-icons/btn-minus.svg')}}" alt="">
                                </div>
                                <input class="counter_input" type="text" value="0" name="number_of_beginner_sticks" readonly>
                                <div class="plus">
                                    <img src="{{asset('assets/img/svg-icons/btn-plus.svg')}}" alt="">
                                </div>
                            </div>
                        </div>


                        <div class="tabPane active" data-tab="delivery">
                            <h6>{{__('front.delivery_address')}}</h6>
                            <div class="selectContainer inputContainer">
                                <input type="hidden" value="{{@$cartDiscount && !@$cartDiscount->allow_free_shipping ? $cartSubtotalWithCoupon : $cartSubtotal}}" id="subtotal">
                                <select class="select2-container" name="zone" id="zone">
                                    @foreach($shippingZones as $zone)
                                        <option value="{{$zone->id}}" data-amount="{{$zone->shipping_amount}}"
                                                data-free-amount="{{$zone->free_shipping_amount}}" @if($zone->is_default) selected @endif>
                                            {{$zone->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="inputContainer">
                                <input type="text" name="section" placeholder="{{__('front.section')}}">
                            </div>
                            <div class="selectContainer inputContainer big">
{{--                                <input type="text" name="address" placeholder="{{__('front.address')}}">--}}
                                <select class="select2-container" name="address" id="address">
                                    <option value="">{{__('front.select_address')}}</option>
                                    @foreach($shippingAdresses as $address)
                                        <option value="{{$address->id}}">
                                            {{$address->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="smallInput">
                                <div class="inputContainer small">
                                    <input type="text" name="house" placeholder="{{__('front.house')}}">
                                </div>
                                <div class="inputContainer small">
                                    <input type="text" name="scale" placeholder="{{__('front.scale')}}">
                                </div>
                                <div class="inputContainer small">
                                    <input type="text" name="apartment" placeholder="{{__('front.apartment')}}">
                                </div>
                                <div class="inputContainer small">
                                    <input type="text" name="floor" placeholder="{{__('front.floor')}}">
                                </div>
                                <div class="inputContainer medium">
                                    <input type="text" name="intercom" placeholder="{{__('front.intercom')}}">
                                </div>
                                <label class="label">
                                    <input type="checkbox" value="1" name="ground_house">
                                    <div class="check"></div>
                                    <p>{{__('front.ground_house')}}</p>
                                </label>
                            </div>
                        </div>

                        <div class="tabPane" data-tab="pickUp">
                            <h6>{{__('front.choose_restaurant')}}</h6>
                            <div class="selectContainer inputContainer">
                                <select class="select2-container" name="restaurant">
                                    <option value="">Select</option>
                                    @foreach($restaurants as $restaurant)
                                        <option value="{{$restaurant->id}}">{{$restaurant->globalName->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="commentDelivery">
                            <div class="commentContainer">
                                <div class="addCommentBtn">
                                    <img src="{{asset('assets/img/svg-icons/addComm.svg')}}" alt="" class="icon">
                                    <img src="{{asset('assets/img/svg-icons/hideComm.svg')}}" alt="" class="icon">
                                    {{__("front.add_comment")}}
                                </div>
                                <textarea name="comment" rows="6" placeholder="{{__("front.comment_dots")}}"></textarea>
                            </div>
                            <div class="infoDelivery">
                                <div class="text deliv">{{__('front.delivery_price')}}</div>
                                <div class="text price"> {!! formatPrice($shippingAmount, $currCurrency) !!} </div>
                                <div class="freeDeliv @if(isset($defaultShipping->free_shipping_amount) && $defaultShipping->free_shipping_amount > 0) show-hiden @endif">
                                    {{__('front.delivery_free_from')}}
                                    <span class="free-from-amount">{!! formatPrice($defaultShipping->free_shipping_amount, $currCurrency) !!}</span>
                                </div>
                            </div>
                        </div>
                        <div class="radioboxContainer">
                            <div class="input">
                                <input type="radio" name="checkout_payment_method" value="cash" id="select1" class="color" checked>
                                <label for="select1">{{__("front.cash_payment")}}</label>
                            </div>
                            <div class="input">
                                <input type="radio" name="checkout_payment_method" value="card" id="select2" class="color">
                                <label for="select2">{{__("front.card_payment")}}</label>
                            </div>
                            <div class="input">
                                <input type="radio" name="checkout_payment_method" value="card_curier" id="select3" class="color">
                                <label for="select3">{{__("front.card_curier")}}</label>
                            </div>
                        </div>
                        <div class="footerBlock">
                            <div class="col">
                                <label class="label" style="margin-left: 0px;">
                                    <input type="checkbox" name="accept_terms" value="1">
                                    <div class="check"></div>
                                    <p>
                                        <span>{{__('front.agree_with')}}</span>
                                        <a href="{{customUrl('privacy-policy')}}" target="_blank" class="text link">{{__('front.privacy_policy')}},</a>
                                        <a href="{{customUrl('termenii-si-conditiile')}}" target="_blank" class="text link">{{__('front.terms_and_conditions')}}</a>
                                        <span>{{__('front.and_with')}}</span>
                                        <a href="{{customUrl('livrarea-si-returnarea')}}" target="_blank" class="text link">{{__('front.delivery_terms')}}</a>
</p>
                                </label>

                                <label class="label">
                                    <input type="checkbox" name="subscribe" value="1" checked>
                                    <div class="check"></div>
                                    <p>{{__('front.find_out_first_about_new_products')}}</p>
                                </label>
                            </div>

                            <div class="col">
                                <div id="cart-button">
                                    <input type="hidden" id="total-with-shipping" value="{{$cartTotal}}">
                                    <input type="hidden" id="total-without-shipping" value="{{$cartSubtotalWithCoupon}}">
                                    <button class="button checkout-form-submit-btn" type="submit" @if(!$allowToOrder) disabled @endif>
                                        <span>{{__('front.place_order')}}</span>
                                        <span class="total-to-pay">{!! formatPrice($cartTotal, $currCurrency) !!}</span>
                                    </button>
                                </div>
                                @if(!$allowToOrder)
                                    <div class="infoDelivs">
                                        {{__('front.delivery_finished_for_today')}}
                                        <a href="{{customUrl('livrarea-si-returnarea')}}" target="_blank" class="link">{{__('front.delivery_terms')}}</a>
                                    </div>
                                @endif
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
