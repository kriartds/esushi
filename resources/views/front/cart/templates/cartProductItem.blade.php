@if(@$item && @$cartProduct)
    <div class="productCart purchaseCard" data-item-id="{{$cartProduct->id}}">
        <div class="imgContainer">
            <img src="{{$cartProduct->customFile}}" alt="{{$item->slug}}" title="{{@$item->globalName->name}}">
            <div class="deleteCart destroy-cart-product" data-item-id="{{$cartProduct->id}}">
                <img src="{{asset('assets/img/cookies.png')}}" alt="">
            </div>
        </div>
        <div class="infoCart">
            <div class="titleCount">
                <div class="titleProd"><a href="{{customUrl(['products', $item->slug, (@$cartProduct->variation ? $cartProduct->variation->id : "")])}}">{{@$item->globalName->name}}</a></div>
                @if($cartProduct->cartAttributes->isNotEmpty())
                    @foreach($cartProduct->cartAttributes as $attribute)
                        @php
                            $parent = @$attribute['parent'];
                            $child = @$attribute['child'];
                        @endphp
                        @if($parent && $child)
                            <div class="count">{{@$child['item']->globalName->name}} {{@$parent->globalName->name}}</div>
                        @endif
                    @endforeach
                @endif
            </div>
            @if(!empty($cartProduct->toppings_ids))
                <div class="toppingBlock">
                    <div class="titleTopping">{{__('front.selected_toppings')}}</div>
                    <div class="topping">{{$cartProduct->toppings['toppingsName']}}</div>
                </div>
            @endif
            <div class="product__counter">
                <div class="price default">{!! $cartProduct->amount ? formatPrice($cartProduct->amount, $currCurrency) : '-' !!}</div>
                <div class="priceContainer">
                    <div class="minus update-product-quantity update-cart" data-op="diff" data-id="{{@$cartProduct->id}}">
                        <img src="{{asset('assets/img/minus-cart.png')}}" alt="">
                    </div>
                    <input type="text" value="{{@$cartProduct->quantity ?: 1}}" disabled="" class="update-product-quantity update-cart" data-id="{{@$cartProduct->id}}">
                    <div class="plus update-product-quantity update-cart" data-op="sum" data-id="{{@$cartProduct->id}}">
                        <img src="{{asset('assets/img/plus-cart.png')}}" alt="">
                    </div>
                </div>
                <div class="price total">{!! formatPrice($cartProduct->amount*$cartProduct->quantity, $currCurrency) !!}</div>
            </div>
        </div>
    </div>
@endif