@if(@$countries)
    <div class="selectContainer">
        <select class="select select2" name="pickup_country" id="pickup-countries">
            @foreach($countries as $country)
                <option value="{{$country->id}}" {{auth()->check() && @$authUser->country && $authUser->country_id == $country->id ? 'selected' : (@$currLocation->id == $country->id ? 'selected' : '')}}>{{$country->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="selectContainer">
        <select class="select select2" name="pickup_region" id="pickup-regions">
            @if($regions->isNotEmpty())
                @foreach($regions as $region)
                    <option value="{{$region->id}}" {{auth()->check() && @$authUser->region && $authUser->region_id == $region->id ? 'selected' : (@$currRegion->id == $region->id ? 'selected' : '')}}>{{$region->name}}</option>
                @endforeach
            @else
                <option value="">{{__("front.chose_country")}}</option>
            @endif
        </select>
    </div>
    <div class="selectContainer">
        <select class="select select2 no-search" name="pickup_address" id="pickup-addresses">
            @if($stores->isNotEmpty())
                @foreach($stores as $store)
                    <option value="{{$store->id}}">{{@$store->globalName->name}} | {{@$store->globalName->address}}</option>
                @endforeach
            @else
                <option value="">{{__("front.chose_region")}}</option>
            @endif
        </select>
    </div>
@endif