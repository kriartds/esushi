@extends('front.app')

@if(config('cms.cart.displayHeader', true))
    @include('front.header')
@endif

@section('container')
<section class="bagSection cartPage">
    <div class="wrap">

        @include("front.templates.breadcrumbs", ['currComponent' => $currComponent])

        <div class="ourBag my_cart">
            <h1 class="h1" id="cart_page_title">
                @if($isAvailableCommonCart)
                    {{__('front.page_common_cart_title')}}
                @else
                    {{@$titlePage}}
                @endif

            </h1>
            <button class="button tooltip @if($products->isEmpty() || $isAvailableCommonCart) hidden @endif" id="create_common_cart" data-page-title="{{__('front.page_common_cart_title')}}">
                <span class="tooltiptext">{{__('front.share_cart_with_friends')}} <a href="{{customUrl('cos-comun')}}">{{__('front.see_here')}}</a> </span>
                {{__('front.create_common_cart')}}
            </button>
            <div class="@if(!$isAvailableCommonCart) hidden @endif" id="common_cart_url" style="width: 36px;height: 36px;margin-left: 40px;">
                <a href="javascript:void(0)" class="icon" id="copy-link" title="{{__('front.copy_common_cart_link')}}"
                   data-link="{{customUrl(['cart', @$currentCart->common_id])}}"
                   data-success="{{__('front.success_function')}}">
                    <svg class="copy">
                        <use xlink:href="{{asset('assets/img/sprite.svg#copy')}}"></use>
                    </svg>
                </a>
            </div>
            <button class="button cancelCart @if(!$isAvailableCommonCart) hidden @endif" id="cancel_common_cart" data-page-title="{{__('front.page_cart_title')}}"> {{__('front.cancel_common_cart')}} </button>
            <button class="button checkProduct @if(!$isAvailableCommonCart) hidden @endif" id="refresh_common_cart"> {{__('front.refresh_common_cart')}} </button>
            <button class="button finish @if(!$isAvailableCommonCart) hidden @endif" id="finish_order"> {{__('front.finish_order')}} </button>

        </div>

        @if($products->isNotEmpty())
            <div class="flex-m">
                <div class="myOrderContainer purchaseContainer">
                    @foreach($products as $product)
                        @include ("{$path}.templates.cartProductItem", ['item' => $product->product, 'cartProduct' => $product])
                    @endforeach
                </div>
            </div>
        @endif
    </div>

    @if($products->isNotEmpty())

        <div class="promocodeContainer totalPriceContainer">
            <div class="wrap">
                <div class="flexCol">
                    <form class="promocode" id="coupon-form" method="post" action="{{customUrl('useCoupon')}}">
    {{--                    <div class="form-group">--}}
    {{--                        <div class="input">--}}
    {{--                            <input type="text" name="coupon_code" placeholder="{{__("front.promo_code")}}" autocomplete="off">--}}
    {{--                        </div>--}}
    {{--                        <button type="submit" class="button disable-button effect" data-form-id="coupon-form">{{__("front.apply")}}</button>--}}
    {{--                    </div>--}}
                    </form>

                    <div class="totalDiv subtotal-block">
                        <div class="grayText">{{__("front.subtotal")}}</div>
                        <div class="totalPrice" id="cart-subtotal-price">
                            {!! formatPrice(@$cartDiscount && !@$cartDiscount->allow_free_shipping ? $cartSubtotalWithCoupon : $cartSubtotal, $currCurrency) !!}
                        </div>
                    </div>

                    <div class="subtotal-without-coupon-block {{!@$cartDiscount || @$cartDiscount->allow_free_shipping ? 'hidden' : ''}}">
                        <div class="title">{{__("front.subtotal_without_coupon")}}</div>
                        <div class="totalPrice" id="cart-subtotal-coupon-price">{!! formatPrice($cartSubtotal, $currCurrency) !!}</div>
                    </div>

                </div>
            </div>
        </div>

        @include ("{$path}.templates.cartTabs")

    @endif

    <section class="resultSearchContainer emptyCartContainer @if($products->isNotEmpty()) hidden @endif">
        <div class="noResultSearch">
            <p>{{__('front.cart_empty')}}</p>
            <a href="{{customUrl('')}}" class="button">{{__('front.go_to_homepage')}}</a>
        </div>
        <div class="imgContainer">
            <img src="{{asset('assets/img/no-result.png')}}" alt="search no result">
        </div>
    </section>

</section>
@stop

@include('front.footer')