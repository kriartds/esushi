@extends('front.app')

@include('front.header')

@section('container')

    <section class="checkoutPage">
        <div class="wrap">
            <div class="checkoutSection">
                <div class="order">
                    <h1 class="h1">{{__('front.order_success_title')}}</h1>
                    <a href="" class="email">{{__('front.verify_email')}}</a>
                    <a href="{{customUrl()}}" class="button">{{__('front.go_to_homepage')}}</a>
                </div>
                <div class="imgCheck">
                    <img src="{{asset('assets/img/check.png')}}" alt="">
                </div>
            </div>

        </div>

        @include('front.components.subscribe.subscribeForm')

    </section>

@stop

@include('front.footer')