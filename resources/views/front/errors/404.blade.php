<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="{{asset('favicon.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('favicon.png')}}">
    <title>Not found</title>
    <meta name=”robots” content=”noindex,nofollow”>
    <style>

        @font-face {
            font-family: 'Gilroy-Bold';
            src: url('{{asset('assets/esushi/fonts/Gilroy-Bold.woff2')}}') format('woff2'),
            url('{{asset('assets/esushi/fonts/Gilroy-Bold.woff')}}') format('woff');
            font-weight: normal;
            font-style: normal;
        }
        @font-face {
            font-family: 'Gilroy-SemiBold';
            src: url('{{asset('assets/esushi/fonts/Gilroy-SemiBold.woff2')}}') format('woff2'),
            url('{{asset('assets/esushi/fonts/Gilroy-SemiBold.woff')}}') format('woff');
            font-weight: normal;
            font-style: normal;
        }

        body {
            margin: 0;
            padding: 0;
            font-family: 'Gilroy-Bold';

        }

        .main {
            width: 60%;
            margin: 0 auto;
            text-align: center;
            padding: 136px 20px 117px 20px;
        }

        h1 {
            font-size: 32px;
            line-height: 39px;
            margin: 0;
        }

        .not-found-image {
            margin: 0 auto;
            max-width: 835px;
        }

        img {
            width: 100%;
        }


        .button {
            display: inline-block;
            background: #EF5101;
            color: #ffffff;
            font-family: 'Gilroy-SemiBold';
            font-size: 16px;
            line-height: 24px;
            border-radius: 20px;
            padding: 8px 25px;
            text-align: center;
            transition: 0.2s;
            text-decoration: none;
        }




        @media (max-width: 767px) {
            .main {
                width: 100%;
                padding: 90px 16px 20px 16px;
            }
            h1{
                font-size: 30px;
            }
            .not-found-image{
                margin: 15px auto;
            }
        }
    </style>
</head>
<body>
<main>
    <div class="main">
        <h1>Oops the content is lost, but we still have the best sushi in town! Check it now</h1>
        <div class="not-found-image">
            <img src="{{asset('assets/img/404.png')}}" alt="">
        </div>
        <a href="{{customUrl()}}" class="button">Mergi la pagina principală</a>
    </div>
</main>
</body>

</html>