<div class="product__counter @if(is_null($cartProduct)) hidden @endif">
    <div class="minus update-product-quantity update-cart" data-op="diff" data-id="{{@$cartProduct->id}}">
        <img src="{{asset('assets/img/svg-icons/minus.svg')}}" alt="">
    </div>
    <input type="text" value="{{@$cartProduct->quantity ?: 1}}" disabled="" class="update-product-quantity update-cart" data-id="{{@$cartProduct->id}}">
    <div class="plus update-product-quantity update-cart" data-op="sum" data-id="{{@$cartProduct->id}}">
        <img src="{{asset('assets/img/svg-icons/plus.svg')}}" alt="">
    </div>
</div>