@extends('front.app')

@include('front.header')

@section('container')
    @if(@$sectionCategories)


        @include('front.templates.catalogFormat')
    @else
        <section class="categoriesPage">
            <div class="wrap wrap__large">
                <div class="breadcrumbs">
                    <div class="linkContainer"><a href="{{customUrl('')}}"><span>Acasa</span></a></div>
                    <div class="linkContainer"><a href="{{customUrl('products-categories')}}"><span>Catalog</span></a></div>
                    <div class="linkContainer"><span>{{@$titlePage}}</span></div>
                </div>
                <h1 class="h1">{{@$titlePage}}</h1>
            </div>
            <div class="border">
                <div class="wrap wrap__large">
                    <form method="POST" action="{{customUrl(['products-categories','filter'])}}" class="filter-categories filterList" id="filter-form">
                        @if($filteredProductsPricesRange->filter()->isNotEmpty())
                            {!! makeFilter(request(), $currComponent, $attributes, $filteredProductsPricesRange, @$currCurrency, @$item) !!}
                        @endif
                    </form>
                </div>
            </div>
            <div class="wrap wrap__large">
                <div class="products-list">
                    @include("front.components.catalog.itemsList", ['products' => $products])
                </div>
            </div>
            @include('front.templates.feedbackForm')
        </section>

{{--                <div class="categories">--}}
{{--                    <div class="wrap wrap__large">--}}
{{--                        <h1 class="pageTitle">--}}
{{--                            {{@$titlePage}}--}}
{{--                        </h1>--}}
{{--                    </div>--}}
{{--                    <div class="catalogContainer">--}}
{{--                        <div class="sort-row-box">--}}
{{--                            <div class="wrap wrap__large">--}}
{{--                                <div class="sort-row">--}}
{{--                                    @if($filteredProductsPricesRange->filter()->isNotEmpty())--}}
{{--                                        {!! makeFilter(request(), $currComponent, $attributes, $filteredProductsPricesRange, @$currCurrency, @$item) !!}--}}
{{--                                    @endif--}}
{{--                                    @if($filteredProductsPricesRange->filter()->isNotEmpty())--}}
{{--                                        {!! makeSortFilter(request(), $currComponent, @$item->id) !!}--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

        {{--                <div class="wrap wrap__large">--}}
        {{--                    <div class="contentContainer">--}}
        {{--                    <!-- <div class="filter_overlay">--}}
        {{--                                        <div class="close_filter_mobile">--}}
        {{--                                            <svg class="icon">--}}
        {{--                                                <use xlink:href="{{asset('assets/img/sprite.svg#closeBtn')}}"></use>--}}
        {{--                                            </svg>--}}
        {{--                                        </div>--}}
        {{--                                    </div>--}}


        {{--                        <div class="contentContainer">--}}
        {{--                        <!-- @if($filteredProductsPricesRange->filter()->isNotEmpty())--}}
        {{--                            {!! makeSortFilter(request(), $currComponent, @$item->id) !!}--}}
        {{--                        @endif -->--}}


        {{--                            <div class="products @if(@$categories) category_pages @endif">--}}
        {{--                                <div class="products-list {{request()->cookie('catalog_view', null) == 'horizontal' ? 'horizontal' : ''}}">--}}
        {{--                                            @include("front.components.catalog.itemsList", ['products' => $products])--}}
        {{--                                </div>--}}
        {{--                                <div id="loading-catalog"></div>--}}
        {{--                            </div>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    @endif
@stop

@include('front.footer')