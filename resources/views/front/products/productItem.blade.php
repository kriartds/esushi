@php $uniqProduct = uniqid(); @endphp
<div class="product__card">
{{--    {{##Nu este obligatoriu, il comentam deocamdata##}}--}}
{{--    <div class="count @if(is_null($item->cartProduct)) hidden @endif">--}}
{{--        @if(!is_null($item->cartProduct) && $item->cartProduct->quantity > 0) {{$item->cartProduct->quantity}} @endif--}}
{{--    </div>--}}
    <a class="product__img getProductModal" data-modal="#modal" href="{{customUrl(['products', @$item->slug])}}" data-slug="products/{{@$item->slug}}">
        <div class="product__badges">
            @if(!empty($item->badges))
                @foreach($item->badges as $badge)
                    <div class="product__badge product__badge_{{$badge}}">{{__('front.badge_'.$badge)}}</div>
                @endforeach
            @endif
        </div>
        <img src="{{getItemFile($item, 'medium')}}" alt="">
    </a>
    <div class="product__box">
        <div class="product__name"> {{@$item->globalName->name}} </div>

        <div class="product__attr">
            {{@$item->globalName->weight}}
        </div>

        <div class="product__desc">
            {{ str_replace('&nbsp;', '', strip_tags(@$item->globalName->description)) }}
        </div>
        <form method="POST" action="{{url(LANG, 'addToCart')}}" class="add-to-cart-form_" id="add-to-cart-form-{{$uniqProduct}}" name="add_to_cart_form_{{$uniqProduct}}">
            <input type="hidden" name="item_id" value="{{$item->id}}" autocomplete="off">
{{--            <input type="hidden" name="variation_id" autocomplete="off" value="{{@$defaultVariation->id}}">--}}
        @if($item->isVariationProduct)
             <div class="product__serving">
                @foreach($item->variationsDetails as $k=>$variationDetails)
                    @php $uniq = uniqid(); @endphp
{{--                    @if($variationDetails->is_default === 1)--}}
{{--                        <input type="hidden" name="variation_id" autocomplete="off" value="{{@$variationDetails->id}}">--}}
{{--                    @endif--}}
                    <div class="radio-wrap">
                        <input id="variation_{{$uniq}}" type="radio" name="variation_id"
                               value="{{@$variationDetails->id}}" @if($variationDetails->is_default === 1) checked @endif
                               data-price="{{$variationDetails->price}}" data-sale-price="{{$variationDetails->sale_price}}"
                        >
                        <label for="variation_{{$uniq}}">{{@$variationDetails->variation->childAttribute->globalName->name}}</label>
                    </div>
                    @if($loop->last)
                        {{@$variationDetails->variation->parentAttribute->globalName->name}}
                    @endif
                @endforeach
{{--                {{__('front.pieces')}}--}}
            </div>
        @endif

        <div class="product__price-container">

            <div class="product__price">
                {!! getProductPrices(@$item, $currCurrency, true, [], false) !!}
            </div>

            <button type="button" data-action="addToCart" data-form-id="add-to-cart-form-{{$uniqProduct}}" class="button submit-add-to-cart-btn @if(!is_null($item->cartProduct)) hidden @endif">
                {{__('front.addToCart')}}
            </button>

            @include('front.products.productUpdateQty', ['cartProduct' => $item->cartProduct])

        </div>
        </form>

    </div>
</div>
