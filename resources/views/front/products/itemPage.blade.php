@extends('front.app')

@include('front.header')

@section('container')

    <div class="wrap wrap__large" id="product" itemscope itemtype="http://schema.org/IndividualProduct">
        @include("front.templates.breadcrumbs", ['currComponent' => $currComponent, 'item' => @$item, 'lastCategory' => @$lastCategory])

        @include("front.components.productPage.mainDetails")

{{--        @include('front.components.productPage.templates.productAnchorsNav')--}}
{{--        @include("front.components.productPage.productDescription")--}}
{{--        @include("front.components.productPage.productCharacteristicsDescription")--}}
{{--        @include("front.components.productPage.productDeliveryInfo")--}}
{{--        @include("front.components.productPage.productReviews")--}}
{{--        @include("front.components.productPage.supportAndShareBlock")--}}
        @include("front.components.productPage.similarProductsSlider")
        @include("front.components.productPage.recommendedProductsSlider")
{{--        @include("front.components.productPage.withThisProductItBuysSlider")--}}
{{--        @include("front.components.productPage.viewedProductsSlider")--}}
        @include("front.components.subscribe.subscribeForm")
    </div>
@stop

@include('front.footer')