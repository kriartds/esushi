<div class="arcticmodal-container" style="display: none" id="productModal">
    <table class="arcticmodal-container_i">
        <tbody>
        <tr>
            <td class="arcticmodal-container_i2">
                <div class="modal" id="modal">
                    <div class="modal-close arcticmodal-close">
                        <svg>
                            <use xlink:href="{{asset('assets/img/sprite.svg#close-modal')}}"></use>
                        </svg>
                    </div>

                    <div class="containerProduct">
                        <div class="imgContainer">
                            <div class="product__badges">
                                @if(!empty($product->badges))
                                    @foreach($product->badges as $badge)
                                        <div class="product__badge product__badge_{{$badge}}">{{__('front.badge_'.$badge)}}</div>
                                    @endforeach
                                @endif
                            </div>
                            <img src="{{getItemFile($product, 'medium')}}" alt="">
                        </div>
                        <div class="aboutProduct">
                            <a href="" class="nameProduct">{{@$product->globalName->name}}</a>
                            <div class="product__attr">
                                {{@$product->globalName->weight}}
                            </div>
                            <div class="description">
                                {!! @$product->globalName->description !!}
                            </div>

                            {!! makeProductVariations(@$product, @$itemVariation, @$defaultValuesFromVariations ?: [], 'product_popup') !!}

                            @include('front.products.shareOnSocial')

                        </div>
                    </div>

                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>