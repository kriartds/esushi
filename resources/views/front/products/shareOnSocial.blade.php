<div class="shareSocialLink">
    <p>{{__('front.share_on_social')}}</p>
    <div class="icons">
        <a href="javascript:void(0)" class="icon social-button" data-share-url="https://www.facebook.com/sharer/sharer.php?u=">
            <svg class="fb">
                <use xlink:href="{{asset('assets/img/sprite.svg#facebook')}}"></use>
            </svg>
        </a>

        <a href="javascript:void(0)" class="icon" id="copy-link" title="{{__('front.copy_product_link')}}" data-success="{{__('front.success_function')}}">
            <svg class="copy">
                <use xlink:href="{{asset('assets/img/sprite.svg#copy')}}"></use>
            </svg>
        </a>

        <a href="javascript:void(0)" class="icon social-button" data-share-url="https://twitter.com/share?text=Share&url=">
            <svg class="tw">
                <use xlink:href="{{asset('assets/img/sprite.svg#twitter')}}"></use>
            </svg>
        </a>
    </div>
</div>