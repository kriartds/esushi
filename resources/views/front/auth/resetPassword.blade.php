@extends('front.app')

@include('front.header')

@section('container')



    <div class="wrap auth-pages">
        @include("front.templates.breadcrumbs")

        <h1 class="pageTitle">
            {{@$titlePage}}
        </h1>

        <div class="container">
            <form action="{{customUrl(['password', 'reset-password', $resetToken])}}" method="post" id="reset-pass-page-form">
                <div class="inputContainer">
                    <input type="password" placeholder="{{__("front.password")}}" name="password">
                </div>
                <div class="inputContainer">
                    <input type="password" placeholder="{{__("front.repeat_password")}}" name="repeat_password">
                </div>
                <div class="btnContainer">
                    <button type="submit" class="btn submit-form-btn" data-form-id="reset-pass-page-form">{{__("front.reset")}}</button>
                    <div class="secondaryBtn getModal" data-modal="#modal-login">{{__('front.login')}}</div>
                </div>
            </form>
        </div>
    </div>

@stop

@include('front.footer')