@extends('front.app')

@include('front.header')

@section('container')
    <section class="dashboardSection">
        <div class="wrap">
            <h1 class="h1">{{__("front.consumer_dash_cabinetul_personal")}}</h1>
            <div class="colsDashboard">
                <div class="colMedium height">
                    @if(@$user->avatar && @file_exists($user->avatar))
                        <img src="{{@asset($user->avatar)}}" alt="{{str_slug(@$user->name)}}" title="{{@$user->name}}">
                    @else
                        <div class="photoUser">
                            <img src="{{asset('assets/img/avatar.png')}}" alt="">
                        </div>
                    @endif
                    <div class="mainBlock">
                        @if(!@$user->name && !@$user->phone &&  !@$user->email && !@$user->address)
                            <div class="yourPersonalData">
                                {{__("front.complete_personal_data")}}
                            </div>
                        @else
                            <div class="mainInfo">
                                <div class="name">{{@$user->name}}</div>
                                <div class="phonenumber">{{@$user->phone}}</div>
                                <div class="email">{{@$user->email}}</div>
                                <br>
                                <div class="address">{{@$user->city}} {{@$user->user_region}} {{@$user->address}} {{@$user->user_ladder}}</div>
                            </div>
                        @endif
                    </div>
                    <a href="{{customUrl(['dashboard', 'settings'])}}" class="edit">Editeaza</a>
                </div>
                <div class="colMedium height">
                    <a class="historyLink" href="{{customUrl(['dashboard','orders-history'])}}" class="">{{__("front.consumer_dash_istoria_comenzilor")}} {{$ordersCount?:''}}</a>
                    @if($orders->isNotEmpty())
                        @foreach($orders as $order)
                            <div class="orderContainer">
                                <div class="date">{{carbon($order['order']->order_date)->format('d M Y')}}</div>
                                <div class="flexCol">
                                    @if($order['cartProducts']->isNotEmpty())
                                        @foreach($order['cartProducts'] as $cartProduct)
                                            <a href="{{customUrl(['products', @$cartProduct->product->slug])}}"
                                            class="imgProd" target="_blank">
                                                <img src="{{$cartProduct->customFile ?: @$cartProduct->info->firstFile}}"
                                                    alt="{{@$cartProduct->product->slug ?: @$cartProduct->info->slug}}"
                                                    title="{{@$cartProduct->product->globalName->name ?: @$cartProduct->info->name}}">
                                            </a>
                                        @endforeach
                                    @endif
                                    @if(@$order['remainCartProductsCount'])
                                        <div class="imgProd opacity">
                                            <div class="moreProd">
                                                +{{$order['remainCartProductsCount']}}
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="historyPhoto">
                            <img src="{{asset('assets/img/esushibox.png')}}" alt="">
                        </div>
                    @endif
                </div>
                <div class="colMedium saleCol">
                    <h5>{{__("front.consumer_dash_card_de_reducere")}}</h5>
                    <!-- <div class="txt">Pentru valorificare reducerii, te rugăm să completezi căsuța de mai jos cu cele 13 cifre de la codul de bare.</div> -->
		    <div class="txt">{{__("front.add_voucher")}}</div>
                    <form action="{{customUrl(['dashboard','apply-discount-card'])}}" method="POST"  class="form-group" id="apply-discount-card">
                        <div class="inputContainer">
                            <input type="text" placeholder="{{__("front.consumer_dash_numarul_cardului")}}">
                        </div>
                        <button class="button submit-form-btn" data-form-id="apply-discount-card">{{__("front.consumer_dash_aplica")}}</button>
                    </form>
                    <div class="img">
                        <img src="{{asset('assets/img/discountcard%201.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
        @include('front.templates.feedbackForm')
    </section>
@stop

@include('front.footer')
