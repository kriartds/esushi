@extends('front.app')

@include('front.header')

@section('container')
    <section class="orderHistory">
        <div class="wrap">
            <div class="breadcrumbs">
                <div class="linkContainer"><a href="{{customUrl(['dashboard'])}}"><span>{{__("front.consumer_dash_cabinetul_personal")}}</span></a>
                </div>
                <div class="linkContainer"><span>{{__("front.consumer_dash_ist_comenzi")}}</span></div>
            </div>
            <h1 class="h1">{{__("front.consumer_dash_ist_comenzi")}}</h1>
            @if($orders->isNotEmpty())
                <div class="order-history-container">
                    @foreach($orders as $order)
                        <section class="history-section is-being-processed">
                            <div class="shortInfo">
                                <div class="col-date">
                                    <div class="date">{{carbon($order['order']->order_date)->format('d M Y')}}</div>
                                    <div class="number">Nº {{$order['order']->order_id}}</div>
                                </div>
                                @if($order['cartProducts']->isNotEmpty())
                                    <div class="col-images">
                                        @foreach($order['cartProducts'] as $cartProduct)
                                            <div class="img {{ $order['remainCartProductsCount']?'opacity':''}}">
                                                <img src="{{$cartProduct->customFile ?: @$cartProduct->info->firstFile}}"
                                                     alt="">
                                                @if($order['remainCartProductsCount'])
                                                    <div class="moreProd">+{{$order['remainCartProductsCount']}}</div>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                @endif

                                <div class="col-in-process">
                                    @switch($order['order']->status)
                                        @case('checkout')
                                        @case('approved')
                                        @case('processing')
                                        <div class="circle in-process"></div>
                                        @break

                                        @case('canceled')
                                        @case('failed')
                                        <div class="circle finish"></div>
                                        @break

                                        @default
                                        <div class="circle rejected"></div>
                                        @break
                                    @endswitch

                                    <div class="text">{{__("front.order_status_{$order['order']->status}")}}</div>
                                </div>

                                <div data-url="{{customUrl('repeat-order')}}" data-order-id="{{$order['order']->order_id}}" class="repeat-order" id="repeat-order">
                                    <div class="text">Repetă comanda</div>
                                    <div class="repeat-order-img">
                                        <svg class="repeat-icon">
                                            <use xlink:href="{{asset('assets/img/sprite.svg#repeat')}}"></use>
                                        </svg>
                                        <svg class="loading-icon" viewBox="0 0 50 50">
                                            <path fill="#fff" d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
                                                <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 25 25" to="360 25 25" dur="1s" repeatCount="indefinite"></animateTransform>
                                            </path>
                                        </svg>
                                    </div>
                                </div>

                                <div class="col-price">
                                    <div class="price">{!! formatPrice(($order['order']->amount - ($order['order']->discount_amount ?: 0) + ($order['order']->shipping_amount ?: 0)), $order['order']->order_currency ?: $currCurrency) !!}</div>
                                </div>
                                <div class="col-open-close">
                                    <div class="open"></div>
                                    <div class="close"></div>
                                </div>
                            </div>
                            @if(@$order['allCartProducts']->products && $order['allCartProducts']->products->isNotEmpty())
                                <div class="mainInfo">
                                    <div class="table">
                                        @foreach($order['allCartProducts']->products as $cartProduct)
                                            <div class="cols">
                                                <div class="col-products">
                                                    <div class="flex">
                                                        <div class="product-img">
                                                            <img src="{{$cartProduct->customFile ?: @$cartProduct->info->firstFile}}"
                                                                 alt="" class="product">
                                                        </div>
                                                        <div class="colInfo">
                                                            <div class="title-product">{{@$cartProduct->product->globalName->name ?: @$cartProduct->info->name}}</div>
                                                            <div class="product__attr">
                                                                <span>285 g</span>|
                                                                <span>325 kcal</span>|
                                                                <span>4 bucăți</span>
                                                            </div>

                                                            <div class="product__desc">
                                                                Somon, cremă de brânză, avocado, castravete proaspăt.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="money left">{!! formatPrice($cartProduct->amount, $order['order']->order_currency ?: $currCurrency) !!}</div>
                                                <div class="add">x{{$cartProduct->quantity}}</div>
                                                <div class="money">{!! formatPrice(($cartProduct->amount * $cartProduct->quantity), $order['order']->order_currency ?: $currCurrency) !!}</div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @if($order['order']->shipping_method_id)
                                        <div class="delivery-cols">
                                            <div class="col-delivery">
                                                <div class="text">Livrare</div>
                                                <div class="info-address"> {{@$order['order']->country}}
                                                    , {{@$order['order']->region}}
                                                    , {{@$order['order']->city?@$order['order']->city.',':''}} {{@$order['order']->details->address}}</div>
                                            </div>
                                            <div class="price-delivery">
                                                <div class="money">{!! formatPrice($order['order']->shipping_amount, $currCurrency) !!}</div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endif
                        </section>
                    @endforeach
                </div>
            @else
                <div class="emptyOrderHistoryContainer">
                    <div class="empty-container">
                        <div class="col">
                            <div class="empty-order">
                                <div class="empty-order-img"></div>
                            </div>
                        </div>
                        <div class="col c-left">
                            <h2> {{__("front.orders_history_msg")}}</h2>
                            <a class="custom-button" href="{{customUrl('')}}">{{__("front.consumer_dash_home_page")}}</a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        @include('front.templates.feedbackForm')
    </section>
@stop
@include('front.footer')
