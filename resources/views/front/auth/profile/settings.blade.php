@extends('front.app')

@include('front.header')

@section('container')
    <section class="personalDate">
        <div class="wrap">
            <?php $currentUser = auth()->user()?>
            <div class="breadcrumbs">
                <div class="linkContainer"><a href="{{customUrl('dashboard')}}"><span>{{__("front.consumer_dash_cabinetul_personal")}}</span></a></div>
                <div class="linkContainer"><span>{{__("front.consumer_dash_date_personale")}}</span></div>
            </div>
            <h1 class="h1">{{__("front.consumer_dash_date_personale")}}</h1>
            <form class="form-group" method="POST" action="{{customUrl(['dashboard','edit-account'])}}" id="edit-user-account">
                <h5>{{__("front.consumer_dash_info_de_contact")}}</h5>
                <div class="form top">
                    <div class="inputContainer">
                        <input name="name" type="text" value="{{$currentUser->name}}" placeholder="{{__("front.consumer_dash_numele")}}">
                    </div>
                    <div class="inputContainer">
                        <input  name="email" value="{{$currentUser->email}}" type="text" placeholder="E-mail">
                    </div>
                    <div class="inputContainer">
                        <input name="phone" class="phone"  type="text" value="{{$currentUser->phone}}" placeholder="{{__("front.consumer_dash_telefon")}}">

                       @if(empty($currentUser->is_check_phone) && !empty($currentUser->phone))
                            <a href="javascript:;" class="phone-confirm">phone confirm</a>
                       @endif

                    </div>
                    <div class="inputContainer" id="phone_confirm_container">

                    </div>
                </div>
{{--                {{dd($currentUser)}}--}}
                <h5>{{__("front.consumer_dash_adresa_de_livrare")}}</h5>
                <div class="form">
                    <div class="inputContainer">
                        <input type="text" value="{{$currentUser->city}}" name="user_city" placeholder="{{__("front.consumer_dash_oras")}}">
                    </div>
                    <div class="inputContainer">
                        <input type="text" name="user_region" value="{{$currentUser->user_region}}" placeholder="{{__("front.consumer_dash_sector")}}">
                    </div>
                    <div class="inputContainer big">
                        <input type="text" name="user_address" value="{{$currentUser->address}}" placeholder="{{__("front.consumer_dash_adresa")}}">
                    </div>
                    <div class="inputContainer small">
                        <input type="text" name="user_ladder" value="{{$currentUser->user_ladder}}" placeholder="{{__("front.consumer_dash_scara")}}">
                    </div>
                    <div class="inputContainer small">
                        <input type="text" name="user_apartment" value="{{$currentUser->user_apartment}}" placeholder="{{__("front.consumer_dash_ap")}}">
                    </div>
                    <div class="inputContainer small">
                        <input type="text" name="user_floor" value="{{$currentUser->user_floor}}" placeholder="{{__("front.consumer_dash_etaj")}}">
                    </div>
                    <div class="inputContainer small" style="width: 110px;">
                        <input type="text" name="user_inter_phone" value="{{$currentUser->user_inter_phone}}" placeholder="{{__("front.consumer_dash_interfon")}}">
                    </div>
                </div>
                <button data-form-id="edit-user-account" class="button submit-form-btn">{{__("front.consumer_dash_salveaza")}}</button>
            </form>
        </div>

       @include('front.templates.feedbackForm')
    </section>
@stop

@include('front.footer')
<style>
    .btn-sm{
        padding: 2px 8px !important;
        font-size: 14px !important;
    }

    .btn-success{
        background: #13a025 !important;
    }

    .btn-info{
        background: #17a2b8 !important;;
    }

</style>