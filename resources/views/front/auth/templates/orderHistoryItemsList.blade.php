@if(@$orders && $orders->isNotEmpty())
    @foreach($orders as $order)
        <div class="orderHistoryBlock">
            @include('front.auth.templates.orderHistoryItem', ['item' => $order])
        </div>
    @endforeach
@endif