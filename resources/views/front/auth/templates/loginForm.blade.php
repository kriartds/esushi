<form action="{{customUrl('login')}}" method="post" id="{{@$isModalForm ? 'modal-' : ''}}login-page-form"
      class="loginForm">
    <div class="inputContainer">
        <input type="email" placeholder="{{__("front.email")}}" name="email">
    </div>
    <div class="inputContainer">
        <input type="password" placeholder="{{__("front.password")}}" name="password">
    </div>
    <div class="recoveryPasswordBtn getModal" data-modal="#modal-recovery">{{__("front.forgot_password")}}</div>
    <div class="btnContainer">
        <button type="submit" class="custom-button btn submit-form-btn" data-form-id="{{@$isModalForm ? 'modal-' : ''}}login-page-form">{{__("front.sing_in")}}</button>
        <div class="btn3 {{!@$isCheckoutForm ? 'getModal' : 'registerBtn'}}" {{!@$isCheckoutForm ? 'data-modal=#modal-registration' : ''}}>{{__("front.sing_up")}}</div>
    </div>
</form>