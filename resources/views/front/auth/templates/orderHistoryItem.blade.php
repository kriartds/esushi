@if(@$item)
    <div class="shortInfo">
        <div class="requiredData">
            <div class="data">{{carbon($item['order']->order_date)->format('d M Y')}}</div>
            <div class="orderId">№ {{$item['order']->order_id}}</div>
        </div>
        <div class="imgPreviewConatiner">
            @if($item['cartProducts']->isNotEmpty())
                @foreach($item['cartProducts'] as $cartProduct)
                    <a href="{{customUrl(['products', @$cartProduct->product->slug ?: @$cartProduct->info->slug, (@$cartProduct->products_variation_item_id ? $cartProduct->products_variation_item_id : "")])}}"
                       class="imgContainer" target="_blank">
                        <img src="{{$cartProduct->customFile ?: @$cartProduct->info->firstFile}}"
                             alt="{{@$cartProduct->product->slug ?: @$cartProduct->info->slug}}"
                             title="{{@$cartProduct->product->globalName->name ?: @$cartProduct->info->name}}">
                        @endforeach
                        @endif
                        @if(@$item['remainCartProductsCount'])
                            <div class="imgHint">
                                +{{$item['remainCartProductsCount']}}
                            </div>

                        @endif
                    </a>
        </div>
        <div class="orderStatus">
            <svg class="icon {{$item['order']->status}}" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                 viewBox="0 0 20 20">
                <g id="Ellipse_682" data-name="Ellipse 682" fill="#ffb300" stroke="#fff" stroke-width="2">
                    <circle cx="10" cy="10" r="10" stroke="none"/>
                    <circle cx="10" cy="10" r="9" fill="none"/>
                </g>
            </svg>
            <div class="text">{{__("front.order_status_{$item['order']->status}")}}</div>
        </div>


        <div class="rightBlock">
            <div class="price">{!! formatPrice(($item['order']->amount - ($item['order']->discount_amount ?: 0) + ($item['order']->shipping_amount ?: 0)), $item['order']->order_currency ?: $currCurrency) !!}</div>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="9" height="5"
                 viewBox="0 0 9 5">
                <defs>
                    <path id="9h9ga"
                          d="M131.5 4932.993a.786.786 0 0 1-.653-.211l-3.622-3.51a.728.728 0 0 1 0-1.053.785.785 0 0 1 1.088 0l3.187 3.086 3.187-3.086a.785.785 0 0 1 1.088 0c.3.29.3.763 0 1.054l-3.623 3.51a.78.78 0 0 1-.652.21z"/>
                </defs>
                <g>
                    <g opacity=".9" transform="translate(-127 -4928)">
                        <use fill="#002538" xlink:href="#9h9ga"/>
                    </g>
                </g>
            </svg>
        </div>
    </div>
    <div class="mainInfo">
        @if(@$item['allCartProducts']->products && $item['allCartProducts']->products->isNotEmpty())
            @foreach($item['allCartProducts']->products as $cartProduct)
                <div class="productRow">
                    <div class="leftBlock">
                        <a class="imgContainer"
                           href="{{customUrl(['products', @$cartProduct->product->slug ?: @$cartProduct->info->slug, (@$cartProduct->products_variation_item_id ? $cartProduct->products_variation_item_id : "")])}}">
                            <img src="{{$cartProduct->customFile ?: @$cartProduct->info->firstFile}}"
                                 alt="{{@$cartProduct->product->slug ?: @$cartProduct->info->slug}}"
                                 title="{{@$cartProduct->product->globalName->name ?: @$cartProduct->info->name}}">
                        </a>
                        <div class="productInfo">
                            <div class="nameContainer">
                                <a class="name"
                                   href="{{customUrl(['products', @$cartProduct->product->slug ?: @$cartProduct->info->slug, (@$cartProduct->products_variation_item_id ? $cartProduct->products_variation_item_id : "")])}}">{{@$cartProduct->product->globalName->name ?: @$cartProduct->info->name}}</a>
                            </div>
                            @if($cartProduct->attributes && !empty($cartProduct->attributes))
                                <div class="productOptionContainer">
                                    @foreach(collect($cartProduct->attributes)->sortBy('parent.position') as $attribute)
                                        @php
                                            $parent = @$attribute->parent;
                                            $child = @$attribute->child;
                                            $parentName = @collect($parent->info)[LANG] ? @$parent->info->{LANG}->name : @collect($parent->info)->first()->name;
                                            $childName = @collect($child->info)[LANG] ? @$child->info->{LANG}->name : @collect($child->info)->first()->name?@collect($child->info)->first()->name : @collect($child->info)->first();

                                        @endphp
                                        @if($parent && $child)
                                            <div class="block">
                                                <div class="title">{{$parentName}}:</div>
                                                <div class="option">
                                                    @if($parent->attribute_type == 'color' && !is_null($child->color) && is_array($child->color))
                                                        <div class="colorContainer"
                                                             title="{{$childName}}">
                                                            @if(!empty($child->color))
                                                                @foreach($child->color as $key => $color)
                                                                    @if(count($child->color) > 4)
                                                                        <div class="color"
                                                                             style="background: {{$color}}; width: calc(100% / {{count($child->color)}});"></div>
                                                                    @elseif(count($child->color) % 2 == 0)
                                                                        <div class="color"
                                                                             style="background: {{$color}}; width: calc(100% / {{count($child->color) / 2}});"></div>
                                                                    @elseif(count($child->color) == 3)
                                                                        <div class="color"
                                                                             style="background: {{$color}}; width: calc(100% / {{$key < 2 ? 2 : 1}});"></div>
                                                                    @else
                                                                        <div class="color"
                                                                             style="background: {{$color}}; width: calc(100% / {{$key + 1}});"></div>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                    @elseif($parent->attribute_type == 'image')
                                                        <div class="imageContainer">
                                                            <img src="{{$child->img}}"
                                                                 alt="{{@$child->slug}}"
                                                                 title="{{$childName}}">
                                                        </div>
                                                    @else
                                                        <span>{{$childName}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="rightBlock">
                        <div class="price">{!! formatPrice($cartProduct->amount, $item['order']->order_currency ?: $currCurrency) !!}</div>
                        <div class="count">x{{$cartProduct->quantity}}</div>
                        <div class="totalPrice">{!! formatPrice(($cartProduct->amount * $cartProduct->quantity), $item['order']->order_currency ?: $currCurrency) !!}</div>
                    </div>
                </div>
            @endforeach
        @endif


            @if($item['order']->shipping_method_id)
                <div class="footer-block">
                    @if($item['order']->shipping_method_id)
                        <div class="left-block">
                            <div class="text">Livrare</div>
                            <div class="address-block">{{@$item['order']->country->name}}, {{@$item['order']->region->name}}, {{@$item['order']->city?@$item['order']->city.',':''}} {{@$item['order']->details->address}}{{@$item['order']->details->zip?'.,'.@$item['order']->details->zip_code:''}}
                            </div>

                        </div>
                     @endif
                    <div class="right-block">
                        <div class="text">{!! formatPrice(($item['order']->amount - ($item['order']->discount_amount ?: 0) + ($item['order']->shipping_amount ?: 0)), $item['order']->order_currency ?: $currCurrency) !!}</div>
                    </div>
                </div>
            @endif
    </div>
@endif