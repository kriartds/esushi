<div class="socialContainer">
    <a class="icon fb" href="{{customUrl(['login', 'facebook'])}}">
        <svg>
            <use xlink:href="{{asset('assets/img/sprite.svg')}}#social_facebook"></use>
        </svg>
    </a>
    <a class="icon gl" href="{{customUrl(['login', 'google'])}}">
        <svg>
            <use xlink:href="{{asset('assets/img/sprite.svg')}}#social_google"></use>
        </svg>
    </a>
{{--    <a class="icon" href="{{customUrl(['login', 'instagram'])}}">--}}
{{--        <svg>--}}
{{--            <use xlink:href="{{asset('assets/img/sprite.svg')}}#social_instagram"></use>--}}
{{--        </svg>--}}
{{--    </a>--}}
{{--    <a class="icon" href="{{customUrl(['login', 'telegram'])}}">--}}
{{--        <svg>--}}
{{--            <use xlink:href="{{asset('assets/img/sprite.svg')}}#social_telegram"></use>--}}
{{--        </svg>--}}
{{--    </a>--}}
    <a class="icon tw" href="{{customUrl(['login', 'twitter'])}}">
        <svg>
            <use xlink:href="{{asset('assets/img/sprite.svg')}}#social_twitter"></use>
        </svg>
    </a>
    <a class="icon vk" href="{{customUrl(['login', 'vk'])}}">
        <svg>
            <use xlink:href="{{asset('assets/img/sprite.svg')}}#social_vk"></use>
        </svg>
    </a>
    @if(@$showLoginModalBtn)
        <a class="icon getModal" href="#modal-login">
            <svg>
                <use xlink:href="{{asset('assets/img/sprite.svg')}}#social_avatar"></use>
            </svg>
        </a>
    @endif
</div>