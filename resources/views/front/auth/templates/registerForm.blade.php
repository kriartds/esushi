<form action="{{customUrl('register')}}" method="post" id="{{@$isModalForm ? 'modal-' : ''}}register-page-form" class="registerForm">
    <div class="inputContainer">
        <input type="email" placeholder="{{__("front.email")}}" name="email">
    </div>
    <div class="inputContainer">
        <input type="password" placeholder="{{__("front.password")}}" name="password">
    </div>
    <div class="inputContainer">
        <input type="password" placeholder="{{__("front.repeat_password")}}" name="repeat_password">
    </div>
    <div class="termsAndConditions">
        <div class="field-wrap">
            <input id="{{@$isModalForm ? 'modal-' : ''}}accept-terms" type="checkbox" name="accept_terms">
            <label for="{{@$isModalForm ? 'modal-' : ''}}accept-terms">
                <span class="checkbox"></span>
                <span class="text">{{__("front.agree_terms_msg")}}</span>
            </label>
        </div>
    </div>
    <div class="btnContainer">
        <button type="submit" class="custom-button submit-form-btn" data-form-id="{{@$isModalForm ? 'modal-' : ''}}register-page-form">{{__("front.sing_up")}}</button>
        <div class="btn3 {{!@$isCheckoutForm ? 'getModal' : 'loginBtn'}}" {{!@$isCheckoutForm ? 'data-modal=#modal-login' : ''}}>{{__("front.sing_in")}}</div>
    </div>
</form>