@extends('front.app')

@include('front.header')

@section('container')

    <div class="wrap auth-pages">
        @include("front.templates.breadcrumbs")

        <h1 class="pageTitle">
            {{@$titlePage}}
        </h1>

        <div class="container">
            @include('front.auth.templates.socialItems')
            @include('front.auth.templates.loginForm')
        </div>
    </div>

@stop

@include('front.footer')