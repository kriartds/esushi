@extends('front.app')

@include('front.header')

@section('container')



    <div class="wrap auth-pages">
        @include("front.templates.breadcrumbs", ['breadcrumbsByUrl' => true])

        <h1 class="pageTitle">
            {{@$titlePage}}
        </h1>

        <div class="container">
            <form action="{{customUrl(['login', 'set-email'])}}" method="post" id="set-email-page-form">
                <div class="inputContainer">
                    <input type="text" placeholder="{{__("front.email")}}" name="email">
                </div>
                <div class="btnContainer">
                    <button type="submit" class="btn submit-form-btn" data-form-id="set-email-page-form">{{__("front.set_email")}}</button>
                </div>
            </form>
        </div>
    </div>

@stop

@include('front.footer')