@php
    /**
     * Template name: Promotions
     */
@endphp
@extends('front.app')

@include('front.header')

@section('container')
    <section class="promoSection">
        <div class="wrap wrap__large">
            <div class="promotionsContainer">
                <h1 class="h1">{{$item->globalName->name}} </h1>
                <div class="txt">{!! $item->globalName->description !!}</div>
                <div class="promotionsSectionCard">
                    @if($ourPromotions->isNotEmpty())
                        @foreach($ourPromotions as $promotion)
                            <div class="promotionCard">
                                <a href="{{customUrl(['promotion', $promotion->slug])}}">
                                    <div class="imgContainer">
                                        <img src="{{getItemFile($promotion, 'medium')}}"
                                             alt="{{@$promotion->globalName->name}}">
                                    </div>
                                    <div class="offer">{{@$promotion->globalName->name}}</div>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <div class="epmtyPromotions">
                            <div class="noResultSearch">
                                <p>Cu părere de rău nimic nu a fost găsit.</p>
                                <a href="{{customUrl('')}}" class="button">Pagia principală</a>
                            </div>
                            <div class="imgContainer">
                                <img src="{{asset('assets/img/no-result.png')}}" alt="search no result">
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @include('front.templates.feedbackForm')
    </section>
@stop

@include('front.footer')