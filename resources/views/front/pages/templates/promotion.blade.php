@extends('front.app')

@include('front.header')

@section('container')
    <section class="promo">
        <div class="wrap wrap__large">
            <div class="promotionPage">
                <div class="breadcrumbs">
                    <div class="linkContainer"><a href="{{customUrl()}}"><span>Home</span></a></div>
                    <div class="linkContainer"><a href="{{customUrl('promotions')}}"><span>Promotions</span></a></div>
                    <div class="linkContainer"><span>{{@$item->globalName->name}}</span></div>
                </div>
                <div class="promotionContainer">
                    <div class="photoPromo">
                        <img src="{{getItemFile($item, 'original')}}" alt="{{@$item->globalName->name}}">
                    </div>
                    <div class="aboutPromo">
                        <h1>{{@$item->globalName->name}}</h1>
                        <div class="text">
                            {!! @$item->globalName->description !!}
                        </div>
                        <div class="offer">Oferta vlabila pana la:</div>
                        <div class="date">{{strtotime($item->end_date) ? carbon($item->end_date)->format('d M Y') : '-'}}</div>
                    </div>
                </div>
                <section class="products-list">
                    @foreach($promotionProducts as $promotionProduct)
                        @include('front.products.productItem', ['item' => $promotionProduct])
                    @endforeach
                </section>
            </div>
        </div>
        @include('front.templates.feedbackForm')
    </section>
@stop

@include('front.footer')