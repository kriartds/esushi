@php
    /**
     * Template name: Terms and conditions page
     */
@endphp

@extends('front.app')

@include('front.header')

@section('container')

    <div class="wrap">
        @include("front.templates.breadcrumbs", ['item' => @$item])



        <div class="termsAndConditionsContainer">
            <h1 class="pageTitle">
                {{$titlePage}}
            </h1>
            {!! @$item->globalName->description !!}
        </div>

    </div>

@stop

@include('front.footer')