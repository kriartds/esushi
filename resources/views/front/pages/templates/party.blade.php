@extends('front.app')

@include('front.header')

@section('container')
    <section class="sushiParty ">
        <div class="wrap wrap__large">
            <div class="breadcrumbs">
                <div class="linkContainer"><a
                            href="{{customUrl($item->slug)}}"><span>{{@$item->globalName->name}}</span></a></div>
                <div class="linkContainer"><span>{{@$sushiParty->globalName->name}}</span></div>
            </div>
            @if($sushiParty->SushiPartyBlockProducts->isNotEmpty())
                <div class="sushiPartySection sushiPartyPage" id="partyProducts">
                    <h1 class="h1">{{@$sushiParty->globalName->name}}</h1>
                    <h5> {!! @$item->globalName->description !!} </h5>
                    <h3>{{__('front.offer_party_list')}}</h3>
                    <div class="large-col-offerts">
                        @foreach($sushiParty->SushiPartyBlockProducts as $products)
                            <a class="offer">
                                <div class="img delete-offer">
                                    <img class="check" src="{{asset('assets/img/check 1.png')}}" alt="">
                                    <img class="delete" src="{{asset('assets/img/delete-offer.png')}}" alt="">
                                </div>

                                <div class="imgContainer">
                                    <img src="{{getItemFile($products, 'medium')}}"
                                         alt="{{@$products->globalName->name}}">
                                </div>
                                <div class="footerInfo">
                                    <div>
                                        <div class="title">{{@$products->globalName->name}}</div>
                                        <div class="product__attr">
                                            {!!  @$item->globalName->weight !!}
                                        </div>
                                        <div class="text titleSet">
                                            {!! @$products->globalName->description !!}
                                        </div>
                                    </div>
                                    <button data-add-text="Adaugă în meniu" data-delete-text="Șterge din meniu"
                                            data-product-id="{{$products->id}}" class="button add-party-product">Adaugă
                                        în meniu
                                    </button>
                                </div>
                            </a>
                        @endforeach
                    </div>
                    <div class="info-form">
                        <form action="{{customUrl('send-sushi-party-form')}}" method="POST" id="sushi-party-form"
                              class="form-group">
                            <h5>{{__('front.programing_formular')}}</h5>
                            <div class="inputContainer">
                                <input type="text" name="name_user" placeholder="{{__('front.name_surname')}}">
                            </div>
                            <div class="inputContainer">
                                <input type="text" name="phone_user" placeholder="{{__('front.nr_phone')}}">
                            </div>

                            <input type="hidden" value="{{$sushiParty->id}}" name="party"  hidden>
                            <input type="hidden" value="[]" name="productIds" id="productIds" hidden>
                            <div class="inputContainer">
                                <input type="text" name="email_user" placeholder="{{__('front.email')}}">
                            </div>
                            <div class="inputContainer">
                                <select class="select select2-container no-search" name="regions" id="regions">
                                    <option value="Balti">Bălți</option>
                                    <option value="Chisinau">Chișinău</option>
                                    <option value="Ocnita">Ocnița</option>
                                </select>
                            </div>
                            <div class="inputContainer">
                                <input type="text" name="number_employment" placeholder="{{__('front.num_emp')}}">
                            </div>
                            <div class="inputContainer">
                                <input id="datetime" name="time" type="datetime-local" placeholder="Time">
                            </div>

                            <button data-form-id="sushi-party-form"
                                    class="bigBtn submit-form-btn button">{{__('front.send_form_party')}}</button>
                        </form>
                        <div class="imgContainer">
                            <img src="{{asset('assets/img/local-sushi.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            @endif
        </div>
        @include('front.templates.feedbackForm')
    </section>
@stop

@include('front.footer')
