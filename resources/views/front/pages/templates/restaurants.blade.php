@php
    /**
     * Template name: Restaurants Page
     */
@endphp

@extends('front.app')

@include('front.header')

@section('container')
    <section class="restaurantSection" xmlns="http://www.w3.org/1999/html">
        <div class="banerImg" style="background-image: url({{getItemFile($item,'original')}});">
            <div class="text">
                <h1 class="h1">{{@$item->globalName->name}}</h1>
                <h5>{!! @$item->globalName->description !!}</h5>
            </div>
        </div>
        @if(isset($ourRestaurants) && count($ourRestaurants))
            <div class="ourRestaurants">
                @foreach($ourRestaurants as $restaurant)
                    <div class="restaurant">
                        <div class="address">
                            {{@$restaurant->globalName->name}}: {{@$restaurant->globalName->address}}
                        </div>
                        <a href="{{customUrl(['restaurant', $restaurant->slug])}}" class="btn">{{__('front.details')}}</a>
                    </div>
                @endforeach
            </div>
        @endif

        <div class="iframeRestaurant">
            {!! getSettingsField('google-iframe', $allSettings) !!}
        </div>
        @include('front.templates.feedbackForm')
    </section>
@stop

@include('front.footer')
