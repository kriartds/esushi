@php
    /**
     * Template name: Contacts page
     */
@endphp

@extends('front.app')

@include('front.header')

@section('container')
     <section class="contactSection">
        <div class="wrap">
            <h1 class="h1">{{@$item->globalName->name}}</h1>
            <div class="flexCol">
                <div class="infoCol">
                    <div class="h5">{{__('front.delivery_operator')}} <a href="tel:{{clearPhone(@getSettingsField('delivery_operator', $allSettings))}}">{{@getSettingsField('delivery_operator', $allSettings)}}</a></div>
                    <div class="h5">{{__('front.restaurant_reservation')}} <a href="tel:{{clearPhone(@getSettingsField('restaurant_reservations', $allSettings))}}">{{@getSettingsField('restaurant_reservations', $allSettings)}}</a></div>
                    <div class="h5">{{__('front.email')}} <a href="mailto:{{@getSettingsField('email', $allSettings)}}">{{@getSettingsField('email', $allSettings)}}</a></div>
                    <a href="{{customUrl('restaurants')}}" class="btn allRestaurants">{{__('front.see_restaurants')}}</a>
                </div>
                <form method="post" action="{{customUrl('send-contact-form')}}" id="contactForm" class="form-group">
                    <div class="inputContainer">
                        <input type="text" name="name" placeholder="{{__('front.name')}}">
                    </div>

                    <div class="inputContainer">
                        <input class="phone" type="text" name="phone" placeholder="{{__('front.phone_number')}}">
                    </div>

                    <div class="inputContainer">
                        <input type="text" name="email" placeholder="{{__('front.email')}}">
                    </div>

                    <div class="inputContainer">
                        <input type="text" name="tema" placeholder="{{__('front.email_subject')}}">
                    </div>

                    <div class="textarea">
                        <textarea  name="message" cols="30" rows="7" placeholder="{{__('front.message')}}"></textarea>
                    </div>

                    <button data-form-id="contactForm" class="button submit-form-btn">{{__('front.send')}}</button>
                </form>
            </div>
        </div>
        <div class="iframeRestaurant">
            {!! getSettingsField('google-iframe', $allSettings) !!}
        </div>
        @include('front.templates.feedbackForm')
    </section>
@stop

@include('front.footer')