@php
    /**
     * Template name: About us page
     */
@endphp

@extends('front.app')

@include('front.header')

@section('container')

    <div class="wrap">
        @include("front.templates.breadcrumbs", ['item' => @$item])


        <div class="aboutUsContainer">
            <h1 class="pageTitle">
                {{$titlePage}}
            </h1>
            <div class="top-block">
                <div class="text ">
                    {!! @$item->globalName->description !!}
                </div>

                @if($itemFile = getItemFile($item, 'medium', false))
                    <div class="add-info">
                        <div class="companyImg ">
                            <img src="{{$itemFile}}" alt="{{$item->slug}}" title="{{@$item->globalName->name}}">
                        </div>

                    </div>
            </div>
            @endif

            @include('front.components.main.mainBenefitsBlock', ['isNotMainPage' => true ])

            @if (isset($employees) && $employees->isNotEmpty())
                <div class="ourTeam">
                    <h2>Echipa noastra</h2>
                    <div class="employeeContainer">
                        @foreach($employees as $employee)
                            <div class="employee">
                                <div class="imgContainer">
                                    <img src="{{$employee->firstFile->small}}" alt="">
                                </div>
                                <div class="aboutEmployee">
                                    <div class="name">{{$employee->name}}</div>
                                    <div class="position">{{$employee->job}}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif

            <?php
            $aboutSlider = getSliderBySlug('about-us');
            ?>

            @if(@$aboutSlider)
                <div class="companySlider">
                    @foreach($aboutSlider as $slide)
                        <div>
                            <div class="imgContainer">
                                <img src="{{$slide->firstFile->original}}" alt="{{$slide->globalName->name}}">
                            </div>
                        </div>
                    @endforeach

                </div>
            @endif


        </div>


    </div>

@stop

@include('front.footer')

{{--@section('scripts')--}}
{{--    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBy76ecqgj4pnTWMpPK4SLl6giQHE1wbps"></script>--}}
{{--@stop--}}