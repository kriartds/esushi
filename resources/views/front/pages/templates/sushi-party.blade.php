@php
    /**
     * Template name: Sushi Party Page
     */
@endphp

@extends('front.app')

@include('front.header')

@section('container')
    <section class="sushiParty">
        <div class="wrap wrap__large">
            <div class="sushiPartySection">
                <div class="breadcrumbs">
                </div>
                <h1 class="h1">{{@$item->globalName->name}}</h1>
                <h5>{!! @$item->globalName->description !!}</h5>
                <h3>{{__('front.offer_list')}}</h3>
                <div class="large-col-offerts">
                    @if(isset($sushiParty) && count($sushiParty))
                        @foreach($sushiParty as $party)
                            <a href="{{customUrl(['party', $party->slug])}}" class="offer">
                                <div class="imgContainer">
                                    <img src="{{getItemFile($party, 'medium')}}" alt="{{@$party->globalName->name}}">
                                </div>
                                <div class="footerInfo">
                                    <div>
                                        <div class="title">{{@$party->globalName->name}}</div>
                                        <div class="text">
                                            {!! nl2br(@$party->globalName->description )!!}
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        @include('front.templates.feedbackForm')
    </section>
@stop

@include('front.footer')
