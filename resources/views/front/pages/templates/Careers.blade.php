@php
    /**
     * Template name: Careers page
     */
@endphp

@extends('front.app')

@include('front.header')

@section('container')
    <main class="main">
        <section class="careerSection">
            <div class="wrap ">
                <div class="careersContainer">
                    <h1 class="h1">{{__('front.new_career_new_life')}}</h1>
                    <div class="form">
                        <form method="post" action="{{customUrl('send-cv-form')}}" class="form-group" id="sendCv">
                            <h5>{{__('front.career_description')}}</h5>
                            <div class="inputContainer">
                                <input name="name" type="text" placeholder="{{__('front.name')}}">
                            </div>
                            <div class="inputContainer">
                                <input name="phone" type="text" placeholder="{{__('front.phone_number')}}">
                            </div>
                            <div class="selectContainer inputContainer">
                                <select class="select2-container no-search" name="position" id="position">
                                    @foreach($ourVacancies as $vacancies)
                                        <option value="{{@$vacancies->globalName->name}}">{{@$vacancies->globalName->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="addCV">
                                <div class="uploaded-file-name">
                                    <div class="delete-file-btn">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                            <g><g><g><g><path fill="none" stroke="#8f959c" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="50" stroke-width="2" d="M12.333 1.667L1.667 12.333"/></g>
                                                        <g>
                                                            <path fill="none" stroke="#8f959c" stroke-linecap="round"
                                                                  stroke-linejoin="round" stroke-miterlimit="50"
                                                                  stroke-width="2" d="M1.667 1.667l10.666 10.666"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                    </div>
                                    <p></p>
                                </div>
                                <div class="txt file-feature">
                                    pdf, .doc, .docx, or .odt — limit 5MB
                                </div>
                                <label for="uploadedCv" class="btn">{{__('front.upload_cv')}}</label>
                                <input type="file" id="uploadedCv" name="uploadedCv" style="display: none" value="{{__('front.upload_cv')}}"  class="button">
                            </div>
                            <div class="termsCondition">
                                <input type="checkbox" name="acceptTerms" id="modal-accept-terms">
                                <label for="modal-accept-terms">
                                    <span class="checkbox"></span>
                                    <span>{{__('front.agree_with')}} <a href="#" target="_blank">{{__('front.personal_data_processing')}}</a> </span>
                                </label>
                            </div>
                            <button data-form-id="sendCv" class="button bigBtn submit-form-btn">{{__('front.send_cv')}}</button>
                        </form>
                        <div class="photoTeam">
                            <img src="{{getItemFile($item,'original')}}" alt="{{@$item->globalName->name}}">
                        </div>
                    </div>
                </div>
            </div>
           @include('front.templates.feedbackForm')
        </section>
    </main>
@stop

@include('front.footer')
