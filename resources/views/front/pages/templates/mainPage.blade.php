@php
    /**
     * Template name: Main page
     */
@endphp

@extends('front.app')

@include('front.header')

@section('container')

    <section class="homePage">
        <div class="wrap wrap__large">
            @include('front.components.main.mainSlider')

            @php
                $headerMenu = getMenuRecursive(1, 3)
            @endphp
            @if( $headerMenu->isNotEmpty())
                <section class="menuResponsive">
                    <div class="col">
                        <a href="{{customUrl('products-categories')}}">{{__('front.menu')}}</a>
                    </div>
                    @foreach($headerMenu as $menu)
                        @if(!empty($menu))
                            @if(!$menu->children->isNotEmpty())
                            <div class="col">
                                <a href="{{$menu->currentUrl}}">{{$menu->currentName}}</a>
                            </div>
                            @endif
                        @endif
                    @endforeach
                </section>
            @endif

        </div>
        @include('front.templates.catalogFormat')
        @include('front.templates.feedbackForm')
    </section>
@stop

@include('front.footer')

