@php
    /**
     * Template name: Restaurants Page
     */
@endphp
@extends('front.app')
@include('front.header')
@section('container')
    <section class="restaurantPage">
        <div class="wrap wrap__large">
            <div class="restaurantContainer">
                <div class="flexCol clearfix">
                    <div class="one-time">
                        <?php
                            $files = getItemFile($restaurant, 'large', true, 'allFiles')
                        ?>
                        @foreach($files as $file)
                            @if(!$loop->last)
                                    <div class="imgRestaurant">
                                        <img src="{{$file}}" alt="{{@$restaurant->globalName->address}}">
                                    </div>
                            @else
                                <?php $lastFile = $file?>
                            @endif
                        @endforeach
                    </div>
                    <div class="col titleAdDress">
                        <h1 class="h1">{{@$restaurant->globalName->name}}</h1>
                        <h5>{{@$restaurant->globalName->address}}</h5>
                    </div>
                    <div class="col middle">
                        <h3>{{__('front.working_hours')}}</h3>
                        <?php
                          $workingHour = array_filter(explode(PHP_EOL, @$restaurant->globalName->working_hour));
                        ?>
                        @if(count($workingHour))
                            @foreach($workingHour as $hour)
                                <div class="txt">{{$hour}}</div>
                            @endforeach
                        @endif
                    </div>
                    <div class="col colContact">
                        <h3>{{__("front.contacts")}}</h3>
                        <div class="txt bold">{{__('front.delivery_operator')}}
                            <a href="{{clearPhone($restaurant->delivery_operator)?:clearPhone(@getSettingsField('delivery_operator', $allSettings))}}">{!! $restaurant->delivery_operator?:@getSettingsField('delivery_operator', $allSettings) !!}</a>
                        </div>
                        <div class="txt bold">{{__('front.restaurant_reservation')}}
                            <a href="{{clearPhone($restaurant->delivery_operator)?:clearPhone(@getSettingsField('restaurant_reservations', $allSettings))}}">{!! $restaurant->restaurant_reservations?:@getSettingsField('restaurant_reservations', $allSettings) !!}</a>
                        </div>
                        <div class="txt bold">{{__('front.email')}}
                            <a href="mailto:{{$restaurant->email?:@getSettingsField('email', $allSettings)}}">{{$restaurant->email?:@getSettingsField('email', $allSettings)}}</a>
                        </div>
                    </div>

                </div>
                <div class="flexCol reverse">
                    <form method="post" class="form-group" action="{{customUrl('send-reservation-form')}}" id="reservationForm">
                        <div class="h1">{{__('front.reservation')}}</div>
                        <div class="inputContainer">
                            <input type="text" name="name" placeholder="{{__('front.name_surname')}}">
                        </div>
                        <div class="inputContainer">
                            <!-- <input type="text" name="time" placeholder="{{__('front.date_time')}}"> -->
                            <input type="datetime-local" name="time">
                        </div>
                        <div class="inputContainer">
                            <input type="text" name="number_of_people" placeholder="{{__('front.num_emp')}}">
                        </div>
                        <input type="hidden" name="restaurant" value="{{@$restaurant->id}}" hidden>
                        <div class="inputContainer">
                            <input type="text" name="phone" placeholder="{{__('front.nr_phone')}}">
                        </div>
                        <!-- <button data-form-id="reservationForm" class="button submit-form-btn">{{__('front.reservation_now')}}</button> -->
                    </form>
                    @if(isset($lastFile))
                        <div class="imgRestaurant">
                            <img src="{{$lastFile}}" alt="{{@$item->globalName->address}}">
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @include('front.templates.feedbackForm')
    </section>
@stop

@include('front.footer')
