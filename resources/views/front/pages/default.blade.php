@extends('front.app')

@include('front.header')

@section('container')

    <div class="wrap">
        <section class="terms">
            <h1 class="h1">{{$titlePage}}</h1>
            {!! @$item->globalName->description !!}
        </section>


    </div>
   @include('front.templates.feedbackForm')
@stop

@include('front.footer')