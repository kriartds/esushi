@extends('front.app')

@include('front.header')

@section('container')
        <section class="promoSection">
            <div class="wrap wrap__large">
                <div class="promotionsContainer">
                    <h1 class="h1">Promotii </h1>
                    <p class="txt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                    <p class="txt">Lorem Ipsum has been the industry's standard dummy text.</p>
                    <div class="promotionsSectionCard">
                        <div class="promotionCard">
                            <a href="">
                                <div class="imgContainer">
                                    <img src="{{asset('assets/img/esushi-promotion.jpg')}}" alt="">
                                </div>
                                <div class="offer">#ecadou 500 lei</div>
                            </a>
                        </div>


                        <div class="promotionCard">
                            <a href="">
                                <div class="imgContainer">
                                    <img src="{{asset('assets/img/esushi-promotion.jpg')}}" alt="">
                                </div>
                                <div class="offer">#ecadou 500 lei</div>
                            </a>
                        </div>


                        <div class="promotionCard">
                            <a href="">
                                <div class="imgContainer">
                                    <img src="{{asset('assets/img/esushi-promotion.jpg')}}" alt="">
                                </div>
                                <div class="offer">#ecadou 500 lei</div>
                            </a>
                        </div>


                        <div class="promotionCard">
                            <a href="">
                                <div class="imgContainer">
                                    <img src="{{asset('assets/img/esushi-promotion.jpg')}}" alt="">
                                </div>
                                <div class="offer">#ecadou 500 lei</div>
                            </a>
                        </div>



                    </div>

                </div>
            </div>

           @include('front.templates.feedbackForm')
        </section>

{{--    <div class="wrap">--}}
{{--        @include("front.templates.breadcrumbs", ['currComponent' => $currComponent, 'item' => @$category])--}}
{{--        <div class="promotions-page">--}}
{{--            <h1 class="pageTitle">--}}
{{--                {{$titlePage}}--}}
{{--            </h1>--}}
{{--            <div class="promotionContainer">--}}

{{--                <div class="description-column">--}}

{{--                    <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis--}}
{{--                        corporis cum cumque, dolorum eligendi fugit inventore natus nemo, neque nobis numquam--}}
{{--                        perferendis quae, quaerat quo sint veritatis vero voluptas.--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--                <div class="img-promotions">--}}
{{--                    <div class="imgContainer">--}}
{{--                        <img src="{{asset('assets/img/small-icons/promo.svg')}}" alt="">--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--            @if($items->isNotEmpty())--}}

{{--                <div class="cartTabs">--}}
{{--                    <div class="navTabs">--}}
{{--                        <div class="navItem active" data-tab="recent">Recente</div>--}}
{{--                        <div class="navItem" data-tab="archiva">Arhiva</div>--}}
{{--                    </div>--}}
{{--                    <div class="tabContent">--}}
{{--                        <div class="tabPane active" data-tab="recent">--}}
{{--                            <div class="flex">--}}
{{--                                @foreach($items as $item)--}}

{{--                                    <a class="promotionCard" href="{{customUrl(['promotions', $item->slug])}}">--}}
{{--                                        <div class="imgContainer">--}}
{{--                                            <img src="{{getItemFile($item, 'medium')}}" alt="{{$item->slug}}"--}}
{{--                                                 title="{{@$item->globalName->name}}">--}}
{{--                                        </div>--}}
{{--                                        <div class="name">{{@$item->globalName->name}}</div>--}}
{{--                                    </a>--}}
{{--                                    --}}{{--TODO: de finisat --}}

{{--                                @endforeach--}}
{{--                            </div>--}}

{{--                        </div>--}}

{{--                        <div class="tabPane {{auth()->check() ? 'active' : ''}}" data-tab="archiva">--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                --}}{{--                <div class="paginationContainer">--}}
{{--                --}}{{--                    @if($items instanceof \Illuminate\Pagination\LengthAwarePaginator && $items->total() > (int)$items->perPage())--}}
{{--                --}}{{--                        @include('front.templates.pagination', ['pagination' => $items, 'displayCount' => true])--}}
{{--                --}}{{--                    @endif--}}
{{--                --}}{{--                </div>--}}
{{--            @else--}}
{{--                <div class="empty-list">{{__("front.empty_list")}}</div>--}}
{{--            @endif--}}

{{--        </div>--}}
{{--    </div>--}}
@stop

@include('front.footer')