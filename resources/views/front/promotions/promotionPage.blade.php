@extends('front.app')

@include('front.header')

@section('container')

    <div class="wrap">
        @include("front.templates.breadcrumbs", ['currComponent' => $currComponent, 'item' => @$item])

        <div class="promotionPageContainer">

            <div class="descriptionContainer">
                <div class="imgContainer">
                    <img src="{{getItemFile($item, 'medium')}}" alt="{{$item->slug}}"
                         title="{{@$item->globalName->name}}">
                </div>
                <div class="description">
                    <div>
                        <h2>{{@$item->globalName->name}}</h2>
                        <div class="text">{!! @$item->globalName->description !!}</div>
                    </div>
                    <div class="descriptionFooter">
                        <div class="title">{{__("front.promotion_valid")}}:</div>
                        <div class="validDate">{{strtotime($item->end_date) ? carbon($item->end_date)->format('d M Y') : '-'}}</div>
                    </div>
                </div>
            </div>

            @if($promotionProducts->isNotEmpty())
                <div class="products">
                    <div class="products-list fourColumns">
                        @foreach($promotionProducts as $promotionProduct)
                            @include('front.products.productItem', ['item' => $promotionProduct])
                        @endforeach
                    </div>
                </div>

                <div class="paginationContainer">
                    @if($promotionProducts instanceof \Illuminate\Pagination\LengthAwarePaginator && $promotionProducts->total() > (int)$promotionProducts->perPage())
                        @include('front.templates.pagination', ['pagination' => $promotionProducts, 'displayCount' => true])
                    @endif
                </div>
            @else
                <div class="empty-list">{{__("front.empty_list")}}</div>
            @endif

        </div>
    </div>
@stop

@include('front.footer')