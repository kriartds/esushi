<!-- LOGIN MODAL -->

<div class="arcticmodal-container">
    <table class="arcticmodal-container_i">
        <tbody>
            <tr>
                <td class="arcticmodal-container_i2">
                    <div class="modal" id="modal-login">
                        <div class="background-modal"></div>
                        <div class="modal-close arcticmodal-close">
                            <div class="close-modal">
                                <img src="{{asset('assets/img/small-icons/close-modal.svg')}}" alt="icon">
                            </div>
                        </div>
                        <h1>{{__('front.login')}}</h1>
                        <div class="small-text">Loghează-te cu rețele de socializare</div>
                        @include('front.auth.templates.socialItems')
                        <div class="black-text">sau folosește e-mailul și parola</div>
                        @include('front.auth.templates.loginForm', ['isModalForm' => true])
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>


<!-- REGISTRATION MODAL -->

<div class="arcticmodal-container">
    <table class="arcticmodal-container_i">
        <tbody>
            <tr>
                <td class="arcticmodal-container_i2">
                    <div class="modal modal2" id="modal-registration">
                        <div class="modal-close arcticmodal-close">
                            <div class="close-modal">
                                <img src="{{asset('assets/img/small-icons/close-modal.svg')}}" alt="icon">
                            </div>
                        </div>
                        <h1>{{__('front.register')}}</h1>
                        <div class="small-text">Înregistrează-te cu rețele de socializare</div>
                        @include('front.auth.templates.socialItems')
                        <div class="black-text">sau folosește e-mailul și parola</div>
                        @include('front.auth.templates.registerForm', ['isModalForm' => true])
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<!-- RECOVERY MODAL -->

<div class="arcticmodal-container">
    <table class="arcticmodal-container_i">
        <tbody>
            <tr>
                <td class="arcticmodal-container_i2">
                    <div class="modal modal3" id="modal-recovery">
                        <div class="modal-close arcticmodal-close">
                            <div class="close-modal">
                                <img src="{{asset('assets/img/small-icons/close-modal.svg')}}" alt="icon">
                            </div>
                        </div>
                        <h1>{{__("front.reset_password")}}</h1>
                        <form action="{{customUrl(['password', 'reset-password'])}}" method="post" id="reset-pass-form">
                            <div class="text">{{__("front.reset_password_instruction")}}</div>
                            <div class="inputContainer">
                                <input type="email" placeholder="{{__("front.email")}}" name="email">
                            </div>
                            <button type="submit" class="custom-button submit-form-btn"
                                    data-form-id="reset-pass-form">{{__("front.reset")}}</button>

                        </form>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>