<div class="arcticmodal-container" style="display: none">
    <table class="arcticmodal-container_i">
        <tbody>
        <tr>
            <td class="arcticmodal-container_i2">
                <div class="modal" id="modal-store-closed">
                    <div class="modal-close arcticmodal-close">
                        <svg>
                            <use xlink:href="{{asset('assets/img/sprite.svg#close-modal')}}"></use>
                        </svg>
                    </div>
                    <div>{{ __('front.closed_store_modal_text', ['time' => @$openTime . ' - ' . @$closeTime]) }}</div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>