<section class="feedback-form">
    <div class="wrap">
        <div class="feedback-form__inner">
            <h3>{{__('front.subscribe_to_newsletter')}}</h3>
            <p>{{__('front.subscribe_first')}}</p>
            <form action="{{customUrl(['subscribe'])}}" id="feedbackForm">
                <div class="form-group">
                    <input type="text" placeholder="Email" name="email">
                    <div class="form-group__error-msg errorMsg">Error message</div>
                </div>
                <button type="submit" class="button submit-form-btn" data-form-id="feedbackForm"> {{__('front.subscribe')}} </button>
            </form>
        </div>
    </div>
    <div class="imgContainer">
        <div class="img">
            <img src="{{asset('assets/img/feedback-form.png')}}" alt="">
        </div>
    </div>
</section>