@extends('front.app')

@include('front.header')

@section('container')

    <div class="wrap">
        <h1 class="pageTitle">
            {{__("front.unsubscribe")}}
        </h1>

        <div class="text text-center">{{__("front.unsubscribe_msg")}}</div>
        <div class="text-center"><a href="{{customUrl()}}">{{__("front.home")}}</a></div>

    </div>

@stop
@include('front.footer')