@if(@$products && $products->isNotEmpty())
    @foreach($products as $product)
        @include('front.products.productItem', ['item' => $product, 'isWishListPage' => @$isWishListPage])
    @endforeach
{{--    <div class="paginationContainer">--}}
{{--        @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator && $products->total() > (int)$products->perPage())--}}
{{--            @include('front.templates.pagination', ['pagination' => $products, 'displayCount' => true])--}}
{{--        @endif--}}
{{--    </div>--}}
@else
    <div class="empty-list">
        <h2>{{__("front.empty_list")}}</h2>
        {{--TODO: count employee --}}
        {{--        <div class="emptyOrderHistoryContainer wishlist">--}}
        {{--        <div class="empty-container">--}}
        {{--            <div class="col">--}}
        {{--                <div class="empty-order">--}}
        {{--                    <img src="{{asset('assets/img/small-icons/empty-wishlist.svg')}}" alt="icon">--}}
        {{--                    <div class="empty-wishPage"></div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <div class="col c-left">--}}
        {{--                <h2>{{__("front.empty_list")}}</h2>--}}
        {{--                <div class="text">Aici e locul special unde adaugi produsele favorite</div>--}}
        {{--                <div class="custom-button">--}}
        {{--                    <a href="#">Home page</a>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--        </div>--}}
    </div>
@endif