<input type="hidden" name="categoryId" value="{{@$categoryId}}">

<div class="filterHeader">
    <span class="bold">Filtru</span>
    @if(!empty($filterParams) && count($filterParams) > 1)
        <button type="button" class="resetFilterBtn" data-action="reset">
            Reseteză
        </button>
    @endif
</div>

@if(@$usePriceRange && @$productsMinPrice && @$productsMaxPrice)
    <div class="filter-container">
        <div class="price-block">
            <div class="blockTitle">{{__("front.price")}}</div>
            <div class="gray">{!! $currCurrency->symbol ?? $currCurrency->name !!}</div>
        </div>
        <div class="filter-item price-range">
            <div class="filter-children">
                <div class="range" id="price-range"
                     data-min="{{$productsMinPrice}}"
                     data-max="{{$productsMaxPrice}}">
                </div>
                <div class="range-values">
                    <div class="range-min"><input type="text" autocomplete="off" id="price-from"
                                                  name="price-from"
                                                  value="{{!empty($filterParams) && array_key_exists("price-from", $filterParams) ? $filterParams["price-from"] : $productsMinPrice}}">
                    </div>
                    <div class="range-max"><input type="text" autocomplete="off" id="price-to"
                                                  name="price-to"
                                                  value="{{!empty($filterParams) && array_key_exists("price-to", $filterParams) ? $filterParams["price-to"] : $productsMaxPrice}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@if(@$attributes && !$attributes->isEmpty($attributes))
    @foreach($attributes as $attribute)
        @if(!$attribute->children->isEmpty())
            <div class="ingredients-filter">
                <div class="ingredients">{{@$attribute->globalName->name}}</div>
                <div class="sort-item">
                    <div class="filter" id="filter-catalog">
                        <div class="overlay"></div>
                        <div class="main">Selectează ingredientele</div>
                        <ul class="drop">
                            <li class="selectat">
                                            <span class="remove" id="filter-clear">
                                                 <svg class="icon">
                                                        <use xlink:href="{{asset('assets/img/sprite.svg#reset-filter')}}"></use>
                                                 </svg>
                                            </span>
                                <span>Selectat</span>
                                <span id="filter-selected">@if(array_key_exists("filter-{$attribute->slug}", $filterParams)) {{count($filterParams['filter-'.$attribute->slug])}} @endif</span>
                            </li>
                            @foreach($attribute->children as $child)
                                <li>
                                    <label>
                                        <input type="checkbox" id="filter-{{$child->slug}}"
                                               name="filter-{{$attribute->slug}}[]"
                                               value="{{$child->id}}"
                                               class="{{!empty($filterParams) && array_key_exists("filter-{$attribute->slug}", $filterParams) && in_array($child->id, $filterParams["filter-{$attribute->slug}"]) ? 'active' : ''}}" {{!empty($filterParams) && array_key_exists("filter-{$attribute->slug}", $filterParams) && in_array($child->id, $filterParams["filter-{$attribute->slug}"]) ? 'checked' : ''}}>
                                        <div class="check"></div>
                                        <span for="filter-{{$child->slug}}">{{@$child->globalName->name}}</span>
                                    </label>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@endif