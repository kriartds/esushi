@if(@$currComponent && ((@$productsMinPrice && @$productsMaxPrice) || @$attributes))
    <form method="POST" action="{{url(LANG, [$currComponent->slug, 'filter'])}}" class="filterList" id="filter-form">
        @include('front.components.catalog.filterListItems')
    </form>
@endif