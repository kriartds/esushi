@if(@$useSort || @$usePerPage)
    <form method="POST" action="{{url(LANG, [$currComponent->slug, 'filter'])}}" id="sort-form" class="sortList">
            {{--<input type="hidden" name="categoryId" value="{{@$categoryId}}">--}}
            @if(@$useSort)
            <div class="sort-container">
                <div class="sortTitle">{{__('front.sort')}}</div>
                <div class="sort-item default-sort">
                    <select class="select select2-container no-search" name="sort-items" id="select">
                        <option value="" {{!@$sortFilterParams['sort-items'] ? 'selected' : ''}}>{{__("front.default_sort")}}</option>
                        <option value="sort-by-popularity" {{@$sortFilterParams['sort-items'] == 'sort-by-popularity' ? 'selected' : ''}}>
                            {{__("front.sort_by_popularity")}}
                        </option>
                        <option value="sort-by-rating" {{@$sortFilterParams['sort-items'] == 'sort-by-rating' ? 'selected' : ''}}>
                            {{__("front.sort_by_rating")}}
                        </option>
                        <option value="sort-by-newest" {{@$sortFilterParams['sort-items'] == 'sort-by-newest' ? 'selected' : ''}}>
                            {{__("front.sort_by_newest")}}
                        </option>
                        <option value="sort-by-price-asc" {{@$sortFilterParams['sort-items'] == 'sort-by-price-asc' ? 'selected' : ''}}>
                            {{__("front.sort_by_price_asc")}}
                        </option>
                        <option value="sort-by-price-desc" {{@$sortFilterParams['sort-items'] == 'sort-by-price-desc' ? 'selected' : ''}}>
                            {{__("front.sort_by_price_desc")}}
                        </option>
                    </select>
                </div>
            @endif
            @if(@$usePerPage)
                <div class="sort-item">
                    <select class="select select2-container no-search perPage" name="sort-per-page">
                        <option value="" {{!@$sortFilterParams['sort-per-page'] ? 'selected' : ''}}>{{getSettingsField('site_items_per_page', $allSettings)}}</option>
                        <option value="24" {{@$sortFilterParams['sort-per-page'] == '24' ? 'selected' : ''}}>24</option>
                        <option value="36" {{@$sortFilterParams['sort-per-page'] == '36' ? 'selected' : ''}}>36</option>
                    </select>
                </div>
            @endif
                <div class="sort-item">
                    <input type="checkbox" autocomplete="off" name="sort-sales-products"
                        id="sort-sales-products" {{@$sortFilterParams['sort-sales-products'] == 'on' ? 'checked' : ''}}/>
                    <label for="sort-sales-products">
                        <div class="slider"></div>
                        <div>{{__("front.sales_products")}}</div>
                    </label>
                </div>
            </div>
    </form>

@endif