@if(@$sliderWideSlides && $sliderWideSlides->isNotEmpty())
    <div class="headerSlider">
        <div class="slider-for">
            @foreach($sliderWideSlides as $sliderWiderSlide)
                <div>
                    <div class="containerTextHeader">
                        <div class="imgContainer">
                            <img src="{{getItemFile($sliderWiderSlide, 'large')}}" alt="{{$sliderWiderSlide->slug}}" title="{{@$sliderWiderSlide->globalName->name}}">
                        </div>
                        <div class="informativCols">
                            <h1>{{@$sliderWiderSlide->globalName->name}}</h1>
                            <div class="text">{!! @$sliderWiderSlide->globalName->description !!}</div>
                            @if(@$sliderWiderSlide->globalName->link)
                                <a href="{{@$sliderWiderSlide->globalName->link}}" class="btn">{{__('front.order_now')}}</a>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
            <div class="slider-nav">
                @foreach($sliderWideSlides as $sliderWiderSlide)
                <div>
                    <div class="imgContainer smallImg">
                        <img src="{{getItemFile($sliderWiderSlide, 'small')}}" alt="{{$sliderWiderSlide->slug}}">
                    </div>
                </div>
                @endforeach
            </div>
    </div>
@endif