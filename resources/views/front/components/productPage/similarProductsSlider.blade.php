@if(@$similarProducts && $similarProducts->isNotEmpty())
    <section class="products-list">
        <h2>{{__("front.similar_products")}}</h2>
        @foreach($similarProducts as $similarProduct)
            @include('front.products.productItem', ['item' => $similarProduct, 'isSimilarOrRecommendedProduct' => true])
        @endforeach
    </section>
@endif