@if(@$recommendsProducts && $recommendsProducts->isNotEmpty())
    <div class="withThisProductItBuySliderContainer">
        <h1>{{__("front.with_product_buy")}}</h1>
        <div class="withThisProductItBuySlider">
            @foreach($recommendsProducts as $recommendsProduct)
                <div>
                    @include('front.products.productItem', ['item' => $recommendsProduct, 'isSimilarOrRecommendedProduct' => true])
                </div>
            @endforeach
        </div>
    </div>
@endif