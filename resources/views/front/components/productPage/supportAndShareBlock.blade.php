<div class="supportAndShareContainer">
    <div class="leftBlock">
        @if($supportPhones = getSettingsField('support_phones', $allSettings))
            <div class="title">{{__("front.any_questions")}}</div>
            {!! parseManyPhones($supportPhones, true, '') !!}
        @endif
    </div>
    @if($sharerItems = getSharerOptions($item))
        <div class="rightBlock">
            <div class="title">{{__("front.share")}}</div>
            {!! $sharerItems !!}
        </div>
    @endif
</div>