@if(@$recommendsProducts && $recommendsProducts->isNotEmpty())
    <section class="products-list">
        <h2>{{__("front.recommended_products")}}</h2>
        @foreach($recommendsProducts as $recommendsProduct)
            @include('front.products.productItem', ['item' => $recommendsProduct, 'isSimilarOrRecommendedProduct' => true])
        @endforeach
    </section>
@endif