<section class="productPageDetails">
    <div class="containerProduct">
        <div class="sliderContainer" id="product-gallery-block">
            <div class="mainSlider">
                @if($itemMediumFiles)
                    @foreach($itemMediumFiles as $file)
                        <div class="container-img">
                            <div class="imgContainer">
                                <img src="{{$file}}" alt="{{$item->slug}}" title="{{@$item->globalName->name}}" itemprop="image">
                            </div>
                        </div>
                    @endforeach
                @endif
                @if(isset($item->video) && !empty($item->video))
                        <div class="container-img">
                            <div class="imgContainer">
                                <iframe width="1237" height="696" src="{{@$item->video}}"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen>
                                </iframe>
                            </div>
                        </div>
                @endif
            </div>
            <div class="secondarySlider">
                @if($itemSmallFiles)
                    @foreach($itemSmallFiles as $file)
                        <div class="container-img">
                            <div class="imgContainer">
                                <img src="{{$file}}" alt="{{$item->slug}}" title="{{@$item->globalName->name}}">
                            </div>
                        </div>
                    @endforeach
                @endif
                    @if(isset($item->video) && !empty($item->video))
                        <div class="container-img">
                            <div class="imgContainer">
                                <iframe width="1237" height="696" src="{{@$item->video}}"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen>
                                </iframe>
                            </div>
                        </div>
                    @endif
            </div>
        </div>

        <div class="aboutProduct">
            <h1 class="nameProduct">{{@$item->globalName->name}}</h1>
            <div class="product__attr">
                {{@$item->globalName->weight}}
            </div>
            <div class="description">
                {!! @$item->globalName->description !!}
            </div>

            {!! makeProductVariations(@$item, $itemVariation, @$defaultValuesFromVariations ?: [], 'product_page', ['toppings' => $toppings]) !!}

            @if(config('cms.product.makeProductVariations.buyInOneClick', false))
                <div class="byOneClickProduct">
                    <h5>{{__('front.add_phone_to_buy_in_one_click')}}</h5>
                    <form class="form-group" action="{{customUrl(['buyInOneClick'])}}" id="buy-in-one-click">
                        <input type="hidden" name="item_id" value="{{@$item->id}}">
                        <div class="input-wrap">
                            <input type="text" name="client_phone" id="one-click-input" placeholder="{{__('front.phone_number')}}">
                        </div>
                        <button type="button" class=" button disable-button buy-in-one-click-btn" data-action="buyInOneClick" data-form-id="buy-in-one-click" id="one-click-send" disabled="disabled">
                            {{__('front.buy_in_one_click')}}
                        </button>
                    </form>
                </div>
            @endif

            @include('front.products.shareOnSocial')

        </div>

    </div>
</section>