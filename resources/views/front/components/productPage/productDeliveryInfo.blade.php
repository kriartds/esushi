@if(@$deliveryAndPayment)
    <div class="productDeliveryInfoContainer" id="product-delivery-info-block">
        <h4>{{__("front.delivery_payment")}}</h4>

            <div class="techDescription">
                <div class="title">{{__("front.delivery_payment_title_1")}}</div>
                <div class="text">{{__("front.delivery_payment_text_1")}}</div>
            </div>
            <div class="techDescription">
                <div class="title">{{__("front.delivery_payment_title_2")}}</div>
                <div class="text">{{__("front.delivery_payment_text_2")}}</div>
            </div>
            <div class="techDescription">
                <div class="title">{{__("front.delivery_payment_title_3")}}</div>
                <div class="text">{{__("front.delivery_payment_text_3")}}</div>
            </div>
            <div class="techDescription">
                <div class="title">{{__("front.delivery_payment_title_4")}}</div>
                <div class="text">{{__("front.delivery_payment_text_4")}} </div>
            </div>
            <div class="techDescription">
                <div class="title">{{__("front.delivery_payment_title_5")}}</div>
                <div class="text">{{__("front.delivery_payment_text_5")}}</div>
            </div>
    </div>
@endif