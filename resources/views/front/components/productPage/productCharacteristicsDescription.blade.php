@if(@$characteristicsAttributes && $characteristicsAttributes->isNotEmpty())
    <div class="productTechDescriptionContainer" id="product-characteristics-description-block">
        <h4>{{__("front.characteristics")}}</h4>
        @foreach($characteristicsAttributes as $characteristicsAttribute)
            <div class="techDescription">
                <div class="title">{{@$characteristicsAttribute['parent']->globalName->name}}</div>
                @if(@$characteristicsAttribute['children'] && $characteristicsAttribute['children']->isNotEmpty())
                    <div class="text">
                        @foreach($characteristicsAttribute['children'] as $key => $characteristicsAttributeChild)

                            @if(!is_null(@$characteristicsAttributeChild->range_value))

                                {{@$characteristicsAttributeChild->range_value}}{{$characteristicsAttribute['children']->count() > $key + 1 ? ', ' : ''}}
                            @else
                                {{@$characteristicsAttributeChild->globalName->name}}{{$characteristicsAttribute['children']->count() > $key + 1 ? ', ' : ''}}
                            @endif
                        @endforeach
                    </div>
                @endif
            </div>
        @endforeach
    </div>
@endif