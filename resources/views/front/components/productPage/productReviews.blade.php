<div class="productReviewsContainer" id="product-reviews-block">
    <h4>{{__("front.reviews")}} <span class="reviews">{{@$reviews && $reviews->total() > 0 ? $reviews->total() : ''}}</span></h4>

    <div class="load-more-content">
        @include('front.components.productPage.templates.productReviewsList', ['reviews' => $reviews, 'item' => $item])
    </div>
    @if($reviews->count() < $reviews->total())
        <div class="viewMoreReviewBtnContainer">
            <button type="button" class="custom-button load-more-btn" data-url="{{customUrl('load-reviews')}}"
                    data-current-page="{{$reviews->currentPage()}}" data-last-page="{{$reviews->lastPage()}}"
                    data-item-id="{{$item->id}}">
                {{__("front.show_more")}}
            </button>
        </div>
    @endif

    @if(!auth()->check())
        <div class="loginForWriteAReview">
            <div class="leftBlock">
                <div class="avatar"></div>
                <div class="text">{{__("front.login_to_write_review")}}</div>
            </div>
            <div class="rightBlock">
                @include('front.auth.templates.socialItems', ['showLoginModalBtn' => true])
                <a class="icon local getModal" href="#modal-loginauthor"></a>
            </div>
        </div>

    @else
        <form class="writeAReview" id="add-product-review-form" action="{{customUrl('add-product-review')}}">
            <input type="hidden" name="productId" value="{{$item->id}}">
            <div class="avatar">
                @if(auth()->check() && @file_exists(auth()->user()->avatar))
                    <img src="{{asset(auth()->user()->avatar)}}" alt="{{str_slug(auth()->user()->name)}}" title="{{auth()->user()->name}}">
                @endif
            </div>
            <div class="mainBlock">
                <div class="header">

                    <div class="name">{{auth()->user()->name}}</div>
                </div>
                <div class="ratingContainer" id="rating">
                    <div class='starrr'>
                        <div class="stars">
                            <label for="5" class=""> </label>
                            <div class="single_star">
                                <input type="checkbox" name="rating" id="5" value="5">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                                    <defs>
                                        <clipPath id="y4fxa">
                                            <path fill="#fff"
                                                  d="M19.241 6.23h-6.567L10.68.46A.704.704 0 0 0 10 0a.704.704 0 0 0-.679.459L7.326 6.23H.714A.706.706 0 0 0 0 6.923c0 .039.004.082.013.117.01.151.08.32.3.489l5.397 3.686-2.072 5.837a.682.682 0 0 0 .246.78c.13.09.25.168.402.168a.893.893 0 0 0 .446-.156L10 14.205l5.268 3.64c.125.086.299.155.446.155.152 0 .273-.074.398-.169a.674.674 0 0 0 .245-.779l-2.071-5.837 5.352-3.72.13-.109c.116-.121.232-.286.232-.463 0-.38-.366-.692-.759-.692z"/>
                                        </clipPath>
                                    </defs>
                                    <g>
                                        <g>
                                            <g/>
                                            <g>
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="50"
                                                      stroke-width="2.4"
                                                      d="M19.241 6.23v0h-6.567v0L10.68.46A.704.704 0 0 0 10 0a.704.704 0 0 0-.679.459L7.326 6.23v0H.714v0A.706.706 0 0 0 0 6.923c0 .039.004.082.013.117.01.151.08.32.3.489l5.397 3.686v0l-2.072 5.837a.682.682 0 0 0 .246.78c.13.09.25.168.402.168a.893.893 0 0 0 .446-.156L10 14.205v0l5.268 3.64c.125.086.299.155.446.155.152 0 .273-.074.398-.169a.674.674 0 0 0 .245-.779l-2.071-5.837v0l5.352-3.72v0l.13-.109c.116-.121.232-.286.232-.463 0-.38-.366-.692-.759-.692z"
                                                      clip-path="url(&quot;#y4fxa&quot;)"/>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <label for="4" class=""> </label>
                            <div class="single_star">
                                <input type="checkbox" name="rating" id="4" value="4">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                                    <defs>
                                        <clipPath id="y4fxa">
                                            <path fill="#fff"
                                                  d="M19.241 6.23h-6.567L10.68.46A.704.704 0 0 0 10 0a.704.704 0 0 0-.679.459L7.326 6.23H.714A.706.706 0 0 0 0 6.923c0 .039.004.082.013.117.01.151.08.32.3.489l5.397 3.686-2.072 5.837a.682.682 0 0 0 .246.78c.13.09.25.168.402.168a.893.893 0 0 0 .446-.156L10 14.205l5.268 3.64c.125.086.299.155.446.155.152 0 .273-.074.398-.169a.674.674 0 0 0 .245-.779l-2.071-5.837 5.352-3.72.13-.109c.116-.121.232-.286.232-.463 0-.38-.366-.692-.759-.692z"/>
                                        </clipPath>
                                    </defs>
                                    <g>
                                        <g>
                                            <g/>
                                            <g>
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="50"
                                                      stroke-width="2.4"
                                                      d="M19.241 6.23v0h-6.567v0L10.68.46A.704.704 0 0 0 10 0a.704.704 0 0 0-.679.459L7.326 6.23v0H.714v0A.706.706 0 0 0 0 6.923c0 .039.004.082.013.117.01.151.08.32.3.489l5.397 3.686v0l-2.072 5.837a.682.682 0 0 0 .246.78c.13.09.25.168.402.168a.893.893 0 0 0 .446-.156L10 14.205v0l5.268 3.64c.125.086.299.155.446.155.152 0 .273-.074.398-.169a.674.674 0 0 0 .245-.779l-2.071-5.837v0l5.352-3.72v0l.13-.109c.116-.121.232-.286.232-.463 0-.38-.366-.692-.759-.692z"
                                                      clip-path="url(&quot;#y4fxa&quot;)"/>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <label for="3" class=""> </label>
                            <div class="single_star">
                                <input type="checkbox" name="rating" id="3" value="3">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                                    <defs>
                                        <clipPath id="y4fxa">
                                            <path fill="#fff"
                                                  d="M19.241 6.23h-6.567L10.68.46A.704.704 0 0 0 10 0a.704.704 0 0 0-.679.459L7.326 6.23H.714A.706.706 0 0 0 0 6.923c0 .039.004.082.013.117.01.151.08.32.3.489l5.397 3.686-2.072 5.837a.682.682 0 0 0 .246.78c.13.09.25.168.402.168a.893.893 0 0 0 .446-.156L10 14.205l5.268 3.64c.125.086.299.155.446.155.152 0 .273-.074.398-.169a.674.674 0 0 0 .245-.779l-2.071-5.837 5.352-3.72.13-.109c.116-.121.232-.286.232-.463 0-.38-.366-.692-.759-.692z"/>
                                        </clipPath>
                                    </defs>
                                    <g>
                                        <g>
                                            <g/>
                                            <g>
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="50"
                                                      stroke-width="2.4"
                                                      d="M19.241 6.23v0h-6.567v0L10.68.46A.704.704 0 0 0 10 0a.704.704 0 0 0-.679.459L7.326 6.23v0H.714v0A.706.706 0 0 0 0 6.923c0 .039.004.082.013.117.01.151.08.32.3.489l5.397 3.686v0l-2.072 5.837a.682.682 0 0 0 .246.78c.13.09.25.168.402.168a.893.893 0 0 0 .446-.156L10 14.205v0l5.268 3.64c.125.086.299.155.446.155.152 0 .273-.074.398-.169a.674.674 0 0 0 .245-.779l-2.071-5.837v0l5.352-3.72v0l.13-.109c.116-.121.232-.286.232-.463 0-.38-.366-.692-.759-.692z"
                                                      clip-path="url(&quot;#y4fxa&quot;)"/>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <label for="2" class=""> </label>
                            <div class="single_star">
                                <input type="checkbox" name="rating" id="2" value="2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                                    <defs>
                                        <clipPath id="y4fxa">
                                            <path fill="#fff"
                                                  d="M19.241 6.23h-6.567L10.68.46A.704.704 0 0 0 10 0a.704.704 0 0 0-.679.459L7.326 6.23H.714A.706.706 0 0 0 0 6.923c0 .039.004.082.013.117.01.151.08.32.3.489l5.397 3.686-2.072 5.837a.682.682 0 0 0 .246.78c.13.09.25.168.402.168a.893.893 0 0 0 .446-.156L10 14.205l5.268 3.64c.125.086.299.155.446.155.152 0 .273-.074.398-.169a.674.674 0 0 0 .245-.779l-2.071-5.837 5.352-3.72.13-.109c.116-.121.232-.286.232-.463 0-.38-.366-.692-.759-.692z"/>
                                        </clipPath>
                                    </defs>
                                    <g>
                                        <g>
                                            <g/>
                                            <g>
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="50"
                                                      stroke-width="2.4"
                                                      d="M19.241 6.23v0h-6.567v0L10.68.46A.704.704 0 0 0 10 0a.704.704 0 0 0-.679.459L7.326 6.23v0H.714v0A.706.706 0 0 0 0 6.923c0 .039.004.082.013.117.01.151.08.32.3.489l5.397 3.686v0l-2.072 5.837a.682.682 0 0 0 .246.78c.13.09.25.168.402.168a.893.893 0 0 0 .446-.156L10 14.205v0l5.268 3.64c.125.086.299.155.446.155.152 0 .273-.074.398-.169a.674.674 0 0 0 .245-.779l-2.071-5.837v0l5.352-3.72v0l.13-.109c.116-.121.232-.286.232-.463 0-.38-.366-.692-.759-.692z"
                                                      clip-path="url(&quot;#y4fxa&quot;)"/>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <label for="1" class=""> </label>
                            <div class="single_star">
                                <input type="checkbox" name="rating" id="1" value="1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                                    <defs>
                                        <clipPath id="y4fxa">
                                            <path fill="#fff"
                                                  d="M19.241 6.23h-6.567L10.68.46A.704.704 0 0 0 10 0a.704.704 0 0 0-.679.459L7.326 6.23H.714A.706.706 0 0 0 0 6.923c0 .039.004.082.013.117.01.151.08.32.3.489l5.397 3.686-2.072 5.837a.682.682 0 0 0 .246.78c.13.09.25.168.402.168a.893.893 0 0 0 .446-.156L10 14.205l5.268 3.64c.125.086.299.155.446.155.152 0 .273-.074.398-.169a.674.674 0 0 0 .245-.779l-2.071-5.837 5.352-3.72.13-.109c.116-.121.232-.286.232-.463 0-.38-.366-.692-.759-.692z"/>
                                        </clipPath>
                                    </defs>
                                    <g>
                                        <g>
                                            <g/>
                                            <g>
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="50"
                                                      stroke-width="2.4"
                                                      d="M19.241 6.23v0h-6.567v0L10.68.46A.704.704 0 0 0 10 0a.704.704 0 0 0-.679.459L7.326 6.23v0H.714v0A.706.706 0 0 0 0 6.923c0 .039.004.082.013.117.01.151.08.32.3.489l5.397 3.686v0l-2.072 5.837a.682.682 0 0 0 .246.78c.13.09.25.168.402.168a.893.893 0 0 0 .446-.156L10 14.205v0l5.268 3.64c.125.086.299.155.446.155.152 0 .273-.074.398-.169a.674.674 0 0 0 .245-.779l-2.071-5.837v0l5.352-3.72v0l.13-.109c.116-.121.232-.286.232-.463 0-.38-.366-.692-.759-.692z"
                                                      clip-path="url(&quot;#y4fxa&quot;)"/>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                @if(config('cms.product.reviews.addAdvantages', true))
                    <div class="advantageContainer">
                        <label class="title" for="add-advantages-review">{{__("front.advantage")}}:</label>
                        <textarea name="advantage" id="add-advantages-review" rows="5"></textarea>
                    </div>
                @endif
                @if(config('cms.product.reviews.addDisadvantages', true))
                    <div class="disadvantageContainer">
                        <label class="title" for="add-disadvantages-review">{{__("front.disadvantage")}}:</label>
                        <textarea name="disadvantage" id="add-disadvantages-review" rows="5"></textarea>
                    </div>
                @endif
                <div class="commentContainer">
                    <label class="title" for="add-comment-review">{{__("front.comment")}}:</label>
                    <textarea name="message" id="add-comment-review" rows="5"></textarea>
                </div>
                <div class="buttonContainer">
                    <div class=""></div>
                    <button class="custom-button submit-form-btn" data-form-id="add-product-review-form">{{__("front.apply")}}</button>
                </div>
            </div>
        </form>
    @endif

</div>