<div class="aboutProductContainer" id="product-description-block">
    <h4  itemprop="name" data-clear-title="{{__("front.product_description_title", ['title' => @$item->globalName->name, 'attributes' => ''])}}">{!! @$defaultVariationDetails['description_title'] ?: __("front.product_description_title", ['title' => @$item->globalName->name, 'attributes' => '']) !!}</h4>
    <div id="short-default-description" class="short-description" itemprop="disambiguatingDescription">{{@$defaultVariationDetails['description'] ?: ''}}</div>
    <div class="overflowContainer" itemprop="description">
        {!! @$item->globalName->description !!}
    </div>
    <div class="showAllbtn">
        <div class="text">{{__("front.show_more")}}</div>
        <div class="text">{{__("front.show_less")}}</div>
    </div>
</div>


