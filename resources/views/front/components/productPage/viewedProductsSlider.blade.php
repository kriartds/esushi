@if(@$recentProducts && $recentProducts->isNotEmpty())
    <div class="viewedProductsSliderContainer">
        <h2>{{__("front.viewed_products")}}</h2>
        <div class="viewedProductsSlider">
            @foreach($recentProducts as $recentProduct)
                <div>
                    <a class="viewedProductCard"
                       href="{{customUrl(['products', $recentProduct->slug, (@$recentProduct->productPromotion && @$recentProduct->productPromotion['variationId'] ? $recentProduct->productPromotion['variationId'] : "")])}}">
                        <img src="{{getItemFile($recentProduct, 'medium')}}" alt="{{$recentProduct->slug}}" title="{{@$recentProduct->globalName->name}}"/>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endif