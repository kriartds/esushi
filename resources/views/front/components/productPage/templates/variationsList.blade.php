@if(@$product && @$product->finPrice)
    <form method="POST" action="{{url(LANG, 'addToCart')}}" class="add-to-cart-form" id="add-to-cart-form">
        <input type="hidden" name="item_id" value="{{$product->id}}" autocomplete="off">
        <input type="hidden" name="variation_id" autocomplete="off" value="{{@$defaultVariation->id}}">
        <input type="hidden" id="product_variations" name="variations" value="{{@$allProductVariations}}" autocomplete="off">
        <input type="hidden" id="product_details" value="{{json_encode(['price' => @$product->price, 'sale_price' => @$product->sale_price])}}">
        <input type="hidden" id="add_amount" value="0">

        @if(@$product->isVariationProduct)
            <input type="hidden" name="product_price" value="{{convertPrice(@$product->finPrice->minPrice, getCurrentCurrency())}}" autocomplete="off" id="product_price_input">
            <input type="hidden" name="product_price_total" value="{{convertPrice(@$product->finPrice->minPrice, getCurrentCurrency())}}" autocomplete="off" id="product_price_input_total">
            @if(@$attributes && $attributes->isNotEmpty())
                @foreach($attributes as $attribute)
                    @php
                        $parent = $attribute['parent'];
                        $children = $attribute['children'];
                    @endphp

                    <div class="attribute-item {{@$parent->type->slug == 'color' ? 'color-fields' : (@$parent->type->slug == 'image' ? 'image-fields' : (@$parent->type->slug == 'radio' ? 'radio-fields' : ''))}}">

{{--                            <div class="title">{{@$parent->globalName->name}}</div>--}}
                            <div class="attribute-item-container" data-item-slug="{{$parent->slug}}" data-item-type="{{@$parent->type->slug}}">
                                @switch(@$parent->type->slug)
                                    @case('radio')
                                        @if($children->isNotEmpty())
                                            <div class="product__serving">
                                                @foreach($children as $child)
                                                    <div class="radio-wrap">
                                                        <input type="radio" name="attributes-{{$parent->slug}}" autocomplete="off" id="attribute-{{$child->slug}}"
                                                               value="{{$child->id}}" {{in_array($child->id, @$defaultVariation->defaultVariationsAttrIds ?: []) ? 'checked' : ''}}>
                                                        <label for="attribute-{{$child->slug}}" title="{{@$child->globalName->name}}">
                                                            <span>{{@$child->globalName->name}}</span>
                                                        </label>
                                                    </div>
                                                @endforeach
                                                {{@$parent->globalName->name}}
                                            </div>
                                        @endif
                                    @break
                            @default
                            <div class="attribute-item-wrap">
                                <select class="select select2 no-search" name="attributes-{{$parent->slug}}" id="attributes-{{$parent->slug}}" autocomplete="off">
                                    <option value="">{{__('front.selectAttribute')}}</option>
                                    @if($children->isNotEmpty())
                                        @foreach($children as $child)
                                            <option value="{{$child->id}}" {{in_array($child->id, @$defaultVariation->defaultVariationsAttrIds ?: []) ? 'selected' : ''}}>{{@$parent->field_type == 'range' ? @$child->range_value : @$child->globalName->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            @endswitch
                        </div>
                    </div>
                @endforeach
            @endif
        @else
            <input type="hidden" name="product_price" value="{{convertPrice(@$product->sale_price ? $product->sale_price : $product->price, getCurrentCurrency())}}" autocomplete="off" id="product_price_input">
            <input type="hidden" name="product_price_total" value="{{convertPrice(@$product->sale_price ? $product->sale_price : $product->price, getCurrentCurrency())}}" autocomplete="off" id="product_price_input_total">
        @endif

        @if(isset($toppings) && $toppings->isNotEmpty())
            <div class="product__serving addTopping">
                <h5>{{__('front.add_toppings')}}</h5>
                <div class="toppingGroup">
                    @foreach($toppings as $topping)
                        <label>
                            <input class="toppings_checkbox" type="checkbox" name="toppings[]" value="{{$topping->id}}" data-price="{{convertPrice($topping->price, getCurrentCurrency())}}">
                            <div class="check"></div>
                            <span class="toppingName">{{$topping->globalName->name}} +{{formatPrice($topping->price, getCurrentCurrency())}}</span>
                        </label>
                    @endforeach
                </div>
            </div>
        @endif

        <div class="productPriceContainer product__price-container">
            <div class="priceCont {{@$product->isVariationProduct ? 'variation-price-block' : ''}}" id="price-product-block">
                {!! getProductPrices(@$product, $currCurrency, !is_null($defaultVariation), ['salePriceClass' => 'oldPrice'], false) !!}
            </div>

            <button type="button" class="button btn submit-add-to-cart-btn @if(!is_null($product->cartProduct)) hidden @endif" data-action="addToCart" data-form-id="add-to-cart-form">
                {{__('front.addToCart')}}
            </button>

            @include('front.products.productUpdateQty', ['cartProduct' => $product->cartProduct])

        </div>

    </form>
@endif