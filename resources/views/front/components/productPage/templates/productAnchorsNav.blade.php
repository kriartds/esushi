@if(config('cms.product.displayAnchorNav', true))
    <div class="anchorsNav">
        <div class="anchorLink scrolling" href="#product-description-block">{{__("front.product_description")}}</div>
        @if(@$characteristicsAttributes && $characteristicsAttributes->isNotEmpty())
            <div class="anchorLink scrolling" href="#product-characteristics-description-block">{{__("front.characteristics")}}</div>
        @endif
        @if(@$deliveryAndPayment)
            <div class="anchorLink scrolling" href="#product-delivery-info-block">{{__("front.delivery_payment")}}</div>
        @endif
        <div class="anchorLink scrolling" href="#product-reviews-block" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
            {{__("front.reviews")}}<span class="count" itemprop="reviewCount">{{@$reviews && $reviews->total() > 0 ? $reviews->total() : ''}}</span>
            <span class="hidden" itemprop="ratingValue">{{@$item->productReviews['avg'] ? $item->productReviews['avg'] : ''}}</span>
            <div class="hidden" itemprop="itemReviewed" itemscope itemtype="http://schema.org/Thing">
                <span itemprop="name">{{@$item->globalName->name}}</span>
            </div>
        </div>
    </div>
@endif