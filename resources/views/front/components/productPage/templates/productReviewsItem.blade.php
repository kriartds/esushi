<div class="productReviwCard" itemprop="review" itemscope itemtype="http://schema.org/Review">
    <div class="avatar">
        @if(@file_exists(@$review->user->avatar))
            <img src="{{asset(@$review->user->avatar)}}" alt="{{str_slug(@$review->user->name)}}"
                 title="{{@$review->user->name}}">
        @endif
    </div>
    <div class="mainBlock">
        <div class="header">
            <div class="name" itemprop="author">{{@$review->user->name ?: __("front.site_user")}}</div>
            <div class="date" itemprop="datePublished" content="{{carbon($review->created_at)->format('Y-m-d')}}">{{@$review->reviewAddedDate}}</div>
        </div>
        <div class="ratingContainer" itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating">
            <span class="hidden" itemprop="ratingValue">{{@$review->reviewRating['rating'] ? $review->reviewRating['rating'] : ''}}</span>
            <div class="stars-wrap"
                 title="{{@$item->productReviews['avg'] ? $item->productReviews['avg'] : ''}}">
                <svg class="stars empty-stars">
                    <use xlink:href="{{asset('assets/img/sprite.svg#empty-rating')}}"></use>
                </svg>
                <div class="full-stars-wrap"
                     style="{{@$item->productReviews['percentage'] ? "width:{$item->productReviews['percentage']}%;" : ''}}">
                    <svg class="stars full-stars">
                        <use xlink:href="{{asset('assets/img/sprite.svg#rating-filled')}}"></use>
                    </svg>
                </div>
            </div>
            <div class="number-percentage">
                {{@$item->productReviews['avg'] ? $item->productReviews['avg'] : ''}}
            </div>
        </div>

        @if(@$review->message->advantage)
            <div class="advantageContainer">
                <div class="title">{{__("front.advantage")}}:</div>
                <div class="text" itemprop="description">{{$review->message->advantage}}</div>
            </div>
        @endif
        @if(@$review->message->disadvantage)
            <div class="disadvantageContainer">
                <div class="title">{{__("front.disadvantage")}}:</div>
                <div class="text" itemprop="description">{{$review->message->disadvantage}}</div>
            </div>
        @endif
        @if(@$review->message->msg)
            <div class="commentContainer">
                <div class="title">{{__("front.comment")}}:</div>
                <div class="text" itemprop="description">{{$review->message->msg ?: ''}}</div>
            </div>
        @endif
    </div>
</div>