@if(@$product && @$product->finPrice)
    <form method="POST" action="{{url(LANG, 'addToCart')}}" class="add-to-cart-form" id="add-to-cart-form">
        <input type="hidden" name="item_id" value="{{$product->id}}" autocomplete="off">
        <input type="hidden" name="variation_id" autocomplete="off" value="{{@$defaultVariation->id}}">
        <input type="hidden" name="variations" value="{{@$allProductVariations}}" autocomplete="off">

        @if(@$product->isVariationProduct)
            @if(@$attributes && $attributes->isNotEmpty())
                @foreach($attributes as $attribute)
                    @php
                        $parent = $attribute['parent'];
                        $children = $attribute['children'];
                    @endphp

                    <div class="attribute-item {{@$parent->type->slug == 'color' ? 'color-fields' : (@$parent->type->slug == 'image' ? 'image-fields' : (@$parent->type->slug == 'radio' ? 'radio-fields' : ''))}}">

                            <div class="title">{{@$parent->globalName->name}}</div>
                            <div class="attribute-item-container" data-item-slug="{{$parent->slug}}"
                                 data-item-type="{{@$parent->type->slug}}">
                                @switch(@$parent->type->slug)
                                    @case('color')
                                    @if($children->isNotEmpty())
                                        @foreach($children as $child)
                                            @if(!empty($child->colors))
                                                <div class="attribute-item-wrap {{in_array($child->id, @$defaultVariation->defaultVariationsAttrIds ?: []) ? 'active' : ''}}">
                                                    <input type="radio" name="attributes-{{$parent->slug}}"
                                                           autocomplete="off"
                                                           id="attribute-{{$child->slug}}"
                                                           value="{{$child->id}}" {{in_array($child->id, @$defaultVariation->defaultVariationsAttrIds ?: []) ? 'checked' : ''}}>
                                                    <label for="attribute-{{$child->slug}}"
                                                           title="{{@$child->globalName->name}}">
                                                        @foreach($child->colors as $key => $color)
                                                            @if(count($child->colors) > 4)
                                                                <span class="color-attribute"
                                                                      style="background: {{$color}}; width: calc(100% / {{count($child->colors)}});"></span>
                                                            @elseif(count($child->colors) % 2 == 0)
                                                                <span class="color-attribute"
                                                                      style="background: {{$color}}; width: calc(100% / {{count($child->colors) / 2}});"></span>
                                                            @elseif(count($child->colors) == 3)
                                                                <span class="color-attribute"
                                                                      style="background: {{$color}}; width: calc(100% / {{$key < 2 ? 2 : 1}});"></span>
                                                            @else
                                                                <span class="color-attribute"
                                                                      style="background: {{$color}}; width: calc(100% / {{$key + 1}});"></span>
                                                            @endif
                                                        @endforeach
                                                    </label>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                    @break
                                    @case('image')
                                    @if($children->isNotEmpty())
                                        @foreach($children as $child)
                                            <div class="attribute-item-wrap {{in_array($child->id, @$defaultVariation->defaultVariationsAttrIds ?: []) ? 'active' : ''}}">
                                                <input type="radio" name="attributes-{{$parent->slug}}"
                                                       autocomplete="off"
                                                       id="attribute-{{$child->slug}}"
                                                       value="{{$child->id}}" {{in_array($child->id, @$defaultVariation->defaultVariationsAttrIds ?: []) ? 'checked' : ''}}>
                                                <label for="attribute-{{$child->slug}}"
                                                       title="{{@$child->globalName->name}}">
                                                    <img src="{{getItemFile($child, 'small')}}" alt="{{$child->slug}}"
                                                         title="{{@$child->globalName->name}}">
                                                </label>
                                            </div>
                                        @endforeach
                                    @endif

                            @break
                            @case('radio')
                            @if($children->isNotEmpty())
                                @foreach($children as $child)
                                    <div class="attribute-item-wrap {{in_array($child->id, @$defaultVariation->defaultVariationsAttrIds ?: []) ? 'active' : ''}}">
                                        <input type="radio" name="attributes-{{$parent->slug}}"
                                               autocomplete="off"
                                               id="attribute-{{$child->slug}}"
                                               value="{{$child->id}}" {{in_array($child->id, @$defaultVariation->defaultVariationsAttrIds ?: []) ? 'checked' : ''}}>
                                        <label for="attribute-{{$child->slug}}"
                                               title="{{@$child->globalName->name}}">
                                            <span>{{@$child->globalName->name}}</span>
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                            @break
                            @default
                            <div class="attribute-item-wrap">
                                <select class="select select2 no-search" name="attributes-{{$parent->slug}}"
                                        id="attributes-{{$parent->slug}}"
                                        autocomplete="off">
                                    <option value="">{{__('front.selectAttribute')}}</option>
                                    @if($children->isNotEmpty())
                                        @foreach($children as $child)
                                            <option value="{{$child->id}}" {{in_array($child->id, @$defaultVariation->defaultVariationsAttrIds ?: []) ? 'selected' : ''}}>{{@$parent->field_type == 'range' ? @$child->range_value : @$child->globalName->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            @endswitch
                        </div>
                    </div>
                @endforeach
            @endif
        @endif
        <div class="product-add-to-cart-info-wrap">
            <div class="product-price-block {{@$product->isVariationProduct ? 'variation-price-block' : ''}}"
                 id="price-product-block">
                {!! getProductPrices(@$product, $currCurrency, !is_null($defaultVariation), [
                    'priceTag' => 'span',
                    'salePriceClass' => 'sale-price',
                    'salePriceTag' => 'span'
                ], true) !!}
            </div>
            <div class="count-items-block {{@$product->isVariationProduct && !$defaultVariation || !$product->globalStock? 'hidden' : ''}}"
                 id="count-items-block">
                @if(@$product->isVariationProduct)
                    <div class="qty">
                            <span class="minus update-product-quantity" data-op="diff"
                                  data-id="{{@$product->id}}"></span>
                        <input type="text" class="update-product-quantity" id="count-items"
                               data-id="{{@$product->id}}" name="count-items"
                               autocomplete="off"
                               value="1" {{@$defaultVariation->finStock && is_numeric($defaultVariation->finStock) ? "data-max-value={$defaultVariation->finStock}" : ''}}>
                        <span class="plus update-product-quantity" data-op="sum" data-id="{{@$product->id}}"></span>
                    </div>
                @else
                    <div class="qty">
                            <span class="minus update-product-quantity" data-op="diff"
                                  data-id="{{@$product->id}}"></span>
                        <input type="text" class="update-product-quantity"
                               id="count-items" name="count-items"
                               autocomplete="off"
                               placeholder="1"
                               data-id="{{@$product->id}}"
                               value="1" {{@$product->finStock && is_numeric($product->finStock) ? "data-max-value={$product->finStock}" : ''}}>
                        <span class="plus update-product-quantity" data-op="sum" data-id="{{@$product->id}}"></span>
                    </div>
                @endif
            </div>
            <div class="add-to-cart-btn-section">
                <button type="button"
                        class="custom-button btn-add-to-cart submit-add-to-cart-btn btn {{@$product->isVariationProduct && !@$defaultVariation->finStock || !@$product->finStock ? 'disabled' : ''}}"
                        {{@$product->isVariationProduct && !@$defaultVariation->finStock || !@$product->finStock ? 'disabled' : ''}} data-action="addToCart"
                        data-form-id="add-to-cart-form">{{__('front.addToCart')}}
                </button>
            </div>
        </div>

        @if($buyInOneClick)
            <div class="buy-in-one-click-block">
                <div class="hide-block">
                <span class="text">Pentru a cumpăra într-un click întroduceți numărul de telefon</span>
                <div class="flex-element">
                    <div class="input-wrap">
                        <input class="number-phone input" type="text" name="client_phone"
                               placeholder="{{__('front.phone_number')}}" autocomplete="off"/>
                    </div>
                    <button class="btn-add-to-cart submit-add-to-cart-btn btn effect" data-action="buyInOneClick"
                            data-form-id="add-to-cart-form">{{__('front.buy_in_one_click')}}</button>
                </div>
                </div>

                <div class="show-block">
                    <div class="flex-element">
                        <div class="text">Comanda a fost expediată! Revenim cu un apel telefonic în timp de 5 minute.</div>
                        <div class="check-icon">
                        <svg class="icon">
                            <use xlink:href="{{asset('assets/img/sprite.svg#check2')}}"></use>
                        </svg>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </form>
@endif