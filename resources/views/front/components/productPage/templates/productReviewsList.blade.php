@if(@$reviews && $reviews->isNotEmpty())
    @foreach($reviews as $review)
        @include('front.components.productPage.templates.productReviewsItem', ['review' => $review, 'item' => $item])
    @endforeach
@endif