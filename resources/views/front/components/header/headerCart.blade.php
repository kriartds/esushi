<div class="cartContainerOnHeader">
    <div class="cartBtn ">
        <svg class="icon">
            <use xlink:href="{{asset('assets/img/sprite.svg#icon-bag')}}"></use>
        </svg>
        <div class="count @if(@$cartCount > 0) show @endif"
             id="cart-count">{{@$cartCount > 0 ? (@$cartCount < 10 ? $cartCount : '9+') : ''}}</div>
    </div>
    <div class="cartResultContainer">
        <div class="triangle"></div>

        <div id="cart-preview">
            @include('front.components.header.headerCartThumbnail', ['cartProducts' => $cartProducts, 'cartSubtotal' => $cartSubtotal])
        </div>

    </div>

</div>