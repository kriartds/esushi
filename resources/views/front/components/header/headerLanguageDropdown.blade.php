<?php
    $currentLang = $langList->where('slug', LANG)->first();
?>
@if(isset($mobile) && $mobile )
    <a class="dropdown__item current-lang">
        <div class="img-container">
            <img src="{{asset(@$currentLang->file->file->file)}}" alt="lang-{{LANG}}">
        </div>
        <span>{{$currentLang->name}}</span>
    </a>
    {!! getLanguagesList($langList, LANG_ID) !!}
@else
    <div class="dropdown">
        <div class="dropdown__btn">
            <div class="img-container">
                <img src="{{asset(@$currentLang->file->file->file)}}" alt="lang-{{LANG}}">
            </div>
            <span>{{$currentLang->name}}</span>
        </div>
        <div class="dropdown__menu">
            <a class="dropdown__item current-lang" href="http://localhost:81/en">
                <div class="img-container">
                    <img src="{{asset(@$currentLang->file->file->file)}}" alt="lang-{{LANG}}">
                </div>
                <span>{{$currentLang->name}}</span>
            </a>
            {!! getLanguagesList($langList, LANG_ID) !!}
        </div>
    </div>
@endif

