{{--@if(isset($cartProducts))--}}
{{--    @if($cartProducts->isNotEmpty())--}}
{{--        @foreach($cartProducts as $cartProduct)--}}
{{--            <div class="cartResult" data-item-id="{{$cartProduct->id}}">--}}
{{--                <div class="imgContainer">--}}
{{--                    <img src="{{@$cartProduct->customFile}}"--}}
{{--                         alt="{{@$cartProduct->product->slug}}"--}}
{{--                         title="{{@$cartProduct->product->globalName->name}}">--}}
{{--                </div>--}}
{{--                <div class="shortInfo">--}}
{{--                    <div class="titleContainer">--}}
{{--                        <a class="name"--}}
{{--                           href="{{customUrl(['products', @$cartProduct->product->slug])}}">{{@$cartProduct->product->globalName->name}}</a>--}}
{{--                        <div class="deleteBtn destroy-cart-product" data-item-id="{{$cartProduct->id}}"--}}
{{--                             data-cart-thumbnail="true">--}}
{{--                            <svg class="">--}}
{{--                                <use xlink:href="{{asset('assets/img/sprite.svg#icon-cart')}}"></use>--}}
{{--                            </svg>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="priceInfo">--}}
{{--                        <div class="priceContainer">--}}
{{--                            {!! getProductPrices(@$cartProduct, $currCurrency, true) !!}--}}
{{--                        </div>--}}
{{--                        <div class="qty">x{{$cartProduct->quantity}}</div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endforeach--}}

{{--        <div class="subTotalContainer">--}}
{{--            <div class="title">{{__("front.subtotal")}}:</div>--}}
{{--            <div class="price">{!! formatPrice($cartSubtotal, $currCurrency) !!}</div>--}}
{{--        </div>--}}
{{--        <div class="btn2">--}}
{{--            <a  href="{{customUrl('cart')}}">{{__("front.go_to_cart")}}</a>--}}
{{--        </div>--}}
{{--    @else--}}
{{--        <div class="emptyCart active">--}}
{{--            <div class="empty">--}}
{{--                <img src="{{asset('assets/img/small-icons/empty.svg')}}" alt="icon">--}}
{{--            </div>--}}
{{--            <div class="text">--}}
{{--                {{__("front.cart_empty")}}--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    @endif--}}
{{--@endif--}}

@if(isset($cartProducts) && $cartProducts->isNotEmpty())
    @foreach($cartProducts as $cartProduct)
        <div class="dropdown__item">
            <div class="product__card">

                <div class="product__img">
                    <img src="{{getItemFile($cartProduct->product, 'small')}}" alt="">
                </div>

                <div class="product__box">
                    <div class="product__name">
                        <a href="{{customUrl(['products', @$cartProduct->product->slug])}}">
                            {{@$cartProduct->product->globalName->name}}
                        </a>
                    </div>

                    <div class="product__attr">
                        {{@$cartProduct->product->globalName->weight}}
                    </div>

                    <div class="product__price-container">
                        <div class="product__price">{{formatPrice($cartProduct->amount, getCurrentCurrency())}}</div>
                        @include('front.products.productUpdateQty')
{{--                        <div class="product__counter">--}}
{{--                            <div class="minus">--}}
{{--                                <img src="{{asset('assets/img/svg-icons/minus.svg')}}" alt="">--}}
{{--                            </div>--}}
{{--                            <input type="text" value="{{$cartProduct->quantity}}" disabled>--}}
{{--                            <div class="plus">--}}
{{--                                <img src="{{asset('assets/img/svg-icons/plus.svg')}}" alt="">--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <div class="dropdown__item">
        <a class="button" id="viewCart" href="{{customUrl('cart')}}">{{__('front.go_to_cart')}}</a>
    </div>
@else
    {{--                                {# If cart is empty #}--}}

    <div class="header__empty-cart">
        <img src="{{asset('assets/img/svg-icons/empty-cart-img.svg')}}" alt="">
        <h5>{{__("front.cart_empty")}}</h5>
        {{--                                        <a  class="button" href="">Vezi meniul</a>--}}
    </div>
@endif