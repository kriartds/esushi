<div class="profileContainerOnHeader">
    <div class="profileBtn {{!auth()->check() ? 'getModal' : ''}}" {{!auth()->check() ? 'data-modal=#modal-login' : ''}}>
        @if(auth()->check() && @file_exists(auth()->user()->avatar))
            <img src="{{asset(auth()->user()->avatar)}}" alt="{{str_slug(auth()->user()->name)}}"
                 title="{{auth()->user()->name}}">
        @else
            <svg class="icon">
                <use xlink:href="{{asset('assets/img/sprite.svg#user-profile')}}"></use>
            </svg>
        @endif
    </div>
    @if(auth()->check())
        <div class="profileLinksContainer">
            <div class="triangle"></div>
            @if(auth()->user()->name)
                <div class="name">{{auth()->user()->name}}</div>
            @else
                <a href="{{customUrl(['dashboard', 'settings'])}}"
                   class="incomplete-data">{{__("front.complete_personal_data")}}</a>
            @endif
      </div>

        <div class="profileLinksContainer">
            <div class="triangle"></div>
            <div class="name">{{auth()->user()->name}}</div>
            <a class="settings profile-info" href="{{customUrl('dashboard')}}">{{__("front.dashboard")}}
                <div class="icon-profile">
                    <svg class="icon">
                        <use xlink:href="{{asset('assets/img/sprite.svg#dashboard')}}"></use>
                    </svg>
                </div>
            </a>
            <a class="settings profile-info"
               href="{{customUrl(['dashboard', 'settings'])}}">{{__("front.personal_data")}}
                <div class="icon-profile">
                    <svg class="icon">
                        <use xlink:href="{{asset('assets/img/sprite.svg#profile')}}"></use>
                    </svg>
                </div>
            </a>
            <a class="orderHistory profile-info"
               href="{{customUrl(['dashboard', 'orders-history'])}}">{{__("front.orders_history")}}
                <div class="icon-profile">
                    <svg class="icon">
                        <use xlink:href="{{asset('assets/img/sprite.svg#history')}}"></use>
                    </svg>
                </div>
            </a>
            <a class="logout profile-info" href="{{customUrl('logout')}}"> <span class="text">{{__("front.logout")}}</span>
                <div class="icon-profile log-out">
                    <svg class="icon">
                        <use xlink:href="{{asset('assets/img/sprite.svg#logout')}}"></use>
                    </svg>
                </div>
            </a>
        </div>
    @else
                @include('front.components.modals.authModals')
    @endif
</div>