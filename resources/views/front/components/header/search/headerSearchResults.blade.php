@if(isset($items))
    @if($items->isNotEmpty())
        @if($categories->isNotEmpty())
            <div class="header__search-result-categories">
                @foreach($categories as $category)
                    <a href="{{customUrl(['search', $category->slug]) . '?q=' . request()->get('q', null)}}">{{@$category->globalName->name}}
                        ({{$category->products_count}})</a>
                @endforeach
            </div>
        @endif
        <div class="header__result-products-list">
            @foreach($items as $item)
                <a class="product__card" href="{{customUrl(['products', $item->slug])}}">
                    <div class="product__img">
                        <img src="{{getItemFile($item, 'small')}}" alt="{{$item->slug}}"
                             title="{{@$item->globalName->name}}"/>
                    </div>
                    <div class="product__box">
                        <div class="product__name">{{@$item->globalName->name}}</div>
                        <div class="product__attr">
                            <span>285 g</span>|
                            <span>325 kcal</span>|
                            <span>4 bucăți</span>
                        </div>
                        <div class="product__price-container">
                            <div class="product__price"> {!! getProductPrices($item, $currCurrency, true) !!}</div>
                        </div>
                    </div>
                </a>
            @endforeach
            @if(@$displayCount < @$totalCount)
                <a class="button" href="{{@$searchUrl}}">{{__("front.see_all_results")}}</a>
            @endif
        </div>
    @else
        <div class="header__empty-search-result">
            <img src="{{asset('assets/img/svg-icons/empty-search-img.svg')}}" alt="">
            <h5>Nimic nu a fost găsit</h5>
        </div>
    @endif
@endif
