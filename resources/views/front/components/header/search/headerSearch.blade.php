<div class="searchBtnOnMobile">
    <svg class="icon">
        <use xlink:href="{{asset('assets/img/sprite.svg#lens')}}"></use>
    </svg>
</div>

<div class="searchContainerOnHeader">
    <div class="searchMask"></div>

    <form method="get" action="{{customUrl('search')}}" id="search-form" class="searchInput">
        <input type="text" placeholder="{{__("front.search")}}" name="q" autocomplete="off">
        <div class="input-group-btn">
            <button class="reset-search-input" type="reset"></button>
            <button type="submit" disabled>
                <svg>
                    <use xlink:href="{{asset('assets/img/sprite.svg#search-icon')}}"></use>
                </svg>
            </button>
        </div>

    </form>

    <div class="searchResultMainContainer" id="searchResult"></div>

</div>