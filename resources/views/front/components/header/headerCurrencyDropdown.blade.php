@if(@$currencies && $currencies->isNotEmpty())
    @if(isset($mobile) && $mobile)
        @foreach($currencies as $currency)
            <a class="dropdown__item @if($currency->id == $currCurrency->currency_id) current-lang @endif "
               data-item-id="{{$currency->id}}"
               title="{{$currency->symbol ? strtoupper($currency->name) : ' '}}">
               <span>{!! @$currency->symbol ?: strtoupper(@$currency->name) !!}</span>
            </a>
        @endforeach
    @else
        <div class="dropdown value">
            <div class="dropdown__btn">
                <span>{!! @$currCurrency->symbol ?: strtoupper(@$currCurrency->name) !!}</span>
            </div>
            <div class="dropdown__menu currency">
                @foreach($currencies as $currency)
                    <a class="dropdown__item @if($currency->id == $currCurrency->currency_id) current-lang @endif"
                       data-item-id="{{$currency->id}}"
                       title="{{$currency->symbol ? strtoupper($currency->name) : ' '}}">
                        <span>{!! @$currency->symbol ?: strtoupper(@$currency->name) !!}</span>
                    </a>
                @endforeach
            </div>
        </div>
    @endif
@endif