@if($headerMenu = getMenuRecursive(1, 3))
    @if($headerMenu->isNotEmpty())
        <nav class="nav">
            <!-- <a href="/" class="visible-only-mobile">Home</a> -->
            <a class="header__logo visible-only-mobile" href="{{customUrl()}}">{!! getLogo($allSettings) !!}</a>
            @foreach($headerMenu as $menu)
                @if(!empty($menu))
                    @if($menu->children->isNotEmpty())
                        <div class="categoryContainer  has-subcategories">
                            <div class="category">
                                {{$menu->currentName}}
                            </div>
                            <div class="subcategoryContainer">
                                <div class="backOneStep toMenu">
                                    <img src="{{asset('assets/img/svg-icons/carret-down.svg')}}" alt="arrow">
                                    {{$menu->currentName}}
                                </div>
                                @foreach($menu->children as $children)
                                    @if(@$children->children->isNotEmpty())
                                        <div class="subcategory has-subcategories @if($loop->first) first-item @endif  @if($loop->last) last-item @endif">
                                            <div class="title">
                                                {{$children->currentName}}
                                            </div>
                                            <div class="linksContainer">
                                                <div class="backOneStep toSubcategory">
                                                    <img src="{{asset('assets/img/svg-icons/carret-down.svg')}}" alt="">
                                                    {{$children->currentName}}
                                                </div>

                                                @foreach($children->children as $item)
                                                    <a href="{{@$item->currentUrl}}">{{@$item->currentName}}</a>
                                                @endforeach
                                            </div>
                                        </div>
                                    @else
                                        <div class="subcategory @if($loop->first) first-item @endif  @if($loop->last) last-item @endif">
                                            <a class="title" href="{{@$children->currentUrl}}">{{@$children->currentName}}</a>
                                        </div>
                                    @endif

                                @endforeach
                            </div>
                        </div>
                    @else
                        <a href="{{$menu->currentUrl}}">{{$menu->currentName}}</a>
                    @endif
                @endif
            @endforeach
        </nav>
    @endif
@endif
