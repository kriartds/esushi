@php $passed = 0; @endphp
<div class="sms-form">

    @if($smsCountInDay >= env('SMS_COUNT_IN_DAY'))
        <p style="color: #F00">
            За последние 24 часа вы запросили код {{$smsCountInDay}} раз. <br>Если код не пришел пожалуйста свяжитесь с менеджером.
        </p>
    @else
        @if(!empty($smsLast))
            @php
                $passed = time()-$smsLast->created_at;
                $sms_id = $smsLast->id;
            @endphp
        @endif

        @if(empty($smsLast))
            @php
                $sms_id = $model->id;
            @endphp
        @endif

        <p id="new_sms_code" style="display: {{ $passed < env('SMS_TIMEOUT') ? '':'none' }}">
            @if(!empty($codeerror))
                <span style="color: #f00">не правильный код</span><br>отправили еще один код<br>
            @endif
            если этот код не придет, новый код можете получить через
            <span class="time" data-second="{{env('SMS_TIMEOUT') - $passed}}">{{env('SMS_TIMEOUT') - $passed}}</span> сек.
        </p>
        @if($passed > env('SMS_TIMEOUT') && $newcode == 0)
            мы ранее отправили код, если вы не получили отправьте запрос для получения нового кода
        @endif

        <div>
            <input id="sms_id" name="sms_id" class="" type="hidden" value="{{$sms_id}}">
            <input id="sms_code" name="sms_code" class="" type="text" value="" placeholder="СМС Код подтверждения">
        </div>
        <div style="text-align: center; padding: 8px 0px">
            <a href="javascript:;" class="btn btn-sm btn-success" id="btnSmsConfirm">Подтвердить</a>
            <a href="javascript:;" class="btn btn-sm btn-info" id='btnSmsNewCode' style="display: {{$passed > env('SMS_TIMEOUT') ? '':'none'}} ">Новый код</a>
        </div>
    @endif
</div>


@if (1==2 && $smsCountInDay < env('SMS_COUNT_IN_DAY') && $passed < env('SMS_TIMEOUT'))
<script>
    Sms.initSmsCode();
</script>
@endif

