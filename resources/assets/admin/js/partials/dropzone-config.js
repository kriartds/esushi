//Gallery

let galleryLoaderGif = $("#gallery-loader-gif");

let extensions = ['3ds', 'aac', 'ai', 'avi', 'mp4', 'bmp', 'cad', 'cdr', 'css', 'dat', 'dll', 'dmg', 'doc', 'eps', 'fla', 'flv', 'gif', 'html', 'indd', 'iso', 'jpg', 'js', 'midi', 'mov', 'mp3', 'mpg', 'pdf', 'php', 'png', 'ppt', 'ps', 'psd', 'raw', 'sql', 'svg', 'tif', 'txt', 'wmv', 'xls', 'xml', 'zip', 'mp4'];
let currentUploadBtn;

Dropzone.options.galleryFileUpload = {
    paramName: "file",
    filesizeBase: 1024,
    maxFilesize: 64,
    maxThumbnailFilesize: 64,
    parallelUploads: 2,
    uploadMultiple: true,
    // acceptedFiles: 'image/*, application/*, audio/*, video/*, text/*',
    acceptedFiles: 'image/gif,.ico,.jpg,.jpeg,.png,.svg, application/doc,.docx,.pdf,.ppt,.pptx,.rar,.xls,.xlsx,.zip,.7z, audio/mp3,.wav,.midi, video/avi,.mkv,.flv,.mov,.moov,.mpg,.vdo,.mp4, text/txt,.xml',
    headers: {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')},
    addRemoveLinks: true,
    dictDefaultMessage: "Drop files here or click to upload",
    dictFallbackMessage: "Your browser does not support drag'n'drop file uploads.",
    dictFileTooBig: "File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.",
    dictInvalidFileType: "You can't upload files of this type.",
    dictResponseError: "Server responded with {{statusCode}} code.",
    dictCancelUpload: "Cancel upload",
    dictCancelUploadConfirmation: "Are you sure you want to cancel this upload?",
    dictRemoveFileConfirmation: "Are you sure you want to delete this file?",
    dictRemoveFile: "Remove file",
    dictMaxFilesExceeded: "You can't upload any more files.",
    previewTemplate: document.querySelector('#preview-template') !== null ? document.querySelector('#preview-template').innerHTML : '',

    init: function () {
        let dropzone = this,
            openGalleryBtn = $('.open-gallery-modal'),
            firstClickedPreviewIndex, lastClickedPreviewIndex,
            textEditorId = openGalleryBtn.data('editor'),
            textEditor = $('.container .form-block form').find('#' + textEditorId),
            galleryInsertBtn = $('#gallery-insert-files'),
            filterGalleryForm = $('#filter-gallery-files'),
            timeoutReference;

        $(document).on('click', '.open-gallery-modal', function () {
            let _this = $(this),
                componentId = _this.data('component-id') !== undefined ? _this.data('component-id') : null,
                types = _this.data('types') !== undefined ? _this.data('types') : null,
                params = {
                    page: 1
                };

            currentUploadBtn = _this;

            if (componentId !== null)
                params.componentId = componentId;

            if (types !== null) {
                let acceptedFiles = dropzone.options.acceptedFiles;
                let acceptedFilesArr = acceptedFiles.split(', ');
                let selectedType;

                if (acceptedFilesArr.length > 0)
                    selectedType = acceptedFilesArr.find((value) => {
                        return value.includes(types)
                    });

                $(dropzone.hiddenFileInput).attr('accept', selectedType);
                params.types = types;
            }

            if (textEditor.length > 0)
                galleryInsertBtn.text('Insert into ' + galleryInsertBtn.data('component-name').toLowerCase());
            else
                galleryInsertBtn.text('Set ' + galleryInsertBtn.data('component-name').toLowerCase() + ' images');

            if (_this.data('hidden-filed-name').startsWith('item_variations[variation_files]'))
                galleryInsertBtn.data('isVariationsGallery', true);

            // Filter

            if (types !== null) {
                filterGalleryForm.find('#gallery-file-type option').prop('selected', false).trigger('change');
                filterGalleryForm.find('#gallery-file-type option[value="' + types + '"]').prop('selected', true).trigger('change');
            }

            if (componentId !== null) {
                filterGalleryForm.find('#gallery-component option').prop('selected', false).trigger('change');
                filterGalleryForm.find('#gallery-component option[value="' + componentId + '"]').prop('selected', true).trigger('change');
            }

            // Filter

            getGalleryFiles(dropzone, params, galleryInsertBtn);
        });

        $(document).on('click', '#gallery-insert-files', function () {

                let attachmentForm = $('#attachment-file-settings'),
                    allSelectedFilesSizeField = $('#all-selected-files-sizes');

                if ($(dropzone.previewsContainer).find('.dz-preview.active').length < 1)
                    return false;

                if (allSelectedFilesSizeField.length < 1 || allSelectedFilesSizeField.val() === '')
                    return false;


                let value = '',
                    oldValue,
                    linkTo = attachmentForm.find('#link-to').val(),
                    customLinkVal = $('input#custom-link').val(),
                    allSizesObj = JSON.parse(allSelectedFilesSizeField.val()),
                    selectedFilesId = '';

                if (getObjectSize(allSizesObj) > 0) {
                    for (let index in allSizesObj) {
                        let fileItem = allSizesObj[index],
                            selectedSizeKey = attachmentForm.find('#size option:selected').data('key'),
                            selectedSize = fileItem.fileLocations[selectedSizeKey];

                        if (textEditor.length) {

                            if (fileItem.itemType === 'image') {
                                if (selectedSize !== undefined) {
                                    let imgHtml = `<img src="${window.location.origin}/${selectedSize}" alt="${fileItem.slug || ''}" title="${fileItem.cleanName || ''}">`;

                                    if (linkTo === 'custom_link' && customLinkVal !== '' && customLinkVal !== undefined)
                                        value += `<a href="${customLinkVal}">${imgHtml}</a>`;
                                    else if (linkTo === 'media_file')
                                        value += `<a href="${attachmentForm.find('#size option[data-is-original="true"]').attr('value')}">${imgHtml}</a>`;
                                    else
                                        value += imgHtml;

                                }
                            } else {
                                value += `<a href="${fileItem.url}">${fileItem.name}</a>`;
                            }
                        } else {

                            // Personal Thumbnail

                            let newFileBlock = `<div class='img-block' style='background-image: url("${fileItem.smallFile || ''}");' data-file-id="${fileItem.fileId}"><input type="hidden" name="${currentUploadBtn.data('hidden-filed-name') ? currentUploadBtn.data('hidden-filed-name') : 'files'}[]" value="${fileItem.fileId}"><span class='file-name tooltip' title="Make double click and copy file link to clipboard" data-file-url="${fileItem.url || ''}"></span><span class='remove-upload-file' data-file-id="${fileItem.fileId}" data-is-attachment></span></div>`;

                            if (currentUploadBtn.siblings('.files-list').find('.img-block').length > 0 && currentUploadBtn.data('single-upload') !== true)
                                currentUploadBtn.siblings('.files-list').find('.img-block').last().after(newFileBlock);
                            else {
                                currentUploadBtn.siblings('.files-list').addClass('not-empty').html(newFileBlock);
                            }

                            currentUploadBtn.siblings('.files-list').find('.img-block').arrangeable();

                            if (currentUploadBtn.data('menu-field') !== undefined) {
                                let selectedFilesSerialized = currentUploadBtn.next('.files-list').find('input[type="hidden"][name="files[]"]').serializeArray();
                                if (selectedFilesSerialized.length)
                                    selectedFilesId = selectedFilesSerialized[0].value;
                            }

                            // Personal Thumbnail

                        }
                    }

                    if (currentUploadBtn.data('menu-field') !== undefined)
                        currentUploadBtn.parents('.item-settings').parent('.sortable-menu-item').data(currentUploadBtn.data('menu-field'), selectedFilesId);
                }

                if (textEditor.length > 0) {
                    if (value !== '') {
                        if (textEditor.length > 0) {
                            if (textEditor.data('type') === 'ckeditor') {
                                oldValue = CKEDITOR.instances[textEditorId].getData();
                                CKEDITOR.instances[textEditorId].setData(oldValue + value);
                            } else {
                                oldValue = textEditor.val();
                                textEditor.val(oldValue + value);
                            }
                        }
                    }
                } else {
                    tippy('.tooltip', tippyOptions);
                }

                $('#modal-gallery').arcticmodal('close');
            }
        );

        $(document).on('change', '#attachment-file-settings #link-to', function () {
            let customLinkBlock = $('#custom-link-block');
            $(this).val() === 'custom_link' ? customLinkBlock.fadeIn() : customLinkBlock.fadeOut();
        });

        // Filter

        if (filterGalleryForm.length > 0) {
            filterGalleryForm.on('submit', function (e) {
                e.preventDefault();
            });

            filterGalleryForm.find('select').on('select2:select', function () {
                if ($('#gallery-file-type') === undefined || $('#gallery-component') === undefined || $('#search_file') === undefined)
                    return false;

                let filesType = $('#gallery-file-type').val();

                if (filesType !== '' && filesType !== undefined)
                    $(dropzone.hiddenFileInput).attr('accept', filesType + '/*');
                else
                    $(dropzone.hiddenFileInput).attr('accept', '');


                let params = {
                    page: 1,
                    componentId: $('#gallery-component').val(),
                    types: filesType,
                    search: $('#search_file').val()
                };

                getGalleryFiles(dropzone, params, galleryInsertBtn);
            });

            //    Ajax load files with scroll

            $('#gallery-file-upload').on('scroll', function () {

                if ($('#gallery-file-upload').height() === $('#gallery-file-upload').get(0).scrollHeight - $('#gallery-file-upload').scrollTop()) {
                    let filesType = $('#gallery-file-type').val();

                    if (filesType !== '' && filesType !== undefined)
                        $(dropzone.hiddenFileInput).attr('accept', filesType + '/*');
                    else
                        $(dropzone.hiddenFileInput).attr('accept', '');

                    let params = {
                        page: 1,
                        componentId: $('#gallery-component').val(),
                        types: filesType,
                        search: $('#search_file').val()
                    };

                    pageCountUpdate(dropzone, params, galleryInsertBtn);
                }
            });

            //    Ajax load files with scroll

            filterGalleryForm.find('input#search_file').on('keyup', function (e) {
                let timeout = 500;
                let key = e.which || e.keyCode;
                let acceptedCharacters = (
                    key >= 48 && key <= 57 // numbers
                    || key >= 65 && key <= 90 // letters
                    || key >= 93 && key <= 111 // numpad
                    || key === 8 // backspace
                    || key === 46 // delete
                    || key === 13 // enter
                    || key === 32 // space
                );
                let params = {
                    componentId: $('#gallery-component').val(),
                    types: $('#gallery-file-type').val(),
                    search: $('#search_file').val()
                };

                if (e.keyCode === 13 && $(this).val() === '')
                    return false;

                if (timeoutReference) clearTimeout(timeoutReference);

                if (acceptedCharacters)
                    timeoutReference = setTimeout(function () {
                        getGalleryFiles(dropzone, params, galleryInsertBtn);
                    }, timeout);
            });
        }

        // Filter

        this.on("addedfile", function (file) {


            let fileIconsPath = getIconFromFilename(file),
                filePreviewElement = $(file.previewElement);

            filePreviewElement.find(".dz-image img").attr("src", fileIconsPath);

            if (file.status === 'added' && $(dropzone.previewsContainer).find('.dz-preview').length > 0) {
                let firstPreviewBlock = $(dropzone.previewsContainer).find('.dz-preview:first');
                filePreviewElement.insertBefore(firstPreviewBlock, null);
            }

            if (!file.status) {
                filePreviewElement.data('file-id', file.fileId).data('file-info', file);

                let activateUploadFile = filePreviewElement.find('.active-upload-file');
                activateUploadFile.data('active', file.active).data('fileId', file.fileId).data('active', parseInt(file.active)).removeClass('active').addClass(parseInt(file.active) === 1 ? 'active' : '');

                fileAddEventListener(filePreviewElement, file, dropzone, textEditor, galleryInsertBtn);
            }


        });

        this.on("sending", function (file, xhr, formData) {
            formData.append('componentId', $('#gallery-component').val());
        });

        this.on("successmultiple", function (files, xhr) {
            let errorFiles = [];

            totalUploadProgress(this.files);

            if (xhr.status && xhr.files) {
                xhr.files.map((file, index) => {
                    if (file !== null) {
                        let filePreviewElement = $(files[index].previewElement),
                            activateUploadFile = filePreviewElement.find('.active-upload-file');

                        files[index].fileId = file.fileId;
                        files[index].uploadedToServer = true;

                        filePreviewElement.data('file-id', file.fileId).data('file-info', file).addClass('isNew');
                        activateUploadFile.data('active', file.active).data('fileId', file.fileId).data('active', parseInt(file.active)).toggleClass('active', parseInt(file.active) === 1);

                        fileAddEventListener(filePreviewElement, file, dropzone, textEditor, galleryInsertBtn);


                    } else
                        errorFiles.push(files[key]);
                });
            }

            if (errorFiles.length > 0)
                dropzone.emit("errormultiple", errorFiles, xhr.error.msg.e);

            if (xhr.msg)
                toastr[xhr.msg.type](xhr.msg.e);

            if (xhr.error.msg)
                toastr[xhr.error.msg.type](xhr.error.msg.e);

        });

        this.on("removedfile", function (file) {
            if (file.fileId)
                destroyFile(dropzone, file, galleryInsertBtn);
        });

        this.on('errormultiple', function (files, response) {

            if (response.status)
                files.map((file) => {
                    dropzone.removeFile(file);
                    toastr.warning([response]);

                    if (typeof response === 'string')
                        $(file.previewElement).find('.dz-error-message').text(response);
                });
            else {
                if (response.msg)
                    toastr[response.msg.type](response.msg.e);
                else if (response.message)
                    toastr.error(response.message);
                else
                    toastr.error('Fail to send data');
            }

        });
    }
};

function pageCountUpdate(dropzone, params, galleryInsertBtn) {
    let page = 1;
    let max_page = 0;
    let form = $('#gallery-file-upload');

    if (form.data('currPage') !== undefined && form.data('currPage') !== null && form.data('lastPage') !== undefined && form.data('lastPage') !== null) {
        page = parseInt(form.data('currPage'));
        max_page = parseInt(form.data('lastPage'));
        if (page < max_page) {
            form.data('currPage', page + 1);
            params.page = form.data('currPage');
            getGalleryFiles(dropzone, params, galleryInsertBtn, true);
        }
    }
}

function getGalleryFiles(dropzone, params, galleryInsertBtn, loadMore) {
    if (!loadMore) {
        $('#modal-gallery').find('.gallery-right-sidebar .file-content').addClass('hidden');
        galleryInsertBtn.prop('disabled', true);

        $(dropzone.previewsContainer).find('.dz-preview').remove();
        dropzone.emit("reset");
    }

    $.ajax({
        method: "POST",
        url: `${window.location.origin}/${globalLang}/admin/getGallery`,
        beforeSend: function () {
            galleryLoaderGif.fadeIn()
        },
        data: params
    })
        .success(function (response) {
            galleryLoaderGif.fadeOut();

            if (response.status) {
                let mockFiles = response.files,
                    dropzoneFiles = dropzone.files;

                if (mockFiles.length > 0) {
                    mockFiles.map((mockFile) => {

                        dropzoneFiles.push(mockFile);
                        dropzone.emit('addedfile', mockFile);

                        if (mockFile.type.indexOf('image') !== -1 && mockFile.type !== 'image/svg+xml')
                            dropzone.createThumbnailFromUrl(mockFile, mockFile.smallFile);
                        else if (mockFile.type.indexOf('image') !== -1 && mockFile.type === 'image/svg+xml')
                            dropzone.emit("thumbnail", mockFile, mockFile.smallFile);
                        else
                            dropzone.emit("thumbnail", mockFile, getIconFromFilename(mockFile));

                        dropzone.emit('complete', mockFile);
                        dropzone.emit('success', mockFile);
                    });
                }

                $('#gallery-file-upload').data('currPage', response.currPage).data('lastPage', response.lastPage)
            }
        })
        .fail(function (response) {
            galleryLoaderGif.fadeOut();

            if (response.responseJSON) {
                if (response.responseJSON.msg)
                    toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
            } else
                toastr.error('Fail to send data');
        });
}

function getFileContentView(dropzone, file, isVisible, textEditor, allSelectedFilesInfo, galleryInsertBtn) {
    galleryInsertBtn.prop('disabled', !allSelectedFilesInfo.ids.length);
    $('.gallery-right-sidebar .attachment-info').toggleClass('hidden', !textEditor.length);

    clearAddFileInfoContent(file, allSelectedFilesInfo, !isVisible);
}

function destroyFile(dropzone, file, galleryInsertBtn) {
    let sidebarFileId = $('#sidebarFileId');

    $.ajax({
        url: `${window.location.origin}/${globalLang}/admin/destroyFile`,
        method: 'post',
        data: {
            fileId: file.fileId
        }
    })
        .success(function (response) {
            if (response.msg)
                toastr[response.msg.type](response.msg.e);

            if (response.status) {
                if (parseInt(sidebarFileId.val()) === file.fileId)
                    sidebarFileId.parent('.file-content').addClass('hidden');

                galleryInsertBtn.prop('disabled', $(file.previewElement).hasClass('active'));
            }
        })
        .fail(function (response) {
            if (response.responseJSON) {
                if (response.responseJSON.msg)
                    toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
            } else
                toastr.error('Fail to send data');
        });

    if ($(dropzone.previewsContainer).find('.dz-preview').length === 0)
        dropzone.emit("reset");
}

function getIconFromFilename(file) {

    let ext = file.name.split('.').pop().toLowerCase();

    let fileIconsPath = "/admin-assets/img/file-icons/";

    if (extensions.indexOf(ext) !== -1 && ext !== 'svg') {

        let http = new XMLHttpRequest();
        http.open('HEAD', fileIconsPath + ext + ".svg", false);
        http.send();

        if (http.status !== 200)
            fileIconsPath = fileIconsPath + "file.svg";
        else
            fileIconsPath = fileIconsPath + ext + ".svg";
    } else if (extensions.indexOf(ext) !== -1 && ext === 'svg')
        fileIconsPath = fileIconsPath + "svg.svg";
    else
        fileIconsPath = fileIconsPath + "file.svg";

    return fileIconsPath;
}

function clearAddFileInfoContent(fileContent, allSelectedFilesInfo, clearAll = false) {
    let galleryRightSidebarContent = $('#modal-gallery').find('.gallery-right-sidebar .file-content'),
        fileInfoBlock = galleryRightSidebarContent.find('.file-info'),
        editFileBlock = galleryRightSidebarContent.find('.edit-file'),
        attachmentInfoBlock = galleryRightSidebarContent.find('.attachment-info'),
        allSelectedFilesField = $('#all-selected-files'),
        allSelectedFilesSizeField = $('#all-selected-files-sizes'),
        data = {
            fileId: fileContent.fileId,
            fileImg: {
                src: fileContent.smallFile,
                alt: fileContent.slug,
                title: fileContent.cleanName
            },
            name: fileContent.name,
            cleanName: fileContent.cleanName,
            createdAt: fileContent.createdAt,
            size: fileContent.itemSize,
            resolution: fileContent.resolution,
            fullUrl: fileContent.url,
            type: fileContent.itemType,
            files: fileContent.fileLocations,
        };

    if (clearAll) {
        data = {};

        if (allSelectedFilesField.length > 0) {
            let newValue = unsetByValueArray(JSON.parse(allSelectedFilesField.val()), data.fileId, true);
            allSelectedFilesField.val(newValue.length > 0 ? JSON.stringify(newValue) : '');
        }

        if (allSelectedFilesSizeField.length > 0) {
            let newValue = unsetByValueArray(JSON.parse(allSelectedFilesSizeField.val()), data.fileId);
            allSelectedFilesSizeField.val(getObjectSize(newValue) > 0 ? JSON.stringify(newValue) : '');
        }
    } else {
        if (allSelectedFilesField.length > 0)
            allSelectedFilesField.val(JSON.stringify(allSelectedFilesInfo.ids));

        if (allSelectedFilesSizeField.length > 0)
            allSelectedFilesSizeField.val(JSON.stringify(allSelectedFilesInfo.items));
    }

    if (!galleryRightSidebarContent.length || !fileInfoBlock.length || !editFileBlock.length || !attachmentInfoBlock.length)
        return false;

    galleryRightSidebarContent.toggleClass('hidden', clearAll);

    galleryRightSidebarContent.find('#sidebarFileId').val(data.fileId || '');

    fileInfoBlock.find('.img a').attr('href', data.fullUrl ? data.fullUrl : (data.fileImg && data.fileImg.src ? data.fileImg.src : ''));
    fileInfoBlock.find('.img img').attr('src', data.fileImg && data.fileImg.src ? data.fileImg.src : '').attr('alt', data.fileImg && data.fileImg.alt ? data.fileImg.alt : '').attr('title', data.fileImg && data.fileImg.title ? data.fileImg.title : '');
    fileInfoBlock.find('.file-id').text(`#${data.fileId}` || '');
    fileInfoBlock.find('.name').text(data.name || '');
    fileInfoBlock.find('.date').text(data.createdAt || '');
    fileInfoBlock.find('.size').text(data.size || '');
    fileInfoBlock.find('.resolution').text(data.resolution && data.resolution.width && data.resolution.height ? `${data.resolution.width}x${data.resolution.height}` : '');
    fileInfoBlock.find('.url input#clipboard-file-url').val(data.fullUrl || '');

    editFileBlock.find('.form-block').toggleClass('hidden', data.type !== 'image');
    editFileBlock.find('.form-block').find('#title').val(data.cleanName || '');


    if (attachmentInfoBlock.find('#link-to').find('option').length) {
        attachmentInfoBlock.find('#link-to').find('option').prop('disabled', false);
        attachmentInfoBlock.find('#link-to').find('option').map((index, item) => {
            let disabled = (data.type === 'image' && $(item).data('type') !== 'image') || (data.type !== 'image' && $(item).data('type') === 'image');
            $(item).prop('disabled', disabled);
        });

        attachmentInfoBlock.find('#link-to').find('option:enabled').first().prop('selected', true);
        attachmentInfoBlock.find('#link-to').select2('destroy').select2({
            minimumResultsForSearch: -1
        });
        attachmentInfoBlock.find('#custom-link-block').toggleClass('hidden', data.type === 'image');
    }

    attachmentInfoBlock.find('#file-sizes-block').toggleClass('hidden', data.type !== 'image');

    if (data.type === 'image') {
        let fileSizesHtml = '';

        if (Object.keys(data.files).length)
            Object.keys(data.files).map(index => {
                fileSizesHtml += `<option value="${window.location.origin}/${data.files[index]}" data-key="${index}" data-is-original="${index === 'original'}">${index}</option>`;
            });

        attachmentInfoBlock.find('#size').html(fileSizesHtml).select2('destroy').select2({
            minimumResultsForSearch: -1
        });
    }

}

function fileAddEventListener(fileElement, file, dropzone, textEditor, galleryInsertBtn) {
    fileElement.get(0).addEventListener("click", function (e) {
        let _this = $(this);

        if (isEventOut($('>*', '.dz-preview'), e, $('.dz-preview .active-upload-file')) && ($(e.target).attr('class') === undefined || $(e.target).attr('class').indexOf('active-upload-file') === -1)) {

            let eventObj = window.event ? event : e;

            if ((eventObj.ctrlKey || eventObj.metaKey) && currentUploadBtn.data('single-upload') !== true) {
                _this.toggleClass('active');
            } else if (eventObj.shiftKey && currentUploadBtn.data('single-upload') !== true) {
                e.stopPropagation();
                $(dropzone.previewsContainer).find('.dz-preview').removeClass('active');
                window.lastClickedPreviewIndex = $(dropzone.previewsContainer).find('.dz-preview').index(this);

                let start = window.firstClickedPreviewIndex;
                let end = window.lastClickedPreviewIndex;

                if (window.firstClickedPreviewIndex > window.lastClickedPreviewIndex) {
                    start = window.lastClickedPreviewIndex;
                    end = window.firstClickedPreviewIndex;
                }

                $(dropzone.previewsContainer).find('.dz-preview').slice(start, end + 1).addClass('active');
            } else {
                $(dropzone.previewsContainer).find('.dz-preview').not(this).removeClass('active');
                _this.toggleClass('active');

                window.firstClickedPreviewIndex = $(dropzone.previewsContainer).find('.dz-preview').index(this);
            }

            let selectedFilesInfo = {
                ids: [],
                items: []
            };

            $(dropzone.previewsContainer).find('.dz-preview.active').each((key, value) => {
                selectedFilesInfo.ids.push($(value).data('file-id'));
                selectedFilesInfo.items.push($(value).data('file-info'));
            });

            getFileContentView(dropzone, file, _this.hasClass('active'), textEditor, selectedFilesInfo, galleryInsertBtn);
        }

        window.lastClickedPreviewIndex = $(dropzone.previewsContainer).find('.dz-preview').index(this);
    });


}

function totalUploadProgress(files) {

    let uploadFiles = files.filter(file => {
        return file.upload !== undefined;
    });

    let allFilesBytes = 0,
        allSentBytes = 0;

    if (uploadFiles.length)
        uploadFiles.map(file => {
            allFilesBytes += file.upload.total;
            allSentBytes += file.upload.bytesSent;
        });

    let allProgress = 100 * allSentBytes / allFilesBytes;

    if (allProgress > 100)
        allProgress = 100;

    let progressBar = $('.gallery-dropzone > .progress-bar'),
        styleProgress = `${allProgress}%`;

    progressBar.show();

    if (allProgress >= 100) {
        progressBar.hide();
        styleProgress = "0%";
    }

    progressBar.children('span').css({
        width: styleProgress
    });
}