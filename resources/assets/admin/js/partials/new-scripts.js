 (function ($) {

    $(document).ready(function () {

        /* Dropdown */

        $(".dropdown .button").on("click", function(e){

            $(this).siblings(".dropdown-menu").toggleClass("active");
            $(".dropdown .button").not($(this).siblings(".dropdown-menu")).removeClass("active");
            e.stopPropagation();
        })

        /* Close dropdown */

        $(document).on("click", function(e) {
            if (!$(".dropdown").has(e.target).length && !$(".dropdown").is(e.target)) {
                $(".dropdown .dropdown-menu").removeClass("active");
            }
        });

        /* Change bg color of tr if destroy item cheked */

        $(".checkbox-items").on("click",function(){
            $(this).closest("tr").toggleClass("active");
        })

        /* Split box on color */

        // $(document).on("input keypress paste change", ".multiple-color", function () {
        //
        //     let string = $(this).val();
        //     let splits = string.split(',');
        //     let count = splits.length;
        //     let boxWithColor = $(this).parents(".field-wrap").siblings(".box-for-colorpicker");
        //     console.log(boxWithColor);
        //
        //     if( count > 4 ){
        //         $(boxWithColor).empty()
        //         $(splits).each(function(){
        //             $(boxWithColor).append("<span style='background: "+ this +"; width: calc(100% / "+ splits.length +" ) '></span>")
        //         })
        //     }
        //
        //     else if( count % 2 == 0 ){
        //         $(boxWithColor).empty()
        //         $(splits).each(function(){
        //             $(boxWithColor).append("<span style='background: "+ this +"; width: calc(100% / 2 ) '></span>")
        //         })
        //     }
        //
        //     else if( count == 3 ){
        //         $(boxWithColor).empty()
        //         $(splits).each(function(i,k){
        //             if(i == 0){
        //                 $(boxWithColor).append("<span style='background: "+ this +"; width: 50% '></span>")
        //             }
        //             if(i == 1){
        //                 $(boxWithColor).append("<span style='background: "+ this +"; width: 50% '></span>")
        //             }
        //             if(i == 2){
        //                 $(boxWithColor).append("<span style='background: "+ this +"; width: 100% '></span>")
        //             }
        //         })
        //     }
        //
        //     else{
        //         $(boxWithColor).empty()
        //         $(splits).each(function(){
        //             $(boxWithColor).append("<span style='background: "+ this +"; width: 100%  '></span>")
        //         })
        //     }
        //
        // });

        /* Write file name on field */
        $('.import-footer input[type="file"]').change(function(e) {
            var filename = e.target.files[0].name;
            $('.import-footer .input-file-name p').addClass("active").html(filename).attr("title", filename);
            $(".import-footer .input-file-name .delete-file").addClass("active");
            $(".import-footer .input-file-name .upload-file").addClass("active");
        });

        /* Delete file from input */
        $(".import-footer .input-file-name .delete-file").on("click", function(e){
            $(".import-footer input[type='file']").val("");
            $(this).removeClass("active").siblings("p").removeClass("active");
            $('.upload-file').removeClass('active');
        })

        /* Add new product on modal in orders */
        $(".add-one-more-product").on("click", function(){
            $('#chose-order-products').select2('open');
        })

        /* Show btn add new product */
        $("#add-order-products-form").bind("DOMSubtreeModified", function() {
            let blockCounter = $(this).children(".chose-product-variation-block").length;
            if( blockCounter > 0){
                $(".add-one-more-product").addClass("active");
            }
            else{
                $(".add-one-more-product").removeClass("active");
            }

        });


    });
})
(jQuery);
