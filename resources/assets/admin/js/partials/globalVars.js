//Loader gif
let loaderGif = $("#loader-gif");
//Loader gif

window.globalLang = $('html').attr('lang');

let iteration = 0;

let tippyOptions = {
    arrow: true,
    theme: 'cms'
};

let globalErrorMsg = 'Ups, something went wrong! Please contact administrator!';

function isEventOut(blocks, e, that) {
    let response = true;
    $(blocks).each(function () {
        if ($(e.target).get(0) === that.get(0) || $(e.target).closest('HTML', that.get(0)).length === 0)
            response = false;
    });
    return response;
}

function activateFiles(data, that) {
    $.ajax({
        url: `${window.location.origin}/${globalLang}/admin/activateFile`,
        type: 'POST',
        data: data,
        success: function (response) {

            if (response.msg)
                toastr[response.msg.type](response.msg.e);

            that.addClass('animate');

            setTimeout(function () {
                that.removeClass('animate');
            }, 300);

            if (response.status) {

                if (data.active === 1) {
                    that.data('active', 0).removeClass('active');
                    if (!response.isAttachment)
                        $('.files-list').find('.img-block[data-file-id="' + response.itemId + '"]').find('.active-upload-file').data('active', 0).removeClass('active');
                } else {
                    that.data('active', 1).addClass('active');
                    if (!response.isAttachment)
                        $('.files-list').find('.img-block[data-file-id="' + response.itemId + '"]').find('.active-upload-file').data('active', 1).addClass('active');
                }
            }
        },
        error: function (response) {
            if (response.responseJSON) {
                if (response.responseJSON.msg)
                    toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
            } else
                toastr.error(globalErrorMsg);
        }
    });
}

function unsetByValueArray(array, value, isValue) {
    let index;
    if (isValue)
        index = array.indexOf(value);
    else
        index = parseInt(value);

    if (array instanceof Object && !isValue)
        delete array[index];
    else if (index > -1)
        array.splice(index, 1);

    return array;
}

function objectToArray(object) {
    return $.map(object, function (value, index) {
        return [value];
    });
}

function getObjectSize(obj) {
    let size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
}

function number_format(number, decimals = 2, decPoint = '.', thousandsSep = ' ') {

    number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
    var n = !isFinite(+number) ? 0 : +number
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
    var s = ''

    var toFixedFix = function (n, prec) {
        if (('' + n).indexOf('e') === -1) {
            return +(Math.round(n + 'e+' + prec) + 'e-' + prec)
        } else {
            var arr = ('' + n).split('e')
            var sig = ''
            if (+arr[1] + prec > 0) {
                sig = '+'
            }
            return (+(Math.round(+arr[0] + 'e' + sig + (+arr[1] + prec)) + 'e-' + prec)).toFixed(prec)
        }
    }

    s = (prec ? toFixedFix(n, prec).toString() : '' + Math.round(n)).split('.')
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
    }

    return s.join(dec)
}

function findAndReplace(searchText, replacement, searchNode) {
    if (!searchText || typeof replacement === 'undefined') {
        return;
    }
    let regex = typeof searchText === 'string' ? new RegExp(searchText, 'i') : searchText,
        childNodes = (searchNode || document.body).childNodes,
        cnLength = childNodes.length,
        excludes = 'html,head,style,title,link,meta,script,object,iframe';
    while (cnLength--) {
        let currentNode = childNodes[cnLength];
        if (currentNode.nodeType === 1 &&
            (excludes + ',').indexOf(currentNode.nodeName.toLowerCase() + ',') === -1) {
            arguments.callee(searchText, replacement, currentNode);
        }
        if (currentNode.nodeType !== 3 || !regex.test(currentNode.data)) {
            continue;
        }
        let parent = currentNode.parentNode,
            frag = (function () {
                let wrap = document.createElement('div'),
                    frag = document.createDocumentFragment();
                wrap.innerHTML = currentNode.data.replace(regex, replacement);
                while (wrap.firstChild) {
                    frag.appendChild(wrap.firstChild);
                }
                return frag;
            })();
        parent.insertBefore(frag, currentNode);
        parent.removeChild(currentNode);
    }
}

function chunkArray(myArray, chunkSize) {

    let results = [];

    while (myArray.length) {
        results.push(myArray.splice(0, chunkSize));
    }

    return results;
}

function arrUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function arrDiff(arr1, arr2) {
    return [...arr1].filter(x => !arr2.includes(x));
}

function arraysEqual(arr1, arr2) {
    if (arr1.length !== arr2.length)
        return false;
    for (let i = arr1.length; i--;) {
        if (arr1[i] !== arr2[i])
            return false;
    }

    return true;
}

function removeURLParameter(url, parameter) {

    let urlparts = url.split('?');
    if (urlparts.length >= 2) {

        let prefix = encodeURIComponent(parameter) + '=';
        let pars = urlparts[1].split(/[&;]/g);

        for (let i = pars.length; i-- > 0;) {
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        return urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
    }
    return url;
}

function parseURL(url) {
    let parser = document.createElement('a'),
        searchObject = {},
        queries, split, i;
    // Let the browser do the work
    parser.href = url;
    // Convert query string to object
    queries = parser.search.replace(/^\?/, '').split('&');
    for (i = 0; i < queries.length; i++) {
        split = queries[i].split('=');
        searchObject[split[0]] = split[1];
    }
    return {
        protocol: parser.protocol,
        host: parser.host,
        hostname: parser.hostname,
        port: parser.port,
        pathname: parser.pathname,
        search: parser.search,
        searchObject: searchObject,
        hash: parser.hash
    };
}

function groupBy(object, key) {
    return object.reduce((r, a) => {
        r[a[key]] = [...r[a[key]] || [], a];
        return r;
    }, {});
}