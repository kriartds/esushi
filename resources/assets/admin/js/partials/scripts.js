
(function ($) {
    //on load
    $(window).on('load', function () {
        $("body").removeClass("preload");

        // Make focused first input from form

        $('.form-block form:not(#edit-form)').find('input:not(.datetimepicker)').filter(':visible:first').focus();

        // Make focused first input from form

        // equalHeight

        $('.content .main-content .main-block').matchHeight();

        // equalHeight

        $(".sidebar .wrap").mCustomScrollbar({
            axis: "y",
            scrollbarPosition: "inside",
            scrollInertia: 100
        });

        setFieldMaxLength({
            field: '.limit-characters'
        });

        if ($('input.colorpicker').length) {
            $('input.colorpicker').each(function (key, item) {
                initColorPicker(item);
            });
        }

        //    Main widgets

        let chartValues = $('#orders-chart').parent('.chart').data('chart-values');
        let chartData = [];
        let chartColors = [];
        let chartLabels = [];

        if (chartValues !== undefined) {
            chartLabels = Object.keys(chartValues);

            Object.values(chartValues).map((value) => {
                chartData.push(value.count);
                chartColors.push(value.color);
            });
        }

        let chartConfig = {
            type: 'pie',
            data: {
                datasets: [{
                    data: chartData,
                    backgroundColor: chartColors,
                }],
                labels: chartLabels
            },
            options: {
                responsive: true,
                legend: {
                    display: false
                }
            }
        };

        if (document.getElementById('orders-chart') !== null) {
            let ctx = document.getElementById('orders-chart').getContext('2d');
            window.myPie = new Chart(ctx, chartConfig);
        }

        //    Visitors chart

        let visitorChartValues = $('#visitors-chart').parent('.chart').data('chart-values');
        let visitorChartDataVisitors = [];
        let visitorChartDataPageViews = [];
        let visitorChartLabels = [];

        if (visitorChartValues !== undefined) {

            Object.values(visitorChartValues).map((value) => {

                visitorChartLabels.push(value.date);
                visitorChartDataVisitors.push(value.visitors);
                visitorChartDataPageViews.push(value.pageViews);
            });
        }

        let visitorsChartConfig = {
            type: 'line',
            data: {
                datasets: [{
                    label: 'Visitors',
                    data: visitorChartDataVisitors,
                    backgroundColor: '#0bca90',
                    borderColor: '#09a576',
                    fill: false
                }, {
                    label: 'Pages views',
                    data: visitorChartDataPageViews,
                    backgroundColor: '#237fca',
                    borderColor: '#2061a5',
                    fill: false
                }],
                labels: visitorChartLabels
            },
            options: {
                responsive: true,
            }
        };


        if (document.getElementById('visitors-chart') !== null) {
            let visitorsCtx = document.getElementById('visitors-chart').getContext('2d');
            window.myPie = new Chart(visitorsCtx, visitorsChartConfig);
        }

        //    Visitors chart

        //    Main widgets

        // Notifications

        if (!window.location.href.includes('auth') && !window.location.href.includes('notifications')) {
            checkUserNotifications();

            setTimeout(function () {
                setInterval(function () {
                    checkUserNotifications();
                }, 30000);
            }, 100000);
        }


        // Notifications

    });

    //on ready
    $(document).ready(function () {

        $(document).on("input keypress paste change", ".multiple-color", function () {

            let string = $(this).val();
            let splits = string.split(',');
            let count = splits.length;
            let boxWithColor = $(this).parents(".field-wrap").siblings(".box-for-colorpicker");
            console.log(boxWithColor);

            if( count > 4 ){
                $(boxWithColor).empty()
                $(splits).each(function(){
                    $(boxWithColor).append("<span style='background: "+ this +"; width: calc(100% / "+ splits.length +" ) '></span>")
                })
            }

            else if( count % 2 == 0 ){
                $(boxWithColor).empty()
                $(splits).each(function(){
                    $(boxWithColor).append("<span style='background: "+ this +"; width: calc(100% / 2 ) '></span>")
                })
            }

            else if( count == 3 ){
                $(boxWithColor).empty()
                $(splits).each(function(i,k){
                    if(i == 0){
                        $(boxWithColor).append("<span style='background: "+ this +"; width: 50% '></span>")
                    }
                    if(i == 1){
                        $(boxWithColor).append("<span style='background: "+ this +"; width: 50% '></span>")
                    }
                    if(i == 2){
                        $(boxWithColor).append("<span style='background: "+ this +"; width: 100% '></span>")
                    }
                })
            }

            else{
                $(boxWithColor).empty()
                $(splits).each(function(){
                    $(boxWithColor).append("<span style='background: "+ this +"; width: 100%  '></span>")
                })
            }

        });

        $.ajaxSetup({
            headers: {
                'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ajaxError(function (event, xhr) {

            if (xhr.status === 419) {

                let response = xhr.responseJSON;

                refreshToken();

                if (response && response.msg)
                    toastr[response.msg.type] = response.msg.e;
            }

        });

        $.fancybox.defaults.hash = false;

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "5000",
            "hideDuration": "5000",
            "timeOut": "5000",
            "extendedTimeOut": "5000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        // Select2

        $('select.select2').select2({
            width: '100%',
        });

        $('select.select2[multiple]').select2({
            closeOnSelect: false,
            width: '100%',
            dropdownCssClass: "multiple",
        });

        $('select.select2-with-icons').select2({
            templateResult: formatStateResult,
            templateSelection: formatStateSelection,
            minimumResultsForSearch: -1,
            width: '100%'
        });

        $('select.select2.no-search').select2({
            minimumResultsForSearch: -1,
            width: '100%'
        });

        $('select.select2.ajax').select2({
            width: '100%',
            ajax: {
                url: function (params) {
                    return $(this).data('url');
                },
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        page: params.page,
                        q: params.term
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.status ? data.items : [],
                        pagination: {
                            more: data.status && (params.page * 10) < data.total
                        }
                    };
                },
                cache: true
            },
            minimumInputLength: 1,
        });

        //    Select all select2

        $('select[multiple]').not('.ajax').siblings('.select2-container').append('<span class="select-all tooltip" title="Select all"></span>');

        $(document).on('click', '.select-all', function (e) {
            e.stopPropagation();
            selectAllSelect2($(this).siblings('.selection').find('.select2-search__field'));
        });

        $(document).on("keyup", ".select2-search__field", function (e) {
            let eventObj = window.event ? event : e;
            if (eventObj.keyCode === 65 && eventObj.ctrlKey)
                selectAllSelect2($(this));
        });
        // Select2

        // Tooltip

       // tippy('.tooltip', tippyOptions);
        tippy('.tooltip', {
            placement: 'top',
            content: 'Tooltip',
            arrow: true,
        });


        // Tooltip

        //Initialize all CkEditors
        $.each($('form').find('textarea[data-type=ckeditor]'), function (e, v) {

            CKEDITOR.replace($(v).attr('id'), {
                language: globalLang
            });
        });
        //Initialize all CkEditors

        // DateTime picker
        $.datetimepicker.setLocale(globalLang);

        $('.datetimepicker').datetimepicker({
            timepicker: false,
            format: 'd-m-Y',
            scrollInput: false
        });

        $('.datetimepicker.time').datetimepicker({
            timepicker: true,
            format: 'd-m-Y H:i',
            scrollInput: false
        });

        $('.set-curr-time').on('click', function () {
            let pickerId = $(this).data('picker-id');

            $('#' + pickerId).timepicker(
                'setTime', new Date()
            );
        });
        // DateTime picker

        // Change language
        $('#lang').on('change', function () {
            let _this = $(this);

            if (_this.parents('#edit-form').length > 0) {
                let url = window.location.origin + window.location.pathname;
                let urlArr = url.split('/');
                urlArr.pop();
                let newUrl = urlArr.join('/');

                window.location.href = newUrl + '/' + _this.val() + window.location.search;
            } else if (_this.parents('#create-edit-form').length > 0) {
                window.location.href = updateQueryStringParam('langId', _this.val());
            }
        });
        // Change language

        //Sitemap generator
        $('#sitemap').on('click', function (e) {
            e.preventDefault();

            let waitSitemap = $('#wait-sitemap').html();

            toastr.warning(waitSitemap);

            let waitMsg = setInterval(function () {
                toastr.warning(waitSitemap);
            }, 10000);

            $.ajax({
                type: "POST",
                url: `${window.location.origin}/${globalLang}/admin/sitemap`,
                success: function (response) {
                    clearInterval(waitMsg);

                    if (response.status)
                        toastr[response.type](response.messages)
                },
                error: function () {
                    clearInterval(waitMsg);
                }
            });
        });
        //Sitemap generator

        //Run modals
        $(document).on('click', '.getModal', function (e) {
            e.preventDefault();
            let target_modal = $(this).attr('href');
            if (!target_modal) {
                target_modal = $(this).data('modal');
            }
            $(target_modal).arcticmodal({
                closeOnEsc: false,
                beforeOpen: function () {
                    $(target_modal).addClass('openEffect');
                    $(target_modal).removeClass('closeEffect');
                },
                beforeClose: function () {
                    $(target_modal).removeClass('openEffect');
                    $(target_modal).addClass('closeEffect');
                }
            });
            // return false;
        });
        //Run modals

        // scroll to anchors
        $('.scrolling').on('click', function () {
            let target = $(this).attr('href');
            $('html, body').animate({scrollTop: $(target).offset().top}, 500);
            return false;
        });
        // hide placeholder on focus and return it on blur
        // $('input, textarea').focus(function () {
        //     $(this).data('placeholder', $(this).attr('placeholder'));
        //     $(this).attr('placeholder', '');
        // });
        // $('input, textarea').blur(function () {
        //     $(this).attr('placeholder', $(this).data('placeholder'));
        // });

        // Go top
        if ($(window).scrollTop() > 150) {
            $('.go-top').stop().show();
        } else {
            $('.go-top').stop().hide();
        }
        $('.go-top').click(function () {
            $('html, body').stop().animate({scrollTop: 0}, 300);
        });
        $(window).scroll(function () {
            if ($(window).scrollTop() > 150) {
                $('.go-top').stop().fadeIn();
            } else {
                $('.go-top').stop().fadeOut();
            }
        });
        // Go top

        // Fixed submit button

        let everyForm = $('.form-block:visible'),
            fixedSubmitFormFromEveryForm = everyForm.find('.submit-form-btn'),
            lastElementFromEveryForm = fixedSubmitFormFromEveryForm.prev();

        if (everyForm.length && fixedSubmitFormFromEveryForm.length && lastElementFromEveryForm.length && (everyForm.find('form').attr('id') === 'edit-form' || everyForm.find('form').attr('id') === 'create-form')) {

            // fixedSubmitFormFromEveryForm.toggleClass('fixed-btn', !lastElementFromEveryForm.isInViewport() && !fixedSubmitFormFromEveryForm.isInViewport());

            $(window).scroll(function () {
                // fixedSubmitFormFromEveryForm.toggleClass('fixed-btn', !lastElementFromEveryForm.isInViewport());
            });
        }

        // Fixed submit button

        // Remove notifications

        $(document).on('click', '#notifications-block .close', function () {
            markNotificationAsRead($(this));
        });

        $(document).on('click', '#notifications-block .link a', function () {
            markNotificationAsRead($(this).closest('.notification').find('.close'));
        });

        // Remove notifications

        //	Remove error label

        $(document).on('click', 'label.error', function (e) {
            e.preventDefault();
            let _this = $(this);
            _this.fadeOut('slow', function () {
                _this.remove();
            });
        });

        $('form input.error, form textarea.error').focusin(function () {
            let _siblings = $(this).siblings('label.error');
            _siblings.stop().fadeOut('slow');
        }).focusout(function () {
            let _siblings = $(this).siblings('label.error');
            _siblings.stop().fadeIn('slow');
        });

        //	Remove error label

        //    Submit form

        $('.submit-form-btn').on('click', function (e) {
            e.preventDefault();
            submitForm(this);
        });

        //    Submit form

        // Sidebar
        $('.sidebar-item.has-children > a').on('click', function (e) {
            e.preventDefault();
            $(this).siblings('.hidden-sidebar-items').stop().slideToggle();
            $(this).closest('.sidebar-item.has-children').toggleClass('open');
        });
        // Activate item
        $(document).on("click", '.activate-item', function (e) {

            let _this = $(this);
            let active = _this.data('active');
            let elementId = _this.data('item-id');
            let url = _this.data('url');
            let action = _this.data('action');
            _this.removeClass('animate');

            if (!confirm("Do you want activate or inactivate this items?"))
                return false;

            $.ajax({
                type: "POST",
                url: url,
                data: {
                    active: active,
                    id: elementId,
                    action: action
                },
                success: function (response) {

                    if (response.msg)
                        toastr[response.msg.type](response.msg.e);

                    _this.addClass('animate');

                    setTimeout(function () {
                        _this.removeClass('animate');
                    }, 300);

                    if (response.status) {

                        if (action === 'default') {
                            _this.parents('.table').find('.default-item.activate-item').removeClass('active');
                            _this.data('active', 1).addClass('active');
                        } else {
                            if (active === 1) {
                                _this.data('active', 0).removeClass('active');
                            } else {
                                _this.data('active', 1).addClass('active');
                            }
                        }
                    }
                },
                error: function (response) {
                    if (response.responseJSON) {
                        if (response.responseJSON.msg)
                            toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                    } else
                        toastr.error(globalErrorMsg);
                }
            });
        });
        $(document).on("click", ".show_on_main", function (e) {
            var _this = $(this);
            var  active = _this.data('active');
            let elementId = _this.data('item-id');
            let url = _this.data('url');
            let action = _this.data('action');
            let msg = _this.data('msg');

            if (!confirm(msg))
                return false;

            $.ajax({
                type: "POST",
                url: url,
                data: {
                    active: active,
                    id: elementId,
                    action: action
                },
                success: function (response) {
                    if (response.msg)
                        toastr[response.msg.type](response.msg.e);

                    if (response.status) {
                        let status = !active ? 1 : 0;
                        if (active === 1) {
                              _this.data('active', status).removeClass('active');
                              _this.parent('.visibility-on-homepage').find('.unvisible').data('active',status).addClass('active')
                        } else {
                            _this.data('active', status).removeClass('active');
                            _this.parent('.visibility-on-homepage').find('.visible').data('active',status).addClass('active')
                        }
                    }
                },
                error: function (response) {
                    if (response.responseJSON) {
                        if (response.responseJSON.msg)
                            toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                    } else
                        toastr.error(globalErrorMsg);
                }
            });
        });
        $(document).on("click", '.default-item', function (e) {
            let _this = $(this);
            let active = _this.data('active');
            let elementId = _this.data('item-id');
            let url = _this.data('url');
            let action = _this.data('action');
            _this.removeClass('animate');

            if (!confirm("Do you want to change default lang?"))
                return false;

            $.ajax({
                type: "POST",
                url: url,
                data: {
                    active: active,
                    id: elementId,
                    action: action
                },
                success: function (response) {

                    if (response.msg)
                        toastr[response.msg.type](response.msg.e);

                    _this.addClass('animate');

                    setTimeout(function () {
                        _this.removeClass('animate');
                    }, 300);

                    if (response.status) {

                        if (action === 'default') {
                            _this.parents('.table').find('.default-item.activate-item').removeClass('active');
                            _this.data('active', 1).addClass('active');
                        } else {
                            if (active === 1) {
                                _this.data('active', 0).removeClass('active');
                            } else {
                                _this.data('active', 1).addClass('active');
                            }
                        }
                    }
                },
                error: function (response) {
                    if (response.responseJSON) {
                        if (response.responseJSON.msg)
                            toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                    } else
                        toastr.error(globalErrorMsg);
                }
            });
        });
        //End activate item

        // Change position
        if ($('.table-block .table .position').length > 0) {
            tableDragger($('.table-block .table').get(0), {
                mode: 'row',
                dragHandler: '.position',
                onlyBody: true,
                animation: 300,
            }).on('drop', (from, to, el) => {
                let table = $(el),
                    items = table.find('tbody tr'),
                    url = table.data('position-url'),
                    ids = [];

                items.map((k, item) => {
                    ids.push($(item).attr('id'));
                });

                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        order: ids
                    },
                    success: function (response) {
                        if (response.msg && response.msg.e)
                            toastr[response.msg.type](response.msg.e);
                    },
                    error: function (response) {
                        if (response.responseJSON) {
                            if (response.responseJSON.msg)
                                toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                        } else
                            toastr.error(globalErrorMsg);
                    }
                });
            });
        }
        // Change position

        // Generate slug
        if ($('#slug').val() === '') {
            $('#name').keyup(function (e) {

                if (e.keyCode !== 67 && !(e.ctrlKey || e.metaKey) && $(this).closest('form').attr('id') === 'create-form')
                    $('#slug').val(getSlug($(this).val()));
            });
        }
        // Generate slug

        $(document).on('click', '#listActionsBtn', function () {
            $(this).parent().find('.dropdown-menu').toggleClass('active');
        });
        $(document).on('click', '.checkbox-all', function () {
            $(this).toggleClass('all-checked');
            let allItems = $(this).parents('table').find('.checkbox-items').children('input[type=checkbox]');
            if ($(this).hasClass('all-checked')) {
                allItems.prop('checked', true);
            } else {
                allItems.prop('checked', false);
            }
        });
        $(document).on('click', '.checkbox-items', function (e) {
            let checkbox = $(this).find('input[type="checkbox"]');
            if (!$(e.target).is('input')) {
                $(checkbox).prop('checked', function (i, value) {
                    return !value;
                });
            }
        });


        // Copy link to clipboard
        $(document).on('dblclick', '.img-block .file-name', function (e) {
            copyToClipboard($(this).data('file-url'));
        });
        $(document).on('click', '.modal-gallery .copy-url', function (e) {
            let text = $(this).next('input').val();

            if (text !== undefined && text !== null && text.length > 0)
                copyToClipboard(text);
        });
        $(document).on('dblclick', '.copy-to-clipboard', function () {
            let text = $(this).data('clipboard-txt');

            if (text !== undefined && text !== null && text.length > 0)
                copyToClipboard(text);
        });
        // Copy link to clipboard

        $(document).on('click', '.remove-upload-file', function (e) {
            e.preventDefault();
            let _this = $(this);
            let currentUploadBtn = _this.parents('.files-list').prev();

            if (!confirm("Are you sure you want to delete this file?"))
                return false;

            let data = {
                fileId: _this.data('fileId'),
                isAttachment: _this.data('isAttachment') !== undefined
            };

            $.ajax({
                url: `${window.location.origin}/${globalLang}/admin/destroyFile`,
                beforeSend: function () {
                    loaderGif.fadeIn();
                },
                type: 'POST',
                data: data,
                success: function (response) {
                    loaderGif.fadeOut();

                    if (response.status) {

                        if (_this.parents('.img-block').siblings('input[type="hidden"]').length > 1)
                            _this.parents('.img-block').siblings('input[type="hidden"][value="' + response.currFile + '"]').remove();
                        else {
                            _this.parents('.files-list').removeClass('not-empty');
                            _this.parents('.img-block').siblings('input[type="hidden"][value="' + response.currFile + '"]').attr('value', '');
                        }

                        _this.parents('.img-block').fadeOut(300, function () {
                            $(this).remove();
                        });

                        if (currentUploadBtn.data('menu-field') !== undefined) {
                            currentUploadBtn.parents('.item-settings').parent('.sortable-menu-item').data(currentUploadBtn.data('menu-field'), '');
                        }
                    }

                    if (response.msg && response.msg.e)
                        toastr[response.msg.type](response.msg.e);

                },
                error: function (response) {
                    if (response.responseJSON) {
                        if (response.responseJSON.msg)
                            toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                    } else
                        toastr.error(globalErrorMsg);
                }
            });
            return false;
        });
        $(document).on('click', '.active-upload-file', function (e) {
            e.preventDefault();
            let _this = $(this);

            if (!confirm("Do you want activate or inactivate this items?"))
                return false;

            let data = {
                active: _this.data('active'),
                fileId: _this.data('fileId'),
                isAttachment: _this.data('isAttachment') !== undefined
            };

            activateFiles(data, _this);

            return false;
        });

        //Drag images

        if ($(document).find('.files-list').length > 0) {
            $('.img-block').arrangeable({
                'ajaxOption': {
                    'method': 'post',
                    'url': `${window.location.origin}/${globalLang}/admin/changeFilePosition`
                }
            });
        }

        //Drag images

        // Check all rights

        let allRightsAreChecked = true;

        $('.rights').find('input[type="checkbox"].check-all-rights').each(function () {
            if (!$(this).prop('checked')) {
                allRightsAreChecked = false;
                return false;
            }
        });

        $('#global-check-all-rights').prop('checked', allRightsAreChecked).trigger('refresh');

        $('input#global-check-all-rights').on('change', function () {
            let _this = $(this);
            let isChecked = _this.prop('checked');
            _this.parents('.rights').find('input[type="checkbox"].check-all-rights').prop('checked', isChecked).trigger('refresh').trigger('change');

        });

        $('input.check-all-rights').on('change', function () {
            let _this = $(this);
            let isChecked = _this.prop('checked');
            let allChecked = true;

            _this.parents('tr').find('input[type="checkbox"]').not('.check-all-rights').prop('checked', isChecked).trigger('refresh');

            $('.table-block input[type="checkbox"].check-all-rights').map((key, value) => {
                if (!$(value).prop('checked')) {
                    allChecked = false;
                    return false;
                }
            });

            $('input#global-check-all-rights').prop('checked', allChecked).trigger('refresh');
        });

        $('.rights input[type="checkbox"]').on('change', function () {
            let _this = $(this);
            let allChecked = true;

            _this.parents('tr').find('input[type="checkbox"]').not('.check-all-rights').each(function (k, v) {
                if (!$(v).prop('checked')) {
                    allChecked = false;
                    return;
                }
            });

            _this.parents('tr').find('input.check-all-rights').prop('checked', allChecked).trigger('refresh');

            let allRightsAreChecked = true;

            $('.rights').find('input[type="checkbox"].check-all-rights').each(function () {
                if (!$(this).prop('checked')) {
                    allRightsAreChecked = false;
                    return false;
                }
            });

            $('#global-check-all-rights').prop('checked', allRightsAreChecked).trigger('refresh');
        });

        // Check all rights

        // Clone items

        $('.clone-item').on('click', function () {

            cloneItem($(this).parents('.clone-items').find('div.clone'), 'add');

            if ($(this).data('clone-type') === 'dynamic-fields' && $(this).data('url') !== undefined && $(this).data('field-id') !== undefined)
                cloneDynamicItem($(this), $(this).data('url'), $(this).data('field-id'), 'add');
        });

        // Clone items

        // Destroy cloned items

        $(document).on('click', '.destroy-clone', function (e) {
            e.preventDefault();
            let _this = $(this);
            let dynamicDestroyUrl = _this.closest('.clone-items').find('.clone-item').data('destroy-url');

            if (_this.data('item-id') !== undefined && _this.data('destroy-url') !== undefined && _this.data('item-id') !== '' && _this.data('destroy-url') !== '') {
                let url = _this.data('destroy-url');

                $.ajax({
                    url: url,
                    beforeSend: function () {
                        loaderGif.fadeIn();
                    },
                    type: 'POST',
                    data: {
                        'itemId': _this.data('item-id')
                    },
                    success: function (response) {
                        loaderGif.fadeOut();


                        if (response.status) {
                            cloneItem(_this.parents('div.clone'), 'destroy');
                            _this.data('destroy-url', '').attr('data-destroy-url', '').data('item-id', '').attr('data-item-id', '');
                        }

                        if (response.msg && response.msg.e)
                            toastr[response.msg.type](response.msg.e);

                    },
                    error: function (response) {
                        loaderGif.fadeOut();
                    }
                });

            } else {
                cloneItem(_this.parents('div.clone'), 'destroy');
            }

            if ($(this).data('clone-type') === 'dynamic-fields' && dynamicDestroyUrl !== undefined && $(this).data('repeater-id') !== undefined && $(this).data('group-id') !== undefined)
                cloneDynamicItem($(this), dynamicDestroyUrl, $(this).data('repeater-id'), 'destroy', $(this).data('group-id'));
        });

        // Destroy cloned items

        //    Filter

        $('.filter-btn').on('click', function (e) {
            e.stopPropagation();
            let form = $('.form-block.filter');

            if (form.is(':visible'))
                form.find('.submit-form-btn[data-form-event="submit-form"]').trigger('click');
            else
                form.stop().slideToggle();
        });

        if ($('.form-block.filter').length > 0)
            $('body').on('click', function (e) {
                if (isEventOut($('>*', 'body'), e, $('.form-block.filter')) && isEventOut($('>*', 'body'), e, $('.select2-dropdown')) && $('.form-block.filter').is(':visible'))
                    $('.form-block.filter').stop().slideUp();
            });

        // Destroy filter params

        $(document).on('click', '.added-filters-item', function () {
            let _this = $(this);
            let dataKey = _this.data('item-key');
            let dataValue = _this.data('item-value');
            let form = $('#filter-form');
            let field = form.find('[name^="' + dataKey + '"]');

            if (field.length > 0) {
                if (field[0].nodeName.toLowerCase() === 'select')
                    field.find('option[value="' + dataValue + '"]').prop('selected', false).trigger('change');
                else if (field[0].nodeName.toLowerCase() === 'input' && field.val() === dataValue.toString())
                    field.val('');
            }

            form.find('.submit-form-btn[data-form-event="submit-form"]').trigger('click');

            _this.remove();

            if ($('.added-filters-item').length === 1)
                $('.destroy-filters').remove();

        });
        $(document).on('click', '.destroy-filters', function () {
            let form = $('#filter-form');

            form.find('.submit-form-btn[data-form-event="refresh-form"]').trigger('click');
        });

        $(document).on('click', '#actionsForm input[type="checkbox"], #actionsForm .delete-btn, #actionsForm .restore-btn', function () {
            let checkedCheckbox = $('.checkbox-items input[type="checkbox"]:checked');
            let _this = $(this);
            let url = _this.closest('#actionsForm').data('url');
            let event = _this.data('event');
            let message = '';
            let data = {};
            let dataId = [];

            checkedCheckbox.each(function (check, value) {
                dataId.push($(value).val());
            });

            data['id'] = dataId;
            data['event'] = event;

            switch (event) {
                case 'status_check':
                    if (_this.is(':checked')) {
                        data['action'] = 1;
                        message = 'Do you want to active selected items?';
                    } else {
                        data['action'] = 0;
                        message = 'Do you want to disable selected items?';
                    }
                    break;
                case 'set_percent':
                    if (_this.is(':checked')) {
                        var percent = $('input[name="percent_count"]').val();
                        data['action'] = percent;
                        message = 'Do you want to set ' + percent + '% discount selected items?';
                    } else {
                        return false
                    }
                    break;
                case 'delete-to-trash':
                    message = 'Do you want deleted selected items?';
                    break;
                case 'delete-from-trash':
                    message = 'Do you want deleted permanently selected items?';
                    break;
                case 'restore-from-trash':
                    message = 'Do you want restore selected items?';
                    break;
                default:
            }

            if (!dataId.length)
                return false;

            if (!confirm(message))
                return false;

            $.ajax({
                type: "POST",
                url: url,
                beforeSend: function () {
                    loaderGif.fadeIn()
                },
                data: data,
                success: function (response) {
                    loaderGif.fadeOut();

                    if (response.status) {
                        if (event == 'delete-to-trash' || event == 'delete-from-trash' || event == 'restore-from-trash') {
                            checkedCheckbox.each(function () {
                                $(this).closest('tr').fadeOut('400').remove();
                            })
                        } else if (event == 'status_check') {
                            if (_this.is(':checked')) {
                                checkedCheckbox.each(function () {
                                    $(this).closest('tr').find('.status span').addClass('active');
                                })
                            } else {
                                checkedCheckbox.each(function () {
                                    $(this).closest('tr').find('.status span').removeClass('active');
                                })
                            }
                        } else {
                            window.location.reload();
                        }
                    }

                    $('#listActionsBtn').trigger("click");

                    if (response.msg && response.msg.e)
                        toastr[response.msg.type](response.msg.e);
                },
                error: function (response) {
                    loaderGif.fadeOut();
                    if (response.responseJSON) {
                        if (response.responseJSON.msg)
                            toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                    } else
                        toastr.error(globalErrorMsg);
                }
            });

        });

        window.addEventListener('popstate', function (e) {

            if (e.state === null || !Object.keys(e.state).length) {
                window.location.href = window.location.protocol + "//" + window.location.host + window.location.pathname;
            } else {
                window.location.href = e.state.path;
            }

        });

        //    Change popState

        //    Create menu

        $('#menu-constructor').nestable({
            maxDepth: 10,
            listNodeName: 'ul',
            rootClass: 'sortable-menu',
            listClass: 'sortable-menu-list',
            itemClass: 'sortable-menu-item',
            dragClass: 'sortable-menu-dragel',
            handleClass: 'sortable-menu-handle',
            collapsedClass: 'sortable-menu-collapsed',
            placeClass: 'sortable-menu-placeholder',
            emptyClass: 'sortable-menu-empty'

        });

        $('.add-to-menu-block').on('click', function () {
            let _this = $(this);
            let url = _this.data('url');
            let element = _this.siblings('select.select2');
            let menuConstructor = $('#menu-constructor');
            let emptyList = $('.empty-menu-list');
            let categoryName = _this.parent('.field-wrap').siblings('.label-wrap').find('label').html();

            if (categoryName) {
                let newName = categoryName.split(' - ');
                newName.pop();
                categoryName = newName.join(' ');
            }

            let categoryId = _this.parent('.field-wrap').siblings('.label-wrap').find('label').data('category-id');
            categoryId = categoryId !== undefined ? categoryId : null;

            let isMenuComponent = element.attr('id') === 'menu-components';

            let elementValues = [];
            let ul = menuConstructor.children('ul.sortable-menu-list');

            if (element.length === 0 && _this.siblings('input.custom-links'))
                element = _this.siblings('.inputs').children('input.custom-links');

            if (element.get(0).nodeName === 'SELECT') {
                // elementValues = element.val();

                if (element.val() !== null) {

                    element.val().map((value) => {
                        elementValues.push({
                            category: categoryName,
                            name: element.find('option[value="' + value + '"]').html().replace(/^([–]?(&nbsp;))+/, ''),
                            value: value
                        });
                    });
                }

                // element.find('option:selected').prop('selected', false).trigger('change');
            } else {

                if (element.length > 0 && $('#custom-links-name').length > 0 && $('#custom-links-url').length > 0 && $('#custom-links-name').val() !== '' && $('#custom-links-url').val() !== '') {
                    elementValues.push({
                        category: categoryName,
                        name: $('#custom-links-name').val().replace(/^([–]?(&nbsp;))+/, ''),
                        url: $('#custom-links-url').val()
                    });
                }

            }

            if (elementValues.length > 0) {
                emptyList.hide();
                $.ajax({
                    method: "POST",
                    url: url,
                    beforeSend: function () {
                        loaderGif.fadeIn()
                    },
                    data: {
                        items: elementValues,
                        isMenuComponent: isMenuComponent,
                        categoryId: categoryId
                    }
                })
                    .success(function (response) {
                        loaderGif.fadeOut();

                        if (response.msg)
                            toastr[response.msg.type](response.msg.e);

                        if (response.status) {
                            if (element.get(0).nodeName === 'SELECT') {
                                element.find('option:selected').prop('selected', false).trigger('change');
                            } else if (element.get(0).nodeName === 'INPUT') {
                                $('#custom-links-name').val('');
                                $('#custom-links-url').val('');
                            }


                            let li = ul.children('li');

                            if (li.length === 0)
                                ul.html(response.view);
                            else if (li.length > 0)
                                li.last().after(response.view);
                            else {
                                ul.html('');
                                emptyList.show();
                            }
                        }


                    })
                    .fail(function (response) {
                        loaderGif.fadeOut();

                        if (response.responseJSON) {
                            if (response.responseJSON.msg)
                                toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                        } else
                            toastr.error('Fail to send data');
                    });

            }

        });

        // Edit menu

        $(document).on('click', '#menu-constructor .edit-item', function () {
            let _this = $(this);
            _this.toggleClass('open');
            _this.parents('.sortable-menu-handle').siblings('.item-settings').stop().slideToggle();
        });

        // Edit menu

        // Remove menu

        $(document).on('click', '#menu-constructor .remove-item', function () {
            let _this = $(this);
            let menuConstructor = $('#menu-constructor');
            let ul = menuConstructor.children('ul.sortable-menu-list');
            let emptyList = $('.empty-menu-list');
            let url = _this.data('url');
            let menuItem = _this.parents('.sortable-menu-handle').parent('.sortable-menu-item');

            if (url !== undefined) {
                $.ajax({
                    method: "POST",
                    url: url,
                    beforeSend: function () {
                        loaderGif.fadeIn()
                    },
                    data: {
                        id: menuItem.data('menu-id')
                    }
                })
                    .success(function (response) {
                        loaderGif.fadeOut();

                        if (response.msg)
                            toastr[response.msg.type](response.msg.e);

                        if (response.status)
                            menuItem.fadeOut(300, function () {
                                $(this).remove();

                                if (ul.children('li').length < 1)
                                    emptyList.show();

                            });

                    })
                    .fail(function (response) {
                        loaderGif.fadeOut();

                        if (response.responseJSON) {
                            if (response.responseJSON.msg)
                                toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                        } else
                            toastr.error('Fail to send data');
                    });
            } else {
                menuItem.fadeOut(300, function () {
                    $(this).remove();

                    if (ul.children('li').length < 1)
                        emptyList.show();

                });
            }


        });

        // Remove menu

        // Change menu type

        $('#menu-type').on('change', function () {
            let _this = $(this);
            window.location.href = window.location.href = updateQueryStringParam('parent', _this.val());
        });

        // Change menu type

        // Trigger change inputs

        $(document).on('keyup', '#menu-constructor input.menu-fields-constructor', function () {
            $(this).parents('.item-settings').parent('.sortable-menu-item').data($(this).data('field'), $(this).val());
        });

        // Trigger change inputs

        //    Create menu

        //    Change tabs

        let tabsBlock = $('.tabs-block');
        if (tabsBlock.find('.tabs-header .tab-item').first().length && tabsBlock.find('.tabs-body .tab-target').first().length) {
            tabsBlock.find('.tabs-header .tab-item').first().addClass('active');
            tabsBlock.find('.tabs-body .tab-target').first().show();
        }

        $(document).on('click', '.tabs-block .tab-item', function (e) {
            e.preventDefault();
            let _this = $(this),
                target = _this.children('a').attr('href'),
                tabsBlock = $('.tabs-block');

            if (!target)
                target = _this.data('target');

            if (tabsBlock.find('.tab-target').length > 0 && tabsBlock.find(target).length > 0) {
                tabsBlock.find('.tab-item').removeClass('active');
                tabsBlock.find('.tab-target').hide();

                _this.addClass('active');

                if (_this.prop('tagName') === 'LABEL')
                    _this.find('input[type=radio]').prop('checked', true);

                if (_this.data('type') === 'variation' && _this.data('itemId') && _this.data('update-content'))
                    window.getVariations(_this);

                tabsBlock.find(target).show();
            }
            return false;
        });

        //    Change tabs

        // START Products

        //    Change product type

        $('#product_type').on('select2:select', function () {
            let _this = $(this);
            let selectedOption = _this.find('option:selected');
            let type = selectedOption.data('type');
            let tabItems = $('.tabs-block .tab-item');
            if (tabItems.length > 0) {
                tabItems.removeClass('active');
                $('.tabs-block .tab-target').hide();

                tabItems.map((key, value) => {
                    if ($(value).data('type') !== undefined && $(value).data('type') !== type)
                        $(value).addClass('hidden');
                    else
                        $(value).removeClass('hidden');
                });
            }

            let firstTabTarget = tabItems.not('.hidden').first().find('a').attr('href');
            tabItems.not('.hidden').first().addClass('active');
            $('.tabs-block .tab-target' + firstTabTarget).show();

            if (type === 'variation') {
                $('#stock-status-block').addClass('hidden');
                $('.use-for-variations-checkbox-label').show();
                // $('#stock-quantity-block').removeClass('hidden');
            } else {
                $('.use-for-variations-checkbox-label').hide();
                if ($('#use_stock').prop('checked')) {
                    $('#stock-quantity-block').removeClass('hidden');
                    $('#stock-status-block').addClass('hidden');
                } else {
                    $('#stock-quantity-block').addClass('hidden');
                    $('#stock-status-block').removeClass('hidden');
                }
            }
        });

        //    Change product type

        //    Change manage stock

        $('#use_stock').on('change', function () {
            let _this = $(this);
            if (_this.prop('checked')) {
                $('#stock-quantity-block').removeClass('hidden');
                if ($('#product_type').find('option:selected').data('type') === 'simple')
                    $('#stock-status-block').addClass('hidden');
            } else {
                $('#stock-quantity-block').addClass('hidden');
                if ($('#product_type').find('option:selected').data('type') === 'simple')
                    $('#stock-status-block').removeClass('hidden');
            }
        });

        //    Change manage stock

        // END Products

        //    Change attributes types

        $('select#attributes-types').on('change', function () {
            $('#attributes-fields-types').css({'display': parseInt($(this).val()) === 1 || parseInt($(this).val()) === 4 ? 'block' : 'none'});
        });

        //    Change attributes types
        $('input#use_many_colors').on('change', function () {
            let input = $('.attribute-colors#color');
            if (input.length) {
                input.toggleClass('colorpicker', !$(this).prop('checked'));
                input.toggleClass("multiple-color");

                let picker = CP.__instance__[input.attr('id')];
                if ($(this).prop('checked')) {
                    if (picker !== undefined)
                        picker.destroy();
                   // input.prop('style', false);
                    input.data('old-value', input.val());
                    input.val('');
                    $(".box-for-colorpicker").css("background", "transparent")
                } else {
                    if (picker !== undefined)
                        picker.create();
                    else
                        initColorPicker(input.get(0));

                    input.val(input.data('old-value'));
                    input.parents(".field-wrap").siblings(".box-for-colorpicker").empty();
                    input.parents(".field-wrap").siblings(".box-for-colorpicker").css({'background': input.data('old-value')});
                }
            }
        });
        //    Select countries and manipulate regions

        $('select.select2.ajax.countries-list').on('select2:select', function () {
            let _this = $(this),
                regionsSelect = $('select.select2.ajax.regions-list'),
                isMultiple = $(this).attr('multiple') !== undefined,
                currVal = isMultiple ? _this.val().join() : _this.val(),
                getParameterName = isMultiple ? `countriesIds` : `countryId`,
                getParameter = isMultiple ? `?countriesIds=[${currVal}]` : `?countryId=${currVal}`;

            if (!regionsSelect.length)
                return false;

            let regionsUrl = regionsSelect.data('url');
            let newUrl = window.removeURLParameter(regionsUrl, getParameterName) + getParameter;
            regionsSelect.data('url', newUrl);
            if (!isMultiple)
                regionsSelect.val('').trigger('change');
        });

        //    Select countries and manipulate regions

        //    Check multiple coupons
        $('#multiple_coupons').on('change', function () {
            let _this = $(this),
                multipleCouponsBlock = $('#multiple-coupons-block');

            if (!multipleCouponsBlock.length)
                return false;

            multipleCouponsBlock.toggleClass('hidden', !_this.prop('checked'));
        });

        $('#random_multiple_coupons').on('change', function () {
            let _this = $(this),
                customMultipleCouponsBlock = $('#custom-multiple-coupons-block');

            if (!customMultipleCouponsBlock.length)
                return false;

            customMultipleCouponsBlock.toggleClass('hidden', _this.prop('checked'));
        });
        //    Check multiple coupons

        //    Show / Hide items
        $(document).on('change', 'input[type=checkbox].showHideBlocks', function () {
            showHiddenItems($(this));
        });
        //    Show / Hide items

        //    Export data
        $(document).on('click', '.export-data label', function () {
            let _this = $(this),
                url = _this.data('url');

            if (!url)
                return false;

            $.ajax({
                method: 'post',
                url: url,
                beforeSend: function () {
                    _this.addClass('loading');
                },
            }).success(function (response) {
                _this.removeClass('loading');

                if (response.msg)
                    toastr[response.msg.type](response.msg.e);

                if (response.redirect)
                    window.location.href = response.redirect;

            }).fail(function (response) {
                _this.removeClass('loading');

                if (response.responseJSON) {
                    if (response.responseJSON.msg)
                        toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                } else
                    toastr.error('Fail to send data');
            });
        });
        //    Export data

    }); //END on ready
    function submitForm(parentThat) {
        if (!(parentThat instanceof jQuery))
            parentThat = $(parentThat);

        let formId = parentThat.data('form-id');
        let formEvent = $(parentThat).data('form-event');
        let form = $('#' + formId);


        if ((form.length < 1)  ) {
            toastr.error(['Error! Please contact administrator']);
            return;
        }

        form.submit(function (event) {
            event.preventDefault();
        });

        $('textarea[data-type="ckeditor"]').each(function (index, el) {
            let fieldId = $(el).attr('id');

            if (CKEDITOR.instances[fieldId] !== undefined)
                $(this).val(CKEDITOR.instances[fieldId].getData());

        });

        let serializedForm = form.find("select, textarea, input:not([type=checkbox]):not([type=radio])").serializeArray();

        let checkboxesFields = form.find("input[type=checkbox]");
        let radioFields = form.find("input[type=radio]");

        if (checkboxesFields.length > 0)
            checkboxesFields.map((key, value) => {
                serializedForm.push({
                    name: $(value).attr('name'),
                    value: $(value).prop('checked') ? ($(value).val() !== undefined && $(value).val() !== null ? $(value).val() : 'on') : null
                });
            });


        if (radioFields.length > 0) {
            let hasCheckedRadio = false;
            let groupedRadioFields = {};

            radioFields.map((key, value) => {
                if (!groupedRadioFields.hasOwnProperty($(value).attr('name'))) {
                    groupedRadioFields[$(value).attr('name')] = []
                }
                groupedRadioFields[$(value).attr('name')].push(radioFields[key]);
            });

            for (let group in groupedRadioFields) {

                hasCheckedRadio = false;

                groupedRadioFields[group].map((value, key) => {
                    if ($(value).prop('checked')) {
                        hasCheckedRadio = true;

                        return false;
                    }

                });

                groupedRadioFields[group].map((value, key) => {

                    if ($(value).attr('name').substr(-2, 2) === '[]')
                        serializedForm.push({
                            name: $(value).attr('name'),
                            value: $(value).prop('checked') ? ($(value).val() !== undefined && $(value).val() !== null ? $(value).val() : 'on') : null
                        });
                    else {

                        if ($(value).prop('checked'))
                            serializedForm.push({
                                name: $(value).attr('name'),
                                value: $(value).val() !== undefined && $(value).val() !== null ? $(value).val() : 'on'
                            });

                        if (!hasCheckedRadio)
                            serializedForm.push({
                                name: $(value).attr('name'),
                                value: null
                            });
                    }
                })
            }
        }

        if (form.hasClass('menu-constructor-form') && $('#menu-constructor').length > 0)
            serializedForm.push({
                name: 'menuTree',
                value: JSON.stringify($('#menu-constructor').nestable('serialize'))
            });

        if (formEvent === 'refresh-form') {

            serializedForm = [];

            $(form).find('input, textarea').val('');

            if ($(form).find('select').length > 0)
                $(form).find('select').each(function (k, v) {
                    $(v).find('option:selected').each(function (key, value) {
                        $(value).prop('selected', false);
                    }).trigger('change');
                });
        }

        form.find('input, textarea, select').removeClass('error-input');
        form.find('label.error').remove();

        $.ajax({
            method: "POST",
            url: form.attr('action'),
            beforeSend: function () {
                loaderGif.fadeIn()
            },
            data: serializedForm
        })
            .success(function (response) {
                loaderGif.fadeOut();

                if (response.msg && !response.validator)
                    toastr[response.msg.type](response.msg.e);

                if (response.status) {

                    form.find('input, textarea').removeClass('error-input');

                    setTimeout(function () {
                        if (response.redirect)
                            window.location.href = response.redirect;
                    }, 500);


                    if (formId === 'filter-form') {

                        let pushUrl = window.location.protocol + "//" + window.location.host + window.location.pathname + response.pushUrl;
                        window.history.pushState({path: pushUrl}, '', pushUrl);

                        if ($('#actionsForm .export').length){
                            let pushUrlExport = window.location.protocol + "//" + window.location.host + window.location.pathname + '/exportList' +  response.pushUrl;
                            $('#actionsForm .export').data('url', pushUrlExport);
                        }


                        if (response.view && $(document).find('.table-block').length > 0) {
                            $(document).find('.table-block').html(response.view);
                        }

                        if ($('.form-block.filter').is(':visible'))
                            $('.form-block.filter').stop().slideUp();

                        $('.filter-btn').find('span.count').html(response.count ? response.count : '');

                        tippy('.tooltip', tippyOptions);

                        let tabsBlock = $('.tabs-block');
                        if (tabsBlock.length && tabsBlock.find('.tabs-header .tab-item').first().length && tabsBlock.find('.tabs-body .tab-target').first().length) {
                            tabsBlock.find('.tabs-header .tab-item').first().addClass('active');
                            tabsBlock.find('.tabs-body .tab-target').first().show();
                        }

                        if (response.filterParams.title !== undefined && $('.table-block table').length > 0) {
                            $('.table-block table').each(function (key, item) {
                                findAndReplace(response.filterParams.title, `<span class="filter-string-result">${response.filterParams.title}</span>`, item);
                            });
                        }
                    }

                    if (response.itemId && $('#make_variation_action-btn').length && $(form.find(`input[type="hidden"][name="save_attributes_events"]`)).length) {
                        if (form.attr('id') === 'create-form')
                            form.attr('action', `${form.attr('action')}/${response.itemId}/${form.find('#lang').val()}`);

                        $('#make_variation_action-btn').data('item-id', response.itemId);
                        $('#save_variations_changes-btn').data('item-id', response.itemId);
                        form.find(`.submit-form-btn[data-form-id="${form.attr('id')}"]`).data('form-id', 'edit-form');
                        form.attr('id', 'edit-form');

                        if (response.editUrl) {
                            window.history.pushState({path: response.editUrl}, '', response.editUrl);

                            let headerBtn = $('.page-top-buttons').find('.item a.active');
                            if (headerBtn.length) {
                                headerBtn.removeClass('active');
                                let cloneHeaderBtnItem = headerBtn.closest('.item').clone();
                                cloneHeaderBtnItem.find('a').attr('href', response.editUrl).addClass('active').text('Edit');

                                headerBtn.closest('.item').after(cloneHeaderBtnItem);
                            }
                        }

                        $(form.find(`input[type="hidden"][name="save_attributes_events"]`)).remove();
                        $('#attributes-block').find('.destroy-attribute').data('product-id', response.itemId);
                        $('#save-attributes-btn').prop('disabled', true).removeClass('active');
                        form.find('.tab-item[data-type="variation"]').data('itemId', response.itemId).data('updateContent', true);
                    }
                } else {

                    if (response.validator && response.msg) {
                        $.each(response.msg.e, function (ObjNames, ObjValues) {

                            let field = form.find(`[name="${ObjNames}"]`);

                            if (!field.length && ObjNames.split('.'))
                                field = form.find(`[name="${ObjNames}[]"]`);

                            if (field.length) {
                                if (!field.is(':visible')) {
                                    field.parents('.dynamic-blocks').find('.show-hide-block').prop('checked', true).trigger('change');
                                    field.closest('.hidden-content').children('.title').find('input[type=checkbox]').prop('checked', true).trigger('change');
                                }

                                field.addClass('error-input');

                                if (field.prop('tagName') === 'SELECT')
                                    field.siblings('.select2-container').append(`<label class="error ${ObjNames}" for="${ObjNames}">${ObjValues}</label>`);
                                else
                                    field.after(`<label class="error ${ObjNames}" for="${ObjNames}">${ObjValues}</label>`);

                            } else {
                                let fields = form.find('[name^="' + keyToNodeName(ObjNames) + '"]');

                                if (fields.length > 0) {

                                    fields.each(function (keyEach, itemEach) {

                                        let fieldName = ObjNames.split('.').reduce((ac, val, k) => {
                                            if (k === 0)
                                                return val;

                                            return ac + '[' + val + ']'
                                        });

                                        let objectSplit = parseInt(ObjNames.split('.').length === 4 ? ObjNames.split('.')[3] : ObjNames.split('.')[2]);

                                        if (fieldName === $(itemEach).attr('name') || keyEach === objectSplit) {
                                            if (!$(itemEach).is(':visible'))
                                                $(itemEach).parents('.hidden').slideDown();

                                            if ($(this).next().length > 0 && $(this).next().get(0).tagName === 'LABEL')
                                                $(this).next().after('<label class="error ' + $(itemEach).attr('name') + '_' + keyEach + '" for="' + $(itemEach).attr('id') + '">' + ObjValues + '</label>');
                                            else
                                                $(this).after('<label class="error ' + $(itemEach).attr('name') + '_' + keyEach + '" for="' + $(itemEach).attr('id') + '">' + ObjValues + '</label>');

                                            $(this).addClass('error-input');

                                            if ($(itemEach).attr('type') === 'radio' || $(itemEach).attr('type') === 'checkbox')
                                                return false;
                                        }
                                    });
                                }
                            }
                        });

                        if (form.find('.error-input').first().length > 0)
                            $('html, body').stop().animate({scrollTop: form.find('.error-input').first().offset().top - 130}, 1000);

                    }

                }

            })
            .fail(function (response) {
                loaderGif.fadeOut();

                if (response.responseJSON) {
                    if (response.responseJSON.msg)
                        toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                } else
                    toastr.error('Fail to send data');
            });

    }

    function keyToNodeName(text) {
        let resp = '';
        let arr = text.split('.');

        if (arr.length === 4)
            resp = arr[0] + '[' + arr[1] + ']' + '[' + arr[2] + ']';
        else if (arr[1] !== undefined)
            resp = arr[0] + '[' + arr[1] + ']';
        else
            resp = arr[0];

        return resp;

    }

    function formatStateSelection(state) {
        if (!state.id) {
            return state.text;
        }
        let baseUrl = state.title;

        return $('<span class="personal-select2-span"><img src="' + baseUrl + '" class="img-flag" title="' + state.text + '" alt="' + state.text + '" /> ' + state.text + '</span>');
    }

    function formatStateResult(state) {
        if (!state.id) {
            return state.text;
        }
        let baseUrl = state.title;

        return $('<span class="personal-select2-span"><img src="' + baseUrl + '" class="img-flag" title="' + state.text + '" alt="' + state.text + '" /> ' + state.text + '</span>');
    }

    function cloneItem(item, event) {

        if (event === 'add') {

            iteration++;

            // Select 2

            if ($('div.clone').find('select.select2-with-icons').length > 0)
                $('div.clone').find('select.select2-with-icons').select2("destroy");

            if ($('div.clone').find('select.select2').length > 0)
                $('div.clone').find('select.select2').select2("destroy");

            // Select 2

            // Tippy

            if ($(item).find('.tooltip').get(0)._tippy !== undefined)
                $(item).find('.tooltip').get(0)._tippy.destroy();

            // Tippy

            // CKEditor

            $.each(item.first().find('textarea[data-type=ckeditor]'), function (e, v) {
                if (CKEDITOR.instances[$(v).attr('id')])
                    CKEDITOR.instances[$(v).attr('id')].destroy();
            });

            // CKEditor

            let cloneItem = item.first().clone(true).addClass('cloned-item');
            let lastClonedItem = item.last();

            cloneItem.find('input:not([type="radio"]):not([type="checkbox"]), textarea').val('');

            cloneItem.data('count', iteration);

            cloneItem.find('.error-input').removeClass('error-input');
            cloneItem.find('label.error').remove();

            cloneItem.find('input[type=radio]').prop('checked', false).attr('checked', false);
            cloneItem.find('.destroy-clone').data('destroy-url', '').attr('data-destroy-url', '').data('item-id', '').attr('data-item-id', '');

            cloneItem.insertAfter(lastClonedItem);

            // Select 2

            if ($('div.clone').find('select.select2-with-icons').length > 0)
                $('div.clone').find('select.select2-with-icons').select2({
                    templateResult: formatStateResult,
                    templateSelection: formatStateSelection,
                    minimumResultsForSearch: -1
                });

            if ($('div.clone').find('select.select2').length > 0)
                $('div.clone').find('select.select2').select2();

            // Select 2

            // CKEditor

            $.each($(document).find('form textarea[data-type=ckeditor]'), function (e, v) {
                if (!CKEDITOR.instances[$(v).attr('id')])
                    CKEDITOR.replace($(v).attr('id'), {
                        language: globalLang
                    });
            });

            // CKEditor

            item.parents('.clone-items').find('.clone').each(function (k, v) {
                if ($(v).find('input[type=radio]').val() === '') {
                    $(v).find('input[type=radio]').val(k * (-1));
                    $(v).find('input[type=hidden]').val(k * (-1));
                }
            });

            tippy('.tooltip', tippyOptions);

        } else if (event === 'destroy') {

            if (item.siblings('.clone').length === 0) {
                item.find('input:not([type="checkbox"]):not([type="radio"])').val('');

                if (item.find('input[type=radio]').length === 1)
                    item.find('input[type=radio]').prop('checked', true).trigger('refresh');
            }

            if (item.siblings('.clone').length > 0) {

                if (item.find('input[type=radio]').prop('checked')) {
                    item.find('input[type=radio]').prop('checked', false).trigger('refresh');
                    item.siblings('.clone').first().find('input[type=radio]').prop('checked', true).trigger('refresh');
                }

                item.remove();
            }
        }

    }

    function cloneDynamicItem(that, url, repeatFieldId, event, groupId) {
        $.ajax({
            method: "POST",
            url: url,
            beforeSend: function () {
                loaderGif.fadeIn()
            },
            data: {
                event: event,
                fieldId: repeatFieldId,
                groupId: groupId
            }
        })
            .complete(function (response) {
                loaderGif.fadeOut();

                response = response.responseJSON;

                if (response) {

                    if (response.msg)
                        toastr[response.msg.type](response.msg.e);

                    if (response.status) {
                        if (event === 'add' && response.fieldGroupId) {
                            let lastClonedItem = that.closest('.clone-items').find('.cloned-item').last();

                            lastClonedItem.find('label').map((key, value) => {
                                $(value).attr('for', $(value).attr('for').concat('-_-' + response.fieldGroupId));
                            });

                            lastClonedItem.find('input:not([type="search"]):not([type="hidden"])').map((key, value) => {
                                $(value).attr('id', $(value).attr('id').concat('-_-' + response.fieldGroupId));
                                $(value).attr('name', $(value).attr('name').replace(/(\[+[0-9]+\]*)/, '[' + response.fieldGroupId + ']'));

                            });

                            lastClonedItem.find('textarea').map((key, value) => {
                                $(value).attr('id', $(value).attr('id').concat('-_-' + response.fieldGroupId));
                                $(value).attr('name', $(value).attr('name').replace(/(\[+[0-9]+\]*)/, '[' + response.fieldGroupId + ']'));
                            });

                            lastClonedItem.find('select').map((key, value) => {
                                $(value).attr('id', $(value).attr('id').concat('-_-' + response.fieldGroupId));
                                $(value).attr('name', $(value).attr('name').replace(/(\[+[0-9]+\]*)/, '[' + response.fieldGroupId + ']'));
                            });

                            lastClonedItem.find('button.getModal').map((key, value) => {
                                $(value).attr('data-hidden-filed-name', $(value).attr('data-hidden-filed-name').replace(/(\[+[0-9]+\]*)/, '[' + response.fieldGroupId + ']'));
                                $(value).data('hidden-filed-name', $(value).data('hidden-filed-name').replace(/(\[+[0-9]+\]*)/, '[' + response.fieldGroupId + ']'));
                            });

                            lastClonedItem.find('.destroy-clone').data('group-id', response.fieldGroupId).attr('data-group-id', response.fieldGroupId);

                            //    CKEditor

                            $.each(lastClonedItem.find('textarea[data-type=ckeditor]'), function (e, v) {
                                if (!CKEDITOR.instances[$(v).attr('id')])
                                    CKEDITOR.replace($(v).attr('id'), {
                                        language: globalLang
                                    });
                            });

                            //    CKEditor

                            //    Files

                            lastClonedItem.find('button.getModal').next('.files-list').removeClass('not-empty').html('');

                            //    Files
                        }
                    }
                } else
                    toastr.error('Fail to send data');

            });
    }

    function showHiddenItems(that) {
        let blockId = that.data('show-hidden-block-id'),
            showHideBlock = $(`#${blockId}`);

        if (!showHideBlock.length)
            return false;

        showHideBlock.toggleClass('hidden');

        return true;
    }

    function selectAllSelect2(that) {

        let selectAll = true;
        let existUnselected = false;
        let id = that.parents("span[class*='select2-container']").siblings('select[multiple]').attr('id');
        let item = $("#" + id);

        item.find("option").each(function (k, v) {
            if (!$(v).prop('selected')) {
                existUnselected = true;
                return false;
            }
        });

        selectAll = existUnselected ? selectAll : !selectAll;

        item.find("option").prop('selected', selectAll).trigger('change');
    }

    function copyToClipboard(string) {
        document.addEventListener('copy', function (e) {
            e.clipboardData.setData('text/plain', string);
            e.preventDefault();
        });

        let response = document.execCommand('copy');

        if (response)
            toastr.info(['Link was copied in your clipboard!']);
        else
            toastr.error(['Link was not copied in your clipboard! Please contact Administrator!']);

    }

    function setFieldMaxLength(options) {

        let defaultOptions = {
            field: null,
            characters: 200,
            infoItem: 'span',
            infoItemClass: 'characters-nr',
        };

        if (!options instanceof Object)
            options = defaultOptions;

        options = {
            field: !options.field ? defaultOptions.field : options.field,
            characters: !options.characters ? defaultOptions.characters : options.characters,
            infoItem: !options.infoItem ? defaultOptions.infoItem : options.infoItem,
            infoItemClass: !options.infoItemClass ? defaultOptions.infoItemClass : options.infoItemClass,
        };

        if (options.field === undefined || options.field === null || parseInt(options.characters) === undefined || parseInt(options.characters) === null)
            return false;

        let infoItem = options.infoItem === undefined || options.infoItem === null ? 'span' : options.infoItem;
        let field = $(options.field);

        if (field.length > 0 && field.get(0).nodeName !== 'TEXTAREA' && field.get(0).nodeName !== 'INPUT')
            return false;

        field.map((key, value) => {
            let currField = $(value);
            let dynamicItem = $(`<${infoItem} class="${options.infoItemClass}"></${infoItem}>`);
            let maxCharacters = parseInt(currField.data('max-characters')) ? parseInt(currField.data('max-characters')) : options.characters;

            let infoItemContent = `${currField.val().length}/${maxCharacters}`;
            currField.after(dynamicItem.text(infoItemContent));

            $(currField).on('keyup', function () {
                let val = $(this).val();
                let length = val.length;
                if (length > maxCharacters) {
                    let newVal = $(this).val().substr(0, maxCharacters);
                    $(this).val(newVal);
                    length = newVal.length;
                }

                infoItemContent = `${length}/${maxCharacters}`;
                dynamicItem.text(infoItemContent)
            });
        });
    }

    function getUrlParameter(sParam) {
        let sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    }

    function updateQueryStringParam(key, value) {

        let baseUrl = [location.protocol, '//', location.host, location.pathname].join(''),
            urlQueryString = document.location.search,
            newParam = key + '=' + value,
            params = '?' + newParam;

        // If the "search" string exists, then build params from it
        if (urlQueryString) {

            let updateRegex = new RegExp('([\?&])' + key + '[^&]*');
            let removeRegex = new RegExp('([\?&])' + key + '=[^&;]+[&;]?');

            if (typeof value === 'undefined' || value == null || value === '') { // Remove param if value is empty
                params = urlQueryString.replace(removeRegex, "$1");
                params = params.replace(/[&;]$/, "");

            } else if (urlQueryString.match(updateRegex) !== null) { // If param exists already, update it
                params = urlQueryString.replace(updateRegex, "$1" + newParam);

            } else { // Otherwise, add it to end of query string
                params = urlQueryString + '&' + newParam;
            }
        }

        // no parameter was set so we don't need the question mark
        params = params === '?' ? '' : params;

        return baseUrl + params;
    }

    function fileSize(size) {
        let cutoff, i, selectedSize, selectedUnit, unit, units, _i, _len;
        selectedSize = 0;
        selectedUnit = "b";
        if (size > 0) {
            units = ['TB', 'GB', 'MB', 'KB', 'b'];
            for (i = _i = 0, _len = units.length; _i < _len; i = ++_i) {
                unit = units[i];
                cutoff = Math.pow(1024, 4 - i) / 10;
                if (size >= cutoff) {
                    selectedSize = size / Math.pow(1024, 4 - i);
                    selectedUnit = unit;
                    break;
                }
            }
            selectedSize = Math.round(10 * selectedSize) / 10;
        }
        return "<strong>" + selectedSize + "</strong> " + selectedUnit;
    }

    function initColorPicker(item) {
        let picker = new CP(item).on("change", function (color) {
            this.source.value = '#' + color;
            $(this.source).parents(".field-wrap").siblings(".box-for-colorpicker").css({'background': '#' + color});
        });

        picker.source.oncut = updateColorPicker;
        picker.source.onpaste = updateColorPicker;
        picker.source.onkeyup = updateColorPicker;
        picker.source.oninput = updateColorPicker;

        function updateColorPicker() {
            if ($(this).hasClass('colorpicker')) {
                $(this).parents(".field-wrap").siblings(".box-for-colorpicker").css({'background': this.value});
                picker.set(this.value).enter();
            }
        }
    }

    function refreshToken() {
        $.get(`${window.location.origin}/magic-token`)
            .done(function (response) {
                if (response.status) {
                    $.ajaxSetup({
                        headers: {
                            'x-csrf-token': response.token
                        }
                    });
                }
            });
    }

    $.fn.isInViewport = function () {
        let elementTop = $(this).offset().top,
            elementBottom = elementTop + $(this).outerHeight(),
            viewportTop = $(window).scrollTop(),
            viewportBottom = viewportTop + $(window).height();
        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    function checkUserNotifications() {
        let notificationsBlock = $('#notifications-block');

        if (!notificationsBlock.length)
            return false;

        $.ajax({
            method: 'post',
            url: `${window.location.origin}/${globalLang}/admin/checkUserNotifications`
        }).success(function (response) {
            if (response.status && response.notifications) {
                notificationsBlock.html(response.notifications);
                notificationsBlock.fadeIn();
                // let duration = response.duration ? response.duration : 15000;
                //
                // setTimeout(function () {
                //     notificationsBlock.fadeOut(300, function () {
                //         $(this).html('');
                //     });
                // }, duration);
            }

        }).fail(function () {
            notificationsBlock.html('');
        });
    }

    function markNotificationAsRead(that) {
        let notificationBlock = that.closest('.notification'),
            notificationsCount = notificationBlock.siblings('.notification').length,
            notificationsBlock = $('#notifications-block');

        $.ajax({
            method: 'post',
            url: `${window.location.origin}/${globalLang}/admin/markNotificationAsRead`,
            data: {
                id: that.data('id')
            }
        }).success(function (response) {
            if (response.msg)
                toastr[response.msg.type](response.msg.e);

            if (response.status) {
                notificationBlock.fadeOut(300, function () {
                    if (!notificationsCount)
                        notificationsBlock.addClass('hidden');

                    $(this).remove();
                });
            }

        }).fail(function () {
            notificationsBlock.html('');
        });
    }

    if ($('#parent').val() == '')
        $('#categoryComponent').show();

    $('#parent').change(function () {
        if ($(this).val() == '')
            $('#categoryComponent').show();
        else
            $('#categoryComponent').hide()
    });

    if ($('#config_email').val() == '' || $('#config_email_pass').val() == '' || $('#config_email_host').val() == '' || $('#config_email_port').val() == '')
        $('#test_connection').show();

    $(document).on('click', '#test_connection', function (event) {
        event.preventDefault();
        $.ajax({
            url: `${window.location.origin}/${globalLang}/admin/testEmail`,
            type: 'POST',
            data: {
                'email': $('#config_email').val(),
                'pass': $('#config_email_pass').val(),
                'host': $('#config_email_host').val(),
                'port': $('#config_email_port').val()
            },
            success: function (response) {

                if (response) {
                    if (response.msg)
                        toastr[response.msg.type](response.msg.e);
                } else
                    toastr.error('Fail to send data');

            }
        });
    });

    //focus buton disable active filter
    $('#email').on('keyup', function () {

        if ($(this).val().length > 3) {
            $('#disable-button').attr('disabled', false)
        } else {
            $('#disable-button').attr('disabled', true)
        }

    });

    $(document).on('click','#coupons-block .tab-item', function () {
        $(this).find('input').prop("checked", true);
    });
})
(jQuery);