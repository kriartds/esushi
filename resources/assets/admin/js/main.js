//=include partials/jquery-1.11.1.min.js
//=include partials/jquery.arcticmodal-0.3.min.js
//=include partials/jquery.fancybox.min.js
//=include partials/toastr.min.js
//=include partials/drag-arrange.js


//=include partials/select2.full.js

//=include partials/jquery.datetimepicker.full.min.js
//=include partials/jquery.timepicker.min.js
//=include partials/tippy.all.min.js
//=include partials/jquery.matchHeight-min.js
//=include partials/jquery.mCustomScrollbar.concat.min.js
//=include partials/table-dragger.min.js
//=include partials/speakingurl.min.js
//=include partials/jquery.nestable.min.js
//=include partials/dropzone.min.js
//=include partials/moment.js
//=include partials/Chart.bundle.min.js
//=include partials/color-picker.min.js
//=include partials/globalVars.js
//=include partials/dropzone-config.js


//PRODUCTS MODULE

//=include components/products/attributes.js
//=include components/products/variations.js
//=include components/products/importProducts.js

//SHIPPING ZONES MODULE

//=include components/shipping-zones/shipping-zones.js

//ORDERS

//=include components/orders/orders.js
//=include components/home-products/home.js

//REPORTS

//=include components/reports/charts.js

//=include partials/scripts.js

