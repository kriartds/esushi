;(function ($) {
        $(window).on('load', function () {

            let reportsChart = $('#reports-orders-chart'),
                legend = $('#reports-legend');

            if (reportsChart.length) {
                chart(reportsChart);

                if (legend.length) {
                    legend.find('.item').on('click', function () {
                        let series = parseInt($(this).data('series'));

                        if (isNaN(series) || series === undefined)
                            return false;

                        let currDataset;
                        if ($(this).data('page') === 'sales_by_category') {
                            reportsOrdersChart.data.datasets.map(item => {
                                if (item.categoryId === series) {
                                    currDataset = item;
                                    return false;
                                }
                            });
                        } else
                            currDataset = reportsOrdersChart.data.datasets[series];

                        if (currDataset) {
                            $(this).toggleClass('inactive');
                            currDataset.hidden = $(this).hasClass('inactive');

                            reportsOrdersChart.update();
                        }

                    });

                    legend.find('.item').hover(function () {
                        let series = parseInt($(this).data('series')),
                            defaultColor = $(this).data('default-color');

                        if (isNaN(series) || series === undefined)
                            return false;

                        let currDataset;
                        if ($(this).data('page') === 'sales_by_category') {
                            reportsOrdersChart.data.datasets.map(item => {
                                if (item.categoryId === series) {
                                    currDataset = item;
                                    return false;
                                }
                            });
                        } else
                            currDataset = reportsOrdersChart.data.datasets[series];

                        if (currDataset) {
                            if ($(this).is(':hover')) {
                                currDataset.backgroundColor = '#9c5d90';

                                if (currDataset.type === 'line') {
                                    currDataset.borderColor = '#9c5d90';
                                    currDataset.pointBorderColor = '#9c5d90';
                                    currDataset.borderWidth = 5;
                                }
                            } else {

                                if (defaultColor) {
                                    currDataset.backgroundColor = defaultColor;

                                    if (currDataset.type === 'line') {
                                        currDataset.borderColor = defaultColor;
                                        currDataset.pointBorderColor = defaultColor;
                                        currDataset.borderWidth = 3;
                                    }
                                }
                            }

                            reportsOrdersChart.update();
                        }
                    });
                }
            }

            $('#reportsInterval').on('change',function () {
                let _this = $(this);
                if (_this.find('option:selected').length)
                    window.location.href = _this.find('option:selected').data('href')
            })


        });

        $(document).ready(function () {
            $('.reports-sidebar .reports-actions-sidebar:not(.no-action) .title').on('click', function () {
                let _this = $(this);

                _this.closest('.item-wrap').siblings('.item-wrap').children('.hidden-content').removeClass('open').slideUp();

                _this.toggleClass('open');
                _this.siblings('.hidden-content').stop().slideToggle(_this.hasClass('open'));
            });
        });

        function chart(reportsChart) {
            let chartValues = reportsChart.closest('.chart').data('chart-values'),
                chartDataGrossOrdersAmount = [],
                chartDataNetOrdersAmount = [],
                chartDataProductsCount = [],
                chartDataOrdersCount = [],
                chartDataShippingAmount = [],
                chartDataCouponsAmount = [],
                chartDataProductsAmount = [],
                chartDataCategoriesAmount = [],
                chartDataCouponsPageCount = [],
                chartDataCouponsPageAmount = [],

                chartDataAuthUsersAmount = [],
                chartDataAuthUsersCount = [],
                chartDataUnauthorisedUsersAmount = [],
                chartDataUnauthorisedUsersCount = [],
                chartLabels = [],
                labelsDateFormat = chartValues.labelsFormat === 'month' ? "month" : "day";

            if (!Object.values(chartValues).length)
                return false;

            if (typeof (chartValues.labels) === 'object') {
                chartValues.labels.map((item) => {
                    chartLabels.push(moment.unix(item).toDate());
                });
            }

            if (typeof (chartValues.points) === 'object') {
                chartValues.points.map((item) => {
                    if (chartValues.currPage === 'sales_by_date') {
                        chartDataGrossOrdersAmount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.grossOrdersAmount
                        });

                        chartDataNetOrdersAmount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.netOrders
                        });

                        chartDataOrdersCount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.orders
                        });

                        chartDataProductsCount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.products
                        });

                        chartDataShippingAmount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.shipping
                        });

                        chartDataCouponsAmount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.coupons
                        });
                    } else if (chartValues.currPage === 'sales_by_product') {
                        chartDataProductsCount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.productsCount
                        });
                        chartDataProductsAmount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.productsAmount
                        });
                    } else if (chartValues.currPage === 'coupons_by_date') {
                        chartDataCouponsPageCount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.couponsCount
                        });
                        chartDataCouponsPageAmount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.couponsAmount
                        });
                    } else if (chartValues.currPage === 'customers_vs_guests') {
                        chartDataAuthUsersAmount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.authorisedUsersAmount
                        });
                        chartDataAuthUsersCount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.authorisedUsersCount
                        });
                        chartDataUnauthorisedUsersAmount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.unauthorisedUsersAmount
                        });
                        chartDataUnauthorisedUsersCount.push({
                            x: moment.unix(item.date).toDate(),
                            y: item.unauthorisedUsersCount
                        });
                    }
                });

                if (chartValues.currPage === 'sales_by_category') {
                    let groupedPoints = groupBy(chartValues.points, 'id');
                    for (let index in groupedPoints) {
                        let items = groupedPoints[index];

                        let barItem = {
                            yAxisID: 'yAxisCategoriesAmount',
                            fill: false,
                            data: [],
                            backgroundColor: items[0] ? items[0].color : '#a3a3a3',
                            categoryId: items[0] ? items[0].id : 0,
                        };

                        items.map(item => {
                            barItem.data.push({
                                x: moment.unix(item.date).toDate(),
                                y: item.amount,
                                label: item ? `${item.name}: ${item.amountStr}` : ''
                            });
                        });

                        chartDataCategoriesAmount.push(barItem);
                    }

                }
            }

            let chartConfig = {
                type: 'bar',
                data: {
                    labels: chartLabels
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false,
                    },
                    scales: {
                        xAxes: [{
                            type: 'time',
                            time: {
                                tooltipFormat: chartValues.labelsFormat === 'month' ? 'MMMM' : 'DD MMM',
                                parser: 'YYYY-MM-DD HH:mm',
                                unit: labelsDateFormat,
                                displayFormats: {
                                    day: 'DD MMM',
                                    month: 'MMMM'
                                },
                            },
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                fontSize: 10,
                            },
                            maxBarThickness: 100,
                            barThickness: 50
                        }],
                        yAxes: [
                            {
                                id: 'yAxisAmount',
                                position: 'right',
                                gridLines: {
                                    display: false
                                },
                                fontSize: 10,
                                ticks: {
                                    beginAtZero: true,
                                    maxTicksLimit: chartValues.productsCount ? chartValues.productsCount : 1,
                                    userCallback: function (label, index, labels) {
                                        return `${label.toFixed(2)}${chartValues.currency}`;
                                    },
                                },
                            },
                            {
                                id: 'yAxisCount',
                                position: 'left',
                                ticks: {
                                    beginAtZero: true,
                                    fontSize: 10,
                                    userCallback: function (label, index, labels) {
                                        if (Math.floor(label) === label) {
                                            return label;
                                        }
                                    },
                                },
                            },
                        ]
                    },
                    tooltips: {
                        custom: function (tooltip) {
                            if (!tooltip) return;
                            tooltip.displayColors = false;
                        }
                    }
                }
            };

            if (chartValues.currPage === 'sales_by_date') {
                chartConfig.data.datasets = [
                    {
                        type: 'line',
                        label: 'Gross order amount',
                        data: chartDataGrossOrdersAmount,
                        yAxisID: 'yAxisAmount',
                        fill: false,
                        borderColor: '#b1d4ea',
                        backgroundColor: '#b1d4ea',
                        pointBorderWidth: 4,
                        pointRadius: 6,
                        pointHoverRadius: 6,
                        pointBorderColor: '#b1d4ea',
                        pointBackgroundColor: '#fff',
                        tension: 0,
                    },
                    {
                        type: 'line',
                        label: 'Net Order amount',
                        data: chartDataNetOrdersAmount,
                        yAxisID: 'yAxisAmount',
                        fill: false,
                        borderColor: '#3498db',
                        backgroundColor: '#3498db',
                        pointBorderWidth: 4,
                        pointRadius: 6,
                        pointHoverRadius: 6,
                        pointBorderColor: '#3498db',
                        pointBackgroundColor: '#fff',
                        tension: 0,
                    },
                    {
                        type: 'line',
                        label: 'Shipping amount',
                        data: chartDataShippingAmount,
                        yAxisID: 'yAxisAmount',
                        fill: false,
                        borderColor: '#5cc488',
                        backgroundColor: '#5cc488',
                        pointBorderWidth: 4,
                        pointRadius: 6,
                        pointHoverRadius: 6,
                        pointBorderColor: '#5cc488',
                        pointBackgroundColor: '#fff',
                        tension: 0,
                    },
                    {
                        type: 'line',
                        label: 'Discount amount',
                        data: chartDataCouponsAmount,
                        yAxisID: 'yAxisAmount',
                        fill: false,
                        borderColor: '#f1c40f',
                        backgroundColor: '#f1c40f',
                        pointBorderWidth: 4,
                        pointRadius: 6,
                        pointHoverRadius: 6,
                        pointBorderColor: '#f1c40f',
                        pointBackgroundColor: '#fff',
                        tension: 0,
                    },
                    {
                        type: 'line',
                        label: 'Products purchased',
                        yAxisID: 'yAxisCount',
                        data: chartDataProductsCount,
                        fill: false,
                        borderColor: '#E67F46',
                        backgroundColor: '#E67F46',
                        pointBorderWidth: 4,
                        pointRadius: 6,
                        pointHoverRadius: 6,
                        pointBorderColor: '#E67F46',
                        pointBackgroundColor: '#fff',
                        tension: 0,
                        borderDash: [10, 10]
                    },
                    {
                        label: 'Orders placed',
                        yAxisID: 'yAxisCount',
                        data: chartDataOrdersCount,
                        fill: false,
                        backgroundColor: '#dbe1e3',
                    },
                ];
                chartConfig.options.tooltips.callbacks = {
                    label: function (tooltipItems, data) {
                        let response;
                        switch (tooltipItems.datasetIndex) {
                            case 4:
                                response = `${tooltipItems.yLabel} ${tooltipItems.yLabel > 1 ? 'products' : 'product'}`;
                                break;
                            case 5:
                                response = `${tooltipItems.yLabel} ${tooltipItems.yLabel > 1 ? 'orders' : 'order'}`;
                                break;
                            default:
                                response = `${tooltipItems.yLabel}${chartValues.currency}`;
                        }

                        return response;
                    }
                };
            } else if (chartValues.currPage === 'sales_by_product') {
                chartConfig.data.datasets = [
                    {
                        type: 'line',
                        label: 'Products sales amount',
                        data: chartDataProductsAmount,
                        yAxisID: 'yAxisAmount',
                        fill: false,
                        borderColor: '#b1d4ea',
                        backgroundColor: '#b1d4ea',
                        pointBorderWidth: 4,
                        pointRadius: 6,
                        pointHoverRadius: 6,
                        pointBorderColor: '#b1d4ea',
                        pointBackgroundColor: '#fff',
                        tension: 0,
                    },
                    {
                        label: 'Products purchased',
                        yAxisID: 'yAxisCount',
                        data: chartDataProductsCount,
                        fill: false,
                        backgroundColor: '#dbe1e3',
                    },
                ];
                chartConfig.options.tooltips.callbacks = {
                    label: function (tooltipItems, data) {
                        return tooltipItems.datasetIndex === 1 ? `${tooltipItems.yLabel} ${tooltipItems.yLabel > 1 ? 'purchases' : 'purchase'}` : `${tooltipItems.yLabel}${chartValues.currency}`;
                    }
                };
            } else if (chartValues.currPage === 'sales_by_category') {
                if (chartDataCategoriesAmount.length) {
                    delete chartConfig.options.scales.xAxes[0].barThickness;
                    chartConfig.options.scales.yAxes = [{
                        id: 'yAxisCategoriesAmount',
                        gridLines: {
                            display: false
                        },
                        fontSize: 10,
                        ticks: {
                            beginAtZero: true,
                            maxTicksLimit: 10,
                            userCallback: function (label, index, labels) {
                                return `${label.toFixed(2)}${chartValues.currency}`;
                            },
                        },
                    }];

                    chartConfig.data.datasets = chartDataCategoriesAmount;
                }
                chartConfig.options.tooltips.callbacks = {
                    label: function (tooltipItems, data) {
                        if (data.datasets[tooltipItems.datasetIndex]) {
                            let labelArr = data.datasets[tooltipItems.datasetIndex].data.filter(item => {
                                return item.y === tooltipItems.yLabel ? item.label : null;
                            });
                            return labelArr[0] ? labelArr[0].label : false;
                        }
                    }
                };
            } else if (chartValues.currPage === 'coupons_by_date') {
                chartConfig.options.scales.yAxes[0].ticks.maxTicksLimit = chartValues.couponsCount ? chartValues.couponsCount : 1;
                chartConfig.data.datasets = [
                    {
                        type: 'line',
                        label: 'Discounts amount',
                        data: chartDataCouponsPageAmount,
                        yAxisID: 'yAxisAmount',
                        fill: false,
                        borderColor: '#b1d4ea',
                        backgroundColor: '#b1d4ea',
                        pointBorderWidth: 4,
                        pointRadius: 6,
                        pointHoverRadius: 6,
                        pointBorderColor: '#b1d4ea',
                        pointBackgroundColor: '#fff',
                        tension: 0,
                    },
                    {
                        label: 'Coupons used',
                        yAxisID: 'yAxisCount',
                        data: chartDataCouponsPageCount,
                        fill: false,
                        backgroundColor: '#dbe1e3',
                    },
                ];
                chartConfig.options.tooltips.callbacks = {
                    label: function (tooltipItems, data) {
                        return tooltipItems.datasetIndex === 1 ? `${tooltipItems.yLabel} used` : `${tooltipItems.yLabel}${chartValues.currency}`;
                    }
                };
            } else if (chartValues.currPage === 'customers_vs_guests') {

                chartConfig.options.scales.yAxes[0].ticks.maxTicksLimit = chartValues.count ? chartValues.count : 1;

                chartConfig.data.datasets = [
                    {
                        type: 'line',
                        label: 'Authorised users amount',
                        data: chartDataAuthUsersAmount,
                        yAxisID: 'yAxisAmount',
                        fill: false,
                        borderColor: '#3498db',
                        backgroundColor: '#3498db',
                        pointBorderWidth: 4,
                        pointRadius: 6,
                        pointHoverRadius: 6,
                        pointBorderColor: '#3498db',
                        pointBackgroundColor: '#fff',
                        tension: 0,
                    },
                    {
                        type: 'line',
                        label: 'Unauthorised users amount',
                        data: chartDataUnauthorisedUsersAmount,
                        yAxisID: 'yAxisAmount',
                        fill: false,
                        borderColor: '#5cc488',
                        backgroundColor: '#5cc488',
                        pointBorderWidth: 4,
                        pointRadius: 6,
                        pointHoverRadius: 6,
                        pointBorderColor: '#5cc488',
                        pointBackgroundColor: '#fff',
                        tension: 0,
                    },
                    {
                        label: 'Authorised users orders count',
                        yAxisID: 'yAxisCount',
                        data: chartDataAuthUsersCount,
                        fill: false,
                        backgroundColor: '#b1d4ea',
                    },
                    {
                        label: 'Unauthorised users orders count',
                        yAxisID: 'yAxisCount',
                        data: chartDataUnauthorisedUsersCount,
                        fill: false,
                        backgroundColor: '#dbe1e3',
                    },
                ];

                chartConfig.options.tooltips.callbacks = {
                    label: function (tooltipItems, data) {
                        let response;
                        switch (tooltipItems.datasetIndex) {
                            case 0:
                            case 1:
                                response = `${tooltipItems.yLabel}${chartValues.currency}`;
                                break;
                            case 2:
                                response = `${tooltipItems.yLabel} authorised users ${tooltipItems.yLabel > 1 ? 'orders' : 'order'}`;
                                break;
                            case 3:
                                response = `${tooltipItems.yLabel} unauthorised users ${tooltipItems.yLabel > 1 ? 'orders' : 'order'}`;
                                break;
                        }

                        return response;
                    }
                };
            }

            if (reportsChart.length) {
                let ctx = reportsChart.get(0).getContext('2d');
                window.reportsOrdersChart = new Chart(ctx, chartConfig);
            }
        }


    }

)
(jQuery);