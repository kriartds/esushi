;(function ($) {

    $(document).ready(function () {

        $(document).on('click', '.upload-file',function () {
            let form = $('#importForm'),
                importBtn = $(this);

            form.submit(function (event) {
                event.preventDefault();
            });

            let data = new FormData(),
                fileInput = form.find('#import').get(0);

            if (fileInput.files[0])
                data.append('file', fileInput.files[0]);

            $.ajax({
                method: "POST",
                url: form.attr('action'),
                beforeSend: function () {
                    importBtn.addClass('loading');
                },
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                data: data,
                success: function (response) {
                    importBtn.removeClass('loading');
                    form.find('input').val('');

                    if (response.msg)
                        toastr[response.msg.type](response.msg.e);
                },
                error: function (response) {
                    importBtn.removeClass('loading');
                    form.find('input').val('');

                    if (response.responseJSON) {
                        if (response.responseJSON.msg)
                            toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                    } else
                        toastr.error('Fail to send data');
                }
            });
        });


        // $(document).on('change', '.import-data input[type=file][name=import_products]', function () {
        //     let _this = $(this),
        //         form = _this.closest('form'),
        //         importBtn = _this.closest('.import-data');
        //
        //     form.submit(function (event) {
        //         event.preventDefault();
        //     });
        //
        //     let data = new FormData(),
        //         fileInput = _this.get(0);
        //
        //     if (fileInput.files[0])
        //         data.append('file', fileInput.files[0]);
        //
        //     $.ajax({
        //         method: "POST",
        //         url: form.attr('action'),
        //         beforeSend: function () {
        //             importBtn.addClass('loading');
        //         },
        //         enctype: 'multipart/form-data',
        //         processData: false,
        //         contentType: false,
        //         cache: false,
        //         data: data,
        //         success: function (response) {
        //             importBtn.removeClass('loading');
        //             form.find('input').val('');
        //
        //             if (response.msg)
        //                 toastr[response.msg.type](response.msg.e);
        //         },
        //         error: function (response) {
        //             importBtn.removeClass('loading');
        //             form.find('input').val('');
        //
        //             if (response.responseJSON) {
        //                 if (response.responseJSON.msg)
        //                     toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
        //             } else
        //                 toastr.error('Fail to send data');
        //         }
        //     });
        //
        //     // if (!url)
        //     //     return false;
        //     //
        //     // $.ajax({
        //     //     method: 'post',
        //     //     url: url,
        //     //     beforeSend: function () {
        //     //         _this.addClass('loading');
        //     //     },
        //     // }).success(function (response) {
        //     //     _this.removeClass('loading');
        //     //
        //     //     if (response.msg && !response.validator)
        //     //         toastr[response.msg.type](response.msg.e);
        //     // }).fail(function (response) {
        //     //     _this.removeClass('loading');
        //     //
        //     //     if (response.responseJSON) {
        //     //         if (response.responseJSON.msg)
        //     //             toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
        //     //     } else
        //     //         toastr.error('Fail to send data');
        //     // });
        // });

    });

})
(jQuery);