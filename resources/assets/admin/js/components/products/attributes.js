;(function ($) {

    $(document).ready(function () {

        $('#attributes').on('change', function () {
            let _this = $(this);
            let getAttrBtn = $('#get-attributes-btn');
            getAttrBtn.prop('disabled', _this.val() === null);
        });

        // Check attributes options

        $(document).on('change', '#attributes-block .selected-attributes-block input, #attributes-block .selected-attributes-block select', function () {
            let checkSelectedOptions = true;

            if ($(this).prop('tagName') === 'INPUT')
                checkSelectedOptions = !!$(this).closest('.field-row').find('select').val();

            $('#save-attributes-btn').prop('disabled', !checkSelectedOptions).toggleClass('active', checkSelectedOptions);
        });

        // Check attributes options

        // Get attributes

        $('#get-attributes-btn').on('click', function () {
            let _this = $(this),
                attributesBlock = $('#attributes-block'),
                url = _this.data('url'),
                attrSelect = $('#attributes'),
                selectedAttr = attrSelect.val(),
                selectedAttrBlock = $('.selected-attributes-block'),
                productTypeVal = $('#product_type').val(),
                productType = _this.data('productType') && _this.data('productType') === productTypeVal ? _this.data('productType') : productTypeVal;

            if (selectedAttr === null) {
                toastr.warning(['Select minim one attribute']);
                return false;
            }

            if (selectedAttrBlock.length === 0)
                return false;

            $.ajax({
                method: "POST",
                url: url,
                beforeSend: function () {
                    attributesBlock.addClass('loading');
                },
                data: {
                    itemIds: selectedAttr,
                    productType: productType
                }
            })
                .success(function (response) {
                    attributesBlock.removeClass('loading');

                    if (response.msg)
                        toastr[response.msg.type](response.msg.e);

                    if (response.status) {
                        _this.prop('disabled', true);
                        attrSelect.val('').trigger('change.select2');

                        if (response.view && response.selectedIds) {
                            if (selectedAttrBlock.find('.field-row').length > 0)
                                selectedAttrBlock.find('.field-row').last().after(response.view);
                            else
                                selectedAttrBlock.html(response.view);

                            selectedAttrBlock.find('select.select2').select2({closeOnSelect: false, dropdownCssClass: "multiple",}).siblings('.select2-container').append('<span class="select-all tooltip" title="Select all"></span>');

                            response.selectedIds.map((value) => {
                                attrSelect.find('option[value="' + value + '"]').prop('disabled', true);
                                attrSelect.select2('destroy').select2({closeOnSelect: false, width: '100%', dropdownCssClass: "multiple",}).siblings('.select2-container').append('<span class="select-all tooltip" title="Select all"></span>');
                            });

                            // saveAttrBtn.prop('disabled', false).addClass('active');
                        }
                    }


                })
                .fail(function (response) {
                    attributesBlock.removeClass('loading');

                    if (response.responseJSON) {
                        if (response.responseJSON.msg)
                            toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                    } else
                        toastr.error('Fail to send data');
                });

        });

        // Get attributes

        // Save attributes

        $('#save-attributes-btn').on('click', function () {
            let _this = $(this),
                selectedAttrBlock = $('.selected-attributes-block'),
                form = _this.parents('form'),
                hiddenField = '<input type="hidden" name="save_attributes_events" value="save_attributes">';

            if (!selectedAttrBlock.length || !form.length)
                return false;

            if (!selectedAttrBlock.children('.field-row').length) {
                toastr.warning(['Add minim one attribute']);
                return false;
            }

            if (!selectedAttrBlock.find('select option:selected').length) {
                toastr.warning(['Select minim one attribute option']);
                return false;
            }

            $(hiddenField).insertAfter(_this);
            form.find('button.submit-form-btn').trigger('click');
            // _this.prop('disabled', true).removeClass('active');
        });

        // Save attributes

        // Destroy attributes

        $(document).on('click', '.destroy-attribute', function () {
            if (!confirm("Do you want to remove this attribute?"))
                return false;

            let _this = $(this),
                productId = _this.data('productId'),
                itemId = _this.data('itemId'),
                attrSelect = $('#attributes'),
                saveAttrBtn = $('#save-attributes-btn');

            if (!itemId) {
                toastr.warning(['Attribute not exist']);
                return false;
            }

            if (!productId) {
                attrSelect.find('option[value="' + itemId + '"]').prop('selected', false).prop('disabled', false);
                attrSelect.select2('destroy').select2({closeOnSelect: false, width: '100%', dropdownCssClass: "multiple",
                }).siblings('.select2-container').append('<span class="select-all tooltip" title="Select all"></span>');

                _this.parents('.field-row[data-item-id="' + itemId + '"]').fadeOut(300, function () {
                    $(this).remove();
                });

                if (!_this.parents('.field-row').siblings('.field-row').length)
                    saveAttrBtn.prop('disabled', true).removeClass('active');

            } else {

                destroyAttributes(_this).then(function (response) {
                    if (response.status) {
                        if (response.confirmDeleteAttr && response.confirmMsg)
                            if (confirm(response.confirmMsg))
                                destroyAttributes(_this, 1);
                            else
                                return false;
                    }

                });
            }
        });

        // Destroy attributes

    });

    function destroyAttributes(that, confirmed = 0) {
        return new Promise((resolve, reject) => {
            let attributesBlock = $('#attributes-block'),
                url = that.data('url'),
                productId = that.data('productId'),
                itemId = that.data('itemId'),
                attrSelect = $('#attributes'),
                saveAttrBtn = $('#save-attributes-btn'),
                variationsBlock = $('#variations-block'),
                tabItem = variationsBlock.closest('.right-side').siblings('.left-side').find('.tab-item[data-type="variation"]');

            $.ajax({
                method: "POST",
                url: url,
                beforeSend: function () {
                    attributesBlock.addClass('loading');
                },
                data: {
                    productId: productId,
                    itemId: itemId,
                    confirmDeleteAttr: confirmed
                }
            })
                .success(function (response) {
                    resolve(response);
                    attributesBlock.removeClass('loading');

                    if (response.msg)
                        toastr[response.msg.type](response.msg.e);

                    if(!response.confirmDeleteAttr && response.status) {
                        tabItem.data('updateContent', true);

                        attrSelect.find('option[value="' + itemId + '"]').prop('selected', false).prop('disabled', false);
                        attrSelect.select2('destroy').select2().siblings('.select2-container').append('<span class="select-all tooltip" title="Select all"></span>');

                        that.parents('.field-row[data-item-id="' + itemId + '"]').fadeOut(300, function () {
                            $(this).remove();
                        });

                        if (!that.parents('.field-row').siblings('.field-row').length)
                            saveAttrBtn.prop('disabled', true).removeClass('active');
                    }
                })
                .fail(function (response) {
                    resolve(response);

                    attributesBlock.removeClass('loading');

                    if (response.responseJSON) {
                        if (response.responseJSON.msg)
                            toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                    } else
                        toastr.error('Fail to send data');
                });
        });
    }
})
(jQuery);
