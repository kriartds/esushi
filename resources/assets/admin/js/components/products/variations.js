;(function ($) {

    $(document).ready(function () {

        //    Toggle variations body

        $(document).on('click', '.toggle-variations-description', function () {
            $(this).toggleClass('open');

            if ($(this).hasClass('open')) {
                $('.variations-list').find('.variation-body').stop().slideDown(300);
                $('.variations-list').find('.toggle-variation-description').addClass('open');
            } else {
                $('.variations-list').find('.variation-body').stop().slideUp(300);
                $('.variations-list').find('.toggle-variation-description').removeClass('open');
            }
        });

        //    Toggle variations body

        //   Toggle variation body

        $(document).on('click', '.toggle-variation-description', function () {
            $(this).toggleClass('open');
            $(this).parents('.variation-header').next('.variation-body').stop().slideToggle(300);
        });

        //    Toggle variation body

        // Change content in variation body

        $(document).on('change', '#variations-block .variation-body input, .variation-body textarea, #variations-block .variations-items select, #variations-block .set-default-variation', function () {
            $('#save_variations_changes_block').find('button').prop('disabled', false);
        });

        $(document).on('click', '#gallery-insert-files', function () {
            if ($(this).data('isVariationsGallery'))
                $('#save_variations_changes_block').find('button').prop('disabled', false);
        });

        // Change content in variation body

        // Use stock variation

        $(document).on('change', 'input.use_stock_variation', function () {
            let _this = $(this);
            if (_this.prop('checked')) {
                _this.parents('.field-wrap-children').next('.field-wrap-children').find('.stock-quantity-block_variation').removeClass('hidden');
                _this.parents('.field-wrap-children').next('.field-wrap-children').find('.stock-status-block_variation').addClass('hidden');
            } else {
                _this.parents('.field-wrap-children').next('.field-wrap-children').find('.stock-quantity-block_variation').addClass('hidden');
                _this.parents('.field-wrap-children').next('.field-wrap-children').find('.stock-status-block_variation').removeClass('hidden');
            }
        });

        // Use stock variation

        // Check all variations usage

        $(document).on('change', '.set-default-variation', function () {
            let allDefaultVariations = $('.set-default-variation').not(this);
            allDefaultVariations.prop('checked', false);
        });

        // Check all variations usage

        // Change variation action

        $('#variation_actions').on('change', function () {
            let _this = $(this);
            let selectedVal = _this.val();
            let selectedOption = _this.find('option[value="' + selectedVal + '"]');
            let actionValueBlock = $('#variation-action-value-block');

            if (selectedOption.data('with-value'))
                actionValueBlock.show();
            else
                actionValueBlock.hide();
        });

        // Change variation action

        // Make variation

        $('#make_variation_action-btn').on('click', function () {
            let _this = $(this),
                url = _this.data('url'),
                destroyUrl = _this.data('destroy-url'),
                variationActions = $('#variation_actions'),
                action = variationActions.val(),
                selectedActionOption = variationActions.find('option[value="' + action + '"]'),
                actionValueBlock = $('#variation-action-value-block'),
                actionValueFiled = actionValueBlock.find('input'),
                itemId = _this.data('item-id'),
                variationsBlock = $('#variations-block'),
                variationsItems = variationsBlock.find('.variations-list').find('.variation-item'),
                variationsItemsCount = variationsItems.length * (-1),
                mainForm = _this.parents('form'),
                serializedMainForm = mainForm.serializeArray(),
                tabItem = variationsBlock.closest('.right-side').siblings('.left-side').find('.tab-item[data-type="variation"]');

            variationActions.find('option:selected').prop('selected', false).trigger('change.select2');
            actionValueBlock.hide();

            //  START Actions

            if (selectedActionOption.data('with-value')) {

                if (variationsBlock.find('.variations-list').find('.variation-item').length > 0) {
                    setVariationsActions(_this, action, actionValueFiled.val() ? actionValueFiled.val() : '');
                    actionValueFiled.val('');
                } else
                    toastr.warning(['Variations list is empty']);
            } else {
                if (action === 'destroy_variations') {
                    if (!confirm("Do you want destroy all variations?"))
                        return false;

                    let variationItems = variationsBlock.find('.variations-list').find('.variation-item');

                    if (variationItems.length === 0) {
                        toastr.warning(['Variations list is empty!']);
                        return false;
                    }

                    $.ajax({
                        method: "POST",
                        url: destroyUrl,
                        beforeSend: function () {
                            variationsBlock.addClass('loading');
                        },
                        data: {
                            itemId: itemId
                        }
                    })
                        .success(function (response) {
                            variationsBlock.removeClass('loading');

                            if (response.msg)
                                toastr[response.msg.type](response.msg.e);

                            if (response.status) {
                                variationsBlock.find('.variations-list .variations-items').html('');
                                variationsBlock.find('.pagination-block').html('');
                                variationsBlock.find('.save-variations-changes-block').removeClass('active');
                                $('.toggle-variations-description').addClass('hidden');
                            }
                        })
                        .fail(function (response) {
                            variationsBlock.removeClass('loading');

                            if (response.responseJSON) {
                                if (response.responseJSON.msg)
                                    toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                            } else
                                toastr.error('Fail to send data');
                        });
                } else if (action === 'add_variation' || action === 'add_variations_recursive') {
                    let defaultData = [
                        {name: 'action', value: action},
                        {name: 'itemId', value: itemId},
                        {name: 'countItems', value: variationsItemsCount},
                    ];

                    let serializeData = [];

                    if (serializedMainForm.length)
                        serializeData = serializedMainForm.filter((item) => {
                            return item.name.startsWith('item_variations') ||
                                item.name.startsWith('attributes_opt') ||
                                item.name === 'lang';
                        });

                    let data = [...defaultData, ...serializeData];

                    $.ajax({
                        method: "POST",
                        url: url,
                        beforeSend: function () {
                            variationsBlock.addClass('loading');
                        },
                        data: data,
                    })
                        .success(function (response) {
                            variationsBlock.removeClass('loading');

                            if (response.msg)
                                toastr[response.msg.type](response.msg.e);

                            if (response.status) {
                                if (!response.updateMultiple || (response.updateMultiple && response.updateMultipleView))
                                    window.getVariations(tabItem, false);

                                $('#save_variations_changes_block').addClass('active');
                                $('.toggle-variations-description').removeClass('hidden');
                            }


                        })
                        .fail(function (response) {
                            variationsBlock.removeClass('loading');

                            if (response.responseJSON) {
                                if (response.responseJSON.msg)
                                    toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                            } else
                                toastr.error('Fail to send data');
                        });
                } else {
                    setVariationsActions(_this, action);
                }
            }

            //  END Actions
        });

        // Save variations changes

        $('#save_variations_changes-btn').on('click', updateVariations);

        // Save variations changes

        // Destroy variation

        $(document).on('click', '.destroy-variation', function () {
            checkForChanges();

            let _this = $(this),
                url = _this.data('url'),
                itemId = _this.data('item-id'),
                variationsBlock = $('#variations-block'),
                variationId = _this.data('variation-id'),
                tabItem = variationsBlock.closest('.right-side').siblings('.left-side').find('.tab-item[data-type="variation"]');

            if (url && variationId && itemId)
                $.ajax({
                    method: "POST",
                    url: url,
                    beforeSend: function () {
                        variationsBlock.addClass('loading');
                    },
                    data: {
                        itemId: itemId,
                        variationId: variationId
                    }
                })
                    .success(function (response) {
                        variationsBlock.removeClass('loading');

                        if (response.msg)
                            toastr[response.msg.type](response.msg.e);

                        if (response.status)
                            window.getVariations(tabItem, false);


                    })
                    .fail(function (response) {
                        variationsBlock.removeClass('loading');

                        if (response.responseJSON) {
                            if (response.responseJSON.msg)
                                toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                        } else
                            toastr.error('Fail to send data');
                    });
            else
                _this.parents('.variation-item').fadeOut(300, function () {
                    $(this).remove();
                });
        });

        // Destroy variation

        // Make variation

        //    Paginate variations

        $(document).on('change', '#variations-block .pagination-block select[name="paginate_variations"]', function () {
            checkForChanges();

            let variationsBlock = $('#variations-block'),
                paginateVariationsSelect = variationsBlock.find('.pagination-block select[name="paginate_variations"]').not(this),
                currVal = $(this).val(),
                tabItem = variationsBlock.closest('.right-side').siblings('.left-side').find('.tab-item[data-type="variation"]');

            if (paginateVariationsSelect.length)
                paginateVariationsSelect.val(currVal);

            window.getVariations(tabItem, false, currVal);

        });

        $(document).on('click', '#variations-block .pagination-block .first-last-page', function () {
            let variationsBlock = $('#variations-block'),
                paginateVariationsSelects = variationsBlock.find('.pagination-block select[name="paginate_variations"]'),
                paginateVariationsSelect = paginateVariationsSelects.first();

            if (!paginateVariationsSelect.length || !paginateVariationsSelects.length || ($(this).hasClass('prev') && parseInt(paginateVariationsSelect.val()) === 1) || ($(this).hasClass('next') && parseInt(paginateVariationsSelect.data('lastPage')) === parseInt(paginateVariationsSelect.val())))
                return false;

            paginateVariationsSelects.find('option').prop('selected', false);

            if ($(this).hasClass('prev'))
                paginateVariationsSelect.find('option').first().prop('selected', true).trigger('change');
            else
                paginateVariationsSelect.find('option').last().prop('selected', true).trigger('change');

        });

        //    Paginate variations

    });

    window.getVariations = function (tabItem, isDirectAccess = true, currPage = null) {
        let url = tabItem.data('url'),
            updateContent = tabItem.data('update-content'),
            itemId = tabItem.data('itemId'),
            variationsBlock = $('#variations-block'),
            variationsListBlock = variationsBlock.find('.variations-list .variations-items');

        if (!url || !variationsListBlock.length || (!updateContent && isDirectAccess))
            return false;

        $.ajax({
            method: "POST",
            url: url,
            beforeSend: function () {
                variationsBlock.addClass('loading');
            },
            data: {
                itemId: itemId,
                currPage: currPage || 1
            }
        })
            .success(function (response) {
                variationsBlock.removeClass('loading');

                if (response.msg)
                    toastr[response.msg.type](response.msg.e);

                if (response.status) {
                    tabItem.data('update-content', false);
                    $('#save_variations_changes_block').addClass('active');

                    if(!isDirectAccess)
                        variationsBlock.find('.default-variation-block').html(response.defaultVariationView);

                    updateVariationsBlock(response.view, !currPage ? response.paginationView : null);

                    if(!response.view)
                        $('#save_variations_changes_block').removeClass('active');
                }


            })
            .fail(function (response) {
                variationsBlock.removeClass('loading');

                if (response.responseJSON) {
                    if (response.responseJSON.msg)
                        toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                } else
                    toastr.error('Fail to send data');
            });
    };

    function updateVariations(actionValues = {}) {
        let saveChangesBtn = $('#save_variations_changes-btn'),
            url = saveChangesBtn.data('url'),
            itemId = saveChangesBtn.data('item-id'),
            mainForm = saveChangesBtn.parents('form'),
            serializedMainForm = mainForm.serializeArray(),
            variationsBlock = $('#variations-block');

        let serializeData = [];

        if (serializedMainForm.length)
            serializeData = serializedMainForm.filter((item) => {
                return item.name.startsWith('item_variations') ||
                    item.name.startsWith('attributes_opt') ||
                    item.name === 'lang';
            });

        serializeData.push({name: 'itemId', value: itemId});

        if (actionValues instanceof Object && Object.keys(actionValues).length)
            serializeData.push(actionValues);

        $.ajax({
            method: "POST",
            url: url,
            beforeSend: function () {
                variationsBlock.addClass('loading');
            },
            data: serializeData
        })
            .success(function (response) {
                variationsBlock.removeClass('loading');

                if (response.msg)
                    toastr[response.msg.type](response.msg.e);

                if (response.status) {
                    saveChangesBtn.prop('disabled', true);
                    variationsBlock.find('.default-variation-block').html(response.defaultVariationView);
                }


            })
            .fail(function (response) {
                variationsBlock.removeClass('loading');

                if (response.responseJSON) {
                    if (response.responseJSON.msg)
                        toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                } else
                    toastr.error('Fail to send data');
            });
    }

    function setVariationsActions(that, action, value) {
        let itemId = that.data('itemId'),
            url = that.data('actionUrl'),
            variationsBlock = $('#variations-block'),
            tabItem = variationsBlock.closest('.right-side').siblings('.left-side').find('.tab-item[data-type="variation"]');

        if (!action || !itemId || !url)
            return false;

        $.ajax({
            method: "POST",
            url: url,
            beforeSend: function () {
                variationsBlock.addClass('loading');
            },
            data: {
                itemId: itemId,
                action: action,
                value: value
            }
        })
            .success(function (response) {
                variationsBlock.removeClass('loading');

                if (response.msg)
                    toastr[response.msg.type](response.msg.e);

                if (response.status)
                    window.getVariations(tabItem, false);

            })
            .fail(function (response) {
                variationsBlock.removeClass('loading');

                if (response.responseJSON) {
                    if (response.responseJSON.msg)
                        toastr[response.responseJSON.msg.type](response.responseJSON.msg.e);
                } else
                    toastr.error('Fail to send data');
            });
    }

    function updateVariationsBlock(variationsHtml, paginationHtml) {
        let timeout = 0,
            variationsBlock = $('#variations-block');

        variationsBlock.find('.variations-list .variations-items').html(variationsHtml);

        if (paginationHtml)
            variationsBlock.find('.pagination-block').html(paginationHtml);

        chunkArray(variationsBlock.find('.variations-list').children('.variations-items').find('select.select2'), 10).map((value) => {
            timeout += 10;

            setTimeout(function () {
                $(value).select2();

                //Drag images

                if (variationsBlock.find('.files-list .img-block').length > 0) {
                    variationsBlock.find('.files-list .img-block').arrangeable('destroy');
                    variationsBlock.find('.files-list .img-block').arrangeable({
                        'ajaxOption': {
                            'method': 'post',
                            'url': `${window.location.origin}/${globalLang}/admin/changeFilePosition`
                        }
                    });
                }

                //Drag images
            }, timeout);
        });

        setTimeout(function () {
            tippy('.tooltip', tippyOptions);
        }, 500);

        return true;
    }

    function checkForChanges() {
        let saveVariationsChangesBtn = $('#save_variations_changes_block').find('button');
        let needUpdate = !saveVariationsChangesBtn.prop('disabled');
        if (needUpdate) {
            if (!confirm("Do you want to save variations changes?")) {
                saveVariationsChangesBtn.prop('disabled', true);
                return false;
            }

            updateVariations();
        }
        return true;
    }

})
(jQuery);
