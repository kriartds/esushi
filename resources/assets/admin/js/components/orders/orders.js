;(function ($) {

    $(document).ready(function () {
        $(document).on('select2:select', '#pickup_country', function () {
            getPickupRegionsStores($(this));
        });

        $(document).on('select2:select', '#pickup_region', function () {
            getPickupRegionsStores($('#pickup_country'), $(this));
        });

        $('.open-close-fields-order').on('click', function () {

            $('#short-client-info-block').addClass('hidden');
            $('#short-client-shipping-info-block').closest('.extra-large').addClass('hidden');

            $('#edit-client-info-block').removeClass('hidden');
            $('#pickup-shipping-edit-block').removeClass('hidden');
        });

        $(document).on('click','#pickup-shipping-edit-block .tab-item', function () {
            $(this).find('input').prop("checked", true);
        });

        /** Add products*/

        $(document).on('select2:select', '#chose-order-products', function () {
            getProduct($(this));
        });

        $(document).on('click', '.remove-order-product', function () {
            let productVariationBlock = $(this).closest('.chose-product-variation-block'),
                allProductVariationBlock = productVariationBlock.siblings('.chose-product-variation-block'),
                addProductsForm = $('#add-order-products-form'),
                emptyOrdersProductList = addProductsForm.find('.empty-orders-product-list');

            productVariationBlock.fadeOut(300, function () {
                if (!allProductVariationBlock.length)
                    emptyOrdersProductList.show();
                $(this).remove();
                manipulateAddOrderProductsBtn();
            });
        });

        $(document).on('change', '#add-order-products-form select, #add-order-products-form input:radio', function () {
            let _this = $(this);


            _this.parents('.attribute-item-container').find('.attribute-item-wrap').removeClass('active');
            _this.parents('.attribute-item-wrap').addClass('active');

            updateProductAttributes(_this);
            manipulateAddOrderProductsBtn();

        });

        $(document).on("keyup", ".update-product-quantity", function (evt) {
            let _this = $(this);
            updateProductQuantity(evt, _this);
        }).on('keypress', ".update-product-quantity", function (e) {
            if (e.which === 13) {
                e.preventDefault();
                return false;
            }
        });

        $('#apply-coupon-btn').on('click', function () {
            let couponFieldsBlock = $(this).closest('.actions-block').find('.coupon-fields-wrap');
            couponFieldsBlock.toggleClass('hidden');
            $(this).toggleClass('active', !couponFieldsBlock.hasClass('hidden'))
        });

        $('#coupon-code').on('keypress', function (e) {
            if (e.which === 13) {
                e.preventDefault();
                $('#apply-coupon-code').trigger('click');
            }
        });

        $('#apply-coupon-code').on('click', function () {
            let couponCodeField = $('#coupon-code'),
                couponCode = couponCodeField.val(),
                couponFieldsBlock = $(this).closest('.coupon-fields-wrap'),
                url = $(this).data('url'),
                couponsBlock = $('#coupons-block');

            if (!couponCode) {
                couponCodeField.addClass('error-input');
                return false;
            }

            $.ajax({
                method: 'POST',
                url: url,
                beforeSend: function () {
                    couponFieldsBlock.addClass('loading');
                },
                data: {
                    couponCode: couponCode
                },
            }).success(function (response) {
                couponFieldsBlock.removeClass('loading');

                if (response.msg)
                    toastr[response.msg.type](response.msg.e);

                if (response.status) {
                    manipulateTotalsBlock(response);

                    if (couponsBlock.length && response.view)
                        couponsBlock.html(response.view);

                    couponFieldsBlock.addClass('hidden').find('#coupon-code').val('');
                }

            }).fail(function (response) {
                couponFieldsBlock.removeClass('loading');
                let jsonResponse = response.responseJSON;

                if (jsonResponse.msg)
                    toastr[jsonResponse.msg.type](jsonResponse.msg.e);
            });
        });

        $(document).on('click', '.remove-coupon', function () {
            let _this = $(this),
                couponId = _this.data('id'),
                url = _this.data('url'),
                couponWrap = _this.closest('#coupons-block'),
                couponItem = _this.closest('.coupon-item');

            $.ajax({
                method: 'POST',
                url: url,
                beforeSend: function () {
                    couponItem.addClass('loading');
                },
                data: {
                    couponId: couponId
                },
            }).success(function (response) {
                couponItem.removeClass('loading');

                if (response.msg)
                    toastr[response.msg.type](response.msg.e);

                if (response.status) {
                    manipulateTotalsBlock(response);

                    couponWrap.html('');
                }

            }).fail(function (response) {
                couponItem.removeClass('loading');
                let jsonResponse = response.responseJSON;

                if (jsonResponse.msg)
                    toastr[jsonResponse.msg.type](jsonResponse.msg.e);
            });
        });

        $('#add-order-products').on('click', function () {
            let _this = $(this),
                currModal = _this.closest('#modal-add-order-products'),
                url = _this.data('url'),
                addOrderProductsForm = $('#add-order-products-form'),
                orderForm = $('form.orders-form'),
                manipulateAddOrderProductsBtnResponse = manipulateAddOrderProductsBtn(),
                orderProductsTable = $('#order-products-table'),
                choseOrderProductsSelect = $('#chose-order-products'),
                applyCouponCodeBtn = $('#apply-coupon-code');

            if (!addOrderProductsForm.length || !orderForm.length || manipulateAddOrderProductsBtnResponse)
                return false;

            let addProductsData = addOrderProductsForm.serializeArray().filter(item => {
                    return item.name.includes('order_products[variation_id]') || item.name.includes('order_products[count]') || item.name.includes('order_products[id]') || item.name.includes('attributes');
                }),
                orderFormData = orderForm.serializeArray().filter(item => item.name !== 'coupon_code'),
                data = [...addProductsData, ...orderFormData, ...[{name: 'isDraft', value: 1}]];

            addOrderProductsForm.find('input').removeClass('error-input').attr('title', '');

            $.ajax({
                method: 'POST',
                url: url,
                beforeSend: function () {
                    currModal.addClass('loading');
                },
                data: data,
            }).success(function (response) {
                currModal.removeClass('loading');

                if (response.msg && !response.orderValidator)
                    toastr[response.msg.type](response.msg.e);

                if (response.status) {
                    if (response.isDraft && response.view && orderProductsTable.length) {
                        manipulateTotalsBlock(response);
                        orderProductsTable.find('tbody').html(response.view);

                        orderProductsTable.closest('.order-products-block').removeClass('hidden');

                        currModal.arcticmodal('close');
                        setTimeout(function () {
                            addOrderProductsForm.find('.chose-product-variation-block').remove();
                            addOrderProductsForm.find('.empty-orders-product-list').show();
                            manipulateAddOrderProductsBtn();
                        }, 300);

                        if (response.editUrl) {
                            window.history.pushState({path: response.editUrl}, '', response.editUrl);

                            let headerBtn = $('.page-top-buttons').find('.item a.active');
                            if (headerBtn.length) {
                                headerBtn.removeClass('active');
                                let cloneHeaderBtnItem = headerBtn.closest('.item').clone();
                                cloneHeaderBtnItem.find('a').attr('href', response.editUrl).addClass('active').text('Edit');

                                headerBtn.closest('.item').after(cloneHeaderBtnItem);
                            }

                            if (response.orderId && choseOrderProductsSelect.length) {
                                let newChoseProductsUrl = removeURLParameter(choseOrderProductsSelect.data('url'), 'cartId') + `?cartId=${response.orderId}`,
                                    orderFormUrlSegments = parseURL(orderForm.attr('action')).pathname.split('/').filter(item => item),
                                    addOrderProductsUrlSegments = parseURL(url).pathname.split('/').filter(item => item),
                                    applyCouponCodeBtnUrlSegments = parseURL(applyCouponCodeBtn.data('url')).pathname.split('/').filter(item => item);

                                choseOrderProductsSelect.data('url', newChoseProductsUrl);

                                if (orderFormUrlSegments[3] === 'save' && !orderFormUrlSegments[4]) {
                                    orderFormUrlSegments.push(response.orderId);
                                    orderForm.attr('action', `${window.location.origin}/${orderFormUrlSegments.join('/')}`);
                                }

                                if (addOrderProductsUrlSegments[3] === 'addOrderProducts' && !addOrderProductsUrlSegments[4]) {
                                    addOrderProductsUrlSegments.push(response.orderId);
                                    _this.data('url', `${window.location.origin}/${addOrderProductsUrlSegments.join('/')}`);
                                }

                                if (applyCouponCodeBtnUrlSegments[3] === 'checkCoupon' && !applyCouponCodeBtnUrlSegments[4]) {
                                    applyCouponCodeBtnUrlSegments.push(response.orderId);
                                    applyCouponCodeBtn.data('url', `${window.location.origin}/${applyCouponCodeBtnUrlSegments.join('/')}`);
                                }
                            }

                            if (response.generatedOrderId)
                                $('#generated-order-id').text(response.generatedOrderId);
                        }

                        if (response.existProductsIds.length && response.existProductsMsg) {
                            toastr[response.existProductsMsg.type](response.existProductsMsg.e);

                            response.existProductsIds.map(productId => {
                                orderProductsTable.find(`tr[data-order-product-id=${productId}]`).addClass('isDuplicate');
                            });

                            $('html, body').stop().animate({scrollTop: orderProductsTable.find('.isDuplicate').first().offset().top - 100}, 1000);
                        }

                        $('#apply-coupon-btn').removeClass('hidden');
                    }

                } else {

                    if (response.scrollToProducts && response.outOfStockProducts.length) {
                        $('html, body').stop().animate({scrollTop: orderProductsTable.find(`tr[data-order-product-id="${response.outOfStockProducts[0].id}"]`).offset().top - 100}, 1000);

                        response.outOfStockProducts.map(item => {
                            orderProductsTable.find(`tr[data-order-product-id="${item.id}"]`).find('.product-quantity').addClass('decreasedStock').find('span').attr('title', item.qtyMsg);
                        });
                    }


                    if (response.orderValidator && response.msg) {
                        $.each(response.msg.e, function (ObjNames, ObjValues) {
                            let field = addOrderProductsForm.find(`[name="${ObjNames}"]`);

                            if (field.length)
                                field.addClass('error-input').attr('title', ObjValues);
                        });

                        if (addOrderProductsForm.find('.error-input').first().length > 0)
                            addOrderProductsForm.stop().animate({scrollTop: addOrderProductsForm.find('.error-input').first().offset().top - addOrderProductsForm.offset().top + addOrderProductsForm.scrollTop() - 40}, 1000);
                    }
                }

            }).fail(function (response) {
                currModal.removeClass('loading');
                let jsonResponse = response.responseJSON;

                if (jsonResponse.msg)
                    toastr[jsonResponse.msg.type](jsonResponse.msg.e);
            });
        });

        $(document).on('click', '.edit-product-order', function () {
            let _this = $(this),
                currTr = _this.closest('tr'),
                qtyBlock = currTr.find('td.product-quantity'),
                changeProductStockBlock = qtyBlock.find('.change-product-stock');

            _this.toggleClass('active');
            changeProductStockBlock.toggleClass('hidden', !_this.hasClass('active'));
            qtyBlock.find('span').toggleClass('hidden', _this.hasClass('active'));

        }).on('keypress', ".update-product-quantity", function (e) {
            if (e.which === 13) {
                e.preventDefault();
                return false;
            }
        });
        $(document).on('keypress', '.product-quantity .change-product-stock .update-product-quantity', function (e) {

            if (e.which === 13) {
                e.preventDefault();
                console.log("intra enter");
                $('.update-product-quantity').trigger('change');
            }
        });
        $(document).on('change', '.update-product-quantity', function (e) {
            let qtyField = $(this);
                qty = qtyField.val(),
                qtyFieldsBlock = $(this).closest('.change-product-stock'),
                url = $(this).data('url'),
                productQtyBlock = $(this).closest('.product-quantity');

            if (!qty) {
                qtyField.addClass('error-input');
                return false;
            }

            $.ajax({
                method: 'POST',
                url: url,
                beforeSend: function () {
                    qtyFieldsBlock.addClass('loading');
                },
                data: {
                    qty: qty,
                    productId: $(this).closest('tr').data('order-product-id')
                },
            }).success(function (response) {
                qtyFieldsBlock.removeClass('loading');

                if (response.msg)
                    toastr[response.msg.type](response.msg.e);

                if (response.status) {
                    manipulateTotalsBlock(response);

                    if(response.productSubtotal)
                        productQtyBlock.siblings('td.order-products-subtotal').html(response.productSubtotal);
                }

            }).fail(function (response) {
                qtyFieldsBlock.removeClass('loading');
                let jsonResponse = response.responseJSON;

                if (jsonResponse.msg)
                    toastr[jsonResponse.msg.type](jsonResponse.msg.e);
            });
        });
        $(document).on('click', '.destroy-product-order', function () {
            let _this = $(this),
                url = _this.data('url'),
                orderProductsBlock = _this.closest('.order-products-block'),
                currTr = _this.closest('tr'),
                productId = currTr.data('order-product-id');

            $.ajax({
                method: 'POST',
                url: url,
                beforeSend: function () {
                    orderProductsBlock.addClass('loading');
                },
                data: {
                    productId: productId
                },
            }).success(function (response) {
                orderProductsBlock.removeClass('loading');

                if (response.msg)
                    toastr[response.msg.type](response.msg.e);

                if (response.status) {
                    manipulateTotalsBlock(response);

                    currTr.fadeOut(300, function () {
                        $(this).remove();

                        if (response.count === 0) {
                            $('#apply-coupon-btn').addClass('hidden');
                            orderProductsBlock.addClass('hidden');
                        }
                    });
                }

            }).fail(function (response) {
                orderProductsBlock.removeClass('loading');
                let jsonResponse = response.responseJSON;

                if (jsonResponse.msg)
                    toastr[jsonResponse.msg.type](jsonResponse.msg.e);
            });
        });

        /** Add products*/
    });

    function getPickupRegionsStores(country, region) {
        let pickupBlock = $('#edit-client-pickup-block'),
            url = pickupBlock.data('url');

        $.ajax({
            method: 'POST',
            url: url,
            beforeSend: function () {
                pickupBlock.addClass('loading');
            },
            data: {
                countryId: country.val(),
                regionId: region !== undefined ? region.val() : ''
            },
        }).success(function (response) {
            pickupBlock.removeClass('loading');

            if (response.msg)
                toastr[response.msg.type](response.msg.e);

            if (response.status && pickupBlock.length) {
                pickupBlock.html(response.view);

                if (response.view) {
                    setTimeout(function () {
                        pickupBlock.find('select.select2').select2();
                        pickupBlock.find('select.select2.no-search').select2({
                            minimumResultsForSearch: -1
                        });
                    }, 100);
                }

            }
        }).fail(function (response) {
            pickupBlock.removeClass('loading');
            let jsonResponse = response.responseJSON;

            if (jsonResponse.msg)
                toastr[jsonResponse.msg.type](jsonResponse.msg.e);
        });
    }

    function getProduct(that) {
        let productId = that.val(),
            url = that.data('product-content-url'),
            addProductsForm = $('#add-order-products-form'),
            addedProductsCount = 0,
            emptyOrdersProductList = addProductsForm.find('.empty-orders-product-list');

        if (addProductsForm.find('.chose-product-variation-block').length) {
            let allIncrements = [];
            addProductsForm.find('.chose-product-variation-block').map((key, item) => {
                allIncrements.push(parseInt($(item).data('increment')))
            });
            addedProductsCount = Math.max(...allIncrements.map(item => item));
        }

        $.ajax({
            method: 'POST',
            url: url,
            beforeSend: function () {
                addProductsForm.addClass('loading');
            },
            data: {
                productId: productId,
                productsCount: addedProductsCount
            },
        }).success(function (response) {
            addProductsForm.removeClass('loading');

            if (response.msg)
                toastr[response.msg.type](response.msg.e);

            if (response.status) {
                emptyOrdersProductList.hide();

                if (addProductsForm.length) {
                    let productsIdsVal = addProductsForm.find('input[type=hidden][name="order_products[id][]"]').serializeArray(),
                        productsIds = [],
                        variationsFieldVal = addProductsForm.find(`input[type=hidden][name="variations[${response.productId}]"]`).val(),
                        variationId = addProductsForm.find(`input[type=hidden][name^="order_products[variation_id][${response.productId}]"]`).val(),
                        isCheckedVariationProduct = variationsFieldVal !== undefined && variationsFieldVal !== '[]' && variationId !== undefined && variationId;

                    console.log(variationId,'variationID');

                    if (productsIdsVal.length)
                        productsIdsVal.map(item => {
                            productsIds.push(parseInt(item.value));
                        });

                    productsIds.filter(arrUnique);

                    let prependNewProduct = !productsIds.length || (productsIds.length && (!productsIds.includes(response.productId) || (productsIds.includes(response.productId) && isCheckedVariationProduct)));

                    if (productsIds.length) {
                        if (productsIds.includes(response.productId) && !isCheckedVariationProduct) {
                            let qtyField = addProductsForm.find(`.chose-product-variation-block[data-product-id=${response.productId}]`).find(`#count-items-${response.productId}`);
                            if (qtyField.length)
                                qtyField.val(parseInt(qtyField.val()) !== undefined ? parseInt(qtyField.val()) + 1 : 1);
                        }
                    }

                    if (prependNewProduct) {
                        addProductsForm.prepend(response.view);
                        manipulateAddOrderProductsBtn();

                        if (response.view) {
                            setTimeout(function () {
                                addProductsForm.find('select.select2').select2({
                                    templateResult: select2FormatState
                                });
                                addProductsForm.find('select.select2.no-search').select2({
                                    minimumResultsForSearch: -1,
                                    templateResult: select2FormatState
                                });
                            }, 100);
                        }
                    }

                }

                that.val('').trigger('change');
            }

        }).fail(function (response) {
            addProductsForm.removeClass('loading');
            let jsonResponse = response.responseJSON;

            if (jsonResponse.msg)
                toastr[jsonResponse.msg.type](jsonResponse.msg.e);
        });
    }

    function updateProductAttributes(that) {
        let currFormBlock = that.closest('.chose-product-variation-block'),
            currProductId = currFormBlock.data('product-id'),
            variationsStr = currFormBlock.find(`input[type=hidden][name="variations[${currProductId}]"]`).val();

        if (variationsStr === '')
            return false;

        let allVariations = JSON.parse(variationsStr);

        if (!Object.keys(allVariations).length)
            return false;

        if (that.prop('tagName') !== 'INPUT' && that.prop('tagName') !== 'SELECT')
            return false;

        let allSelectedAttributesIds = [],
            variations = allVariations.variations,
            variationsArr = Object.keys(variations),
            unavailableVariationsAttrIds = [],
            availableVariationsAttrIds = [],
            allAttributesArr = [],
            serializeForm = currFormBlock.find(':input').serializeArray(),
            allFormWraps = currFormBlock.find('.attribute-item-wrap'),
            formAttributesFields = allFormWraps.closest('.attribute-item-container'),
            allAttributesArrIds = [];

        /** Get all selected attributes*/



        if (serializeForm.length)
            serializeForm.map((item) => {
                if (item.name.startsWith('order_products[attributes]')){
                    allSelectedAttributesIds.push(parseInt(item.value));
                 }

            });

        allSelectedAttributesIds = allSelectedAttributesIds.filter(arrUnique);

        /** END Get all selected attributes*/

        /** Update all attributes*/

        // console.log(allFormWraps, 'all');

        updateInputsAndSelects(that, allFormWraps);

        /** END Update all attributes*/

        // console.log(variationsArr);

        if (variationsArr.length) {

            /** Get all attributes from all variations*/

            variationsArr.map((variationId) => {
                let item = variations[variationId],
                    attributes = item.attributes;

                if (attributes instanceof Array) {
                    attributes.map(attrs => {
                        allAttributesArrIds.push(Object.values(attrs));
                        allAttributesArr.push(attrs);
                    });
                } else {
                    allAttributesArrIds.push(Object.values(attributes));
                    allAttributesArr.push(attributes);
                }
            });

            /** END Get all attributes from all variations*/

            /** Get all variations*/

            let chosenAttributes = getChosenAttributes(formAttributesFields),
                currentAttributes = chosenAttributes.data,
                checkIfAllAttrIsCheeked = chosenAttributes.count === chosenAttributes.chosenCount;

            formAttributesFields.each((key, item) => {
                let currentAttrName = $(item).data('itemSlug');

                let checkAttributes = $.extend(true, {}, currentAttributes);
                checkAttributes[currentAttrName] = '';

                let variations = findMatchingVariations(allAttributesArr, checkAttributes);

                for (let num in variations) {
                    if (typeof (variations[num]) !== 'undefined') {
                        let variationAttributes = variations[num];

                        for (let attr_name in variationAttributes) {
                            if (variationAttributes.hasOwnProperty(attr_name)) {
                                let attr_val = variationAttributes[attr_name];

                                if (attr_name === currentAttrName)
                                    availableVariationsAttrIds.push(attr_val);
                            }
                        }
                    }
                }
            });


            allAttributesArrIds = allAttributesArrIds.flat().filter(arrUnique);
            availableVariationsAttrIds = availableVariationsAttrIds.filter(arrUnique);
            unavailableVariationsAttrIds = arrDiff(allAttributesArrIds, availableVariationsAttrIds);

            /** Get all variations*/

            if (unavailableVariationsAttrIds.length) {
                unavailableVariationsAttrIds.map((itemId) => {
                    allFormWraps.find(`input[type=radio][value="${itemId}"]`).siblings('label').addClass('disabled');
                    allFormWraps.find(`select option[value="${itemId}"]`).addClass('disabled');
                });

            }

            manipulateWithVariation(that, !checkIfAllAttrIsCheeked);
        }
    }

    function manipulateWithVariation(that, reset) {

        let currVariation = getCurrVariation(that),
            currFormBlock = that.closest('.chose-product-variation-block'),
            currProductId = currFormBlock.data('product-id'),
            currIteration = currFormBlock.data('increment');


        reset = !(!reset || typeof reset === undefined);


        if (!Object.keys(currVariation).length && !reset)
            return false;

        if (Object.values(currVariation).length && !currVariation.price)
            return false;

        let infoProductStockBlock = currFormBlock.find(`#info-product-stock-${currProductId}--${currIteration}`),
            priceProductBlock = currFormBlock.find(`#price-product-block-${currProductId}--${currIteration}`),
            countItemsBlock = currFormBlock.find(`#count-items-block-${currProductId}--${currIteration}`),
            countItemsInput = countItemsBlock.find(`input#count-items-${currProductId}--${currIteration}`),
            variationIdInput = currFormBlock.find(`input[type="hidden"][name="order_products[variation_id][${currProductId}][${currIteration}]"]`),
            priceBlock = '',
            imgBlock = that.closest('.chose-product-variation-block').find('.product-img'),
            variationIdBlock = currFormBlock.find(`#variation-id-block-${currProductId}--${currIteration}`),
            salePercentageBlock = currFormBlock.find(`#product-sale-percentage-${currProductId}--${currIteration}`);

        // console.log(variationIdInput, 'variationIdInput');

        if (variationIdBlock.length)
            variationIdBlock.toggleClass('withoutId', !currVariation.id).text(currVariation.id ? `#${currVariation.id}` : '');

        if (infoProductStockBlock.length)
            infoProductStockBlock.toggleClass('outOfStock', !currVariation.stock_qty).find('span').text(currVariation.stock ? currVariation.stock : '');

        if (priceProductBlock.length && currVariation.price) {
            if (currVariation.price)
                priceBlock += `<span class="price ${currVariation.sale_price ? 'cut' : ''}">${currVariation.sale_price ? currVariation.sale_price : currVariation.price}</span>`;

            if (currVariation.sale_price) {
                priceBlock += `<span class="lastPrice">${currVariation.price}</span>`;

                if (currVariation.productPromotionPercentage && salePercentageBlock.length)
                    salePercentageBlock.html(`<span>(-${currVariation.productPromotionPercentage}%)</span>`);
            } else
                salePercentageBlock.html('');

            priceProductBlock.html(priceBlock);
        }

        if (countItemsBlock.length && countItemsInput.length) {
            countItemsBlock.toggleClass('hidden', !currVariation.stock_qty);
            countItemsInput.data('max-value', $.isNumeric(currVariation.stock_qty) ? currVariation.stock_qty : '').val(1);
        }



        if (variationIdInput.length) {
            variationIdInput.val(!false ? currVariation.id : '');
            /*variationIdInput.val(!reset ? currVariation.id : '');*/
        }

        if (!reset && imgBlock.length && Object.keys(currVariation.productFiles).length) {
            let firstFile = '';

            if (Object.keys(currVariation.files).length && currVariation.files.small.length) {
                firstFile = currVariation.files.small[0];
            } else if (currVariation.productFiles.small.length) {
                firstFile = currVariation.productFiles.small[0];
            }

            if (firstFile.length)
                imgBlock.find('img').attr('src', firstFile);
        }
    }

    function updateInputsAndSelects(that, allFormWraps) {

        if (that.prop('tagName') === 'INPUT') {
            if (that.siblings('label').hasClass('disabled')) {
                allFormWraps.removeClass('active').find('input[type="radio"]').prop('checked', false);
                allFormWraps.find('select option').removeClass('disabled').prop('selected', false);
                that.prop('checked', true).parents('.attribute-item-wrap').addClass('active');

                manipulateWithVariation(that, true);
            }
        } else if (that.prop('tagName') === 'SELECT') {
            let currVal = that.val();
            let disabledOptionValue = that.find(`option.disabled[value="${currVal}"]`);
            if (disabledOptionValue.length) {
                allFormWraps.removeClass('active').find('input[type="radio"]').prop('checked', false);
                allFormWraps.find('select option').removeClass('disabled').prop('selected', false);
                that.find(`option[value="${currVal}"]`).prop('selected', true).parents('.attribute-item-wrap').addClass('active');

                manipulateWithVariation(that, true);
            }
        }

        allFormWraps.find('label').removeClass('disabled');
        allFormWraps.find('select').find('option').removeClass('disabled').trigger('change.select2');
    }

    function findMatchingVariations(allAttributesArr, attributes) {
        let matching = [];

        for (let i = 0; i < allAttributesArr.length; i++) {
            let attribute = allAttributesArr[i];

            if (isMatch(attribute, attributes))
                matching.push(attribute);
        }
        return matching;
    }

    function isMatch(variation_attributes, attributes) {
        let match = true;

        for (let attrName in variation_attributes) {

            if (variation_attributes.hasOwnProperty(attrName)) {
                let val1 = variation_attributes[attrName];
                let val2 = parseInt(attributes[attrName]);

                if (val1 !== undefined && val2 !== undefined && !isNaN(val1) && !isNaN(val2) && val1 !== val2)
                    match = false;
            }
        }
        return match;
    }

    function getChosenAttributes(formAttributesFields) {
        let data = {};
        let count = 0;
        let chosen = 0;

        formAttributesFields.each((index, item) => {
            let attributeName = $(item).data('itemSlug'),
                attributeType = $(item).data('itemType'),
                // attributesField = attributeType !== 'select' ? $(item).find('[name^=attributes]:checked') : $(item).find('[name^=attributes]'),
                attributesField = attributeType !== 'select' ? $(item).find('[name^="order_products[attributes]"]:checked') : $(item).find('[name^="order_products[attributes]"]'),
                value;

            value = attributesField.val() || '';

            if (value.length > 0)
                chosen++;

            count++;
            data[attributeName] = value;
        });

        return {
            'count': count,
            'chosenCount': chosen,
            'data': data
        }
    }

    function getCurrVariation(that) {
        let currFormBlock = that.closest('.chose-product-variation-block'),
            currProductId = currFormBlock.data('product-id'),
            variationsStr = currFormBlock.find(`input[type=hidden][name="variations[${currProductId}]"]`).val();

        if (!variationsStr.length)
            return {};

        let allSelectedAttributesIds = [],
            allVariations = JSON.parse(variationsStr),
            variations = allVariations.variations,
            variationsArr = Object.keys(variations),
            serializeForm = currFormBlock.find(':input').serializeArray(),
            allFormWraps = currFormBlock.find('.attribute-item-wrap'),
            formAttributesFields = allFormWraps.closest('.attribute-item-container'),
            currVariation = {},
            chosenAttributes = getChosenAttributes(formAttributesFields);

        if (serializeForm.length)
            serializeForm.map((item) => {
                if (item.name.startsWith('order_products[attributes]'))
                    allSelectedAttributesIds.push(parseInt(item.value));
            });

        allSelectedAttributesIds = allSelectedAttributesIds.filter(arrUnique);

        let checkIfAllAttrIsCheeked = chosenAttributes.count === chosenAttributes.chosenCount;

        variationsArr.map((variationId) => {
            let item = variations[variationId],
                attributes = item.attributes;


            if (attributes instanceof Array) {
                attributes.map(attrs => {
                    /*if (checkIfAllAttrIsCheeked && arraysEqual(Object.values(attrs).sort(), allSelectedAttributesIds.sort())){

                    }*/
                    if ( arraysEqual(Object.values(attrs).sort(), allSelectedAttributesIds.sort())) {
                        currVariation = item.variation;
                        currVariation['id'] = parseInt(variationId);
                    }
                });
            } else {
                /*if (checkIfAllAttrIsCheeked && arraysEqual(Object.values(attributes).sort(), allSelectedAttributesIds.sort())) {

                }*/
                if ( arraysEqual(Object.values(attributes).sort(), allSelectedAttributesIds.sort())) {
                    currVariation = item.variation;
                    currVariation['id'] = parseInt(variationId);
                }
            }

        });

        return currVariation;
    }

    function select2FormatState(state) {
        let element = $(state.element);

        let response;
        response = $(`<span class="${(element.length && element.attr('class') ? element.attr('class') : '')}"></span><span class="orders-select-option">${state.text}</span>`);

        return response;
    }

    function updateProductQuantity(evt, that) {
        let quantity;

        if (evt.type === 'click')
            return false;

        let e = evt || window.event;
        let key = e.which || e.keyCode;
        let validKeys = (
            key >= 48 && key <= 57 // numbers
            || key >= 96 && key <= 105 // num pad
            || key === 8 // backspace
            || key === 46 // delete
        );

        if (!validKeys)
            that.val(!isNaN(parseInt(that.val())) ? parseInt(that.val()) : 1);

        if ((parseInt(that.val()) < 1 && !isNaN(parseInt(that.val()))))
            that.val(1);
        else if (that.val() !== '')
            that.val(parseInt(that.val()));

        quantity = parseInt(that.val());

        if (that.data('max-value') !== undefined && that.data('max-value') !== '' && that.data('max-value') < quantity) {
            that.val(that.data('max-value'));
            if (that.closest('.qty').hasClass('decreasedStock'))
                quantity = parseInt(that.val());
        }

        return quantity;
    }

    function manipulateAddOrderProductsBtn() {

        let btn = $('#add-order-products'),
            addOrderProductsForm = $('#add-order-products-form'),
            productsIdsVal = addOrderProductsForm.find('input[type=hidden][name="order_products[id][]"]').serializeArray(),
            productsIds = [],
            countItemsBlock = addOrderProductsForm.find('.count-items-block').not('.hidden');

        if (productsIdsVal.length)
            productsIdsVal.map(item => {
                productsIds.push(item.value);
            });


        if (!btn.length || !addOrderProductsForm.length)
            return false;

        btn.prop('disabled', countItemsBlock.length !== productsIds.length || !productsIds.length);

        return countItemsBlock.length !== productsIds.length || !productsIds.length;
    }

    function manipulateTotalsBlock(response) {
        let totalsBlock = $('#totals-block');

        if (!totalsBlock.length)
            return false;

        if (response.appliedCoupons != undefined){
            $('.applied-coupons-container').removeClass('hidden');
            $('.applied-coupons').html(response.appliedCoupons)
        }


        if (response.cartSubtotal && response.cartSubtotalStr)
            totalsBlock.find('.subtotalBlock').html(response.cartSubtotalStr);
        else
            totalsBlock.find('.subtotalBlock').html('-');

        if (response.cartDiscountAmount && response.cartDiscountAmountStr)
            totalsBlock.find('.discountBlock').html(response.freeShippingStr ? response.freeShippingStr : response.cartDiscountAmountStr);
        else
            totalsBlock.find('.discountBlock').html('-');

        if (response.shippingAmount && response.shippingAmountStr && !response.freeShippingStr)
            totalsBlock.find('.shippingBlock').html(response.shippingAmountStr);
        else
            totalsBlock.find('.shippingBlock').html('-');

        if (response.cartTotal && response.cartTotalStr)
            totalsBlock.find('.totalBlock').html(response.cartTotalStr);
        else
            totalsBlock.find('.totalBlock').html('-');
    }
})
(jQuery);