;(function ($) {
    $(document).ready(function () {

        $(document).on('select2:select', '#chose-home-products', function () {
            getProduct($(this));
        });

        $('#add-home-products').on('click', function () {
            $('#homeProductsBlock').prepend($('#add-home-products-form').html());
            $('.arcticmodal-close').trigger('click');
            $('#add-home-products-form').html(' ');
        });

        $(document).on('click', '.remove-home-product', function () {
            let productVariationBlock = $(this).closest('.chose-product-variation-block'),
                allProductVariationBlock = productVariationBlock.siblings('.chose-product-variation-block'),
                addProductsForm = $('#add-home-products-form'),
                emptyHomeProductList = addProductsForm.find('.empty-home-product-list');


            // let productsIds = addProductsForm.serializeArray();
            // console.log(productsIds);
            //
            // return false;

            productVariationBlock.fadeOut(300, function () {
                if (!allProductVariationBlock.length)
                    emptyHomeProductList.show();
                $(this).remove();
                manipulateAddOrderProductsBtn();
            });
        });

        $(document).on('click', '.edit-product-order', function () {
            let _this = $(this),
                currTr = _this.closest('tr'),
                qtyBlock = currTr.find('td.product-quantity'),
                changeProductStockBlock = qtyBlock.find('.change-product-stock');

            _this.toggleClass('active');
            changeProductStockBlock.toggleClass('hidden', !_this.hasClass('active'));
            qtyBlock.find('span').toggleClass('hidden', _this.hasClass('active'));

        }).on('keypress', ".update-product-quantity", function (e) {
            if (e.which === 13) {
                e.preventDefault();
                return false;
            }
        });

        $(document).on('click', '.destroy-home-product', function () {
            let _this = $(this),
                url = _this.data('url'),
                orderProductsBlock = _this.closest('.order-products-block'),
                currTr = _this.closest('tr'),
                productId = currTr.data('order-product-id');

            if (!(_this.data('added'))){
                currTr.fadeOut(300, function () {
                    currTr.remove();
                });

                return false;
            }

            $.ajax({
                method: 'POST',
                url: url,
                beforeSend: function () {
                    orderProductsBlock.addClass('loading');
                },
                data: {
                    productId: productId
                },
            }).success(function (response) {
                orderProductsBlock.removeClass('loading');

                if (response.msg)
                    toastr[response.msg.type](response.msg.e);

                if (response.status) {
                    currTr.fadeOut(300, function () {
                        currTr.remove();
                    });
                }

            }).fail(function (response) {
                orderProductsBlock.removeClass('loading');
                let jsonResponse = response.responseJSON;

                if (jsonResponse.msg)
                    toastr[jsonResponse.msg.type](jsonResponse.msg.e);
            });
        });

        /** Add products*/
    });

    function getProduct(that) {
        let productId = that.val(),
            url = that.data('product-content-url'),
            addProductsForm = $('#add-home-products-form'),
            addedProductsCount = 1,
            emptyHomeProductList = addProductsForm.find('.empty-home-product-list');

        $.ajax({
            method: 'POST',
            url: url,
            beforeSend: function () {
                addProductsForm.addClass('loading');
            },
            data: {
                productId: productId,
                productsCount: addedProductsCount
            },
        }).success(function (response) {
                addProductsForm.removeClass('loading');

                if (response.msg)
                    toastr[response.msg.type](response.msg.e);
                if (response.status) {
                    emptyHomeProductList.hide();
                    addProductsForm.prepend(response.view);
                    manipulateAddHomeProductsBtn();
                    setTimeout(function () {
                        addProductsForm.find('select.select2').select2({
                            templateResult: select2FormatState
                        });
                        addProductsForm.find('select.select2.no-search').select2({
                            minimumResultsForSearch: -1,
                            templateResult: select2FormatState
                        });
                    }, 100);
                    that.val('').trigger('change');
                }
        }
        ).fail(function (response) {
            addProductsForm.removeClass('loading');
            let jsonResponse = response.responseJSON;

            if (jsonResponse.msg)
                toastr[jsonResponse.msg.type](jsonResponse.msg.e);
        });
    }

    function select2FormatState(state) {
        let element = $(state.element);
        let response;
        response = $(`<span class="${(element.length && element.attr('class') ? element.attr('class') : '')}"></span><span class="orders-select-option">${state.text}</span>`);
        return response;
    }

    function manipulateAddHomeProductsBtn() {
        let btn = $('#add-home-products');
        btn.prop('disabled', false);
        return true;
    }
})
(jQuery);