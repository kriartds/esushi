;(function ($) {
    $(document).ready(function () {

        //    Manipulate shipping methods

        $('#shipping_method_type').on('change', function () {
            let _this = $(this),
                currVal = _this.val(),
                flatPickupBlock = $('#flat-method-block'),
                freeMethodBlock = $('#free-method-block'),
                freeShippingRequires = $('#free_shipping_requires');

            if (!flatPickupBlock.length || !freeMethodBlock.length)
                return false;

            if (currVal === 'flat' || currVal === 'pickup') {
                flatPickupBlock.removeClass('hidden');
                freeMethodBlock.addClass('hidden');
            } else {
                flatPickupBlock.addClass('hidden');
                freeMethodBlock.removeClass('hidden')
                $('#min-order-amount-block').toggleClass('hidden', !(freeShippingRequires.length && freeShippingRequires.val() !== 'no_action' && freeShippingRequires.val() !== 'free_coupon'));
            }
        });

        $('#free_shipping_requires').on('change', function() {
            let _this = $(this),
                currVal = _this.val(),
                minAmountBLock = $('#min-order-amount-block');

            if(!minAmountBLock.length)
                return false;

            minAmountBLock.toggleClass('hidden', !(currVal !== 'no_action' && currVal !== 'free_coupon'));

        });

        //    Manipulate shipping methods

    }); //END on ready

})
(jQuery);