/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
    config.removeDialogTabs = 'link:upload;image:upload';
    config.height = 350;
    //config.disallowedContent = 'span; *{font*}; *{style*}; *{class*}; table[border*,class*]{*}; td[style]{*}';
    //config.allowedContent = true;
    //config.extraAllowedContent = 'span(*)';
    //config.pasteFilter = 'semantic-text';
    //editor.pasteFilter.disallow( 'span; *{font*}; *{style*}; *{class*}; table[border*,class*]{*}; td[style]{*}' );
    config.startupFocus = false;
    config.pasteFilter = 'semantic-content';
    config.removePlugins = 'image';
    config.extraPlugins = 'image2';
    config.toolbar = [
        { name: 'document',    items: [ 'Source', '-',  'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
        { name: 'clipboard',   items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-','Undo', 'Redo' ] },
        { name: 'editing',     items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
        { name: 'insert',      items: [ 'CreatePlaceholder', 'Image', 'Flash', 'Table', 'HorizontalRule', 'PageBreak', 'Iframe', 'InsertPre' ] },
        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
        { name: 'paragraph',   items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'BidiLtr', 'BidiRtl' ] },
        { name: 'links',       items: [ 'Link', 'Unlink', 'Anchor' ] },
        { name: 'tools',       items: [ 'UIColor', 'Maximize', 'ShowBlocks' ] },
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        '/',
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] }
    ];

};
