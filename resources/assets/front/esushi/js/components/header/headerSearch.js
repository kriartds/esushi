(function ($) {

    $(document).ready(function () {
        let searchSetTimeout;

        $('#search-form input[type="text"]').on('keyup', function (evt) {
            let form = $(this).closest('form'),
                e = evt || window.event,
                key = e.which || e.keyCode,
                submitBtn = form.find('button[type="submit"]'),
                serializedForm = form.serializeArray(),
                searchResult = $("#searchResult");

            let validKeys = (
                key >= 48 && key <= 57 // numbers
                || key >= 65 && key <= 90 // letters
                || key >= 96 && key <= 105 // num pad
                || key >= 186 && key <= 222 // symbols
                || key === 8 // backspace
                || key === 46 // delete
                || key === 13 // enter
            );
            if (!validKeys || $(this).val().length < 3)
                return false;

            submitBtn.prop('disabled', false);
            submitBtn.addClass('button_loading');

            searchResult.slideUp(200, function () {
                $(this).html('');
            });

            if (searchSetTimeout !== undefined)
                clearTimeout(searchSetTimeout);

            searchSetTimeout = setTimeout(function () {
                $.ajax({
                    method: "POST",
                    url: form.attr('action'),
                    beforeSend: function () {
                        form.addClass('loading');
                    },
                    data: serializedForm
                })
                    .success(function (response) {
                        form.removeClass('loading');
                        submitBtn.removeClass('button_loading');
                        submitBtn.prop('disabled', !response.status);

                        if (response.status) {

                            if (response.view.length) {
                                searchResult.html(response.view);
                                searchResult.slideDown();
                            }
                        } else
                            searchResult.slideUp(200);

                    })
                    .fail(function (response) {
                        form.removeClass('loading');
                        submitBtn.removeClass('button_loading');
                        searchResult.slideUp(200);
                    });
            }, 1000);
        }).on('focus', function () {
            let searchContainerOnHeader = $('.header .searchContainerOnHeader');
            searchContainerOnHeader.find('.searchMask').show();
            searchContainerOnHeader.find('#search-form').addClass("active");
        });

        $('#search-form button[type="reset"]').on('click', function () {
            $(this).hide();
        });

        // Search form
    });

})(jQuery);