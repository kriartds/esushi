(function ($) {

    $(document).ready(function () {
        let teamBlock = $('.teamSlider');

        if (teamBlock.length)
            teamBlock.slick({
                infinite: false,
                dots: false,
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
            });

    })

})(jQuery);