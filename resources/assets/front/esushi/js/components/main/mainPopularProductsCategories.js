(function ($) {

    $(document).ready(function () {
        let popularCategoriesBlock = $(".popularCategoriesSlider");
        if (popularCategoriesBlock.length) {
            popularCategoriesBlock.slick({
                infinite: true,
                dots: true,
                arrows: false,
                slidesToShow: 6,
                slidesToScroll: 6,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 5,
                            slidesToScroll: 5,
                        }
                    },
                    {
                        breakpoint: 1025,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 4
                        }
                    },
                    {
                        breakpoint: 769,
                        settings: {
                            slidesToScroll: 3,
                            variableWidth: true
                        }
                    },
                    {
                        breakpoint: 481,
                        settings: {
                            slidesToScroll: 2,
                            variableWidth: true
                        }
                    }
                ]
            });

        }

    })

})(jQuery);