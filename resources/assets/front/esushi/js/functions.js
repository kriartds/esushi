(function ($) {
    window.submitForm = function (parentThat) {
        let submitBtn = $(parentThat);
        let form = $('#' + submitBtn.data('form-id'));
        let data = new FormData();

        if (form.length < 1) {
            return;
        }
        let serializedForm = form.find("select, textarea, input").serializeArray();
        let files = form.find("input[type=file]")
        for (let i = 0; i < serializedForm.length; i++) {
            data.append(serializedForm[i].name, serializedForm[i].value);
        }
        if (files.length > 0) {
            for (let i = 0; i < files.length; i++) {
                if ( files[i].files[0] !== undefined)
                data.append(files[i].name, files[i].files[0]);
            }
        }

        form.find('input, textarea, select').removeClass('error');
        form.find('div.errorMsg').remove();
        $.ajax({
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            method: "POST",
            url: form.attr('action'),
            beforeSend: function () {
                submitBtn.addClass('loading');
            },
            data: data
        })
            .success(function (response) {
                window.successErrorAjaxBtn(submitBtn, response.status);
                if (response.msg && response.msg.message)
                    toastr[response.msg.type](response.msg.message);

                if (response.status) {
                    if (response.redirect)
                        window.location.href = response.redirect;

                    if (!response.preventClearForm)
                        form.find('input:not(input[type=hidden]), textarea').val('');
                    form.find('input, textarea, select').removeClass('error');
                } else {
                    if (response.validator && response.msg) {
                        $.each(response.msg.e, function (ObjNames, ObjValues) {
                            let field = form.find("[name='" + ObjNames + "']");
                            if (field.length > 0) {
                                field.addClass('error');
                                if (field.prop("tagName") === 'SELECT') {
                                    field.closest('.selectContainer').append(`<div class="errorMsg ${ObjNames}">${ObjValues}</div>`);
                                } else {
                                    if (field.attr('type') === 'checkbox') {
                                        field.closest('.field-wrap').append(`<div class="errorMsg ${ObjNames}">${ObjValues}</div>`);
                                    } else {
                                        field.after(`<div class="errorMsg ${ObjNames}">${ObjValues}</div>`);
                                    }
                                }
                            }
                        });
                    }
                }

            })
            .fail(function (response) {
                // window.successErrorAjaxBtn(submitBtn, false);
                let jsonResponse = response.responseJSON;
                if (jsonResponse.msg && jsonResponse.msg.message)
                    toastr[jsonResponse.msg.type](jsonResponse.msg.message);
            });
    };

    window.successErrorAjaxBtn = function (btn, status, timeout = 1000) {
        if (timeout === undefined || !$.isNumeric(timeout))
            timeout = 1000;

        if (status)
            btn.addClass('added');
        else
            btn.addClass('error');

        setTimeout(function () {
            btn.removeClass('loading').removeClass('added').removeClass('error');
        }, timeout);
    };
})
(jQuery);