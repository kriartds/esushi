(function ($) {
    //on load
    $(window).on('load', function () {

    });


    //on ready
    $(document).ready(function () {
        //run modals
        $('.getModal').on('click', function (e) {
            e.preventDefault();
            let target_modal = $(this).attr('href');
            if (!target_modal) {
                target_modal = $(this).data('modal');
            }
            $.arcticmodal('close');

            $(target_modal).arcticmodal({
                beforeOpen: function () {
                    $(target_modal).addClass('openEffect').addClass('isOpen');
                    $(target_modal).removeClass('closeEffect');
                },
                beforeClose: function () {
                    $(target_modal).removeClass('openEffect');
                    $(target_modal).addClass('closeEffect');

                    setTimeout(function() {
                        $(target_modal).removeClass('isOpen');
                    }, 600);
                },
                afterOpen: function () {

                    if ($('#modal-cart').height() > 0) {

                        let headerHeight = $('#cart-header').height();
                        let sliderHeight = $('#cart-slider').height();
                        let buttonHeight = $('#cart-button').height();
                        let paddings = 48 + 36;

                        let offset = headerHeight + sliderHeight + buttonHeight + paddings;

                        $('#cart-scroll').css('height', 'calc( 100vh - ' + offset + 'px)');

                    }

                }
            });
            return false;
        });

        $('.getProductModal').on('click', function (e){
            e.preventDefault();
            let _this = $(this);
            var product_slug = $(this).attr('href').split('/').pop();
            $.ajax({
                url: base_url + '/getProductDetails',
                data: {product_slug: product_slug},
                success: function (res){
                    //console.log(res);
                    $('#productModal').remove();
                    $('body').append(res);
                    $.arcticmodal('close');
                    let target_modal = '#modal';
                    $(target_modal).arcticmodal({
                        beforeOpen: function () {
                            $(target_modal).addClass('openEffect').addClass('isOpen');
                            $(target_modal).removeClass('closeEffect');
                        },
                        beforeClose: function () {
                            $(target_modal).removeClass('openEffect');
                            $(target_modal).addClass('closeEffect');

                            if(history.pushState) {
                                //history.pushState(null, null, base_url);
                                //console.log(document.referrer);
                                history.back();
                            }

                            setTimeout(function() {
                                $(target_modal).removeClass('isOpen');
                            }, 600);
                        },
                        afterOpen: function () {
                            console.log(history.pushState);
                            if(history.pushState) {
                                history.pushState(null, null, _this.attr('href'));
                            }
                            // if ($('#modal-cart').height() > 0) {
                            //
                            //     let headerHeight = $('#cart-header').height();
                            //     let sliderHeight = $('#cart-slider').height();
                            //     let buttonHeight = $('#cart-button').height();
                            //     let paddings = 48 + 36;
                            //
                            //     let offset = headerHeight + sliderHeight + buttonHeight + paddings;
                            //
                            //     $('#cart-scroll').css('height', 'calc( 100vh - ' + offset + 'px)');
                            //
                            // }

                        }
                    });
                },
            });

            return false;
        });

        // //masked input
        // // const phoneMask = ['+', '373', ' ', '(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/]; // +7 (___) ___-__-__
        const phoneMask = ['+', '3', '7', '3', ' ', '(', 0, /\d/, /\d/,')', ' ', /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/]; // +373 (__) __-__-__
        const phoneInputs = document.querySelectorAll('input[name=phone]');
        phoneInputs.forEach(function (phoneInput) {
            vanillaTextMask.maskInput({
                inputElement: phoneInput,
                mask: phoneMask,
                placeholderChar: '_',
                // showMask: true
            });
        });

        // hide placeholder on focus and return it on blur
        $('input, textarea').focus(function () {
            $(this).data('placeholder', $(this).attr('placeholder'));
            $(this).attr('placeholder', '');
        });
        $('input, textarea').blur(function () {
            $(this).attr('placeholder', $(this).data('placeholder'));
        });

        /* Dropdowns */

        $(".dropdown__btn").click(function (e) {
            $(this).parent('.dropdown').toggleClass("dropdown-active");
            $(".dropdown").not($(this).parent(".dropdown")).removeClass("dropdown-active");
            e.stopPropagation();
        })

        $(document).on("click", function (e) {
            if (!$(".dropdown").has(e.target).length && !$(".dropdown").is(e.target)) {
                $(".dropdown").removeClass("dropdown-active");
            }
        });


        /* Open search */

        var scrollbarWidth = window.innerWidth - $(document).width()

        $('.header__open-search-btn').on('click', function () {
            $(this).siblings(".header__search-form").addClass("header__search-form_visible");
            $(".header__close-search-form").addClass("header__close-search-form_visible");
            $("body").toggleClass("body_fixed")
                .css({"overflow": "hidden", "margin-right": scrollbarWidth});
            setTimeout(function () {
                $(".header__search-input-container input").focus();
            }, 100);
        });

        $(".header__close-search-form").on("click", function () {
            $(".header__search-form").removeClass("header__search-form_visible");
            $(".header__close-search-form").removeClass("header__close-search-form_visible");
            $("body").toggleClass("body_fixed")
                .css({"overflow": "auto", "margin-right": "0"});
        });


        /* Open mobile nav */

        $(".header__open-mobile-nav").on("click", function () {
            $(this).toggleClass("header__open-mobile-nav_opened");
            $(".nav__nav-container").toggleClass("nav__opened");
            $(".nav__close-nav").toggleClass("nav__close-nav_visible");
            $("body").toggleClass("body_fixed");
        })

        $(".nav__close-nav").on("click", function () {
            $(".header__open-mobile-nav").toggleClass("header__open-mobile-nav_opened");
            $(".nav__nav-container").toggleClass("nav__opened");
            $(".nav__close-nav").toggleClass("nav__close-nav_visible");
            $("body").toggleClass("body_fixed");
        })


        //slider home page header

        $('.slider-for').slick({
            slidesToShow: 1,
            arrows: false,
            asNavFor: '.slider-nav',
            vertical: true,
            //verticalSwiping: true,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        dots: true,
                        vertical: false,
                    }
                },
            ]
        });

        $('.slider-nav').slick({
            slidesToShow: 3,
            asNavFor: '.slider-for',
            vertical: true,
            focusOnSelect: true,
            arrows: true,
        });

    /*** Show/hide menu in header for catalog page ***/
        if ($(window).width() <= 1024 ) {
            if ($(".catalog-page")[0]){
                $('.btnAnchorSlider').removeClass('hidden')
                $('.header__logo').addClass('hidden')
            } else {
                $('.btnAnchorSlider').addClass('hidden')
                $('.header__logo').removeClass('hidden')
            }
        }

        /*** Open slider anchore on mobile ***/
        $('.btnAnchorSlider').on('click', function () {
            $(this).toggleClass('active')
            $(".anchorSlider").toggleClass('active')
            $('.closeAnchorSlider').toggleClass('active')
        })

        //slider anchore

        $('.multiple-items').slick({
            slidesToShow: 10,
            slidesToScroll: 10,
            prevArrow: $('.slick-prev'),
            nextArrow: $('.slick-next'),
            infinite: false,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 7.5,
                        slidesToScroll: 7,
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 6.5,
                        slidesToScroll: 6,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4.5,
                        slidesToScroll: 4,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3.5,
                        slidesToScroll: 3,
                        arrows: false,
                    }
                }

            ]
        });

        //cart


        $('.multiple-items-cart').slick({
            infinite: true,
            variableWidth: true,
            dots: false,
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: false,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        infinite: true,

                    }
                },
                {
                    breakpoint: 780,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1
                    }
                }
            ]
        });



        $(".categoriesListLink").click(function (e) {
            $(".categoriesListLink").removeClass("active");
            $(this).addClass("active");
            $(".closeAnchorSlider").removeClass("active")
            $(".anchorSlider").removeClass("active")
            $(".btnAnchorSlider").removeClass("active")
        });

        $(".closeAnchorSlider").click(function (e) { 
            $(this).removeClass('active')
            $(".anchorSlider").removeClass("active")
            $(".btnAnchorSlider").removeClass("active")
        })

        $(".categoriesListItem ").click(function (e) {
            $(".categoriesListItem ").removeClass("active");
            $(this).addClass("active");
        });


        // stiky menu anchor
        var link = $('#anchor-slider-main');

        if (link.length) { 
            const headerHeight = $(".header").height();
            const anchorSlider = $(".anchorSlider");
            
            const stickyHandler = () => {
                anchorSlider.css('top', headerHeight);
            }

            const visibilityHandler = () => {
                if (anchorSlider.offset().top == $(".header").offset().top + headerHeight) {
                    anchorSlider.addClass('static');
                } else {
                    anchorSlider.removeClass('static');
                }
            }

            stickyHandler();
            visibilityHandler();

            $(window).scroll(function () {
                stickyHandler();
                visibilityHandler();
            })
        }


        //scroll to anchors
        $('.scrolling').click(function () {
            let target = $(this).attr('href').match('#');
            target = target.input.slice(target['index']);
            $('html, body').animate({scrollTop: $(target).offset().top - 200}, 500);
            return false;
        });

        /*** Select2 ***/

        $('.select2-container').select2({
            closeOnSelect: true
        });
        $('.select2-sort').select2({
            closeOnSelect: false,
            dropdownCssClass: 'dropdown-checkbox',
            containerCssClass: 'select-checkbox',
            width: '300px'
            // placeholder: "Select a state",
        });
        $('select.select2-container.no-search').select2({
            minimumResultsForSearch: -1
        });


        //order history
        $(".shortInfo").click(function () {
            $(this).toggleClass("active").siblings(".mainInfo").stop().slideToggle();
        })

        /*** Open comment area ***/
        $(" .addCommentBtn").on("click", function () {
            $(this).siblings().stop().slideToggle(300);
        });
        $(".addCommentBtn").click(function (e) {

            $(this).toggleClass("show-hiden");// + -
        });


        /*** pentru catalog hide la logo ca sa aratam categroriile ***/

        // var pathname = window.location.pathname;
        // console.log(pathname, 'Cacat mare, trebuie de fixat!!!');

        // if(pathname == '/') {
        //     $('.footer').addClass('hide');
        // }

        //     if (pathname == '/catalog.html') {
        //
        //     $('.header').addClass('hide');
        //
        //
        //     $(document).on("scroll", onScroll);
        //     function onScroll(event){
        //         var scrollPos = $(document).scrollTop();
        //
        //         $('#menu-center a').each(function () {
        //
        //             var currLink = $(this);
        //             var refElement = $(currLink.attr("href"));
        //
        //             if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
        //                 $('#menu-center a').removeClass("active");
        //                 currLink.addClass("active");
        //             }
        //             else{
        //                 currLink.removeClass("active");
        //             }
        //
        //         });
        //     }
        // }

        $('.one-time').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true,
            arrows: false
        });


        //MENU CATATLOG

        $(".showAnchor").click(function (e) {
            $(".anchorSlider").toggleClass("show-hiden");// add class
        });

        //log pop up

        //pop up

        // $(".hideContent").click(function (e) {
        //     $(".padding").addClass("changeContent");// add class
        // });





///input value

        // $("#inputId1").keyup(function () {
        //     var value = $(this).val();
        //     $("#inputId2").attr("placeholder", value);
        // });
        //
        //
        // //input disabled login
        //
        // $(".edit").click(function() {
        //     $('#inputId2').prop('disabled', function( i, currentValue ) {
        //         return !currentValue;
        //     });
        // });

///SA ascunde futeru la homepage responsive


        //  functions for Toastr

        // toastr.options = {
        //     "debug": false,
        //     "newestOnTop": false,
        //     "progressBar": false,
        //     "preventDuplicates": false,
        //     "onclick": null,
        //     "showDuration": "300",
        //     "hideDuration": "1000",
        //     "showEasing": "swing",
        //     "hideEasing": "linear",
        //     "showMethod": "fadeIn",
        //     "hideMethod": "fadeOut",
        //     "positionClass": "toast-bottom-left",
        //     "timeOut": 2000,
        //     "closeButton": true,

        //   // "timeOut": 0,
        //   //  "extendedTimeOut": 0
        // };
        // toastr["warning"]( "Mesajul este expediat.");
        // toastr["info"]( "Mesajul este expediat.");
        // toastr["error"]("Datele personale nu sant salvate din cauza erorii.");
        // toastr["success"]("Asteptati sunetul operatorului timp de 3-8 minute. În caz contrar reveniți cu un apel! ", "Comanda plasata cu succes! ");


        /*** cumpara intr-un click Produc page ***/

        $('#one-click-input').on('keyup', function () {

            if ( $(this).val().length > 3) {
                $('#one-click-send').attr('disabled', false)
            } else {
                $('#one-click-send').attr('disabled', true)
            }

        });

    }); //END on ready
})(jQuery);





