(function ($) {

    $(document).ready(function () {

        /*** Open tabs ***/
        $(".cartTabs .navTabs .navItem").on("click", function () {
            let tabName = $(this).attr("data-tab"),
                cartTotalPriceBlock = $('.cart-total-price');

            $(this).siblings(".navItem").removeClass("active");
            $(this).addClass("active");
            $(this).parent().siblings().children().removeClass("active");
            $(document).find(".tabPane[data-tab=" + tabName + "]").addClass("active");

            if (cartTotalPriceBlock.length)
                if ($(this).data('tab') === 'pickUp') {
                    cartTotalPriceBlock.html(cartTotalPriceBlock.data('no-shipping-amount'));
                } else if ($(this).data('tab') === 'delivery') {
                    cartTotalPriceBlock.html(cartTotalPriceBlock.data('shipping-amount'));
                }
        });

    });


})(jQuery);