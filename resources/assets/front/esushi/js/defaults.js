(function ($) {
  window.globalLang = $("html").attr("lang");

  $(document).on("ready", function () {

    $.ajaxSetup({
      headers: {
        "x-csrf-token": $('meta[name="csrf-token"]').attr("content"),
      },
    });

    /*** Submit form ***/
    $(document).on("click", ".submit-form-btn", function (e) {
      e.preventDefault();
      submitForm(this);
    });
    /*** Submit form ***/
  });
})(jQuery);
