(function ($) {
    //on load
    $(window).on("load", function () {
    });

    //on ready
    $(document).ready(function () {

        /*** Change currencies ***/
        $('.dropdown__menu.currency a.dropdown__item').on('click', function (e) {
            e.preventDefault();
            let itemId = $(this).data('item-id');
            $.ajax({
                method: "POST",
                url: `${window.location.origin}/${window.globalLang}/setCurrency`,
                data: {
                    itemId: itemId
                }
            }).success(function (response) {
                if (response.status) {
                    if (location.href.indexOf("search") != -1) {
                        window.location.href = window.location.href
                    } else {
                        window.location.href = window.location.href.split('?')[0];
                    }
                }
            })
        });

        /* Write file name on field */
        $('#uploadedCv').change(function (e) {
            var filename = e.target.files[0].name;
            $('.uploaded-file-name p').html(filename);
            $(".file-feature").hide();
            $(".uploaded-file-name").addClass("active");
        });

        /* Delte file from input */
        $(".delete-file-btn").on("click", function () {
            $("input[name='uploadedCv']").val("");
            $(this).parent().removeClass("active");
            $(".file-feature").show();
        })

        $(".product__serving input[type='radio']").change(function () {
            let thisel = $(this);
            let amountStr = '';

            if (thisel.data('sale-price')) {
                amountStr += '<div class="price">' + thisel.data('sale-price') + current_currency_name + '</div>';
                if (thisel.data('price'))
                    amountStr += '<div class="oldPrice">' + thisel.data('price') + current_currency_name + '</div>';
            } else if (thisel.data('price'))
                amountStr += '<div class="price">' + thisel.data('price') + current_currency_name + '</div>';
            else
                amountStr += '--';

            thisel.parent().parent().next().find('.product__price').html(amountStr);
        });

        $(document).on('click', '.social-button', function (e) {
            var popupSize = {
                width: 780,
                height: 550
            };

            var verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
                horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

            let url = encodeURIComponent(document.URL);

            var popup = window.open($(this).data('share-url') + url, 'social',
                'width=' + popupSize.width + ',height=' + popupSize.height +
                ',left=' + verticalPos + ',top=' + horisontalPos +
                ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

            if (popup) {
                popup.focus();
                e.preventDefault();
            }
        });
        //copy-link

        $(document).on('click', '#partyProducts .add-party-product', function () {
            let productsInputs = $('#productIds'),
                product = $(this),
                currentProductId = product.data('product-id');

            let addedProducts = JSON.parse(productsInputs.val());
            addedProducts.push(currentProductId);
            productsInputs.val('[' + addedProducts.toString() + ']');

            product.closest('.offer').addClass('added')

            product.addClass('grayBtn')
            product.removeClass('add-party-product')
            product.addClass('delete-party-product')
            product.html(product.data('delete-text'))
        });
        $(document).on('click', '#partyProducts .delete-party-product', function () {
            let productsInputs = $('#productIds'),
                product = $(this),
                currentProductId = product.data('product-id');


            let addedProducts = JSON.parse(productsInputs.val());

            addedProducts2 = $.grep(addedProducts, function (value) {
                return value != currentProductId;
            });

            productsInputs.val('[' + addedProducts2.toString() + ']');

            product.closest('.offer').removeClass('added')
            product.removeClass('grayBtn')
            product.addClass('add-party-product')
            product.removeClass('delete-party-product')
            product.html(product.data('add-text'))
        });
        $(document).on('click', '#partyProducts .delete-offer', function () {
            $('#partyProducts .delete-party-product').trigger('click');
        });
        $(document).on('click', '#repeat-order', function () {
            let repeatOrder = $(this);

            $.ajax({
                method: "POST",
                url: repeatOrder.data('url'),
                beforeSend: function () {
                    repeatOrder.addClass('loading');
                },
                data: {
                    orderId: repeatOrder.data('order-id'),
                },
            })
                .success(function (response) {
                    // window.successErrorAjaxBtn(submitBtn, response.status);
                    if (response.msg && response.msg.message)
                        toastr[response.msg.type](response.msg.message);

                    if (response.status && response.redirect) {
                        window.location.href = response.redirect;
                    }
                })
                .fail(function (response) {
                    // window.successErrorAjaxBtn(submitBtn, false);
                    let jsonResponse = response.responseJSON;
                    if (jsonResponse.msg && jsonResponse.msg.message)
                        toastr[jsonResponse.msg.type](jsonResponse.msg.message);
                });
        });

        $(document).on('click', '#copy-link', function () {

            var dummy = document.createElement('input'),
                text = window.location.href;

            document.body.appendChild(dummy);
            dummy.value = text;
            dummy.select();
            document.execCommand('copy');
            document.body.removeChild(dummy);

            alert("URL: " + text + " successfuly copied.");
        });

    });
})(jQuery);
