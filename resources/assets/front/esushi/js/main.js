//=include partials/jquery-1.11.1.min.js
//=include partials/jquery.arcticmodal-0.4.min.js
//include partials/jquery.arcticmodal-0.3.min.js
//=include partials/svgxuse.min.js
//=include partials/jquery.tabslet.min.js
//=include partials/nouislider.min.js


//=include partials/vanillaTextMask.js
//=include partials/jquery.formstyler.min.js
//=include partials/slick.min.js
//=include partials/toastr.js
//=include partials/hammer.min.js

//=include partials/cartTabs.js
//=include partials/select2.min.js
//=include partials/filter.js
//=include partials/product-page.js

//=include ./functions.js
//=include ./defaults.js
//=include ./components/productPage/addToCart.js
//=include ./components/cart/cart.js
//=include ./components/header/headerSearch.js
//=include ./components/nav/nav.js

//=include partials/scripts.js
//=include ./scripts.js