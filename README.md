
Template API
========================

Requirements
========================

- PHP 7.4
- MySQL 5.7
- NGINX or apache2
- composer
- Node.js

Prepare development environment(on linux)
========================

1. Install [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) and [Docker-compose](https://docs.docker.com/compose/install/)
2. Clone repository
3. If local changes is needed update .env file(optional) 
4. Create and start docker containers  
    ```
    $ /scripts/start-dev.sh
    ```
5. Enter to the project environment  
    ```
    $ scripts/backend.sh 
    ```
6. Install all laravel components  
    ``` 
    $ composer install
    ```
7. Make database migration (!!! IN PROCESS, Use the SQL file, which is in the directory "database" )
    ```
    $ php artisan migrate
    ```
   
9. Open our project: [localhost:8080](localhost:8080), Enjoy!

**Install webpack**

1. Install [Node.js](https://nodejs.org/en/download/) and [gulp package manager](https://classic.yarnpkg.com/en/docs/install#debian-stable)
2. Install encore packages  
    ```
    $ npm install
    ```
3. Compile assets  
    ```
    $ gulp
    ```

**Optional:**

* Update your system host file(add template.local):  
    ```
    $ sudo -- sh -c "echo  \ \ >> /etc/hosts";sudo -- sh -c "echo $(docker network inspect bridge | grep Gateway | grep -o -E '[0-9\.]+')  template.local >> /etc/hosts"
    ```  
    Open our project with: [template.local:8080](template.local:8080)

Commands for environment management
========================
**DOCKER containers**

* Start container(project)  
    ```
    $ scripts/start-dev.sh
    ```
* Enter to the container environment  
    ```
    $ scripts/backend.sh  
    ```
* Stop containers  
    ```
    $ scripts/stop-dev.sh  
    ```

Useful commands
========================

```
# bash commands
$ docker-compose exec php bash

# Composer install
$ docker-compose -f docker-compose.yml exec php composer install

# Composer (e.g. composer update)
$ docker-compose exec php composer update

# Retrieve an IP Address (here for the nginx container)
$ docker inspect --format '{{ .NetworkSettings.Networks.dockersymfony_default.IPAddress }}' $(docker ps -f name=nginx -q)
$ docker inspect $(docker ps -f name=nginx -q) | grep IPAddress

# MySQL commands
$ docker-compose exec db mysql -uroot -p"root"

# Check docker containers information
$ docker ps -a

# view specific container logs
$ docker lgos CONTAINER_ID

# Check CPU consumption
$ docker stats $(docker inspect -f "{{ .Name }}" $(docker ps -q))

# Delete all containers
$ docker rm $(docker ps -aq)

# Delete all images
$ docker rmi $(docker images -q)
```

Tests
========================
- In project environment run:  
    ```
    ./bin/phpunit
    ```
